﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.OpCodes;

namespace APIManager
{
    public class BaseResponse : ICommand
    {
        public string JSONObjectArgs { get; set; }
        public string UserID { get; set; }
        public string Command { get; set; }
        public string Entity { get; set; }
        public string RequestID { get; set; }

        public virtual dynamic Execute()
        {
            throw new NotImplementedException();
        }

        public dynamic generateResponse(dynamic result)
        {
            dynamic response = initResponse();
            response.Result = result.IsOK ? "OK" : "Error";
            return response;
        }

        public dynamic generateResponse(Result result)
        {
            dynamic response = initResponse();
            response.Result = result.IsOK ? "OK" : "Error";
            if (result.IsOK) return response;
            response.Error = result.LastException != null ? result.LastException.Message : result.MessageLog;
            return response;
        }

        public dynamic initResponse()
        {
            dynamic response = new System.Dynamic.ExpandoObject();
            response.RequestID = RequestID;
            response.Command = Command;
            response.Entity = Entity;
            return response;
        }


       
    }
}
