﻿using APIManager.Commands;
using Infrastructure;
using Infrastructure.Entities;
using Infrastructure.Responses;
using Newtonsoft.Json;
using Server.Infrastructure;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Collections.Specialized;
using System.Dynamic;
using Log4Tech;
using Newtonsoft.Json.Linq;

namespace APIManager
{
    public class CommandMain
    {
        private static CommandMain _commnadMain;
        private readonly IEnumerable<ICommand> commands;
        private readonly CommandParser _parser;
        private readonly LicenseAPI.Manager _licenseCheck;
        private readonly string _licenseFullPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\license.lic";

        internal ConcurrentDictionary<string, string> connectedUsers = new ConcurrentDictionary<string, string>();

        // Michael - Generic Command - 10/05/2015
        private readonly GenericCommand _genericCommandFactory = new GenericCommand();

        private CommandMain()
        {
            commands = GetCommands();
            _parser = new CommandParser(commands, _genericCommandFactory);
            _licenseCheck = new LicenseAPI.Manager();
        }

        public static CommandMain GetInstance()
        {
            return _commnadMain ?? (_commnadMain = new CommandMain());
        }

        private CommandObject ReadCommand(string data)
        {
            try
            {
                data = CleanString(data);

                var start = data.IndexOf('{');
                var end = data.LastIndexOf('}');
                if (start < 0 || end < 0 || end <= start)
                    return null;
                data = data.Substring(start, end - start + 1);

                var argValues = new NameValueCollection();
                var json = JsonConvert.DeserializeObject(data);
                if (json is IEnumerable)
                {
                    var en = json as IEnumerable;
                    var enm = en.GetEnumerator();
                    while (enm.MoveNext())
                    {
                        if (!(enm.Current is JProperty)) continue;

                        var prop = (JProperty) enm.Current;
                        argValues[prop.Name] = prop.Value.ToString();
                    }
                }

                var command = new CommandObject
                {
                    RequestID = argValues["RequestID"],
                    Command = argValues["Command"],
                    UserID = argValues["UserID"],
                    Entity = argValues["Entity"],
                    Data = argValues["Data"],
                    SiteID = argValues["SiteID"]
                };
                return command;
            }
            catch (Exception ex)
            {
                Log.Instance.Write("Cannot read a command. Exception: " + ex.Message, this, Log4Tech.Log.LogSeverity.DEBUG);
            }
            return null;
        }

        private string CleanString(string data)
        {
            string cleaned = data.Replace("\\\"", "\"");
            while (cleaned != data)
            {
                data = cleaned;
                cleaned = data.Replace("\\\"", "\"");
            }
            return data;
        }

        private int ExtractNumber(string str)
        {
            int index;
            for (index = 0; index < str.Length; index++)
                if (char.IsDigit(str[index]))
                    break;

            if (index > 0)
                str = str.Substring(index);
            return int.Parse(str);
        }

        private List<CommandObject> ParseData(string data)
        {
            try
            {
                var command_list = new List<CommandObject>();
                do
                {
//                    var pos = data.IndexOf('#');
//                    if (pos > 0)
//                    {
//                        var prefix = data.Substring(0, pos);
//                        var length = ExtractNumber(prefix);
//                        if (length <= 0) continue;
//
//                        var content = data.Substring(pos + 1, length);
//                        data = data.Substring(pos + length);
//
//                        var command = ReadCommand(content);
//                        if (command != null)
//                            command_list.Add(command);
//                    }
//                    else
//                    {
                        var command = ReadCommand(data);
                        if (command != null)
                            command_list.Add(command);
                        break;
//                    }
                }
                while (data.Length > 0);
                return command_list;
            }
            catch (Exception ex)
            {
                Log.Instance.Write("Cannot Parse data. Exception: " + ex.Message, this, Log4Tech.Log.LogSeverity.DEBUG);
            }
            return null;
        }

        public byte[] Execute(string data)
        {
            var commandList = ParseData(data);
            if (commandList == null)
                return Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(new Response
                {
                    Result = "Error"
                }));

            if (commandList.Count > 1)
                Log.Instance.Write("Block of commands. We break here !!!", this);

            var json = string.Empty;
            dynamic response = new ExpandoObject();
            response.Result = "Error";
            response.Error = "General error...";
            try
            {

                foreach (var args in commandList)
                {
                    //before execute the command
                    //checks first if this command requires permissions and if the user is allowed
                    if (
                        IsCommandAllowed(new UserCommand
                        {
                            UserID = Convert.ToInt32(args.UserID),
                            Command = args.Command,
                            Entity = args.Entity,
                            SiteID = Convert.ToInt32(args.SiteID)
                        }))
                    {
                        //try to find the right command to execute
                        var command = _parser.Parse(args);
                        

                        if (command == null)
                        {
                            //if return nothing then this method is not implmeneted/supported
                            return
                                Encoding.ASCII.GetBytes(
                                    JsonConvert.SerializeObject(new Response()
                                    {
                                        Result = "Not Supported!",
                                        RequestID = args.RequestID
                                    }));
                        }
                        //before execution checks if the license is valid
                        if (_licenseCheck.IsLicenseExpired(_licenseFullPath))
                        {
                            Log.Instance.Write("License is not valid!", this);

                            response.Result = "Error";
                            response.Error = "License is not valid!";
                            response.UserID = command.UserID;
                            response.RequestID = command.RequestID;
                        }
                        else
                        {
                            response.RequestID = command.RequestID;
                            //checks if users list is empty
                            //then return session timeout
                            if ((!Common.User2Customer.Any() && command.Entity != "login" && command.Entity != "signup" 
                                && command.Entity!="customers" && command.Entity!="user")
                                ||
                                !(Common.User2Customer.ContainsKey(Convert.ToInt32(command.UserID))) &&
                                command.Entity != "login" && command.Entity != "signup" 
                                && command.Entity != "customers" && command.Entity != "user")
                                return
                                    Encoding.ASCII.GetBytes(
                                        JsonConvert.SerializeObject(new Response()
                                        {
                                            Result = "SESSION_TIMEOUT",
                                            RequestID = args.RequestID
                                        }));
                            //try to execute the command
                            Log.Instance.Write("Command: {0}, Entity: {1}, Json: {2}",this, Log.LogSeverity.DEBUG,command.Command,command.Entity,command.JSONObjectArgs);
                            response = command.Execute();
                            //checks if the response is ok
                            var responseType = response as IDictionary<string, object>;
                            if (responseType != null)
                            {
                                if (response.Result == "OK")
                                {
                                    //then send to CFR validation (checks if this is a CFR action and need to save it)
                                    var serialNumber = 0;
                                    var reason = string.Empty;
                                    //checks if have serial number to save the device id as well

                                    if (responseType.ContainsKey("SerialNumber"))
                                        serialNumber = Convert.ToInt32(response.SerialNumber.ToString());

                                    //checks if have reason 
                                    if (responseType.ContainsKey("Reason"))
                                        reason = response.Reason.ToString();

                                    DataManager.Instance.UserCFRCheck(Convert.ToInt32(args.UserID.ToString()),
                                        args.Command, args.Entity, serialNumber, reason);
                                    //if license has cfr enabled then
                                    //var licDef = licenseCheck.ReadLicense(licenseFullPath);
                                    //if (licDef != null && licDef.UseCFR)
                                    //{
                                    //execute the CFR audit trail
                                    //}
                                }
                            }
                            else
                            {
                                Type type = response.GetType();
                                if (type.IsArray) //assuming it is a byte[] need to return it back as is
                                    return response;
                            }
                        }
                        // Remove IsOK attribute as redundant
                        if (response is IDictionary<string, object>)
                            (response as IDictionary<string, object>).Remove("IsOK");

                        json = JsonConvert.SerializeObject(response);
                       
                    }
                    else
                    {
                        Log.Instance.Write("Command '{0} {1}' is NOT allowed", this,
                            Log.LogSeverity.DEBUG, args.Command, args.Entity);
                        return
                            Encoding.ASCII.GetBytes(
                                JsonConvert.SerializeObject(new Response()
                                {
                                    Result = "PERMISSION_DENIED",
                                    RequestID = args.RequestID,
                                    Command = args.Command,
                                    Entity = args.Entity
                                }));
                    }
                }
                return Encoding.ASCII.GetBytes(json);
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in execute", this, ex);
            }
            return Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(response));
        }

        private static bool IsCommandAllowed(UserCommand cmd)
        {
            if (cmd.Command.ToLower()=="validate" && cmd.Entity.ToLower() == "login")
                return true;
            return DataManager.Instance.isCommandAllowed(cmd);
        }

        private static IEnumerable<ICommand> GetCommands()
        {
            return new List<ICommand>
            {
                new Customers() 
                { 
                    Entity="customers"
                },
                new Users()
                {
                    Entity="users"
                },
                new Users()
                {
                    Entity="user"
                },
                new RegisterLanRcv()
                {
                    Entity ="Registerlanrcv"
                },
                new ValidateLogin()
                {
                    Entity ="login"
                },
                new GroupPermissions()
                {
                    Entity ="grouppermissions"
                },
                new GetLanguages()
                {
                    Entity ="languages"
                },
                new SecureQuestions()
                {
                    Entity ="securedQuestions"
                },
                new Categories()
                {
                    Entity ="categories"
                },
                new Contacts()
                {
                    Entity = "contacts"
                },
                new Sites()
                {
                    Entity = "sites"
                },
                new Devices()
                {
                    Entity = "devices"
                },
                new Device()
                {
                    Entity = "device"
                },
                new DBMaintain()
                {
                    Entity = "dbmaintain"
                },
                new SMS()
                {
                    Entity = "sms"
                },
                new Alarm()
                {
                    Entity = "alarms"
                },
                new Analytics()
                {
                    Entity = "analytics"
                },
                new Calibration()
                {
                    Entity = "calibration"
                },
                new Reports()
                {
                    Entity = "reports"
                },
                 new General()
                {
                    Entity = "general"
                },
                 new Users
                 {
                    Entity = "signup"
                },
            };
        }
    }
}
