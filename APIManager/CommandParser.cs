﻿using Server.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIManager
{
    class CommandParser
    {
        private readonly IEnumerable<ICommand> _commands;
        ICommandFactory _generic;

        public CommandParser(IEnumerable<ICommand> commands, ICommandFactory generic)
        {
            _commands = commands;
            _generic = generic;
        }

        internal ICommand Parse(CommandObject args)
        {
            string entity = args.Entity;
            //assuming the args are: requestid,userid,command,entity,data
            ICommand command = Find(entity);
            if (command == null)
                return _generic.Create(args);
            return ((ICommandFactory)command).Create(args);
        }

        private ICommand Find(string entity)
        {
            return _commands
                .FirstOrDefault(c => c.Entity == entity);
        }
    }
}
