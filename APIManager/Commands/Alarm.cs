﻿using Infrastructure;
using Newtonsoft.Json;
using Server.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DeviceLayer;
using Log4Tech;

namespace APIManager.Commands
{
    class Alarm : BaseResponse, ICommandFactory
    {
        #region ICommand

        public override dynamic Execute()
        {
            dynamic result = new Response() { Result = "Error", RequestID = this.RequestID };
            try
            {
                //checks which method to execute
                CFRActions.eMethods command = (CFRActions.eMethods)Enum.Parse(typeof(Infrastructure.CFRActions.eMethods), this.Command.ToLower());
                dynamic alarm = null, temp = null;
                switch (command)
                {
                    case CFRActions.eMethods.update: // Not used - call setreason instead
                        Log4Tech.Log.Instance.Write("call update alarm reason: {0}", this,
                            Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
                        alarm = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                        DataManager.Instance.UpdateAlarmReason(0, Convert.ToInt32(alarm.SerialNumber.ToString()),
                            Convert.ToInt32(alarm.AlarmTypeID.ToString()), alarm.Reason.ToString(),
                            Convert.ToInt32(alarm.SensorAlarmID.ToString()));
                        break;

                    case CFRActions.eMethods.hide:
                        Log.Instance.Write("call hide alarm : {0}", this, Log4Tech.Log.LogSeverity.DEBUG,
                            JSONObjectArgs);
                        alarm = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                        //DataManager.Instance.HideAlarm(alarm.SerialNumber.ToString(), Convert.ToInt32(alarm.AlarmTypeID.ToString()), Convert.ToInt32(alarm.SensorAlarmID.ToString()));

                        int serial_number =
                            DataManager.Instance.DeviceSerialNumberByAlarmID(Convert.ToInt32(alarm.AlarmID.ToString()));

                        DataManager.Instance.HideAlarmByID(Convert.ToInt32(alarm.AlarmID.ToString()));
                        result = DataManager.Instance.GetAlarmNotifications(Convert.ToInt32(UserID.ToString()));

//                        //dynamic notification = new System.Dynamic.ExpandoObject();
//                        PushGUI.Instance.SendAlarm(null);

                        if (serial_number != 0)
                            PushGUIManager.SendDevice(serial_number, PushGUIManager.GUIEvents.deviceChanged);
                        break;

                    case CFRActions.eMethods.hideall:
                        Log.Instance.Write("call hide all alarms : {0}", this, Log.LogSeverity.DEBUG, JSONObjectArgs);

                        var serialNumbers = DataManager.Instance.AllAlarmedDevicesSerialNumbers(Convert.ToInt32(UserID));

                        DataManager.Instance.HideAllAlarms(Convert.ToInt32(UserID));
                        result = DataManager.Instance.GetAlarmNotifications(Convert.ToInt32(UserID));
                        PushGUIManager.SendAlarm(null);

                        foreach (var sn in serialNumbers)
                            PushGUIManager.SendDevice(sn, PushGUIManager.GUIEvents.deviceChanged);
                        break;

                    case CFRActions.eMethods.get:
                        Log.Instance.Write("call get all alarms : {0}", this, Log.LogSeverity.DEBUG,
                            JSONObjectArgs);
                        result = DataManager.Instance.GetAlarmNotifications(Convert.ToInt32(UserID));
                        break;

                    case CFRActions.eMethods.count:
                        Log.Instance.Write("call get alarms count: {0}", this, Log4Tech.Log.LogSeverity.DEBUG,
                            JSONObjectArgs);
                        result = new System.Dynamic.ExpandoObject();
                        temp = DataManager.Instance.GetAlarmNotifications(Convert.ToInt32(UserID.ToString()));
                        if (temp != null && temp.Alarms != null && temp.Alarms.Count > 0)
                            result.Count = temp.Alarms.Count;
                        else
                            result.Count = 0;
                        break;

                    case CFRActions.eMethods.setreason:
                        Log.Instance.Write("call update alarm reason: {0}", this, Log4Tech.Log.LogSeverity.DEBUG,
                            JSONObjectArgs);
                        alarm = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                        DataManager.Instance.UpdateAlarmReason(Convert.ToInt32(alarm.AlarmId.ToString()), 0, 0,
                            alarm.AlarmReason.ToString(), 0);
                        break;
                    case CFRActions.eMethods.lastsample:
                        return getLastSampleUTC();
                    case CFRActions.eMethods.isonline:
                        return getOnlineStatus();
                    case CFRActions.eMethods.getsites:
                        return getSites();
                    case CFRActions.eMethods.getdevices:
                        return getDevices();
                    case CFRActions.eMethods.updatedevicenotifications:
                        return updateDeviceNotifications();
                    case CFRActions.eMethods.getcalibrationexpiryreminder:
                        return getCalibrationExpiryReminder();
                    case CFRActions.eMethods.updatecalibrationexpiryreminder:
                        return UpdateCalibrationExpiryReminder();

                }
                if (alarm != null)
                    if (alarm.SerialNumber != null)
                        result.SerialNumber = alarm.SerialNumber;
                
                result.RequestID = RequestID;
                result.Command = Command;
                result.Entity = Entity;
                result.Result = "OK";
            }
            catch (Exception ex)
            {
                result.Result = "Error";
                Log.Instance.WriteError("Fail in Alarm Execute", this, ex);
            }

            return result;
        }
        #endregion

        #region private methods

        private dynamic UpdateCalibrationExpiryReminder()
        {
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                 var reminder = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                DataManager.Instance.UpdateCalibrationExpiryReminder(Convert.ToInt32(UserID), reminder);
                //result.Data = response.Data;
                result.Result = "OK";
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in updateDeviceNotifications", this, ex);
            }
            return result;
        }

        private dynamic getCalibrationExpiryReminder()
        {
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                dynamic response = DataManager.Instance.GetCalibrationExpiryReminder(Convert.ToInt32(UserID));
                result.Data = response.Data;
                result.Result = "OK";
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in updateDeviceNotifications", this, ex);
            }
            return result;
        }

        private dynamic updateDeviceNotifications()
        {
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var notifications = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);

                DataManager.Instance.UpdateSensorAlarmNotification(notifications);
                result.Result = "OK";
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in updateDeviceNotifications", this, ex);
            }
            return result;
        }

        private dynamic getDevices()
        {
            dynamic result = initResponse();
            result.Result = "Error";
            result.Data = new List<dynamic>();
            try
            {
                var contactData = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                dynamic response = DataManager.Instance.GetDevicesByUser(Convert.ToInt32(UserID));
                var alarmNotificationData =
                    DataManager.Instance.GetDeviceAlarmNotifications(Convert.ToInt32(UserID));
                //go over the device alarm list
                foreach (var alarm in alarmNotificationData.Data)
                {
                    //go over the devices
                    foreach (var device in response.Data)
                    {
                        if (device.ID.ToString() == alarm.DeviceID.ToString())
                        {
                            //add flag of notification    
                            //mark 0 or 1 according to the data of alarmNotificationData
                            var recieveEmail = Convert.ToInt32(alarm.ReceiveEmail.ToString());
                            var recieveSms = Convert.ToInt32(alarm.ReceiveSMS.ToString());
                            device.NotificationBy = recieveEmail == 1 && recieveSms == 1
                                ? "EMAIL_SMS"
                                : recieveEmail == 1
                                    ? "EMAIL"
                                    : recieveSms == 1
                                        ? "SMS"
                                        : "OFF";
                            device.NotificationsIsOn = 1;
                            foreach (var sensor in device.Sensors)
                            {
                                if (sensor.ID.ToString() == alarm.DeviceSensorID.ToString())
                                {
                                    sensor.NotificationsIsOn = 1;
                                    break;
                                }
                            }
                        }
                        //result.Data.Add(device);
                    }
                }
                result.Data = response.Data;
                result.Result = "OK";
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in getDevices", this, ex);
            }
            return result;
        }

        private dynamic getSites()
        {
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                dynamic response = DataManager.Instance.GetUserSites(Convert.ToInt32(UserID));
                result.Data = response.Data;
                result.Result = "OK";
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in getSites", this, ex);
            }
            return result;
        }

        private dynamic getOnlineStatus()
        {
            Log.Instance.Write("call getOnlineStatus", this);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {

                result.Status = Common.DeviceApiManager.ConnectedDevices(Convert.ToInt32(UserID)) > 0;
                result.Result = "OK";
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in getOnlineStatus", this, ex);
            }
            return result;
        }

        private dynamic getLastSampleUTC()
        {
            dynamic result = initResponse();
            result.Result = "Error";
            result.RequestID = RequestID;
            try
            {
                Log.Instance.Write("call lastsample from all loggers for user {0}", this, Log.LogSeverity.INFO, UserID);
                var userId = Convert.ToInt32(UserID);
                int customerId;
                //var devices = DataManager.Instance.GetDevicesByUser(UserID);
                if (Common.User2Customer.TryGetValue(userId, out customerId))
                {
                    var datetime = SampleManager.Instance.CustomerLastSample(customerId);
                    result.lastSample = null;
                    if(!datetime.Equals(DateTime.MinValue))
                        result.lastSample = (datetime.Subtract(new DateTime(1970, 1, 1, 0, 0, 0)).TotalMilliseconds);
                    result.Result = "OK";
                }
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in getLastSampleUTC", this, ex);
            }
            return result;
        }
        #endregion

        #region ICommandFactory
        public ICommand Create(CommandObject args)
        {
            return new Alarm
            {
                RequestID = args.RequestID,
                UserID = args.UserID,
                Command = args.Command,
                Entity = args.Entity,
                JSONObjectArgs = args.Data.Replace(@"\", @"\\"),
            };
        }
        #endregion
    }
}
