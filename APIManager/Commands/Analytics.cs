﻿using System;
using System.IO;
using System.Dynamic;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Base.Sensors.Types;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Infrastructure;
using Log4Tech;
using Server.Infrastructure;
using Newtonsoft.Json;


namespace APIManager.Commands
{
    internal class Analytics : BaseResponse, ICommandFactory
    {

        private int _userId;
        private const int MaxSamples = 1000;
        private dynamic _args = new ExpandoObject();

        public override dynamic Execute()
        {
            _userId = Convert.ToInt32(UserID);
            dynamic result = new Response {Result = "Error", RequestID = this.RequestID};
            try
            {
                _args = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);

                //checks which method to execute
                var command = (CFRActions.eMethods) Enum.Parse(typeof (CFRActions.eMethods), Command.ToLower());
                switch (command)
                {
                    case CFRActions.eMethods.getsensors:
                        Log.Instance.Write("Analytics - get all sensors: {0}", this,
                            Log.LogSeverity.DEBUG, JSONObjectArgs);
                        result = DataManager.Instance.GetAnalyticsSensors(_userId);
                        break;

                    case CFRActions.eMethods.addsensor:
                        Log.Instance.Write("Analytics - add sensor: {0}", this, Log.LogSeverity.DEBUG,
                            JSONObjectArgs);
                        return updateAnalyticsSensors();

                    case CFRActions.eMethods.updatesensor:
                        Log.Instance.Write("Analytics - update sensor: {0}", this, Log.LogSeverity.DEBUG,
                            JSONObjectArgs);
                        return updateAnalyticsSensors(true);

                    case CFRActions.eMethods.removesensor:
                        Log.Instance.Write("Analytics - remove sensor: {0}", this,
                            Log.LogSeverity.DEBUG, JSONObjectArgs);
                        //args = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                        DataManager.Instance.RemoveAnalyticsSensor(_userId, (int) _args.ID);
                        result = DataManager.Instance.GetAnalyticsSensors(_userId);
                        break;

                    case CFRActions.eMethods.getgraphdata:
                        return GetGraphData();


                    case CFRActions.eMethods.gettabledata:
                        return GetTableData(_args);
                    case CFRActions.eMethods.getstatisticsdata:
                        Log.Instance.Write("Analytics - get statistics data: {0}", this,
                            Log.LogSeverity.DEBUG, JSONObjectArgs);
                        result = GetStatisticsData(_args);
                        break;

                    case CFRActions.eMethods.calculatemkt:
                        Log.Instance.Write("Analytics - Calculate MKT: {0}", this,
                            Log.LogSeverity.DEBUG, JSONObjectArgs);
                        result = CalculateMKT(_args);
                        break;

                    case CFRActions.eMethods.getmktdata:
                        Log.Instance.Write("Analytics - get MKT data: {0}", this,
                            Log.LogSeverity.DEBUG, JSONObjectArgs);
                        result = GetMKTData(_args);
                        break;

                    case CFRActions.eMethods.exportmkt:
                        Log.Instance.Write("Analytics - Export MKT: {0}", this, Log4Tech.Log.LogSeverity.DEBUG,
                            JSONObjectArgs);
                        result = ExportMKT(_args);
                        break;

                    case CFRActions.eMethods.gethistogramdata:
                        return getHistogramData();

                    case CFRActions.eMethods.getdewpointdata:
                        return getDewPointsData();

                    case CFRActions.eMethods.getbins:
                        return getBins();
                }
                result.RequestID = RequestID;
                result.Command = Command;
                result.Entity = Entity;
                result.Result = "OK";
            }
            catch (Exception ex)
            {
                result.Result = "Error";
                Log.Instance.WriteError("Fail in Analytics Execute", this, ex);
            }

            return result;
        }

        #region private methods

        private dynamic getBins()
        {
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var sensor = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                var serialNumber = Convert.ToInt32(sensor.sensor.serialNumber.ToString());
                var sensorData = DataManager.Instance.GetDeviceSensorsID(serialNumber);
                var sensorTypeId = Convert.ToInt32(sensorData[0].SensorTypeID);
                int start, end;
                Common.CheckDateRange(sensor.daterange, out start, out end);
                List<Sample> samples = SampleManager.Instance.FindPart(serialNumber, sensorTypeId, start, end);

//                var listValues = response.ConvertAll(x => (double) x.Value);

                result.Data = new ExpandoObject();
                if (samples.Any())
                {
                    result.Data.firstBin = samples.Min(x => x.Value);
                    result.Data.lastBin = samples.Max(x => x.Value);
                }
                else
                {
                    result.Data.firstBin = null;
                    result.Data.lastBin = null;
                }
                result.Result = "OK";
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in getBins", this, ex);
            }
            return result;
        }

        private dynamic updateAnalyticsSensors(bool updateSensorAction = false)
        {
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                if (_args is Newtonsoft.Json.Linq.JArray)
                {
                    for (var index = 0; index < _args.Count; index++)
                        if (updateSensorAction)
                            DataManager.Instance.AddAnalyticsSensor(_userId, (int) _args[index].ID,
                                Convert.ToBoolean(_args[index].isActive) ? 1 : 0);
                        else
                            DataManager.Instance.AddAnalyticsSensor(_userId, (int) _args[index].ID);
                }
                else if (updateSensorAction)
                    DataManager.Instance.AddAnalyticsSensor(_userId, (int) _args.ID,
                        Convert.ToBoolean(_args.isActive) ? 1 : 0);
                else
                    DataManager.Instance.AddAnalyticsSensor(_userId, (int) _args.ID);

                if (!updateSensorAction)
                {
                    var response = DataManager.Instance.GetAnalyticsSensors(_userId);
                    if (response != null && response.Sensors != null)
                        result.Sensors = response.Sensors;
                }

                result.Result = "OK";
            }
            catch (Exception  ex)
            {
                Log.Instance.WriteError("Fail in updateAnalyticsSensors", this, ex);
            }
            return result;
        }

        private string getSensorTypeID(int typeID)
        {
            switch (typeID)
            {
                case 1:
                    return "Internal NTC";
                case 2:
                    return "DigitalTemperature";
                case 4:
                    return "DewPoint";
                case 7:
                    return "ExternalNTC";
                case 8:
                    return "PT100";
                case 3:
                    return "Humidity";
                case 5:
                    return "Current4_20mA";
                case 6:
                    return "Voltage0_10V";
            }
            return string.Empty;
        }

        private dynamic CalculateMKT(dynamic args)
        {
            dynamic result = new ExpandoObject();
            result.Result = "Error";

            //seletedSensors
            int start, end;
            if (Common.CheckDateRange(args.dateRange, out start, out end) == false)
                return result;

            var Sensors = new List<dynamic>();
            for (var index = 0; index < args.data.Count; index++)
            {
                int serialNumber, sensorTypeID;
                var sensorID = (int) args.data[index].ID;
                if (DataManager.Instance.GetSensorLocatorByID(sensorID, out serialNumber, out sensorTypeID))
                {
                    if (Common.MeasurementNameBySensorTypeID(sensorTypeID) == "Temperature")
                    {
                        var mkt = new MKTCalculation();
                        double low = args.data[index].low;
                        double high = args.data[index].high;

                        mkt.ActivationEnergy = args.data[index].energy;
                        if (SampleManager.Instance.BuildSampleAggregator(serialNumber, sensorTypeID, start, end, mkt))
                        {
                            DataManager.Instance.SaveMKTSensorData(Convert.ToInt32(UserID), sensorID, low, high,
                                mkt.ActivationEnergy);
                            var mktValue = mkt.MKT;
                            dynamic data = DataManager.Instance.ReadMKTSensorData(_userId, sensorID);
                            data.ID = sensorID;
                            data.mkt = mktValue;
                            data.status = mktValue >= low && mktValue <= high;
                            Sensors.Add(data);
                        }
                    }
                }
            }
            result.Data = Sensors;
            result.Result = "OK";
            return result;
        }

        private dynamic ExportMKT(dynamic args)
        {
            dynamic result = new ExpandoObject();
            result.Result = "Error";

            int start, end;
            if (Common.CheckDateRange(args.dateRange, out start, out end) == false)
                return result;

            // Generate text buffer
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);

            // CSV format: 
            // sensorID (???), activation energy, low limit, high limit, MKT value, status(V / X)


            result.Sensors = new List<dynamic>();
            for (int index = 0; index < args.sensors.Count; index++)
            {
                int serialNumber, sensorTypeID;
                int sensorID = (int) args.sensors[index].ID.Value;
                if (DataManager.Instance.GetSensorLocatorByID(sensorID, out serialNumber, out sensorTypeID))
                {
                    if (Common.MeasurementNameBySensorTypeID(sensorTypeID) == "Temperature")
                    {
                        dynamic data = DataManager.Instance.ReadMKTSensorData(args.userID, sensorID);
                        MKTCalculation mkt = new MKTCalculation();
                        double low = data.lowlimit;
                        double high = data.highlimit;

                        mkt.ActivationEnergy = args.sensors[index].activation;
                        if (SampleManager.Instance.BuildSampleAggregator(serialNumber, sensorTypeID, start, end, mkt))
                        {
                            data.mktValue = mkt.MKT;
                            result.Sensors.Add(data);
                        }
                    }
                }
            }
            return result;
        }

        private dynamic GetMKTData(dynamic args)
        {
            dynamic result = new ExpandoObject();
            result.Result = "Error";

            int start, end;
            if (Common.CheckDateRange(args.dateRange, out start, out end) == false)
                return result;

            //result.Sensors = new List<dynamic>();
            var Sensors = new List<dynamic>();
            for (var index = 0; index < args.sensors.Count; index++)
            {
                int serialNumber, sensorTypeID;
                var sensorID = (int) args.sensors[index].ID.Value;

                if (DataManager.Instance.GetSensorLocatorByID(sensorID, out serialNumber, out sensorTypeID))
                {
                    if (Common.MeasurementNameBySensorTypeID(sensorTypeID) == "Temperature")
                    {
                        dynamic data = DataManager.Instance.ReadMKTSensorData(_userId, sensorID);
                        var mkt = new MKTCalculation {ActivationEnergy = data.energy};
                        data.ID = sensorID;
                        data.mkt = 0;
                        if (SampleManager.Instance.BuildSampleAggregator(serialNumber, sensorTypeID, start, end, mkt))
                        {
                            var mktValue = Math.Round( mkt.MKT,3);
                            data.mkt = mktValue;
                            data.status = mktValue >= Convert.ToDouble(data.low.ToString()) &&
                                          mktValue <= Convert.ToDouble(data.high.ToString());
                            Sensors.Add(data);
                        }
                    }
                }
            }
            result.Data = Sensors;
            result.Result = "OK";
            return result;
        }

        private dynamic GetStatisticsData(dynamic args)
        {
            dynamic result = new ExpandoObject();
            result.Result = "Error";

            int start, end;
            if (Common.CheckDateRange(args.dateRange, out start, out end) == false)
                return result;

            result.Data = new dynamic[args.sensors.Count];

            int serialNumber, sensorTypeID;
            for (int index = 0; index < args.sensors.Count; index++)
            {
                dynamic sensor = args.sensors[index];
                int sensorID = (int) sensor.ID.Value;
                if (DataManager.Instance.GetSensorLocatorByID(sensorID, out serialNumber, out sensorTypeID))
                {
                    result.Data[index] = SampleManager.Instance.GetSensorStatistics(serialNumber, sensorTypeID, start,
                        end);
                    result.Data[index].sensor = args.sensors[index].Index;
                }
            }
            return result;
        }

        private dynamic GetTableData(dynamic args)
        {
            Log.Instance.Write("Analytics - get table data: {0}", this,
                Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            const int pageSize = 20;
            try
            {
                int start, end;
                if (Common.CheckDateRange(args.dateRange, out start, out end) == false)
                    return result;
                var pageNumber = Convert.ToInt32(args.page.ToString());

                /*********************************************************/
                Log.Instance.Write("Page #{0} for table data", this, Log.LogSeverity.DEBUG, pageNumber);
                /*********************************************************/

                //if this is the first page then
                if (pageNumber == 1)
                    tableDataToCsv(args);

                var offset = (pageNumber - 1)*pageSize;
                result = buildTableData(result, args, offset, pageSize, start, end);
                result.Result = "OK";
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in GetTableData", this, ex);
            }
            return result;
        }

        private void tableDataToCsv(dynamic args)
        {
            /*********************************************************/
            Log.Instance.Write("Kick-off csv and PDF data export", this);
            /*********************************************************/

            ThreadStart action = () =>
            {
                dynamic result = new ExpandoObject();
                int start, end;
                try
                {
                    Common.CheckDateRange(args.dateRange, out start, out end);
                    //get first all data for csv

                    /*********************************************************/
                    Log.Instance.Write("Getting data for csv and PDF", this);
                    /*********************************************************/

                    result = buildTableData(result, args, 0, -1, start, end);
                    if (result.Data != null)
                    {
                        /*********************************************************/
                        Log.Instance.Write("Start build the csv and PDF", this);
                        /*********************************************************/
                        //pdf header table
                        //****************************
                        var pdfTable = new PdfPTable(args.sensors.Count + 1);

                        var cell =
                            new PdfPCell(
                                new Phrase(string.Format("Analytics Table [{0}{1}]", getPdfHeaderRange(args.dateRange),
                                    start > 0
                                        ? string.Format(": {0}-{1}",
                                            new DateTime(1970, 1, 1, 0, 0, 0).AddSeconds(start).ToString("yyyy-MM-dd"),
                                            new DateTime(1970, 1, 1, 0, 0, 0).AddSeconds(end).ToString("yyyy-MM-dd"))
                                        : "")))
                            {
                                Colspan = args.sensors.Count + 1,
                                HorizontalAlignment = 1, //0=Left, 1=Centre, 2=Right
                                BackgroundColor = BaseColor.GRAY,
                            };

                        pdfTable.AddCell(cell);
                        //****************************

                        var csvData = new StringBuilder();
                        var csvHeader = new StringBuilder();
                        csvHeader.AppendFormat("Date and Time,");
                        pdfTable.AddCell(new PdfPCell(new Phrase("Date and Time"))
                        {
                            BackgroundColor = BaseColor.LIGHT_GRAY,
                        });

                        /*********************************************************/
                        Log.Instance.Write("Build the header for csv and PDF", this);
                        /*********************************************************/

                        for (var index = 0; index < args.sensors.Count; index++)
                        {
                            var data = string.Format("[{0}-{1} #{2}]", args.sensors[index].Index.Value,
                                args.sensors[index].Measurement, args.sensors[index].serialNumber);

                            csvHeader.AppendFormat(data);
                            if (index + 1 < args.sensors.Count)
                                csvHeader.AppendFormat(",");

                            data = string.Format("[{0} #{1}]", args.sensors[index].Measurement,
                                args.sensors[index].serialNumber);
                            var headerCell =
                                new PdfPCell(new Phrase(data))
                                {
                                    BackgroundColor = BaseColor.LIGHT_GRAY,
                                };
                            pdfTable.AddCell(headerCell);
                        }
                        csvData.AppendLine(csvHeader.ToString());

                        //                string tempTimeZone;
                        //                Common.User2TimeZone.TryGetValue(Convert.ToInt32(UserID), out tempTimeZone);
                        //                if (tempTimeZone != null)
                        //                {
                        //                    var tz = TimeZoneInfo.FindSystemTimeZoneById(tempTimeZone);

                        /*********************************************************/
                        Log.Instance.Write("Process the data for csv and PDF", this);
                        /*********************************************************/

                        //convert the data to csv and save on local server at - [backup folder]\csv-ready\[userid]\analytics-table
                        foreach (var items in result.Data)
                        {
                            DateTime timestamp =
                                new DateTime(1970, 1, 1, 0, 0, 0).AddSeconds((Convert.ToInt64(items.date) / 1000));
                            //                        var utcOffset = tz.BaseUtcOffset.Hours;
                            //                        if (tz.IsDaylightSavingTime(timestamp))
                            //                            utcOffset = utcOffset + 1;
                            //
                            //                        timestamp = timestamp.AddHours(utcOffset);

                            /*********************************************************/
//                            Log.Instance.Write("Timestamp for the data on csv and PDF - {0}", this, Log.LogSeverity.DEBUG, timestamp);
                            /*********************************************************/

                            int offset;
                            Common.GetUtcOffset(Convert.ToInt32(UserID), timestamp, out timestamp, out offset);

                            //******
                            //pdf cell
                            pdfTable.AddCell(timestamp.ToString("yyyy-MM-dd HH:mm:ss"));
                            //******
                            var builder = new StringBuilder();
                            builder.AppendFormat("{0},", timestamp.ToString("yyyy-MM-dd HH:mm:ss"));
                            foreach (var item in items)
                            {
                                if (item.Key == "date")
                                    break;
                                double value;
                                if (double.TryParse(item.Value.ToString(), out value))
                                {
                                    builder.AppendFormat("{0}", Math.Round(value, 2));
                                    //******
                                    //pdf cell
                                    pdfTable.AddCell(Math.Round(value, 2).ToString(CultureInfo.CurrentCulture));
                                    //******
                                }
                                else
                                {
                                    builder.AppendFormat("");
                                    //******
                                    //pdf cell
                                    pdfTable.AddCell("");
                                    //******
                                }
                                builder.AppendFormat(",");
                            }
                            builder = builder.Remove(builder.Length - 1, 1);
                            csvData.AppendLine(builder.ToString());

                        }
                        //                }

                        /*********************************************************/
                        Log.Instance.Write("Building the path for csv folder", this);
                        /*********************************************************/

                        var path = string.Format(@"{0}\csv-ready\{1}\analytics-table",
                            ConfigurationManager.AppSettings["backupPath"], UserID);
                        buildExportFolder("csv-ready", "analytics-table");
                        var fullPath = string.Format(@"{0}\data.csv", path);
                        var canWrite2File = true;
                        if (File.Exists(fullPath))
                        {
                            try
                            {
                                File.Delete(fullPath);
                            }
                            catch
                            {
                                canWrite2File = false;
                            }
                        }
                        if (canWrite2File)
                        {
                            /*********************************************************/
                            Log.Instance.Write("Writing the data to csv at {0}", this, Log.LogSeverity.DEBUG, fullPath);
                            /*********************************************************/
                            File.WriteAllText(fullPath, csvData.ToString());
                        }

                        /*********************************************************/
                        Log.Instance.Write("Build the path for PDF folder", this);
                        /*********************************************************/

                        //create pdf file 
                        buildExportFolder("pdf-ready", "analytics-table");
                        var pdfPath = string.Format(@"{0}\pdf-ready\{1}\analytics-table\data.pdf",
                            ConfigurationManager.AppSettings["backupPath"], UserID);
                        canWrite2File = true;
                        if (File.Exists(pdfPath))
                        {
                            try
                            {
                                File.Delete(pdfPath);
                            }
                            catch
                            {
                                canWrite2File = false;
                            }
                        }
                        if (canWrite2File)
                        {
                            /*********************************************************/
                            Log.Instance.Write("Writing data to PDF at {0}", this, Log.LogSeverity.DEBUG, pdfPath);
                            /*********************************************************/

                            var pdfDoc = new Document();
                            using (var fileStream = new FileStream(pdfPath, FileMode.Create))
                            {
                                var writer = PdfWriter.GetInstance(pdfDoc, fileStream);
                                pdfDoc.Open();
                                var canvas = writer.DirectContent;
                                pdfTable.TotalWidth = 500f; // must specify a width
                                pdfTable.HorizontalAlignment = 0;
                                //pdfTable.WriteSelectedRows(0, -1, 200, 650, canvas); // -1 means all rows
                                pdfDoc.Add(pdfTable);
                                pdfDoc.Close();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Instance.WriteError("Fail in tableDataToCSV", this, ex);
                }
            };
            var thread = new Thread(action) { IsBackground = true };
            thread.Start();
        }

        private string getPdfHeaderRange(dynamic dateRange)
        {
            switch ((string)dateRange.range)
            {
                case "LAST24H":
                    return "Last 24 Hours";

                case "LAST3D":
                    return "Last 3 Days";

                case "LAST7D":
                    return "Last 7 Days";

                case "LAST30D":
                    return "Last 30 Days";

                case "LAST_YEAR":
                    return "Last Year";

                case "ALL":
                    return "All Time";

                default:
                    return "Dates";
            }
        }

        private void buildExportFolder(string readyFolder, string sourceFolder)
        {
            var backupFolder = ConfigurationManager.AppSettings["backupPath"];

            if (
                !Directory.Exists(string.Format(@"{0}\{1}",
                    backupFolder, readyFolder)))

                Directory.CreateDirectory(string.Format(@"{0}\{1}",
                    backupFolder, readyFolder));
            if (
                !Directory.Exists(string.Format(@"{0}\{1}\{2}",
                    backupFolder, readyFolder, UserID)))
                Directory.CreateDirectory(string.Format(@"{0}\{1}\{2}",
                    backupFolder, readyFolder, UserID));
            if (
                !Directory.Exists(string.Format(@"{0}\{1}\{2}\{3}",
                    backupFolder, readyFolder, UserID, sourceFolder)))
                Directory.CreateDirectory(string.Format(@"{0}\{1}\{2}\{3}",
                    backupFolder, readyFolder, UserID, sourceFolder));
        }

        private dynamic buildTableData(dynamic result,dynamic args, int offset, int pageSize,int start, int end)
        {
            int pos;
            var valueMap = new SortedDictionary<int, double[]>();
            var colDefItems = new ExpandoObject() as IDictionary<string, object>;

            for (var index = 0; index < args.sensors.Count; index++)
            {
                dynamic sensor = args.sensors[index];
                var sensorId = (int)sensor.ID.Value;
                int serialNumber;
                int sensorTypeId;

                if (DataManager.Instance.GetSensorLocatorByID(sensorId, out serialNumber, out sensorTypeId))
                {
                    var samples = SampleManager.Instance.FindPart(serialNumber, sensorTypeId, start, end,
                       offset , pageSize);

                    foreach (var sample in samples)
                    {
                        int time = Convert.ToInt32(sample.SampleTime.ToString());
                        if (!valueMap.ContainsKey(time))
                        {
                            valueMap[time] = new double[args.sensors.Count];
                            for (pos = 0; pos < args.sensors.Count; pos++)
                                valueMap[time][pos] = double.NaN;
                        }
                        valueMap[time][index] = sample.Value;
                    }
                    dynamic def = new ExpandoObject();
                    def.sensorName = args.sensors[index].Measurement.ToString();
                    def.serialNumber = serialNumber;
                    colDefItems.Add(args.sensors[index].Index.Value.ToString(), def);
                }
            }

            result.colDef = colDefItems;

            pos = 0;
            result.Data = new dynamic[valueMap.Count];
            foreach (var key in valueMap.Keys)
            {
                var values = valueMap[key];
                var item = new ExpandoObject() as IDictionary<string, object>;
                for (var sensor_pos = 0; sensor_pos < args.sensors.Count; sensor_pos++)
                {
                    var val = values[sensor_pos];
                    long sensor_index = args.sensors[sensor_pos].Index.Value;
                    if (double.IsNaN(val))
                        item.Add(sensor_index.ToString(), "");
                    else
                        item.Add(sensor_index.ToString(), val);
                }

                result.Data[pos] = item;
                result.Data[pos].date = (ulong)key * 1000;
                pos++;
            }
            return result;
        }

        private dynamic GetGraphData()
        {
            Log.Instance.Write("Analytics - get graph data: {0}", this,
                            Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";

            int start, end;
            if (Common.CheckDateRange(_args.dateRange, out start, out end) == false)
                return result;

            var responseSensors = new List<dynamic>();
            var tasks = new Task[_args.sensors.Count];
            var i = 0;
            foreach (var sensor in _args.sensors)
            {
                var task = Task.Factory.StartNew(() =>
                {
                    var sensorID = (int) sensor.ID.Value;
                    int serialNumber;
                    int sensorTypeID;
                    if (DataManager.Instance.GetSensorLocatorByID(sensorID, out serialNumber, out sensorTypeID))
                        addReducedSensorForGraphData((int)sensor.Index-1, serialNumber, sensorTypeID, start, end, responseSensors);
                });
                tasks[i] = task;
                i++;
            }
            Task.WaitAll(tasks);

            result.Data = new dynamic[responseSensors.Count];
            for (var index = 0; index < responseSensors.Count; index++)
                result.Data[index] = responseSensors[index];
            
            result.Result = "OK";
            
            return result;
        }

        private void addReducedSensorForGraphData(int iColorIndex, int serialNumber, int sensorTypeID, int start, int end, dynamic responseSensors)
        {
            var samples = SampleManager.Instance.FindReduced(serialNumber, sensorTypeID, start, end,
                       MaxSamples);

            var size = samples.Count;
            var mName = Common.MeasurementNameBySensorTypeID(sensorTypeID);
            dynamic sensor_data = new ExpandoObject();

            if (mName == (string)_args.yAxis1)
            {
                sensor_data.name = mName;
                sensor_data.yAxis = 0;
            }
            else if (mName == (string)_args.yAxis2)
            {
                sensor_data.name = mName;
                sensor_data.yAxis = 1;
            }
//            else
//                continue;

            sensor_data.type = "line";
            sensor_data.sensorName = getSensorTypeID(sensorTypeID);
            sensor_data.serialNumber = serialNumber;
            sensor_data.data = new dynamic[size];
            sensor_data.color = Common.DeviceApiManager.GetSensorColor(iColorIndex);
            sensor_data.index = iColorIndex;

            double step = 1;
            if (samples.Count > size)
                step = (double)samples.Count / size;
            for (var index = 0; index < size; index++)
            {
                var pos = Math.Min(Math.Round(step * index), samples.Count);
                sensor_data.data[index] = new ExpandoObject();
                sensor_data.data[index].x = (ulong)samples[(int)pos].Item1 * 1000;
                sensor_data.data[index].y = samples[(int)pos].Item2;
            }
            responseSensors.Add(sensor_data);
        }

        private dynamic getHistogramData()
        {
            Log.Instance.Write("call get Histogram Data - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            result.IsOK = false;
            result.RequestID = RequestID;
            try
            {
                var histogramData = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                //get the serial number of this sensor
                int serialNumber;
                int sensorTypeId;
                //get the sensor info
                DataManager.Instance.GetSensorLocatorByID(Convert.ToInt32(histogramData.selectedSensor.ToString()),
                    out serialNumber,
                    out sensorTypeId);
                //calculate the range for the sensor
                Tuple<int, int> dateRange = histogramData.dateRange.range.ToString().ToUpper() == "CUSTOM"
                    ? new Tuple<int, int>(Convert.ToInt32(histogramData.dateRange.fromDate/1000),
                        Convert.ToInt32(histogramData.dateRange.toDate/1000))
                    : getDateRange(histogramData.dateRange.range.ToString());
                //get the sensor data on the ranage provided
                var samples = SampleManager.Instance.FindPart(serialNumber, sensorTypeId, dateRange.Item1,
                    dateRange.Item2);

                dynamic data = new ExpandoObject();
                data.series = new List<dynamic>();
                data.categories = new List<dynamic>();
                result.Data = data;

                if (samples != null && samples.Count > 0)
                {
                    
                    //build a list of histogram bins based on the data of the bins provided
                    if (histogramData.binsDef != null && histogramData.binsDef.first != null &&
                        histogramData.binsDef.last != null && histogramData.binsDef.width != null)
                    {
                        var first = Convert.ToDouble(histogramData.binsDef.first.ToString());
                        var last = Convert.ToDouble(histogramData.binsDef.last.ToString());
                        
                        //calculate the amount of points in the range of dates
                        var numOfPoints = samples.Count(n => n.Value >= first && n.Value <= last);// samples.Count;
                        
                        var binWidth = Convert.ToDouble(histogramData.binsDef.width.ToString());
                        var current = first;
                        var prev = current;
                        var binList = new List<double>(); //x - categories
                        var histogramList = new List<double>(); //y - series
                        //go over the points starting the first bin
                        while (Math.Round(current,3) <= Math.Round(last,3))
                        {
                            //if it is the first bin then count the amount of occurences of the first bin only
                            if (current == first)
                            {
                                var count = samples.Count(n => n.Value == current);
                                //add the bin value (point) and the amount of points/sum of points overall into the dictionary
                                //binList.Add(new Tuple<double, double>(current, count / (numOfPoints / 1.0)));
                                binList.Add(current);
                                histogramList.Add((count/(numOfPoints/1.0)*100));
                            }
                            //if the result is greater or equal the last bin then
                            else if (current >= last)
                            {
                                //calculate till this point from the previous bin
                                var count = samples.Count(n => n.Value > prev && n.Value <= last);
                                //add the bin value (point) and the amount of points/sum of points overall into the dictionary
                                //binList.Add(new Tuple<double, double>(current, count / (numOfPoints / 1.0)));
                                binList.Add(current);
                                histogramList.Add((count/(numOfPoints/1.0)*100));
                                break;
                            }
                            //else calculate the amount of points between the last bin till the current bin
                            else
                            {
                                var count = samples.Count(n => n.Value > prev && n.Value <= current);
                                //binList.Add(new Tuple<double, double>(current, count / (numOfPoints / 1.0)));
                                binList.Add(current);
                                histogramList.Add((count/(numOfPoints/1.0)*100));
                            }
                            //set the current to prev
                            prev = current;
                            //adds the width to the current bin
                            current += binWidth;
                        }
                        if (histogramList.Count > 1 && binList.Count > 1)
                        {
                            data.series = histogramList;
                            data.categories = binList;
                            result.Data = data;
                            saveHistogramCSV(result);
                            result.Result = "OK";
                        }
                        else
                        {
                            result.Result = "NO_DATA";
                            result.Reason = "No data available";
                        }
                    }
                }
                else
                {
                    result.Result = "NO_DATA";
                    result.Reason = "No data available";
                }
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in getHistogramData", this, ex);
            }
            return result;
        }

        private void saveHistogramCSV(dynamic result)
        {
            Task.Factory.StartNew(() =>
            {
                var csvData = new StringBuilder();
                var csvHeader = new StringBuilder();
                var source = "analytics-histogram";

                csvHeader.AppendFormat("Bin,Value");
                csvData.AppendLine(csvHeader.ToString());
                try
                {
                    for (var index = 0; index < result.Data.series.Count; index++)
                    {
                        var builder = new StringBuilder();
                        double value;
                        if (double.TryParse(result.Data.categories[index].ToString(), out value))
                            builder.AppendFormat("{0},{1}", result.Data.series[index], Math.Round(value, 2));
                        else
                            builder.AppendFormat("{0},{1}", result.Data.series[index], "");

                        csvData.AppendLine(builder.ToString());
                    }
                    var path = string.Format(@"{0}\csv-ready\{1}\{2}", ConfigurationManager.AppSettings["backupPath"],
                        UserID, source);
                    if (
                        !Directory.Exists(string.Format(@"{0}\csv-ready",
                            ConfigurationManager.AppSettings["backupPath"])))
                        Directory.CreateDirectory(string.Format(@"{0}\csv-ready",
                            ConfigurationManager.AppSettings["backupPath"]));
                    if (
                        !Directory.Exists(string.Format(@"{0}\csv-ready\{1}",
                            ConfigurationManager.AppSettings["backupPath"], UserID)))
                        Directory.CreateDirectory(string.Format(@"{0}\csv-ready\{1}",
                            ConfigurationManager.AppSettings["backupPath"], UserID));
                    if (
                        !Directory.Exists(string.Format(@"{0}\csv-ready\{1}\{2}",
                            ConfigurationManager.AppSettings["backupPath"], UserID, source)))
                        Directory.CreateDirectory(string.Format(@"{0}\csv-ready\{1}\{2}",
                            ConfigurationManager.AppSettings["backupPath"], UserID, source));
                    var fullPath = string.Format(@"{0}\data.csv", path);
                    if (File.Exists(fullPath))
                        File.Delete(fullPath);
                    File.WriteAllText(fullPath, csvData.ToString());
                }
                catch (Exception ex)
                {
                    Log.Instance.WriteError("Fail in saveHistogramCSV", this, ex);
                }
            });
        }

        private void saveDewPointsToCSV(dynamic result)
        {
            Task.Factory.StartNew(() =>
            {
                var csvData = new StringBuilder();
                var csvHeader = new StringBuilder();
                var source = "analytics-dewpoint";

                csvHeader.AppendFormat("Date and Time,Temperature,Humidity,Dew Point");
                csvData.AppendLine(csvHeader.ToString());

                try
                {
                    for (var index = 0; index < result.Data[0].data.Length; index++)
                    {
                        var builder = new StringBuilder();
                        var timestamp =
                            new DateTime(1970, 1, 1, 0, 0, 0).AddSeconds((Convert.ToInt64(result.Data[0].data[index].x)/
                                                                          1000)).ToString("yyyy-MM-dd HH:mm:ss");
                        builder.AppendFormat("{0},{1},{2},{3}", timestamp,
                            Math.Round(result.Data[0].data[index].y, 2),
                            Math.Round(result.Data[1].data[index].y, 2),
                            Math.Round(result.Data[2].data[index].y, 2));
                        csvData.AppendLine(builder.ToString());
                    }
                    var path = string.Format(@"{0}\csv-ready\{1}\{2}", ConfigurationManager.AppSettings["backupPath"],
                        UserID, source);
                    if (
                        !Directory.Exists(string.Format(@"{0}\csv-ready",
                            ConfigurationManager.AppSettings["backupPath"])))
                        Directory.CreateDirectory(string.Format(@"{0}\csv-ready",
                            ConfigurationManager.AppSettings["backupPath"]));
                    if (
                        !Directory.Exists(string.Format(@"{0}\csv-ready\{1}",
                            ConfigurationManager.AppSettings["backupPath"], UserID)))
                        Directory.CreateDirectory(string.Format(@"{0}\csv-ready\{1}",
                            ConfigurationManager.AppSettings["backupPath"], UserID));
                    if (
                        !Directory.Exists(string.Format(@"{0}\csv-ready\{1}\{2}",
                            ConfigurationManager.AppSettings["backupPath"], UserID, source)))
                        Directory.CreateDirectory(string.Format(@"{0}\csv-ready\{1}\{2}",
                            ConfigurationManager.AppSettings["backupPath"], UserID, source));
                    var fullPath = string.Format(@"{0}\data.csv", path);
                    if (File.Exists(fullPath))
                        File.Delete(fullPath);
                    File.WriteAllText(fullPath, csvData.ToString());
                }
                catch (Exception ex)
                {
                    Log.Instance.WriteError("Fail in saveDewPointsToCSV", this, ex);
                }
            });
        }

        private dynamic getDewPointsData()
        {
            Log.Instance.Write("call get DewPoints Data - {0}", this, Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            result.RequestID = RequestID;

            try
            {
                var data = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                var dateRange = new Tuple<int, int>(0, 0);
                var serialNumberTemp = 0;
                var serialNumberHum = 0;
                var sensorTypeIdTemp = 0;
                var sensorTypeIdHum = 0;
                //get the sensor info for temperature and humidity
                DataManager.Instance.GetSensorLocatorByID(Convert.ToInt32(data.sensors.temperature.ID.ToString()), out serialNumberTemp,
                    out sensorTypeIdTemp);
                DataManager.Instance.GetSensorLocatorByID(Convert.ToInt32(data.sensors.humidity.ID.ToString()), out serialNumberHum,
                    out sensorTypeIdHum);
              
                var fromDate = data.dateRange.fromDate.ToString().Length > 10
                    ? Convert.ToInt32(data.dateRange.fromDate.ToString().Substring(0,10))
                    : 0;
                var toDate = data.dateRange.toDate.ToString().Length > 10
                    ? Convert.ToInt32(data.dateRange.toDate.ToString().Substring(0, 10))
                    : 0;
                //calculate the range for the sensor
                dateRange = data.dateRange.range.ToString().ToUpper() == "CUSTOM"
                    ? new Tuple<int, int>(fromDate, toDate)
                    : getDateRange(data.dateRange.range.ToString());

                var measurementUnit = "C";
                var device = DataManager.Instance.GetDevice(serialNumberTemp);
                if (device != null && device.Device != null)
                    measurementUnit = device.Device.CelsiusMode == 1 ? "C" : "F";

                var stopWatch = new Stopwatch();
                stopWatch.Start();
                //get the sensor data on the ranage provided for each 
                var temperatureList = new List<Tuple<int,double>>();
                var humidityList = new List<Tuple<int, double>>();

                var task1 = Task.Factory.StartNew(() =>
                {
                    return
                        temperatureList =
                            SampleManager.Instance.FindReduced(serialNumberTemp, sensorTypeIdTemp, dateRange.Item1,
                                dateRange.Item2, MaxSamples);
                });

                var task2 = Task.Factory.StartNew(() =>
                {
                    return
                        humidityList =
                            SampleManager.Instance.FindReduced(serialNumberHum, sensorTypeIdHum, dateRange.Item1,
                                dateRange.Item2, MaxSamples);
                });
                
                Task.WaitAll(task1,task2);


//                stopWatch.Stop();
//                Log.Instance.Write("[Analytics] Get data from Mongo Db took {0} ms. ",this, Log.LogSeverity.DEBUG, stopWatch.ElapsedMilliseconds);
//                stopWatch.Reset();

                var size = temperatureList.Count;
                var responseSensors = new List<dynamic>();

                dynamic temperatureData = new ExpandoObject();
                dynamic rhData = new ExpandoObject();
                dynamic dewpointData = new ExpandoObject();

                temperatureData.name = "Temperature";
                temperatureData.yAxis = 0;
                temperatureData.type = "line";
                temperatureData.serialNumber = serialNumberTemp;
                temperatureData.MeasurementUnit = measurementUnit;
                temperatureData.color = Common.DeviceApiManager.GetSensorColor((int)data.sensors.temperature.Index);
                temperatureData.data = new dynamic[size];
                rhData.name = "Humidity";
                rhData.yAxis = 0;
                rhData.type = "line";
                rhData.serialNumber = serialNumberHum;
                rhData.MeasurementUnit = "RH";
                rhData.color = Common.DeviceApiManager.GetSensorColor((int)data.sensors.humidity.Index);
                rhData.data = new dynamic[size];
                dewpointData.name = "Dew Point";
                dewpointData.yAxis = 0;
                dewpointData.type = "line";
                dewpointData.serialNumber = "NA";
                dewpointData.MeasurementUnit = measurementUnit;
                dewpointData.color = "red";
                dewpointData.data = new dynamic[size];

                var bFoundDewPoint = false;

                //iterate on the temperature list
                for (var index = 0; index < size; index++)
                {
                    var temperature = temperatureList[index];
                    var sampletime = (long)((int)temperature.Item1) * 1000;

                    temperatureData.data[index] = new ExpandoObject();
                    temperatureData.data[index].x = sampletime;
                    temperatureData.data[index].y = temperature.Item2;

                    double dewPoint;
                    //if found on the same time in humidity then 
                    if (humidityList.Exists(x => x.Item1 == temperature.Item1))
                    {
                        var temp = temperature.Item2;
                        var rhSample = (from x in humidityList
                            where x.Item1 == temperature.Item1
                            select x).FirstOrDefault();
                        if (rhSample != null)
                        {
                            var rh = rhSample.Item2;
                            //calculate the dew point 
                            dewPoint = calculateDewPoint(temp, rh);
                            dewpointData.data[index] = new ExpandoObject();
                            dewpointData.data[index].x = sampletime;
                            dewpointData.data[index].y = dewPoint;
                            //add to the humidity list
                            rhData.data[index] = new ExpandoObject();
                            rhData.data[index].x = sampletime;
                            rhData.data[index].y = rh;
                        }
                        continue;
                    }
                    //if not then 
                    //checks if in humidity list a value lower than current temperature and value higher then current temperature
                    var lowerRh = (from x in humidityList
                        where x.Item1 < temperature.Item1
                        select x).FirstOrDefault();
                    var highRh = (from x in humidityList
                        where x.Item1 > temperature.Item1
                        select x).FirstOrDefault();
                    //if has values then calculate the linear interpoletion 
                    if (lowerRh == null || highRh == null) continue;

                    var interpolatePoint = interpolate(Convert.ToDouble(lowerRh.Item1.ToString()),
                        lowerRh.Item2,
                        Convert.ToDouble(highRh.Item1.ToString()),
                        highRh.Item2,
                        Convert.ToDouble(temperature.Item1.ToString()));
                    //calculate the dew point and add to the list of dew point
                    dewPoint = calculateDewPoint(temperature.Item2, interpolatePoint);

                    bFoundDewPoint = true;

                    dewpointData.data[index] = new ExpandoObject();
                    dewpointData.data[index].x = sampletime;
                    dewpointData.data[index].y = dewPoint;
                    //add to the humidity list
                    rhData.data[index] = new ExpandoObject();
                    rhData.data[index].x = sampletime;
                    rhData.data[index].y = interpolatePoint;
                }

                //if (dewpointData.data != null && ((object[])dewpointData.data).Any())
                if (bFoundDewPoint)
                {
                    responseSensors.Add(temperatureData);
                    responseSensors.Add(rhData);
                    responseSensors.Add(dewpointData);

                    result.Data = new dynamic[responseSensors.Count];
                    for (var index = 0; index < responseSensors.Count; index++)
                        result.Data[index] = responseSensors[index];

                    saveDewPointsToCSV(result);

                    result.Result = "OK";
                }
                else
                {
                    result.Reason = "No matching points found to manipulate dewpoints graph";
                }
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in getDewPointsData", this, ex);
            }
            return result;
        }

        private double interpolate(double x0, double y0, double x1, double y1, double x)
        {
            return y0*(x - x1)/(x0 - x1) + y1*(x - x0)/(x1 - x0);
        }

        private double calculateDewPoint(double temperature, double rh)
        {
            var b = 17.368;
            var c = 238.88;
            if (temperature < 0)
            {
                b = 17.966;
                c = 247.15;
            }
            var alpha = Math.Log(rh/100) + ((b*temperature)/(c + temperature));
            return (c*alpha)/(b - alpha);
        }

        private Tuple<int, int> getDateRange(string range)
        {
            switch (range.ToUpper())
            {
                case "LAST24H":
                    return new Tuple<int, int>(getEpocTime(DateTime.Now.AddHours(-24)), getEpocTime(DateTime.Now));
                case "LAST3D":
                    return new Tuple<int, int>(getEpocTime(DateTime.Now.AddDays(-3)), getEpocTime(DateTime.Now));
                case "Last7D":
                    return new Tuple<int, int>(getEpocTime(DateTime.Now.AddDays(-7)), getEpocTime(DateTime.Now));
                case "LAST30D":
                    return new Tuple<int, int>(getEpocTime(DateTime.Now.AddDays(-30)), getEpocTime(DateTime.Now));
                case "LAST YEAR":
                    return new Tuple<int, int>(getEpocTime(DateTime.Now.AddYears(-1)), getEpocTime(DateTime.Now));
                case "ALL":
                    return new Tuple<int, int>(1, getEpocTime(DateTime.Now));
            }
            return new Tuple<int, int>(0, 0);
        }

        private int getEpocTime(DateTime dt)
        {
            return Convert.ToInt32(dt.Subtract(new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds);
        }

        #endregion

        #region ICommand Factory

        public ICommand Create(CommandObject args)
        {
            return new Analytics
            {
                RequestID = args.RequestID,
                UserID = args.UserID,
                Command = args.Command,
                Entity = args.Entity,
                JSONObjectArgs = args.Data.Replace(@"\", @"\\"),
            };
        }

        #endregion
    }
}
