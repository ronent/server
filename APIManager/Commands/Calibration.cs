﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Base.OpCodes;
using DeviceLayer;
using Infrastructure;
using Log4Tech;
using Newtonsoft.Json;
using Server.Infrastructure;

namespace APIManager.Commands
{
    internal class Calibration : BaseResponse, ICommandFactory
    {

        private static ManualResetEvent _mre = new ManualResetEvent(false);
        private Stream _stream;
        #region ICommand

        public override dynamic Execute()
        {
            dynamic result = new System.Dynamic.ExpandoObject();
            result.Result = "Error";
            result.RequestID = RequestID;
            try
            {
                //checks which method to execute
                var command =
                    (CFRActions.eMethods) Enum.Parse(typeof (CFRActions.eMethods), Command);
                
                switch (command)
                {
                    case CFRActions.eMethods.calibrate:
                        return calibrate(); 
                    case CFRActions.eMethods.savecalibration:
                        return uploadCalibration();
                    case CFRActions.eMethods.loadcalibration:
                        return getTemplate();
                    case CFRActions.eMethods.certificate:
                        return getCertificateFileUrl();
                    case CFRActions.eMethods.getcalibrationlist:
                        return getCalibrationList();
                }
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in Calibration Execute", this, ex);
            }
            return result;
        }

        #endregion

        #region private methods

        private dynamic getCalibrationList()
        {
            Log.Instance.Write("call get calibration template - {0}", this, Log.LogSeverity.DEBUG,
                              JSONObjectArgs);
            dynamic result = initResponse();
            var calibration = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
            result.Result = "Error";
            try
            {
                var customerid = DataManager.Instance.GetCustomerBySensor(Convert.ToInt32(calibration.ID.ToString()));
                var keyPrefix = string.Format("{0}/{1}", customerid, calibration.SensorTypeID);
                result.Data = S3Manager.Instance.GetCalibrationFileList(keyPrefix);
                result.Result = "OK";
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in getCalibrationList", this, ex);
            }
            return result;
        }

        private dynamic getTemplate()
        {
            Log.Instance.Write("call get calibration template - {0}", this, Log.LogSeverity.DEBUG,
                              JSONObjectArgs);
            dynamic result = initResponse();
            var calibration = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
            result.Result = "Error";
            try
            {
                var customerid = DataManager.Instance.GetCustomerBySensor(Convert.ToInt32(calibration.sensor.ID.ToString()));//device sensorid
                var keyPath = string.Format("{0}/{1}/{2}", customerid, calibration.sensor.TypeID, calibration.calibration.Name.ToString());
                var setupTemplate = S3Manager.Instance.GetCalibrationFileContent(keyPath);
                result.Template = setupTemplate;
                result.Result = "OK";
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in getTemplate", this, ex);
            }
            return result;
        }

        private dynamic calibrate()
        {
            Log.Instance.Write("call set calibration - {0}", this, Log.LogSeverity.DEBUG,
                            JSONObjectArgs);
            var calibration = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
            //iterate the pairs array and convert to list of tuple decimal
            var pairs = new List<Tuple<decimal, decimal>>
                        {
                            new Tuple<decimal, decimal>(Convert.ToDecimal(calibration.Calibration.Log1.ToString()),
                                Convert.ToDecimal(calibration.Calibration.Ref1.ToString())),
                            new Tuple<decimal, decimal>(Convert.ToDecimal(calibration.Calibration.Log2.ToString()),
                                Convert.ToDecimal(calibration.Calibration.Ref2.ToString()))
                        };
            if (calibration.Calibration.Log3 != null)
            {
                pairs.Add(
                    new Tuple<decimal, decimal>(Convert.ToDecimal(calibration.Calibration.Log3.ToString()),
                        Convert.ToDecimal(calibration.Calibration.Ref3.ToString())));
            }
            var serialNumber = calibration.SerialNumber.ToString();
            tryStopDevice(serialNumber);

            var calibrationResult = Common.DeviceApiManager.Calibration(calibration.SerialNumber.ToString(),
                Convert.ToInt32(calibration.SensorIndex.ToString()), pairs);
            //audit action
            if (calibrationResult.IsOK)
            {
                DataManager.Instance.SystemActionsAudit(Convert.ToInt32(UserID.ToString()), Command, Entity,
                    Convert.ToInt32(calibration.SerialNumber.ToString()));
            }
            var result = generateResponse(calibrationResult);
            result.SerialNumber = calibration.SerialNumber;
            return result;
        }

        private dynamic uploadCalibration()
        {
            Log.Instance.Write("call calibration upload - {0}", this, Log.LogSeverity.DEBUG,
                           JSONObjectArgs);
            var calibration = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            if (calibration.sensor != null && calibration.calibrationName != null)
            {
                var customerid =
                    DataManager.Instance.GetCustomerBySensor(Convert.ToInt32(calibration.sensor.ID.ToString()));
                    //device sensorid

                var keyPath = string.Format("{0}/{1}/{2}", customerid, calibration.sensor.TypeID,
                    calibration.calibrationName);

                string json = JsonConvert.SerializeObject(calibration);

                return generateResponse(S3Manager.Instance.UploadCalibrationFile(keyPath, json)
                        ? new Result(eResult.OK)
                        : new Result(eResult.ERROR));
            }
            return result;
        }

        private dynamic getCertificateFileUrl()
        {
            Log.Instance.Write("call calibration certificate - {0}", this, Log.LogSeverity.DEBUG,
                           JSONObjectArgs);
            var calibration = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                //send the calibration info to the certificate
                _mre = new ManualResetEvent(false);
                var report = new ReportsManager();
                report.OnFinishReport += Instance_OnFinishReport;
                report.GenerateCertificate(calibration);
                _mre.WaitOne();

                result.certificateUrl = report.CalibrationUrl;
                result.Result = "OK";
            }
            catch
            {
                _mre.Set();
            }
             return result;
        }

        private void tryStopDevice(string serialNumber)
        {
            
            //before start calibration, first stop the device if is running now
//            try
//            {
//                var deviceStatus = Common.DeviceApiManager.Status(serialNumber);
//                if (deviceStatus != null && deviceStatus.Running)
                    Common.DeviceApiManager.Stop(serialNumber);
//            }
//            catch
//            {
                // ignored
//            }
        }

        private void Instance_OnFinishReport()
        {
            _mre.Set();
        }

//        private byte[] getAllBytes()
//        {
//            byte[] result;
//            using (var streamReader = new MemoryStream())
//            {
//                _stream.CopyTo(streamReader);
//                result=streamReader.ToArray();
//            }
//            return result;
//        }

        #endregion

        #region ICommandFactory

        public ICommand Create(CommandObject args)
        {
            return new Calibration
            {
                RequestID = args.RequestID,
                UserID = args.UserID,
                Command = args.Command,
                Entity = args.Entity,
                JSONObjectArgs = args.Data.Replace(@"\", @"\\"),
            };
        }

        #endregion
    
    }
}
