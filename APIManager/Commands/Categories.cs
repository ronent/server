﻿using Infrastructure;
using Newtonsoft.Json;
using Server.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIManager.Commands
{
    class Categories : BaseResponse, ICommandFactory
    {
        #region ICommand

        public override dynamic Execute()
        {
            Log4Tech.Log.Instance.Write("call execute for get categories",this);
            var response = DataManager.Instance.GetCategories();
            response.RequestID = this.RequestID;
            response.Command = this.Command;
            response.Entity = this.Entity;
            return response;
        }
        #endregion

        #region ICommandFactory
        public ICommand Create(CommandObject args)
        {
            return new Categories
            {
                RequestID = args.RequestID,
                UserID = args.UserID,
                Command = args.Command,
                Entity = args.Entity,
                JSONObjectArgs = args.Data,
            };
        }
        #endregion
    }
}
