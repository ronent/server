﻿using Infrastructure;
using Infrastructure.Entities;
using Infrastructure.Responses;
using Newtonsoft.Json;
using Server.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIManager.Commands
{
    class Contacts : BaseResponse, ICommandFactory
    {
        #region ICommand

        public override dynamic Execute()
        {
            dynamic result = new Response() { Result = "Error", RequestID = this.RequestID };
            try
            {
                //checks which method to execute
                CFRActions.eMethods command = (CFRActions.eMethods)Enum.Parse(typeof(Infrastructure.CFRActions.eMethods), this.Command);
                switch (command)
                {
                    case CFRActions.eMethods.get:
                        Log4Tech.Log.Instance.Write("call execute for get contacts",this);

                        SingleContact singleContact = new SingleContact();
                        if (JSONObjectArgs.Length > 0)
                        {
                            singleContact = JsonConvert.DeserializeObject<SingleContact>(JSONObjectArgs);
                        }
                        ContactList contactList = DataManager.Instance.GetContacts(singleContact);
                        contactList.RequestID = this.RequestID;
                        contactList.Command = this.Command;
                        contactList.Entity = this.Entity;
                        result = contactList;
                        break;
                    case CFRActions.eMethods.create:
                        Log4Tech.Log.Instance.Write("call execute for new contact with args: {0}",this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
                        Contact contact = JsonConvert.DeserializeObject<Contact>(JSONObjectArgs);
                        Response response = DataManager.Instance.CreateContact(contact);
                        response.RequestID = this.RequestID;
                        response.Command = this.Command;
                        response.Entity = this.Entity;
                        result = response;
                        break;
                    case CFRActions.eMethods.update:
                        Log4Tech.Log.Instance.Write("call execute for update contact with args: {0}", this, Log4Tech.Log.LogSeverity.DEBUG,  JSONObjectArgs);
                        Contact contactUpdate = JsonConvert.DeserializeObject<Contact>(JSONObjectArgs);
                        Response response2 = DataManager.Instance.UpdateContact(contactUpdate);
                        response2.RequestID = this.RequestID;
                        response2.Command = this.Command;
                        response2.Entity = this.Entity;
                        result = response2;
                        break;
                    case CFRActions.eMethods.deletecontacts:
                        return deleteContacts();
                    case CFRActions.eMethods.alarmnotifications:
                        Log4Tech.Log.Instance.Write("call execute for alarm notification with args: {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
                        dynamic alarmNotifications = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                        dynamic responseAlarm = DataManager.Instance.SaveAlarmNotification(alarmNotifications);
                        responseAlarm.RequestID = this.RequestID;
                        responseAlarm.Command = this.Command;
                        responseAlarm.Entity = this.Entity;
                        result = responseAlarm;
                        break;
                    case CFRActions.eMethods.getallcontacts:
                        return getAllContacts();
                    case CFRActions.eMethods.getgroups:
                        return getGroups();
                    case CFRActions.eMethods.saveuser:
                        return upsertContact();
                    case CFRActions.eMethods.deletegroups:
                        return deleteGroups();
                    case CFRActions.eMethods.savegroup:
                        return saveGroup();

                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in Contacts Execute", this, ex);
            }
            return result;
        }
        #endregion

        #region private methods

        private dynamic saveGroup()
        {
            Log4Tech.Log.Instance.Write("call saveGroup - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var userId = Convert.ToInt32(UserID);
                var groupData = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);

                DataManager.Instance.UpsertGroup(userId, groupData);
                result.Result = "OK";
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in saveGroup", this, ex);
            }
            return result;
        }

        private dynamic deleteGroups()
        {
            Log4Tech.Log.Instance.Write("call deleteGroups - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var userId = Convert.ToInt32(UserID);
                var groupData = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);

                foreach (var group in groupData)
                    DataManager.Instance.DeleteGroup(Convert.ToInt32(group.groupId.ToString()));
                
                var response = DataManager.Instance.GetGroups(userId);
                result.Data = response.Data;
                result.Result = "OK";
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in deleteGroups", this, ex);
            }
            return result;
        }

        private dynamic upsertContact()
        {
            Log4Tech.Log.Instance.Write("call upsertContact - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var userId = Convert.ToInt32(UserID);
                var contactData = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                var response = DataManager.Instance.UpsertContact(userId, contactData);
                if (response.Result.ToString() == "Error")
                    result.Error = response.Error;
                else
                    result.Result = "OK";
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in upsertContact", this, ex);
            }
            return result;
        }

        private dynamic deleteContacts()
        {
            Log4Tech.Log.Instance.Write("call deleteContact - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var userId = Convert.ToInt32(UserID);
                var contactData = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                foreach (var contact in contactData)
                    DataManager.Instance.DeleteContact(Convert.ToInt32(contact.contactID.ToString()));
                
                var response = DataManager.Instance.GetAllContacts(userId);
                result.Data = response.Data;
                result.Result = "OK";
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in deleteContact", this, ex);
            }
            return result;
        }

        private dynamic getGroups()
        {
            Log4Tech.Log.Instance.Write("call getGroups - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var userId = Convert.ToInt32(UserID);
                var response = DataManager.Instance.GetGroups(userId);
                result.Data = response.Data;
                result.Result = "OK";
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in getGroups", this, ex);
            }
            return result;
        }

        private dynamic getAllContacts()
        {
            Log4Tech.Log.Instance.Write("call getAllContacts - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var userId = Convert.ToInt32(UserID);
                var response = DataManager.Instance.GetAllContacts(userId);
                result.Data = response.Data;
                result.Result = "OK";
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in getAllContacts", this, ex);
            }
            return result;
        }
        #endregion

        #region ICommandFactory
        public ICommand Create(CommandObject args)
        {
            return new Contacts
            {
                RequestID = args.RequestID,
                UserID = args.UserID,
                Command = args.Command,
                Entity = args.Entity,
                JSONObjectArgs = args.Data.Replace(@"\", @"\\"),
            };
        }
        #endregion
    }
}
