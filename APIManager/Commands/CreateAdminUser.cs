﻿using Infrastructure;
using Infrastructure.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace APIManager.DataCommands
{
    class CreateAdminUser : ICommand, ICommandFactory
    {

        public string JSONObjectArgs { get; set; }
        public string Method { get; set; }
        public string RequestID { get; set; }

        public string Execute()
        {
            Logger4Net.Debug(string.Format("call execute for new admin user with args: {0}", JSONObjectArgs));
            AdminUser newUser = JsonConvert.DeserializeObject<AdminUser>(JSONObjectArgs);
            var response = DataManager.getInstance().CreateNewAdminUser(newUser);
            response.RequestID = this.RequestID;
            return JsonConvert.SerializeObject(response);
        }

        public ICommand Create(string[] args)
        {
            Logger4Net.Debug("CreateAdminUser class initiate...");
            return new CreateAdminUser
            {
                RequestID = args[0],
                Method = args[1],
                JSONObjectArgs = args[2],
            };
        }
    }
}
