﻿using Infrastructure;
using Infrastructure.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIManager.DataCommands
{
    class CreateContact : ICommand, ICommandFactory
    {
        #region ICommand
        public string JSONObjectArgs { get; set; }
        public string Method { get; set; }
        public string RequestID { get; set; }

        public string Execute()
        {
            Logger4Net.Debug(string.Format("call execute for new contact with args: {0}", JSONObjectArgs));
            NewContact contact = JsonConvert.DeserializeObject<NewContact>(JSONObjectArgs);
            var response = DataManager.getInstance().CreateContact(contact);
            response.RequestID = this.RequestID;
            return JsonConvert.SerializeObject(response);
        }
        #endregion

        #region ICommandFactory
        public ICommand Create(string[] args)
        {
            Logger4Net.Debug("CreateContact class initiate...");
            return new CreateContact
            {
                RequestID = args[0],
                Method = args[1],
                JSONObjectArgs = args[2],
            };
        }
        #endregion
    }
}
