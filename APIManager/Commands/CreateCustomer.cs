﻿using Infrastructure;
using Infrastructure.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace APIManager.DataCommands
{
    public class CreateCustomer : ICommand, ICommandFactory
    {
        #region ICommand
        public string JSONObjectArgs { get; set; }
        public string Method { get; set; }
        public string RequestID { get; set; }

        public string Execute()
        {
            Logger4Net.Debug(string.Format("call execute for new customer with args: {0}", JSONObjectArgs));
            Customer newCustomer = JsonConvert.DeserializeObject<Customer>(JSONObjectArgs);
            var response= DataManager.getInstance().CreateNewCustomer(newCustomer);
            response.RequestID = this.RequestID;
            return JsonConvert.SerializeObject(response);
        }
        #endregion

        #region ICommandFactory
        public ICommand Create(string[] args)
        {
            Logger4Net.Debug("CreateCustomer class initiate...");
            return new CreateCustomer
            {
                RequestID = args[0],
                Method = args[1],
                JSONObjectArgs = args[2],
            };
        }
        #endregion
        
    }
}
