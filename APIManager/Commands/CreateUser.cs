﻿using Infrastructure;
using Infrastructure.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace APIManager.DataCommands
{
    class CreateUser : ICommand, ICommandFactory
    {
        #region ICommand
        public string JSONObjectArgs { get; set; }
        public string Method { get; set; }
        public string RequestID { get; set; }

        public string Execute()
        {
            Logger4Net.Debug(string.Format("call execute for new admin user with args: {0}", JSONObjectArgs));
            RegularUser user = JsonConvert.DeserializeObject<RegularUser>(JSONObjectArgs);
            //first call to the buisness logic validate
            if (BLManager.getInstance().ValidateUser(new UserCFRAction() { UserID = user.AdminUserID, ActionID = Method, Reason = "" }))
            {
                //then call to the appropiate class execute command
                var response = DataManager.getInstance().CreateUser(user);
                response.RequestID = this.RequestID;
                return JsonConvert.SerializeObject(response);
            }
            return string.Empty;
        }
        #endregion

        #region ICommandFactory
        public ICommand Create(string[] args)
        {
            Logger4Net.Debug("CreateUser class initiate...");
            return new CreateUser
            {
                RequestID = args[0],
                Method = args[1],
                JSONObjectArgs = args[2],
            };
        }
        #endregion
    }
}
