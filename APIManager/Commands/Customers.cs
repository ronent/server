﻿using Infrastructure;
using Infrastructure.Entities;
using Newtonsoft.Json;
using Server.Infrastructure;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIManager.Commands
{
    class Customers : BaseResponse, ICommandFactory
    {
        #region ICommand

        public override dynamic Execute()
        {
            dynamic result = new Response() { Result = "Error", RequestID = this.RequestID };
            try
            {
                //checks which method to execute
                CFRActions.eMethods command = (CFRActions.eMethods)Enum.Parse(typeof(Infrastructure.CFRActions.eMethods), this.Command);
                switch (command)
                {
                    case CFRActions.eMethods.create:
                        Log4Tech.Log.Instance.Write("call execute for new customer with args: {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
                        Customer newCustomer = JsonConvert.DeserializeObject<Customer>(JSONObjectArgs);
                        var response = DataManager.Instance.CreateNewCustomer(newCustomer);
                        response.RequestID = this.RequestID;
                        response.Command = this.Command;
                        response.Entity = this.Entity;
                        result = response;
                        break;
                    case CFRActions.eMethods.update:
                        break;
                    case CFRActions.eMethods.delete:
                        break;
                    case CFRActions.eMethods.get:
                        Log4Tech.Log.Instance.Write("call execute for get customer with args: {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
                        dynamic customerEmail = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                        var responseCustomer = DataManager.Instance.GetCustomer(customerEmail);
                        responseCustomer.RequestID = this.RequestID;
                        responseCustomer.Command = this.Command;
                        responseCustomer.Entity = this.Entity;
                        result = responseCustomer;
                        break;
                    case CFRActions.eMethods.signup:
                        return signupCustomer();
                    case CFRActions.eMethods.registercustomer:
                        return registerCustomer();
                    case CFRActions.eMethods.guidisvalid:
                        return guidIsValid();
                }

            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in Customers Execute", this, ex);
            }
            return result;
        }
        #endregion

        #region  private methods

        private dynamic guidIsValid()
        {
            Log4Tech.Log.Instance.Write("call guidIsValid - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            result.Data = new ExpandoObject();
            result.Data.valid = false;
            try
            {
                var guidData = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                var response = DataManager.Instance.CustomerGuidIsValid(guidData.guid.ToString());
                result.Result = "OK";
                if (response.Result == "OK")
                    result.Data.valid = true;
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in guidIsValid", this, ex);
            }
            return result;
        }


        private dynamic registerCustomer()
        {
            Log4Tech.Log.Instance.Write("call registerCustomer - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var customerData = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                var response = DataManager.Instance.CreateNewCustomer(customerData);

                if (response.Result == "OK")
                {
                    var email = response.email;
                    sendAdminEmail(email, customerData.userName.ToString());
                    result.Result = "OK";
                }
                else
                    result.Error = response.Error;
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in registerCustomer", this, ex);
            }
            return result;
        }

        private void sendAdminEmail(string email, string username)
        {
            var emailBody = new StringBuilder();
            emailBody.AppendFormat("Dear Administrator, </br>");
            emailBody.AppendFormat("You have just been granted to access to SiMiO </br>");
            emailBody.AppendFormat("Please login with username : {0}. (the password is what you have provided during registration) </br>",username);
            emailBody.AppendFormat("Regards,</br>");
            emailBody.AppendFormat("The SiMiO Team</br>");

            Common.SendEmail(email, "New Admin user in SiMiO", emailBody.ToString());
        }

        private dynamic signupCustomer()
        {
            Log4Tech.Log.Instance.Write("call signupCustomer - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var customerData = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                var response = DataManager.Instance.SignupCustomer(customerData.userMail.ToString());

                if (response.Result == "OK")
                {
                    sendCustomerInvitation(customerData.userMail.ToString(),response.guid.ToString());
                    result.Result = "OK";
                }
                else
                    result.Error = "Email already exists.";
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in signupCustomer", this, ex);
            }
            return result;
        }

        private void sendCustomerInvitation(string email, string guid)
        {
            var bodyEmail = string.Empty;
            bodyEmail = "<div id=':jw' class='ii gt m13f9e13cebad46e0 adP adO'><div id=':jv' style='overflow: hidden;'>";
            bodyEmail += "<div>";
            bodyEmail += "	<div>";
            bodyEmail += "		<div>";
            bodyEmail += "			<table border='0' cellpadding='0' cellspacing='0' width='100%'>";
            bodyEmail += "				<tbody>";
            bodyEmail += "					<tr>";
            bodyEmail += "						<td width='100%' style='word-wrap:break-word'>";
            bodyEmail += "							<table cellpadding='2' cellspacing='3' border='0' width='100%'>";
            bodyEmail += "								<tbody>";
            bodyEmail += "									<tr>";
            bodyEmail += "										<td align='left' valign='bottom'>";
            bodyEmail += "											<span style='font-weight:bold;font-size:xx-small;font-family:verdana,sans-serif;color:#666'>";
            bodyEmail += "												<b>Fourtec sent this message to ##customeremail##.</b>";
            bodyEmail += "												<br>";
            bodyEmail += "											</span>";
            bodyEmail += "											<span style='font-size:xx-small;font-family:verdana,sans-serif;color:#666'>";
            bodyEmail += "												Your registered name is included to show this message originated from Fourtec. ";
            bodyEmail += "												<a href='http://www.fourtec.com/company/fourtec-quality-policy/' target='_blank'>Learn more</a>.";
            bodyEmail += "											</span>";
            bodyEmail += "										</td>";
            bodyEmail += "									</tr>";
            bodyEmail += "								</tbody>";
            bodyEmail += "							</table>";
            bodyEmail += "						</td>";
            bodyEmail += "					</tr>";
            bodyEmail += "				</tbody>";
            bodyEmail += "			</table>";
            bodyEmail += "		</div>";
            bodyEmail += "	</div>";
            bodyEmail += "	<div>";
            bodyEmail += "		<div>";
            bodyEmail += "			<table border='0' cellpadding='2' cellspacing='3' width='100%'>";
            bodyEmail += "				<tbody>";
            bodyEmail += "					<tr>";
            bodyEmail += "						<td>";
            bodyEmail += "							<font style='font-size:10pt;font-family:arial,sans-serif'>Dear Customer, <br>";
            bodyEmail += "								<br>This email was sent automatically by Fourtec in response to your request to register to our cloud solution ";
            bodyEmail += "								<span class='il'><b>SiMiO</b></span>. ";
            bodyEmail += "								This is done for your protection; only you, the recipient of this email can take the next step in the";
            bodyEmail += "								<span class='il'><b>registration</b></span> process. ";
            bodyEmail += "							</font>";
            bodyEmail += "						</td>";
            bodyEmail += "					</tr>";
            bodyEmail += "					<tr>";
            bodyEmail += "						<td>";
            bodyEmail += "							<font style='font-size:10pt;font-family:arial,sans-serif'>";
            bodyEmail += "								<div>";
            bodyEmail += "									<br>To ";
            bodyEmail += "									<span class='il'>Register</span> ";
            bodyEmail += "									your ";
            bodyEmail += "									<span class='il'>account</span> ";
            bodyEmail += "									by clicking on the following link :<br>";
            bodyEmail += "									<a href='##registerPage##?regID=##token##' target='_blank'>";
            bodyEmail += "										##registerPage##?regID=##token##";
            bodyEmail += "									</a>";
            bodyEmail += "									<br><br>If you did not register to ";
            bodyEmail += "									<span class='il'>SiMiO</span>";
            bodyEmail += "									, please ignore this email.<br>";
            bodyEmail += "								</div>";
            bodyEmail += "							</font>";
            bodyEmail += "						</td>";
            bodyEmail += "					</tr>";
            bodyEmail += "				</tbody>";
            bodyEmail += "			</table>";
            bodyEmail += "		</div>";
            bodyEmail += "	</div>";
            bodyEmail += "	<div>";
            bodyEmail += "		<hr style='min-height:1px'>";
            bodyEmail += "		<table border='0' cellpadding='0' cellspacing='0' width='100%'>";
            bodyEmail += "			<tbody>";
            bodyEmail += "				<tr>";
            bodyEmail += "					<td width='100%'>";
            bodyEmail += "						<font style='font-size:xx-small;font-family:verdana;color:#666'>";
            bodyEmail += "							<a href='http://www.fourtec.com/company/fourtec-quality-policy/' target='_blank'>Learn More</a>";
            bodyEmail += "							to protect yourself from spoof (fake) emails.";
            bodyEmail += "							<br><br>Fourtec sent this email to you at ";
            bodyEmail += "							<a href='mailto:support@fourtec.com' target='_blank'>support@fourtec.com</a> ";
            bodyEmail += "							about your account registered on ";
            bodyEmail += "							<a href='http://www.Fourtec.com' target='_blank'>www.Fourtec.com</a>.";
            bodyEmail += "							<br><br>Fourtec will periodically send you required emails about the site and your transactions. See our ";
            bodyEmail += "							<a href='http://www.fourtec.com/footer/privacy-policy/' target='_blank'>Privacy Policy</a> ";
            bodyEmail += "							and ";
            bodyEmail += "							<a href='http://www.fourtec.com/footer/terms-of-use/' target='_blank'>User Agreement</a> ";
            bodyEmail += "							if you have any questions.<br><br>All Rights Reserved. Designated trademarks and brands are the property of their respective owners.";
            bodyEmail += "						</font>";
            bodyEmail += "					</td>";
            bodyEmail += "				</tr>";
            bodyEmail += "			</tbody>";
            bodyEmail += "		</table>";
            bodyEmail += "	</div>";
            bodyEmail += "</div>";
            bodyEmail += "</div>";
            bodyEmail += "<div class='yj6qo'>";
            bodyEmail += "</div>";
            bodyEmail += "</div>";

            bodyEmail = bodyEmail.Replace("##customeremail##", email);
            bodyEmail = bodyEmail.Replace("##token##", guid);
            bodyEmail = bodyEmail.Replace("##registerPage##", ConfigurationManager.AppSettings["RegisterCustomerURL"]);

            Common.SendEmail(email, "Customer Registration to SiMiO", bodyEmail);
        }

        #endregion

        #region ICommandFactory
        public ICommand Create(CommandObject args)
        {
            return new Customers
            {
                RequestID = args.RequestID,
                UserID = args.UserID,
                Command = args.Command,
                Entity = args.Entity,
                JSONObjectArgs = args.Data.Replace(@"\", @"\\"),
            };
        }
        #endregion
    }
}
