﻿using Infrastructure;
using Newtonsoft.Json;
using Server.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIManager.Commands
{
    class DBMaintain : BaseResponse, ICommandFactory
    { 
        
        #region ICommand

        public override dynamic Execute()
        {
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                //checks which method to execute
                var command = (CFRActions.eMethods)Enum.Parse(typeof(CFRActions.eMethods), Command);

                switch (command)
                {
                    case CFRActions.eMethods.backup:
                        return backupData();
//                    case CFRActions.eMethods.restore:
                        //first restore to a new database scema in case of mongo
                        //if sql database then restore to a table with the date of the backup
//                        Log4Tech.Log.Instance.Write("call restore: {0}", this, Log4Tech.Log.LogSeverity.DEBUG,  JSONObjectArgs);
//                        dynamic db = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
//                        //run the restore
//                        if (db!=null && SampleManager.Instance.Restore())
//                        {
//                            //uppon success send back ok
//                            result.Result = "OK";
//                        }
//                        break;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in DBMaintain Execute", this, ex);
            }
            return result;
        }
        #endregion

        #region private methods

        private dynamic backupData()
        {
            Log4Tech.Log.Instance.Write("call backup data: {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                // db = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                //try to backup the data of loggers
                //Convert.ToBoolean(db.WipeData), Convert.ToInt32(db.StartTime.ToString()), Convert.ToInt32(db.EndTime.ToString())
                int customerId;
                if (Common.User2Customer.TryGetValue(Convert.ToInt32(UserID), out customerId))
                {
                    result.Result = "OK";
                    result.file =  SampleManager.Instance.Backup(customerId);
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in backup data", this, ex);
            }
            return result;
        }
        #endregion

        #region ICommandFactory
        public ICommand Create(CommandObject args)
        {
            return new DBMaintain
            {
                RequestID = args.RequestID,
                UserID = args.UserID,
                Command = args.Command,
                Entity = args.Entity,
                JSONObjectArgs = args.Data,
            };
        }
        #endregion
    }
}
