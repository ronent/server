﻿using Infrastructure;
using Infrastructure.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIManager.DataCommands
{
    class DeleteUser : ICommand, ICommandFactory
    {
        #region ICommand
        public string JSONObjectArgs { get; set; }
        public string Method { get; set; }
        public string RequestID { get; set; }

        public string Execute()
        {
            Logger4Net.Debug(string.Format("call execute for delete User with args: {0}", JSONObjectArgs));
            SingleUser user = JsonConvert.DeserializeObject<SingleUser>(JSONObjectArgs);
            var response = DataManager.getInstance().DeleteUser(user);
            response.RequestID = this.RequestID;
            return JsonConvert.SerializeObject(response);
        }
        #endregion

        #region ICommandFactory
        public ICommand Create(string[] args)
        {
            Logger4Net.Debug("DeleteUser class initiate...");
            return new DeleteUser
            {
                RequestID = args[0],
                Method = args[1],
                JSONObjectArgs = args[2],
            };
        }
        #endregion
    }
}
