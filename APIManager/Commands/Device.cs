﻿using Base.OpCodes;
using Base.Sensors.Types;
using Infrastructure;
using Infrastructure.Entities;
using Newtonsoft.Json;
using Server.Infrastructure;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Devices.Management;
using Newtonsoft.Json.Serialization;

namespace APIManager.Commands
{
    class Device : BaseResponse, ICommandFactory
    {
        #region ICommand

        public override dynamic Execute()
        {
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                //checks which method to execute
                var command = (CFRActions.eMethods) Enum.Parse(typeof (CFRActions.eMethods), Command);
                dynamic device;
                switch (command)
                {
                        #region download

                    case CFRActions.eMethods.download:
                        Log4Tech.Log.Instance.Write("call download: {0}", this, Log4Tech.Log.LogSeverity.DEBUG,
                            JSONObjectArgs);
                        device = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                        var downloadResult = Common.DeviceApiManager.Download(device.SerialNumber.ToString());
                        if (Common.DeviceApiManager.IsDeviceConnected(device.SerialNumber.ToString(),false))
                        {
                            //audit action
                            if (downloadResult.IsOK)
                                DataManager.Instance.SystemActionsAudit(Convert.ToInt32(UserID.ToString()),
                                    Command, Entity, Convert.ToInt32(device.SerialNumber.ToString()));

                            result = generateResponse(downloadResult);
                            result.SerialNumber = device.SerialNumber;
                        }
                        break;

                        #endregion

                        #region downloaddata

                    case CFRActions.eMethods.downloaddata:
                        result = getDownloadData();
                        break;

                        #endregion

                        #region status

                    case CFRActions.eMethods.status:
                        Log4Tech.Log.Instance.Write("call status - {0}", this, Log4Tech.Log.LogSeverity.DEBUG,
                            JSONObjectArgs);
//                        device = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
//                        if (Common.DeviceApiManager.IsDeviceConnected(device.SerialNumber.ToString()))
//                        {
//                            var deviceStatus = Common.DeviceApiManager.Status(device.SerialNumber.ToString());
//                            //audit action
//                            if (deviceStatus.IsOK)
//                            {
//                                DataManager.Instance.SystemActionsAudit(Convert.ToInt32(this.UserID.ToString()),
//                                    Command, Entity, Convert.ToInt32(device.SerialNumber.ToString()));
//                            }
//                            deviceStatus.Result = deviceStatus.IsOK ? "OK" : "Error";
//                            result = deviceStatus;
//                            result.SerialNumber = device.SerialNumber;
//                            result.Result = "OK";
//                        }
//                        else
//                        {
//                            result.Reason = "Device is not connected.";
//                        }
                        break;

                        #endregion

                        #region setup

                    case CFRActions.eMethods.setup:
                        result = sendSetup();
                        break;

                        #endregion

                        #region stop

                    case CFRActions.eMethods.stop:
                        Log4Tech.Log.Instance.Write("call stop - {0}", this, Log4Tech.Log.LogSeverity.DEBUG,
                            JSONObjectArgs);
                        device = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                        if (Common.DeviceApiManager.IsDeviceConnected(device.SerialNumber.ToString(),false))
                        {
                            var stopResult = Common.DeviceApiManager.Stop(device.SerialNumber.ToString());
                            //audit action
                            if (stopResult.IsOK)
                            {
                                DataManager.Instance.SystemActionsAudit(Convert.ToInt32(this.UserID.ToString()),
                                    this.Command, this.Entity, Convert.ToInt32(device.SerialNumber.ToString()));
                            }
                            result = generateResponse(stopResult);
                            result.SerialNumber = device.SerialNumber;
                        }
                        break;

                        #endregion

                        #region run

                    case CFRActions.eMethods.run:
                        Log4Tech.Log.Instance.Write("call run - {0}", this, Log4Tech.Log.LogSeverity.DEBUG,
                            JSONObjectArgs);
                        device = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                        if (Common.DeviceApiManager.IsDeviceConnected(device.SerialNumber.ToString(),false))
                        {
                            Result runResult = Common.DeviceApiManager.Run(device.SerialNumber.ToString());
                            //audit action
                            if (runResult.IsOK)
                            {
                                DataManager.Instance.SystemActionsAudit(Convert.ToInt32(UserID),
                                    Command, Entity, Convert.ToInt32(device.SerialNumber.ToString()));
                            }
                            result = generateResponse(runResult);
                            result.SerialNumber = device.SerialNumber;
                        }
                        break;

                        #endregion

                        #region updatefirmware

                    case CFRActions.eMethods.updatefirmware:
                        Log4Tech.Log.Instance.Write("call update firmware - {0}", this, Log4Tech.Log.LogSeverity.DEBUG,
                            JSONObjectArgs);
                        device = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                        if (Common.DeviceApiManager.IsDeviceConnected(device.SerialNumber.ToString(),false))
                        {
                            Result updateFWResult =
                                Common.DeviceApiManager.UpdateFirmware(device.SerialNumber.ToString());
                            //audit action
                            if (updateFWResult.IsOK)
                            {
                                DataManager.Instance.SystemActionsAudit(Convert.ToInt32(this.UserID.ToString()),
                                    this.Command, this.Entity, Convert.ToInt32(device.SerialNumber.ToString()));
                            }
                            result = generateResponse(updateFWResult);
                            result.SerialNumber = device.SerialNumber;
                        }
                        break;

                        #endregion

                        #region marktimestamp

                    case CFRActions.eMethods.marktimestamp:
                        Log4Tech.Log.Instance.Write("call mark timestamp - {0}", this, Log4Tech.Log.LogSeverity.DEBUG,
                            JSONObjectArgs);
                        device = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                        var timestampResult = Common.DeviceApiManager.MarkTimeStamp(device.SerialNumber.ToString());
                        //audit action
                        if (timestampResult.IsOK)
                        {
                            DataManager.Instance.SystemActionsAudit(Convert.ToInt32(this.UserID.ToString()),
                                this.Command, this.Entity, Convert.ToInt32(device.SerialNumber.ToString()));
                        }
                        result = generateResponse(timestampResult);
                        result.SerialNumber = device.SerialNumber;
                        break;

                        #endregion

                        #region sensors

                    case CFRActions.eMethods.sensors:
                        Log4Tech.Log.Instance.Write("call get sensors - {0}", this, Log4Tech.Log.LogSeverity.DEBUG,
                            JSONObjectArgs);
//                        device = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
//                        List<GenericSensor> sensorsList =
//                            Common.DeviceApiManager.GetSensors(device.SerialNumber.ToString());
//                        List<dynamic> sensors = new List<dynamic>();
//                        var enu = sensorsList.GetEnumerator();
//
//                        while (enu.MoveNext())
//                        {
//                            dynamic sensor = new System.Dynamic.ExpandoObject();
//                            sensor.Name = enu.Current.Name;
//                            sensor.Type = enu.Current.Type.ToString();
//                            sensor.Index = enu.Current.Index;
//                            sensors.Add(sensor);
//                        }
//
//                        dynamic sensorsResponse = generateResponse(new Result(eResult.OK));
//                        sensorsResponse.Sensors = sensors;
//                        result = sensorsResponse;
//                        result.SerialNumber = device.SerialNumber;
                        break;

                        #endregion

                        #region setuptemplate

                    case CFRActions.eMethods.setuptemplate:
                        result = saveSetupTemplate();
                        break;

                        #endregion

                        #region gettemplate

                    case CFRActions.eMethods.loadsetup:
                        result = getTemplate();
                        break;

                        #endregion

                        #region get template files

                    case CFRActions.eMethods.getsetuplist:
                        result = getSetupList();
                        break;

                        #endregion

                        #region snooze

                    case CFRActions.eMethods.snooze:
                        Log4Tech.Log.Instance.Write("call snooze alarm - {0}", this, Log4Tech.Log.LogSeverity.DEBUG,
                            JSONObjectArgs);
                        var snooze = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);

                        //go over the sensors in the JSON
                        if (snooze.Sensors != null)
                        {
                            //for each sensor send silent value
                            foreach (var sensor in snooze.Sensors)
                            {
                                DataManager.Instance.SnoozeAlarm(snooze.SerialNumber.ToString(),
                                    Convert.ToInt32(sensor.ID.ToString()), Convert.ToInt32(sensor.Silent.ToString()));
                            }
                        }
                        result = initResponse();
                        //get the current device from database 
                        var deviceToReturn = DataManager.Instance.GetDevice(Convert.ToInt32(snooze.SerialNumber));
                        result.Device = deviceToReturn.Device;
                        result.Result = "OK";
                        break;

                        #endregion

                        #region devicesite

                    case CFRActions.eMethods.devicesite:
                        Log4Tech.Log.Instance.Write("call device site update - {0}", this,
                            Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
                        var deviceSite = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);

                        //set the site related to this device
                        DataManager.Instance.RelateDeviceToSite(Convert.ToInt32(deviceSite.SerialNumber.ToString()),
                            Convert.ToInt32(deviceSite.SiteID.ToString()));

                        result = initResponse();
                        //get the current device from database 
                        var deviceSiteToReturn = DataManager.Instance.GetDevice(Convert.ToInt32(deviceSite.SerialNumber));
                        result.Device = deviceSiteToReturn.Device;
                        result.Result = "OK";
                        break;

                        #endregion

                        #region reset calibration

                    case CFRActions.eMethods.resetdevicecalibration:
                        Log4Tech.Log.Instance.Write("call reset device calibration - {0}", this,
                            Log4Tech.Log.LogSeverity.DEBUG,
                            JSONObjectArgs);
                        device = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                        if (Common.DeviceApiManager.IsDeviceConnected(device.SerialNumber.ToString(),false))
                        {
                            var resetResult = Common.DeviceApiManager.ResetCalibration(device.SerialNumber.ToString());
                            //audit action
                            if (resetResult.IsOK)
                            {
                                DataManager.Instance.SystemActionsAudit(Convert.ToInt32(this.UserID.ToString()),
                                    this.Command, this.Entity, Convert.ToInt32(device.SerialNumber.ToString()));
                            }
                            result = generateResponse(resetResult);
                            result.SerialNumber = device.SerialNumber;
                        }
                        break;

                        #endregion

                        #region restore factory calibration

                    case CFRActions.eMethods.restoredevicefactorycalibration:

                        return restoreDeviceFactoryCalibration();

                        #endregion

                        #region calibration mode

                    case CFRActions.eMethods.initcalibrationmode:
                        Log4Tech.Log.Instance.Write("call set calibration - {0}", this, Log4Tech.Log.LogSeverity.DEBUG,
                            JSONObjectArgs);
                        var deviceSetup = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                        //save the previous setup of the device

                        //send setup with 1 sample per second and run the device
                        var modeResult = DeviceManager.Instance.EnterDeviceToCalibrationMode(deviceSetup);
                        result = generateResponse(modeResult);
                        break;

                        #endregion

                        #region more sensor data

                    case CFRActions.eMethods.moresensordata:
                        return getMoreSensorData();

                        #endregion

                    case CFRActions.eMethods.getcustomsensors:
                        return getCustomSensors();
                    case CFRActions.eMethods.getdevicefamily:
                        return getDeviceFamily();
                    case CFRActions.eMethods.getsensorunits:
                        return getSensorUnits();
                    case CFRActions.eMethods.getbasesensor:
                        return getBaseSensor();
                    case CFRActions.eMethods.updatecustomsensor:
                        return updateCustomSensor();
                    case CFRActions.eMethods.deletecustomsensors:
                        return deleteCustomSensors();
                }
            }
            catch
                (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in Device Execute", this, ex);
            }

            return result;
        }

        #endregion

        #region ICommandFactory
        public ICommand Create(CommandObject args)
        {
            return new Device
            {
                RequestID = args.RequestID,
                UserID = args.UserID,
                Command = args.Command,
                Entity = args.Entity,
                JSONObjectArgs = args.Data.Replace(@"\", @"\\"),
            };
        }
        #endregion

        #region private methods

        private dynamic restoreDeviceFactoryCalibration()
        {
            Log4Tech.Log.Instance.Write("call restore device factory calibration - {0}", this,
                            Log4Tech.Log.LogSeverity.DEBUG,
                            JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var device = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                if (Common.DeviceApiManager.IsDeviceConnected(device.SerialNumber.ToString(),false))
                {
                    var restoreResult =
                        Common.DeviceApiManager.RestoreCalibration(device.SerialNumber.ToString());
                    //audit action
                    if (restoreResult.IsOK)
                    {
                        DataManager.Instance.SystemActionsAudit(Convert.ToInt32(UserID),
                            Command, Entity, Convert.ToInt32(device.SerialNumber.ToString()));
                    }
                    result = generateResponse(restoreResult);
                    result.SerialNumber = device.SerialNumber;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in restoreDeviceCalibration", this, ex);
            }
            return result;
        }

        private dynamic deleteCustomSensors()
        {
            Log4Tech.Log.Instance.Write("call deleteCustomSensors - {0}", this, Log4Tech.Log.LogSeverity.DEBUG,
                JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var definedSensors = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                foreach (var definedSensor in definedSensors)
                {
                    DataManager.Instance.DeleteDefinedSensor(Convert.ToInt32(definedSensor.sensorId.ToString()));
                }
                return getCustomSensors();
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in deleteCustomSensors", this, ex);
            }
            return result;
        }

        private dynamic updateCustomSensor()
        {
            Log4Tech.Log.Instance.Write("call updateCustomSensor - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var definedSensor = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                DataManager.Instance.UpdateDefinedSensor(Convert.ToInt32(UserID), definedSensor);
                result.Result = "OK";
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in updateCustomSensor", this, ex);
            }
            return result;
        }

        private dynamic getBaseSensor()
        {
            Log4Tech.Log.Instance.Write("call getBaseSensor - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var family = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                var response = DataManager.Instance.GetBaseSensor(Convert.ToInt32(family.familyId.ToString()));
                result.Data = response.Data;
                result.Result = "OK";
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in getBaseSensor", this, ex);
            }
            return result;
        }

        private dynamic getSensorUnits()
        {
            Log4Tech.Log.Instance.Write("call getSensorUnits - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var family = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                var response = DataManager.Instance.GetSensorUnits(Convert.ToInt32(family.familyId.ToString()));
                result.Data = response.Data;
                result.Result = "OK";
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in getSensorUnits", this, ex);
            }
            return result;
        }

        private dynamic getDeviceFamily()
        {
            Log4Tech.Log.Instance.Write("call getDeviceFamily - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var response = DataManager.Instance.GetDeviceFamilyTypes();
                result.Data = response.Data;
                result.Result = "OK";
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in getDeviceFamily", this, ex);
            }
            return result;
        }

        private dynamic getCustomSensors()
        {
            Log4Tech.Log.Instance.Write("call getCustomSensors - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var response = DataManager.Instance.GetDefinedSensors(Convert.ToInt32(UserID));
                result.Data = response.Data;
                result.Result = "OK";
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in getCustomSensors", this, ex);
            }
            return result;
        }

        private dynamic getMoreSensorData()
        {
            Log4Tech.Log.Instance.Write("call get More Sensor Data - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var sensor = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                //un-check all the sensors in analytics sensor table
                DataManager.Instance.RemoveAnalyticsSensors(Convert.ToInt32(UserID));
                //check only the sensor recieved
                DataManager.Instance.AddAnalyticsSensor(Convert.ToInt32(UserID), Convert.ToInt32(sensor.ID.ToString()));
                result.Result = "OK";
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in getMoreSensorData", this, ex);
            }
            return result;
        }

        private dynamic getDownloadData()
        {
            Log4Tech.Log.Instance.Write("call download data - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            result.IsOK = false;
            result.RequestID = RequestID;
            try
            {
                var device = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                Log4Tech.Log.Instance.Write("Device serial number {0} to get the device sensors", this, Log4Tech.Log.LogSeverity.DEBUG, device.SerialNumber);
                var serialNumber = Convert.ToInt32(device.SerialNumber.ToString());
                //var downloadResult = DeviceManager.Instance.Download(device.SerialNumber.ToString());
                        
                //if (downloadResult.IsOK)
                {

                    dynamic downloadedData = new System.Dynamic.ExpandoObject();
                    List<dynamic> sensorArray = new List<dynamic>();
                    //get the device sensors
                    List<dynamic> sensorList = DataManager.Instance.GetDeviceSensorsID(serialNumber);
                    //get enumerator from the returning sensors list
                    var sensorEnu = sensorList.GetEnumerator();
                    //iterate the sensors
                    while (sensorEnu.MoveNext())
                    {
                        dynamic sensor = new ExpandoObject();
                        sensor.DeviceSensorID = Convert.ToInt32(sensorEnu.Current.ID);
                      
                        // var status = Common.DeviceApiManager.Status(device.SerialNumber.ToString());

                        sensor.data = SampleManager.Instance.Find(serialNumber,
                            Convert.ToInt32(sensorEnu.Current.SensorTypeID), false);

                        var normalized_data = new dynamic[sensor.data.Count];
                        for (var index = 0; index < sensor.data.Count; index++)
                            normalized_data[index] = sensor.data[index].ToDynamic();
                        sensor.data = normalized_data;

                        Log4Tech.Log.Instance.Write("samples for sensor {0} return - {1}", this,
                            Log4Tech.Log.LogSeverity.DEBUG, sensorEnu.Current.SensorTypeID, sensor.data.Length);
                        sensorArray.Add(sensor);
                    }

                    downloadedData.SensorsLogs = sensorArray;
                    downloadedData.RequestID = this.RequestID;
                    downloadedData.Command = this.Command;
                    downloadedData.Entity = this.Entity;
                    result = downloadedData;
                }
                result.SerialNumber = device.SerialNumber;
                result.Result = "OK";
                result.IsOK = true;
            }
            catch(Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in getDownloadData", this, ex);
            }
            return result;
        }

        private dynamic getSetupList()
        {
            Log4Tech.Log.Instance.Write("call get Setup List - {0}", this, Log4Tech.Log.LogSeverity.DEBUG,
               JSONObjectArgs);

            dynamic result = initResponse();
            var setup = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
            result.Result = "Error";
            result.Data = new List<dynamic>();
            try
            {
                //int customerid;
               // Common.SerialNumber2Customer.TryGetValue(Convert.ToInt32(setup.SerialNumber), out customerid);
                //var keyPrefix = string.Format("{0}/{1}", customerid, setup.DeviceType);
                var response = DataManager.Instance.GetDeviceSetupFiles(Convert.ToInt32(UserID));  //S3Manager.Instance.GetSetupFileList(keyPrefix);
                if (response.Data != null)
                {
                    foreach (var setupList in response.Data)
                    {
                        dynamic setupObj = new ExpandoObject();
                        setupObj.ID = setupList.id;
                        setupObj.Name = setupList.name;
                        result.Data.Add(setupObj);
                    }
                    result.Result = "OK";
                }
               
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in getSetupList", this, ex);
            }
            return result;
        }

        private dynamic getTemplate()
        {
            Log4Tech.Log.Instance.Write("call get template - {0}", this, Log4Tech.Log.LogSeverity.DEBUG,
                JSONObjectArgs);

            dynamic result = initResponse();
            var template = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
            result.Result = "Error";
            try
            {
                if (template.SerialNumber != null && template.setup!=null)
                {
                    //get the device from database
                    var serialNumber = Convert.ToInt32(template.SerialNumber.ToString());
                    var device = DataManager.Instance.GetDevice(serialNumber);
                    int customerid;
                    Common.SerialNumber2Customer.TryGetValue(Convert.ToInt32(device.Device.SerialNumber), out customerid);
                    var keyPath = string.Format("{0}/{1}/{2}", customerid, device.Device.DeviceType,template.setup.Name.ToString());
                    var setupTemplate = S3Manager.Instance.GetSetupFileContent(keyPath);
                    if (setupTemplate == null)
                    {
                        DataManager.Instance.DeleteSetupFile(Convert.ToInt32(UserID), template.setup.ID.ToString());
                        result.Error = "File not exists anymore, please choose another one.";
                    }
                    else
                    {
                        result.Data = setupTemplate;
                        result.Result = "OK";    
                    }
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in getTemplate", this, ex);
            }
            return result;
        }

        private dynamic saveSetupTemplate()
        {
            Log4Tech.Log.Instance.Write("call save setup template - {0}", this, Log4Tech.Log.LogSeverity.DEBUG,
                JSONObjectArgs);
            dynamic result = initResponse();
            var setup = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
            result.Result = "Error";
            try
            {
                //checking if serial number and template name was transfered
                if (setup.SerialNumber != null && setup.setupName != null)
                {
                    //send to S3 the current config in the following path format:
                    //[device type]/[firmware version]/[template name]
                    //try to get the device by his serial number
                    var serialNumber = Convert.ToInt32(setup.SerialNumber.ToString());
                    var device = DataManager.Instance.GetDevice(serialNumber);
                    if (device != null)
                    {
                        int customerid;
                        Common.SerialNumber2Customer.TryGetValue(Convert.ToInt32(device.Device.SerialNumber), out customerid);

                        //build the key path based on the device data and according the format mentioned above
                        var keyPath = string.Format("{0}/{1}/{2}", customerid, device.Device.DeviceType,setup.setupName);
                        //checks if the S3 save was ok
                        string json = JsonConvert.SerializeObject(device);
                        string responseSetupFile = S3Manager.Instance.UploadSetupFile(keyPath, json);
                        if (responseSetupFile.Length > 0)
                        {
                            result = generateResponse(new Result(eResult.OK));
                            //add device setup file to database
                            dynamic setupFile = new ExpandoObject();
                            setupFile.UserID = Convert.ToInt32(UserID);
                            setupFile.DeviceType = device.Device.DeviceType;
                            setupFile.SetupID = string.Format("{0}_{1}", device.Device.PartNumber, Convert.ToInt32(DateTime.Now.Subtract(new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds));
                            setupFile.SetupName = setup.setupName;
                            DataManager.Instance.AddDeviceSetupFile(setupFile);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in saveSetupTemplate", this, ex);
            }
            return result;
        }

        private dynamic sendSetup()
        {
            Log4Tech.Log.Instance.Write("call setup - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic resultDevice = initResponse();
            resultDevice.Result = "Error";
            try
            {
                dynamic setup = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);

                if (Common.DeviceApiManager.IsDeviceConnected(setup.SerialNumber.ToString(),false))
                {
                    dynamic setupResult = new ExpandoObject();
                    setupResult.IsOK = true;
                    resultDevice.Device = new ExpandoObject();
                    dynamic returnedDevice = new ExpandoObject();
                    returnedDevice.Device = new ExpandoObject();
                    int serialNumber = Convert.ToInt32(setup.SerialNumber.ToString());
                    //checks if needs to stop the logger before sending setup
                    if (setup.LoggerActivation != null && setup.LoggerActivation.ToString() == "2") //stop
                    {
                        setupResult = Common.DeviceApiManager.Stop(serialNumber.ToString());
                        auditAction(serialNumber);
                    }
                    else
                    {
                        setup.UserID = UserID;
                        //run setup for the logger
                        setupResult = Common.DeviceApiManager.Setup(setup);
                      
                        Log4Tech.Log.Instance.Write("Setup resulted {0}({2}) on device #{1} ", this,
                            Log4Tech.Log.LogSeverity.DEBUG, setupResult.IsOK, serialNumber,
                            setupResult.IsOK
                                ? ""
                                : setupResult.LastException != null
                                    ? setupResult.LastException.Message
                                    : setupResult.MessageLog);
                        //audit action
                        if (setupResult.IsOK)
                        {
                            DataManager.Instance.SystemActionsAudit(Convert.ToInt32(UserID),
                                Command, Entity, Convert.ToInt32(setup.SerialNumber.ToString()));
                                auditAction(serialNumber);
                        }
                        resultDevice.Result = setupResult.IsOK;
                    }
                    resultDevice = generateResponse(setupResult);
                    //get the updated device from database
                    returnedDevice = DataManager.Instance.GetDevice(serialNumber);
                    //return the device
                    resultDevice.Device = returnedDevice.Device;
                    resultDevice.SerialNumber = setup.SerialNumber;
                }
            }
            catch(Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in sendSetup", this, ex);
            }
            return resultDevice;
        }

        private void auditAction(int serialNumber)
        {
            DataManager.Instance.SystemActionsAudit(Convert.ToInt32(UserID), Command, Entity, serialNumber);
        }

        
        #endregion
    }
}
