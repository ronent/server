﻿using Infrastructure;
using Infrastructure.Entities;
using Infrastructure.Responses;
using Newtonsoft.Json;
using Server.Infrastructure;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIManager.Commands
{
    class Devices : BaseResponse, ICommandFactory
    {
        #region ICommand

        public override dynamic Execute()
        {
            dynamic response = new ExpandoObject();
            //checks which method to execute
            dynamic result = new Response() { Result = "Error", RequestID = this.RequestID };
            try
            {
                //checks which method to execute
               // var command = (CFRActions.eMethods)Enum.Parse(typeof(CFRActions.eMethods), Command);

                //get the json parameters
                var jsonParams = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                result = initResponse();
                //checks if the object contains one of the following
                //then filter according to this parameter
                if (jsonParams.CategoryID!=null)
                {
                    Log4Tech.Log.Instance.Write("call execute for get devices related to a category", this);
                    response = DataManager.Instance.GetDevicesByCategory(Convert.ToInt32(jsonParams.CategoryID), Convert.ToInt32(this.UserID));
                    response.Result = "OK";
                }
                else if (jsonParams.SiteID != null)
                {
                    Log4Tech.Log.Instance.Write("call execute for get devices related to a site", this);
                    response = DataManager.Instance.GetDevicesBySite(Convert.ToInt32(jsonParams.SiteID), Convert.ToInt32(this.UserID));
                    response.Result = "OK";
                }
                else if (jsonParams.SerialNumber != null)
                {
                    Log4Tech.Log.Instance.Write("call execute for get a device by serial number", this);
                    response = DataManager.Instance.GetDevice(Convert.ToInt32(jsonParams.SerialNumber));
                    response.Result = "OK";
                }
                else
                {
                    Log4Tech.Log.Instance.Write("call execute for get all devices by userid", this);
                    response = DataManager.Instance.GetDevicesByUser(Convert.ToInt32(UserID));
                    result.Result = "OK";
                    result.Data = response.Data;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in Devices Execute", this, ex);
            }
            return result;
        }
        #endregion

        #region ICommandFactory
        public ICommand Create(CommandObject args)
        {
            return new Devices
            {
                RequestID = args.RequestID,
                UserID = args.UserID,
                Command = args.Command,
                Entity = args.Entity,
                JSONObjectArgs = args.Data.Replace(@"\", @"\\"),
            };
        }
        #endregion
    }
}
