﻿using Infrastructure;
using Infrastructure.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIManager.DataCommands
{
    class DevicesByCategory : ICommand, ICommandFactory
    {
        #region ICommand
        public string JSONObjectArgs { get; set; }
        public string Method { get; set; }
        public string RequestID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>json serialized</returns>
        public string Execute()
        {
            Logger4Net.Debug(string.Format("call execute for get devices related to a category"));
            DeviceByCategory devicebycategory = JsonConvert.DeserializeObject<DeviceByCategory>(JSONObjectArgs);
            var response = DataManager.getInstance().GetDevicesByCategory(devicebycategory);
            response.RequestID = this.RequestID;
            return JsonConvert.SerializeObject(response);
        }
        #endregion

        #region ICommandFactory
        public ICommand Create(string[] args)
        {
            Logger4Net.Debug("GetDevicesByCategory class initiate...");
            return new DevicesByCategory
            {
                RequestID = args[0],
                Method = args[1],
                JSONObjectArgs = args[2],
            };
        }
        #endregion
    }
}
