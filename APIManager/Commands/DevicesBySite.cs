﻿using Infrastructure;
using Infrastructure.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIManager.DataCommands
{
    class DevicesBySite : ICommand, ICommandFactory
    {
        #region ICommand
        public string JSONObjectArgs { get; set; }
        public string Method { get; set; }
        public string RequestID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>json serialized</returns>
        public string Execute()
        {
            Logger4Net.Debug(string.Format("call execute for get devices related to a site"));
            DeviceBySite devicebysite = JsonConvert.DeserializeObject<DeviceBySite>(JSONObjectArgs);
            var response = DataManager.getInstance().GetDevicesBySite(devicebysite);
            response.RequestID = this.RequestID;
            return JsonConvert.SerializeObject(response);
        }
        #endregion

        #region ICommandFactory
        public ICommand Create(string[] args)
        {
            Logger4Net.Debug("GetDevicesBySite class initiate...");
            return new DevicesBySite
            {
                RequestID = args[0],
                Method = args[1],
                JSONObjectArgs = args[2],
            };
        }
        #endregion
    }
}
