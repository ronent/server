﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infrastructure;
using Newtonsoft.Json;
using Server.Infrastructure;

namespace APIManager.Commands
{
    class General : BaseResponse, ICommandFactory
    {
        #region ICommand

        public override dynamic Execute()
        {
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                //checks which method to execute
                var command = (CFRActions.eMethods)Enum.Parse(typeof(CFRActions.eMethods), Command);

                switch (command)
                {
                    case CFRActions.eMethods.storagestatus:
                        return getStorageStatus();
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in General Execute", this, ex);
            }
            return result;
        }
        #endregion

        #region private methods

        private dynamic getStorageStatus()
        {
            Log4Tech.Log.Instance.Write("call getStorageStatus - {0}", this, Log4Tech.Log.LogSeverity.DEBUG,
                JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var userId = Convert.ToInt32(UserID);
               // var response = DataManager.Instance.GetCustomerBalance(userId);
                int customerId;
                long maxStorageAllowed;
                Common.User2Customer.TryGetValue(userId, out customerId);
                Common.MaxStorageAllowed.TryGetValue(customerId, out maxStorageAllowed);

                var total = maxStorageAllowed;
                var used = SampleManager.Instance.RowsCount(customerId);
                result.Data = new ExpandoObject();
                result.Data.total = total;
                result.Data.used = used;
                result.Data.unitsName = "Samples";
                result.Result = "OK";
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in getStorageStatus", this, ex);
            }
            return result;

        }
        #endregion

        #region ICommandFactory
        public ICommand Create(CommandObject args)
        {
            return new General
            {
                RequestID = args.RequestID,
                UserID = args.UserID,
                Command = args.Command,
                Entity = args.Entity,
                JSONObjectArgs = args.Data,
            };
        }
        #endregion
    }
}
