﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Dynamic;

using Newtonsoft.Json;
using Server.Infrastructure;


namespace APIManager.Commands
{
    class GenericCommand : BaseResponse, ICommandFactory
    {
        #region ICommand

        public override dynamic Execute()
        {
            dynamic result = new ExpandoObject();
            switch (Entity)
            {
                case "TestCommand":
                    result.Result = "OK";
                    result.Grade = "Very Good";
                    break;
            }
            result.RequestID = this.RequestID;
            result.Command = this.Command;
            result.Entity = this.Entity;
            return result;
        }
        #endregion

        #region ICommandFactory
        public ICommand Create(CommandObject args)
        {
            return new GenericCommand
            {
                RequestID = args.RequestID,
                UserID = args.UserID,
                Command = args.Command,
                Entity = args.Entity,
                JSONObjectArgs = args.Data.Replace(@"\", @"\\"),
            };
        }
        #endregion

       
    }
}
