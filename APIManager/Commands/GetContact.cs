﻿using Infrastructure;
using Infrastructure.Entities;
using Infrastructure.Responses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIManager.DataCommands
{
    class GetContact : ICommand, ICommandFactory
    {
        #region ICommand
        public string JSONObjectArgs { get; set; }
        public string Method { get; set; }
        public string RequestID { get; set; }

        public string Execute()
        {
            Logger4Net.Debug(string.Format("call execute for get users"));
            SingleContact contact = JsonConvert.DeserializeObject<SingleContact>(JSONObjectArgs);
            var response = DataManager.getInstance().GetContact(contact);
            response.RequestID = this.RequestID;
            return JsonConvert.SerializeObject(response);
        }
        #endregion

        #region ICommandFactory
        public ICommand Create(string[] args)
        {
            Logger4Net.Debug("Users class initiate...");
            return new Users
            {
                RequestID = args[0],
                Method = args[1],
                JSONObjectArgs = "",
            };
        }
        #endregion
    }
}
