﻿using Infrastructure;
using Newtonsoft.Json;
using Server.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIManager.Commands
{
    class GetLanguages : BaseResponse, ICommandFactory
    {
        #region ICommand

        public override dynamic Execute()
        {
            Log4Tech.Log.Instance.Write("call execute for get languages",this);
            var response = DataManager.Instance.GetLanguages();
            response.RequestID = RequestID;
            response.Command = Command;
            response.Entity = Entity;
            return response;
        }
        #endregion

        #region ICommandFactory
        public ICommand Create(CommandObject args)
        {
            return new GetLanguages
            {
                RequestID = args.RequestID,
                UserID = args.UserID,
                Command = args.Command,
                Entity = args.Entity,
                JSONObjectArgs = args.Data,
            };
        }
        #endregion
    }
}
