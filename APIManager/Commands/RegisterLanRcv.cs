﻿using Infrastructure;
using Infrastructure.Entities;
using Newtonsoft.Json;
using Server.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIManager.Commands
{
    class RegisterLanRcv : BaseResponse, ICommandFactory
    {

        public override dynamic Execute()
        {
            Log4Tech.Log.Instance.Write("call execute for register lan-rcv with args: {0}",this, Log4Tech.Log.LogSeverity.DEBUG,  JSONObjectArgs);
            LanRcvRegisteration lanRcv = JsonConvert.DeserializeObject<LanRcvRegisteration>(JSONObjectArgs);
            var response = DataManager.Instance.RegsiterLanRcv(lanRcv);
            response.RequestID = this.RequestID;
            response.Command = this.Command;
            response.Entity = this.Entity;
            return response;
        }

        public ICommand Create(CommandObject args)
        {
            return new RegisterLanRcv
            {
                RequestID = args.RequestID,
                UserID = args.UserID,
                Command = args.Command,
                Entity = args.Entity,
                JSONObjectArgs = args.Data.Replace(@"\", @"\\"),
            };
        }
    }
}
