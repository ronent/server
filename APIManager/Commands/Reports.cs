﻿using Infrastructure;
using Newtonsoft.Json;
using Server.Infrastructure;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIManager.Commands
{
    class Reports : BaseResponse, ICommandFactory
    {
        #region ICommand

        public override dynamic Execute()
        {
            dynamic result = new ExpandoObject();
            result.Result = "Error";
            result.RequestID = RequestID;
            try
            {
                //checks which method to execute
                var command = (CFRActions.eMethods)Enum.Parse(typeof(CFRActions.eMethods), this.Command);
                switch (command)
                {
                    case CFRActions.eMethods.boomerang:
                        Log4Tech.Log.Instance.Write("call execute for boomerang with args: {0}",this, Log4Tech.Log.LogSeverity.DEBUG,  JSONObjectArgs);
                        var device = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                        break;
                    case CFRActions.eMethods.getarchive:
                        return getArchive();
                    case CFRActions.eMethods.delete:
                        return deleteReports();
                    case CFRActions.eMethods.getreviews:
                        return getReviews();
                    case CFRActions.eMethods.count:
                        return getWaitingApprovalCount();
                    case CFRActions.eMethods.approval:
                        return approveReport();
                    case CFRActions.eMethods.addprofile:
                        return addProfile();
                    case CFRActions.eMethods.getapprovalusers:
                        return getApprovalReviewersList();
                    case CFRActions.eMethods.getdistributionlist:
                        return getDistributionList();
                    case CFRActions.eMethods.getprofiles:
                        return getProfiles();
                    case CFRActions.eMethods.fileupload:
                        return uploadReportHeader();
                    case CFRActions.eMethods.updateprofiles:
                        return updateProfile();
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in Reports Execute",this, ex);
            }

            return result;
        }
        #endregion

        #region private methods

        private dynamic updateProfile()
        {
            Log4Tech.Log.Instance.Write("call update profile report - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);

            dynamic result = initResponse();
            var profile = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
            result.Result = "Error";
            try
            {
                var response = DataManager.Instance.AddProfileReport(Convert.ToInt32(UserID), profile,false);
                if (response != null)
                    result.Result = "OK";
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in updateProfile", this, ex);
            }
            return result;
        }

        private dynamic uploadReportHeader()
        {
            Log4Tech.Log.Instance.Write("call upload file header - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                //S3Manager.Instance.UploadReportTemplatesHeader()
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in uploadReportHeader", this, ex);
            }
            return result;
        }

        private dynamic getProfiles()
        {
            Log4Tech.Log.Instance.Write("call get Profiles List - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);

            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var profile = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                var response = DataManager.Instance.GetReportProfiles(Convert.ToInt32(UserID), profile.list.ToString());
                if (response != null && response.Data != null)
                {
                    result.Result = "OK";
                    result.Data = response.Data;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in getProfiles", this, ex);
            }
            return result;
        }

        private dynamic getDistributionList()
        {
            Log4Tech.Log.Instance.Write("call get Distribution List - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);

            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var response = DataManager.Instance.GetDistributionList(Convert.ToInt32(UserID));
                if (response != null && response.Data != null)
                {
                    result.Result = "OK";
                    result.Data = response.Data;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in getDistributionList", this, ex);
            }
            return result;
        }

        private dynamic getApprovalReviewersList()
        {
            Log4Tech.Log.Instance.Write("call get Approval Reviewers List - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);

            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var response = DataManager.Instance.GetApprovalsReviewersList(Convert.ToInt32(UserID));
                if (response != null && response.Data != null)
                {
                    result.Result = "OK";
                    result.Data = response.Data;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in getApprovalReviewersList", this, ex);
            }
            return result;
        }

        private dynamic addProfile()
        {
            Log4Tech.Log.Instance.Write("call add Profile report - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);

            dynamic result = initResponse();
            var profile = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
            result.Result = "Error";
            try
            {
                var response = DataManager.Instance.AddProfileReport(Convert.ToInt32(UserID), profile);
                if (response.Result=="OK")
                {
                    profile.profileId = response.ReportID;
                    //checks if the report is active and the period type is ONCE
                    //then call to execute the report
                    if (profile.activity.ToString().ToLower() == "true" && profile.scheduleType.ToString()=="ONCE")
                        ReportAutomation.Execute(profile);
                   
                    result.Result = "OK";
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in addProfile", this, ex);
            }
            return result;
        }

        private dynamic approveReport()
        {
            Log4Tech.Log.Instance.Write("call approve report - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);

            dynamic result = initResponse();
            var report = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
            result.Result = "Error";
            try
            {
                var response = DataManager.Instance.ApproveReport(Convert.ToInt32(UserID), Convert.ToInt32(report.report.id.ToString()));
                if (response != null)
                {
                    result.Result = "OK";
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in approveReport", this, ex);
            }
            return result;
        }

        private dynamic getWaitingApprovalCount()
        {
            Log4Tech.Log.Instance.Write("call get reviews - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);

            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var response = DataManager.Instance.GetReportsReviews(Convert.ToInt32(UserID), getReportType("YOUR_APPROVAL"));
                if (response.Data != null)
                {
                    result.Result = "OK";
                    result.count = response.Data.Count;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in getReviews", this, ex);
            }
            return result;
        }

        private dynamic getReviews()
        {
            Log4Tech.Log.Instance.Write("call get reviews - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);

            dynamic result = initResponse();
            var reviews = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
            result.Result = "Error";
            try
            {
                var response = DataManager.Instance.GetReportsReviews(Convert.ToInt32(UserID), getReportType(reviews.list.ToString()));
                if (response.Data != null)
                {
                    result.Result = "OK";
                    result.Data = response.Data;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in getReviews", this, ex);
            }
            return result;
        }

        private int getReportType(string list)
        {
            switch (list)
            {
                case "ALL":
                    return 0;
                case "PENDING_ACTION":
                    return 1;
                case "YOUR_APPROVAL":
                    return 2;
                case "OTHERS_APPROVAL":
                    return 3;
            }
            return 0;
        }

        private dynamic deleteReports()
        {
            Log4Tech.Log.Instance.Write("call delete reports - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);

            dynamic result = initResponse();
            var deleteList = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
            result.Result = "Error";
            try
            {

                foreach (var delete in deleteList)
                {
                    DataManager.Instance.DeleteReport(Convert.ToInt32(delete.id.ToString()));
                }

                var start = Common.Utc2TimeStamp(DateTime.MinValue);
                var end = Common.Utc2TimeStamp(DateTime.Now);
                var response = DataManager.Instance.GetReportsArchive(Convert.ToInt32(UserID), start, end);
                result.Result = "OK";
                result.Data = response.Data;
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in getArchive", this, ex);
            }
            return result;
        }

        private dynamic getArchive()
        {
            Log4Tech.Log.Instance.Write("call get Archive - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);

            dynamic result = initResponse();
            var archive = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
            result.Result = "Error";
            try
            {
                int start, end;
                if (Common.CheckDateRange(archive, out start, out end) == false)
                    return result;
                var response = DataManager.Instance.GetReportsArchive(Convert.ToInt32(UserID), start, end);
                result.Result = "OK";
                result.Data = response.Data;
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in getArchive", this, ex);
            }
            return result;
        }
        #endregion

        #region ICommandFactory
        public ICommand Create(CommandObject args)
        {
            return new Reports
            {
                RequestID = args.RequestID,
                UserID = args.UserID,
                Command = args.Command,
                Entity = args.Entity,
                JSONObjectArgs = args.Data,
            };
        }
        #endregion
    }
}
