﻿using Infrastructure;
using Newtonsoft.Json;
using Server.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIManager.Commands
{
    public class SMS : BaseResponse, ICommandFactory
    {
       #region ICommand

        public override dynamic Execute()
        {
            dynamic result = new Response() { Result = "Error", RequestID = this.RequestID };
            try
            {
                CFRActions.eMethods command = (CFRActions.eMethods)Enum.Parse(typeof(Infrastructure.CFRActions.eMethods), this.Command);

                switch (command)
                {
                    case CFRActions.eMethods.smsdelivery:
                        Log4Tech.Log.Instance.Write("call execute for sms delivery", this);
                        Log4Tech.Log.Instance.Write("SMS Delivery params {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
                        dynamic smsParams = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                        //parse the message according to the format:
                        //'Data':'{"msg_id":"VE_251622033","status":"ENROUTE","detail":"1","to":"972528231144"}'
                        Log4Tech.Log.Instance.Write("SMS #{0} was sent to {1} with status {2}", this, Log4Tech.Log.LogSeverity.INFO, smsParams.msg_id, smsParams.to, smsParams.status);
                        DataManager.Instance.UpdateSMSStatus(smsParams.msg_id.ToString(), smsParams.status.ToString());
                        dynamic response = new System.Dynamic.ExpandoObject();
                        response.Result = "OK";
                        response.RequestID = this.RequestID;
                        response.Command = this.Command;
                        response.Entity = this.Entity;
                        result = response;
                        break;
                }
            }
            catch(Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in SMS Delivery", this, ex);
            }
            return result;
        }
        #endregion
        #region ICommandFactory
        public ICommand Create(CommandObject args)
        {
            return new SMS
            {
                RequestID = args.RequestID,
                UserID = args.UserID,
                Command = args.Command,
                Entity = args.Entity,
                JSONObjectArgs = args.Data,
            };
        }
        #endregion
    }
}
