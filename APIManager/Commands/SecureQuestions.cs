﻿using Infrastructure;
using Newtonsoft.Json;
using Server.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIManager.Commands
{
    class SecureQuestions : BaseResponse, ICommandFactory
    {
        #region ICommand

        public override dynamic Execute()
        {
            Log4Tech.Log.Instance.Write("call execute for get Secure questions",this);
            var response = DataManager.Instance.GetSecureQuestions();
            response.RequestID = this.RequestID;
            response.Command = this.Command;
            response.Entity = this.Entity;
            return response;
        }
        #endregion

        #region ICommandFactory
        public ICommand Create(CommandObject args)
        {
            return new SecureQuestions
            {
                RequestID = args.RequestID,
                UserID = args.UserID,
                Command = args.Command,
                Entity = args.Entity,
                JSONObjectArgs = args.Data,
            };
        }
        #endregion
    }
}
