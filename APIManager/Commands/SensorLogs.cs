﻿using Infrastructure;
using Infrastructure.Entities;
using Infrastructure.Responses;
using Newtonsoft.Json;
using Server.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIManager.Commands
{
    class SensorLogs : BaseResponse, ICommandFactory
    {
        #region ICommand

        public override dynamic Execute()
        {
            //checks which method to execute
            dynamic result = new Response() { Result = "Error", RequestID = this.RequestID };
            try
            {
                //checks which method to execute
                CFRActions.eMethods command = (CFRActions.eMethods)Enum.Parse(typeof(Infrastructure.CFRActions.eMethods), this.Command);
                switch (command)
                {
                    case CFRActions.eMethods.get:
                        Log4Tech.Log.Instance.Write("call execute for get last sensor logs",this);

                        SingleSensor singleSensor = new SingleSensor();
                        if (JSONObjectArgs.Length > 0)
                        {
                            singleSensor = JsonConvert.DeserializeObject<SingleSensor>(JSONObjectArgs);
                        }
                        ResponseSensorLogs getSensorLogs = DataManager.Instance.GetLastSensorLogs(singleSensor);
                        getSensorLogs.RequestID = this.RequestID;
                        getSensorLogs.Command = this.Command;
                        getSensorLogs.Entity = this.Entity;
                        result = getSensorLogs;
                        break;
                    case CFRActions.eMethods.create:
                        break;
                    
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in SensorLogs Execute", this, ex);
            }
            return result;
        }
        #endregion

        #region ICommandFactory
        public ICommand Create(CommandObject args)
        {
            return new SensorLogs
            {
                RequestID = args.RequestID,
                UserID = args.UserID,
                Command = args.Command,
                Entity = args.Entity,
                JSONObjectArgs = args.Data.Replace(@"\", @"\\"),
            };
        }
        #endregion
    }
}
