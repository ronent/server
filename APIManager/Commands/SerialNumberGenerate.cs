﻿using Infrastructure;
using Newtonsoft.Json;
using Server.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIManager.Commands
{
    class SerialNumberGenerate : BaseResponse, ICommandFactory
    {
        #region ICommand

        public override dynamic Execute()
        {
            //checks which method to execute
            dynamic result = new System.Dynamic.ExpandoObject();
            result.Result = "Error";
            result.RequestID = this.RequestID;
            result.Command = this.Command;
            result.Entity = this.Entity;
            try
            {
                //checks which method to execute
                CFRActions.eMethods command = (CFRActions.eMethods)Enum.Parse(typeof(Infrastructure.CFRActions.eMethods), this.Command);
                switch (command)
                {
                    case CFRActions.eMethods.get:
                        Log4Tech.Log.Instance.Write("call execute for get next serial number",this);
                        dynamic serialNumber = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                        int getNextSerialNumber = DataManager.Instance.GetNextSerialNumber(serialNumber.AllocateNumber);
                        result.Result = "OK";
                        result.NextSerialNumber = getNextSerialNumber;
                        break;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in SerialNumberGenerate Execute",this, ex);
            }
            return result;
        }
        #endregion

        #region ICommandFactory
        public ICommand Create(CommandObject args)
        {
            return new SerialNumberGenerate
            {
                RequestID = args.RequestID,
                UserID = args.UserID,
                Command = args.Command,
                Entity = args.Entity,
                JSONObjectArgs = args.Data,
            };
        }
        #endregion
    }
}
