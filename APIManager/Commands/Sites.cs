﻿using Infrastructure;
using Infrastructure.Entities;
using Infrastructure.Responses;
using Newtonsoft.Json;
using Server.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIManager.Commands
{
    class Sites : BaseResponse, ICommandFactory
    {
        #region ICommand

        public override dynamic Execute()
        {
            dynamic result = new Response() { Result = "Error", RequestID = this.RequestID };
            try
            {
                //checks which method to execute
                CFRActions.eMethods command = (CFRActions.eMethods)Enum.Parse(typeof(Infrastructure.CFRActions.eMethods), this.Command);
                dynamic site = null;
                switch (command)
                {
                    case CFRActions.eMethods.get:
                        Log4Tech.Log.Instance.Write("call execute for get user sites with args: {0}",this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
                        SingleUser user = JsonConvert.DeserializeObject<SingleUser>(JSONObjectArgs);
                        var sites = DataManager.Instance.GetUserSites(Convert.ToInt32(this.UserID.ToString()));
                        result = sites;
                        break;
                    case CFRActions.eMethods.create:
                        Log4Tech.Log.Instance.Write("call execute for new site with args: {0}",this, Log4Tech.Log.LogSeverity.DEBUG,  JSONObjectArgs);
                        site = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                        site.UserID = this.UserID;
                        result = DataManager.Instance.CreateSite(site);
                        if (result.IsOK)
                        {
                            var site_data = DataManager.Instance.GetSiteById(result.ID);
                            result.Name = site_data.Name;
                        }
                        
                        //add the site from database to return
                        //var sitesToReturn = DataManager.Instance.GetUserSites(Convert.ToInt32(this.UserID.ToString()));
                        //result.Sites = sitesToReturn.Data;
                        break;
                    case CFRActions.eMethods.update:
                        Log4Tech.Log.Instance.Write("call execute for update site with args: {0}",this, Log4Tech.Log.LogSeverity.DEBUG,  JSONObjectArgs);
                        site = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                        site.UserID = this.UserID;
                        result = DataManager.Instance.UpdateSite(site);
                        break;
                    case CFRActions.eMethods.delete:
                        Log4Tech.Log.Instance.Write("call execute for delete site with args: {0}",this, Log4Tech.Log.LogSeverity.DEBUG,  JSONObjectArgs);
                        site = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                        site.UserID = this.UserID;
                        result = DataManager.Instance.DeleteSite(site);
                        break;
                }
                result.RequestID = this.RequestID;
                result.Command = this.Command;
                result.Entity = this.Entity;
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in Sites Execute", this, ex);
            }
            return result;
            
        }
        #endregion

        #region ICommandFactory
        public ICommand Create(CommandObject args)
        {
            return new Sites
            {
                RequestID = args.RequestID,
                UserID = args.UserID,
                Command = args.Command,
                Entity = args.Entity,
                JSONObjectArgs = args.Data.Replace(@"\", @"\\"),
            };
        }
        #endregion
    }
}
