﻿using Infrastructure;
using Infrastructure.Base;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIManager.DataCommands
{
    class UpdateUser : ICommand, ICommandFactory
    {
        #region ICommand
        public string JSONObjectArgs { get; set; }
        public string Method { get; set; }
        public string RequestID { get; set; }

        public string Execute()
        {
            Logger4Net.Debug(string.Format("call execute for update user with args: {0}", JSONObjectArgs));
            Users user = JsonConvert.DeserializeObject<Users>(JSONObjectArgs);
            var response = DataManager.getInstance().UpdateUser(user);
            response.RequestID = this.RequestID;
            return JsonConvert.SerializeObject(response);
        }
        #endregion

        #region ICommandFactory
        public ICommand Create(string[] args)
        {
            Logger4Net.Debug("UpdateUser class initiate...");
            return new UpdateUser
            {
                RequestID = args[0],
                Method = args[1],
                JSONObjectArgs = args[2],
            };
        }
        #endregion
    }
}
