﻿using Infrastructure;
using Infrastructure.Entities;
using Infrastructure.MailService;
using Infrastructure.Responses;
using Newtonsoft.Json;
using Server.Infrastructure;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Dynamic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using DeviceLayer;

namespace APIManager.Commands
{
    class Users : BaseResponse, ICommandFactory
    {
        #region ICommand

        public override dynamic Execute()
        {
            dynamic result = new Response() { Result = "Error", RequestID = this.RequestID };
            try
            {
                //checks which method to execute
                var command = (CFRActions.eMethods)Enum.Parse(typeof(Infrastructure.CFRActions.eMethods), this.Command);
                switch (command)
                {
                    case CFRActions.eMethods.getusers:
                        return getUsers();

                    case CFRActions.eMethods.createadmin:
                        Log4Tech.Log.Instance.Write("call execute for new admin user with args: {0}", this,
                            Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
                        var newUser = JsonConvert.DeserializeObject<AdminUser>(JSONObjectArgs);
                        var postBack = DataManager.Instance.CreateNewAdminUser(newUser);
                        postBack.RequestID = RequestID;
                        result = postBack;
                        break;
                    case CFRActions.eMethods.setup:
                        return setupUser();
                    case CFRActions.eMethods.passwordrecovery:
                        return passwordRecovery();
                    case CFRActions.eMethods.secureanswercheck:
                        return resetPassword();
                    case CFRActions.eMethods.resetpassword:
                        return resetPasswordUpdate();
                    case CFRActions.eMethods.getuser:
                        return getUser();
                    case CFRActions.eMethods.getaccountbalance:
                        return getCustomerBalance();
                    case CFRActions.eMethods.changepwd:
                        return changePassword();
                    case CFRActions.eMethods.getsystemsettings:
                        return getSystemSettings();
                    case CFRActions.eMethods.savesystemsettings:
                        return saveSystemSettings();
                    case CFRActions.eMethods.getdeviceautosetup:
                        return getDeviceAutoSetup();
                    case CFRActions.eMethods.applydeviceautosetup:
                        return applyDeviceAutoSetup();
                    case CFRActions.eMethods.getsetupfiles:
                        return getSetupFiles();
                    case CFRActions.eMethods.deletesetupfiles:
                        return deleteSetupFiles();
                    case CFRActions.eMethods.getcalibrationcertificatedefaults:
                        return getCalibrationCertificateDefaults();
                    case CFRActions.eMethods.setcalibrationcertificatedefaults:
                        return setCalibrationCertificateDefaults();
                    case CFRActions.eMethods.fwupdates:
                        return checkFWUpdates();
                    case CFRActions.eMethods.startfwupdate:
                        return startFwUpdate();
                    case CFRActions.eMethods.getactivity:
                        return getActivity();
                    case CFRActions.eMethods.getpwdconfig:
                        return getPwdConfig();
                    case CFRActions.eMethods.getprivilegesgroups:
                        return getPrivilegesGroups();
                    case CFRActions.eMethods.getmatrix:
                        return getMatrix();
                    case CFRActions.eMethods.updatepwdconfig:
                        return updatePwdConfig();
                    case CFRActions.eMethods.saveuser:
                        var userData = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                        return ((Newtonsoft.Json.Linq.JObject)userData).GetValue("userId").ToString() == string.Empty
                            ? createNewUser()
                            : saveUser();
                    case CFRActions.eMethods.invokeaction:
                        return invokeAction();
                    case CFRActions.eMethods.getdata:
                        return getAuditTrailData();
                    case CFRActions.eMethods.deassociatedevices:
                        return deAssociateDevices();
                    case CFRActions.eMethods.newgroup:
                        return newGroup();
                    case CFRActions.eMethods.getroles:
                        return getRoles();
                    case CFRActions.eMethods.savematrix:
                        return saveMatrix();
                    case CFRActions.eMethods.deleteusers:
                        return deleteUsers();
                    case CFRActions.eMethods.guidisvalid:
                        return guidIsValid();
                    case CFRActions.eMethods.registeruser:
                        return setupUser();
                    case CFRActions.eMethods.saveaccountprofile:
                        return saveAccountProfile();
                    case CFRActions.eMethods.resetpwd:
                        return resetUserPassword();
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in Users Execute",this, ex);
            }
            
            return result;
        }
        #endregion
        #region private methods

        private dynamic passwordRecovery()
        {
            Log4Tech.Log.Instance.Write("call passwordRecovery - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var userData = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                var responseReset = DataManager.Instance.GetUserByUserName(userData.recovery.username.ToString());
                if (responseReset.Result == "OK")
                {
                    //get the token
                    string token = responseReset.Token;
                    //build the body
                    string body = getResetPasswordEmailBody(responseReset.UserName, token);
                    //send the email
                    var fromMail = new MailAddress(ConfigurationManager.AppSettings["fromEmail"],
                        "SiMiO");

                    var mail = new MailMessage
                    {
                        Body = body,
                        From = fromMail,
                        IsBodyHtml = true,
                        Subject = ConfigurationManager.AppSettings["resetEmailSubject"],
                        To = {responseReset.Email},
                    };
                    Log4Tech.Log.Instance.Write("Sending reset password email to {0}", this,
                        Log4Tech.Log.LogSeverity.DEBUG,
                        responseReset.Email);
                    MailManager.SendEmail(mail);

                    result.Result = "OK";
                }
                else
                {
                    result.Error = "Username does not exists";
                }

            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in passwordRecovery", this, ex);
            }
            return result;
        }

        private dynamic resetUserPassword()
        {
            Log4Tech.Log.Instance.Write("call resetUserPassword - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var newPasswordData = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                var response = DataManager.Instance.ResetUserPassword(newPasswordData);
                result.Result = response.Result;
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in resetUserPassword", this, ex);
            }
            return result;
        }

        private dynamic guidIsValid()
        {
            Log4Tech.Log.Instance.Write("call guidIsValid - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            result.Data = new ExpandoObject();
            result.Data.valid = false;
            try
            {
                var guidData = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                var response = DataManager.Instance.UserGuidIsValid(guidData.guid.ToString());
                result.Result = "OK";
                if (response.Result == "OK")
                    result.Data.valid = true;
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in guidIsValid", this, ex);
            }
            return result;
        }

        private dynamic deleteUsers()
        {
            Log4Tech.Log.Instance.Write("call deleteUsers - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var deletedUsers = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                foreach (var deletedUser in deletedUsers)
                {
                    DataManager.Instance.DeleteUser(Convert.ToInt32(deletedUser.userId.ToString()));
                }
                return getUsers();
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in deleteUsers", this, ex);
            }
            return result;
        }

        private dynamic resetPasswordUpdate()
        {
            Log4Tech.Log.Instance.Write("call resetPasswordUpdate - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var newPwd = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                var response = DataManager.Instance.ResetPassword(newPwd);
                result.Result = response.Result;

            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in resetPasswordUpdate", this, ex);
            }
            return result;
        }

        private dynamic setupUser()
        {
            Log4Tech.Log.Instance.Write("call setupUser - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var userData = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                var response = DataManager.Instance.SetupUser(userData);
                if (response.Result == "Error")
                    result.Error = response.Error;
                else
                    result.Result = "OK";

            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in setupUser", this, ex);
            }
            return result;
        }

        private dynamic saveMatrix()
        {
            Log4Tech.Log.Instance.Write("call saveMatrix - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var userId = Convert.ToInt32(UserID);
                var matrixData = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                DataManager.Instance.SaveMatrix(userId, matrixData);
                result.Result = "OK";
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in saveMatrix", this, ex);
            }
            return result;
        }

        private dynamic getRoles()
        {
            Log4Tech.Log.Instance.Write("call getRoles - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var userId = Convert.ToInt32(UserID);
                var response = DataManager.Instance.GetPermissonRoles(userId);
                result.Data = response.Data;
                result.Result = "OK";
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in getRoles", this, ex);
            }
            return result;
        }

        private dynamic createNewUser()
        {
            //if creation of user was ok
            //send an invitation to the user email
            //contain a link for the cloud app
            //with hash key that expires after 24H
            //when clicking on the link
            Log4Tech.Log.Instance.Write("call saveUser - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var userId = Convert.ToInt32(UserID);
                var userData = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                var user = DataManager.Instance.AddUser(userId, userData);
                if (user.Result == "OK")
                {
                    sendInvitation(user.email.ToString(), user.guid.ToString(),
                        Convert.ToInt32(user.newUserId.ToString()));
                    result.Result = "OK";
                }
                else
                {
                    result.Error = user.Error;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in saveUser", this, ex);
            }
            return result;
        }

        private void sendInvitation(string email, string guid, int userId)
        {
            var emailBody = new StringBuilder();
            emailBody.AppendFormat("Congratulations! </br>");
            emailBody.AppendFormat("You have just been granted to access to SiMiO </br>");
            emailBody.AppendFormat("Please click <a href='{0}?regID={1}'>here</a> to continue your registeration. </br>", ConfigurationManager.AppSettings["SignupURL"], guid);
            emailBody.AppendFormat("(Notice: This link will be expired in the next 7 days) </br>");
            emailBody.AppendFormat("Regards,</br>");
            emailBody.AppendFormat("The SiMiO Team</br>");

            Common.SendEmail(email, "New user in SiMiO", emailBody.ToString());
        }

        private dynamic newGroup()
        {
            Log4Tech.Log.Instance.Write("call newGroup - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var userId = Convert.ToInt32(UserID);
                var groupData = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                DataManager.Instance.NewPermissionGroup(userId, groupData);
               
                result.Result = "OK";

            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in newGroup", this, ex);
            }
            return result;
        }

        private dynamic deAssociateDevices()
        {
            Log4Tech.Log.Instance.Write("call deAssociateDevices - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var devices = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                foreach (var device in devices)
                {
                    DataManager.Instance.DeAssociateDevices(Convert.ToInt32(device.SerialNumber.ToString()));
                }
                result.Result = "OK";
                PushGUIManager.SendAlarm(null);
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in deAssociateDevices", this, ex);
            }
            return result;
        }

        private dynamic getAuditTrailData()
        {
            Log4Tech.Log.Instance.Write("call getAuditTrailData - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var userId = Convert.ToInt32(UserID);
                var auditData = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                int start, end;
                if (Common.CheckDateRange(auditData, out start, out end))
                {
                    var response = DataManager.Instance.GetAuditTrailData(userId,start,end);
                    result.Data = response.Data;
                    result.Result = "OK";
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in getAuditTrailData", this, ex);
            }
            return result;
        }

        private dynamic resetPassword()
        {
            Log4Tech.Log.Instance.Write("call execute for reset password email with args: {0}", this,
                Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var userData = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                var responseReset = DataManager.Instance.SecureAnswerCheck(userData);
                //checks if the response is valid then send an email

                Log4Tech.Log.Instance.Write("response of reset email is OK", this);
                //get the token
                string token = responseReset.Token;
                //build the body
                string body = getResetPasswordEmailBody(responseReset.UserName, token);
                //send the email
                var fromMail = new MailAddress(ConfigurationManager.AppSettings["fromEmail"],
                    "SiMiO");

                var mail = new MailMessage
                {
                    Body = body,
                    From = fromMail,
                    IsBodyHtml = true,
                    Subject = ConfigurationManager.AppSettings["resetEmailSubject"],
                    To = {responseReset.Email},
                };
                Log4Tech.Log.Instance.Write("Sending reset password email to {0}", this,
                    Log4Tech.Log.LogSeverity.DEBUG,
                    responseReset.Email);
                MailManager.SendEmail(mail);

                result.Result = "OK";

            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in resetPassword", this, ex);
            }
            return result;
        }

        private dynamic invokeAction()
        {
            Log4Tech.Log.Instance.Write("call invokeAction - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var invokeData = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                string action = invokeData.action.ToString();

                switch (action.ToLower())
                {
                    case "resetpassword":
                        return resetPassword();
                    case "retrieveusername":
                        return retrieveUserName();
                    case "resendinvitation":
                        return resendInvitation();
                }

                result.Result = "OK";
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in invokeAction", this, ex);
            }
            return result;
        }

        private dynamic resendInvitation()
        {
            Log4Tech.Log.Instance.Write("call resendInvitation - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var userData = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                int userId;
                if (int.TryParse(userData.user.userId.ToString(), out userId))
                {
                    var user = DataManager.Instance.GetUserRecover(userId);
                    if (user.Data != null)
                    {
                        var email = user.Data.userEmail.ToString();
                        var guid = user.Data.GUID.ToString();

                        sendInvitation(email,guid,userId);

                        result.Result = "OK";
                    }
                }

            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in resendInvitation", this, ex);
            }
            return result;
        }

        private void SendUserNameRecover(string userName, string email)
        {
            var bodyHtml = new StringBuilder();
            bodyHtml.AppendFormat("Hi {0}, <br><br>", userName);
            bodyHtml.AppendFormat("Here is your username to login to the system : {0} <br><br>", userName);
            bodyHtml.AppendFormat("Regards,<br>");
            bodyHtml.AppendFormat("The SiMiO Team");

            Common.SendEmail(email, "UserName recover", bodyHtml.ToString());
        }

        private dynamic retrieveUserName()
        {
            Log4Tech.Log.Instance.Write("call retrieveUserName - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var userData = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                int userId;
                if (int.TryParse(userData.user.userId.ToString(), out userId))
                {
                    var user = DataManager.Instance.GetUser(userId);
                    if (user.Data != null)
                    {
                        var email = user.Data.userEmail.ToString();
                        var userName = user.Data.userName.ToString();

                        SendUserNameRecover(userName, email);

                        result.Result = "OK";
                    }
                }

            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in retrieveUserName", this, ex);
            }
            return result;
        }

        private dynamic updatePwdConfig()
        {
            Log4Tech.Log.Instance.Write("call updatePwdConfig - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var userId = Convert.ToInt32(UserID);
                var passwordExpiry = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                var response = DataManager.Instance.UpdatePwdConfig(userId, passwordExpiry);
                result.Data = response.Data;
                result.Result = "OK";
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in updatePwdConfig", this, ex);
            }
            return result;
        }

        private dynamic getMatrix()
        {
            Log4Tech.Log.Instance.Write("call getMatrix - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var userId = Convert.ToInt32(UserID);
                var matrix = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                var response = DataManager.Instance.GetMatrix(userId, Convert.ToInt32(matrix.groupLevel.ToString()));
                result.Data = response.Data;
                result.Result = "OK";
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in getMatrix", this, ex);
            }
            return result;
        }

        private dynamic getPrivilegesGroups()
        {
            Log4Tech.Log.Instance.Write("call getPrivilegesGroups - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var userId = Convert.ToInt32(UserID);
                var response = DataManager.Instance.GetPrivilegesGroups(userId);
                result.Data = response.Data;
                result.Result = "OK";
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in getPrivilegesGroups", this, ex);
            }
            return result;
        }

        private dynamic getPwdConfig()
        {
            Log4Tech.Log.Instance.Write("call getPwdConfig - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var userId = Convert.ToInt32(UserID);
                var response = DataManager.Instance.GetPasswordExpiry(userId);
                result.Data = response.Data;
                result.Result = "OK";
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in getPwdConfig", this, ex);
            }
            return result;
        }

        private dynamic getActivity()
        {
            Log4Tech.Log.Instance.Write("call getActivity - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var userId = Convert.ToInt32(UserID);
                var response = DataManager.Instance.GetActivity(userId);
                result.Data = response.Data;
                result.Result = "OK";
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in getActivity", this, ex);
            }
            return result;
        }

        private dynamic startFwUpdate()
        {
            Log4Tech.Log.Instance.Write("call startFwUpdate - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            result.Data = new List<dynamic>();
            try
            {
                dynamic fwUpdateSelection = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                var listDeviceTypes = new List<int>();
                foreach (var fwSelection in fwUpdateSelection)
                    listDeviceTypes.Add(Convert.ToInt32(fwSelection.deviceTypeID.ToString()));

                var connectedDevices = Common.DeviceApiManager.GetConnectedDevices();
                if (connectedDevices.Any())
                {
                    var list = new List<dynamic>();
                    //iterate all connected device and checks if need to update firmware
                    foreach (var connectedDevice in connectedDevices)
                    {
                        dynamic device = connectedDevice.Value.Device;
                        var firmwareUpdateRequired = Convert.ToInt32(device.FirmwareUpdateRequired);
                        if (firmwareUpdateRequired > 0)
                        {
                            //checks if this type is the one selected for firmware update
                            var deviceTypeId = Convert.ToInt32(device.DeviceTypeID);
                            if (listDeviceTypes.Exists(x => x == deviceTypeId))
                                //send firmware update for this connected device
                                Task.Factory.StartNew(
                                    () => Common.DeviceApiManager.UpdateFirmware(device.SerialNumber.ToString()));
                        }
                    }
                    result.Data = list;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in startFwUpdate", this, ex);
            }
            return result;
        }

        private dynamic checkFWUpdates()
        {
            Log4Tech.Log.Instance.Write("call checkFWUpdates - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            result.Data = new List<dynamic>();
            try
            {
                var Devices = DataManager.Instance.GetDevicesByUser(Convert.ToInt32(UserID));
                //if (Devices.Data)
                {
                    var list = new List<dynamic>();
                    //iterate all connected device and checks if need to update firmware
                    foreach (dynamic device in Devices.Data)
                    {
                        var firmwareUpdateRequired = Convert.ToInt32(device.FirmwareUpdateRequired);
                        if (firmwareUpdateRequired > 0)
                        {
                            dynamic data = new ExpandoObject();
                            data.currentVersion = device.FirmwareVersion;
                            data.deviceTypeID = device.DeviceTypeID;
                            data.deviceTypeName = device.DeviceType;
                            data.newVersion = device.LatestFwVersion;
                            list.Add(data);
                        }
                    }
                    result.Data = list;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in checkFWUpdates", this, ex);
            }
            return result;
        }

        private dynamic setCalibrationCertificateDefaults()
        {
            Log4Tech.Log.Instance.Write("call setCalibrationCertificateDefaults - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var userId = Convert.ToInt32(UserID);
                dynamic calibrationDefaults = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);

                DataManager.Instance.SetCalibrationCertificateDefault(userId, calibrationDefaults);
                return getCalibrationCertificateDefaults();
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in setCalibrationCertificateDefaults", this, ex);
            }
            return result;
        }

        private dynamic getCalibrationCertificateDefaults()
        {
            Log4Tech.Log.Instance.Write("call getCalibrationCertificateDefaults - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var userId = Convert.ToInt32(UserID);
                var response = DataManager.Instance.GetCalibrationCertificateDefault(userId);
                result.Data = response.Data;
                result.Result = "OK";
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in getCalibrationCertificateDefaults", this, ex);
            }
            return result;
        }

        private dynamic deleteSetupFiles()
        {
            Log4Tech.Log.Instance.Write("call deleteSetupFiles - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var userId = Convert.ToInt32(UserID);
                dynamic setupFile = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                foreach (var setup in setupFile)
                {
                    DataManager.Instance.DeleteSetupFile(userId, setup.id.ToString());
                }
                
                var response = DataManager.Instance.GetDeviceSetupFiles(userId);
                result.Data = response.Data;
                result.Result = "OK";
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in deleteSetupFiles", this, ex);
            }
            return result;
        }

        private dynamic getSetupFiles()
        {
            Log4Tech.Log.Instance.Write("call getSetupFiles - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var userId = Convert.ToInt32(UserID);
                var response = DataManager.Instance.GetDeviceSetupFiles(userId);
                result.Data = response.Data;
                result.Result = "OK";
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in getSetupFiles", this, ex);
            }
            return result;
        }

        private dynamic applyDeviceAutoSetup()
        {
            Log4Tech.Log.Instance.Write("call applyDeviceAutoSetup - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                dynamic settings = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                var userId = Convert.ToInt32(UserID);
                foreach (var data in settings)
                {
                    DataManager.Instance.ApplyDeviceAutoSetup(userId, data);
                }
                var response = DataManager.Instance.GetDeviceAutoSetup(userId);
                result.Data = response.Data;
                result.Result = "OK";
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in applyDeviceAutoSetup", this, ex);
            }
            return result;
        }

        private dynamic getDeviceAutoSetup()
        {
            Log4Tech.Log.Instance.Write("call getDeviceAutoSetup - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var response = DataManager.Instance.GetDeviceAutoSetup(Convert.ToInt32(UserID));
                result.Data = response.Data;
                result.Result = "OK";
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in getDeviceAutoSetup", this, ex);
            }
            return result;
        }

        private dynamic saveSystemSettings()
        {
            Log4Tech.Log.Instance.Write("call saveSystemSettings", this);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var settings = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                settings.userID = UserID;
                var response = DataManager.Instance.SaveSystemSettings(settings);

                result.Data = response.Data;
                result.Result = "OK";
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in saveSystemSettings", this, ex);
            }
            return result;
        }

        private dynamic getSystemSettings()
        {
            Log4Tech.Log.Instance.Write("call getSystemSettings", this);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var response = DataManager.Instance.GetSystemSettings(Convert.ToInt32(UserID));

                result.Data = response.Data;
                result.Result = "OK";
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in getSystemSettings", this, ex);
            }
            return result;
        }

        private dynamic changePassword()
        {
            Log4Tech.Log.Instance.Write("call changePassword", this);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var user = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                var isChanged = DataManager.Instance.ChangePassword(user);

               if(isChanged)
                    result.Result = "OK";
               else
               {
                   result.Error = "Wrong password provided.";
               }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in changePassword", this, ex);
            }
            return result;
        }

        private dynamic saveUser()
        {
            Log4Tech.Log.Instance.Write("call saveUser", this);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var userId = Convert.ToInt32(UserID);
                var user = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                DataManager.Instance.AdminUpdateUser(userId,user);
                result.Result = "OK";
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in saveUser", this, ex);
            }
            return result;
        }

        private dynamic saveAccountProfile()
        {
            Log4Tech.Log.Instance.Write("call saveAccountProfile", this);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var user = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                result = DataManager.Instance.SaveUser(user);
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in saveAccountProfile", this, ex);
            }
            return result;
        }

        private dynamic getCustomerBalance()
        {
            Log4Tech.Log.Instance.Write("call getCustomerBalance", this);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var response = DataManager.Instance.GetCustomerBalance(Convert.ToInt32(UserID));

                result.Data = response.Data;
                result.Result = "OK";
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in getCustomerBalance", this, ex);
            }
            return result;
        }

        private dynamic getUser()
        {
            Log4Tech.Log.Instance.Write("call getUser", this);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var response = DataManager.Instance.GetUser(Convert.ToInt32(UserID));

                result.Data = response.Data;
                result.Result = "OK";
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in getUser", this, ex);
            }
            return result;
        }

        private dynamic getUsers()
        {
            Log4Tech.Log.Instance.Write("call execute for get users", this);
            dynamic result = initResponse();
            result.Result = "Error";
            try
            {
                var userList = DataManager.Instance.GetUsers(Convert.ToInt32(UserID));
                result.Data = userList.Data;
                result.Result = "OK";
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in getUsers", this, ex);
            }
            return result;
        }

        private string getResetPasswordEmailBody(string username, string token)
        {
            var bodyEmail = string.Empty;
            bodyEmail = "<div id=':jw' class='ii gt m13f9e13cebad46e0 adP adO'><div id=':jv' style='overflow: hidden;'>";
            bodyEmail += "<div>";
            bodyEmail += "	<div>";
            bodyEmail += "		<div>";
            bodyEmail += "			<table border='0' cellpadding='0' cellspacing='0' width='100%'>";
            bodyEmail += "				<tbody>";
            bodyEmail += "					<tr>";
            bodyEmail += "						<td width='100%' style='word-wrap:break-word'>";
            bodyEmail += "							<table cellpadding='2' cellspacing='3' border='0' width='100%'>";
            bodyEmail += "								<tbody>";
            bodyEmail += "									<tr>";
            bodyEmail += "										<td align='left' valign='bottom'>";
            bodyEmail += "											<span style='font-weight:bold;font-size:xx-small;font-family:verdana,sans-serif;color:#666'>";
            bodyEmail += "												<b>Fourtec sent this message to ##username##.</b>";
            bodyEmail += "												<br>";
            bodyEmail += "											</span>";
            bodyEmail += "											<span style='font-size:xx-small;font-family:verdana,sans-serif;color:#666'>";
            bodyEmail += "												Your registered name is included to show this message originated from Fourtec. ";
            bodyEmail += "												<a href='http://www.fourtec.com/company/fourtec-quality-policy/' target='_blank'>Learn more</a>.";
            bodyEmail += "											</span>";
            bodyEmail += "										</td>";
            bodyEmail += "									</tr>";
            bodyEmail += "								</tbody>";
            bodyEmail += "							</table>";
            bodyEmail += "						</td>";
            bodyEmail += "					</tr>";
            bodyEmail += "				</tbody>";
            bodyEmail += "			</table>";
            bodyEmail += "		</div>";
            bodyEmail += "	</div>";
            bodyEmail += "	<div>";
            bodyEmail += "		<div>";
            bodyEmail += "			<table border='0' cellpadding='2' cellspacing='3' width='100%'>";
            bodyEmail += "				<tbody>";
            bodyEmail += "					<tr>";
            bodyEmail += "						<td>";
            bodyEmail += "							<font style='font-size:10pt;font-family:arial,sans-serif'>Dear ##username##, <br>";
            bodyEmail += "								<br>This email was sent automatically by Fourtec in response to your request to recover your ";
            bodyEmail += "								<span class='il'>password</span>. ";
            bodyEmail += "								This is done for your protection; only you, the recipient of this email can take the next step in the";
            bodyEmail += "								<span class='il'>password</span> recover process. ";
            bodyEmail += "							</font>";
            bodyEmail += "						</td>";
            bodyEmail += "					</tr>";
            bodyEmail += "					<tr>";
            bodyEmail += "						<td>";
            bodyEmail += "							<font style='font-size:10pt;font-family:arial,sans-serif'>";
            bodyEmail += "								<div>";
            bodyEmail += "									<br>To ";
            bodyEmail += "									<span class='il'>reset</span> ";
            bodyEmail += "									your ";
            bodyEmail += "									<span class='il'>password</span> ";
            bodyEmail += "									and access your account click on the following link :<br>";
            bodyEmail += "									<a href='##reset-page##?rstID=##token##' target='_blank'>";
            bodyEmail += "										##reset-page##?rstID=##token##";
            bodyEmail += "									</a>";
            bodyEmail += "									<br><br>If you did not forget your ";
            bodyEmail += "									<span class='il'>password</span>";
            bodyEmail += "									, please ignore this email.<br>";
            //bodyEmail += "                                  <br>This request was made from:<br> IP address: ##IP## <br>";
            bodyEmail += "								</div>";
            bodyEmail += "							</font>";
            bodyEmail += "						</td>";
            bodyEmail += "					</tr>";
            bodyEmail += "				</tbody>";
            bodyEmail += "			</table>";
            bodyEmail += "		</div>";
            bodyEmail += "	</div>";
            bodyEmail += "	<div>";
            bodyEmail += "		<hr style='min-height:1px'>";
            bodyEmail += "		<table border='0' cellpadding='0' cellspacing='0' width='100%'>";
            bodyEmail += "			<tbody>";
            bodyEmail += "				<tr>";
            bodyEmail += "					<td width='100%'>";
            bodyEmail += "						<font style='font-size:xx-small;font-family:verdana;color:#666'>";
            bodyEmail += "							<a href='http://www.fourtec.com/company/fourtec-quality-policy/' target='_blank'>Learn More</a>";
            bodyEmail += "							to protect yourself from spoof (fake) emails.";
            bodyEmail += "							<br><br>Fourtec sent this email to you at ";
            bodyEmail += "							<a href='mailto:support@fourtec.com' target='_blank'>support@fourtec.com</a> ";
            bodyEmail += "							about your account registered on ";
            bodyEmail += "							<a href='http://www.Fourtec.com' target='_blank'>www.Fourtec.com</a>.";
            bodyEmail += "							<br><br>Fourtec will periodically send you required emails about the site and your transactions. See our ";
            bodyEmail += "							<a href='http://www.fourtec.com/footer/privacy-policy/' target='_blank'>Privacy Policy</a> ";
            bodyEmail += "							and ";
            bodyEmail += "							<a href='http://www.fourtec.com/footer/terms-of-use/' target='_blank'>User Agreement</a> ";
            bodyEmail += "							if you have any questions.<br><br>All Rights Reserved. Designated trademarks and brands are the property of their respective owners.";
            bodyEmail += "						</font>";
            bodyEmail += "					</td>";
            bodyEmail += "				</tr>";
            bodyEmail += "			</tbody>";
            bodyEmail += "		</table>";
            bodyEmail += "	</div>";
            bodyEmail += "</div>";
            bodyEmail += "</div>";
            bodyEmail += "<div class='yj6qo'>";
            bodyEmail += "</div>";
            bodyEmail += "</div>";

            bodyEmail = bodyEmail.Replace("##username##", username);
            bodyEmail = bodyEmail.Replace("##token##", token);
            bodyEmail = bodyEmail.Replace("##reset-page##", ConfigurationManager.AppSettings["ResetPwdPage"]);
            //bodyEmail = bodyEmail.Replace("##IP##", IP);

            return bodyEmail;
        }
        #endregion
        #region ICommandFactory
        public ICommand Create(CommandObject args)
        {
            return new Users
            {
                RequestID = args.RequestID,
                UserID = args.UserID,
                Command = args.Command,
                Entity = args.Entity,
                JSONObjectArgs = args.Data.Replace(@"\", @"\\"),
            };
        }
        #endregion
    }
}
