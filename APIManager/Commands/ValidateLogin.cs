﻿using Infrastructure;
using Infrastructure.Entities;
using Newtonsoft.Json;
using Server.Infrastructure;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DeviceLayer;

namespace APIManager.Commands
{
    class ValidateLogin : BaseResponse, ICommandFactory
    {
        
        #region ICommandFactory
        public override dynamic Execute()
        {
            dynamic result = new ExpandoObject();
            CFRActions.eMethods command = (CFRActions.eMethods)Enum.Parse(typeof(Infrastructure.CFRActions.eMethods), this.Command);
            switch (command)
            {
                case CFRActions.eMethods.validate:
                    result = validateLogin();
                    break;
                case CFRActions.eMethods.get:
                    result = getLoginData();
                    break;
                case CFRActions.eMethods.logout:
                    logout();
                    result.IsOK = true;
                    result.Result = "OK";
                    break;
            }
            result.RequestID = this.RequestID;
            result.Command = this.Command;
            result.Entity = this.Entity;
            return result;
        }
        #endregion
        #region private methods

        private void logout()
        {
            Log4Tech.Log.Instance.Write("call execute for logout with args: {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            try
            {
                var user = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                string existUser = string.Empty;
                //check if the user name exists in the connected users
                if (CommandMain.GetInstance().connectedUsers.TryGetValue(user.UserName.ToString(), out existUser))
                {
                    //then remove it
                    CommandMain.GetInstance().connectedUsers.TryRemove(user.UserName.ToString(), out existUser);
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in logout", this, ex);
            }
        }

        private dynamic getLoginData()
        {
            Log4Tech.Log.Instance.Write("call execute for get login with args: {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic response = new ExpandoObject();
            try
            {
                var user = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                int userID = Convert.ToInt32(user.UserID.ToString());
                var devices = DataManager.Instance.GetDevicesByUser(userID);
                response = getUserData(userID, devices);
                if (devices != null)
                {
                    response.UserName = devices[0].UserName;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in getLogin", this, ex);
            }
            return response;
        }

        private dynamic validateLogin()
        {
            Log4Tech.Log.Instance.Write("call execute for validate login with args: {0}", this, Log4Tech.Log.LogSeverity.DEBUG, JSONObjectArgs);
            dynamic response = new ExpandoObject();
            response.Result = "Error";
            try
            {
                var login = JsonConvert.DeserializeObject<dynamic>(JSONObjectArgs);
                //first validate this login
                response = DataManager.Instance.ValidateLogin(login.UserName.ToString(), login.Password.ToString());
                //if success then checks the concurrent users limit
                if (response.Result == "OK")
                {
                    //checks if customer parameter exists
                    if (login.CustomerExists==1)
                    {
                        //if exists then checks if transfer null then this is first login
                        if (login.Customer == -1)
                        {
                            //return list of customers
                            var result = DataManager.Instance.GetCustomers();
                            if (result.Result == "OK")
                            {
                                response = initResponse();
                                response.Result = "OK";
                                response.Data = result.Data;
                                return response;
                            }
                        }
                        else
                        {
                            //if customer is not null then update "special user" customer id
                            DataManager.Instance.UpdateUserCustomerId(Convert.ToInt32(response.UserID), Convert.ToInt32(login.Customer.ToString()));
                            //and login...
                        }
                    }

                    //get from the license the concurrent limit of users
                    var licConcurrentUsersLimit = getMaxUsersConcurrent();
                    //get the concurrent limit from database
                    var concurrentUsersLimit = response.ConcurrentUsersLimit;
                    //if the limit on the license is lower than the database definition then take the license definition
                    if (licConcurrentUsersLimit < concurrentUsersLimit)
                        concurrentUsersLimit = licConcurrentUsersLimit;

                    Log4Tech.Log.Instance.Write("customer concurrent users limit is {0}", this, Log4Tech.Log.LogSeverity.DEBUG, concurrentUsersLimit);
                    var existsUser = string.Empty;
                    //checks if this user already exists in the concurrent users
                    if (!CommandMain.GetInstance().connectedUsers.TryGetValue(login.UserName.ToString(), out existsUser))
                    {
                        //Log4Tech.Log.Instance.Write("user not exists, checking count {0} vs limit {1}", this, Log4Tech.Log.LogSeverity.DEBUG, CommandMain.GetInstance().connectedUsers.Count + 1, concurrentUsersLimit);
                        //if not then checks if the amount of users concurrent allows this user to login
                        if (CommandMain.GetInstance().connectedUsers.Count + 1 > concurrentUsersLimit)
                        {
                            response.Result = "Error";
                            response.Reason = "You have exceeded the limit of users allowed!";
                        }
                        else
                            CommandMain.GetInstance().connectedUsers.TryAdd(login.UserName.ToString(), login.UserName.ToString());
                    }
                }
                response.UserName = login.UserName;
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in validateLogin", this, ex);
            }
            return response;
        }

        private int getMaxUsersConcurrent()
        {
//            var licenseFullPath =
//                System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) +
//                "\\license.lic";
//            var manager = new LicenseAPI.Manager();
//            var licDef = manager.ReadLicense(licenseFullPath);
//            return licDef != null ? licDef.MaxUsers : 0;
            return 100;
        }

        private dynamic getUserData(int userID, List<dynamic> deviceList)
        {
            dynamic result = new ExpandoObject();
            result.RequestID = this.RequestID;
            result.Command = this.Command;
            result.Entity = this.Entity;

            var DeviceList = new List<dynamic>();

            var sites = DataManager.Instance.GetUserSites(userID);

            foreach (var device in deviceList)
            {
//                dynamic deviceStatus = Common.DeviceApiManager.Status(device.SerialNumber.ToString());
//                if (deviceStatus == null || deviceStatus.IsOK == false)
//                {
                 dynamic deviceStatus = DataManager.Instance.GetDevice(Convert.ToInt32(device.DeviceID.ToString()));
//                }
                DeviceList.Add(deviceStatus);
            }

            result.Devices = DeviceList;
            result.Connections = DeviceList.Count;
            result.Sites = sites;
            result.Result = "OK";
            return result;
        }

        #endregion
        #region ICommand
        public ICommand Create(CommandObject args)
        {
            return new ValidateLogin
            {
                RequestID = args.RequestID,
                UserID = args.UserID,
                Command = args.Command,
                Entity = args.Entity,
                JSONObjectArgs = args.Data.Replace(@"\", @"\\"),
            };
        }
        #endregion
     

    }
}
