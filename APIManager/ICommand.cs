﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIManager
{
    public interface ICommand
    {
        string JSONObjectArgs { get; set; }
        string UserID { get; set; }
        string Command { get; set; }
        string Entity { get; set; }
        string RequestID { get; set; }

        dynamic Execute();

    }
}
