﻿using Server.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIManager
{
    interface ICommandFactory
    {
        ICommand Create(CommandObject args);
    }
}
