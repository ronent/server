﻿using Infrastructure.Entities;
using Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    class BL : IBLManager
    {
        public BL() { 
            
        }

        #region IBLManager
        public bool ValidateUser(UserCFRAction user)
        {
            return UserCFRValidate.getInstance().check(user);
        }
        #endregion
    }
}
