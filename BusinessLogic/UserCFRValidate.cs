﻿using Infrastructure;
using Infrastructure.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public class UserCFRValidate 
    {

        private static UserCFRValidate _userCFR;

        private UserCFRValidate() { }

        public static UserCFRValidate getInstance()
        {
            if (_userCFR == null)
                _userCFR = new UserCFRValidate();

            return _userCFR;
        }

        public bool check(UserCFRAction user)
        {
            return DataManager.getInstance().UserCFRCheck(user);
        }


        
    }
}
