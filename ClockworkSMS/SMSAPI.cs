﻿using Clockwork;
using Infrastructure;
using Server.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ClockworkSMS
{
    public class SMSAPI : ISMS
    {
        #region Members
        private Clockwork.API api;
        #endregion
        #region Constructor
        public SMSAPI()
        {
            api = new API("f24a33ea0599129a12de357e5086feb13f670560");
        }
        #endregion
        #region ISMS Members
        public void SendSMS(Server.Infrastructure.SMS.SMSManager.AlarmType alarmType,string serialNumber, int contactID, int countryCode, long mobileNumber, string msgBody, int retryInterval,int sensorTypeID=0)
        {
            try
            {
                SMSResult result = api.Send(new Clockwork.SMS 
                { 
                    To = string.Concat(countryCode,mobileNumber), 
                    Message = msgBody, 
                    From = "Simio" 
                });
                
                if (result.Success)
                {
                    Log4Tech.Log.Instance.Write("SMS Sent to {0}, SMS ID: {1}", this, Log4Tech.Log.LogSeverity.DEBUG, result.SMS.To, result.ID);
                    //if success then update the database sms sent flag
                    if (alarmType == Server.Infrastructure.SMS.SMSManager.AlarmType.Sensor)
                    {
                        DataManager.Instance.UpdateNotificationSMSSent(serialNumber, contactID, result.ID, sensorTypeID);    
                    }
                    else if (alarmType == Server.Infrastructure.SMS.SMSManager.AlarmType.Battery)
                    {
                        DataManager.Instance.UpdateNotificationBatterySMSSent(serialNumber, contactID, result.ID);    
                    }
                }
                else
                {
                    if (retryInterval>0)
                    {
                        Log4Tech.Log.Instance.Write("Retry send SMS to {0} ,Error: {1}", this, Log4Tech.Log.LogSeverity.DEBUG, result.SMS.To, result.ErrorMessage);
                        //Retry send SMS
                        SendSMS(alarmType, serialNumber, contactID, countryCode, mobileNumber, msgBody, --retryInterval);
                    }
                    else
                    {
                        Log4Tech.Log.Instance.Write("Failed sending SMS to {0} , Error: {1}", this, Log4Tech.Log.LogSeverity.DEBUG, result.SMS.To, result.ErrorMessage);
                    }
                }
            }
            catch (APIException ex)
            {
                // You'll get an API exception for errors
                // such as wrong username or password
                Log4Tech.Log.Instance.WriteError("API Exception: ", this, ex);
            }
            catch (WebException ex)
            {
                // Web exceptions mean you couldn't reach the Clockwork server
                Log4Tech.Log.Instance.WriteError("Web Exception: ", this, ex);
            }
            catch (ArgumentException ex)
            {
                // Argument exceptions are thrown for missing parameters,
                // such as forgetting to set the username
                Log4Tech.Log.Instance.WriteError("Argument Exception: ", this, ex);
            }
            catch (Exception ex)
            {
                // Something else went wrong, the error message should help
                Log4Tech.Log.Instance.WriteError("Unknown Exception: ", this, ex);
            }
        }

        public bool CanCreate(string port, string uid)
        {
            return true;
        }
        #endregion
        
    }
}
