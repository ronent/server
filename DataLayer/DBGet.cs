﻿using System;
using System.Linq;
using Infrastructure.Entities;
using Infrastructure;
using System.Collections.Generic;
using System.Data.Common;
using System.Data;
using Infrastructure.Responses;
using System.Dynamic;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Text;
using Base.Sensors.Alarms;
using Base.Sensors.Management;
using Base.Sensors.Types;
using Infrastructure.DAL;
using Server.Infrastructure;

namespace DataLayer
{
    public class DBGet : DBBase
    {
        #region Constructor
        public DBGet(string connString, string provider)
        {
            Init(connString, provider);
        }
        #endregion
        #region public get methods

        public dynamic ValidateLogin(string userName , string password)
        {
            var parameters = new List<DbParameter>();
            dynamic response = new ExpandoObject();
            response.UserName = string.Empty;
            response.FullName = string.Empty;
            response.API_KEY = string.Empty;
            response.Result = "Error";
            //response.UserRole = 5;
            response.UserID = 0;
            try
            {
                parameters.Add(CreateParameter("p_UserName", userName, DbType.String));
                parameters.Add(CreateParameter("p_Password", password, DbType.String));

                var dt = Execute(SPNames.ValidateLogin, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    if (dt.Rows[0][0] != null && Convert.ToInt32(dt.Rows[0][0]) > 0)
                    {
                        //set the response 
                        response.Result = "OK";

                        var userId = Convert.ToInt32(dt.Rows[0]["UserID"]);
                        var customerId = Convert.ToInt32(dt.Rows[0]["customerid"]);
                        var maxStorageAllowed = Convert.ToInt64(dt.Rows[0]["maxStorageAllowed"]);
                        var dataStorageExpiry = Convert.ToInt32(dt.Rows[0]["DataStorageExpiry"]);
                        var timeZoneValue = dt.Rows[0]["timezonevalue"].ToString();

                        response.UserName = userName;
                        response.API_KEY = dt.Rows[0]["API_KEY"].ToString();
                        response.firstName = dt.Rows[0]["firstName"].ToString();
                        response.lastName = dt.Rows[0]["lastName"].ToString();
                        response.UserID = userId;
                        response.isCFR = Convert.ToInt32(dt.Rows[0]["isCfr"]);
                        response.UserRole = Convert.ToInt32(dt.Rows[0]["roleid"]);
                        response.ConcurrentUsersLimit = Convert.ToInt32(dt.Rows[0]["concurrentUsersLimit"]);
                        response.languageCode = dt.Rows[0]["languageCode"].ToString();
                        response.companyName = dt.Rows[0]["companyName"].ToString();

                        int tempCustomer;
                        Common.User2Customer.TryRemove(userId, out tempCustomer);
                        Common.User2Customer.TryAdd(userId, customerId);
                        
                        long tempmaxStorageAllowed;
                        Common.MaxStorageAllowed.TryRemove(customerId, out tempmaxStorageAllowed);
                        Common.MaxStorageAllowed.TryAdd(customerId, maxStorageAllowed);

                        int tempdataStorageExpiry;
                        Common.DataStorageExpiry.TryRemove(customerId, out tempdataStorageExpiry);
                        Common.DataStorageExpiry.TryAdd(customerId, dataStorageExpiry);

                        string tempTimeZone;
                        //if (Common.User2TimeZone.TryGetValue(userId, out tempTimeZone))
                        Common.User2TimeZone.TryRemove(userId, out tempTimeZone);
                        Common.User2TimeZone.TryAdd(userId, timeZoneValue);

                        int offset;
                        DateTime convertedTime;
                        Common.GetUtcOffset(userId, DateTime.Now, out convertedTime, out offset);
                        response.timeZone = offset;

                        //go over the serial number's returned and build the list 
                        //this list will be used for pushing the devices currently connected to the GUI
                        if (dt.Rows[0]["serialnumber"] != DBNull.Value)
                        {
                            response.AllowedDevices = new List<int>();
                            foreach (DataRow row in dt.Rows)
                            {
                                var serialNumber = Convert.ToInt32(row["serialnumber"]);
                                //fill up the devices for this customer
                                Common.SerialNumber2Customer.TryAdd(serialNumber, customerId);
                                //build the allowed devices list for this user
                                response.AllowedDevices.Add(serialNumber);
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in ValidateLogin", "DataLayer", ex);
            }
            return response;
        }

        public dynamic GetAllowedUsers(int serialNumber)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            dynamic response = new System.Dynamic.ExpandoObject();
            response.Result = "Error";
            try
            {
                parameters.Add(CreateParameter("p_SerialNumber", serialNumber, DbType.Int32));
                DataTable dt = Execute(SPNames.GetAllowedUsers, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    //set the response 
                    response.Result = "OK";
                    List<int> Users = new List<int>();
                    foreach (DataRow dr in dt.Rows)
                    {
                        Users.Add(Convert.ToInt32(dr["userid"]));
                    }
                    response.Users = Users;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetAllowedUsers", "DataLayer", ex);
            }
            return response;
        }

        internal List<dynamic> GetDeviceSensorsID(int serialNumber)
        {
            
            var sensors = new List<dynamic>();
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_SerialNumber", serialNumber, DbType.Int32)
                };
                var dt = Execute(SPNames.GetDeviceSensorsID, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        dynamic sensor = new ExpandoObject();
                        sensor.SensorTypeID = Convert.ToInt32(row["SensorTypeID"]);
                        sensor.ID = Convert.ToInt32(row["ID"]);
                        sensor.LastSetupTime = Convert.ToInt32(row["LastSetupTime"]);
                        sensor.SensorName = row["SensorName"].ToString();
                        sensor.DeviceName = row["deviceName"].ToString();
                        sensors.Add(sensor);
                    }
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetDeviceSensorsID", "DataLayer", ex);
            }
            return sensors;
        }

        internal Int32 GetDeviceSensorID(int serialNumber, int sensorTypeID)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            try
            {
                parameters.Add(CreateParameter("p_SerialNumber", serialNumber, DbType.Int32));
                parameters.Add(CreateParameter("p_SensorTypeID", sensorTypeID, DbType.Int32));
                DataTable dt = Execute(SPNames.GetDeviceSensorID, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    return Convert.ToInt32(dt.Rows[0][0]);
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetDeviceSensorID", "DataLayer", ex);

            }
            return 0;
        }

        internal dynamic ReadMKTSensorData(int userId, int sensorId)
        {
            var parameters = new List<DbParameter>();
            try
            {
                parameters.Add(CreateParameter("p_UserID", userId, DbType.Int32));
                parameters.Add(CreateParameter("p_DeviceSensorID", sensorId, DbType.Int32));
                var dt = Execute(SPNames.GetUserAnalytics, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    dynamic result = new ExpandoObject();
                    result.sensor = dt.Rows[0]["sensor"].ToString();
                    result.low = (double)dt.Rows[0]["LowLimit"];
                    result.high = (double)dt.Rows[0]["HighLimit"];
                    result.energy = (double)dt.Rows[0]["ActivationEnergy"];
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in ReadMKTSensorData", "DataLayer", ex);

            }
            return null;
        }

        internal dynamic GetBaseSensor(int familyId)
        {
            dynamic response = new ExpandoObject();
            response.Result = "Error";
            response.Data = new List<dynamic>();
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_FamilyTypeID", familyId, DbType.Int32)
                };
                var dt = Execute(SPNames.GetBaseSensor, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Result = "OK";

                    var listData = new List<dynamic>();

                    foreach (DataRow dataRow in dt.Rows)
                    {
                        dynamic data = new ExpandoObject();
                        data.baseId = Convert.ToInt32(dataRow["BaseSensorID"]);
                        data.baseName = dataRow["name"].ToString();

                        listData.Add(data);
                    }
                    response.Data = listData;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetBaseSensor", this, ex);
            }
            return response;
        }

        internal dynamic GetSensorUnits(int familyId)
        {
            dynamic response = new ExpandoObject();
            response.Result = "Error";
            response.Data = new List<dynamic>();
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_FamilyTypeID", familyId, DbType.Int32)
                };
                var dt = Execute(SPNames.GetSensorUnits, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Result = "OK";

                    var listData = new List<dynamic>();

                    foreach (DataRow dataRow in dt.Rows)
                    {
                        dynamic data = new ExpandoObject();
                        data.unitId = Convert.ToInt32(dataRow["id"]);
                        data.unitName = dataRow["name"].ToString();

                        listData.Add(data);
                    }
                    response.Data = listData;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetSensorUnits", this, ex);
            }
            return response;
        }

        internal dynamic GetDeviceFamilyTypes()
        {
            dynamic response = new ExpandoObject();
            response.Result = "Error";
            response.Data = new List<dynamic>();
            try
            {
                var parameters = new List<DbParameter>();


                var dt = Execute(SPNames.GetDeviceFamilyTypes, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Result = "OK";

                    var listData = new List<dynamic>();

                    foreach (DataRow dataRow in dt.Rows)
                    {
                        dynamic data = new ExpandoObject();
                        data.familyId = Convert.ToInt32(dataRow["id"]);
                        data.familyName = dataRow["name"].ToString();

                        listData.Add(data);
                    }
                    response.Data = listData;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetDeviceFamilyTypes", this, ex);
            }
            return response;
        }

        internal dynamic GetGroups(int userId)
        {
            dynamic response = new ExpandoObject();
            response.Result = "Error";
            response.Data = new List<dynamic>();
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_UserID", userId, DbType.Int32),
                };

                var dt = Execute(SPNames.GetGroups, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Result = "OK";
                    var groupId = 0;
                    var listGroups = new List<dynamic>();
                    dynamic data=new ExpandoObject();

                    foreach (DataRow row in dt.Rows)
                    {
                        var currGroupID = Convert.ToInt32(row["groupId"]);

                        if (groupId != currGroupID)
                        {
                            //if not the first one then add the previous one
                            if (groupId != 0)
                                listGroups.Add(data);
                        
                            groupId = currGroupID;
                            data = new ExpandoObject();
                            data.groupName = row["groupName"].ToString();
                            data.groupId = currGroupID;
                            data.groupMembers = new List<dynamic>();
                        }

                        dynamic groupContact = new ExpandoObject();

                        groupContact.isSystem = Convert.ToInt32(row["isSystem"]);
                        groupContact.contactID = Convert.ToInt32(row["contactId"]);
                        groupContact.contactName = row["name"].ToString();
                        groupContact.userName = row["username"].ToString();
                        groupContact.title = row["title"].ToString();
                        groupContact.mobilePhone = row["PhoneNumber"].ToString();
                        groupContact.email = row["email"].ToString();
                        groupContact.country = new ExpandoObject();
                        groupContact.country.name = "";
                        groupContact.country.code = row["countryCode"].ToString();
                        groupContact.groups = new List<int>();

                        //get all groups related to this contact
                        parameters = new List<DbParameter>
                        {
                            CreateParameter("p_ContactID", Convert.ToInt32(row["contactId"]), DbType.Int32),
                        };
                        var dtGroups = Execute(SPNames.GetGroupsByContactID, parameters);
                        if (dtGroups != null && dtGroups.Rows.Count > 0)
                        {
                            var listGroupsIds = (from DataRow dataRow in dtGroups.Rows select Convert.ToInt32(dataRow[0])).ToList();
                            groupContact.groups = listGroupsIds;
                        }

                        groupContact.workingDays = new ExpandoObject();
                        groupContact.workingDays.sun = Convert.ToInt32(row["WorkingSun"]) == 1;
                        groupContact.workingDays.mon = Convert.ToInt32(row["WorkingMon"]) == 1;
                        groupContact.workingDays.tue = Convert.ToInt32(row["WorkingTue"]) == 1;
                        groupContact.workingDays.wed = Convert.ToInt32(row["WorkingWed"]) == 1;
                        groupContact.workingDays.thu = Convert.ToInt32(row["WorkingThu"]) == 1;
                        groupContact.workingDays.fri = Convert.ToInt32(row["WorkingFri"]) == 1;
                        groupContact.workingDays.sat = Convert.ToInt32(row["WorkingSat"]) == 1;
                        groupContact.workHours = new ExpandoObject();
                        groupContact.workHours.mode = row["workHoursMode"].ToString();
                        groupContact.workHours.from = row["WorkdayStart"].ToString();
                        groupContact.workHours.to = row["WorkdayEnd"].ToString();
                        groupContact.smsResends = Convert.ToInt32(row["smsResends"]);

                        data.groupMembers.Add(groupContact);
                        
                    }
                    listGroups.Add(data);

                    response.Data = listGroups;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetGroups", this, ex);
            }
            return response;
        }

        internal dynamic GetAllContacts(int userId)
        {
            dynamic response = new ExpandoObject();
            response.Result = "Error";
            response.Data = new List<dynamic>();
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_UserID", userId, DbType.Int32),
                };

                var dt = Execute(SPNames.GetAllContacts, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Result = "OK";

                    foreach (DataRow row in dt.Rows)
                    {
                        dynamic data = new ExpandoObject();
                        data.isSystem = Convert.ToInt32(row["isSystem"]);
                        data.contactID = Convert.ToInt32(row["contactId"]);
                        data.contactName = row["name"].ToString();
                        data.userName = row["username"].ToString();
                        data.title = row["title"].ToString();
                        data.mobilePhone = row["PhoneNumber"].ToString();
                        data.email = row["email"].ToString();
                        data.country = new ExpandoObject();
                        data.country.name = "";
                        data.country.code = row["countryCode"].ToString();
                        data.groups = new List<int>();

                        //get all groups related to this contact
                        parameters = new List<DbParameter>
                        {
                            CreateParameter("p_ContactID", Convert.ToInt32(row["contactId"]), DbType.Int32),
                        };
                        var dtGroups = Execute(SPNames.GetGroupsByContactID, parameters);
                        if (dtGroups != null && dtGroups.Rows.Count > 0)
                        {
                            var listGroups = (from DataRow dataRow in dtGroups.Rows select Convert.ToInt32(dataRow[0])).ToList();
                            data.groups = listGroups;
                        }

                        data.workingDays = new ExpandoObject();
                        data.workingDays.sun = Convert.ToInt32(row["WorkingSun"]) == 1;
                        data.workingDays.mon = Convert.ToInt32(row["WorkingMon"]) == 1;
                        data.workingDays.tue = Convert.ToInt32(row["WorkingTue"]) == 1;
                        data.workingDays.wed = Convert.ToInt32(row["WorkingWed"]) == 1;
                        data.workingDays.thu = Convert.ToInt32(row["WorkingThu"]) == 1;
                        data.workingDays.fri = Convert.ToInt32(row["WorkingFri"]) == 1;
                        data.workingDays.sat = Convert.ToInt32(row["WorkingSat"]) == 1;

                        data.workHours = new ExpandoObject();
                        data.workHours.mode = row["workHoursMode"].ToString();
                        data.workHours.from = Convert.ToInt32(row["WorkdayStart"]);
                        data.workHours.to = Convert.ToInt32(row["WorkdayEnd"]);

                        data.smsResends = Convert.ToInt32(row["smsResends"]);

                        response.Data.Add(data);
                    }
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetAllContacts", this, ex);
            }
            return response;
        }

        internal dynamic GetAuditTrailData(int userId, int start, int end)
        {
            dynamic response = new ExpandoObject();
            response.Result = "Error";
            response.Data = new List<dynamic>();
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_UserID", userId, DbType.Int32),
                    CreateParameter("p_start", start, DbType.Int32),
                    CreateParameter("p_end", end, DbType.Int32)
                };

                var dt = Execute(SPNames.GetAuditTrailData, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Result = "OK";

                    foreach (DataRow row in dt.Rows)
                    {
                        dynamic data = new ExpandoObject();
                        data.id = Convert.ToInt32(row["id"]);
                        data.timestamp = row["ActivityTimeStamp"].ToString();
                        data.userName = row["username"].ToString();
                        data.userId = Convert.ToInt32(row["userid"]);
                        data.actionType = row["ActionType"].ToString();
                        data.actionName =row["actionName"].ToString();
                        data.product = row["product"].ToString();
                        data.productSn = row["serialNumber"].ToString();
                        data.additionalInfo = row["additionalInfo"].ToString();

                        response.Data.Add(data);
                    }
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetAuditTrailData", this, ex);
            }
            return response;
        }

        internal dynamic GetMatrix(int userId, int groupLevel)
        {
            dynamic response = new ExpandoObject();
            response.Result = "Error";
            response.Data = new List<dynamic>();
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_UserID", userId, DbType.Int32),
                    CreateParameter("p_groupLevel", groupLevel, DbType.Int32)
                };

                var dt = Execute(SPNames.GetMatrix, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Result = "OK";

                    foreach (DataRow row in dt.Rows)
                    {
                        dynamic data = new ExpandoObject();
                        data.privilege = row["PrivilageName"].ToString();
                        data.comment = row["comment"].ToString();
                        data.allowed = Convert.ToInt32(row["isallowed"]) == 1;
                        data.permissionActionId = Convert.ToInt32(row["PermissionActionID"]);
                        data.permissionGroupId = Convert.ToInt32(row["PermissionGroupID"]);
                        response.Data.Add(data);
                    }
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetMatrix", this, ex);
            }
            return response;
        }

        internal dynamic GetPermissonRoles(int userId)
        {
            dynamic response = new ExpandoObject();
            response.Result = "Error";
            response.Data = new List<dynamic>();
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_UserID", userId, DbType.Int32)
                };

                var dt = Execute(SPNames.GetPermissonRoles, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Result = "OK";

                    foreach (DataRow row in dt.Rows)
                    {
                        dynamic data = new ExpandoObject();
                        data.roleId = Convert.ToInt32(row["id"]);
                        data.roleName = row["name"].ToString();

                        response.Data.Add(data);
                    }
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetPermissonRoles", this, ex);
            }
            return response;
        }

        internal dynamic GetPrivilegesGroups(int userId)
        {
            dynamic response = new ExpandoObject();
            response.Result = "Error";
            response.Data = new List<dynamic>();
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_UserID", userId, DbType.Int32)
                };

                var dt = Execute(SPNames.GetPrivilegesGroups, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Result = "OK";
                    
                    foreach (DataRow row in dt.Rows)
                    {
                        dynamic data = new ExpandoObject();
                        data.groupLevel = Convert.ToInt32(row["groupLevel"]);
                        data.groupName = row["groupName"].ToString();
                        data.readOnly = Convert.ToInt32(row["IsReadOnly"]) == 1;
                        data.groupId = Convert.ToInt32(row["ID"]);

                        response.Data.Add(data);
                    }
                    
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetPrivilegesGroups", this, ex);
            }
            return response;
        }

        internal dynamic GetPasswordExpiry(int userId)
        {
            dynamic response = new ExpandoObject();
            response.Result = "Error";
            response.Data = new ExpandoObject();
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_UserID", userId, DbType.Int32)
                };

                var dt = Execute(SPNames.GetPasswordExpiry, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Result = "OK";
                    response.Data.pwdMinLength = Convert.ToInt32(dt.Rows[0]["pwdMinLength"]);
                    response.Data.pwdExpiry = Convert.ToInt32(dt.Rows[0]["passwordExpiryByDays"]);
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetPasswordExpiry", this, ex);
            }
            return response;
        }

        internal dynamic GetActivity(int userId)
        {
            dynamic response = new ExpandoObject();
            response.Result = "Error";
            response.Data = new ExpandoObject();
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_UserID", userId, DbType.Int32)
                };

                var dt = Execute(SPNames.GetActivity, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Result = "OK";
                    response.Data.activeUser = Convert.ToInt32(dt.Rows[0]["activeUsers"]);
                    response.Data.availableUsers = Convert.ToInt32(dt.Rows[0]["availableUsers"]);
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetActivity", this, ex);
            }
            return response;
        }

        internal dynamic GetCalibrationExpiryReminder(int userId)
        {
            dynamic response = new ExpandoObject();
            response.Result = "Error";
            response.Data = new ExpandoObject();
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_UserID", userId, DbType.Int32)
                };

                var dt = Execute(SPNames.GetCalibrationExpiryReminder, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Result = "OK";
                    response.Data.reminderIsOn = Convert.ToInt32(dt.Rows[0]["reminderIsOn"]);
                    response.Data.reminderMode = dt.Rows[0]["reminderMode"].ToString();
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetCalibrationExpryReminder", this, ex);
            }
            return response;
        }

        internal dynamic GetDefinedSensors(int userId)
        {
           
            dynamic response = new ExpandoObject();
            response.Result = "Error";
            response.Data = new List<dynamic>();
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_UserID", userId, DbType.Int32)
                };

                var dt = Execute(SPNames.GetDefinedSensors, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Result = "OK";

                    var listData = new List<dynamic>();

                    foreach (DataRow dataRow in dt.Rows)
                    {
                        dynamic data = new ExpandoObject();
                        data.sensorId = Convert.ToInt32(dataRow["sensorId"]);
                        data.baseSensor = new ExpandoObject();
                        data.baseSensor.baseName = dataRow["basesensor"].ToString();
                        data.baseSensor.baseId = Convert.ToInt32(dataRow["baseSensorId"]);
                        data.digits = Convert.ToInt32(dataRow["digits"]);
                        data.deviceFamily = new ExpandoObject();
                        data.deviceFamily.familyName = dataRow["familyName"].ToString();
                        data.deviceFamily.familyId = Convert.ToInt32(dataRow["familyId"]);
                        data.output1 = Convert.ToDouble(dataRow["output1"]);
                        data.output2 = Convert.ToDouble(dataRow["output2"]);
                        data.sensorName = dataRow["sensorName"].ToString();
                        data.units = new ExpandoObject();
                        data.units.unitId = Convert.ToInt32(dataRow["unitId"]);
                        data.units.unitName = dataRow["unitName"].ToString();
                        data.real1 = Convert.ToDouble(dataRow["real1"]);
                        data.real2 = Convert.ToDouble(dataRow["real2"]);

                        listData.Add(data);
                    }
                    response.Data = listData;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetDefinedSensors", this, ex);
            }
            return response;
        }

        internal string GetDeviceFirmwareVersion(int serialNumber)
        {
            string query = "SELECT PCBVersion, FirmwareVersion, FirmwareRevision FROM Devices WHERE SerialNumber=" + serialNumber;
            DataTable dt = ExecuteQuery(query);
            if (dt != null && dt.Rows.Count > 0)
            {
                string ver = "", part;
                for (int index = 0; index < 3; index++)
                {
                    if (dt.Rows[0][index] is DBNull)
                        part = "0";
                    else
                        part = dt.Rows[0][index].ToString().Trim();
                    
                    if (index > 0)
                        ver += ".";
                    ver += part;
                }
                return ver;
            }
            return null;
        }

        internal dynamic GetDevicesByCustomer(int userId)
        {
            //create the dynamic object
            dynamic response = new ExpandoObject();
            response.Result = "OK";
            response.Data = new List<dynamic>();
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_UserID", userId, DbType.Int32)
                };

                var dt = Execute(SPNames.GetDevicesBySite, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
//                    var deviceIdTemp = 0;
//                    //go over the rows
//                    foreach (DataRow row in dt.Rows)
//                    {
//                        var deviceId = Convert.ToInt32(row["id"]);
//                        if (deviceIdTemp != deviceId)
//                        {
//                            deviceIdTemp = deviceId;
//                            //fill device additional data from field returned
//                            dynamic res = new ExpandoObject();
//                            //fill first the device basic fields
//                            fillDeviceData(res, row);
//                            //filter the device to pass through the sensors
//                            var rows = dt.Select("id=" + deviceId);
//                            //add the sensors for this device
//                            addSensorsToDevice(res, rows);
//                            //add to the list of dynamic
//                            response.Data.Add(res);
//                        }
//                    }
                    buildDevicesTree(dt,response);
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetDevicesByCustomer", "DataLayer", ex);
                response.Result = "Error";
            }
            return response;
        }

        internal dynamic GetDevicesBySite(int siteID, int userID)
        {
            //create the dynamic object
            dynamic response = new ExpandoObject();
            var parameters = new List<DbParameter>();
           
            response.Result = "OK";
            response.Data = new List<dynamic>();
            //response.Sensors = new List<dynamic>();
            try
            {
                parameters.Add(CreateParameter("p_SiteID", siteID, DbType.String));
                parameters.Add(CreateParameter("p_UserID", userID, DbType.String));

                DataTable dt = Execute(SPNames.GetDevicesBySite, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
//                    //go over the rows
//                    foreach (DataRow row in dt.Rows)
//                    {
//                        //fill device additional data from field returned
//                        dynamic res = new ExpandoObject();
//                        //fill first the device basic fields
//                        fillDeviceData(res, row);
//                        //filter the device to pass through the sensors
//                        var deviceId = Convert.ToInt32(row["id"]);
//                        var rows = dt.Select("id=" + deviceId);
//                        //add the sensors for this device
//                        addSensorsToDevice(res,rows);
//                        //add to the list of dynamic
//                        response.Data.Add(res);
//                    }
                    buildDevicesTree(dt, response);
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetDevicesBySite", "DataLayer", ex);
                response.Result = "Error";
            }
            return response;
        }

        public dynamic GetDevicesByCategory(int categoryID, int userID)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            dynamic response = new ExpandoObject();
            
            response.Result = "OK";
            response.Data = new List<dynamic>();
            //response.Sensors = new List<dynamic>();
            try
            {
                parameters.Add(CreateParameter("p_CategoryID", categoryID, DbType.Int32));

                DataTable dt = Execute(SPNames.GetDevicesByCategory, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
//                    //go over the rows
//                    foreach (DataRow row in dt.Rows)
//                    {
//                        //fill device additional data from field returned
//                        dynamic res = new ExpandoObject();
//                        //fill first the device basic fields
//                        fillDeviceData(res, row);
//                        //filter the device to pass through the sensors
//                        var deviceId = Convert.ToInt32(row["id"]);
//                        var rows = dt.Select("id=" + deviceId);
//                        //add the sensors for this device
//                        addSensorsToDevice(res, rows);
//                        //add to the list of dynamic
//                        response.Data.Add(res);
//                    }
                    buildDevicesTree(dt, response);
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetDevicesByCategory", "DataLayer", ex);
                response.Result = "Error";
            }
            return response;
        }

        public dynamic GetDevice(int serialNumber)
        {
           
            dynamic response = new ExpandoObject();

            response.Result = "OK";
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_SerialNumber", serialNumber, DbType.Int32)
                };

                var dt = Execute(SPNames.GetDevices, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    dynamic res = new ExpandoObject();
                    res.Data = new List<dynamic>();
                    buildDevicesTree(dt, res);
                    response.Device = res.Data[0];
                }
            }
            catch (Exception ex)
            {
                response.Result = "Error";
                Log4Tech.Log.Instance.WriteError("Fail in GetDevice", "DataLayer", ex);
            }
            return response;
        }

       /* public dynamic GetDevices(DeviceIDs deviceids)
        {
            
            dynamic response = new ExpandoObject();

            response.Result = "OK";
            response.Data = new List<dynamic>();
            try
            {
                if (deviceids.Devices!=null && deviceids.Devices.Count > 0)
                {
                    var enu = deviceids.Devices.GetEnumerator();
                    var sb = new StringBuilder();
                    while (enu.MoveNext())
                    {
                        sb.AppendFormat("{0},", enu.Current.DeviceID);
                    }
                    //removes last ','
                    sb = sb.Remove(sb.Length - 1, 1);
                    var parameters = new List<DbParameter>
                    {
                        CreateParameter("p_SerialNumber", sb.ToString(), DbType.Int32)
                    };
                }

                var dt = Execute(SPNames.GetDevices, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    //go over the rows
                    foreach (DataRow row in dt.Rows)
                    {
                        //fill device additional data from field returned
                        dynamic res = new ExpandoObject();
                        //fill first the device basic fields
                        fillDeviceData(res, row);
                        //filter the device to pass through the sensors
                        var deviceId = Convert.ToInt32(row["id"]);
                        var rows = dt.Select("id=" + deviceId);
                        //add the sensors for this device
                        addSensorsToDevice(res, rows);
                        //add to the list of dynamic
                        response.Devices.Add(res);
                    }
                }

               
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetDeviceSensors", "DataLayer", ex);
            }
            return response;
        }*/

        public dynamic GetSiteById(int siteID)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            dynamic response = new ExpandoObject();
            response.Result = "Error";
            response.Name = null;
            try
            {
                string query = "SELECT Name FROM Sites WHERE ID=" + siteID;
                DataTable dt = ExecuteQuery(query);
                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Result = "OK";
                    response.Name = dt.Rows[0][0];
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetSiteById", this, ex);
            }
            return response;
        }

        public dynamic GetUserSites(int userID)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            dynamic response = new ExpandoObject();
            response.Result = "Error";
            response.Data = new List<dynamic>();
            try
            {
                parameters.Add(CreateParameter("p_UserID", userID, DbType.String));

                DataTable dt = Execute(SPNames.GetUserSites, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Result = "OK";
                    foreach (DataRow dr in dt.Rows)
                    {
                        dynamic site = new ExpandoObject();

                        site.ID = Convert.ToInt32(dr["ID"]);
                        site.Name = dr["Name"].ToString();
                        site.ParentID = Convert.ToInt32(dr["ParentID"]);
                        site.Level = Convert.ToInt32(dr["Level"]);
                        site.TreePath = dr["treepath"].ToString();
                       // site.IconName = dr["IconName"].ToString();

                        response.Data.Add(site);
                    }

                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetUserSites", this, ex);
            }
            return response;
        }

        public ListCategories GetCategories()
        {
            try
            {
                DataTable dt = Execute(SPNames.GetCategories);
                if (dt != null && dt.Rows.Count > 0)
                {
                    ListCategories response = new ListCategories();
                    response.Result = "OK";
                    response.CategoryList = new List<Category>();

                    foreach (DataRow dr in dt.Rows)
                    {
                        response.CategoryList.Add(new Category() { ID = Convert.ToInt32(dr["ID"]), Name = dr["Name"].ToString(), });
                    }

                    return response;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetCategories", "DataLayer", ex);
            }
            return new ListCategories() { Result = "Error" };
        }

        public dynamic GetSecureQuestions()
        {
            dynamic response = new System.Dynamic.ExpandoObject();
            response.IsOK = false;
            try
            {
                DataTable dt = Execute(SPNames.GetSecureQuestions);
                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Result = "OK";
                    response.SecureQuestionList = new List<SecureQuestion>();

                    foreach (DataRow dr in dt.Rows)
                    {
                        response.SecureQuestionList.Add(new SecureQuestion() { ID = Convert.ToInt32(dr["ID"]), Name = dr["Name"].ToString(), });
                    }

                    response.IsOK = true;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetSecureQuestions", "DataLayer", ex);
            }
            return response;
        }

        public ResponseSensorLogs GetLastSensorLogs(SingleSensor sensor)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            try
            {
                parameters.Add(CreateParameter("p_SensorID", sensor.SensorID, DbType.Int32));
                DataTable dt = Execute(SPNames.GetLastSensorLog, parameters);

                if (dt != null && dt.Rows.Count > 0)
                {
                    ResponseSensorLogs response = new ResponseSensorLogs();
                    response.Result = "OK";
                    response.Logs = new List<SensorLog>();

                    foreach (DataRow dr in dt.Rows)
                    {
                        response.Logs.Add(new SensorLog()
                        {
                            IsTimeStamp = Convert.ToBoolean(dr["IsTimeStamp"]),
                            SampleTime = Convert.ToInt32(dr["SampleTime"]),
                            TimeStampComment = dr["TimeStampComment"].ToString(),
                            Value = Convert.ToInt32(dr["Value"]),
                        });
                    }

                    return response;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetSensorLogs", "DataLayer", ex);
            }
            return new ResponseSensorLogs() { Result = "Error" };
        }

        public ContactList GetContacts(SingleContact contact)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            try
            {
                /*if (contacts.Contacts!=null && contacts.Contacts.Count > 0)
                {
                    var enu = contacts.Contacts.GetEnumerator();
                    StringBuilder sb = new StringBuilder();
                    while (enu.MoveNext())
                    {
                        sb.AppendFormat("{0},", enu.Current.ContactID);
                    }
                    //removes last ','
                    sb = sb.Remove(sb.Length - 1, 1);

                    parameters.Add(CreateParameter("p_ContactIDs", sb.ToString(), DbType.String));
                }*/

                parameters.Add(CreateParameter("p_ContactID", contact.ContactID, DbType.Int32));

                DataTable dt = Execute(SPNames.GetContacts,parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    ContactList response = new ContactList();
                    response.Result = "OK";
                    response.Contacts = new List<Contact>();

                    foreach (DataRow dr in dt.Rows)
                    {
                        response.Contacts.Add(new Contact()
                        {
                            ContactID = Convert.ToInt32(dr["ID"]),
                            Name = dr["name"].ToString(),
                            Email = dr["email"].ToString(),
                            OutOfOfficeEnd = Convert.ToInt32(dr["OutOfOfficeEnd"]),
                            OutOfOfficeStart = Convert.ToInt32(dr["OutOfOfficeStart"]),
                            Phone = dr["Phone"].ToString(),
                            SMSResendInterval = Convert.ToInt32(dr["SMSResendInterval"]),
                            Title = dr["Title"].ToString(),
                            WeekdayStart = Convert.ToInt32(dr["WeekdayStart"]),
                            WorkdayEnd = dr["WorkdayEnd"].ToString(),
                            WorkdayStart = dr["WorkdayStart"].ToString(),
                        });
                    }

                    return response;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetContacts", this, ex);
            }
            return new ContactList() { Result = "Error" };
        }

        internal dynamic GetUserByUserName(string userName)
        {
            dynamic response = new ExpandoObject();
            response.Result = "Error";

            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_UserName", userName, DbType.String)
                };

                var dt = Execute(SPNames.GetUserByUserName, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Result = "OK";

                    var dr = dt.Rows[0];
                    response.userID = Convert.ToInt32(dr["userid"]);
                    response.UserName = dr["Username"].ToString();
                    response.Email = dr["email"].ToString();
                    response.Token = dr["PasswordResetGUID"].ToString();
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetUserByUserName", "DataLayer", ex);
            }
            return response;
        }

        internal dynamic GetCustomers()
        {
            dynamic response = new ExpandoObject();
            response.Result = "Error";
            try
            {
                var dt = Execute(SPNames.GetCustomers);
                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Result = "OK";
                    var customers = new List<dynamic>();
                    foreach (DataRow row in dt.Rows)
                    {
                        dynamic customer = new ExpandoObject();
                        customer.Id = Convert.ToInt32(row["id"]);
                        customer.Name = row["name"].ToString();
                        customers.Add(customer);
                    }
                    response.Data = customers;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetCustomers", "DataLayer", ex);
            }
            return response;
        }

        internal dynamic GetUserRecover(int userId)
        {
           
            dynamic response = new ExpandoObject();
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_UserID", userId, DbType.Int32)
                };

                var dt = Execute(SPNames.GetUserRecovery, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Result = "OK";
                    var privilages = new List<dynamic>();
                    var dr = dt.Rows[0];

                    dynamic user = new ExpandoObject();
                    user.userID = Convert.ToInt32(dr["userid"]);
                    user.userName = dr["Username"].ToString();
                    user.userEmail = dr["email"].ToString();
                    user.firstName = dr["firstname"].ToString();
                    user.lastName = dr["LastName"].ToString();
                    user.userMobile = dr["mobilenumber"];
                    user.countryCode = dr["countrycode"];
                    user.GUID = dr["PasswordResetGUID"].ToString();

                    foreach (DataRow dataRow in dt.Rows)
                    {
                        dynamic privilage = new ExpandoObject();
                        privilage.site = dataRow["sitename"].ToString();
                        privilage.level = dataRow["privilagelevel"].ToString();
                        privilages.Add(privilage);
                    }

                    user.accessPrivileges = privilages;

                    response.Data = user;

                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetUserRecover", "DataLayer", ex);
            }
            return response;
        }

        internal dynamic CustomerGuidIsValid(string guid)
        {
            dynamic response = new ExpandoObject();
            response.Result = "Error";
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_GUID", guid.Replace("-",""), DbType.String)
                };
                var dt = Execute(SPNames.CustomerGuidIsValid, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    var result = Convert.ToInt32(dt.Rows[0][0]);
                    if (result == 1)
                        response.Result = "OK";
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in CustomerGuidIsValid", "DataLayer", ex);
            }
            return response;
        }

        internal dynamic UserGuidIsValid(string guid)
        {
            dynamic response = new ExpandoObject();
            response.Result = "Error";
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_GUID", guid.Replace("-",""), DbType.String)
                };
                var dt = Execute(SPNames.UserGuidIsValid, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    var result = Convert.ToInt32(dt.Rows[0][0]);
                    if (result==1)
                        response.Result = "OK";
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in UserGuidIsValid", "DataLayer", ex);
            }
            return response;
        }

        internal dynamic GetUser(int userId)
        {
           
            dynamic response = new ExpandoObject();
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_UserID", userId, DbType.Int32)
                };

                var dt = Execute(SPNames.GetUser, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Result = "OK";
                    var privilages = new List<dynamic>();
                    var dr = dt.Rows[0];

                    dynamic user = new ExpandoObject();
                    user.userID = Convert.ToInt32(dr["userid"]);
                    user.userName = dr["Username"].ToString().Length == 0 ? null : dr["Username"].ToString();
                    user.userEmail = dr["email"].ToString();
                    user.firstName = dr["firstname"].ToString();
                    user.lastName = dr["LastName"].ToString();
                    user.userMobile = dr["mobilenumber"];
                    user.countryCode = dr["countrycode"];
                    user.dialCode = dr["dialcode"];
                    user.customerId = Convert.ToInt32(dr["customerid"]);

                    foreach (DataRow dataRow in dt.Rows)
                    {
                        dynamic privilage = new ExpandoObject();
                        privilage.site = dataRow["sitename"].ToString();
                        privilage.level = dataRow["privilagelevel"].ToString();
                        privilages.Add(privilage);
                    }

                    user.accessPrivileges = privilages;

                    response.Data = user;

                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetUsers", "DataLayer", ex);
            }
            return response;
        }

        public dynamic GetUsers(int userId)
        {
            var parameters = new List<DbParameter>();
            dynamic response = new ExpandoObject();
            response.Data = new List<dynamic>();
            try
            {
                parameters.Add(CreateParameter("p_UserID", userId, DbType.Int32));

                var dt = Execute(SPNames.GetUsers, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Result = "OK";
                    response.Users = new List<dynamic>();

                    var users = new List<dynamic>();
                    var privilages = new List<dynamic>();
                    var tempUser = 0;
                    dynamic user = new ExpandoObject();

                    foreach (DataRow dataRow in dt.Rows)
                    {
                        var currUserId = Convert.ToInt32(dataRow["ID"]);
                        
                        dynamic privilege = new ExpandoObject();
                        if (currUserId != tempUser)
                        {
                            tempUser = currUserId;
                            //add the previous user
                            if (privilages.Count > 0)
                            {
                                user.privileges = privilages;
                                privilages = new List<dynamic>();
                                users.Add(user);
                            }
                            //init new user
                            user = new ExpandoObject();
                            user.userId = currUserId;
                            user.userName = dataRow["Username"].ToString().Length == 0
                                ? null
                                : dataRow["Username"].ToString();
                            user.userEmail = dataRow["email"].ToString();
                            user.isAgree = true;
                            user.inactivityTimeout = Convert.ToInt32(dataRow["InactivityTimeout"]);
                            user.roleId = Convert.ToInt32(dataRow["roleid"]);

                            var permissionCount = Convert.ToInt32(dataRow["permissionCount"]);
                            user.profile = permissionCount == 0 ? "ALL" : "SPECIFIC";

                            if (permissionCount == 0)
                            {
                                user.privileges = new ExpandoObject();
                                user.privileges.groupLevel = Convert.ToInt32(dataRow["levelID"]);
                                user.privileges.groupName = dataRow["groupname"].ToString();

                                //privilages.Add(privilege);
                                users.Add(user);
                                continue;
                            }
                        }

                        privilege.site = new ExpandoObject();
                        privilege.site.ID = Convert.ToInt32(dataRow["siteid"]);
                        privilege.site.Name = dataRow["name"].ToString();
                        privilege.site.ParentID = Convert.ToInt32(dataRow["parentid"]);
                        privilege.site.Level = Convert.ToInt32(dataRow["level"]);
                        privilege.site.TreePath = dataRow["treepath"].ToString();
                        privilege.group = new ExpandoObject();
                        privilege.group.groupLevel = Convert.ToInt32(dataRow["levelID"]);
                        privilege.group.groupName = dataRow["groupname"].ToString();

                        privilages.Add(privilege);
                    }

                    if (privilages.Count > 0)
                    {
                        user.privileges = privilages;
                        users.Add(user);
                    }

                    response.Data = users;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetUsers", "DataLayer", ex);
            }
            return response;
        }

        public dynamic GetLanguages()
        {
            dynamic response = new System.Dynamic.ExpandoObject();
            response.IsOK = false;
            try
            {
                DataTable dt = Execute(SPNames.GetLanguages);
                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Result = "OK";
                    response.LanguageList = new List<Language>();

                    foreach (DataRow dr in dt.Rows)
                    {
                        response.LanguageList.Add(new Language() { ID = Convert.ToInt32(dr["ID"]), Name = dr["Name"].ToString(), });
                    }

                    response.IsOK = true;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetLanguages", this, ex);
            }
            return response;
        }

        public dynamic GetGroupPermissions()
        {
            dynamic response = new System.Dynamic.ExpandoObject();
            response.IsOK = false;
            try
            {
                DataTable dt = Execute(SPNames.GetGroupPermissions);
                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Result = "OK";
                    response.GroupList = new List<Group>();

                    foreach (DataRow dr in dt.Rows)
                    {
                        response.GroupList.Add(new Group() { ID = Convert.ToInt32(dr["ID"]), Name = dr["Name"].ToString(), });
                    }
                    response.IsOK = true;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GroupPermissions", this, ex);
            }
            return response;
        }

        internal bool isCommandAllowed(UserCommand cmd)
        {
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_UserID", cmd.UserID, DbType.Int32),
                    CreateParameter("p_Command", cmd.Command, DbType.String),
                    CreateParameter("p_Entity", cmd.Entity, DbType.String),
                    CreateParameter("p_SiteID", cmd.SiteID, DbType.Int32)
                };

                var dt = Execute(SPNames.IsCommandAllowed, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    if (dt.Rows[0][0].ToString()=="1")
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in isCommandAllowed", this, ex);
            }
            return false;
        }

        internal dynamic GetLastSensorDownload(int serialNumber, int sensorTypeID)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            dynamic response = new System.Dynamic.ExpandoObject();
            response.IsOK = false;
            try
            {
                parameters.Add(CreateParameter("p_SerialNumber", serialNumber, DbType.Int32));
                parameters.Add(CreateParameter("p_SensorTypeID", sensorTypeID, DbType.Int32));

                DataTable dt = Execute(SPNames.GetLastSensorDownload, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    response.FirstDownloadTime = Convert.ToInt32(dt.Rows[0]["FirstDownloadTime"]);
                    response.LastDownloadTime = Convert.ToInt32(dt.Rows[0]["LastDownloadTime"]);
                    response.IsOK = true;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetLastSensorDownload", this, ex);
            }
            return response;
        }

        internal int GetNextSerialNumber(int AllocateNumber)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            try
            {
                parameters.Add(CreateParameter("p_AllocateNumber", AllocateNumber, DbType.Int32));

                DataTable dt = Execute(SPNames.GetNextSerialNumber, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    return Convert.ToInt32(dt.Rows[0][0]);
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetNextSerialNumber",this, ex);
            }
            return 0;
        }

        internal dynamic PasswordRecovery(dynamic email)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            dynamic response = new System.Dynamic.ExpandoObject();
            response.IsOK = false;
            response.Result = "";
            try
            {
                parameters.Add(CreateParameter("p_Email", email.Email, DbType.String));

                DataTable dt = Execute(SPNames.PasswordRecovery, parameters);
                if (dt != null && dt.Rows.Count > 0 && Convert.ToInt32(dt.Rows[0][0])>0)
                {
                    response.SecureQuestionID = Convert.ToInt32(dt.Rows[0][0]);
                    response.Question = dt.Rows[0]["question"].ToString();
                    response.Result = "OK";
                    response.IsOK = true;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in PasswordRecovery", this, ex);
            }
            return response;
        }

        internal dynamic ResetPassword(dynamic reset)
        {
            dynamic response = new ExpandoObject();
            response.Result = "Error";
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_NewPassword", reset.userPassword.ToString(),DbType.String),
                    CreateParameter("p_GuidReset", reset.userToken.ToString(), DbType.String)
                };

                var dt = Execute(SPNames.ResetPassword, parameters);
                if (dt != null && dt.Rows.Count > 0)
                    response.Result = Convert.ToInt32(dt.Rows[0][0]) == 1 ? "OK" : dt.Rows[0][0].ToString();
            }
            catch(Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in ResetPassword", this, ex);
            }
            return response;
        }

        internal int GetCountryCallingCode(string countryCode)
        {
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_countryCode", countryCode, DbType.String)
                };

                return Convert.ToInt32(ExecuteScalar(SPNames.GetCountryCallingCode, parameters));
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetCountryCallingCode", this, ex);
            }
            return 0;
        }

        internal dynamic SecureAnswerCheck(dynamic userData)
        {
            dynamic response = new ExpandoObject();
            response.Result = "Error";
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_Email", userData.user.userEmail.ToString(), DbType.String),
                    CreateParameter("p_Answer", "1", DbType.String)
                };
                //parameters.Add(CreateParameter("p_Answer", Encryptor.Instance.getHashSha256(answer.Answer.ToString()), DbType.String));

                var dt = Execute(SPNames.SecurityAnswerCheck, parameters);
                if (dt != null && dt.Rows.Count > 0 && Convert.ToInt32(dt.Rows[0][0]) > 0)
                {
                    response.Result = "OK";
                    response.Token = dt.Rows[0]["token"].ToString();
                    response.UserName = dt.Rows[0]["username"].ToString();
                    response.Email = dt.Rows[0]["email"].ToString();
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in SecureAnswerCheck", this, ex);
            }
            return response;
        }

        internal dynamic GetNotificationContacts(string serialNumber, int sensorTypeID)
        {
           
            dynamic response = new ExpandoObject();
            response.Result = "Error";
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_SerialNumber", serialNumber, DbType.Int32),
                    CreateParameter("p_SensorTypeID", sensorTypeID, DbType.Int32)
                };

                var dt = Execute(SPNames.GetNotificationContacts, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Result = "OK";
                    var contacts = new List<dynamic>();

                    foreach (DataRow row in dt.Rows)
                    {
                        dynamic contact = new ExpandoObject();
                        contact.Email = row["email"].ToString();
                        contact.ReceiveEmail = Convert.ToInt32(row["ReceiveEmail"]);
                        contact.ReceiveSMS = Convert.ToInt32(row["ReceiveSMS"]);
                        contact.SMSResendInterval = Convert.ToInt32(row["SMSResendInterval"]);
                        contact.CountryCode = (row["CountryCode"]);
                        contact.CountryName = row["countryname"].ToString();
                        //contact.AreaCode = int.Parse(row["AreaCode"].ToString());
                        contact.PhoneNumber = Convert.ToInt32(row["PhoneNumber"]);
                        contact.EmailSent = Convert.ToInt32(row["EmailSent"]);
                        contact.SMSSent = Convert.ToInt32(row["SMSSent"]);
                        contact.ContactID = Convert.ToInt32(row["ContactID"]);
                        contact.ReceiveBatteryLow = Convert.ToInt32(row["ReceiveBatteryLow"]);
                        contact.BatteryLowValue = Convert.ToInt32(row["BatteryLowValue"]);
                        contact.BatteryEmailSent = Convert.ToInt32(row["BatteryEmailSent"]);
                        contact.BatterySMSSent = Convert.ToInt32(row["BatterySMSSent"]);
                        
                        contacts.Add(contact);
                    }
                    response.Contacts = contacts;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetNotificationContacts", this, ex);
            }
            return response;
        }

        internal dynamic GetCustomer(string email)
        {
            var parameters = new List<DbParameter>();
            dynamic response = new ExpandoObject();
            response.IsOK = false;
            response.Result = "";
            try
            {
                parameters.Add(CreateParameter("p_Email", email, DbType.String));

                var dt = Execute(SPNames.GetCustomer, parameters);
                if (dt != null && dt.Rows.Count > 0 && Convert.ToInt32(dt.Rows[0][0]) > 0)
                {
                    response.Result = "OK";
                    response.IsOK = true;
                    response.Email = dt.Rows[0]["email"].ToString();
                    response.Name = dt.Rows[0]["name"].ToString();
                    response.CreationDate = Convert.ToDateTime(dt.Rows[0]["creationdate"]);
                    response.IsActive = Convert.ToInt32(dt.Rows[0]["isactive"]);
                    response.SMSCounter = Convert.ToInt32(dt.Rows[0]["smscounter"]);
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetCustomer", this, ex);
            }
            return response;
        }

        internal List<int> GetDevicesByUser(string userid)
        {
            var parameters = new List<DbParameter> {CreateParameter("p_UserID", userid, DbType.Int32)};
            var usersList = new List<int>();
            var dt = Execute(SPNames.GetDevicesByUser, parameters);
            if (dt != null && dt.Rows.Count > 0)
            {
                usersList.AddRange(from DataRow row in dt.Rows select Convert.ToInt32(row["serialnumber"].ToString()));
            }
            return usersList;
        }

        internal dynamic GetDevicesByUser(int userID)
        {
            var parameters = new List<DbParameter>();
            dynamic response = new ExpandoObject();
            response.Data = new List<dynamic>();
            //response.Devices = new List<dynamic>();
            response.Result = "OK";
            try
            {
                parameters.Add(CreateParameter("p_UserID", userID, DbType.Int32));
                var dt = Execute(SPNames.GetDevicesByUser, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
//                    var devices = new List<dynamic>();
//                    //go over the rows
//                    foreach (DataRow row in dt.Rows)
//                    {
//                        //fill device additional data from field returned
//                        dynamic res = new ExpandoObject();
//                        //fill first the device basic fields
//                        fillDeviceData(res, row);
//                        //filter the device to pass through the sensors
//                        var deviceId = Convert.ToInt32(row["id"]);
//                        var rows = dt.Select("id=" + deviceId);
//                        //add the sensors for this device
//                        addSensorsToDevice(res, rows);
//                        //add to the list of dynamic
//                        devices.Add(res);
//                    }
//                    response.Data = devices;
                    buildDevicesTree(dt,response);
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetDevicesByUser", "DataLayer", ex);
            }
            
            return response;
        }

        internal DateTime GetLastStoredSampleTime(int serialNumber)
        {
            string query = "SELECT LastStoredSampleTime FROM Devices WHERE SerialNumber=" + serialNumber;
            try
            {
                DataTable table = ExecuteQuery(query);
                if (table != null && table.Rows.Count > 0)
                    return (DateTime)table.Rows[0][0];
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetLastStoredSampleTime", this, ex);
            }
            return DateTime.MinValue;
        }
        
        internal void SetLastStoredSampleTime(int serialNumber, DateTime dt)
        {
            if (dt != DateTime.MinValue)
            {
                string query = "UPDATE Devices SET LastStoredSampleTime='" + dt.ToString("yyyy-MM-dd HH:mm:ss") + "' WHERE SerialNumber=" + serialNumber;
                ExecuteQuery(query);
            }
        }

        internal int[] AllAlarmedDevicesSerialNumbers(int userId)
        {
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_UserID", userId, DbType.Int32)
                };
                var dt = Execute(SPNames.GetAllAlarmedDevicesSerialNumbers, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    var serialNumbers = new int[dt.Rows.Count];
                    for (var index = 0; index < dt.Rows.Count; index++)
                        serialNumbers[index] = (int)dt.Rows[index][0];
                    return serialNumbers;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in AllAlarmedDevicesSerialNumbers", this, ex);
            }
            return null;
        }

        internal int DeviceSerialNumberByAlarmID(int alarm_id)
        {
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_alarmId", alarm_id, DbType.Int32)
                };
                var dt = Execute(SPNames.DeviceSerialNumberByAlarmID, parameters);
                if (dt != null && dt.Rows.Count > 0 && dt.Columns.Count == 1)
                    return (int)dt.Rows[0][0];
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in DeviceSerialNumberByAlarmID", this, ex);
            }
            return 0;
        }

        internal bool GetSensorLocatorByID(int deviceSensorID, out int serialNumber, out int sensorTypeID)
        {
            serialNumber = sensorTypeID = 0;
            var parameters = new List<DbParameter>();
            try
            {
                parameters.Add(CreateParameter("p_DeviceSensorID", deviceSensorID, DbType.Int32));
                var dt = Execute(SPNames.GetSensorLocatorByID, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    serialNumber = Convert.ToInt32(dt.Rows[0]["SerialNumber"].ToString());
                    sensorTypeID = Convert.ToInt32(dt.Rows[0]["SensorTypeID"].ToString());
                    return true;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetSensorLocatorByID", this, ex);
            }
            return false;
        }

        internal dynamic GetAnalyticsSensors(int userid)
        {
            var parameters = new List<DbParameter>();
            try
            {
                dynamic result = new ExpandoObject();
                result.Sensors = new Array[0];

                parameters.Add(CreateParameter("p_UserID", userid, DbType.Int32));

                var dt = Execute(SPNames.GetAnalyticsSensors, parameters);
                result.Sensors = new ExpandoObject[dt.Rows.Count];
                for (var index = 0; index < dt.Rows.Count; index++)
                {
                    dynamic sensor = new ExpandoObject();
                    sensor.Index = (index + 1);
                    sensor.ID = (int)dt.Rows[index][0];
                    sensor.isActive = (bool)dt.Rows[index][1];
                    sensor.LoggerName = (string)dt.Rows[index][2];
                    sensor.Measurement = (string)dt.Rows[index][3];
                    sensor.MeasurementUnit = (string)dt.Rows[index][4];
                    sensor.serialNumber = (int)dt.Rows[index][5];
                    sensor.color = Common.DeviceApiManager.GetSensorColor(index);
                    
                    var mName = Common.MeasurementNameBySensorTypeID((int)dt.Rows[index][6]);
                    sensor.measurementName = mName; // +" (" + sensor.MeasurementUnit + ")";
                    result.Sensors[index] = sensor;
                }
                return result;
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetAnalyticsSensors", this, ex);
            }
            return null;
        }

        internal dynamic GetReportsReviews(int userId, int reportType)
        {
            var parameters = new List<DbParameter>();
            dynamic response = new ExpandoObject();
            try
            {
                parameters.Add(CreateParameter("p_UserID", userId, DbType.Int32));
                parameters.Add(CreateParameter("p_ReportType", reportType, DbType.Int32));
                var reports = new List<dynamic>();

                var dt = Execute(SPNames.GetReportsReviews, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        dynamic report = new ExpandoObject();
                        report.id = Convert.ToInt32(row["ID"].ToString());
                        report.createdBy = row["generatedby"].ToString();
                        report.deliveryDate = Convert.ToInt64(row["CreationDate"]) * 1000;
                        report.reportProfileName = row["profilename"].ToString();
                        report.actionRequired = row["processType"].ToString();

                        var serialNumber = Convert.ToInt32(row["SerialNumber"]);
                        var keyPath = string.Format("{0}/{1}/{2}.pdf", userId, serialNumber, row["CreationDate"]);
                        report.pdf = S3Manager.Instance.GetReportPdfUrl(keyPath);

                        reports.Add(report);
                    }
                }
                response.Data = reports;
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetReportsArchive", this, ex);
            }
            return response;
        }

        internal dynamic GetReportsArchive(int userId,int start, int end)
        {
            var parameters = new List<DbParameter>();
            dynamic response = new ExpandoObject();
            try
            {
                parameters.Add(CreateParameter("p_UserID", userId, DbType.Int32));
                parameters.Add(CreateParameter("p_ReportStartDate", start, DbType.Int32));
                parameters.Add(CreateParameter("p_ReportEndDate", end, DbType.Int32));
                var reports = new List<dynamic>();

                var dt = Execute(SPNames.GetReportsArchive, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        dynamic report = new ExpandoObject();
                        var profileId = Convert.ToInt32(row["ID"]);
                        report.id = profileId;
                        report.createdBy = row["generatedby"].ToString();
                        report.deliveryDate = Convert.ToInt64(row["CreationDate"])*1000;
                        report.profileName = row["profilename"].ToString();
                        
                        var keyPath =  row["s3url"].ToString();
                        report.pdf = S3Manager.Instance.GetReportPdfUrl(keyPath);

                        reports.Add(report);
                    }
                }
                response.Data = reports;
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetReportsArchive", this, ex);
            }
            return response;
        }

        internal dynamic GetAlarmNotificationsBySerialNumber(int serialNumber)
        {
            dynamic response = new ExpandoObject();
            response.Result = "";
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_serialNumber", serialNumber, DbType.Int32)
                };
                var alarms = new List<dynamic>();

                var dt = Execute(SPNames.GetAlarmNotificationsBySerialNumber, parameters);
                if (dt != null && dt.Rows.Count > 0 /*&& Convert.ToInt32(dt.Rows[0][0]) > 0*/)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        dynamic alarm = new ExpandoObject();
                        alarm.AlarmID = row["ID"].ToString();
                        alarm.Name = row["name"].ToString();
                        alarm.Value = Convert.ToDouble(row["Value"]);

                        if (!(row["starttime"] is DBNull))
                        {
                            int start_time = (int)row["starttime"];
                            alarm.StartTime = (UInt64)start_time * 1000;
                        }

                        if (!(row["cleartime"] is DBNull))
                        {
                            var clear_time = (int)row["cleartime"];
                            alarm.ClearTime = (UInt64)clear_time * 1000;
                        }

                        alarm.AlarmReason = row["alarmreason"].ToString();
                        alarm.AlarmType = row["alarmtype"].ToString();
                        alarm.AlarmTypeID = row["AlarmTypeID"].ToString();
                        alarm.DeviceSensorID = row["DeviceSensorID"].ToString();
                        alarm.SerialNumber = row["SerialNumber"].ToString();
                        alarm.SensorName = row["sensorName"].ToString();

                        alarms.Add(alarm);
                    }
                }
                response.Alarms = alarms;
                response.Result = "OK";
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetAlarmNotificationsBySerialNumber", this, ex);
            }
            return response;
        }

        internal dynamic GetAlarmNotifications(int userid)
        {
           
            dynamic response = new ExpandoObject();
            response.Result = "";
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_UserID", userid, DbType.Int32)
                };
                var alarms = new List<dynamic>();

                var dt = Execute(SPNames.GetAlarmNotifications, parameters);
                if (dt != null && dt.Rows.Count > 0 /*&& Convert.ToInt32(dt.Rows[0][0]) > 0*/)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        dynamic alarm = new ExpandoObject();
                        alarm.AlarmID = row["ID"].ToString();
                        alarm.Name = row["name"].ToString();
                        alarm.Value = Convert.ToDouble(row["Value"]);

                        if (!(row["starttime"] is DBNull))
                        {
                            int start_time = (int)row["starttime"];
                            alarm.StartTime = (UInt64)start_time * 1000;
                        }

                        if (!(row["cleartime"] is DBNull))
                        {
                            var clear_time = (int)row["cleartime"];
                            alarm.ClearTime = (UInt64)clear_time * 1000;
                        }
                        
                        alarm.AlarmReason = row["alarmreason"].ToString();
                        alarm.AlarmType = row["alarmtype"].ToString();
                        alarm.AlarmTypeID = row["AlarmTypeID"].ToString();
                        alarm.DeviceSensorID = row["DeviceSensorID"].ToString();
                        alarm.SerialNumber = row["SerialNumber"].ToString();
                        alarm.SensorName = row["sensorName"].ToString();

                        alarms.Add(alarm);
                    }
                }
                response.Alarms = alarms;
                response.Result = "OK";
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetAlarmNotifications", this, ex);
            }
            return response;
        }

        internal dynamic GetDeviceAlarmNotifications(int contactId)
        {
            dynamic response = new ExpandoObject();
            response.Result = "Error";
            response.Data = new List<dynamic>();
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_ContactID", contactId, DbType.Int32),
                };

                var dt = Execute(SPNames.GetDeviceAlarmNotifications, parameters);
                response.Data = new List<dynamic>();
                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Result = "OK";
                    var list = new List<dynamic>();

                    foreach (DataRow dataRow in dt.Rows)
                    {
                        dynamic data = new ExpandoObject();
                        data.DeviceID = dataRow["deviceId"];
                        data.DeviceSensorID = dataRow["DeviceSensorID"];
                        data.ReceiveEmail = dataRow["ReceiveEmail"];
                        data.ReceiveSMS = dataRow["ReceiveSMS"];

                        list.Add(data);
                    }

                    response.Data = list;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetDeviceAlarmNotifications", this, ex);
            }
            return response;
        }

        internal dynamic GetDeviceSetupFiles(int userId, int deviceTypeId =0)
        {
           
            dynamic response = new ExpandoObject();
            response.Result = "Error";
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_UserID", userId, DbType.Int32),
                    CreateParameter("p_DeviceTypeID", deviceTypeId, DbType.Int32)
                };

                var dt = Execute(SPNames.GetDeviceSetupFiles, parameters);
                response.Data = new List<dynamic>();
                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Result = "OK";
                    var list = new List<dynamic>();

                    foreach (DataRow dataRow in dt.Rows)
                    {
                        dynamic data = new ExpandoObject();
                        data.deviceModel = dataRow["devicetype"];
                        data.id = dataRow["SetupID"];
                        data.name = dataRow["SetupName"];
                   
                        list.Add(data);
                    }
                    
                    response.Data = list;

                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetDeviceSetupFiles", this, ex);
            }
            return response;
        }

        internal dynamic GetCalibrationCertificateDefault(int userId)
        {
            dynamic response = new ExpandoObject();
            response.Result = "Error";
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_UserID", userId, DbType.Int32),
                };

                var dt = Execute(SPNames.GetCalibrationCertificateDefault, parameters);

                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Result = "OK";

                    var dataRow = dt.Rows[0];

                    dynamic data = new ExpandoObject();
                    data.humidity = Convert.ToDouble(dataRow["humidity"]);
                    data.includeInReport = Convert.ToInt32(dataRow["includeinreport"]) == 0;
                    data.manufacturer = dataRow["manufacture"].ToString();
                    data.model = dataRow["model"].ToString();
                    data.nist = Convert.ToInt32(dataRow["nist"])==0;
                    data.productName = dataRow["productname"].ToString();
                    data.serialNumber = Convert.ToInt32(dataRow["serialnumber"]);
                    data.temperature = Convert.ToDouble(dataRow["temperature"]);

                    response.Data = data;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetCalibrationCertificateDefault", this, ex);
            }
            return response;
        }

        internal dynamic IsDeviceAutoSetup(int serialNumber)
        {
            dynamic response = new ExpandoObject();
            response.Result = "Error";
            response.SetupName = string.Empty;
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_SerialNumber", serialNumber, DbType.Int32)
                };
                var dt = Execute(SPNames.IsDeviceAutoSetup, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Result = "OK";
                    response.SetupName = dt.Rows[0]["setupname"].ToString();
                    response.CustomerID = Convert.ToInt32(dt.Rows[0]["customerid"]);
                    response.DeviceType = dt.Rows[0]["devicetype"].ToString();
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in IsDeviceAutoSetup", this, ex);
            }
            return response;
        }

        internal dynamic GetDeviceAutoSetup(int userId)
        {
            
            dynamic response = new ExpandoObject();
            response.Result = "Error";
            response.Data = new List<dynamic>();
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_UserID", userId, DbType.Int32)
                };

                var dt = Execute(SPNames.GetDeviceAutoSetup, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Result = "OK";

                    var listData = new List<dynamic>();

                    foreach (DataRow dataRow in dt.Rows)
                    {
                        dynamic data = new ExpandoObject();
                        data.autoSetup = Convert.ToInt32(dataRow["isautosetup"]) == 1;
                        data.deviceModel = dataRow["deviceType"].ToString();
                        data.fileID = dataRow["setupid"].ToString();
                        data.usageCounter = Convert.ToInt32(dataRow["usagecounter"]);

                        var deviceTypeId = Convert.ToInt32(dataRow["devicetypeid"]);
                        //get the list of device setup files by device type and customer id
                        var setupFiles = GetDeviceSetupFiles(userId, deviceTypeId);
                        data.files = new ExpandoObject();
                        if (setupFiles != null && setupFiles.Data != null)
                        {
                            var listFiles = new List<dynamic>();
                            foreach (var fileData in setupFiles.Data)
                            {
                                dynamic file = new ExpandoObject();
                                file.ID = fileData.id;
                                file.Name = fileData.name;

                                listFiles.Add(file);
                            }
                            data.files = listFiles;
                        }
                        listData.Add(data);
                    }
                    response.Data = listData;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetDeviceAutoSetup", this, ex);
            }
            return response;
        }

        internal dynamic GetSystemSettings(int userId)
        {
            var parameters = new List<DbParameter>();
            dynamic response = new ExpandoObject();
            response.Result = "Error";
            try
            {
                parameters.Add(CreateParameter("p_UserID", userId, DbType.Int32));

                var dt = Execute(SPNames.GetSystemSettings, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Result = "OK";

                    dynamic data = new ExpandoObject();
                    data.isCfrMode = Convert.ToInt32(dt.Rows[0]["isCFREnabled"]);
                    data.CelsiusMode = Convert.ToInt32(dt.Rows[0]["TemperatureUnit"]);
                    data.debugMode = Convert.ToInt32(dt.Rows[0]["debugMode"]);
                    data.language = dt.Rows[0]["languageCode"].ToString();
                    data.timezone = new ExpandoObject();
                    data.timezone.value = dt.Rows[0]["timezonevalue"].ToString();
                    data.timezone.abbr = dt.Rows[0]["timezoneabbr"].ToString();
                    data.timezone.offset = Convert.ToInt32(dt.Rows[0]["timezoneoffset"]);
                    data.timezone.isdst = Convert.ToInt32(dt.Rows[0]["timezoneisdst"]);
                    data.timezone.text = dt.Rows[0]["timezonetext"].ToString();

                    response.Data = data;

                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetSystemSettings", this, ex);
            }
            return response;
        }

        internal dynamic GetCustomerBalance(int userId)
        {
            var parameters = new List<DbParameter>();
            dynamic response = new ExpandoObject();
            response.Result = "Error";
            try
            {
                parameters.Add(CreateParameter("p_UserID", userId, DbType.Int32));

                var dt = Execute(SPNames.GetCustomerBalance, parameters);
                if (dt != null && dt.Rows.Count > 0 )
                {
                    response.Result = "OK";

                    dynamic data = new ExpandoObject();
                    data.currentBundle = "Fourtec";
                    data.systemUsers = new ExpandoObject();
                    data.systemUsers.bundle =Convert.ToInt32(dt.Rows[0]["bundleUsers"]);
                    data.systemUsers.usage = Convert.ToInt32(dt.Rows[0]["bundleUsers"])- Convert.ToInt32(dt.Rows[0]["usageUsers"]);
                    data.cloudStorage = new ExpandoObject();
                    data.cloudStorage.bundle = dt.Rows[0]["bundleStorage"];
                    data.cloudStorage.usage = Convert.ToInt64(dt.Rows[0]["bundleStorage"]) - SampleManager.Instance.RowsCount(Convert.ToInt32(dt.Rows[0]["customerid"]));
                    data.subscriptionTime = new ExpandoObject();
                    data.subscriptionTime.bundle = dt.Rows[0]["bundleExpiry"];
                    data.subscriptionTime.usage = dt.Rows[0]["usageExpiry"];
                    data.CustomerID = Convert.ToInt32(dt.Rows[0]["customerid"]);

                    response.Data = data;

                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetCustomerBalance", this, ex);
            }
            return response;
        }

        internal dynamic GetCustomerStorage(int serialNumber)
        {
            
            dynamic response = new ExpandoObject();
            response.Result = "Error";
            response.DataStorageExpiry = 0;
            response.MaxStorageAllowed = 0;
            response.CustomerID = 0;
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_SerialNumber", serialNumber, DbType.Int32)
                };

                var dt = Execute(SPNames.GetCustomerStorage, parameters);
                if (dt != null && dt.Rows.Count > 0 && Convert.ToInt32(dt.Rows[0][0]) > 0)
                {
                    response.Result = "OK";
                    response.MaxStorageAllowed = Convert.ToInt32(dt.Rows[0]["MaxStorageAllowed"]);
                    response.DataStorageExpiry = Convert.ToInt32(dt.Rows[0]["DataStorageExpiry"]);
                    response.CustomerID = Convert.ToInt32(dt.Rows[0]["customerid"]);
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetCustomerStorage", this, ex);
            }
            return response;
        }

        internal dynamic GetDeviceSensorsDownloaded(int serialNumber)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            dynamic response = new System.Dynamic.ExpandoObject();
            response.IsOK = false;
            response.Result = "";
            try
            {
                parameters.Add(CreateParameter("p_SerialNumber", serialNumber, DbType.Int32));

                DataTable dt = Execute(SPNames.GetAllDeviceSensorsDownloaded, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Result = "OK";
                    response.IsOK = true;

                    List<int> sensorTypes = new List<int>();

                    foreach (DataRow row in dt.Rows)
                    {
                        sensorTypes.Add(Convert.ToInt32(row[0]));
                    }
                    response.SensorTypes = sensorTypes;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetDeviceSensorsDownloaded", this, ex);
            }
            return response;
        }

        internal dynamic GetDefinedSensorsByDevice(int serialNumber)
        {
            dynamic response = new ExpandoObject();
            response.Result = "Error";
            response.DefinedSensors = new List<dynamic>();
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_SerialNumber", serialNumber, DbType.Int32)
                };

                var dt = Execute(SPNames.GetDefinedSensorsByDevice, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Result = "OK";

                    var definedSensors = new List<dynamic>();

                    foreach (DataRow row in dt.Rows)
                    {
                        dynamic definedSensor = new ExpandoObject();
                        definedSensor.devicesensorid = Convert.ToInt32(row["devicesensorid"]);
                        definedSensor.sensortype = row["sensortype"].ToString();
                        definedSensor.sensortypeid = row["sensortypeid"].ToString();
                        definedSensor.DecimalPlaces = row["DecimalPlaces"].ToString();
                        definedSensor.familyName = row["familyName"].ToString();
                        definedSensor.familyId = row["familyId"].ToString();
                        definedSensor.output1 = row["output1"].ToString();
                        definedSensor.output2 = row["output2"].ToString();
                        definedSensor.real1 = row["real1"].ToString();
                        definedSensor.real2 = row["real2"].ToString();
                        definedSensor.sensorName = row["sensorName"].ToString();
                        definedSensor.measurementunit = row["measurementunit"].ToString();
                        definedSensor.unitId = row["unitId"].ToString();
                        definedSensor.siteid = row["siteid"].ToString();
                        definedSensor.IsExternalSensor = row["IsExternalSensor"].ToString();
                        definedSensor.IsAlarmEnabled = row["IsAlarmEnabled"].ToString();
                        definedSensor.LowValue = row["LowValue"].ToString();
                        definedSensor.HighValue = row["HighValue"].ToString();
                        definedSensor.MinValue = row["MinValue"].ToString();
                        definedSensor.MaxValue = row["MaxValue"].ToString();
                        definedSensor.devicetypeid = row["devicetypeid"].ToString();
                        definedSensor.CalibrationExpiry = row["CalibrationExpiry"].ToString();
                        definedSensor.Silent = row["Silent"].ToString();
                        definedSensor.serialNumber = row["serialNumber"].ToString();

                        definedSensors.Add(definedSensor);
                    }
                    response.DefinedSensors = definedSensors;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetDefinedSensorsByDevice", this, ex);
            }
            return response;
        }

        internal dynamic GetCustomerDisableStatus(int serialNumber)
        {
            
            dynamic response = new ExpandoObject();
            response.Result = "Error";
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_SerialNumber", serialNumber, DbType.Int32)
                };

                var dt = Execute(SPNames.GetCustomerDisableStatus, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Result = "OK";

                    response.WasDisabled = Convert.ToInt32(dt.Rows[0]["wasDisabled"]);
                    response.DisableDate = dt.Rows[0]["DisableDate"].ToString();
                    response.MongoDataBackUp = Convert.ToInt32(dt.Rows[0]["MongoDataBackUp"]);
                    response.EmailWasSentToday = Convert.ToInt32(dt.Rows[0]["EmailWasSentToday"]);
                    response.Email = dt.Rows[0]["email"].ToString();
                    response.Name = dt.Rows[0]["name"].ToString();
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetCustomerDisableStatus", this, ex);
            }
            return response;
        }

        internal int GetDeviceTimeZone(int serialNumber)
        {
            try
            {
                string query = "SELECT LoggerUTC FROM DeviceMicroX JOIN Devices ON Devices.ID=DeviceMicroX.DeviceID WHERE SerialNumber=" + serialNumber;
                DataTable table = ExecuteQuery(query);
                if (table != null && table.Rows.Count > 0)
                    return (int)table.Rows[0][0];
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetDeviceTimeZone", this, ex);
            }
            return 0;
        }

        internal dynamic GetAllCustomerSerialNumbers(int serialNumber)
        {
            var parameters = new List<DbParameter>();
            dynamic response = new ExpandoObject();
            response.IsOK = false;
            response.Result = "";
            try
            {
                parameters.Add(CreateParameter("p_SerialNumber", serialNumber, DbType.Int32));

                DataTable dt = Execute(SPNames.GetAllSerialNumbers, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Result = "OK";
                    response.IsOK = true;

                    List<int> serialNumbers = new List<int>();

                    foreach (DataRow row in dt.Rows)
                    {
                        serialNumbers.Add(Convert.ToInt32(row["serialNumber"]));
                    }

                    response.SerialNumbers = serialNumbers;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetAllCustomerSerialNumbers", this, ex);
            }
            return response;
        }

        internal int GetCustomerID(int serialNumber)
        {
            var parameters = new List<DbParameter>();
            try
            {
                parameters.Add(CreateParameter("p_SerialNumber", serialNumber, DbType.Int32));
                return Convert.ToInt32(ExecuteScalar(SPNames.GetCustomerBySerialNumber, parameters));
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetCustomerID", this, ex);
            }
            return 0;
        }

        internal int GetCustomerBySensor(int deviceSensorId)
        {
            var parameters = new List<DbParameter>();
            try
            {
                parameters.Add(CreateParameter("p_DeviceSensorID", deviceSensorId, DbType.Int32));
                return Convert.ToInt32(ExecuteScalar(SPNames.GetCustomerBySensor, parameters));
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetCustomerBySensor", this, ex);
            }
            return 0;
        }

        internal dynamic GetReportEmails(int reportProfileId)
        {
            dynamic response = new ExpandoObject();
            response.Result = "Error";
            response.ReportEmails = new List<dynamic>();
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_reportTemplateId", reportProfileId, DbType.Int32)
                };
                var dt = Execute(SPNames.GetReportEmails, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Result = "OK";
                    var emails = new List<dynamic>();

                    foreach (DataRow row in dt.Rows)
                    {
                        dynamic email = new ExpandoObject();
                        email.Email = row["email"].ToString();
                        email.Name = row["name"].ToString();
                        email.ProcessTypeId = Convert.ToInt32(row["ProcessTypeId"]);
                        email.EmailAsLink = row["emailAsLink"].ToString();
                        email.EmailAsPDF = row["emailAsPdf"].ToString();
                        email.PeriodTypeId = Convert.ToInt32(row["periodTypeId"]);
                        emails.Add(email);
                    }
                    response.ReportEmails = emails;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetReportEmails", this, ex);
            }
            return response;
        }


        internal dynamic GetApprovalsReviewersList(int userId)
        {
            var parameters = new List<DbParameter>();
            dynamic response = new ExpandoObject();
            response.Result = "Error";
            try
            {
                parameters.Add(CreateParameter("p_UserID", userId, DbType.Int32));

                var dt = Execute(SPNames.GetApproversReviewersList, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Result = "OK";

                    var users = new List<dynamic>();

                    foreach (DataRow row in dt.Rows)
                    {
                        dynamic user = new ExpandoObject();
                        user.userId = Convert.ToInt32(row["id"]);
                        user.name = row["name"].ToString();

                        users.Add(user);
                    }

                    response.Data = users;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetApprovalsReviewersList", this, ex);
            }
            return response;
        }

        internal dynamic GetDistributionList(int userId)
        {
            var parameters = new List<DbParameter>();
            dynamic response = new ExpandoObject();
            response.Result = "Error";
            response.Data = new List<dynamic>();
            try
            {
                parameters.Add(CreateParameter("p_UserID", userId, DbType.Int32));

                var dt = Execute(SPNames.GetDistributionList, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Result = "OK";

                    var users = new List<dynamic>();

                    foreach (DataRow row in dt.Rows)
                    {
                        dynamic user = new ExpandoObject();
                        user.userId = Convert.ToInt32(row["id"]);
                        user.name = row["name"].ToString();

                        users.Add(user);
                    }

                    response.Data = users;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetDistributionList", this, ex);
            }
            return response;
        }

        internal dynamic GetReportsForExecution()
        {
            dynamic response = new ExpandoObject();
            response.Result = "Error";
            try
            {
                var dt = Execute(SPNames.GetReportsForExecution);
                if (dt != null)
                {
                    response.Result = "OK";
                    var dataList = new List<dynamic>();
                    foreach (DataRow row in dt.Rows)
                    {
                        var reportTemplateId = Convert.ToInt32(row["id"]);
                        dynamic data = new ExpandoObject();
                        data.ID = reportTemplateId;
                        data.PeriodStart = Convert.ToInt32(row["PeriodStart"]);
                        data.PeriodEnd = Convert.ToInt32(row["PeriodEnd"]);
                        data.ReportHeaderLink = row["ReportHeaderLink"].ToString();
                        data.ReportDateFormat = row["ReportDateFormat"].ToString();
                        data.TimeZoneOffset = Convert.ToInt32(row["TimeZoneOffset"]);
                        data.PageSize = row["PageSize"].ToString();
                        data.TemperatureUnit = Convert.ToInt32(row["TemperatureUnit"]);

                        //get the list of devices related
                        var responseDevices = getReportDeviceNotYetExecuted(reportTemplateId);
                        data.loggers = new List<dynamic>();
                        if (responseDevices != null && responseDevices.Result == "OK")
                        {
                            data.loggers = responseDevices.Data;

                            var responseApprovals = getReportApproversReviewers(reportTemplateId);
                            if (responseApprovals != null && responseApprovals.Approvers != null &&
                                responseApprovals.Reviewers != null)
                            {
                                //get the list of approvers related
                                data.approvers = responseApprovals.Approvers;
                                //get the list of reviewers related
                                data.reviewers = responseApprovals.Reviewers;
                            }
                            var responseContacts = getReportContacts(reportTemplateId);
                            if (responseContacts != null && responseContacts.Contacts != null)
                                //get the list of contacts related
                                data.ccd = responseContacts.Contacts;
                        }
                        dataList.Add(data);
                    }
                    response.Data = dataList;
                }
            }
            catch(Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetDistributionList", this, ex);
            }
            return response;
        }

        internal dynamic GetReportProfiles(int userId, string filter)
        {
            var parameters = new List<DbParameter>();
            dynamic response = new ExpandoObject();
            response.Result = "Error";
            response.Data = new List<dynamic>();
            try
            {
                parameters.Add(CreateParameter("p_UserID", userId, DbType.Int32));
                parameters.Add(CreateParameter("p_Filter", filter, DbType.String));

                var dt = Execute(SPNames.GetReportProfiles, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Result = "OK";
                    var dataList = new List<dynamic>();
                    foreach (DataRow row in dt.Rows)
                    {
                        var reportTemplateId = Convert.ToInt32(row["profileid"]);
                        dynamic data = new ExpandoObject();

                        data.profileId = reportTemplateId;
                        data.createdBy = row["createdby"].ToString();
                        data.name = row["name"].ToString();
                        data.activity = Convert.ToInt32(row["activity"]) == 1;
                        data.description = row["Description"].ToString();
                        data.timezone = new ExpandoObject();
                        data.timezone.abbr = "";
                        data.timezone.isdst = true;
                        data.timezone.offset = Convert.ToInt32(row["offset"]);
                        data.timezone.test = "";
                        data.timezone.value = "";
                        data.pagesize = row["pagesize"].ToString();
                        data.formatDate = row["formatdate"].ToString();
                        data.units = row["units"].ToString();
                        data.header = new ExpandoObject();
                        data.header.imageSrc = "";
                        data.header.imageID = "";
                        data.reportAsLink = Convert.ToInt32(row["reportAsLink"]) == 1;
                        data.reportAsPdf = Convert.ToInt32(row["EmailAsPDF"]) == 1;
                        data.scheduleType = row["scheduletype"].ToString();
                        data.scheduleInfo = new ExpandoObject();
                        var periodTypeId = Convert.ToInt32(row["PeriodTypeID"]);
                        switch (periodTypeId)
                        {
                            case 1://daily
                                var time = Convert.ToDateTime(row["periodtime"]);
                                data.scheduleInfo.hours = time.Hour;
                                data.scheduleInfo.minutes = time.Minute;
                                data.scheduleInfo.period = time.ToString("tt", CultureInfo.InvariantCulture);
                                break;
                            case 2://weekly
                                data.scheduleInfo.day = row["periodday"].ToString();
                                break;
                            case 3://monthly
                                data.scheduleInfo.day = row["periodmonth"].ToString();
                                break;
                            case 4://once
                                data.scheduleInfo.from = Convert.ToInt64( row["periodstart"])*1000;
                                data.scheduleInfo.to = Convert.ToInt64(row["periodend"])*1000;
                                break;
                        }
                        data.creationDate =
                            Convert.ToDouble(Convert.ToDateTime(row["creationdate"]).Subtract(new DateTime(1970, 1, 1, 0, 0, 0)).TotalMilliseconds);
                        //get the list of devices related
                        var resosneDevices = getReportDevices(reportTemplateId);
                        if (resosneDevices != null)
                            data.loggers = resosneDevices.Data;

                        var responseApprovals = getReportApproversReviewers(reportTemplateId);
                        if (responseApprovals != null && responseApprovals.Approvers != null &&
                            responseApprovals.Reviewers != null)
                        {
                            //get the list of approvers related
                            data.approvers = responseApprovals.Approvers;
                            //get the list of reviewers related
                            data.reviewers = responseApprovals.Reviewers;
                        }
                        var responseContacts = getReportContacts(reportTemplateId);
                        if (responseContacts != null && responseContacts.Contacts!=null)
                            //get the list of contacts related
                            data.ccd = responseContacts.Contacts;

                        dataList.Add(data);
                    }
                    response.Data = dataList;
                }
            }
            catch
                (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GetDistributionList", this, ex);
            }
            return response;
        }

        #endregion
        #region private dynamic stuff

        private dynamic getReportDeviceNotYetExecuted(int reportTemplateId)
        {
           
            dynamic response = new ExpandoObject();
            response.Result = "Error";
            response.Data = new List<dynamic>();
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_ReportTemplateID", reportTemplateId, DbType.Int32)
                };

                var dt = Execute(SPNames.GetReportDeviceNotYetExecuted, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Result = "OK";

                    var loggers = new List<dynamic>();

                    foreach (DataRow row in dt.Rows)
                    {
                        var serialNumber = Convert.ToInt32(row[0]);
                        dynamic device = GetDevice(serialNumber);
                        loggers.Add(device.Device);
                    }

                    response.Data = loggers;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in getReportDevices", this, ex);
            }
            return response;
        }

        private dynamic getReportContacts(int reportTemplateId)
        {
            var parameters = new List<DbParameter>();
            dynamic response = new ExpandoObject();
            response.Result = "Error";
            response.Contacts = new List<dynamic>();
            try
            {
                parameters.Add(CreateParameter("p_ReportTemplateID", reportTemplateId, DbType.Int32));

                var dt = Execute(SPNames.GetReportContacts, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Result = "OK";

                    var contacts = new List<dynamic>();

                    foreach (DataRow row in dt.Rows)
                    {
                        dynamic contact = new ExpandoObject();
                        contact.userId = Convert.ToInt32(row["id"]);
                        contact.name = row["name"].ToString();
                        contact.selected = true;

                        contacts.Add(contact);
                    }

                    response.Contacts = contacts;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in getReportDevices", this, ex);
            }
            return response;
        }

        private dynamic getReportApproversReviewers(int reportTemplateId)
        {
            var parameters = new List<DbParameter>();
            dynamic response = new ExpandoObject();
            response.Result = "Error";
            response.Approvers = new List<dynamic>();
            response.Reviewers = new List<dynamic>();
            try
            {
                parameters.Add(CreateParameter("p_ReportTemplateID", reportTemplateId, DbType.Int32));

                var dt = Execute(SPNames.GetReportApproversReviewers, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Result = "OK";

                    var approvers = new List<dynamic>();
                    var reviewers = new List<dynamic>();

                    foreach (DataRow row in dt.Rows)
                    {
                        var userId = Convert.ToInt32(row["userid"]);
                        var name = row["username"].ToString();
                        var processTypeId = Convert.ToInt32(row["processtypeid"]);
                        dynamic user = new ExpandoObject();
                        user.userId = userId;
                        user.name = name;
                        user.selected = true;

                        if (processTypeId == 1)
                            reviewers.Add(user);
                        else
                            approvers.Add(user);

                    }

                    response.Approvers = approvers;
                    response.Reviewers = reviewers;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in getReportApproversReviewers", this, ex);
            }
            return response;

        }

        private dynamic getReportDevices(int reportTemplateId)
        {
            var parameters = new List<DbParameter>();
            dynamic response = new ExpandoObject();
            response.Result = "Error";
            response.Data = new List<dynamic>();
            try
            {
                parameters.Add(CreateParameter("p_ReportTemplateID", reportTemplateId, DbType.Int32));

                var dt = Execute(SPNames.GetReportDevices, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Result = "OK";

                    var loggers = new List<dynamic>();

                    foreach (DataRow row in dt.Rows)
                    {
                        var serialNumber = Convert.ToInt32(row[0]);
                        dynamic device = GetDevice(serialNumber);
                        loggers.Add(device.Device);
                    }

                    response.Data = loggers;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in getReportDevices", this, ex);
            }
            return response;
        }

        private void fillDeviceData(dynamic res, DataRow row)
        {
            res.ID = row["id"];
            res.SerialNumber = row["serialnumber"].ToString();
            res.Name = row["name"].ToString().Length == 0 ? "[No name defined]" : row["name"].ToString();
            res.DeviceTypeID = row["devicetypeid"].ToString();
            res.DeviceType = row["devicetype"].ToString();
            res.DeviceTypeName = row["devicetypename"].ToString();
            res.DeviceStatus = row["devicestatus"].ToString();
            res.IsLocked = row["islocked"].ToString();
            res.FirmwareVersion = row["firmwareversion"].ToString();
          //  res.BuildNumber = row["buildnumber"].ToString();
            res.FirmwareUpdateRequired = Convert.ToInt32(row["FirmwareUpdateNeeded"]);
            res.LatestFwVersion = row["fwversion"].ToString();
            res.isFwUpdate = Convert.ToInt32(row["InFwUpdate"])==1;
            res.BatteryLevel = row["batterylevel"].ToString();
            res.IsRunning =  Convert.ToInt16(row["IsRunning"]) == 1;
            res.InCalibrationMode = Convert.ToInt16(row["InCalibrationMode"]) == 1;
            res.LoggerUTC = Convert.ToInt32(row["LoggerUTC"]);
            //res.TimeZone = row["timezone"];
            //gps coordinates
            res.Longitude = row["longitude"];
            res.Latitude = row["latitude"];
            //map coordinates
           // res.IconXCoordinate = row["iconxcoordinate"];
           // res.IconYCoordinate = row["iconycoordinate"];

            res.CelsiusMode = row["CelsiusMode"];
            res.PartNumber = row["PartNumber"];
            //MicroX fields
            res.CyclicMode = row["CyclicMode"];

            //checks if the logger is running then allow only stop
            //if logger not running then allow run and timer run
            var actions = new List<Tuple<int, string>>();
            //checks if the logger is running
            if (Convert.ToInt16(row["IsRunning"]) == 1)
            {
                actions.Add(new Tuple<int, string>(2, "Immediate Stop"));
            }
            else
            {
                actions.Add(new Tuple<int, string>(1, "Immediate Run"));
                actions.Add(new Tuple<int, string>(3, "Timer Run"));
            }
            res.LoggerActionsAllowed = actions;
            res.LoggerActivation = Convert.ToInt16(row["TimerRunEnabled"]) == 0 ? 0 : 3;
            if (Convert.ToInt16(row["TimerRunEnabled"]) == 1)
            {
                //get the timer start
                var timerStart = Convert.ToInt32(row["TimerStart"]);
                //convert to UTC
                var dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Local);
               // Log4Tech.Log.Instance.Write("TimerRun -> Local time {0}", this, Log4Tech.Log.LogSeverity.DEBUG, dtDateTime);
                dtDateTime = dtDateTime.AddSeconds(timerStart).ToLocalTime();
               // Log4Tech.Log.Instance.Write("TimerRun -> After adding seconds of logger {0}", this, Log4Tech.Log.LogSeverity.DEBUG, dtDateTime);

                dtDateTime = dtDateTime.AddHours((-1) * Convert.ToInt32(row["LoggerUTC"]));

               // Log4Tech.Log.Instance.Write("TimerRun -> After convert to UTC {0}", this, Log4Tech.Log.LogSeverity.DEBUG, dtDateTime);
                timerStart = Convert.ToInt32(dtDateTime.Subtract(new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds);
               // Log4Tech.Log.Instance.Write("TimerRun -> After convert to seconds {0}", this, Log4Tech.Log.LogSeverity.DEBUG, timerStart);
                //return in seconds
                res.LoggerTimerStart = timerStart;
            }
            else
                res.LoggerTimerStart = 0;

            //boomerang fields
            res.BoomerangEnabled = row["BoomerangEnabled"];
            res.BoomerangGeneratedBy = row["BoomerangAuthor"];
            res.BoomerangCelsiusMode = row["BoomerangCelsiusMode"];
            //res.BoomerangComment = row["BoomerangComment"];
            res.BoomerangContacts = new List<dynamic>();
            if (row["BoomerangContacts"].ToString().Length > 0)
            {
                var bommerangEmails = row["BoomerangContacts"].ToString().Split(',');
                foreach (var bommerangEmail in bommerangEmails)
                {
                    res.BoomerangContacts.Add(bommerangEmail);
                }
            }
            
            //res.BoomerangDisplayAlarmLevels = row["BoomerangDisplayAlarmLevels"];
            res.BoomerangUTC = row["BoomerangUTC"];
           
            //Logger fields
            res.SampleRateInSec = row["samplerateinsec"];
            //checks if the flag of hide is raised, if not then the device is in alarm
            res.IsInAlarm = Convert.ToInt32(row["CurrentAlarmState"])>0;
            
            res.AlarmDelay = row["alarmdelay"];

            //defined sensors
//            dynamic definedSensorsResult = GetDefinedSensorsByDevice(Convert.ToInt32(row["serialnumber"]));
//            if (definedSensorsResult.IsOK)
//            {
//                res.DefinedSensors = definedSensorsResult.DefinedSensors;
//            }

            if (row["devicetype"].ToString().ToLower().StartsWith("microlite"))
            {
                //microlite fields
                var stopOnKeyPress = Convert.ToInt32(row["microliteStopOnKeyPress"]);
                var pushTuRun = Convert.ToInt32(row["PushToRun"]);
                res.MagnetActivation = getKeyPressActivation(stopOnKeyPress, pushTuRun);
                
                //add only if external microlite 
                if (row["devicetype"].ToString().ToLower().Contains("external") ||
                    row["devicetype"].ToString().ToLower().Contains("voltage") ||
                    row["devicetype"].ToString().ToLower().Contains("current"))
                {
                    res.StopOnCapRemoval = row["microliteStopOnDisconnect"];
                }

                var showAll = Convert.ToInt32(row["microliteShowMinMax"]);
                var showLast24H = Convert.ToInt32(row["microliteShowPast24H"]);

                int showMinMaxOnLCD = 0;

                if (showLast24H == 1)
                    showMinMaxOnLCD = 1;
                else if (showAll == 1)
                    showMinMaxOnLCD = 2;

                res.ShowMinMaxSamplesOnLCD = showMinMaxOnLCD;

                res.SampleAvgPoints = row["microliteAveragePoints"];
                res.LEDConfig = row["microliteLCDConfig"];
                res.EnableLEDOnAlarm = row["microliteEnableLEDOnAlarm"];
                res.MemorySize = row["microliteMemorySize"];
                //res.AlarmDelay = row["microliteAlarmDelay"];
                res.SamplingRateFormat = "HH:mm:ss";
            }
            else if (row["devicetype"].ToString().ToLower().StartsWith("ec8"))
            {
                //MicroLog fields
                var stopOnKeyPress = Convert.ToInt32(row["StopOnKeyPress"]);
                var pushTuRun = Convert.ToInt32(row["PushToRun"]);
                res.KeyPressActivation = getKeyPressActivation(stopOnKeyPress, pushTuRun);

                res.DeepSleepMode = row["DeepSleepMode"];
                res.EnableLEDOnAlarm = row["EnableLEDOnAlarm"];
                res.MemorySize = row["MemorySize"];
                res.StopOnDisconnect = row["StopOnDisconnect"];
                res.SampleAvgPoints = row["sampleavgpoints"];
                res.LEDConfig = row["micrologLCDConfig"];
                //res.AlarmDelay = row["ecAlarmDelay"];
                res.SamplingRateFormat = "HH:mm:ss";
            }
            else if (row["devicetype"].ToString().ToLower().StartsWith("picolite"))
            {
                var stopOnKeyPress = Convert.ToInt32(row["picoStopOnKeyPress"]);
                var pushTuRun = Convert.ToInt32(row["PushToRun"]);

                res.KeyPressActivation = getKeyPressActivation(stopOnKeyPress, pushTuRun);
                //PicoLite fields
                //res.AlarmDelay = row["picoAlarmDelay"];
                //v2 only options
                if (row["devicetype"].ToString() == "PicoLite V2")
                    res.RunDelay = row["RunDelay"];
                //****************

                res.MemorySize = 16000;
                res.SamplingRateFormat = "HH:mm";
                res.SetupTime = row["SetupTime"];
                res.RunTime = row["RunTime"];
            }
            res.SamplingRateMaxValue = ushort.MaxValue;
            res.KeyPressActivationList = getKeyPressActivationList(row["devicetype"].ToString());
           // res.UnitRequiresSetup = row["UnitRequiresSetup"];
            res.SiteID = Convert.ToInt32(row["siteid"]);

            res.NotificationBy = "OFF";
            res.NotificationsIsOn = 0;

            res.Connected = res.DeviceStatus == "connected";
//            if (Common.DeviceApiManager.IsDeviceConnected(row["serialnumber"].ToString(), false))
//                //checks if the device is connected by trying to get it from the device api
//                res.Connected = true;
        }

        private List<Tuple<int, string>> getKeyPressActivationList(string deviceType)
        {
            var actions = new List<Tuple<int, string>>();
            if (deviceType == "PicoLite")
            {
                actions.Add(new Tuple<int, string>(2, "Start only"));
                actions.Add(new Tuple<int, string>(4, "No action"));
            }
            else
            {
                actions.Add(new Tuple<int, string>(1, "Start and Stop"));
                actions.Add(new Tuple<int, string>(2, "Start only"));
                actions.Add(new Tuple<int, string>(3, "Stop only"));
                actions.Add(new Tuple<int, string>(4, "No action"));
            }
            return actions;
        }

        private int getKeyPressActivation(int stopOnKeyPress, int pushTuRun)
        {
            //checks what is the value of key press activation according the stop on key press and push to run
            if (stopOnKeyPress == 1 && pushTuRun == 1)
                return 1;
            if (stopOnKeyPress == 0 && pushTuRun == 1)
                return 2;
            if (stopOnKeyPress == 1 && pushTuRun == 0)
                return 3;
            return 4;
        }

        private dynamic fillSensorData(DataRow row)
        {
            dynamic res = new ExpandoObject();

            res.ID = row["devicesensorid"];
            res.TypeID = row["sensortypeid"];
            res.SensorName = row["sensorname"];
           // res.SiteID = row["SiteID"];
            res.IsExternalSensor = row["IsExternalSensor"];
            res.BaseSensorID = row["baseSensorId"];
            res.SensorType = row["sensortype"];
            res.IsAlarmEnabled = row["IsAlarmEnabled"];
            res.LowValue = row["LowValue"];
            res.HighValue = row["HighValue"];
            res.MinValue = row["MinValue"];
            res.MaxValue = row["MaxValue"];

            var deviceTypeId = Convert.ToInt32(row["devicetypeid"]);
            if (deviceTypeId >= 14 && deviceTypeId <= 16)
            {
                res.PreLowValue = row["PreLowValue"];
                res.PreHighValue = row["PreHighValue"];
            }
            res.CalibrationExpiry = row["CalibrationExpiry"];
            res.Silent = row["Silent"];
            res.MeasurementUnit = row["measurementunit"];

            var mName = Common.MeasurementNameBySensorTypeID((int)row["sensortypeid"]);
            res.MeasurementName = mName;
            res.SensorIndex = row["sensorindex"];
            //res.BaseSensorType = row["sensortypeid"];
            res.DecimalDigits = row["DecimalPlaces"];
            //res.CustomUnit = row["CustomUnitName"];

            //set default of calibration points minimum 2 points
            res.Calibration = new ExpandoObject();
            res.Calibration.Log1 = 0;
            res.Calibration.Ref1 = 0;
            res.Calibration.Log2 = 0;
            res.Calibration.Ref2 = 0;

            //if it is a defined sensor then take the actual values and put in the calibration log/ref values
            if (Convert.ToInt32(row["sensortypeid"]) == (int)eSensorType.UserDefined)
            {
                res.Calibration.Log1 = row["Log1"];
                res.Calibration.Ref1 = row["Ref1"];
                res.Calibration.Log2 = row["Log2"];
                res.Calibration.Ref2 = row["Ref2"];
            }

            res.NotificationsIsOn = 0;

            //only if the device is connected then checks the calibration points
//            var serialNumber = row["serialNumber"].ToString();
//            if (Common.DeviceApiManager.IsDeviceConnected(serialNumber, false))
//            {
//                var sensors = Common.DeviceApiManager.GetSensors(serialNumber);
//
//                if (row["sensortypeid"] != null && sensors != null)
//                {
//                    var sensor = (from x in sensors
//                        where (int) x.Type == (int) row["sensortypeid"]
//                        select x).FirstOrDefault();
//                    if (sensor != null && sensor.Calibration.Points > 2)
//                    {
//                        res.Calibration.Log3 = 0;
//                        res.Calibration.Ref3 = 0;
//                    }
//                }
//            }

            //if the sensor type is digital temperature or device type is picolite or simi then 2 calibation point
            //otherwise 3 calibration point
            if (Convert.ToInt32(row["sensortypeid"]) == 1 || Convert.ToInt32(row["sensortypeid"]) > 4)
            {
                res.Calibration.Log3 = 0;
                res.Calibration.Ref3 = 0;
            }


//            res.LastSampleValue = SampleManager.Instance.LastSampleValue(Convert.ToInt32(row["serialNumber"]),
//                   Convert.ToInt32(row["sensortypeid"]));
            res.LastSampleValue = row["LastSampleValue"];

            return res;
        }

        private void addSensorsToDevice(dynamic res,DataRow[] rows)
        {
            
            try
            {
                //get the sensors for this device
                res.Sensors = new List<dynamic>();
                    foreach (var row in rows)
                    {
                        dynamic resSensors = fillSensorData(row);
                        if (Convert.ToInt32(row["sensortypeid"]) > 2) //only those that are not temperature
                            resSensors.IsActive = row["isEnabled"];
                        //add the device type to the sensor (for calibration use)
                        resSensors.DeviceType = row["devicetype"].ToString();
                        res.Sensors.Add(resSensors);
                    }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in addSensorsToDevice", "DataLayer", ex);
            }
        }

        private void buildDevicesTree(DataTable dt, dynamic response)
        {
            var deviceIdTemp = 0;
            //go over the rows
            foreach (DataRow row in dt.Rows)
            {
                var deviceId = Convert.ToInt32(row["id"]);
                var serialNumber = Convert.ToInt32(row["serialnumber"]);
                if (deviceIdTemp == deviceId) continue;

                deviceIdTemp = deviceId;
                //fill device additional data from field returned
                dynamic res = new ExpandoObject();
                //fill first the device basic fields
                fillDeviceData(res, row);
                //filter the device to pass through the sensors
                var rows = dt.Select("id=" + deviceId);
                //sort the rows by the field of enabled
                rows = rows.OrderByDescending(x => x["isenabled"]).ToArray();
                addSensorsToDevice(res, rows);
                
                //checks if this device type can have external sensor
                var deviceTypeName = row["devicetype"].ToString().ToUpper();
                //add custom sensors that matches to this device type
                if (deviceTypeName.IndexOf("EXTERNAL", StringComparison.Ordinal) >= 0 ||
                    deviceTypeName.IndexOf("EC800", StringComparison.Ordinal) >= 0 ||
                    deviceTypeName.IndexOf("VOLTAGE", StringComparison.Ordinal) >= 0 ||
                    deviceTypeName.IndexOf("CURRENT", StringComparison.Ordinal) >= 0 ||
                    deviceTypeName.IndexOf("EC850", StringComparison.Ordinal) >= 0)
                {
                    //add the sensors for this device
                    var definedSensors = GetDefinedSensorsByDevice(serialNumber);
                    foreach (var definedSensor in definedSensors.DefinedSensors)
                    {
                        var mName = Common.MeasurementNameBySensorTypeID(Convert.ToInt32(definedSensor.sensortypeid));
                        dynamic rowDefinedSensor = new ExpandoObject();
                        rowDefinedSensor.ID = definedSensor.devicesensorid;
                        rowDefinedSensor.TypeID = 9;
                        rowDefinedSensor.SensorName = definedSensor.sensorName;
                        rowDefinedSensor.SensorType = definedSensor.sensortype;
                        rowDefinedSensor.BaseSensorID = definedSensor.sensortypeid;
                        rowDefinedSensor.CalibrationExpiry = definedSensor.CalibrationExpiry;
                        rowDefinedSensor.MeasurementUnit = definedSensor.measurementunit;
                        rowDefinedSensor.MeasurementName = mName;
                        rowDefinedSensor.DecimalDigits = definedSensor.DecimalPlaces;
                        rowDefinedSensor.Calibration = new ExpandoObject();
                        rowDefinedSensor.Calibration.Log1 = definedSensor.output1;
                        rowDefinedSensor.Calibration.Ref1 = definedSensor.real1;
                        rowDefinedSensor.Calibration.Log2 = definedSensor.output2;
                        rowDefinedSensor.Calibration.Ref2 = definedSensor.real2;
                        rowDefinedSensor.NotificationsIsOn = 0;
                        rowDefinedSensor.SiteID =0;
                        rowDefinedSensor.IsExternalSensor = 1;
                        rowDefinedSensor.IsAlarmEnabled = 0;
                        rowDefinedSensor.LowValue = 0;
                        rowDefinedSensor.HighValue = 100;
                        rowDefinedSensor.MinValue = 0;
                        rowDefinedSensor.MaxValue = 100;
                        rowDefinedSensor.Silent =0;
                        rowDefinedSensor.SensorIndex = 9;
                        rowDefinedSensor.LastSampleValue = null;
                        rowDefinedSensor.IsActive = 0;

                        res.Sensors.Add(rowDefinedSensor);
                    }
                }
                //add to the list of dynamic
                response.Data.Add(res);
            }
        }

        #endregion
    }
}
