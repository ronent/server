﻿using Base.Sensors.Management;
using Base.Sensors.Samples;
using Infrastructure;
using Infrastructure.DAL;
using Infrastructure.Entities;
using Infrastructure.Responses;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;
using System.Text;
using Server.Infrastructure;


namespace DataLayer
{
    public class DBInsert : DBBase
    {
        public DBInsert(string connString, string provider)
        {
            Init(connString, provider);
        }
        #region Internal methods
        internal dynamic CreateNewCustomer(dynamic customer)
        {
            dynamic response = new ExpandoObject();
            response.Error = "The system fail to create new customer";
            response.Result = "Error";
            response.email = "";
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_GUID", customer.guid.ToString(), DbType.String),
                    CreateParameter("p_CompanyName", customer.companyName.ToString(), DbType.String),
                    CreateParameter("p_Address", customer.address.ToString(), DbType.String),
                    CreateParameter("p_CountryCode", customer.country.code.ToString(), DbType.String),
                    CreateParameter("p_FirstName", customer.fName.ToString(), DbType.String),
                    CreateParameter("p_LastName", customer.lName.ToString(), DbType.String),
                    CreateParameter("p_Industry", customer.industry.ToString(), DbType.String),
                    CreateParameter("p_MobileNumber", customer.mobile.ToString(), DbType.String),
                    CreateParameter("p_PhoneNumber", customer.phone.ToString(), DbType.String),
                    CreateParameter("p_Prefix", customer.prefix.ToString(), DbType.String),
                    CreateParameter("p_timezonevalue", customer.timezone.value.ToString(), DbType.String),
                    CreateParameter("p_timezoneabbr", customer.timezone.abbr.ToString(), DbType.String),
                    CreateParameter("p_timezoneoffset", Convert.ToInt32(customer.timezone.offset.ToString()),
                        DbType.Int32),
                    CreateParameter("p_timezoneisdst", customer.timezone.isdst.ToString() == "True" ? 1 : 0,
                        DbType.Int32),
                    CreateParameter("p_timezonetext", customer.timezone.text.ToString(), DbType.String),
                    CreateParameter("p_Title", customer.title.ToString(), DbType.String),
                    CreateParameter("p_Username", customer.userName.ToString(), DbType.String),
                    CreateParameter("p_Password", customer.password1.ToString(), DbType.String),
                    CreateParameter("p_DialCode", customer.dialCode.ToString(), DbType.String),
                };

                var dt = Execute(SPNames.CreateNewCustomer, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    var email = dt.Rows[0][0].ToString();
                    response.email = email;
                    if (email.Length > 0)
                    {
                        response.Result = "OK";
                        response.Error = "";
                    }
                }
            }
            catch (SqlException se)
            {
                switch (se.Number)
                {
                    case 2601:
                        response.Error = "Username already exists.";
                        break;
                }
                Log4Tech.Log.Instance.WriteError("Fail in createNewCustomer (SQL Exception)", "DataLayer", se);
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in createNewCustomer", "DataLayer", ex);
                response.Error = ex.Message;
            }
            return response;
        }

        internal bool userCFRCheck(Int32 userid, String command, String entity, Int32 serialNumber = 0, String reason = "")
        {
            Log4Tech.Log.Instance.Write("[Database] userCFRCheck: {0},{1},{2},{3},{4}", "DataLayer", Log4Tech.Log.LogSeverity.INFO,  userid, command, entity, serialNumber, reason);
            var parameters = new List<DbParameter>();
            try
            {
                parameters.Add(CreateParameter("p_UserID", userid, DbType.Int32));
                parameters.Add(CreateParameter("p_Command", command, DbType.String));
                parameters.Add(CreateParameter("p_Entity", entity, DbType.String));
                parameters.Add(CreateParameter("p_SerialNumber", serialNumber, DbType.Int32));
                parameters.Add(CreateParameter("p_Reason", reason, DbType.String));

                DataTable dt = Execute(SPNames.UserCFRCheck, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    if (dt.Rows[0][0] != null && dt.Rows[0][0].ToString().Length > 0)
                    {
                        if (dt.Rows[0][0].ToString() == "1")
                        {
                            return true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in userCFRCheck", this, ex);
            }
            return false;
        }

        internal void AddDeviceSetupFile(dynamic setupFile)
        {
            var parameters = new List<DbParameter>();
            try
            {
                parameters.Add(CreateParameter("p_UserID", setupFile.UserID, DbType.Int32));
                parameters.Add(CreateParameter("p_DeviceType", setupFile.DeviceType, DbType.String));
                parameters.Add(CreateParameter("p_SetupID", setupFile.SetupID, DbType.String));
                parameters.Add(CreateParameter("p_SetupName", setupFile.SetupName, DbType.String));

                Execute(SPNames.AddDeviceSetupFile, parameters);
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in AddDeviceSetupFile", this, ex);
            }
        }

        internal void AddAnalyticsSensor(int userID, int sensorID, int isActive=1)
        {
            var parameters = new List<DbParameter>();
            try
            {
                parameters.Add(CreateParameter("p_UserID", userID, DbType.Int32));
                parameters.Add(CreateParameter("p_DeviceSensorID", sensorID, DbType.Int32));
                parameters.Add(CreateParameter("p_IsActive", isActive, DbType.Int32));

                Execute(SPNames.UpsertAnalyticsSensors, parameters);
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in AddAnalyticsSensor", this, ex);
            }
        }

        internal dynamic SignupCustomer(string email)
        {
            dynamic response = new ExpandoObject();
            response.Result = "Error";
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_CustomerEmail", email, DbType.String),
                };

                var dt = Execute(SPNames.CustomerSignUp, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    var customerId = Convert.ToInt32(dt.Rows[0]["customerid"]);
                    if (customerId < 1)
                    {
                        response.Error = "Email already exists.";
                    }
                    else
                    {
                        response.Result = "OK";
                        response.newCustomerId = customerId;
                        response.guid = dt.Rows[0]["VerificationGUID"].ToString();
                        response.email = email;
                    }
                }
            }
            catch (SqlException se)
            {
                switch (se.Number)
                {
                    case 2601:
                        response.Error = "Email already exists.";
                        break;
                }
                Log4Tech.Log.Instance.WriteError("Sql Exception in CreateUser", this, se);
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in createUser", this, ex);
            }
            return response;
        }

        internal dynamic createUser(int userId, dynamic userData)
        {
            dynamic response = new ExpandoObject();
            response.Result = "Error";
            try
            {
                var email = userData.userEmail.ToString();
                var permissionGroupId = 0;
                if (userData.privileges.GetType().Name != "JArray")
                    //one permission for all
                    permissionGroupId = Convert.ToInt32(userData.privileges.groupId);
               
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_AdminUserID", userId, DbType.String),
                    CreateParameter("p_PermissionRoleID", Convert.ToInt32(userData.roleId.ToString()), DbType.Int32),
                    CreateParameter("p_InactivityTimeout", Convert.ToInt32(userData.inactivityTimeout.ToString()),
                        DbType.Int32),
                    CreateParameter("p_PermissionGroupID", permissionGroupId, DbType.Int32),
                    CreateParameter("p_Email", email, DbType.String),
                };

                var dt = Execute(SPNames.CreateNewUser, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Result = "OK";
                    response.newUserId = Convert.ToInt32(dt.Rows[0]["userid"]);
                    response.guid = dt.Rows[0]["PasswordResetGUID"].ToString();
                    response.email = email;

                    if (permissionGroupId == 0)
                    {
                        foreach (var privilege in userData.privileges)
                        {
                            var siteId = Convert.ToInt32(privilege.site.ID.ToString());
                            permissionGroupId = Convert.ToInt32(privilege.group.groupId);
                            
                            var parametersPermissions = new List<DbParameter>
                            {
                                CreateParameter("p_UserID", Convert.ToInt32(userData.userId.ToString()), DbType.Int32),
                                CreateParameter("p_PermissionGroupID", permissionGroupId, DbType.Int32),
                                CreateParameter("p_SiteID", siteId, DbType.Int32),
                            };
                            Execute(SPNames.AddPermissionGroupSite, parametersPermissions);
                        }
                    }
                }
            }
            catch (SqlException se)
            {
                switch (se.Number)
                {
                    case 2601:
                        response.Error = "Email already exists.";
                        break;
                }
                Log4Tech.Log.Instance.WriteError("Sql Exception in CreateUser", this, se);
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in createUser", this, ex);
            }
            return response;
        }

        

        internal PostBack createNewAdminUser(AdminUser newUser)
        {
            
            try
            {
                Guid guid;
                if (Guid.TryParseExact(newUser.GUID, "D", out guid))
                {
                    var parameters = new List<DbParameter>
                    {
                        CreateParameter("p_GUID", guid, DbType.Guid),
                        CreateParameter("p_UserName", newUser.UserName, DbType.String),
                        CreateParameter("p_UserPassword", Encryptor.Instance.getHashSha256(newUser.Password),
                            DbType.String),
                        CreateParameter("p_Email", newUser.Email.ToString(), DbType.String),
                        CreateParameter("p_QuestionID", newUser.SecureQuestionID, DbType.Int32),
                        CreateParameter("p_Answer", newUser.SecureAnswer, DbType.String)
                    };

                    var dt = Execute(SPNames.CreateAdminUser, parameters);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        if (dt.Rows[0]["userid"] != null && dt.Rows[0]["userid"].ToString().Length > 0)
                        {
                            if (dt.Rows[0]["userid"].ToString() == "0")
                            {
                                return new PostBack() { Result = "Error" };
                            }
                            else
                            {
                                return new PostBack() { Result = "OK", PostBackURL = "/u=" + dt.Rows[0]["userid"].ToString() };
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in createNewAdminUser", "DataLayer", ex);
            }
            return new PostBack() { Result = "Error" };
        }

        internal Response registerLanRcv(LanRcvRegisteration lanRcv)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            try
            {
                parameters.Add(CreateParameter("p_CustomerID", lanRcv.CustomerID, DbType.Int32));
                parameters.Add(CreateParameter("p_UserName", lanRcv.UserName, DbType.String));
                parameters.Add(CreateParameter("p_Password", lanRcv.Password, DbType.String));
                parameters.Add(CreateParameter("p_MacAddress", lanRcv.MacAddress, DbType.String));

                DataTable dt = Execute(SPNames.RegisterLanRcv, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    if (dt.Rows[0][0] != null && dt.Rows[0][0].ToString().Length > 0)
                    {
                        if (dt.Rows[0][0].ToString() == "1")
                        {
                            return new Response() { Result = "OK" };
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in registerLanRcv", "DataLayer", ex);
            }
            return new Response() { Result = "Error" };
        }

        internal dynamic CreateSite(dynamic site)
        {
            var parameters = new List<DbParameter>();
            dynamic response = new ExpandoObject();
            response.IsOK = false;
            response.Result = "Error";
            try
            {
                parameters.Add(CreateParameter("p_UserID", site.UserID, DbType.Int32));
                parameters.Add(CreateParameter("p_Name", site.Name, DbType.String));
                parameters.Add(CreateParameter("p_ParentID", site.ParentID, DbType.Int32));
               // parameters.Add(CreateParameter("p_IconName", site.IconName, DbType.String));

                DataTable dt = Execute(SPNames.CreateNewSite, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    if (dt.Rows[0][0] != null && dt.Rows[0][0].ToString().Length > 0)
                    {
                        if ((int)dt.Rows[0][0] > 0)
                        {
                            response.Result = "OK";
                            response.IsOK = true;
                            response.ID = (int)dt.Rows[0][0];
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in CreateSite", "DataLayer", ex);
            }
            return response;
        }

        internal void InsertSensorDownloadHistory(int serialNumber, int sensorTypeID, int firstDownloadTime, int lastDownloadTime)
        {
            var parameters = new List<DbParameter>();
            try
            {
                parameters.Add(CreateParameter("p_SerialNumber", serialNumber, DbType.Int32));
                parameters.Add(CreateParameter("p_SensorTypeID", sensorTypeID, DbType.Int32));
                parameters.Add(CreateParameter("p_FirstDownloadTime", firstDownloadTime, DbType.Int32));
                parameters.Add(CreateParameter("p_LastDownloadTime", lastDownloadTime, DbType.Int32));

                Execute(SPNames.InsertDownloadHistory, parameters);
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in InsertSensorDownloadHistory", "DataLayer", ex);
            }
        }

        internal void SystemActionsAudit(int userid, string command, string entity, int serialnumber)
        {
            var parameters = new List<DbParameter>();
            try
            {
                parameters.Add(CreateParameter("p_UserID", userid, DbType.Int32));
                parameters.Add(CreateParameter("p_Command", command, DbType.String));
                parameters.Add(CreateParameter("p_Entity", entity, DbType.String));
                parameters.Add(CreateParameter("p_SerialNumber", serialnumber, DbType.String));

                Execute(SPNames.SystemActionsAudit, parameters);
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in CreateContact", "DataLayer", ex);
            }
        }

        internal Response CreateContact(Contact contact)
        {
            var parameters = new List<DbParameter>();
            try
            {
                parameters.Add(CreateParameter("p_UserID", contact.UserID, DbType.Int32));
                parameters.Add(CreateParameter("p_Name", contact.Name, DbType.String));
                parameters.Add(CreateParameter("p_Title", contact.Title, DbType.String));
                parameters.Add(CreateParameter("p_Phone", contact.Phone, DbType.String));
                parameters.Add(CreateParameter("p_Email", contact.Email, DbType.String));
                parameters.Add(CreateParameter("p_WeekdayStart", contact.WeekdayStart, DbType.Int32));
                parameters.Add(CreateParameter("p_WorkdayStart", contact.WorkdayStart, DbType.Time));
                parameters.Add(CreateParameter("p_WorkdayEnd", contact.WorkdayEnd, DbType.Time));
                parameters.Add(CreateParameter("p_SMSResendInterval", contact.SMSResendInterval, DbType.Int32));
               

                DataTable dt = Execute(SPNames.CreateNewContact, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    if (dt.Rows[0][0] != null && dt.Rows[0][0].ToString().Length > 0)
                    {
                        if (dt.Rows[0][0].ToString() == "1")
                        {
                            return new Response() { Result = "OK" };
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in CreateContact", "DataLayer", ex);
            }
            return new Response() { Result = "Error" };
        }

        internal void SaveReport(int serialNumber, string S3Url, int reportProfileId)
        {
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_SerialNumber", serialNumber, DbType.Int32),
                    CreateParameter("p_S3Url", S3Url, DbType.String),
                    CreateParameter("p_profileId", reportProfileId, DbType.Int32)
                };

                Execute(SPNames.SaveReport, parameters);
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in SaveReport", "DataLayer", ex);
            }
        }

        internal dynamic InsertAlarmNotification(string serialNumber, DataManager.AlarmType alarmType, double value, DateTime startTime,eSensorType sensorType )
        {
            var parameters = new List<DbParameter>();
            dynamic response = new ExpandoObject();
            response.Alarms = new List<dynamic>();
            response.IsOK = false;
            try
            {
                parameters.Add(CreateParameter("p_SerialNumber", Convert.ToInt32(serialNumber), DbType.Int32));
                parameters.Add(CreateParameter("p_AlarmTypeID", (int)alarmType, DbType.Int32));
                parameters.Add(CreateParameter("p_Value", value, DbType.Double));
                parameters.Add(CreateParameter("p_StartTime", startTime.Subtract(new DateTime(1970,1,1,0,0,0)).TotalSeconds, DbType.Int32));
                parameters.Add(CreateParameter("p_SensorTypeID", (int)sensorType, DbType.Int32));

                var dt = Execute(SPNames.InsertAlarmNotification, parameters);

                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Result = "OK";
                    response.IsOK = true;

                    var alarms = new List<dynamic>();
                    foreach (DataRow row in dt.Rows)
                    {
                        if ((int)row[0] > 0)
                        {
                            dynamic alarm = new ExpandoObject();
                            alarm.Name = row["DeviceName"].ToString();
                            alarm.Value = Convert.ToDouble(row["Value"]);
                            alarm.StartTime = row["StartTime"].ToString();
                            alarm.ClearTime = row["ClearTime"].ToString();
                            alarm.AlarmReason = row["AlarmReason"].ToString();
                            alarm.AlarmType = row["AlarmTypeName"].ToString();
                            alarm.AlarmTypeID = row["AlarmTypeID"].ToString();
                            alarm.DeviceSensorID = row["DeviceSensorID"].ToString();
                            alarm.SerialNumber = row["SerialNumber"].ToString();
                            alarms.Add(alarm);
                        }
                    }
                    response.Alarms = alarms;
                    response.SerialNumber = serialNumber;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in InsertAlarmNotification", "DataLayer", ex);
            }
            return response;
        }

        internal void InsertDevicesBatch(string batchDevices)
        {
            ExecuteQuery(batchDevices);
        }

        internal void NewPermissionGroup(int userId, dynamic groupData)
        {
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_UserID", userId, DbType.Int32),
                    CreateParameter("p_Name", groupData.groupName.ToString(), DbType.String),
                    CreateParameter("p_ParentName", groupData.permissions.groupName, DbType.String),
                };

                Execute(SPNames.NewPermissionGroup, parameters);

            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in AddReportContact", "DataLayer", ex);
            }
        }

        internal dynamic AddProfileReport(int userId, dynamic profile, bool isNew = true)
        {
           
            dynamic response = new ExpandoObject();
            response.Result = "Error";
            try
            {
                long dateFrom = 0;
                long dateTo = 0;
                if (profile.scheduleInfo.from != null)
                    dateFrom = Convert.ToInt64(profile.scheduleInfo.from.ToString());
                if (profile.scheduleInfo.to != null)
                    dateTo = Convert.ToInt64(profile.scheduleInfo.to.ToString());

                var parameters = new List<DbParameter>
                {
                    !isNew
                        ? CreateParameter("p_ReportID", profile.profileId, DbType.Int32)
                        : CreateParameter("p_ReportID", 0, DbType.Int32),
                    CreateParameter("p_UserID", userId, DbType.Int32),
                    CreateParameter("p_ProfileName", profile.name.ToString(), DbType.String),
                    CreateParameter("p_Description", profile.description.ToString(), DbType.String),
                    CreateParameter("p_IsActive", profile.activity.ToString().ToLower() == "true" ? 1 : 0, DbType.Int32),
                    CreateParameter("p_PageSize", profile.pagesize.ToString(), DbType.String),
                    CreateParameter("p_DateFormat", profile.formatDate.ToString(), DbType.String),
                    CreateParameter("p_TemperatureUnit", profile.units.ToString().ToUpper() == "F" ? 1 : 0, DbType.Int32),
                    CreateParameter("p_TimeZoneOffset", Convert.ToInt32(profile.timezone.offset.ToString()),
                        DbType.Int32),
                    CreateParameter("p_PeriodTypeID", getScheduleTypeId(profile.scheduleType.ToString()), DbType.Int32),
                    CreateParameter("p_PeriodTime", getTime(profile.scheduleInfo), DbType.DateTime),
                    CreateParameter("p_PeriodDay", getDay(profile.scheduleInfo), DbType.Int32),
                    CreateParameter("p_PeriodMonth", getMonthDay(profile.scheduleInfo), DbType.Int32),
                    CreateParameter("p_PeriodStart", dateFrom/1000, DbType.Int32),
                    CreateParameter("p_PeriodEnd", dateTo/1000, DbType.Int32),
                    CreateParameter("p_EmailAsLink", profile.reportAsLink.ToString() == "True" ? 1 : 0,
                        DbType.Int32),
                    CreateParameter("p_EmailAsPDF", profile.reportAsPdf.ToString() == "True" ? 1 : 0,
                        DbType.Int32)
                };



                if (profile.header != null && profile.header.imageID != null &&
                    profile.header.imageID.ToString() != "null")
                {
                    //save the logo in the folder of used and delete the image from S3 in folder upload
                    if (S3Manager.Instance.CopyImageLogo(Convert.ToInt32(profile.header.imageID.ToString()),
                        profile.header.imageType.ToString()))
                        //save the key path in the database
                        parameters.Add(CreateParameter("p_ReportHeaderLink",
                            string.Format("{0}.{1}", profile.header.imageID, profile.header.imageType), DbType.String));
                }
                else
                    parameters.Add(CreateParameter("p_ReportHeaderLink","", DbType.String));

                var dt = Execute(SPNames.AddProfileReport, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    if (dt.Rows[0][0] != null && dt.Rows[0][0].ToString().Length > 0)
                    {
                        if (Convert.ToInt32(dt.Rows[0][0]) > 0)
                        {
                            response.Result = "OK";
                            response.ReportID = Convert.ToInt32(dt.Rows[0][0]);

                            foreach (var logger in profile.loggers)
                            {
                                //add to report devices
                                AddReportDevice(Convert.ToInt32(dt.Rows[0][0]), Convert.ToInt32(logger.SerialNumber.ToString()));
                            }
                            //add approvers
                            foreach (var approve in profile.approvers)
                            {
                                //add to report approver
                                AddReportApprover(Convert.ToInt32(dt.Rows[0][0]), Convert.ToInt32(approve.userId.ToString()));
                            }
                            //add reviewers
                            foreach (var reviewer in profile.reviewers)
                            {
                                //add to report reviewer
                                AddReportReviewer(Convert.ToInt32(dt.Rows[0][0]), Convert.ToInt32(reviewer.userId.ToString()));
                            }
                            //add contacts
                            foreach (var cc in profile.ccd)
                            {
                                //add to report devices
                                AddReportContact(Convert.ToInt32(dt.Rows[0][0]), Convert.ToInt32(cc.userId.ToString()));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in AddProfileReport", "DataLayer", ex);
            }
            return response;
        }

        internal void AddReportContact(int reportId, int contactId)
        {
            
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_ReportTemplateID", reportId, DbType.Int32),
                    CreateParameter("p_ContactID", contactId, DbType.Int32)
                };

                Execute(SPNames.AddReportContact, parameters);

            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in AddReportContact", "DataLayer", ex);
            }
        }

        internal void AddReportReviewer(int reportId, int userId)
        {
            var parameters = new List<DbParameter>();
            try
            {
                parameters.Add(CreateParameter("p_ReportTemplateID", reportId, DbType.Int32));
                parameters.Add(CreateParameter("p_UserID", userId, DbType.Int32));

                Execute(SPNames.AddReportReviewer, parameters);

            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in AddReportReviewer", "DataLayer", ex);
            }
        }

        internal void AddReportApprover(int reportId, int userId)
        {
            var parameters = new List<DbParameter>();
            try
            {
                parameters.Add(CreateParameter("p_ReportTemplateID", reportId, DbType.Int32));
                parameters.Add(CreateParameter("p_UserID", userId, DbType.Int32));

                Execute(SPNames.AddReportApprover, parameters);

            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in AddReportApprover", "DataLayer", ex);
            }
        }

        internal void AddReportDevice(int reportId, int serialNumber)
        {
            var parameters = new List<DbParameter>();
            try
            {
                parameters.Add(CreateParameter("p_ReportTemplateID", reportId, DbType.Int32));
                parameters.Add(CreateParameter("p_SerialNumber", serialNumber, DbType.Int32));

                Execute(SPNames.AddReportDevice, parameters);

            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in AddReportDevice", "DataLayer", ex);
            }
        }

        #endregion

        private int getDateFrom(dynamic scheduleInfo)
        {
            if (scheduleInfo.from != null)
            {
                var date = scheduleInfo.from;

                var splitDateTime = date.Split('T');
                var dateSplit = splitDateTime[0].Split('-');
                var timeSplit = splitDateTime[1].Split(':');
                var dt = new DateTime(Convert.ToInt32(dateSplit[0]), Convert.ToInt32(dateSplit[1]),
                    Convert.ToInt32(dateSplit[2]), Convert.ToInt32(timeSplit[0]), Convert.ToInt32(timeSplit[1]), 0);
                return Convert.ToInt32(dt.Subtract(new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds);
            }
            return 0;
        }

        private int getDateTo(dynamic scheduleInfo)
        {
            if (scheduleInfo.from != null)
            {
                var date = scheduleInfo.to;

                var splitDateTime = date.Split('T');
                var dateSplit = splitDateTime[0].Split('-');
                var timeSplit = splitDateTime[1].Split(':');
                var dt = new DateTime(Convert.ToInt32(dateSplit[0]), Convert.ToInt32(dateSplit[1]),
                    Convert.ToInt32(dateSplit[2]), Convert.ToInt32(timeSplit[0]), Convert.ToInt32(timeSplit[1]), 0);
                return Convert.ToInt32(dt.Subtract(new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds);
            }
            return 0;
        }

        private int getMonthDay(dynamic scheduleInfo)
        {
            if (scheduleInfo.day != null)
            {
                string day = scheduleInfo.day.ToString();
                switch (day)
                {
                    case "LAST_DAY":
                        return 30;
                    case "1ST":
                        return 1;
                    case "2ND":
                        return 2;
                    case "3RD":
                        return 3;
                    case "15TH":
                        return 15;
                }
            }
            return -1;
        }

        private int getDay(dynamic scheduleInfo)
        {
            if (scheduleInfo.day !=null)
            {
                string day = scheduleInfo.day.ToString();
                switch (day)
                {
                    case "Sun":
                        return 1;
                    case "Mon":
                        return 2;
                    case "Tue":
                        return 3;
                    case "Wed":
                        return 4;
                    case "Thu":
                        return 5;
                    case "Fri":
                        return 6;
                    case "Sat":
                        return 7;
                }
            }
            return -1;
        }

        private DateTime getTime(dynamic scheduleInfo)
        {
            if (scheduleInfo.period != null)
            {
                var hour = scheduleInfo.period.ToString() == "AM"
                    ? Convert.ToInt32(scheduleInfo.hours.ToString())
                    : Convert.ToInt32(scheduleInfo.hours.ToString()) + 12;

                return new DateTime(1970, 1, 1, hour == 24 ? 0 : hour, Convert.ToInt32(scheduleInfo.minutes.ToString()),
                    0);
            }
            return DateTime.UtcNow;
        }

        private int getScheduleTypeId(string scheduleType)
        {
            switch (scheduleType)
            {
                case "DAILY":
                    return 1;
                case "WEEKLY":
                    return 2;
                case "MONTHLY":
                    return 3;
                case "ONCE":
                    return 4;
            }
            return 0;
        }



       
    }
}
