﻿using Base.Devices;
using Base.Sensors.Management;
using Base.Sensors.Types;
using Base.Sensors.Types.Temperature;
using Infrastructure;
using Infrastructure.DAL;
using Infrastructure.Entities;
using Infrastructure.Responses;
using MicroLite2E.Devices;
using MicroLogAPI.Sensors.V1;
using MicroXBase.Devices.Types;
using PicoLite.Devices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;
using System.Text;
using Log4Tech;
using Server.Infrastructure;


namespace DataLayer
{
    public class DBUpdate : DBBase
    {
        #region public methods
        public DBUpdate(string connString, string provider)
        {
            Init(connString, provider);
        }

        internal void RemoveAnalyticsSensors(int userId)
        {
            var parameters = new List<DbParameter>();
            try
            {
                parameters.Add(CreateParameter("p_UserID", userId, DbType.Int32));
                parameters.Add(CreateParameter("p_DeviceSensorID", 0, DbType.Int32));

                Execute(SPNames.DeleteAnalyticsSensors, parameters);
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in RemoveAnalyticsSensors", this, ex);
            }
        }

        internal void RemoveAnalyticsSensor(int userId, int sensorId)
        {
            var parameters = new List<DbParameter>();
            try
            {
                parameters.Add(CreateParameter("p_UserID", userId, DbType.Int32));
                parameters.Add(CreateParameter("p_DeviceSensorID", sensorId, DbType.Int32));

                Execute(SPNames.DeleteAnalyticsSensors, parameters);
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in RemoveAnalyticsSensor", this, ex);
            }
        }

        internal dynamic DeleteContact(int contactId)
        {
            dynamic response = new ExpandoObject();
            response.Result = "Error";
            response.Data = new List<dynamic>();
            
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_ContactID", contactId, DbType.Int32)
                };

                Execute(SPNames.DeleteContact, parameters);
                response.Result = "OK";
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in DeleteContact", "DataLayer", ex);
            }
            return response;
        }

        internal void DeleteUser(int userId)
        {
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_UserID", userId, DbType.Int32)
                };

                Execute(SPNames.DeleteUser, parameters);
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in DeleteUser", "DataLayer", ex);
            }
        }

        internal dynamic ApproveReport(int userId, int reportDeviceHistoryId)
        {
            var parameters = new List<DbParameter>();
            try
            {
                parameters.Add(CreateParameter("p_UserID", userId, DbType.Int32));
                parameters.Add(CreateParameter("p_ReportDeviceHistoryID", reportDeviceHistoryId, DbType.Int32));

                Execute(SPNames.ApproveReport, parameters);
                return new Response
                {
                    Result = "OK",
                };
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in ApproveReport", this, ex);
            }
            return new Response { Result = "Error", }; 
        }

        internal Response UpdateUser(User user)
        {
            var parameters = new List<DbParameter>();
            try
            {
                parameters.Add(CreateParameter("p_UserID", user.UserID, DbType.Int32));
                parameters.Add(CreateParameter("p_UserName", user.UserName, DbType.String));
                //parameters.Add(CreateParameter("p_UserPassword", user.Password, DbType.String));
                parameters.Add(CreateParameter("p_Email", user.Email, DbType.String));
                parameters.Add(CreateParameter("p_LanguageID", user.LanguageID, DbType.Int32));
                parameters.Add(CreateParameter("p_defaultDateFormat", user.DefaultDateFormat, DbType.String));
                //parameters.Add(CreateParameter("p_QuestionID", user.SecureQuestionID, DbType.Int32));
                //parameters.Add(CreateParameter("p_Answer", user.SecureAnswer, DbType.String));


                DataTable dt = Execute(SPNames.UpdateUser, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    if (dt.Rows[0][0] != null && dt.Rows[0][0].ToString().Length > 0)
                    {
                        if (Convert.ToInt32(dt.Rows[0][0]) > 0)
                        {
                            return new Response()
                            {
                                Result = "OK",
                            };
                        }
                        else
                        {
                            return new Response() { Result = "Error", };
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in UpdateUser", this, ex);
            }
            return new Response() { Result = "Error", }; 
        }

        internal void DeleteGroup(int groupId)
        {
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_GroupID", groupId, DbType.Int32)
                };

                Execute(SPNames.DeleteGroup, parameters);
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in DeleteGroup", "DataLayer", ex);
            }
        }

        internal dynamic ResetUserPassword(dynamic newPasswordData)
        {
            dynamic response = new ExpandoObject();
            response.Result="Error";
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_password", newPasswordData.password.ToString(), DbType.String),
                    CreateParameter("p_GUID", newPasswordData.guid.ToString(), DbType.String)
                };

                var dt = Execute(SPNames.ResetUserPassword, parameters);
                var result = 0;
                if (dt != null && dt.Rows.Count > 0)
                    result = Convert.ToInt32(dt.Rows[0][0]);
                if(result==1)
                    response.Result = "OK";
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in ResetUserPassword", "DataLayer", ex);
            }
            return response;
        }

        internal void DeAssociateDevices(int serialNumber)
        {
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_SerialNumber", serialNumber, DbType.Int32)
                };

                Execute(SPNames.DeAssociateDevices, parameters);
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in DeAssociateDevices", "DataLayer", ex);
            }
        }

        internal void SaveMatrix(int userId, dynamic matrixData)
        {
            try
            {
                foreach (var matrix in matrixData.matrix)
                {
                    var parameters = new List<DbParameter>
                    {
                        CreateParameter("p_UserID", userId, DbType.Int32),
                        CreateParameter("p_PermissionActionID",
                            Convert.ToInt32(matrix.permissionActionId.ToString()), DbType.Int32),
                        CreateParameter("p_PermissionGroupID",
                            Convert.ToInt32(matrix.permissionGroupId.ToString()), DbType.Int32),
                        CreateParameter("p_IsAllowed", matrix.allowed.ToString() == "true" ? 1 : 0, DbType.Int32)
                    };

                    Execute(SPNames.SaveMatrix, parameters);
                }

            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in SaveMatrix", "DataLayer", ex);
            }
        }

        internal void AdminUpdateUser(int userId, dynamic userData)
        {
            var permissionGroupId = 0;
            try
            {
                if (userData.privileges.GetType().Name != "JArray")
                    //one permission for all
                    permissionGroupId = Convert.ToInt32(userData.privileges.groupId);
                //first update the user permission and data
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_AdminUserID", userId, DbType.Int32),
                    CreateParameter("p_UserID", Convert.ToInt32(userData.userId.ToString()), DbType.Int32),
                    CreateParameter("p_PermissionRoleID", Convert.ToInt32(userData.roleId.ToString()), DbType.Int32),
                    CreateParameter("p_InactivityTimeout", Convert.ToInt32(userData.inactivityTimeout.ToString()),
                        DbType.Int32),
                    CreateParameter("p_PermissionGroupID", permissionGroupId, DbType.Int32),
                    CreateParameter("p_Email", userData.userEmail.ToString(), DbType.String),
                };
                Execute(SPNames.AdminUpdateUser, parameters);
                //handle permission per site
                if (permissionGroupId == 0) 
                {
                    foreach (var privilege in userData.privileges)
                    {
                        var siteId = Convert.ToInt32(privilege.site.ID);
                        permissionGroupId = Convert.ToInt32(privilege.group.groupId);
                        var parametersPermissions = new List<DbParameter>
                        {
                            CreateParameter("p_UserID", Convert.ToInt32(userData.userId.ToString()), DbType.Int32),
                            CreateParameter("p_PermissionGroupID", permissionGroupId, DbType.Int32),
                            CreateParameter("p_SiteID", siteId, DbType.Int32),
                        };
                        Execute(SPNames.AddPermissionGroupSite, parametersPermissions);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in AdminUpdateUser", "DataLayer", ex);
            }
        }

        internal void UpsertGroup(int userId, dynamic groupData)
        {
            try
            {
//                var builder = new StringBuilder();
//                foreach (var contact in groupData.groupMembers)
//                    builder.AppendFormat("{0},", contact.contactID);
//               
//                builder = builder.Remove(builder.Length - 1, 1);

                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_UserID", userId, DbType.Int32),
                    CreateParameter("p_GroupID",
                        groupData.groupId.ToString() == string.Empty ? 0 : Convert.ToInt32(groupData.groupId.ToString()),
                        DbType.Int32),
                    CreateParameter("p_GroupName", groupData.groupName.ToString(), DbType.String),
                };

                var dt = Execute(SPNames.AddGroup, parameters);

                var groupId = Convert.ToInt32(dt.Rows[0][0]);

                foreach (var groupMember in groupData.groupMembers)
                {
                    var contactId = Convert.ToInt32(groupMember.contactID.ToString());
                    if (contactId <= 0) continue;

                    parameters = new List<DbParameter>
                    {
                        CreateParameter("p_UserID", userId, DbType.Int32),
                        CreateParameter("p_GroupID", groupId, DbType.Int32),
                        CreateParameter("p_ContactID", Convert.ToInt32(groupMember.contactID.ToString()),
                            DbType.Int32),
                        CreateParameter("p_IsUser", Convert.ToInt32(groupMember.isSystem.ToString()), DbType.Int32)
                    };
                    Execute(SPNames.UpsertGroup, parameters);
                }

            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in UpsertGroup", "DataLayer", ex);
            }
        }

        internal dynamic UpsertContact(int userId, dynamic contactData)
        {
            dynamic response = new ExpandoObject();
            response.Result = "Error";
            response.Error = string.Empty;
            try
            {
                string from = contactData.workHours.from.ToString();
                int fromInt;
                if (!int.TryParse(from, out fromInt))
                    if (from.IndexOf(':') > 0)
                        fromInt = Convert.ToInt32(from.Substring(0, from.IndexOf(':')));
                string to = contactData.workHours.to.ToString();
                int toInt;
                if (!int.TryParse(to, out toInt))
                    if (to.IndexOf(':') > 0)
                        toInt = Convert.ToInt32(to.Substring(0, to.IndexOf(':')));


                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_UserID", userId, DbType.Int32),
                    CreateParameter("p_ContactID",
                        contactData.contactID.ToString() == string.Empty
                            ? 0
                            : Convert.ToInt32(contactData.contactID.ToString()), DbType.Int32),
                    CreateParameter("p_ContactName", contactData.contactName.ToString(), DbType.String),
                    CreateParameter("p_CountryCode", contactData.country.code.ToString(), DbType.String),
                    CreateParameter("p_CountryName", contactData.country.name.ToString(), DbType.String),
                    CreateParameter("p_Email", contactData.email.ToString(), DbType.String),
                    CreateParameter("p_MobilePhone", contactData.mobilePhone.ToString(), DbType.String),
                    CreateParameter("p_SMSResend", Convert.ToInt32(contactData.smsResends.ToString()), DbType.Int32),
                    CreateParameter("p_Title", contactData.title.ToString(), DbType.String),
                    CreateParameter("p_WorkingHourMode", contactData.workHours.mode.ToString(), DbType.String),
                    CreateParameter("p_From", fromInt, DbType.Int32),
                    CreateParameter("p_To", toInt, DbType.Int32),
                    CreateParameter("p_WorkingSun", contactData.workingDays.sun == "true" ? 1 : 0, DbType.Int32),
                    CreateParameter("p_WorkingMon", contactData.workingDays.mon == "true" ? 1 : 0, DbType.Int32),
                    CreateParameter("p_WorkingTue", contactData.workingDays.tue == "true" ? 1 : 0, DbType.Int32),
                    CreateParameter("p_WorkingWed", contactData.workingDays.wed == "true" ? 1 : 0, DbType.Int32),
                    CreateParameter("p_WorkingThu", contactData.workingDays.thu == "true" ? 1 : 0, DbType.Int32),
                    CreateParameter("p_WorkingFri", contactData.workingDays.fri == "true" ? 1 : 0, DbType.Int32),
                    CreateParameter("p_WorkingSat", contactData.workingDays.sat == "true" ? 1 : 0, DbType.Int32)
                };

                var dt = Execute(SPNames.UpsertContact, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    var contactId = Convert.ToInt32(dt.Rows[0][0]);
                    foreach (var groupId in contactData.groups)
                        UpdateContactGroup(contactId, Convert.ToInt32(groupId.ToString()));
                }
                response.Result = "OK";
            }
            catch (SqlException se)
            {
                switch (se.Number)
                {
                    case 2601:
                        response.Error = "Duplicate Email. This email already exists.";
                        break;
                   
                }
                Log.Instance.Write("SQL Exception in UpsertContact - {0}", "DataLayer", Log.LogSeverity.WARNING, se.Message);
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in UpsertContact", "DataLayer", ex);
               
            }
            return response;
        }

        internal Response UpdateContact(Contact contact)
        {
            
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_ContactID", contact.ContactID, DbType.Int32),
                    CreateParameter("p_Name", contact.Name, DbType.String),
                    CreateParameter("p_Title", contact.Title, DbType.String),
                    CreateParameter("p_Phone", contact.Phone, DbType.String),
                    CreateParameter("p_Email", contact.Email, DbType.String),
                    CreateParameter("p_WeekdayStart", contact.WeekdayStart, DbType.Int32),
                    CreateParameter("p_WorkdayStart", contact.WorkdayStart, DbType.Time),
                    CreateParameter("p_WorkdayEnd", contact.WorkdayEnd, DbType.Time),
                    CreateParameter("p_OutOfOfficeStart", contact.OutOfOfficeStart, DbType.Int32),
                    CreateParameter("p_OutOfOfficeEnd", contact.OutOfOfficeEnd, DbType.Int32),
                    CreateParameter("p_SMSResendInterval", contact.SMSResendInterval, DbType.Int32)
                };


                DataTable dt = Execute(SPNames.UpdateContact, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    if (dt.Rows[0][0] != null && dt.Rows[0][0].ToString().Length > 0)
                    {
                        if (Convert.ToInt32(dt.Rows[0][0]) > 0)
                        {
                            return new Response()
                            {
                                Result = "OK",
                            };
                        }
                        else
                        {
                            return new Response() { Result = "Error", };
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in UpdateContact", "DataLayer", ex);
            }
            return new Response() { Result = "Error", }; 
        }

        internal Response UpsertDevice(DeviceUpsert device)
        {
            try
            {
                //first upsert the device
                var deviceid = UpsertMainDevice(device);
                //if ok then update the other values for device by type
                if (deviceid > 0)
                {
                    bool bSuccess;
                    //save the parameters for the generic logger:
                    if (device.Device is GenericLogger)
                    {
                        bSuccess = UpsertDeviceLogger(deviceid, ((GenericLogger)device.Device));
                    }
                    //save the parameters for microx device
                    if (device.Device is MicroXLogger)
                    {
                        bSuccess = UpsertMicroXDevice(deviceid, ((MicroXLogger)device.Device), device.BoomerangUTC, device.LoggerUTC);
                    }

                    //save the parameters for picolite device sensor
                    if (device.Device is PicoLiteLogger)
                    {
                        bSuccess = UpsertDevicePicoLite(deviceid, ((PicoLiteLogger)device.Device));
                    }
                    //save the parameters for microlog device
                    if (device.Device is MicroLogAPI.Devices.GenericMicroLogLogger)
                    {
                        bSuccess = UpsertMicroLogDevice(deviceid, ((MicroLogAPI.Devices.GenericMicroLogLogger)device.Device));
                    }
                    //save the parameters for microlite device
                    if (device.Device is MicroLogAPI.Devices.MicroLiteLogger)
                    {
                        bSuccess = UpsertMicroLiteDevice(deviceid, ((MicroLogAPI.Devices.MicroLiteLogger)device.Device));
                    }
                    //update the sensors for this device
                    bSuccess = UpsertDeviceSensors(deviceid, (GenericLogger)device.Device, device.SensorsName);
                    //if all went well...
                    if (bSuccess)
                    {
                        return new Response()
                        {
                            Result = "OK",
                        };
                    }
                }

            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in UpsertDevice", "DataLayer", ex);
            }
            return new Response { Result = "Error", }; 
        }

        internal void UpdateDeviceAlarmData(int serialNumber, IEnumerable<GenericSensor> sensors)
        {
            
            try
            {
                var parameters = new List<DbParameter>();
                var userId = 0;
                //iterate sensors
                foreach (var sensor in sensors)
                {
                    parameters.Clear();
                    //checks if the sensor has samples
                    if (sensor.Samples.Count > 0)
                    {
                        //get the last sensor value
                        var sample = (from s in sensor.Samples
                                     where s.IsDummy == false
                                     orderby s.Date descending
                                     select s).FirstOrDefault();

                        if (sample!=null && sample.AlarmStatus > 0)
                        {
                            parameters.Add(CreateParameter("p_SerialNumber", serialNumber, DbType.Int32));
                            parameters.Add(CreateParameter("p_SensorTypeID", (int)sensor.Type, DbType.Int32));
                            parameters.Add(CreateParameter("p_SensorUnitName", sensor.Unit.Name.Trim('°', '%').Trim(), DbType.String));
                            parameters.Add(CreateParameter("p_AlarmDate", sample.Date.Subtract(new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds, DbType.Int32));
                            parameters.Add(CreateParameter("p_AlarmValue", sample.Value, DbType.Double));

                            Execute(SPNames.UpdateDeviceAlarmData, parameters);
                            
                        }
                    }
                }
                
            }
            catch(Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in UpdateDeviceAlarmData", "DataLayer", ex);
            }
        }

        internal dynamic SetupUser(dynamic userData)
        {
            dynamic response = new ExpandoObject();
            response.Result = "Error";
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_GUID", userData.guid.ToString(), DbType.String),
                    CreateParameter("p_UserName", userData.userName.ToString(), DbType.String),
                    CreateParameter("p_userPassword", userData.password1.ToString(), DbType.String),
                    CreateParameter("p_timezonevalue", userData.timezone.value.ToString(), DbType.String),
                    CreateParameter("p_timezoneabbr", userData.timezone.abbr.ToString(), DbType.String),
                    CreateParameter("p_timezoneoffset", Convert.ToInt32(userData.timezone.offset.ToString()), DbType.Int32),
                    CreateParameter("p_timezoneisdst", userData.timezone.isdst.ToString()=="True"?1:0, DbType.Int32),
                    CreateParameter("p_timezonetext", userData.timezone.text.ToString(), DbType.String),
                    CreateParameter("p_CountryCode", userData.country.code.ToString(), DbType.String),
                    CreateParameter("p_FirstName", userData.fName.ToString(), DbType.String),
                    CreateParameter("p_LastName", userData.lName.ToString(), DbType.String),
                    CreateParameter("p_MobileNumber", userData.mobile.ToString(), DbType.String),
                    CreateParameter("p_DialCode", userData.dialCode.ToString(), DbType.String),
                };

                var dt = Execute(SPNames.SetupUser, parameters);

                if (dt != null && dt.Rows.Count > 0)
                {
                    var dataRow = dt.Rows[0];
                    var updateUserId = Convert.ToInt32(dataRow[0]);
                    if (updateUserId < 0)
                    {
                        switch (updateUserId)
                        {
                            case -100:
                                response.Error = "User name already exists.";
                                break;
                            case -99:
                                response.Error = "User registration has expired.";
                                break;
                            default:
                                response.Error = "User name/password are invalid";
                                break;

                        }
                    }
                    else
                        response.Result = "OK";
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in SetupUser", "DataLayer", ex);
            }
            return response;
        }

        internal dynamic SaveAlarmNotification(dynamic alarmNotifications)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            dynamic response = new System.Dynamic.ExpandoObject();
            response.IsOK = false;
            response.Result = "";
            try
            {
                parameters.Add(CreateParameter("p_ContactID", alarmNotifications.ContactID, DbType.Int32));
                parameters.Add(CreateParameter("p_SerialNumber", alarmNotifications.SerialNumber, DbType.Int32));
                parameters.Add(CreateParameter("p_ReceiveEmail", alarmNotifications.ReceiveEmail, DbType.Int16));
                parameters.Add(CreateParameter("p_ReceiveSMS", alarmNotifications.ReceiveSMS, DbType.Int16));
                parameters.Add(CreateParameter("p_ReceiveBatteryLow", alarmNotifications.ReceiveBatteryLow, DbType.Int16));
                parameters.Add(CreateParameter("p_BatteryLowValue", alarmNotifications.BatteryLowValue, DbType.Int32));

                DataTable dt = Execute(SPNames.SaveAlarmNotifications, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    if (dt.Rows[0][0] != null && dt.Rows[0][0].ToString().Length > 0)
                    {
                        response.Result = "OK";
                        response.IsOK = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in SaveAlarmNotification", "DataLayer", ex);
            }
            return response;
        }

        internal void UpdateNotificationEmailSent(string serialNumber, int contactID, int sensorTypeID)
        {
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_SerialNumber", serialNumber, DbType.Int32),
                    CreateParameter("p_ContactID", contactID, DbType.Int32),
                    CreateParameter("p_SensorTypeID", sensorTypeID, DbType.Int32)
                };

                Execute(SPNames.UpdateNotificationEmailSent, parameters);
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in UpdateNotificationEmailSent", "DataLayer", ex);
            }
        }

        internal void UpdateNotificationBatteryEmailSent(string serialNumber, int contactID)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            try
            {
                parameters.Add(CreateParameter("p_SerialNumber", serialNumber, DbType.Int32));
                parameters.Add(CreateParameter("p_ContactID", contactID, DbType.Int32));

                Execute(SPNames.UpdateNotificationBatteryEmailSent, parameters);
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in UpdateNotificationBatteryEmailSent", "DataLayer", ex);
            }
        }

        internal void UpdateNotificationSMSSent(string serialNumber, int contactID, string MsgId, int sensorTypeID)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            try
            {
                parameters.Add(CreateParameter("p_SerialNumber", serialNumber, DbType.Int32));
                parameters.Add(CreateParameter("p_ContactID", contactID, DbType.Int32));
                parameters.Add(CreateParameter("p_SMSID", MsgId, DbType.String));
                parameters.Add(CreateParameter("p_SensorTypeID", sensorTypeID, DbType.Int32));

                Execute(SPNames.UpdateNotificationSMSSent, parameters);
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in UpdateNotificationSMSSent", "DataLayer", ex);
            }
        }

        internal void UpdateNotificationBatterySMSSent(string serialNumber, int contactID, string MsgId)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            try
            {
                parameters.Add(CreateParameter("p_SerialNumber", serialNumber, DbType.Int32));
                parameters.Add(CreateParameter("p_ContactID", contactID, DbType.Int32));
                parameters.Add(CreateParameter("p_SMSID", MsgId, DbType.String));

                Execute(SPNames.UpdateNotificationBatterySMSSent, parameters);
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in UpdateNotificationBatterySMSSent", "DataLayer", ex);
            }
        }

        internal void UpdateSMSStatus(string smsID, string smsStatus)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            try
            {
                parameters.Add(CreateParameter("p_SMSID", smsID, DbType.String));
                parameters.Add(CreateParameter("p_SMSStatus", smsStatus, DbType.String));

                Execute(SPNames.UpdateSMSStatus, parameters);
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in UpdateSMSStatus", "DataLayer", ex);
            }
        }

        internal void UpdateAlarmNotification(string serialNumber, DataManager.AlarmType alarmType, eSensorType sensorType)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            try
            {
                parameters.Add(CreateParameter("p_SerialNumber", Convert.ToInt32(serialNumber), DbType.Int32));
                parameters.Add(CreateParameter("p_AlarmTypeID",(int) alarmType, DbType.Int32));
                parameters.Add(CreateParameter("p_SensorTypeID", (int)sensorType, DbType.Int32));

                Execute(SPNames.UpdateAlarmNotification, parameters);
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in UpdateAlarmNotification", "DataLayer", ex);
            }
        }

        internal void SetAlarmNotificationVisibility(dynamic alarmNotification)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            try
            {
                parameters.Add(CreateParameter("p_DeviceID", alarmNotification.DeviceID, DbType.Int32));
                parameters.Add(CreateParameter("p_AlarmTypeID", alarmNotification.AlarmTypeID, DbType.Int32));
                parameters.Add(CreateParameter("p_Value", alarmNotification.Value, DbType.Double));
                parameters.Add(CreateParameter("p_IsSilent", alarmNotification.IsSilent, DbType.Int16));
                parameters.Add(CreateParameter("p_SensorAlarmID", alarmNotification.SensorAlarmID, DbType.Int32));

                Execute(SPNames.SetAlarmNotificationVisiblity, parameters);
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in SetAlarmNotificationVisibility", "DataLayer", ex);
            }
        }

        internal void UpdateAlarmReason(int alarmID, int serialNumber, int alarmTypeID, string reason, eSensorType sensorType)
        {
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_AlarmID", alarmID, DbType.Int32),
                    CreateParameter("p_SerialNumber", serialNumber, DbType.Int32),
                    CreateParameter("p_AlarmTypeID", alarmTypeID, DbType.Int32),
                    CreateParameter("p_Reason", reason, DbType.String),
                    CreateParameter("p_DeviceSensorID", sensorType, DbType.Int32)
                };

                Execute(SPNames.UpdateAlarmReason, parameters);
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in UpdateAlarmReason", "DataLayer", ex);
            }
        }

        internal void HideAllAlarms(int userID)
        {
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_UserID", userID, DbType.Int32)
                };

                Execute(SPNames.HideAllAlarmNotifications, parameters);
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in HideAllAlarms", "DataLayer", ex);
            }
        }

        internal void HideAlarmByID(int id)
        {
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_AlarmID", id, DbType.Int32),
                    CreateParameter("p_AlarmTypeID", 0, DbType.Int32),
                    CreateParameter("p_SensorTypeId", 0, DbType.Int32),
                    CreateParameter("p_SerialNumber", 0, DbType.Int32)
                };

                Execute(SPNames.HideAlarmNotification, parameters);
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in HideAlarmByID", "DataLayer", ex);
            }
        }
        
        internal void HideAlarm(string serialNumber, int alarmTypeID, eSensorType sensorType)
        {
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_AlarmID", 0, DbType.Int32),
                    CreateParameter("p_AlarmTypeID", alarmTypeID, DbType.Int32),
                    CreateParameter("p_SensorTypeId",(int) sensorType, DbType.Int32),
                    CreateParameter("p_SerialNumber", Convert.ToInt32(serialNumber), DbType.Int32)
                };

                Execute(SPNames.HideAlarmNotification, parameters);
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in HideAlarm", "DataLayer", ex);
            }
        }

        internal dynamic UpdateSite(dynamic site)
        {
           
            dynamic response = new ExpandoObject();
            response.IsOK = false;
            response.Result = "";
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_SiteID", site.ID, DbType.Int32),
                    CreateParameter("p_ParentID", site.ParentID, DbType.Int32),
                    CreateParameter("p_Name", site.Name, DbType.String)
                };
                // parameters.Add(CreateParameter("p_IconName", site.IconName, DbType.String));
                
                DataTable dt = Execute(SPNames.UpdateSite, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {

                    response.Result = "OK";
                    response.IsOK = true;
                    dynamic siteData = new System.Dynamic.ExpandoObject();
                    siteData.ID = Convert.ToInt32(dt.Rows[0]["id"]);
                    siteData.Name = dt.Rows[0]["name"].ToString();
                    siteData.ParentID = Convert.ToInt32(dt.Rows[0]["parentid"]);
                    siteData.TreeOrder = Convert.ToInt32(dt.Rows[0]["treeorder"]);
                    response.Data = siteData;

                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in UpdateSite", "DataLayer", ex);
            }
            return response;
        }

        internal void DeleteReport(int reportId)
        {
            var parameters = new List<DbParameter>();
            try
            {
                parameters.Add(CreateParameter("p_ReportID", reportId, DbType.Int32));
                Execute(SPNames.DeleteReport, parameters);
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in DeleteReport", "DataLayer", ex);
            }
        }

        internal dynamic DeleteSite(dynamic site)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            dynamic response = new System.Dynamic.ExpandoObject();
            response.IsOK = false;
            response.Result = "Error";
            try
            {
                parameters.Add(CreateParameter("p_SiteID", site.ID, DbType.Int32));
                DataTable dt = Execute(SPNames.DeleteSite, parameters);

                if (dt != null && dt.Rows.Count > 0 && Convert.ToInt32( dt.Rows[0][0]) == 1)
                {
                    response.IsOK = true;
                    response.Result = "OK";
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in DeleteSite", "DataLayer", ex);
            }
            return response;  
        }

        internal void SnoozeAlarm(string serialNumber, int deviceSensorID, int silent)
        {
            List<DbParameter> parameters = new List<DbParameter>();
           
            try
            {
                parameters.Add(CreateParameter("p_SerialNumber", Convert.ToInt32(serialNumber), DbType.Int32));
                parameters.Add(CreateParameter("p_DeviceSensorID", deviceSensorID, DbType.Int32));
                parameters.Add(CreateParameter("p_Silent", silent, DbType.Int32));

                Execute(SPNames.SnoozeAlarm, parameters);
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in SnoozeAlarm", "DataLayer", ex);
            }
        }

        internal void RelateDeviceToSite(int serialNumber, int siteID)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            try
            {
                parameters.Add(CreateParameter("p_SerialNumber", serialNumber, DbType.Int32));
                parameters.Add(CreateParameter("p_SiteID", siteID, DbType.Int32));

                Execute(SPNames.RelateDeviceToSite, parameters);
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in RelateDeviceToSite", "DataLayer", ex);
            }
        }

        internal void UpdateCustomerEmailSent(int serialNumber)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            try
            {
                parameters.Add(CreateParameter("p_SerialNumber", serialNumber, DbType.Int32));

                Execute(SPNames.UpdateCustomerEmailSent, parameters);
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in UpdateCustomerEmailSent", "DataLayer", ex);
            }
        }

        internal void UpdateMongoDBBackup(int serialNumber)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            try
            {
                parameters.Add(CreateParameter("p_SerialNumber", serialNumber, DbType.Int32));

                Execute(SPNames.UpdateMongoDBBackup, parameters);
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in UpdateMongoDBBackup", "DataLayer", ex);
            }
        }

        internal void SaveMKTSensorData(int userId, int sensorId, double low, double high, double activationEnergy)
        {
            var parameters = new List<DbParameter>();
            try
            {
                parameters.Add(CreateParameter("p_UserID", userId, DbType.Int32));
                parameters.Add(CreateParameter("p_DeviceSensorID", sensorId, DbType.Int32));
                parameters.Add(CreateParameter("p_ActivationEnergy", activationEnergy, DbType.Double));
                parameters.Add(CreateParameter("p_LowLimit", low, DbType.Double));
                parameters.Add(CreateParameter("p_HighLimit", high, DbType.Double));

                Execute(SPNames.UpdateAnalyticsSensors, parameters);
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in SaveMKTSensorData", this, ex);
            }
        }

        internal void DeleteSetupFile(int userId, string fileId)
        {
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_UserID", userId, DbType.Int32),
                    CreateParameter("p_SetupID", fileId, DbType.String),
                };

                Execute(SPNames.DeleteDeviceSetupFile, parameters);
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in DeleteSetupFile", this, ex);
            }
        }

        internal void UpdateDeviceFwFlag(int serialNumber, int fwUpdateFlag)
        {
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_SerialNumber", serialNumber, DbType.Int32),
                    CreateParameter("p_InFwUpdate",fwUpdateFlag , DbType.Int32),
                };

                Execute(SPNames.UpdateDeviceMode, parameters);
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in UpdateDeviceFwFlag", this, ex);
            }
        }

        internal dynamic SetCalibrationCertificateDefault(int userId, dynamic calibrationDefault)
        {
            dynamic response = new ExpandoObject();
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_UserID", userId, DbType.Int32),
                    CreateParameter("p_calibtemperature", Convert.ToDouble(calibrationDefault.temperature.ToString()), DbType.Double),
                    CreateParameter("p_calibhumidity", Convert.ToDouble(calibrationDefault.humidity.ToString()), DbType.Double),
                    CreateParameter("p_calibfourtecwaxseal", calibrationDefault.includeInReport.ToString() == "false" ? 0 : 1, DbType.Int32),
                    CreateParameter("p_calibmanufacture", calibrationDefault.manufacturer.ToString(), DbType.String),
                    CreateParameter("p_calibmodel", calibrationDefault.model.ToString() , DbType.String),
                    CreateParameter("p_calibnist", calibrationDefault.nist.ToString() =="true"?1:0 , DbType.Int32),
                    CreateParameter("p_calibproductname", calibrationDefault.productName.ToString(), DbType.String),
                    CreateParameter("p_calibserialnumber", Convert.ToInt32(calibrationDefault.serialNumber.ToString()), DbType.Int32)
                };

                Execute(SPNames.SetCalibrationCertificateDefault, parameters);
                response.Result = "OK";
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in SetCalibrationCertificateDefault", this, ex);
            }
            return response;
        }

        internal dynamic ApplyDeviceAutoSetup(int userId,dynamic setup)
        {

            dynamic response = new ExpandoObject();
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_UserID", userId, DbType.Int32),
                    CreateParameter("p_DeviceType", setup.deviceModel.ToString(), DbType.String),
                    CreateParameter("p_SetupID", setup.fileID.ToString(), DbType.String),
                    CreateParameter("p_IsAutoSetup", setup.autoSetup.ToString().ToLower() == "false" ? 0 : 1, DbType.Int32)
                };

                var dt = Execute(SPNames.UpdateDeviceSetup, parameters);

                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Result = "OK";
                    
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in ApplyDeviceAutoSetup", this, ex);
            }
            return response;
        }

        internal dynamic SaveSystemSettings(dynamic settings)
        {
            dynamic response = new ExpandoObject();
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_UserID", Convert.ToInt32(settings.userID.ToString()), DbType.Int32),
                    CreateParameter("p_isCFREnabled", Convert.ToInt32(settings.isCfrMode.ToString()), DbType.Int32),
                    CreateParameter("p_TemperatureUnit", Convert.ToInt32(settings.CelsiusMode.ToString()), DbType.String),
                    CreateParameter("p_debugmode", Convert.ToInt32(settings.debugMode.ToString()), DbType.Int32),
                    CreateParameter("p_LanguageCode", settings.language.ToString(), DbType.String),
                    CreateParameter("p_timezonevalue", settings.timezone.value.ToString(), DbType.String),
                    CreateParameter("p_timezoneabbr", settings.timezone.abbr.ToString(), DbType.String),
                    CreateParameter("p_timezoneoffset", Convert.ToInt32(settings.timezone.offset.ToString()),
                        DbType.Int32),
                    CreateParameter("p_timezoneisdst", settings.timezone.isdst.ToString() == "false" ? 0 : 1,
                        DbType.Int32),
                    CreateParameter("p_timezonetext", settings.timezone.text.ToString(), DbType.String)
                };

                var dt = Execute(SPNames.SaveSystemSettings, parameters);

                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Result = "OK";
                    var dr = dt.Rows[0];

                    dynamic settingsData = new ExpandoObject();
                    settingsData.isCfrMode = Convert.ToInt32(dr["isCFREnabled"]);
                    settingsData.CelsiusMode = Convert.ToInt32(dr["TemperatureUnit"]);
                    settingsData.debugMode = Convert.ToInt32(dr["debugMode"]);
                    settingsData.language = dr["languageCode"].ToString();
                    settingsData.timezone = new ExpandoObject();
                    settingsData.timezone.value = dr["timezonevalue"].ToString();
                    settingsData.timezone.abbr = dr["timezoneabbr"].ToString();
                    settingsData.timezone.offset = Convert.ToInt32(dr["timezoneoffset"]);
                    settingsData.timezone.isdst = Convert.ToInt32(dr["timezoneisdst"]);
                    settingsData.timezone.text = dr["timezonetext"].ToString();

                    response.Data = settingsData;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in SaveSystemSettings", this, ex);
            }
            return response;
        }

        internal void UpdateUserCustomerId(int userId, int customerId)
        {
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_UserID", userId, DbType.Int32),
                    CreateParameter("p_CustomerId", customerId, DbType.Int32)
                };

                Execute(SPNames.UpdateUserCustomerId, parameters);
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in UpdateUserCustomerId", this, ex);
            }
        }

        internal bool ChangePassword(dynamic user)
        {
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_UserID", Convert.ToInt32(user.userID.ToString()), DbType.Int32),
                    CreateParameter("p_CurrentPassword", user.currentPassword.ToString(), DbType.String),
                    CreateParameter("p_NewPassword", user.newPassword.ToString(), DbType.String),
                };

                var dt = Execute(SPNames.ChangePassword, parameters);

                if (dt != null && dt.Rows.Count > 0)
                {
                    var isChanged = Convert.ToInt32(dt.Rows[0][0]);
                    return isChanged == 1;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in ChangePassword", this, ex);
            }
            return false;
        }

        internal dynamic UpdatePwdConfig(int userId, dynamic passwordExpiry)
        {
            dynamic response = new ExpandoObject();
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_UserID", userId, DbType.Int32),
                    CreateParameter("p_PasswordMinLength",  Convert.ToInt32(passwordExpiry.pwdMinLength.ToString()), DbType.Int32),
                    CreateParameter("p_PasswordExpiryByDays", Convert.ToInt32(passwordExpiry.pwdExpiry.ToString()), DbType.Int32),
                };

                var dt = Execute(SPNames.UpdatePasswordExpiry, parameters);

                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Result = "OK";
                    var dr = dt.Rows[0];

                    dynamic userData = new ExpandoObject();
                    userData.pwdMinLength = Convert.ToInt32(dr["passwordMinLength"]);
                    userData.pwdExpiry = Convert.ToInt32(dr["passwordExpiryByDays"].ToString());

                    response.Data = userData;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in UpdatePwdConfig", this, ex);
            }
            return response;
        }

        internal void DeleteDefinedSensor(int definedSensorId)
        {
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_DefinedSensorID", definedSensorId, DbType.Int32),
                };

                Execute(SPNames.DeleteDefinedSensor, parameters);

            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in DeleteDefinedSensor", this, ex);
            }
        }

        internal void UpdateSensorAlarmNotification(dynamic notifications)
        {
            try
            {
                string notificationFlag = notifications.device.NotificationBy.ToString();
                var receiveEmail = 0;
                var receiveSMS = 0;
                var contactId = Convert.ToInt32(notifications.user.userId.ToString());
                switch (notificationFlag.ToUpper())
                {
                    case "EMAIL":
                        receiveEmail = 1;
                        break;
                    case "SMS":
                        receiveSMS = 1;
                        break;
                    case "EMAIL_SMS":
                        receiveSMS = 1;
                        receiveEmail = 1;
                        break;
                }
                foreach (var sensor in notifications.device.Sensors)
                {
                    var setNotification = Convert.ToInt32(sensor.NotificationsIsOn.ToString());
                    var deviceSensorID = Convert.ToInt32(sensor.ID.ToString());
                    if (setNotification == 1)
                    {
                        var parameters = new List<DbParameter>
                        {
                            CreateParameter("p_DeviceSensorID", deviceSensorID, DbType.Int32),
                            CreateParameter("p_ContactID", contactId, DbType.Int32),
                            CreateParameter("p_ReceiveEmail", receiveEmail, DbType.Int32),
                            CreateParameter("p_ReceiveSMS", receiveSMS, DbType.Int32),
                        };
                        Execute(SPNames.UpdateSensorAlarmNotification, parameters);
                    }
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in UpdateSensorAlarmNotification", this, ex);
            }
        }

        internal void UpdateCalibrationExpiryReminder(int userId, dynamic reminder)
        {
            try
            {
                string reminderInMonth = reminder.reminderMode.ToString();
                var reminderInt = 0;
                switch (reminderInMonth)
                {
                    case "12_MONTH":
                        reminderInt = 12;
                        break;
                    case "9_MONTH":
                        reminderInt =9;
                        break;
                    case "6_MONTH":
                        reminderInt = 6;
                        break;
                    case "3_MONTH":
                        reminderInt = 3;
                        break;
                }

                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_UserID", userId, DbType.Int32),
                    CreateParameter("p_IsReminderOn", Convert.ToInt32(reminder.reminderIsOn.ToString()), DbType.Int32),
                    CreateParameter("p_ReminderInMonth", reminderInt, DbType.Int32),
                };

                Execute(SPNames.UpdateCalibrationExpiryReminder, parameters);

            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in UpdateCalibrationExpiryReminder", this, ex);
            }
        }

        internal void UpdateDefinedSensor(int userId, dynamic definedSensor)
        {
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_UserID", userId, DbType.Int32),
                    CreateParameter("p_DefinedSensorID",
                        definedSensor.sensorId == null
                            ? 0
                            : Convert.ToInt32(definedSensor.sensorId.ToString()), DbType.Int32),
                    CreateParameter("p_BaseSensorID", Convert.ToInt32(definedSensor.baseSensor.baseId.ToString()),
                        DbType.Int32),
                    CreateParameter("p_Digits", Convert.ToInt32(definedSensor.digits.ToString()), DbType.Int32),
                    CreateParameter("p_FamilyTypeID", Convert.ToInt32(definedSensor.deviceFamily.familyId.ToString()),
                        DbType.Int32),
                    CreateParameter("p_Log1", Convert.ToDouble(definedSensor.output1.ToString()), DbType.Double),
                    CreateParameter("p_Log2", Convert.ToDouble(definedSensor.output2.ToString()), DbType.Double),
                    CreateParameter("p_Name", definedSensor.sensorName.ToString(), DbType.String),
                    CreateParameter("p_SensorUnitID",
                        definedSensor.units.unitId.ToString() == "custom"
                            ? 0
                            : Convert.ToInt32(definedSensor.units.unitId.ToString()),
                        DbType.Int32),
                    CreateParameter("p_Ref1", Convert.ToDouble(definedSensor.real1.ToString()), DbType.Double),
                    CreateParameter("p_Ref2", Convert.ToDouble(definedSensor.real2.ToString()), DbType.Double),
                    CreateParameter("p_customUnit", definedSensor.units.unitName.ToString(), DbType.String)
                };

                Execute(SPNames.UpdateDefinedSensor, parameters);
                
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in UpdateDefinedSensor", this, ex);
            }
        }

        internal dynamic SaveUser(dynamic user)
        {
            dynamic response = new ExpandoObject();
            response.Result = "Error";
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_UserID", Convert.ToInt32(user.userID.ToString()), DbType.Int32),
                    CreateParameter("p_UserName", user.userName.ToString(), DbType.String),
                    CreateParameter("p_FirstName", user.firstName.ToString(), DbType.String),
                    CreateParameter("p_LastName", user.lastName.ToString(), DbType.String),
                    CreateParameter("p_Email", user.userEmail.ToString(), DbType.String),
                    CreateParameter("p_countryCode", user.countryCode.ToString(), DbType.String),
                    CreateParameter("p_mobileNumber", user.userMobile.ToString(), DbType.String)
                };

                var dt = Execute(SPNames.SaveUser, parameters);

                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Result = "OK";
                    var privilages = new List<dynamic>();
                    var dr = dt.Rows[0];

                    dynamic userData = new ExpandoObject();
                    userData.userID = Convert.ToInt32(dr["userid"]);
                    userData.userName = dr["Username"].ToString();
                    userData.userEmail = dr["email"].ToString();
                    userData.firstName = dr["firstname"].ToString();
                    userData.lastName = dr["LastName"].ToString();
                    userData.userMobile = dr["mobilenumber"];
                    userData.countryCode = dr["countrycode"];

                    foreach (DataRow dataRow in dt.Rows)
                    {
                        dynamic privilage = new ExpandoObject();
                        privilage.site = dataRow["sitename"].ToString();
                        privilage.level = dataRow["privilagelevel"].ToString();
                        privilages.Add(privilage);
                    }
                    userData.accessPrivileges = privilages;
                    response.Data = userData;
                }
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in SaveUser", this, ex);
            }
            return response;
        }

        internal void UpdateDevicesDisconnection()
        {
            try
            {
                Execute(SPNames.UpdateDeviceConnection);

            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in UpdateDevicesDisconnection", this, ex);
            }
        }

        internal void UpdateDeviceSensorName(int serialNumber, string sensorName, int sensorTypeId)
        {
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_serialNumber", serialNumber, DbType.Int32),
                    CreateParameter("p_SensorAliasName", sensorName, DbType.String),
                    CreateParameter("p_sensorTypeId", sensorTypeId, DbType.Int32),
                };

                Execute(SPNames.UpdateSensorName, parameters);

            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in UpdateDeviceSensorName", this, ex);
            }
        }

        internal void UpdateSensorLastSample(int serialNumber, int sensorTypeId, int sampleTime, double sampleValue)
        {
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_serialNumber", serialNumber, DbType.Int32),
                    CreateParameter("p_SensorTypeID", sensorTypeId, DbType.Int32),
                    CreateParameter("p_LastSampleValue", sampleValue, DbType.Double),
                    CreateParameter("p_LastSampleTime", sampleTime, DbType.Int32)
                };

                Execute(SPNames.UpdateLastSensorValue, parameters);

            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in UpdateSensorLastSample", this, ex);
            }
        }

        internal void UpdateDeviceStatus(int serialNumber, int statusId, int siteId, int isResetDownload, int alarmDelay)
        {
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_serialNumber", serialNumber, DbType.Int32),
                    CreateParameter("p_statusid", statusId, DbType.Int32),
                    CreateParameter("p_siteid", siteId, DbType.Int32),
                    CreateParameter("p_isResetDownload", isResetDownload, DbType.Int32),
                    CreateParameter("p_alarmDelay", alarmDelay, DbType.Int32),
                };

                Execute(SPNames.UpdateDeviceStatus, parameters);

            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in UpdateDeviceStatus", this, ex);
            }
        }

        #endregion
        #region private methods
        private int UpsertMainDevice(DeviceUpsert device)
        {
            var deviceId = 0;
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_SerialNumber", device.Device.Status.SerialNumber, DbType.Int32),
                    CreateParameter("p_UserID", device.UserID, DbType.Int32),
                    CreateParameter("p_Name", device.Device.Status.Comment, DbType.String),
                    CreateParameter("p_DeviceTypeName", device.Device.DeviceTypeName, DbType.String),
                    CreateParameter("p_SiteID", device.SiteID, DbType.Int32),
                    CreateParameter("p_StatusID",device.Status == DeviceManager.Statuses.Connected ? 1 : 2,
                        DbType.Int32),
                    CreateParameter("p_IsRunning", device.IsRunning ? 1 : 0, DbType.Int16),
                    CreateParameter("p_MacAddress", "", DbType.String),
                    CreateParameter("p_FirmwareVersion", device.Device.Status.FirmwareVersion.ToString(2),
                        DbType.Decimal),
                    CreateParameter("p_BuildNumber", device.Device.Status.FirmwareVersion.Build, DbType.Int32),
                    CreateParameter("p_BatteryLevel",
                        device.Device.Battery != null ? device.Device.Battery.BatteryLevel : 100f, DbType.Double),
                    CreateParameter("p_FirmwareRevision", device.Device.Status.FWRevision, DbType.String),
                    CreateParameter("p_PCBAssembly", device.Device.Status.PCBAssembly, DbType.String),
                    CreateParameter("p_PCBVersion", device.Device.Status.PCBVersion, DbType.String),
                    CreateParameter("p_IsResetDownload", device.IsResetDownload ? 1 : 0, DbType.Int32),
                    CreateParameter("p_AlarmDelay", device.AlarmDelay, DbType.Int32),
                    CreateParameter("p_InCalibrationMode", device.IsInCalibrationMode ? 1 : 0, DbType.Int16)
                };

                var dt = Execute(SPNames.UpsertDevice, parameters);

                if (dt != null && dt.Rows.Count > 0)
                    if (dt.Rows[0][0] != null && dt.Rows[0][0].ToString().Length > 0)
                        deviceId = Convert.ToInt32(dt.Rows[0]["deviceid"]);
            }
            catch(Exception ex)
            {
                Log.Instance.WriteError("Fail in UpsertMainDevice", this, ex);
            }
            return deviceId;
        }

        private bool UpsertMicroLogDevice(int deviceid, MicroLogAPI.Devices.GenericMicroLogLogger microlog)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            bool bSuccess = true;
            try
            {
                bSuccess = false;

                parameters.Add(CreateParameter("p_DeviceID", deviceid, DbType.Int32));
                parameters.Add(CreateParameter("p_AveragePoints", microlog.Status.AveragePoints, DbType.Int32));
                parameters.Add(CreateParameter("p_DeepSleepMode", microlog.Status.DeepSleepMode,DbType.Int32));
                parameters.Add(CreateParameter("p_EnableLEDOnAlarm", microlog.Status.EnableLEDOnAlarm, DbType.Int32));
                parameters.Add(CreateParameter("p_LCDConfiguration", microlog.Status.LCDConfiguration, DbType.Int32));
                parameters.Add(CreateParameter("p_MemorySize", microlog.Status.MemorySize, DbType.Int32));
                parameters.Add(CreateParameter("p_ShowMinMax", microlog.Status.ShowMinMax, DbType.Int32));
                parameters.Add(CreateParameter("p_ShowPast24HMinMax", microlog.Status.ShowPast24HMinMax, DbType.Int32));
                parameters.Add(CreateParameter("p_StopOnDisconnect", microlog.Status.StopOnDisconnect, DbType.Int32));
                parameters.Add(CreateParameter("p_StopOnKeyPress", microlog.Status.StopOnKeyPress, DbType.Int32));

                DataTable dt = Execute(SPNames.UpsertDeviceMicroLog, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    if (dt.Rows[0][0] != null && dt.Rows[0][0].ToString().Length > 0)
                    {
                        bSuccess = true;
                    }
                }

            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in UpsertMicroLogDevice", "DataLayer", ex);
            }
            return bSuccess;
        }

        private bool UpsertMicroXDevice(int deviceid, MicroXLogger microx, string UTC, int loggerUTC)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            bool bSuccess = true;
            try
            {
                bSuccess = false;

                parameters.Add(CreateParameter("p_DeviceID", deviceid, DbType.Int32));
                parameters.Add(CreateParameter("p_CyclicMode", microx.Status.CyclicMode, DbType.Int32));
                parameters.Add(CreateParameter("p_PushToRun",  microx.Status.PushToRunMode, DbType.Int32));
                parameters.Add(CreateParameter("p_TimerRunEnabled",  microx.Status.TimerRunEnabled, DbType.Int32));
                parameters.Add(CreateParameter("p_TimerStart", microx.Status.TimerRunEnabled && microx.Status.TimerStart.Date.Year > 1970 && microx.Status.TimerStart.Date.Year <= DateTime.Now.Year ? Convert.ToInt32(microx.Status.TimerStart.Subtract(new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds) : 0, DbType.Int32));
                parameters.Add(CreateParameter("p_BoomerangEnabled",  microx.Status.BoomerangEnabled, DbType.Int32));
                parameters.Add(CreateParameter("p_BoomerangAuthor", microx.Status.BoomerangEnabled ? microx.Status.Boomerang.Author : "", DbType.String));
                parameters.Add(CreateParameter("p_BoomerangCelsiusMode", microx.Status.Boomerang.CelsiusMode ? 1 : 0, DbType.Int32));
                parameters.Add(CreateParameter("p_BoomerangComment", microx.Status.BoomerangEnabled ? microx.Status.Boomerang.Comment : "", DbType.String));
                parameters.Add(CreateParameter("p_BoomerangContacts", microx.Status.BoomerangEnabled ? extractEmails(microx.Status.Boomerang.Contacts) : "", DbType.String));
                parameters.Add(CreateParameter("p_BoomerangDisplayAlarmLevels", microx.Status.BoomerangEnabled && microx.Status.Boomerang.DisplayAlarmLevels, DbType.Int32));
                parameters.Add(CreateParameter("p_BoomerangUTC", UTC, DbType.String));
                parameters.Add(CreateParameter("p_LoggerUTC", loggerUTC, DbType.Int32));

                DataTable dt = Execute(SPNames.UpsertMicroXDevice, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    if (dt.Rows[0][0] != null && dt.Rows[0][0].ToString().Length > 0)
                    {
                        bSuccess = true;
                    }
                }

            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in UpsertMicroXDevice", "DataLayer", ex);
            }
            return bSuccess;
        }

        private bool UpsertDeviceLogger(int deviceid, GenericLogger logger)
        {
            var bSuccess = true;
            try
            {
                bSuccess = false;

                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_DeviceID", deviceid, DbType.Int32),
                    CreateParameter("p_Interval", logger.Status.Interval.TotalSeconds, DbType.Int32),
                    CreateParameter("p_CelsiusMode", logger.Status.FahrenheitMode ? 2 : 1, DbType.Int32)
                };
                var dt = Execute(SPNames.UpsertDeviceLogger, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    if (dt.Rows[0][0] != null && dt.Rows[0][0].ToString().Length > 0)
                    {
                        bSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in UpsertDeviceLogger", "DataLayer", ex);
            }
            return bSuccess;
        }

        private bool UpsertDeviceSensors(int deviceid ,GenericLogger device, List<string> sensorsName)
        {
            
            bool bSuccess = true;
            
            try
            {
                var sensors = device.Sensors.GetAll();
                bSuccess = upsertSensors(deviceid, sensors, sensorsName);
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in UpsertDeviceSensors", "DataLayer", ex);
            }
            return bSuccess;
        }

        private bool upsertSensors(int deviceid, IEnumerable<GenericSensor> sensors, List<string> sensorsName)
        {
            var parameters = new List<DbParameter>();
            var bSuccess = true;
            var genericSensors = sensors as GenericSensor[] ?? sensors.ToArray();
            var enu = genericSensors.GetEnumerator();
            Log.Instance.Write("{0} enabled sensors for device {1}", this, Log.LogSeverity.DEBUG, genericSensors.Count(), deviceid);
            var index = 0;
            
            while (enu.MoveNext())
            {
                parameters.Clear();
                bSuccess = false;
                var sensor = (GenericSensor)enu.Current;

                parameters.Add(CreateParameter("p_DeviceID", deviceid, DbType.Int32));
                parameters.Add(CreateParameter("p_SensorTypeID", (int)sensor.Type, DbType.Int32));
                parameters.Add(CreateParameter("p_SensorUnitName", sensor.Unit.Name.Trim('°', '%').Trim(), DbType.String));
                parameters.Add(CreateParameter("p_IsEnabled", sensor.Enabled, DbType.Int32));
                parameters.Add(CreateParameter("p_IsAlarmEnabled", sensor.Alarm.Enabled ? 1 : 0, DbType.Int32));
                parameters.Add(CreateParameter("p_LowValue", sensor.Alarm.Enabled ? sensor.Alarm.Low : 0, DbType.Double));
                parameters.Add(CreateParameter("p_HighValue", sensor.Alarm.Enabled ? sensor.Alarm.High : 0, DbType.Double));
                parameters.Add(CreateParameter("p_MinValue", sensor.Minimum, DbType.Double));
                parameters.Add(CreateParameter("p_MaxValue", sensor.Maximum, DbType.Double));
                parameters.Add(CreateParameter("p_SensorAliasName", (sensorsName != null && sensorsName[index] != null) ? sensorsName[index] : "", DbType.String));
                parameters.Add(CreateParameter("p_SensorIndex", (int)sensor.Index, DbType.Int16));

                parameters.Add(CreateParameter("p_IsDefinedSensor", sensor.Type == eSensorType.UserDefined ? 1 : 0, DbType.Int32));

                if (sensor.Type == eSensorType.UserDefined)
                {
                    var userDefinedSensor = sensor as UserDefinedSensor;
                    if (userDefinedSensor != null)
                    {
                        parameters.Add(CreateParameter("p_BaseSensorTypeId", (int)userDefinedSensor.UDS.BaseType,
                            DbType.Int32));
                        parameters.Add(CreateParameter("p_DefineSensorName", userDefinedSensor.UDS.Name, DbType.String));
                        parameters.Add(CreateParameter("p_CustomUnitName", userDefinedSensor.UDS.Unit, DbType.String));
                        parameters.Add(CreateParameter("p_DecimalPlaces", (int) userDefinedSensor.SignificantFigures,
                            DbType.Int16));
                        parameters.Add(CreateParameter("p_Gain", userDefinedSensor.UDS.Gain, DbType.Double));
                        parameters.Add(CreateParameter("p_Offset", userDefinedSensor.UDS.Offset, DbType.Double));
                        parameters.Add(CreateParameter("p_Log1", userDefinedSensor.UDS.References[0].Logger,
                            DbType.Double));
                        parameters.Add(CreateParameter("p_Log2", userDefinedSensor.UDS.References[1].Logger,
                            DbType.Double));
                        parameters.Add(CreateParameter("p_Ref1", userDefinedSensor.UDS.References[0].Reference,
                            DbType.Double));
                        parameters.Add(CreateParameter("p_Ref2", userDefinedSensor.UDS.References[1].Reference,
                            DbType.Double));
                    }
                }
                else
                {
                    parameters.Add(CreateParameter("p_BaseSensorTypeId", 0, DbType.Int32));
                    parameters.Add(CreateParameter("p_DefineSensorName", string.Empty, DbType.String));
                    parameters.Add(CreateParameter("p_CustomUnitName", string.Empty, DbType.String));
                    parameters.Add(CreateParameter("p_DecimalPlaces", 0, DbType.Int16));
                    parameters.Add(CreateParameter("p_Gain", 0, DbType.Double));
                    parameters.Add(CreateParameter("p_Offset", 0, DbType.Double));
                    parameters.Add(CreateParameter("p_Log1", 0, DbType.Double));
                    parameters.Add(CreateParameter("p_Log2", 0, DbType.Double));
                    parameters.Add(CreateParameter("p_Ref1", 0, DbType.Double));
                    parameters.Add(CreateParameter("p_Ref2", 0, DbType.Double));
                }

                var dt = Execute(SPNames.UpsertDeviceSensors, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    if (dt.Rows[0][0] != null && dt.Rows[0][0].ToString().Length > 0)
                        bSuccess = true;
                }

                index++;
            }
            return bSuccess;
        }

        private bool UpsertDevicePicoLite(int deviceid, PicoLiteLogger picolite)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            try
            {
                parameters.Add(CreateParameter("p_DeviceID", deviceid, DbType.Int32));
                parameters.Add(CreateParameter("p_LEDConfig", picolite.Status.LEDConfiguration, DbType.Int32));
                if (picolite is PicoLite.Devices.V2.PicoLiteLogger)
                {
                    var picolite2 = (picolite as PicoLite.Devices.V2.PicoLiteLogger);
                    //Log.Instance.Write("PicoLite2 [{0}] run delay at {1} , last runtime was at {2} , last setuptime was at {3}", "DataLayer", Log4Tech.Log.LogSeverity.DEBUG, picolite2.Status.SerialNumber, picolite2.Status.RunDelay, picolite2.Status.RunTime, picolite2.Status.SetupTime);
                    parameters.Add(CreateParameter("p_RunDelay", picolite2.Status.RunDelay, DbType.Int32));
                    parameters.Add(CreateParameter("p_RunTime",
                        picolite2.Status.RunTime.Year > 1970
                            ? Convert.ToInt32(
                                picolite2.Status.RunTime.Subtract(new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds)
                            : 0, DbType.Int32));
                    parameters.Add(CreateParameter("p_SetupTime",
                        picolite2.Status.SetupTime.Year > 1970
                            ? Convert.ToInt32(
                                picolite2.Status.SetupTime.Subtract(new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds)
                            : 0, DbType.Int32));
                    parameters.Add(CreateParameter("p_StopOnKeyPress", picolite2.Status.StopOnKeyPress, DbType.Int32));
                }
                else
                {
                    parameters.Add(CreateParameter("p_RunDelay", 0, DbType.Int32));
                    parameters.Add(CreateParameter("p_RunTime",  0, DbType.Int32));
                    parameters.Add(CreateParameter("p_SetupTime",  0, DbType.Int32));
                    parameters.Add(CreateParameter("p_StopOnKeyPress", 0, DbType.Int32));
                }
                

                //first upsert the device
                var dt = Execute(SPNames.UpsertPicoLite, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    if (dt.Rows[0][0] != null && dt.Rows[0][0].ToString().Length > 0)
                    {
                        if (Convert.ToInt32(dt.Rows[0][0]) > 0)
                        {
                            return true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in UpsertDevicePicoLite", "DataLayer", ex);
            }
            return false;
        }

        private bool UpsertMicroLiteDevice(int deviceid, MicroLogAPI.Devices.MicroLiteLogger microLiteDevice)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            try
            {
                parameters.Add(CreateParameter("p_DeviceID", deviceid, DbType.Int32));
                parameters.Add(CreateParameter("p_ShowMinMax", microLiteDevice.Status.ShowMinMax, DbType.Int16));
                parameters.Add(CreateParameter("p_ShowPast24H", microLiteDevice.Status.ShowPast24HMinMax, DbType.Int16));
                parameters.Add(CreateParameter("p_MemorySize", microLiteDevice.Status.MemorySize, DbType.Int32));
                parameters.Add(CreateParameter("p_EnableLEDOnAlarm", microLiteDevice.Status.EnableLEDOnAlarm, DbType.Int16));
                parameters.Add(CreateParameter("p_LCDConfig", microLiteDevice.Status.LCDConfiguration, DbType.Int16));
                parameters.Add(CreateParameter("p_StopOnDisconnect", microLiteDevice.Status.StopOnDisconnect, DbType.Int16));
                parameters.Add(CreateParameter("p_StopOnKeyPress", microLiteDevice.Status.StopOnKeyPress, DbType.Int16));
                parameters.Add(CreateParameter("p_UnitRequiresSetup", microLiteDevice.Status.UnitRequiresSetup, DbType.Int16));
                parameters.Add(CreateParameter("p_AveragePoints", microLiteDevice.Status.AveragePoints, DbType.Int16));

                //first upsert the device
                DataTable dt = Execute(SPNames.UpsertDeviceMicroLite, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    if (dt.Rows[0][0] != null && dt.Rows[0][0].ToString().Length > 0)
                    {
                        if (Convert.ToInt32(dt.Rows[0][0]) > 0)
                        {
                            return true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in UpsertMicroLiteDevice", "DataLayer", ex);
            }
            return false;
        }

        private string extractEmails(List<Base.Misc.BasicContact> contacts)
        {
            StringBuilder sb = new StringBuilder();

            try
            {
                if (contacts.Count > 0)
                {
                    List<Base.Misc.BasicContact>.Enumerator enu = contacts.GetEnumerator();

                    while (enu.MoveNext())
                    {
                        sb.AppendFormat("{0},", enu.Current.EMail);
                    }
                    sb = sb.Remove(sb.Length - 1, 1);
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in extractEmails", "DataLayer", ex);
            }
            return sb.ToString();
        }

        private void UpdateContactGroup(int contactId, int groupId)
        {
            try
            {
                var parameters = new List<DbParameter>
                {
                    CreateParameter("p_ContactID", contactId, DbType.Int32),
                    CreateParameter("p_GroupID", groupId, DbType.Int32)
                };

                Execute(SPNames.UpsertContactGroup, parameters);
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in UpdateContactGroup", "DataLayer", ex);
            }
        }
        #endregion

        
       
    }
}
