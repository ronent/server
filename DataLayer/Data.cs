﻿using Base.Sensors.Management;
using Base.Sensors.Types;
using Infrastructure;
using Infrastructure.DAL;
using Infrastructure.Entities;
using Infrastructure.Interfaces;
using Infrastructure.Responses;
using MicroXBase.Devices.Types;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Server.Infrastructure.Interfaces;

namespace DataLayer
{
    public class Data : IData
    {

        private DBInsert dbInsert;
        private DBGet dbGet;
        private DBUpdate dbUpdate;

        public Data()
        {
            // Get Application settings Key Name for the connection string (see App.confid)
            //var key = "SQLConnection";
            var key = "MySQLConnection";
//            switch (Environment.MachineName)
//            {
//                case "GUYK-LAP":
//                    key = "SQLConnectionLocal";
//                    break;
//                    
//                case "FOURTECDEV":
//                    key = "SQLConnectionFourtecDev";
//                    break;
//                 case "YUVALK-PC":
//                    key = "SQLConnectionFourtecYuval";
//                    break;
//           }
//            
            // Get Database connection string
            var cs = ConfigurationManager.AppSettings[key];

            // Init database connections
//            dbInsert = new DBInsert(cs, DBProviders.MSSQL);
//            dbUpdate = new DBUpdate(cs, DBProviders.MSSQL);
//            dbGet = new DBGet(cs, DBProviders.MSSQL);

            dbInsert = new DBInsert(cs, DBProviders.MySQL);
            dbUpdate = new DBUpdate(cs, DBProviders.MySQL);
            dbGet = new DBGet(cs, DBProviders.MySQL);
        }

        #region IData

        public dynamic CreateNewCustomer(dynamic newCustomer)
        {
            return dbInsert.CreateNewCustomer(newCustomer);
        }

        public PostBack CreateNewAdminUser(AdminUser newUser)
        {
            return dbInsert.createNewAdminUser(newUser);
        }

//        public dynamic CreateUser(BasicUser user)
//        {
//            return dbInsert.createUser(user);
//        }

        public bool UserCFRCheck(Int32 userid, String command, String entity, Int32 serialNumber = 0, String reason = "")
        {
            return dbInsert.userCFRCheck(userid, command, entity, serialNumber, reason);
        }

        public Response RegsiterLanRcv(LanRcvRegisteration lanRcv)
        {
            return dbInsert.registerLanRcv(lanRcv);
        }

        public dynamic ValidateLogin(string userName,string password)
        {
            return dbGet.ValidateLogin(userName, password);
        }

        public dynamic GetUserSites(int userID)
        {
            return dbGet.GetUserSites(userID);
        }

        public dynamic GetSiteById(int siteID)
        {
            return dbGet.GetSiteById(siteID);
        }

        public dynamic GetGroupPermissions()
        {
            return dbGet.GetGroupPermissions();
        }

        public dynamic SetupUser(dynamic userData)
        {
            return dbUpdate.SetupUser(userData);
        }

        public dynamic GetLanguages()
        {
            return dbGet.GetLanguages();
        }

        public dynamic GetSecureQuestions()
        {
            return dbGet.GetSecureQuestions();
        }

        public dynamic GetDevicesBySite(int siteID, int userID)
        {
            return dbGet.GetDevicesBySite(siteID, userID);
        }

        public ListCategories GetCategories()
        {
            return dbGet.GetCategories();
        }

        public dynamic GetDevicesByCategory(int categoryID, int userID)
        {
            return dbGet.GetDevicesByCategory(categoryID, userID);
        }

//        public DeviceList GetDevices(DeviceIDs parameters)
//        {
//            return dbGet.GetDevices(parameters);
//        }

        public dynamic GetDevice(int serialNumber)
        {
            return dbGet.GetDevice(serialNumber);
        }

        public dynamic GetUsers(int userId)
        {
            return dbGet.GetUsers(userId);
        }

        public void DeleteUser(int userId)
        {
            dbUpdate.DeleteUser(userId);
        }

        public dynamic CreateSite(dynamic site)
        {
            return dbInsert.CreateSite(site);
        }

        public Response CreateContact(Contact contact)
        {
            return dbInsert.CreateContact(contact);
        }

        public ContactList GetContacts(SingleContact contact)
        {
            return dbGet.GetContacts(contact);
        }

        public Response UpdateContact(Contact contact)
        {
            return dbUpdate.UpdateContact(contact);
        }

        public Response UpdateUser(User user)
        {
            return dbUpdate.UpdateUser(user);
        }

        public dynamic DeleteContact(int contactId)
        {
            return dbUpdate.DeleteContact(contactId);
        }

        public ResponseSensorLogs GetLastSensorLogs(SingleSensor sensor)
        {
            return dbGet.GetLastSensorLogs(sensor);
        }

        public bool isCommandAllowed(UserCommand cmd)
        {
            return dbGet.isCommandAllowed(cmd);
        }

        public Response UpsertDevice(DeviceUpsert device)
        {
            return dbUpdate.UpsertDevice(device);
        }

        public void SystemActionsAudit(int userid, string command, string entity, int serialnumber)
        {
            dbInsert.SystemActionsAudit(userid, command, entity, serialnumber);
        }

        public dynamic GetAllowedUsers(int serialNumber)
        {
            return dbGet.GetAllowedUsers(serialNumber);
        }

        public Int32 GetDeviceSensorID(int serialNumber, int sensorTypeID)
        {
            return dbGet.GetDeviceSensorID(serialNumber, sensorTypeID);
        }

        public void InsertSensorDownloadHistory(int serialNumber, int sensorTypeID, int firstDownloadTime, int lastDownloadTime)
        {
            dbInsert.InsertSensorDownloadHistory(serialNumber,sensorTypeID, firstDownloadTime, lastDownloadTime);
        }

        public dynamic GetLastSensorDownload(int serialNumber, int sensorTypeID)
        {
            return dbGet.GetLastSensorDownload(serialNumber, sensorTypeID);
        }

        public List<dynamic> GetDeviceSensorsID(int serialNumber)
        {
            return dbGet.GetDeviceSensorsID(serialNumber);
        }

        public int GetNextSerialNumber(int AllocateNumber)
        {
            return dbGet.GetNextSerialNumber(AllocateNumber);
        }

        public dynamic PasswordRecovery(dynamic email)
        {
            return dbGet.PasswordRecovery(email);
        }

        public dynamic SecureAnswerCheck(dynamic answer)
        {
            return dbGet.SecureAnswerCheck(answer);
        }

        public dynamic ResetPassword(dynamic reset)
        {
            return dbGet.ResetPassword(reset);
        }

        public void SaveReport(int serialNumber, string S3Url, int reportProfileId)
        {
            dbInsert.SaveReport(serialNumber,S3Url, reportProfileId);
        }

        public dynamic SaveAlarmNotification(dynamic alarmNotifications)
        {
            return dbUpdate.SaveAlarmNotification(alarmNotifications);
        }

        public dynamic GetNotificationContacts(string serialNumber, int sensorTypeID)
        {
            return dbGet.GetNotificationContacts(serialNumber, sensorTypeID);
        }

        public void UpdateNotificationEmailSent(string serialNumber, int contactID, int sensorTypeID)
        {
            dbUpdate.UpdateNotificationEmailSent(serialNumber, contactID, sensorTypeID);
        }

        public void UpdateNotificationBatteryEmailSent(string serialNumber, int contactID)
        {
            dbUpdate.UpdateNotificationBatteryEmailSent(serialNumber, contactID);
        }

        public void UpdateNotificationSMSSent(string serialNumber, int contactID, string MsgId, int sensorTypeID)
        {
            dbUpdate.UpdateNotificationSMSSent(serialNumber, contactID, MsgId, sensorTypeID);
        }

        public void UpdateNotificationBatterySMSSent(string serialNumber, int contactID, string MsgId)
        {
            dbUpdate.UpdateNotificationBatterySMSSent(serialNumber, contactID,MsgId);
        }

        public  dynamic GetCustomer(string email)
        {
            return dbGet.GetCustomer(email);
        }

        public void UpdateSMSStatus(string smsID, string smsStatus)
        {
            dbUpdate.UpdateSMSStatus(smsID, smsStatus);
        }

        public dynamic GetDevicesByUser(int userID)
        {
            return dbGet.GetDevicesByUser(userID);
        }

        public List<int> GetDevicesByUser(string userid)
        {
            return dbGet.GetDevicesByUser(userid);
        }

        public dynamic InsertAlarmNotification(string serialNumber, DataManager.AlarmType alarmType, double value, DateTime startTime, eSensorType sensorType)
        {
           return dbInsert.InsertAlarmNotification(serialNumber,alarmType,value,startTime,sensorType);
        }

        public void UpdateAlarmNotification(string serialNumber, DataManager.AlarmType alarmType, eSensorType sensorType)
        {
            dbUpdate.UpdateAlarmNotification(serialNumber, alarmType, sensorType);
        }

        public dynamic GetAlarmNotifications(int userid)
        {
            return dbGet.GetAlarmNotifications(userid);
        }

        public void UpdateAlarmReason(int alarmID, int serialNumber, int alarmTypeID, string reason, eSensorType sensorType)
        {
            dbUpdate.UpdateAlarmReason(alarmID, serialNumber, alarmTypeID, reason, sensorType);
        }

        public int[] AllAlarmedDevicesSerialNumbers(int userId)
        {
            return dbGet.AllAlarmedDevicesSerialNumbers(userId);
        }

        public int DeviceSerialNumberByAlarmID(int alarm_id)
        {
            return dbGet.DeviceSerialNumberByAlarmID(alarm_id);
        }

        public void HideAlarm(string serialNumber, int alarmTypeID, eSensorType sensorType)
        {
            dbUpdate.HideAlarm(serialNumber, alarmTypeID, sensorType);
        }

        public void HideAlarmByID(int id)
        {
            dbUpdate.HideAlarmByID(id);
        }
        
        public void HideAllAlarms(int userID)
        {
            dbUpdate.HideAllAlarms(userID);
        }

        public DateTime GetLastStoredSampleTime(int serialNumber)
        {
            return dbGet.GetLastStoredSampleTime(serialNumber);
        }

        public void SetLastStoredSampleTime(int serialNumber, DateTime dt)
        {
            dbGet.SetLastStoredSampleTime(serialNumber, dt);
        }

        public bool GetSensorLocatorByID(int deviceSensorID, out int serialNumber, out int sensorTypeID)
        {
            return dbGet.GetSensorLocatorByID(deviceSensorID, out serialNumber, out sensorTypeID);
        }

        public dynamic ReadMKTSensorData(int userID, int sensorID)
        {
            return dbGet.ReadMKTSensorData(userID, sensorID);
        }

        public void SaveMKTSensorData(int userID, int sensorID, double low, double high, double activationEnergy)
        {
            dbUpdate.SaveMKTSensorData(userID, sensorID, low, high, activationEnergy);
        }

        public dynamic GetAnalyticsSensors(int userid)
        {
            return dbGet.GetAnalyticsSensors(userid);
        }

        public void AddAnalyticsSensor(int userID, int sensorID, int isActive =1)
        {
            dbInsert.AddAnalyticsSensor(userID, sensorID, isActive);
        }

        public void RemoveAnalyticsSensor(int userID, int sensorID)
        {
            dbUpdate.RemoveAnalyticsSensor(userID, sensorID);
        }

        public void RemoveAnalyticsSensors(int userId)
        {
            dbUpdate.RemoveAnalyticsSensors(userId);
        }
        
        public dynamic UpdateSite(dynamic site)
        {
            return dbUpdate.UpdateSite(site);
        }

        public void SnoozeAlarm(string serialNumber, int deviceSensorID, int silent)
        {
            dbUpdate.SnoozeAlarm(serialNumber, deviceSensorID, silent);
        }

        public dynamic GetCustomerStorage(int serialNumber)
        {
            return dbGet.GetCustomerStorage(serialNumber);
        }

        public  dynamic GetDeviceSensorsDownloaded(int serialNumber)
        {
            return dbGet.GetDeviceSensorsDownloaded(serialNumber);
        }

        public void RelateDeviceToSite(int serialNumber, int siteID)
        {
            dbUpdate.RelateDeviceToSite(serialNumber, siteID);
        }

        public string GetDeviceFirmwareVersion(int serialNumber)
        {
            return dbGet.GetDeviceFirmwareVersion(serialNumber);
        }

        public dynamic GetDefinedSensorsByDevice(int serialNumber)
        {
            return dbGet.GetDefinedSensorsByDevice(serialNumber);
        }

        public dynamic GetCustomerDisableStatus(int serialNumber)
        {
            return dbGet.GetCustomerDisableStatus(serialNumber);
        }

        public void UpdateCustomerEmailSent(int serialNumber)
        {
            dbUpdate.UpdateCustomerEmailSent(serialNumber);
        }

        public dynamic GetAllCustomerSerialNumbers(int serialNumber)
        {
            return dbGet.GetAllCustomerSerialNumbers(serialNumber);
        }

        public int GetDeviceTimeZone(int serialNumber)
        {
            return dbGet.GetDeviceTimeZone(serialNumber);
        }

        public void UpdateMongoDBBackup(int serialNumber)
        {
            dbUpdate.UpdateMongoDBBackup(serialNumber);
        }

        public void UpdateDeviceAlarmData(int serialNumber, IEnumerable<GenericSensor> sensors)
        {
            dbUpdate.UpdateDeviceAlarmData(serialNumber, sensors);
        }

        public dynamic DeleteSite(dynamic site)
        {
           return dbUpdate.DeleteSite(site);
        }

        public int GetCustomerID(int serialNumber)
        {
            return dbGet.GetCustomerID(serialNumber);
        }

        public int GetCustomerBySensor(int deviceSensorId)
        {
            return dbGet.GetCustomerBySensor(deviceSensorId);
        }

        public dynamic GetReportsArchive(int userId, int start, int end)
        {
            return dbGet.GetReportsArchive(userId,start,end);
        }

        public void DeleteReport(int reportId)
        {
            dbUpdate.DeleteReport(reportId);
        }

        public dynamic GetReportsReviews(int userId, int reportType)
        {
            return dbGet.GetReportsReviews(userId, reportType);
        }

        public dynamic ApproveReport(int userId, int reportDeviceHistoryId)
        {
            return dbUpdate.ApproveReport(userId, reportDeviceHistoryId);
        }

        public dynamic AddProfileReport(int userId, dynamic profile, bool isNew = true)
        {
            return dbInsert.AddProfileReport(userId,profile,isNew);
        }

        public dynamic GetApprovalsReviewersList(int userId)
        {
            return dbGet.GetApprovalsReviewersList(userId);
        }

        public dynamic GetDistributionList(int userId)
        {
            return dbGet.GetDistributionList(userId);
        }

        public void AddReportDevice(int reportId, int serialNumber)
        {
            dbInsert.AddReportDevice(reportId, serialNumber);
        }

        public dynamic GetReportProfiles(int userId, string filter)
        {
            return dbGet.GetReportProfiles(userId, filter);
        }

        public dynamic GetUser(int userId)
        {
            return dbGet.GetUser(userId);
        }

        public dynamic GetCustomerBalance(int userId)
        {
            return dbGet.GetCustomerBalance(userId);
        }

        public dynamic SaveUser(dynamic user)
        {
            return dbUpdate.SaveUser(user);
        }

        public bool  ChangePassword(dynamic user)
        {
            return dbUpdate.ChangePassword(user);
        }

        public dynamic GetSystemSettings(int userId)
        {
            return dbGet.GetSystemSettings(userId);
        }

        public dynamic SaveSystemSettings(dynamic settings)
        {
            return dbUpdate.SaveSystemSettings(settings);
        }

        public dynamic GetDeviceAutoSetup(int userId)
        {
            return dbGet.GetDeviceAutoSetup(userId);
        }

        public void AddDeviceSetupFile(dynamic setupFile)
        {
            dbInsert.AddDeviceSetupFile(setupFile);
        }

        public dynamic GetDeviceSetupFiles(int userId)
        {
            return dbGet.GetDeviceSetupFiles(userId);
        }

        public dynamic ApplyDeviceAutoSetup(int userId,dynamic setup)
        {
            return dbUpdate.ApplyDeviceAutoSetup(userId,setup);
        }

        public void DeleteSetupFile(int userId, string fileId)
        {
            dbUpdate.DeleteSetupFile(userId, fileId);
        }

        public dynamic GetCalibrationCertificateDefault(int userId)
        {
            return dbGet.GetCalibrationCertificateDefault(userId);
        }

        public dynamic SetCalibrationCertificateDefault(int userId, dynamic calibrationDefault)
        {
            return dbUpdate.SetCalibrationCertificateDefault(userId, calibrationDefault);
        }

        public void UpdateDeviceFwFlag(int serialNumber, int fwUpdateFlag)
        {
            dbUpdate.UpdateDeviceFwFlag(serialNumber,fwUpdateFlag);
        }

        public dynamic GetDefinedSensors(int userId)
        {
            return dbGet.GetDefinedSensors(userId);
        }

        public dynamic GetDeviceFamilyTypes()
        {
            return dbGet.GetDeviceFamilyTypes();
        }

        public dynamic GetSensorUnits(int familyId)
        {
            return dbGet.GetSensorUnits(familyId);
        }

        public dynamic GetBaseSensor(int familyId)
        {
            return dbGet.GetBaseSensor(familyId);
        }

        public void UpdateDefinedSensor(int userId, dynamic definedSensor)
        {
            dbUpdate.UpdateDefinedSensor(userId, definedSensor);
        }

        public void DeleteDefinedSensor(int definedSensorId)
        {
            dbUpdate.DeleteDefinedSensor(definedSensorId);
        }

        public void UpdateSensorAlarmNotification(dynamic notifications)
        {
            dbUpdate.UpdateSensorAlarmNotification(notifications);
        }

        public dynamic GetCalibrationExpiryReminder(int userId)
        {
            return dbGet.GetCalibrationExpiryReminder(userId);
        }

        public void UpdateCalibrationExpiryReminder(int userId, dynamic reminder)
        {
            dbUpdate.UpdateCalibrationExpiryReminder(userId, reminder);
        }

        public dynamic IsDeviceAutoSetup(int serialNumber)
        {
            return dbGet.IsDeviceAutoSetup(serialNumber);
        }

//        public int GetCustomerIdByUser(int userId)
//        {
//            return dbGet.GetCustomerIdByUser(userId);
//        }

        public dynamic GetActivity(int userId)
        {
            return dbGet.GetActivity(userId);
        }

        public dynamic GetPasswordExpiry(int userId)
        {
            return dbGet.GetPasswordExpiry(userId);
        }

        public dynamic GetPrivilegesGroups(int userId)
        {
            return dbGet.GetPrivilegesGroups(userId);
        }

        public dynamic GetMatrix(int userId, int groupLevel)
        {
            return dbGet.GetMatrix(userId,groupLevel);
        }

        public dynamic UpdatePwdConfig(int userId, dynamic passwordExpiry)
        {
            return dbUpdate.UpdatePwdConfig(userId, passwordExpiry);
        }

        public void InsertDevicesBatch(string batchDevices)
        {
            dbInsert.InsertDevicesBatch(batchDevices);
        }

        public dynamic GetAuditTrailData(int userId, int start, int end)
        {
            return dbGet.GetAuditTrailData(userId, start, end);
        }

        public dynamic GetAllContacts(int userId)
        {
            return dbGet.GetAllContacts(userId);
        }

        public dynamic GetGroups(int userId)
        {
            return dbGet.GetGroups(userId);
        }

        public dynamic UpsertContact(int userId, dynamic contactData)
        {
            return dbUpdate.UpsertContact(userId, contactData);
        }

        public void DeleteGroup(int groupId)
        {
            dbUpdate.DeleteGroup(groupId);
        }

        public void UpsertGroup(int userId, dynamic groupData)
        {
            dbUpdate.UpsertGroup(userId, groupData);
        }

        public dynamic GetDevicesByCustomer(int userId)
        {
            return dbGet.GetDevicesByCustomer(userId);
        }

        public void DeAssociateDevices(int serialNumber)
        {
            dbUpdate.DeAssociateDevices(serialNumber);
        }

        public void NewPermissionGroup(int userId, dynamic groupData)
        {
            dbInsert.NewPermissionGroup(userId, groupData);
        }

        public dynamic GetPermissonRoles(int userId)
        {
            return dbGet.GetPermissonRoles(userId);
        }

        public void SaveMatrix(int userId, dynamic matrixData)
        {
            dbUpdate.SaveMatrix(userId, matrixData);
        }

        public dynamic AddUser(int userId, dynamic userData)
        {
            return dbInsert.createUser(userId, userData);
        }

        public dynamic GetUserRecover(int userId)
        {
            return dbGet.GetUserRecover(userId);
        }

        public dynamic GetDeviceAlarmNotifications(int contactId)
        {
            return dbGet.GetDeviceAlarmNotifications(contactId);
        }

        public dynamic UserGuidIsValid(string guid)
        {
            return dbGet.UserGuidIsValid(guid);
        }

        public dynamic SignupCustomer(string email)
        {
            return dbInsert.SignupCustomer(email);
        }

        public dynamic CustomerGuidIsValid(string guid)
        {
            return dbGet.CustomerGuidIsValid(guid);
        }

        public void AdminUpdateUser(int userId, dynamic userData)
        {
            dbUpdate.AdminUpdateUser(userId, userData);
        }

        public int GetCountryCallingCode(string countryCode)
        {
            return dbGet.GetCountryCallingCode(countryCode);
        }

        public dynamic ResetUserPassword(dynamic newPasswordData)
        {
            return dbUpdate.ResetUserPassword(newPasswordData);
        }

        public dynamic GetUserByUserName(string userName)
        {
            return dbGet.GetUserByUserName(userName);
        }

        public dynamic GetCustomers()
        {
            return dbGet.GetCustomers();
        }

        public void UpdateUserCustomerId(int userId, int customerId)
        {
            dbUpdate.UpdateUserCustomerId(userId, customerId);
        }

        public void UpdateDeviceStatus(int serialNumber, int statusId, int siteId, int isResetDownload, int alarmDelay)
        {
            dbUpdate.UpdateDeviceStatus(serialNumber, statusId, siteId, isResetDownload, alarmDelay);
        }

        public void UpdateDeviceSensorName(int serialNumber, string sensorName, int sensorTypeId)
        {
            dbUpdate.UpdateDeviceSensorName(serialNumber,sensorName,sensorTypeId);
        }

        public void UpdateDevicesDisconnection()
        {
            dbUpdate.UpdateDevicesDisconnection();
        }

        public void UpdateSensorLastSample(int serialNumber, int sensorTypeId, int sampleTime, double sampleValue)
        {
            dbUpdate.UpdateSensorLastSample(serialNumber, sensorTypeId, sampleTime, sampleValue);
        }

        public dynamic GetAlarmNotificationsBySerialNumber(int serialNumber)
        {
            return dbGet.GetAlarmNotificationsBySerialNumber(serialNumber);
        }

        public dynamic GetReportEmails(int reportProfileId)
        {
            return dbGet.GetReportEmails(reportProfileId);
        }

        public dynamic GetReportsForExecution()
        {
            return dbGet.GetReportsForExecution();
        }
        #endregion
    }
}
