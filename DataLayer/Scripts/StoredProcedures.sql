USE [Fourtec]
GO
/****** Object:  StoredProcedure [dbo].[spAddDeviceSetupFile]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spAddDeviceSetupFile]
	@UserID int,
	@DeviceType varchar(255),
	@SetupID varchar(255),
	@SetupName varchar(50)
AS
BEGIN
	
	SET NOCOUNT ON;

	Declare @CustomerID int
	declare @DeviceTypeID int

	select @DeviceTypeID = id
	from DeviceTypes
	where name=@DeviceType

	select @CustomerID=customerid
	from users
	where id=@UserID

    Insert into DeviceSetupFiles(CustomerID,DeviceTypeID,SetupID, SetupName)
	values(@CustomerID, @DeviceTypeID,@SetupID,@SetupName)


END

GO
/****** Object:  StoredProcedure [dbo].[spAddGroup]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spAddGroup]
	@UserID int,
	@GroupID int,
	@GroupName varchar(50)
AS
BEGIN
	SET NOCOUNT ON;

	declare @CustomerID int

	select @CustomerID = customerid
	from users
	where id=@userid

	if @GroupID>0
	begin
		update groups
		set name=@GroupName
		where id=@GroupID

		delete from ContactDistributionGroups
		where GroupID = @GroupID

		select @GroupID
	end
	else
	begin

		insert into groups(name,customerid)
		values(@GroupName, @CustomerID)

		select @@IDENTITY
	end
END

GO
/****** Object:  StoredProcedure [dbo].[spAddNewSample]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spAddNewSample]
	@DeviceSensorID int,
	@Value float,
	@SampleTime int,
	@IsTimeStamp tinyint,
	@Comment nvarchar(50),
	@AlarmStatus tinyint,
	@IsDummy tinyint=0
AS

BEGIN
	SET NOCOUNT ON;

	declare @return tinyint

	set @return=0

	if not exists(select top 1 id from sensorlogs where devicesensorid=@DeviceSensorID and value=@Value and SampleTime=@SampleTime)
	begin
		insert into sensorlogs(devicesensorid, value, sampletime, istimestamp, comment, alarmstatus,IsDummy)
		values(@DeviceSensorID,@Value,@SampleTime,@IsTimeStamp,@Comment,@AlarmStatus,@IsDummy)

		set @return=1
	end
	
	select @return
END


GO
/****** Object:  StoredProcedure [dbo].[spAddPermissionGroup]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spAddPermissionGroup]
	@UserID int,
	@Name varchar(255),
	@ParentName varchar(255)
AS
BEGIN
	SET NOCOUNT ON;

	declare @ParentID int
	declare @CustomerID int
	declare @LevelID int

	select @CustomerID=customerid
	from Users
	where id=@UserID

	select @ParentID = id,@LevelID=levelid
	from PermissionGroups
	where customerID = 0
	and name =@ParentName

	if @ParentID>0
	begin
		insert into PermissionGroups(name,IsAdmin,ParentID,LevelID,IsReadOnly,CustomerID)
		values(@Name,0,@ParentID,@LevelID,0,@CustomerID)
	end

END

GO
/****** Object:  StoredProcedure [dbo].[spAddReportApprovers]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spAddReportApprovers]
    @ReportTemplateID int,
	@UserID int
AS
BEGIN
	SET NOCOUNT ON;

	if exists (select top 1 ReportTemplateID from ReportTemplateProcessUsers where ReportTemplateID=@ReportTemplateID)
	begin
		delete from ReportTemplateProcessUsers where ReportTemplateID=@ReportTemplateID
	end
	
	insert into ReportTemplateProcessUsers(ReportTemplateID,UserId,ProcessTypeId)
	values(@ReportTemplateID,@UserID,2)
	

END

GO
/****** Object:  StoredProcedure [dbo].[spAddReportContact]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spAddReportContact]
    @ReportTemplateID int,
	@ContactID int
AS
BEGIN
	SET NOCOUNT ON;

	if exists (select top 1 ContactID from ReportContacts where ReportTemplateID=@ReportTemplateID)
	begin
		delete from ReportContacts where ReportTemplateID=@ReportTemplateID
	end
	
	insert into ReportContacts(ReportTemplateID,ContactID)
	values(@ReportTemplateID,@ContactID)
	

END

GO
/****** Object:  StoredProcedure [dbo].[spAddReportDevice]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spAddReportDevice]
    @ReportTemplateID int,
	@SerialNumber int
AS
BEGIN
	SET NOCOUNT ON;

	if exists (select top 1 id from ReportDevices where ReportTemplateID=@ReportTemplateID)
	begin
		delete from ReportDevices where ReportTemplateID=@ReportTemplateID
	end
	
	insert into ReportDevices(ReportTemplateID,SerialNumber)
	values(@ReportTemplateID,@SerialNumber)
	

END

GO
/****** Object:  StoredProcedure [dbo].[spAddReportReviewer]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[spAddReportReviewer]
    @ReportTemplateID int,
	@UserID int
AS
BEGIN
	SET NOCOUNT ON;

	
	
	insert into ReportTemplateProcessUsers(ReportTemplateID,UserId,ProcessTypeId)
	values(@ReportTemplateID,@UserID,1)
	

END

GO
/****** Object:  StoredProcedure [dbo].[spAdminCreateUser]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spAdminCreateUser] 
	@AdminUserID INT,
	@PermissionRoleID INT,
	@InactivityTimeout int,
	@PermissionGroupID int =0 ,
	@Email nvarchar(255),
	@Reason NVARCHAR(500) = ''
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @CustomerID INT;
	declare @languageID int;
	declare @userID int;
	--declare @IsCFREnabled int;
	declare @ActionID int;
	declare @PermissionActionID int
	declare @MaxUsersAllowed int
	declare @UsersCount int

	set @userID = 0;
	--set @IsCFREnabled =0;

	-- get the customer id based on the user admin
    SELECT @CustomerID = dbo.Users.CustomerID
	FROM dbo.Users
	WHERE ID=@AdminUserID;

	--get the langauge default
	select @languageID = Languages.ID
	from Languages
	where Languages.isDefault=1;

	--checks if the customer is not over quota
	select @MaxUsersAllowed=MaxUsersAllowed
	from customersettings
	where customersettings.customerid=@CustomerID

	select @UsersCount=count(id)
	from users
	where customerid=@CustomerID
	and isnull(deleted,0)=0

	if (@CustomerID > 0 and @UsersCount<@MaxUsersAllowed)

	begin
		--insert the new user with the related customer
		insert Users(CustomerID,RoleID,PermissionGroupID,Email,LanguageID,defaultDateFormat,PasswordResetGUID,PasswordResetExpiry,isactive,InactivityTimeout)
		values(@customerID, @PermissionRoleID,@PermissionGroupID,@Email,@languageID,'yyyy-MM-dd',NEWID(),dateadd(day,7,getdate()),0,@InactivityTimeout);

		set @userID = @@IDENTITY

		select @PermissionActionID=id
		from PermissionActions
		where command='create'
		and entity='users'
		and iscfraction=1

		--CFR
		exec spCFRAuditTrail @AdminUserID,@PermissionActionID,0, @Reason

	end

	select id userid,PasswordResetGUID
	from users
	where id=@userID



END

GO
/****** Object:  StoredProcedure [dbo].[spApproveReport]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[spApproveReport]
	@UserID int,
	@ReportDeviceHistoryID int
AS
BEGIN
	SET NOCOUNT ON;

	update ReportProcessUsers
	set isConfirmed=1
	where ReportDeviceHistoryID = @ReportDeviceHistoryID
	and UserID = @UserID
END

GO
/****** Object:  StoredProcedure [dbo].[spCFRAuditTrail]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spCFRAuditTrail]
	@UserID int,
	@Command nvarchar(50),
	@Entity nvarchar(50),
	@SerialNumber int = 0,
	@Reason nvarchar(500)=''
AS
BEGIN
	SET NOCOUNT ON;

	declare @IsCFREnabled int
	declare @PermissionActionID int
	declare @DeviceID int

	select @DeviceId=ID
	from devices
	where serialnumber=@SerialNumber

	select @PermissionActionID=id
	from PermissionActions
	where Command=@Command
	and Entity=@Entity
	and IsCFRAction=1

	--checks if need to audit trail this action
	SELECT @IsCFREnabled =dbo.fnIsCFREnabled(@UserID);
    
	IF @IsCFREnabled = 1 and @PermissionActionID>0
	BEGIN
			
		INSERT dbo.AuditTrail
				( UserID ,
					PermissionActionID ,
					ActivityTimeStamp ,
					DeviceID ,
					Reason
				)
		VALUES  (   @UserID ,
					@PermissionActionID , 
					GETDATE() , 
					@DeviceID , 
					@Reason 
				); 

	END  --CFR Enabled

	-- select @@RowCount;


END

GO
/****** Object:  StoredProcedure [dbo].[spChangePassword]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spChangePassword]
	@UserID int,
	@CurrentPassword varchar(32),
	@NewPassword varchar(32)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if exists(select top 1 id from users where id=@UserID and UserPassword=@CurrentPassword)
	begin
		update users
		set UserPassword=@NewPassword
		where id=@userID

		select 1
	end
	else
	begin 
		select 0
	end

   
END

GO
/****** Object:  StoredProcedure [dbo].[spCleanDataTables]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spCleanDataTables]

as

begin

truncate table alarmnotifications

truncate table audittrail

truncate table audittrailarchive

truncate table definedsensors

truncate table devicelogger

truncate table devicemicrolite

truncate table devicemicrolite

truncate table devicemicrox

truncate table devicepicolite

delete from  SensorAlarm

truncate table sensordownloadhistory

delete from devicesensors

delete from devices


delete from PermissionUserSiteDeviceGroup
where siteid not in(select id from sites where parentid=0)

delete from sites
where ParentID<>0

end
GO
/****** Object:  StoredProcedure [dbo].[spCreateAdminUser]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spCreateAdminUser]
	@GUID uniqueidentifier,
	@UserName nvarchar(50),
	@UserPassword nvarchar(255),
	@Email nvarchar(255),
	@QuestionID int,
	@Answer nvarchar(500)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @customerID int;
	declare @groupID int;
	declare @languageID int;
	declare @userID int;
	--init the user id
	set @userID=0;

	--first check the guid if exists
	if exists(select top 1 customers.ID from Customers where Customers.VerificationGUID=@GUID)
	begin
		--then get the customer id	
		select @customerID = Customers.ID
		from Customers
		where Customers.VerificationGUID = @GUID;

		--checks if the user not exists
		if not exists(select top 1 Users.ID 
						from Users 
						where Users.CustomerID = @customerID
						and Users.Username = @UserName)
		begin
			--check for valid question and answer
			if exists (select top 1 SecureQuestions.ID from SecureQuestions where SecureQuestions.ID=@QuestionID)
				and len(@Answer)>0
			begin
				--get the group id with all permissions
				select @groupID = PermissionGroups.ID
				from PermissionGroups
				where PermissionGroups.Name = 'Administrators';
				--get the langauge default
				select @languageID = Languages.ID
				from Languages
				where Languages.isDefault=1;
				--insert the new user with the related customer
				insert Users(Username,CustomerID,UserPassword,IsAdmin,Email,GroupID,LanguageID,PasswordCreationDate)
				values(@UserName,@customerID,@UserPassword,1,@Email,@groupID,@languageID,GETDATE());

				set @userID = @@IDENTITY;

				--insert the secret question and answer
				insert UserSecureQuestions(UserID,QuestionID,Answer)
				values(@userID,@QuestionID,@Answer);
				--save the password in history table
				insert UserPasswordHistory(UserID,UserPassword,ChangedDate)
				values(@userID,@UserPassword,GETDATE());
			end	
		end
	end
	
    select @userID as userid;

	set nocount off;
END

GO
/****** Object:  StoredProcedure [dbo].[spCreateNewContact]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spCreateNewContact]
	@UserID int,
	@Name nvarchar(50),
	@Title nvarchar(10),
	@Phone nvarchar(50),
	@Email nvarchar(255),
	@WeekdayStart int,
	@WorkdayStart time,
	@WorkdayEnd time,
	@SMSResendInterval int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    declare @CustomerID int

	select @CustomerID = users.customerid
	from users
	where users.id = @UserID;
	
	INSERT INTO [dbo].[Contacts]
           (CustomerID
		   ,[Name]
           ,[Title]
           ,[Phone]
           ,[Email]
           ,[WeekDayStart]
           ,[WorkdayStart]
           ,[WorkdayEnd]
           ,[SMSResendInterval]
           )
     VALUES
           (@CustomerID
		   ,@Name
           ,@Title
           ,@Phone
           ,@Email
           ,@WeekdayStart
           ,@WorkdayStart
           ,@WorkdayEnd
           ,@SMSResendInterval
           )

	select @@ROWCOUNT;


END

GO
/****** Object:  StoredProcedure [dbo].[spCreateNewCustomer]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spCreateNewCustomer]
	@GUID varchar(255),
	@CompanyName varchar(255),
	@Address varchar(255),
	@CountryCode varchar(4),
	@FirstName varchar(32),
	@LastName varchar(32),
	@Industry varchar(255),
	@MobileNumber varchar(10),
	@PhoneNumber varchar(10),
	@Prefix varchar(10),
	@timezonevalue varchar(20),
	@timezoneabbr varchar(5),
	@timezoneoffset int,
	@timezoneisdst int,
	@timezonetext varchar(255),
	@Title varchar(50),
	@Username varchar(50),
	@Password varchar(255)
AS
BEGIN
	
	SET NOCOUNT ON;

	declare @customerid int
	declare @CustomerEmail varchar(50)
	--dbo.fnAppEmailCheck(@Email)=1
	if exists (select top 1 id from Customers where VerificationGUID=@GUID ) 
	begin
		
		select @CustomerID = id,@CustomerEmail=email
		from customers
		where VerificationGUID=@GUID

		if not exists(select top 1 id from customersettings where customerid=@CustomerID)
		begin 
			insert into customersettings(customerid)
			values(@CustomerID)
		end

		update customers
		set name=@CompanyName,
		CompanyAddress=@Address,
		Industry=@Industry,
		PhoneNumber=@PhoneNumber,
		Prefix = @Prefix
		where id=@customerid

		update CustomerSettings
		set timezonevalue=@timezonevalue,
		timezoneabbr=@timezoneabbr,
		timezoneoffset=@timezoneoffset,
		timezoneisdst=@timezoneisdst,
		timezonetext=@timezonetext
		where customerid=@CustomerID

		if not exists(select top 1 id from users where email=@CustomerEmail and username=@Username and customerid=@customerid)
		begin
			insert into users(customerid,Username,UserPassword,email,RoleID,PermissionGroupID
							,IsActive,LanguageID,firstname,lastname,mobilenumber,countrycode,
							timezonevalue,timezoneabbr,timezoneoffset,timezoneisdst,timezonetext)
			values(@CustomerID,@UserName,@Password,@CustomerEmail,2,1,1,1,@FirstName,@LastName,@MobileNumber,@CountryCode,
					@timezonevalue,@timezoneabbr,@timezoneoffset,@timezoneisdst,@timezonetext)
		end

		/*insert CustomerSettings(CustomerID,ConcurrentUsersLimit,MaxStorageAllowed,LanguageID)
		values(@customerid,@MaxUsers,@MaxStorage,1)*/

		insert into PermissionGroupActions(PermissionActionID, PermissionGroupID, CustomerID, IsAllowed)
		select PermissionActionID,PermissionGroupID,@customerid,1
		from PermissionGroupActionMatrix
		where not exists(select permissionactionid
						 from PermissionGroupActions 
						 where PermissionGroupActions.PermissionActionID=PermissionGroupActionMatrix.PermissionActionID
						 and PermissionGroupActions.PermissionGroupID=PermissionGroupActionMatrix.PermissionGroupID
						 and customerid = @customerid)

		
		select @CustomerEmail

	end	
	else
	begin
		select ''
	end

END

GO
/****** Object:  StoredProcedure [dbo].[spCreateNewSite]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spCreateNewSite] 
	@UserID int,
	@Name nvarchar(50),
	@ParentID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @siteid int
	declare @PermissionGroupID int
	declare @CustomerID int

	select @CustomerID = customerid
	from users
	where id=@UserID

    insert into sites(parentid,name,CustomerID)
	values(@ParentID,@Name,@CustomerID);
    
	set @siteid = @@Identity

	/*select @PermissionGroupID=PermissionGroupID
	from PermissionUserSiteDeviceGroup
	where UserID=@UserID
	and SiteID = @ParentID

	insert into PermissionUserSiteDeviceGroup(userid,siteid,PermissionGroupID)
	values(@UserID,@SiteID,@PermissionGroupID)*/

	
	if @siteid>0
	begin
		select @siteid
	end
	else 
	begin
		select 0
	end
END

GO
/****** Object:  StoredProcedure [dbo].[spCustomerGuidIsValid]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[spCustomerGuidIsValid]
	@GUID varchar(200)
AS
BEGIN
	SET NOCOUNT ON;
	declare @guidConverted uniqueidentifier

	SELECT  @guidConverted= CAST(
        SUBSTRING(@GUID, 1, 8) + '-' + SUBSTRING(@GUID, 9, 4) + '-' + SUBSTRING(@GUID, 13, 4) + '-' +
        SUBSTRING(@GUID, 17, 4) + '-' + SUBSTRING(@GUID, 21, 12)
        AS UNIQUEIDENTIFIER)

	if exists(select top 1 customers.ID from customers where customers.VerificationGUID=@guidConverted) 
	begin
		select 1
	end
	else
	begin
		select 0
	end

END

GO
/****** Object:  StoredProcedure [dbo].[spCustomerSignUp]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spCustomerSignUp]
	@CustomerEmail varchar(50)
AS
BEGIN
	SET NOCOUNT ON;

	declare @CustomerID int

    insert into customers(name,email,VerificationGUID)
	values('',@CustomerEmail,NEWID())

	set @CustomerID= @@IDENTITY

	select VerificationGUID,@CustomerID customerid
	from Customers
	where id=@CustomerID
END

GO
/****** Object:  StoredProcedure [dbo].[spdeAssociateDevices]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spdeAssociateDevices] 
	@SerialNumber int
AS
BEGIN
	SET NOCOUNT ON;

    update Devices
	set CustomerID=0
	where serialnumber=@SerialNumber

END

GO
/****** Object:  StoredProcedure [dbo].[spDeleteAnalyticsSensors]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spDeleteAnalyticsSensors]
	@UserID int,
	@DeviceSensorID int = 0
AS
BEGIN
	SET NOCOUNT ON;

	if @DeviceSensorID>0
	BEGIN
	
		DELETE AnalyticsSensors 
		WHERE UserID=@UserID 
		AND DeviceSensorID=@DeviceSensorID

	END
	ELSE
	BEGIN
	
		Update AnalyticsSensors 
		set IsActive=0
		WHERE UserID=@UserID 

	END

END

GO
/****** Object:  StoredProcedure [dbo].[spDeleteContact]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spDeleteContact]
	@ContactID int
AS
BEGIN
	
	SET NOCOUNT ON;

    update contacts
	set deleted=1
	where contacts.id = @ContactID

	

END

GO
/****** Object:  StoredProcedure [dbo].[spDeleteDefinedSensor]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spDeleteDefinedSensor]
	@DefinedSensorID int
	
AS
BEGIN
	SET NOCOUNT ON;
			
		delete DefinedSensors
		where id = @DefinedSensorID

END

GO
/****** Object:  StoredProcedure [dbo].[spDeleteGroup]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spDeleteGroup]
	@GroupID int
AS
BEGIN
	SET NOCOUNT ON;

	delete from Groups
	where id=@GroupID


	delete from ContactDistributionGroups
	where GroupID=@GroupID

END

GO
/****** Object:  StoredProcedure [dbo].[spDeleteReport]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spDeleteReport]
	@ReportID int
AS
BEGIN
	SET NOCOUNT ON;

	delete from ReportDeviceHistory
	where id=@ReportID
	
   
END

GO
/****** Object:  StoredProcedure [dbo].[spDeleteSetupFile]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spDeleteSetupFile]
	@UserID int,
	@SetupID varchar(255)
AS
BEGIN
	SET NOCOUNT ON;

	declare @CustomerID int

	select @CustomerID=customerid
	from users
	where id=@UserID
   
    delete from DeviceSetupFiles
	where CustomerID=@CustomerID
	and SetupID = @SetupID

	update DeviceAutoSetup
	set SetupID=''
	,IsAutoSetup=0
	where CustomerID=@CustomerID
	and SetupID=@SetupID

END

GO
/****** Object:  StoredProcedure [dbo].[spDeleteSite]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spDeleteSite]
	@SiteID int
AS
BEGIN
	SET NOCOUNT ON;

    if not exists (select top 1 id from sites where parentid=@SiteID) and
	   not exists (select top 1 id from devices where siteid=@SiteID)

	begin
		delete from PermissionUserSiteDeviceGroup
		where siteid=@SiteID

		/*delete from PermissionUserSites
		where siteid=@SiteID*/

		delete from sites
		where id=@SiteID

		select 1
	end
	else
	begin
		select 0
	end
END

GO
/****** Object:  StoredProcedure [dbo].[spDeleteUser]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spDeleteUser] 
	@UserID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    update users
	set users.deleted=1
	where users.id = @UserID;

	select @@ROWCOUNT;
END

GO
/****** Object:  StoredProcedure [dbo].[spDisableAccount]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spDisableAccount]
	@SerialNumber int
AS
BEGIN
	SET NOCOUNT ON;

    declare @CustomerID int
	declare @wasDisabled tinyint
	declare @EmailWasSentToday int
	declare @EmailSentDate datetime
	declare @dayDiff int

	select @CustomerID=customerid
	from devices
	where serialNumber=@SerialNumber

	--checks first of not disabled already
	if exists(select top 1 id from customers where id=@CustomerID and isnull(IsActive,1)=1)
	begin

		update customers
		set IsActive=0
		,DisableDate = getdate()
		where id=@CustomerID

		set @wasDisabled=0
	end
	else
	begin

		set @wasDisabled=1
	end
	
	--checks first what is the current email sent date
	select @EmailSentDate=isnull(EmailSentDate,getdate()-1)
	from customers
	where id=@CustomerID
	--if it is lower than today then update the flag of EmailWasSentToday to 0
	set @dayDiff =  DATEDIFF ( day , @EmailSentDate , getdate() )
	
	set @EmailWasSentToday=1

	if @dayDiff>0
	begin
		set @EmailWasSentToday=0
	end
	

	select @wasDisabled wasDisabled,DisableDate ,isnull( MongoDataBackUp,0)MongoDataBackUp,@EmailWasSentToday EmailWasSentToday,email,name
	from customers
	where id=@CustomerID
END

GO
/****** Object:  StoredProcedure [dbo].[spGetActivity]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetActivity]
	@UserID int
AS
BEGIN
	SET NOCOUNT ON;

	declare @CustomerID int

	select @CustomerID = customerid
	from users
	where users.id=@UserID

	select 
	(select count(id) from users where customerid=@customerid and isnull(isactive,0)=1 and isnull(deleted,0)=0)activeUsers,
	(select ConcurrentUsersLimit from customersettings where customerid=@customerid) availableUsers
   
END

GO
/****** Object:  StoredProcedure [dbo].[spGetAlarmNotifications]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetAlarmNotifications]
	@UserID int
AS
BEGIN
	SET NOCOUNT ON;

	declare @CustomerID int

	select @CustomerID = customerid
	from users
	where id=@UserID

    select DISTINCT AlarmNotifications.ID, devices.name,AlarmNotifications.value,AlarmNotifications.starttime, AlarmNotifications.cleartime
	,AlarmNotifications.alarmreason
	,(select AlarmTypes.name from AlarmTypes where AlarmTypes.id=AlarmNotifications.alarmtypeid)alarmtype
	,AlarmNotifications.AlarmTypeID,AlarmNotifications.DeviceSensorID,devices.SerialNumber
	,case 
	when (select SensorName from DeviceSensors where AlarmNotifications.DeviceSensorID = DeviceSensors.id)='' then (select name from SensorTypes where sensortypes.id = (select SensorTypeID from DeviceSensors where AlarmNotifications.DeviceSensorID = DeviceSensors.id))
	else  (select SensorName from DeviceSensors where AlarmNotifications.DeviceSensorID = DeviceSensors.id)
	end sensorName
	from AlarmNotifications
	inner join devices
	on devices.id=AlarmNotifications.deviceid
	--inner join PermissionUserSiteDeviceGroup
	--on PermissionUserSiteDeviceGroup.siteid = devices.siteid
	--where PermissionUserSiteDeviceGroup.userid=@UserID
	where devices.CustomerID=@CustomerID
	and AlarmNotifications.Hide=0
END

GO
/****** Object:  StoredProcedure [dbo].[spGetAllContacts]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetAllContacts]
	@UserID int
AS
BEGIN
	SET NOCOUNT ON;

    declare @CustomerID int

	select @CustomerID=users.CustomerID
	from users
	where id=@UserID

	select 0 isSystem, contacts.id contactId, contacts.name, '' username
	,contacts.title,contacts.PhoneNumber,contacts.email,contacts.countryCode
	,(select name  from ContactsWorkHoursMode where ContactsWorkHoursMode.id= contacts.WorkingHoursID)workHoursMode
	,contacts.WorkdayStart,contacts.WorkdayEnd
	,contacts.SMSResendInterval smsResends
	,isnull(WorkingSun,0)WorkingSun,isnull(WorkingMon,0)WorkingMon,isnull(WorkingTue,0)WorkingTue
	,isnull(WorkingWed,0)WorkingWed,isnull(WorkingThu,0)WorkingThu,isnull(WorkingFri,0)WorkingFri
	,isnull(WorkingSat,0)WorkingSat
	from Contacts
	where contacts.customerid=@CustomerID
	and isnull(contacts.deleted,0)=0
	union
	select 1,users.id,concat(users.FirstName,' ',users.lastname),username
	,'',users.mobilenumber,Users.Email,Users.countrycode
	,'',0,23,1
	,1,1,1,1,1,1,1
	from users
	where users.CustomerID=@CustomerID
	and isnull(users.deleted,0)=0

END

GO
/****** Object:  StoredProcedure [dbo].[spGetAllDeviceSensorsDownloaded]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetAllDeviceSensorsDownloaded]
	@SerialNumber int
AS
BEGIN
	SET NOCOUNT ON;

	/*Declare @CustomerID int
	
	select @CustomerID=CustomerID
	from Devices
	where Devices.serialNumber = @SerialNumber

    --get device sensors related to customer and downloaded data
	select DeviceSensors.id
	from SensorDownloadHistory
	inner join Devices
	on Devices.SerialNumber = SensorDownloadHistory.SerialNumber
	inner join DeviceSensors
	on DeviceSensors.DeviceID = Devices.ID
	where Devices.CustomerID=@CustomerID*/

	select SensorTypeID
	from DeviceSensors
	inner join Devices
	on Devices.Id = DeviceSensors.DeviceID
	where SerialNumber = @SerialNumber

END

GO
/****** Object:  StoredProcedure [dbo].[spGetAllowedUsers]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetAllowedUsers]
	@SerialNumber int
AS
BEGIN
	SET NOCOUNT ON;

	--get all allowed users for this device

	select distinct userid
	from permissionusersitedevicegroup
	inner join devices
	on devices.siteid=permissionusersitedevicegroup.siteid
	where devices.serialnumber=@SerialNumber

    
END

GO
/****** Object:  StoredProcedure [dbo].[spGetAllSerialNumbers]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetAllSerialNumbers]
	@SerialNumber int
AS
BEGIN
	SET NOCOUNT ON;

    declare @CustomerID int

	select @CustomerID=customerid
	from devices
	where serialnumber=@SerialNumber

	select serialNumber
	from devices
	where customerid=@CustomerID


END

GO
/****** Object:  StoredProcedure [dbo].[spGetAnalyticsSensors]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetAnalyticsSensors]
	@UserID int
AS
BEGIN
	SET NOCOUNT ON;

    SELECT DeviceSensorID, IsActive, case len(Devices.Name) when 0 then '[No name defined]' else Devices.Name end name, SensorTypes.Name, SensorUnits.Name, Devices.SerialNumber, SensorTypeID 
    FROM AnalyticsSensors 
	INNER JOIN DeviceSensors ON DeviceSensors.ID=AnalyticsSensors.DeviceSensorID 
	INNER JOIN Devices ON Devices.ID=DeviceID 
	INNER JOIN SensorTypes ON SensorTypes.ID=DeviceSensors.SensorTypeID 
	INNER JOIN SensorUnits ON SensorUnits.ID=DeviceSensors.SensorUnitID 
	WHERE UserID=@UserID

END

GO
/****** Object:  StoredProcedure [dbo].[spGetApproversReviewersList]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetApproversReviewersList]
	@UserID int
AS
BEGIN
	SET NOCOUNT ON;

	declare @CustomerID int

	select @CustomerID = customerid
	from users
	where id=@UserID

    select id,username name
	from Users
	where PermissionGroupID<=3
	and CustomerID=@CustomerID
    

END

GO
/****** Object:  StoredProcedure [dbo].[spGetAuditTrailData]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetAuditTrailData]
	@UserID int,
	@start int,
	@end int
AS
BEGIN
	SET NOCOUNT ON;

	declare @CustomerID int
	declare @starttime datetime
	declare @endtime datetime
	declare @UnixDate datetime

	set @UnixDate = Convert(datetime,'1970-01-01',104)
	set @starttime = DateAdd(second,@start,@UnixDate)
	set @endtime = DateAdd(second,@end,@UnixDate)

	select @CustomerID = customerid
	from users
	where id=@UserID

	select AuditTrail.id, AuditTrail.userid, username
	,PermissionGroupActionMatrix.PrivilageName actionName
	,(select PermissionActionTypes.name from PermissionActionTypes where PermissionActionTypes.id=PermissionActions.ActionTypeID) actionType
	,AuditTrail.reason additionalInfo
	,isnull((select partnumber from devicetypes where devicetypes.id = Devices.devicetypeid),'')product
	,devices.serialNumber
	,AuditTrail.ActivityTimeStamp
	from AuditTrail
	inner join PermissionActions
	on PermissionActions.ID = AuditTrail.PermissionActionID
	left join Devices
	on Devices.id = AuditTrail.DeviceID
	inner join PermissionGroupActionMatrix
	on PermissionGroupActionMatrix.PermissionActionID = PermissionActions.ID
	inner join Users
	on Users.id= AuditTrail.UserID
	where users.customerid=@CustomerID
	and AuditTrail.ActivityTimeStamp>=@starttime
	and AuditTrail.ActivityTimeStamp<=@endtime


   
END

GO
/****** Object:  StoredProcedure [dbo].[spGetBaseSensor]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[spGetBaseSensor]
	@FamilyTypeID int
AS
BEGIN
	SET NOCOUNT ON;

	select BaseSensors.id,BaseSensors.name
	from BaseSensors
	inner join BaseSenorsFamily
	on BaseSenorsFamily.BaseSensorID = BaseSensors.ID
	where BaseSenorsFamily.FamilyTypeID = @FamilyTypeID
    
END

GO
/****** Object:  StoredProcedure [dbo].[spGetCalibrationCertificateDefault]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetCalibrationCertificateDefault]
	@UserID int
AS
BEGIN
	SET NOCOUNT ON;

	declare @CustomerID int

	select @CustomerID=customerid
	from users
	where id=@UserID
   
    select isnull(calibtemperature,0) temperature,isnull(calibhumidity,0) humidity,isnull(calibfourtecwaxseal,0) includeinreport
	,isnull(calibmanufacture,'') manufacture,isnull(calibmodel,'') model,isnull(calibnist,0) nist,isnull(calibproductname,'') productname,isnull(calibserialnumber,0) serialnumber
	from customersettings
	where customerid= @CustomerID

END

GO
/****** Object:  StoredProcedure [dbo].[spGetCalibrationExpiryReminder]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetCalibrationExpiryReminder]
	@UserID int
AS
BEGIN
	SET NOCOUNT ON;

	declare @CustomerID int

	select @CustomerID = customerid
	from users
	where users.id=@UserID

	select isnull(IsCalibrationReminderActive,0) reminderIsOn,
	case isnull(CalibrationReminderInMonths,0)
		when 3 then '3_MONTH'
		when 6 then '6_MONTH'
		when 9 then '9_MONTH'
		when 12 then '12_MONTH'
		end reminderMode
	from users
	where id=@UserID
   
END

GO
/****** Object:  StoredProcedure [dbo].[spGetCategories]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetCategories]
	
AS
BEGIN
	
	select id, name 
	from categories;

END

GO
/****** Object:  StoredProcedure [dbo].[spGetContact]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetContact]
	@ContactId int
AS
BEGIN
	SET NOCOUNT ON;

    SELECT [ID]
      ,[Name]
      ,isnull([Title],'')[Title]
      ,isnull([Phone],0)[Phone]
      ,isnull([Email],'')[Email]
      ,isnull([WeekDayStart],0)[WeekDayStart]
      ,isnull([WorkdayStart],'')[WorkdayStart]
      ,isnull([WorkdayEnd],'')[WorkdayEnd]
      ,isnull([OutOfOfficeStart],0)[OutOfOfficeStart]
      ,isnull([OutOfOfficeEnd],0)[OutOfOfficeEnd]
      ,isnull([SMSResendInterval],0)[SMSResendInterval]
      
  FROM [dbo].[Contacts]
  where isnull(deleted,0)=0
  and id=@ContactID

END

GO
/****** Object:  StoredProcedure [dbo].[spGetContacts]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetContacts]
	@ContactID int
AS
BEGIN
	SET NOCOUNT ON;

	declare @SQL nvarchar(max)
	
set @SQL = 'SELECT [ID]
      ,[Name]
      ,isnull([Title],'''')[Title]
      ,isnull([Phone],0)[Phone]
      ,isnull([Email],'''')[Email]
      ,isnull([WeekDayStart],0)[WeekDayStart]
      ,isnull([WorkdayStart],''00:00'')[WorkdayStart]
      ,isnull([WorkdayEnd],''00:00'')[WorkdayEnd]
      ,isnull([OutOfOfficeStart],0)[OutOfOfficeStart]
      ,isnull([OutOfOfficeEnd],0)[OutOfOfficeEnd]
      ,isnull([SMSResendInterval],0)[SMSResendInterval]
      
  FROM [dbo].[Contacts]
  where isnull(deleted,0)=0 ' 

  if isnull(@ContactID,0)>0

	set @SQL += ' and contacts.id in(' + cast(@ContactID as nvarchar) + ') '

  exec (@SQL)

END

GO
/****** Object:  StoredProcedure [dbo].[spGetCustomer]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetCustomer]
	@Email nvarchar(255)
AS
BEGIN
	SET NOCOUNT ON;

    select name,email,creationdate,isactive,smscounter
	from customers
	where email=@email
END

GO
/****** Object:  StoredProcedure [dbo].[spGetCustomerBalance]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetCustomerBalance]
	@UserID int
AS
BEGIN
	SET NOCOUNT ON;

	declare @CustomerID int

	select @CustomerID = customerid
	from users
	where id=@UserID

	select isnull(MaxStorageAllowed,0) bundleStorage,0 usageStorage
	, isnull(MaxUsersAllowed,0) bundleUsers,isnull((select count(id) from users where customerid=@CustomerID),0) usageUsers
	,isnull(subscriptiontime,0) bundleExpiry, datediff(day,getdate(), DATEADD(day,subscriptiontime,dateadd(second,subscriptionstart,{d '1970-01-01'}))) usageExpiry
	,CustomerID
	from CustomerSettings
	where CustomerID=@CustomerID

END

GO
/****** Object:  StoredProcedure [dbo].[spGetCustomerByDeviceID]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetCustomerByDeviceID]
	@DeviceID int
AS
BEGIN
	SET NOCOUNT ON;

    select customerid
	from devices
	where id=@DeviceID

END

GO
/****** Object:  StoredProcedure [dbo].[spGetCustomerByDeviceSensorID]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetCustomerByDeviceSensorID]
	@DeviceSensorID int
AS
BEGIN
	SET NOCOUNT ON;

    select customerid
	from devices
	inner join DeviceSensors
	on devices.ID = DeviceSensors.DeviceID
	where DeviceSensors.id=@DeviceSensorID

END

GO
/****** Object:  StoredProcedure [dbo].[spGetCustomerBySerialNumber]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[spGetCustomerBySerialNumber]
	@SerialNumber int
AS
BEGIN
	SET NOCOUNT ON;

    select customerid
	from devices
	where SerialNumber=@SerialNumber

END

GO
/****** Object:  StoredProcedure [dbo].[spGetCustomerIdByUser]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetCustomerIdByUser]
	@UserID int
AS
BEGIN
	SET NOCOUNT ON;

    select customerid
	from users
	where id=@UserID

END

GO
/****** Object:  StoredProcedure [dbo].[spGetCustomerStorage]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetCustomerStorage]
	@SerialNumber int
AS
BEGIN
	SET NOCOUNT ON;

    select isnull(MaxStorageAllowed,1)MaxStorageAllowed, isnull(DataStorageExpiry,0)DataStorageExpiry
	,CustomerSettings.CustomerID
	from Devices 
	left join CustomerSettings
	on Devices.CustomerID=CustomerSettings.CustomerID
	where Devices.SerialNumber = @SerialNumber

END

GO
/****** Object:  StoredProcedure [dbo].[spGetDefinedSensors]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetDefinedSensors]
	@UserID int
AS
BEGIN
	SET NOCOUNT ON;

	declare @CustomerID int

	select @CustomerID = customerid
	from users
	where users.id=@UserID

	select DefinedSensors.id  sensorId
	, BaseSensors.name basesensor,BaseSensors.id baseSensorId,DecimalPlaces digits
	,(select name from DeviceFamilyTypes where DeviceFamilyTypes.id = DefinedSensors.FamilyTypeID) familyName
	,DefinedSensors.FamilyTypeID familyId,log1 output1 , log2 output2,DefinedSensors.Name sensorName
	,(select name from SensorUnits where SensorUnits.id = DefinedSensors.SensorUnitID)unitName
	, DefinedSensors.SensorUnitID unitId
	,ref1 real1, ref2 real2

	from DefinedSensors
	inner join BaseSensors
	on BaseSensors.ID = DefinedSensors.BaseSensorID
	where DefinedSensors.CustomerID = @CustomerID
   
END

GO
/****** Object:  StoredProcedure [dbo].[spGetDefinedSensorsByDevice]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetDefinedSensorsByDevice]
	@SerialNumber int
AS
BEGIN
	SET NOCOUNT ON;

    select definedsensors.ID, concat(basesensors.Name,' ',SensorUnits.Name,' ', definedsensors.name) DefinedSensorName
	from devices
	inner join devicetypes
	on devices.devicetypeid=devicetypes.id
	left join definedsensors
	on definedsensors.FamilyTypeID = devicetypes.FamilyTypeID
	inner join SensorUnits
	on SensorUnits.ID = definedsensors.SensorUnitID
	inner join basesensors
	on basesensors.id = definedsensors.basesensorid
	where serialnumber=@SerialNumber

END

GO
/****** Object:  StoredProcedure [dbo].[spGetDeviceAlarmNotifications]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetDeviceAlarmNotifications] 
	@ContactID int
AS
BEGIN
	SET NOCOUNT ON;

    
	select isnull(ReceiveEmail,0)ReceiveEmail,isnull(ReceiveSMS,0)ReceiveSMS,DeviceAlarmNotifications.DeviceSensorID
	,(select DeviceSensors.DeviceID from DeviceSensors where DeviceSensors.id = DeviceAlarmNotifications.DeviceSensorID) deviceId
	from DeviceAlarmNotifications
	where DeviceAlarmNotifications.ContactID = @ContactID


END

GO
/****** Object:  StoredProcedure [dbo].[spGetDeviceAutoSetup]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetDeviceAutoSetup]

	@UserID int	

AS
BEGIN
	SET NOCOUNT ON;

	Declare @CustomerID int

	select @CustomerID = customerid
	from users
	where id=@UserID

	if not exists (select top 1 id from DeviceAutoSetup where customerid=@CustomerID)
	begin
		insert into DeviceAutoSetup(customerid,devicetypeid,setupid,usagecounter,isautosetup,firmwareversion)
		select @CustomerID,DeviceTypes.id,'',0,0,fwversion
		from DeviceTypes
	end

	select DeviceAutoSetup.id, (select name from DeviceTypes where id=devicetypeid) deviceType
	,isnull(setupid,'')setupid, usagecounter,isautosetup,firmwareversion,devicetypeid
	from DeviceAutoSetup
	where customerid = @CustomerID
	

END

GO
/****** Object:  StoredProcedure [dbo].[spGetDeviceFamilyTypes]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetDeviceFamilyTypes]
	
AS
BEGIN
	SET NOCOUNT ON;

	select id,name
	from DeviceFamilyTypes
	where HasExternalSensor=1
    
END

GO
/****** Object:  StoredProcedure [dbo].[spGetDeviceProperties]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetDeviceProperties]
	@DeviceID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    declare @columnName nvarchar(255)
	declare @columnValue int
	declare @getColumn cursor
	declare @columns nvarchar(max)
	declare @columnDef nvarchar(max)
	declare @values nvarchar(max)
	declare @SQLString nvarchar(max)
	
	set @getColumn = cursor for
	select  DeviceProperties.deviceproperty,propertyvalue 
	from DevicePropertyValue
	inner join DeviceProperties
	on DeviceProperties.id = DevicePropertyValue.devicepropertyid
	where DevicePropertyValue.deviceid=@DeviceID
	
	set @columns=''
	set @values = ''
	set @columnDef=''

	open @getColumn
	fetch next
	from @getColumn into @columnName,@columnValue
	while @@fetch_status = 0
	begin
		
		set @columnDef += @columnName + ' int ,'

		set @columns += @columnName + ' ,'

		set @values +=  cast(@columnValue as nvarchar) + ','
		
		fetch next
		from @getColumn into @columnName,@columnValue
	end

	close @getColumn
	deallocate @getColumn

	set @columnDef = substring(@columnDef,1,len(@columnDef)-1)
	set @columns = substring(@columns,1,len(@columns)-1)
	set @values = substring(@values,1,len(@values)-1)

	set @SQLString = 'create table #temp1 (' + @columnDef + ') insert into #temp1(' + @columns + ') values (' + @values + ') 
	                  select * from #temp1 '
	exec (@SQLString)

END

GO
/****** Object:  StoredProcedure [dbo].[spGetDevices]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetDevices]
	@SerialNumber int
AS
BEGIN
	SET NOCOUNT ON;

    

	
    
		select devices.id,devices.serialnumber,devices.name,devices.siteid,isrunning
		,isnull((select count(hide) from AlarmNotifications where AlarmNotifications.deviceid = devices.id and hide=0),1)CurrentAlarmState
		,isnull(InCalibrationMode,0)InCalibrationMode
		, devicetypes.name devicetype
		, devicetypes.imagename devicetypename
		, devicetypes.partnumber partnumber
		,(select devicestatuses.name from devicestatuses where devicestatuses.id = devices.statusid)devicestatus
		, case when devicetypes.fwversion>devices.firmwareversion 
		  then 1
		  else 
			case when devicetypes.buildnumber>devices.buildnumber
			then 1
			else 0
			end
		  end FirmwareUpdateNeeded
		,islocked,firmwareversion,devices.buildnumber,devicetypes.fwversion,DeviceTypes.ID devicetypeid,isnull(devices.InFwUpdate,0)InFwUpdate
		,batterylevel
		,(select timezones.value from timezones where timezones.id = devices.timezoneid)timezone
		,isnull(longitude,0)longitude,isnull(latitude,0)latitude,isnull(iconxcoordinate,0)iconxcoordinate,isnull(iconycoordinate,0)iconycoordinate
		--DeviceMicroX
		,DeviceMicroX.CyclicMode, DeviceMicroX.PushToRun,DeviceMicroX.TimerRunEnabled, DeviceMicroX.TimerStart
		,DeviceMicroX.BoomerangEnabled, DeviceMicroX.BoomerangAuthor, DeviceMicroX.BoomerangCelsiusMode, DeviceMicroX.BoomerangComment,DeviceMicroX.BoomerangContacts, DeviceMicroX.BoomerangDisplayAlarmLevels,DeviceMicroX.BoomerangUTC
		--DeviceLogger
		,DeviceLogger.Interval samplerateinsec,DeviceLogger.CelsiusMode --case isnull(CelsiusMode,0) when 1 then ''C'' when 2 then ''F'' end CelsiusMode 
		--DeviceMicroLog
		,DeviceMicroLog.AveragePoints sampleavgpoints,DeviceMicroLog.DeepSleepMode,DeviceMicroLog.EnableLEDOnAlarm, DeviceMicroLog.MemorySize, DeviceMicroLog.ShowMinMax, DeviceMicroLog.ShowPast24HMinMax, DeviceMicroLog.StopOnDisconnect, DeviceMicroLog.StopOnKeyPress, DeviceMicroLog.UnitRequiresSetup 
		,DeviceMicroLog.LCDConfiguration micrologLCDConfig
		--DevicePicoLite
		, DevicePicoLite.LEDConfig, DevicePicoLite.RunDelay, DevicePicoLite.RunTime, DevicePicoLite.SetupTime
		,isnull(DevicePicoLite.StopOnKeyPress,0) picoStopOnKeyPress
		--DeviceMicroLite
		,isnull(DeviceMicroLite.ShowMinMax,0) microliteShowMinMax,isnull(DeviceMicroLite.ShowPast24H,0) microliteShowPast24H,DeviceMicroLite.MemorySize microliteMemorySize,DeviceMicroLite.EnableLEDOnAlarm microliteEnableLEDOnAlarm,DeviceMicroLite.LCDConfig microliteLCDConfig
		,DeviceMicroLite.StopOnDisconnect microliteStopOnDisconnect,isnull(DeviceMicroLite.StopOnKeyPress,0) microliteStopOnKeyPress,DeviceMicroLite.UnitRequiresSetup microliteUnitRequiresSetup, DeviceMicroLite.AveragePoints microliteAveragePoints
		,isnull(devices.alarmdelay,0)alarmdelay
		,DeviceMicroX.LoggerUTC

		---------------------------------------------------------------------------------------
		,devicesensors.id deviceSensorID,devicesensors.SensorIndex
		,case(devicesensors.sensortypeid)
		  when 9
			then 
				DefinedSensors.Name
		  else
				case 
					when len(isnull(devicesensors.sensorname,'')) =0 then SensorTypes.name 
					else devicesensors.sensorname 
				end 
		  end sensorname 
		,dbo.fnIsExternalSensor(devicesensors.sensortypeid) IsExternalSensor
		,devicesensors.sensortypeid
		, case(devicesensors.sensortypeid)
		  when 9
			then
				(select SensorUnits.name from SensorUnits where SensorUnits.id = DefinedSensors.SensorUnitID)
			else
				(select SensorUnits.name from SensorUnits where SensorUnits.id = devicesensors.SensorUnitID)
		  end measurementunit
		,(select SensorTypes.name from SensorTypes where SensorTypes.id = devicesensors.sensortypeid) sensortype
		,case IsAlarmEnabled when 0 then null else PreLowValue end PreLowValue
		,case IsAlarmEnabled when 0 then null else LowValue end LowValue
		,case IsAlarmEnabled when 0 then null else PreHighValue end PreHighValue
		,case IsAlarmEnabled when 0 then null else HighValue end HighValue
		,case IsAlarmEnabled when 0 then null else PreDelayValue end PreDelayValue
		,case IsAlarmEnabled when 0 then null else DelayValue end DelayValue
		,case IsAlarmEnabled when 0 then null else BuzzDuration end BuzzDuration
		,IsAlarmEnabled,isnull(devicesensors.MinValue,0)minvalue,isnull(devicesensors.MaxValue,0)maxvalue,CalibrationExpiry,isenabled
		,isnull(AlarmNotifications.Silent,0)Silent
		,DefinedSensors.FamilyTypeID
		,isnull(DefinedSensors.DecimalPlaces,0)DecimalPlaces
		,DefinedSensors.Gain, DefinedSensors.Offset
		,isnull(DefinedSensors.Log1,0)Log1, isnull(DefinedSensors.Log2,0)Log2
		,isnull(DefinedSensors.Ref1,0)Ref1, isnull(DefinedSensors.Ref2,0)Ref2
		---------------------------------------------------------------------------------------

		from devices 

		-------------------------------------------------------
		inner join devicesensors
		on devicesensors.deviceid =devices.id
		left join SensorAlarm
		on SensorAlarm.DeviceSensorID=DeviceSensors.ID
		left join AlarmNotifications
		on AlarmNotifications.DeviceSensorID = devicesensors.ID
		inner join SensorTypes
		on devicesensors.SensorTypeID = SensorTypes.ID
		left join DefinedSensors
		on DefinedSensors.ID = devicesensors.DefinedSensorID
		-------------------------------------------------------

		inner join devicetypes
		on devicetypes.id= devices.devicetypeid
		left join DeviceMicroX
		on DeviceMicroX.DeviceID = Devices.ID
		left join DeviceLogger
		on DeviceLogger.DeviceID = Devices.ID
		left join DeviceMicroLog
		on DeviceMicroLog.DeviceID = Devices.ID
		left join DevicePicoLite
		on DevicePicoLite.DeviceID = Devices.ID
		left join DeviceMicroLite
		on DeviceMicroLite.DeviceID= Devices.ID  

	  where devices.serialnumber = @SerialNumber 
		
	
END

GO
/****** Object:  StoredProcedure [dbo].[spGetDevicesByCategory]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetDevicesByCategory]
	@CategoryID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	declare @customerid int;
    declare @SQLString nvarchar(max)

	set @SQLString = '
    
		select ' + dbo.fnGetBaseDeviceFields() + '
		,dbo.fnGetDevicePropertiesTable(devices.id) additionalproperties
		from devices
		where devices.categoryid = ' + cast(@CategoryID as nvarchar) 

	EXEC (@SQLString)
END

GO
/****** Object:  StoredProcedure [dbo].[spGetDevicesByCustomer]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetDevicesByCustomer]
	@UserID int
AS
BEGIN
	SET NOCOUNT ON;

    declare @CustomerID int

	select @CustomerID=  customerid
	from users
	where id=@UserID

	select devices.id,devices.serialnumber,devices.name,devices.siteid,isrunning,isnull(InCalibrationMode,0)InCalibrationMode
		, devicetypes.name devicetype
		,isnull((select count(hide) from AlarmNotifications where AlarmNotifications.deviceid = devices.id and hide=0),1)CurrentAlarmState
		, devicetypes.imagename devicetypename
		, devicetypes.partnumber partnumber
		,(select devicestatuses.name from devicestatuses where devicestatuses.id = devices.statusid)devicestatus
		, case when devicetypes.fwversion>devices.firmwareversion 
		  then 1
		  else 
			case when devicetypes.buildnumber>devices.buildnumber
			then 1
			else 0
			end
		  end FirmwareUpdateNeeded
		,islocked,firmwareversion,devices.buildnumber,devicetypes.fwversion,DeviceTypes.ID devicetypeid,isnull(devices.InFwUpdate,0)InFwUpdate
		,batterylevel
		,(select timezones.value from timezones where timezones.id = devices.timezoneid)timezone
		,isnull(longitude,0)longitude,isnull(latitude,0)latitude,isnull(iconxcoordinate,0)iconxcoordinate,isnull(iconycoordinate,0)iconycoordinate
		--DeviceMicroX
		,DeviceMicroX.CyclicMode, DeviceMicroX.PushToRun,DeviceMicroX.TimerRunEnabled, DeviceMicroX.TimerStart
		,DeviceMicroX.BoomerangEnabled, DeviceMicroX.BoomerangAuthor, DeviceMicroX.BoomerangCelsiusMode, DeviceMicroX.BoomerangComment,DeviceMicroX.BoomerangContacts, DeviceMicroX.BoomerangDisplayAlarmLevels,DeviceMicroX.BoomerangUTC
		--DeviceLogger
		,DeviceLogger.Interval samplerateinsec,DeviceLogger.CelsiusMode --case isnull(CelsiusMode,0) when 1 then ''C'' when 2 then ''F'' end CelsiusMode 
		--DeviceMicroLog
		,DeviceMicroLog.AveragePoints sampleavgpoints,DeviceMicroLog.DeepSleepMode,DeviceMicroLog.EnableLEDOnAlarm, DeviceMicroLog.MemorySize, DeviceMicroLog.ShowMinMax, DeviceMicroLog.ShowPast24HMinMax, DeviceMicroLog.StopOnDisconnect, DeviceMicroLog.StopOnKeyPress, DeviceMicroLog.UnitRequiresSetup 
		,DeviceMicroLog.LCDConfiguration micrologLCDConfig
		--DevicePicoLite
		, DevicePicoLite.LEDConfig, DevicePicoLite.RunDelay, DevicePicoLite.RunTime, DevicePicoLite.SetupTime
		,isnull(DevicePicoLite.StopOnKeyPress,0) picoStopOnKeyPress
		--DeviceMicroLite
		,isnull(DeviceMicroLite.ShowMinMax,0) microliteShowMinMax,isnull(DeviceMicroLite.ShowPast24H,0) microliteShowPast24H,DeviceMicroLite.MemorySize microliteMemorySize,DeviceMicroLite.EnableLEDOnAlarm microliteEnableLEDOnAlarm,DeviceMicroLite.LCDConfig microliteLCDConfig
		,DeviceMicroLite.StopOnDisconnect microliteStopOnDisconnect,isnull(DeviceMicroLite.StopOnKeyPress,0) microliteStopOnKeyPress,DeviceMicroLite.UnitRequiresSetup microliteUnitRequiresSetup, DeviceMicroLite.AveragePoints microliteAveragePoints
		,isnull(devices.alarmdelay,0)alarmdelay
		,DeviceMicroX.LoggerUTC
		from devices 
		inner join devicetypes
		on devicetypes.id= devices.devicetypeid
		inner join PermissionUserSites
		on devices.siteid = PermissionUserSites.siteid 
		left join DeviceMicroX
		on DeviceMicroX.DeviceID = Devices.ID
		left join DeviceLogger
		on DeviceLogger.DeviceID = Devices.ID
		left join DeviceMicroLog
		on DeviceMicroLog.DeviceID = Devices.ID
		left join DevicePicoLite
		on DevicePicoLite.DeviceID = Devices.ID
		left join DeviceMicroLite
		on DeviceMicroLite.DeviceID= Devices.ID
	where devices.customerid=@CustomerID
END

GO
/****** Object:  StoredProcedure [dbo].[spGetDevicesBySite]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetDevicesBySite]
	@SiteID int,
	@UserID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @CustomerID int

	select @CustomerID=customerid
	from users
	where users.id=@UserID

	select devices.id,devices.serialnumber,devices.name,devices.siteid,isrunning,isnull(InCalibrationMode,0)InCalibrationMode
		, devicetypes.name devicetype
		,isnull((select count(hide) from AlarmNotifications where AlarmNotifications.deviceid = devices.id and hide=0),1)CurrentAlarmState
		, devicetypes.imagename devicetypename
		, devicetypes.partnumber partnumber
		,(select devicestatuses.name from devicestatuses where devicestatuses.id = devices.statusid)devicestatus
		, case when devicetypes.fwversion>devices.firmwareversion 
		  then 1
		  else 
			case when devicetypes.buildnumber>devices.buildnumber
			then 1
			else 0
			end
		  end FirmwareUpdateNeeded
		,islocked,firmwareversion,devices.buildnumber,devicetypes.fwversion,DeviceTypes.ID devicetypeid,isnull(devices.InFwUpdate,0)InFwUpdate
		,batterylevel
		,(select timezones.value from timezones where timezones.id = devices.timezoneid)timezone
		,isnull(longitude,0)longitude,isnull(latitude,0)latitude,isnull(iconxcoordinate,0)iconxcoordinate,isnull(iconycoordinate,0)iconycoordinate
		--DeviceMicroX
		,DeviceMicroX.CyclicMode, DeviceMicroX.PushToRun,DeviceMicroX.TimerRunEnabled, DeviceMicroX.TimerStart
		,DeviceMicroX.BoomerangEnabled, DeviceMicroX.BoomerangAuthor, DeviceMicroX.BoomerangCelsiusMode, DeviceMicroX.BoomerangComment,DeviceMicroX.BoomerangContacts, DeviceMicroX.BoomerangDisplayAlarmLevels,DeviceMicroX.BoomerangUTC
		--DeviceLogger
		,DeviceLogger.Interval samplerateinsec,DeviceLogger.CelsiusMode --case isnull(CelsiusMode,0) when 1 then ''C'' when 2 then ''F'' end CelsiusMode 
		--DeviceMicroLog
		,DeviceMicroLog.AveragePoints sampleavgpoints,DeviceMicroLog.DeepSleepMode,DeviceMicroLog.EnableLEDOnAlarm, DeviceMicroLog.MemorySize, DeviceMicroLog.ShowMinMax, DeviceMicroLog.ShowPast24HMinMax, DeviceMicroLog.StopOnDisconnect, DeviceMicroLog.StopOnKeyPress, DeviceMicroLog.UnitRequiresSetup 
		,DeviceMicroLog.LCDConfiguration micrologLCDConfig
		--DevicePicoLite
		, DevicePicoLite.LEDConfig, DevicePicoLite.RunDelay, DevicePicoLite.RunTime, DevicePicoLite.SetupTime
		,isnull(DevicePicoLite.StopOnKeyPress,0) picoStopOnKeyPress
		--DeviceMicroLite
		,isnull(DeviceMicroLite.ShowMinMax,0) microliteShowMinMax,isnull(DeviceMicroLite.ShowPast24H,0) microliteShowPast24H,DeviceMicroLite.MemorySize microliteMemorySize,DeviceMicroLite.EnableLEDOnAlarm microliteEnableLEDOnAlarm,DeviceMicroLite.LCDConfig microliteLCDConfig
		,DeviceMicroLite.StopOnDisconnect microliteStopOnDisconnect,isnull(DeviceMicroLite.StopOnKeyPress,0) microliteStopOnKeyPress,DeviceMicroLite.UnitRequiresSetup microliteUnitRequiresSetup, DeviceMicroLite.AveragePoints microliteAveragePoints
		,isnull(devices.alarmdelay,0)alarmdelay
		,DeviceMicroX.LoggerUTC

		-- device sensors
		---------------------------------------------------------------------------------------
		,devicesensors.id deviceSensorID,devicesensors.SensorIndex
		,case(devicesensors.sensortypeid)
		  when 9
			then 
				DefinedSensors.Name
		  else
				case 
					when len(isnull(devicesensors.sensorname,'')) =0 then SensorTypes.name 
					else devicesensors.sensorname 
				end 
		  end sensorname 
		,dbo.fnIsExternalSensor(devicesensors.sensortypeid) IsExternalSensor
		,devicesensors.sensortypeid
		, case(devicesensors.sensortypeid)
		  when 9
			then
				(select SensorUnits.name from SensorUnits where SensorUnits.id = DefinedSensors.SensorUnitID)
			else
				(select SensorUnits.name from SensorUnits where SensorUnits.id = devicesensors.SensorUnitID)
		  end measurementunit
		,(select SensorTypes.name from SensorTypes where SensorTypes.id = devicesensors.sensortypeid) sensortype
		,case IsAlarmEnabled when 0 then null else PreLowValue end PreLowValue
		,case IsAlarmEnabled when 0 then null else LowValue end LowValue
		,case IsAlarmEnabled when 0 then null else PreHighValue end PreHighValue
		,case IsAlarmEnabled when 0 then null else HighValue end HighValue
		,case IsAlarmEnabled when 0 then null else PreDelayValue end PreDelayValue
		,case IsAlarmEnabled when 0 then null else DelayValue end DelayValue
		,case IsAlarmEnabled when 0 then null else BuzzDuration end BuzzDuration
		,IsAlarmEnabled,isnull(devicesensors.MinValue,0)minvalue,isnull(devicesensors.MaxValue,0)maxvalue,CalibrationExpiry,isenabled
		,isnull(AlarmNotifications.Silent,0)Silent
		,DefinedSensors.FamilyTypeID
		,isnull(DefinedSensors.DecimalPlaces,0)DecimalPlaces
		,DefinedSensors.Gain, DefinedSensors.Offset
		,isnull(DefinedSensors.Log1,0)Log1, isnull(DefinedSensors.Log2,0)Log2
		,isnull(DefinedSensors.Ref1,0)Ref1, isnull(DefinedSensors.Ref2,0)Ref2
		---------------------------------------------------------------------------------------

		from devices 
		
		-------------------------------------------------------
		inner join devicesensors
		on devicesensors.deviceid =devices.id
		left join SensorAlarm
		on SensorAlarm.DeviceSensorID=DeviceSensors.ID
		left join AlarmNotifications
		on AlarmNotifications.DeviceSensorID = devicesensors.ID
		inner join SensorTypes
		on devicesensors.SensorTypeID = SensorTypes.ID
		left join DefinedSensors
		on DefinedSensors.ID = devicesensors.DefinedSensorID
		-------------------------------------------------------

		inner join devicetypes
		on devicetypes.id= devices.devicetypeid
		inner join PermissionUserSites
		on devices.siteid = PermissionUserSites.siteid 
		left join DeviceMicroX
		on DeviceMicroX.DeviceID = Devices.ID
		left join DeviceLogger
		on DeviceLogger.DeviceID = Devices.ID
		left join DeviceMicroLog
		on DeviceMicroLog.DeviceID = Devices.ID
		left join DevicePicoLite
		on DevicePicoLite.DeviceID = Devices.ID
		left join DeviceMicroLite
		on DeviceMicroLite.DeviceID= Devices.ID
		/*left join PermissionUserSiteDeviceGroup
		on devices.siteid=PermissionUserSiteDeviceGroup.siteid
		and PermissionUserSiteDeviceGroup.userid=  @UserID*/
		where devices.CustomerID = @CustomerID
END

GO
/****** Object:  StoredProcedure [dbo].[spGetDevicesByUser]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetDevicesByUser]
	@UserID int
AS
BEGIN
	SET NOCOUNT ON;

		declare @UserName nvarchar(50)
		declare @CustomerID int

		select @UserName = username,@CustomerID=customerid
		from users
		where id=@UserID

    	select devices.id,devices.serialnumber,devices.name,devices.siteid,isrunning,isnull(InCalibrationMode,0)InCalibrationMode
		--,Hide as CurrentAlarmState
		,isnull((select count(hide) from AlarmNotifications where AlarmNotifications.deviceid = devices.id and hide=0),1)CurrentAlarmState
		, devicetypes.name devicetype
		, devicetypes.imagename devicetypename
		, devicetypes.partnumber partnumber
		,(select devicestatuses.name from devicestatuses where devicestatuses.id = devices.statusid)devicestatus
		, case when devicetypes.fwversion>devices.firmwareversion 
		  then 1
		  else 
			case when devicetypes.buildnumber>devices.buildnumber
			then 1
			else 0
			end
		  end FirmwareUpdateNeeded
		,islocked,firmwareversion,devices.buildnumber,devicetypes.fwversion,DeviceTypes.ID devicetypeid,isnull(devices.InFwUpdate,0)InFwUpdate
		,batterylevel
		,(select timezones.value from timezones where timezones.id = devices.timezoneid)timezone
		,isnull(longitude,0)longitude,isnull(latitude,0)latitude,isnull(iconxcoordinate,0)iconxcoordinate,isnull(iconycoordinate,0)iconycoordinate
		--DeviceMicroX
		,DeviceMicroX.CyclicMode, DeviceMicroX.PushToRun,DeviceMicroX.TimerRunEnabled, DeviceMicroX.TimerStart
		,DeviceMicroX.BoomerangEnabled, DeviceMicroX.BoomerangAuthor, DeviceMicroX.BoomerangCelsiusMode, DeviceMicroX.BoomerangComment,DeviceMicroX.BoomerangContacts, DeviceMicroX.BoomerangDisplayAlarmLevels,DeviceMicroX.BoomerangUTC
		--DeviceLogger
		,DeviceLogger.Interval samplerateinsec,DeviceLogger.CelsiusMode --case isnull(CelsiusMode,0) when 1 then ''C'' when 2 then ''F'' end CelsiusMode 
		--DeviceMicroLog
		,DeviceMicroLog.AveragePoints sampleavgpoints,DeviceMicroLog.DeepSleepMode,DeviceMicroLog.EnableLEDOnAlarm, DeviceMicroLog.MemorySize, DeviceMicroLog.ShowMinMax, DeviceMicroLog.ShowPast24HMinMax, DeviceMicroLog.StopOnDisconnect,isnull(DeviceMicroLog.StopOnKeyPress,0)StopOnKeyPress, DeviceMicroLog.UnitRequiresSetup 
		,DeviceMicroLog.LCDConfiguration micrologLCDConfig
		--DevicePicoLite
		, DevicePicoLite.LEDConfig, DevicePicoLite.RunDelay, DevicePicoLite.RunTime, DevicePicoLite.SetupTime
		,isnull(DevicePicoLite.StopOnKeyPress,0) picoStopOnKeyPress
		--DeviceMicroLite
		,isnull(DeviceMicroLite.ShowMinMax,0) microliteShowMinMax,isnull(DeviceMicroLite.ShowPast24H,0) microliteShowPast24H,DeviceMicroLite.MemorySize microliteMemorySize,DeviceMicroLite.EnableLEDOnAlarm microliteEnableLEDOnAlarm,DeviceMicroLite.LCDConfig microliteLCDConfig
		,DeviceMicroLite.StopOnDisconnect microliteStopOnDisconnect,isnull(DeviceMicroLite.StopOnKeyPress,0) microliteStopOnKeyPress,DeviceMicroLite.UnitRequiresSetup microliteUnitRequiresSetup, DeviceMicroLite.AveragePoints microliteAveragePoints
		,isnull(devices.alarmdelay,0)alarmdelay
		,DeviceMicroX.LoggerUTC

		-- device sensors
		---------------------------------------------------------------------------------------
		,devicesensors.id deviceSensorID,devicesensors.SensorIndex
		,case(devicesensors.sensortypeid)
		  when 9
			then 
				DefinedSensors.Name
		  else
				case 
					when len(isnull(devicesensors.sensorname,'')) =0 then SensorTypes.name 
					else devicesensors.sensorname 
				end 
		  end sensorname 
		,dbo.fnIsExternalSensor(devicesensors.sensortypeid) IsExternalSensor
		,devicesensors.sensortypeid
		, case(devicesensors.sensortypeid)
		  when 9
			then
				(select SensorUnits.name from SensorUnits where SensorUnits.id = DefinedSensors.SensorUnitID)
			else
				(select SensorUnits.name from SensorUnits where SensorUnits.id = devicesensors.SensorUnitID)
		  end measurementunit
		,(select SensorTypes.name from SensorTypes where SensorTypes.id = devicesensors.sensortypeid) sensortype
		,case IsAlarmEnabled when 0 then null else PreLowValue end PreLowValue
		,case IsAlarmEnabled when 0 then null else LowValue end LowValue
		,case IsAlarmEnabled when 0 then null else PreHighValue end PreHighValue
		,case IsAlarmEnabled when 0 then null else HighValue end HighValue
		,case IsAlarmEnabled when 0 then null else PreDelayValue end PreDelayValue
		,case IsAlarmEnabled when 0 then null else DelayValue end DelayValue
		,case IsAlarmEnabled when 0 then null else BuzzDuration end BuzzDuration
		,IsAlarmEnabled,isnull(devicesensors.MinValue,0)minvalue,isnull(devicesensors.MaxValue,0)maxvalue,CalibrationExpiry,isenabled
		,isnull(AlarmNotifications.Silent,0)Silent
		,DefinedSensors.FamilyTypeID
		,isnull(DefinedSensors.DecimalPlaces,0)DecimalPlaces
		,DefinedSensors.Gain, DefinedSensors.Offset
		,isnull(DefinedSensors.Log1,0)Log1, isnull(DefinedSensors.Log2,0)Log2
		,isnull(DefinedSensors.Ref1,0)Ref1, isnull(DefinedSensors.Ref2,0)Ref2
		---------------------------------------------------------------------------------------

		from devices 
		-------------------------------------------------------
		inner join devicesensors
		on devicesensors.deviceid =devices.id
		left join SensorAlarm
		on SensorAlarm.DeviceSensorID=DeviceSensors.ID
		left join AlarmNotifications
		on AlarmNotifications.DeviceSensorID = devicesensors.ID
		inner join SensorTypes
		on devicesensors.SensorTypeID = SensorTypes.ID
		left join DefinedSensors
		on DefinedSensors.ID = devicesensors.DefinedSensorID
		-------------------------------------------------------

		inner join devicetypes
		on devicetypes.id= devices.devicetypeid
		left join DeviceMicroX
		on DeviceMicroX.DeviceID = Devices.ID
		left join DeviceLogger
		on DeviceLogger.DeviceID = Devices.ID
		left join DeviceMicroLog
		on DeviceMicroLog.DeviceID = Devices.ID
		left join DevicePicoLite
		on DevicePicoLite.DeviceID = Devices.ID
		left join DeviceMicroLite
		on DeviceMicroLite.DeviceID= Devices.ID
		/*left join PermissionUserSiteDeviceGroup
		on devices.siteid=PermissionUserSiteDeviceGroup.siteid
		and PermissionUserSiteDeviceGroup.userid=  @UserID*/
		where devices.CustomerID = @CustomerID
		order by devices.id
	
	
END

GO
/****** Object:  StoredProcedure [dbo].[spGetDeviceSensorID]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetDeviceSensorID]
	@SerialNumber int,
	@SensorTypeID int
AS
BEGIN
	
	SET NOCOUNT ON;

    declare @DeviceID int
	declare @DeviceSensorID int

	set @DeviceSensorID=0
	--get the device id by his serial number
	select @DeviceId = id
	from devices
	where SerialNumber = @SerialNumber

	if @DeviceID>0
	begin
		--get the device sensor id
		select @DeviceSensorID = id
		from DeviceSensors
		where deviceid = @DeviceID
		and SensorTypeID = @SensorTypeID
	end

	select @DeviceSensorID
END

GO
/****** Object:  StoredProcedure [dbo].[spGetDeviceSensors]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetDeviceSensors]
	@DeviceID int
AS
BEGIN
	SET NOCOUNT ON;

	select devicesensors.id,devicesensors.SensorIndex
	,(select siteid from devices where id=@DeviceID) siteId
	,(select serialnumber from devices where id = devicesensors.DeviceID) serialNumber
	,(select devicetypes.name from devicetypes inner join devices on devices.devicetypeid=devicetypes.id where devices.id=@DeviceID) devicetype
	, 
	  case(devicesensors.sensortypeid)
	  when 9
		then 
			DefinedSensors.Name
	  else
			case 
				when len(isnull(devicesensors.sensorname,'')) =0 then SensorTypes.name 
				else devicesensors.sensorname 
			end 
	  end sensorname 
	,dbo.fnIsExternalSensor(devicesensors.sensortypeid) IsExternalSensor
	,devicesensors.sensortypeid
	, case(devicesensors.sensortypeid)
	  when 9
		then
			(select SensorUnits.name from SensorUnits where SensorUnits.id = DefinedSensors.SensorUnitID)
		else
			(select SensorUnits.name from SensorUnits where SensorUnits.id = devicesensors.SensorUnitID)
	  end measurementunit
	, /*case(devicesensors.sensortypeid)
	  when 9 
		then 
			(select SensorTypes.name from SensorTypes where SensorTypes.id = DefinedSensors.sensortypeid)
		else */
			(select SensorTypes.name from SensorTypes where SensorTypes.id = devicesensors.sensortypeid)
	  /*end*/ sensortype
	,case IsAlarmEnabled when 0 then null else PreLowValue end PreLowValue
	,case IsAlarmEnabled when 0 then null else LowValue end LowValue
	,case IsAlarmEnabled when 0 then null else PreHighValue end PreHighValue
	,case IsAlarmEnabled when 0 then null else HighValue end HighValue
	,case IsAlarmEnabled when 0 then null else PreDelayValue end PreDelayValue
	,case IsAlarmEnabled when 0 then null else DelayValue end DelayValue
	,case IsAlarmEnabled when 0 then null else BuzzDuration end BuzzDuration
	,IsAlarmEnabled,isnull(devicesensors.MinValue,0)minvalue,isnull(devicesensors.MaxValue,0)maxvalue,CalibrationExpiry,isenabled
	,isnull(AlarmNotifications.Silent,0)Silent
	,DefinedSensors.FamilyTypeID
	,isnull(DefinedSensors.DecimalPlaces,0)DecimalPlaces
	,DefinedSensors.Gain, DefinedSensors.Offset
	,isnull(DefinedSensors.Log1,0)Log1, isnull(DefinedSensors.Log2,0)Log2
	,isnull(DefinedSensors.Ref1,0)Ref1, isnull(DefinedSensors.Ref2,0)Ref2
	from devicesensors
	left join SensorAlarm
	on SensorAlarm.DeviceSensorID=DeviceSensors.ID
	left join AlarmNotifications
	on AlarmNotifications.DeviceSensorID = devicesensors.ID
	inner join SensorTypes
	on devicesensors.SensorTypeID = SensorTypes.ID
	left join DefinedSensors
	on DefinedSensors.ID = devicesensors.DefinedSensorID
	where devicesensors.deviceid =@DeviceID

	

END

GO
/****** Object:  StoredProcedure [dbo].[spGetDeviceSensorsID]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetDeviceSensorsID]
	@SerialNumber int
AS
BEGIN
	SET NOCOUNT ON;

    declare @DeviceID int
	declare @DeviceSensorID int
	declare @LastSetupTime int

	set @DeviceSensorID=0
	--get the device id by his serial number
	select @DeviceId = id,@LastSetupTime=isnull(LastSetupTime,dbo.fnUNIX_TIMESTAMP(getdate()-1))
	from devices
	where SerialNumber = @SerialNumber

	if @DeviceID>0
	begin
		--get the device sensor id
		select SensorTypeID,id,@LastSetupTime LastSetupTime
		from DeviceSensors
		where deviceid = @DeviceID
		and isEnabled=1
	end

	
END

GO
/****** Object:  StoredProcedure [dbo].[spGetDeviceSetupFiles]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetDeviceSetupFiles]
	@UserID int,
	@DeviceTypeID int=0
AS
BEGIN
	SET NOCOUNT ON;

    declare @CustomerID int
	
	select @CustomerID = customerid
	from users
	where id=@UserID

	if @DeviceTypeID=0 
	begin
	
		select (select name from devicetypes where devicetypes.id = DeviceSetupFiles.DeviceTypeID) devicetype
		, SetupID,SetupName
		from DeviceSetupFiles
		where CustomerID = @CustomerID

	end
	else
	begin 
		select (select name from devicetypes where devicetypes.id = DeviceSetupFiles.DeviceTypeID) devicetype
		, SetupID,SetupName
		from DeviceSetupFiles
		where CustomerID = @CustomerID
		and DeviceSetupFiles.DeviceTypeID = @DeviceTypeID
	end

END

GO
/****** Object:  StoredProcedure [dbo].[spGetDistributionList]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetDistributionList]
	@UserID int
AS
BEGIN
	SET NOCOUNT ON;

	declare @CustomerID int

	select @CustomerID = customerid
	from users
	where id=@UserID

    select id,name
	from Contacts
	where CustomerID=@CustomerID
    

END

GO
/****** Object:  StoredProcedure [dbo].[spGetDownloadData]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetDownloadData]
	@DeviceSensorID int,
	@IsLastDownload tinyint=0,
	@StartTime int=0,
	@EndTime int=0,
	@AlarmStatus int=0
	
AS
BEGIN
	SET NOCOUNT ON;

	declare @SQLString nvarchar(max)
	Declare @FirstDownloadTime int
	declare @LastDownloadTime int
	
	declare @WhereCond nvarchar(2000)

	set @WhereCond = ' SensorLogs.DeviceSensorID = ' + cast(@DeviceSensorID as nvarchar)

	if @IsLastDownload>0 
	begin
		select top 1 @FirstDownloadTime=FirstDownloadTime,@LastDownloadTime=LastDownloadTime
		from SensorDownloadHistory
		where DeviceSensorID=@DeviceSensorID
		order by creationdate desc

		set @WhereCond = @WhereCond + ' and SampleTime >= ' + cast(@FirstDownloadTime as nvarchar) + ' and SampleTime<= ' + cast(@LastDownloadTime as nvarchar)
	end
	
	if @StartTime>0 and @EndTime>0
	begin
		set @WhereCond = @WhereCond + ' and SampleTime >= ' + cast(@StartTime as nvarchar) + ' and SampleTime<= ' + cast(@EndTime as nvarchar)
	end

	if @AlarmStatus > 0
	begin
		set @WhereCond = @WhereCond + ' and AlarmStatus = ' + cast(@AlarmStatus as nvarchar)
	end
	--dateadd(second,SampleTime,'1970-01-01 00:00:00')
	set @SQLString = ' select Value,SampleTime,IsTimeStamp,Comment TimeStampComment
	,isnull(AlarmStatus,0)AlarmStatus,IsDummy
	from SensorLogs
	where ' + @WhereCond + '
	order by sampletime asc '

	EXEC (@SQLString)
	
END

GO
/****** Object:  StoredProcedure [dbo].[spGetGroupContacts]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetGroupContacts]
	@UserID int
AS
BEGIN
	SET NOCOUNT ON;

    declare @CustomerID int

	select @CustomerID=users.CustomerID
	from users
	where id=@UserID

	select groups.id GroupID ,groups.name groupName
	,  0 isSystem, contacts.id contactId, contacts.name, '' username
	,contacts.title,contacts.PhoneNumber,contacts.email,contacts.countryCode
	,(select name  from ContactsWorkHoursMode where ContactsWorkHoursMode.id= contacts.WorkingHoursID)workHoursMode
	,contacts.WorkdayStart,contacts.WorkdayEnd
	,contacts.SMSResendInterval smsResends
	,isnull(WorkingSun,0)WorkingSun,isnull(WorkingMon,0)WorkingMon,isnull(WorkingTue,0)WorkingTue
	,isnull(WorkingWed,0)WorkingWed,isnull(WorkingThu,0)WorkingThu,isnull(WorkingFri,0)WorkingFri
	,isnull(WorkingSat,0)WorkingSat
	from Groups
	inner join ContactDistributionGroups
	on ContactDistributionGroups.groupid=Groups.ID
	inner join contacts
	on contacts.id=ContactDistributionGroups.contactid
	where contacts.CustomerID=@CustomerID
	and isnull(contacts.deleted,0)=0

	union all
	select groups.id GroupID ,groups.name groupName
	,0,0,'','','',0,'','','',0,0,0,0,0,0,0,0,0,0
	from groups 
	where customerid=@CustomerID

	union all
	
	select groups.id  ,groups.name 
	,  1 , users.id contactId, users.username, '' 
	,'',users.mobilenumber,users.email,users.countryCode
	,''	,0,0
	,0,0,0,0,0,0,0,0
	from groups 
	inner join ContactDistributionGroups
	on ContactDistributionGroups.groupid=Groups.ID
	inner join users
	on users.id=ContactDistributionGroups.contactid
	where groups.customerid=@CustomerID

	order by groups.name

END

GO
/****** Object:  StoredProcedure [dbo].[spGetGroupPermissions]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetGroupPermissions]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT PermissionGroups.id, PermissionGroups.Name
	from PermissionGroups
	where PermissionGroups.isadmin=0;
	
END

GO
/****** Object:  StoredProcedure [dbo].[spGetGroupsByContactID]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetGroupsByContactID]
	@ContactID int
AS
BEGIN
	SET NOCOUNT ON;

    select ContactDistributionGroups.GroupID
	from ContactDistributionGroups
	where contactid=@ContactID

END

GO
/****** Object:  StoredProcedure [dbo].[spGetLanguages]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetLanguages]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT languages.id,languages.name
	from languages
END

GO
/****** Object:  StoredProcedure [dbo].[spGetLastSensorDownload]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetLastSensorDownload]
	@SerialNumber int,
	@SensorTypeID int
AS
BEGIN
	SET NOCOUNT ON;

    if exists (select top 1 id from SensorDownloadHistory 
				where SerialNumber=@SerialNumber
				and SensorTypeID = @SensorTypeID)
	begin
		select top 1 FirstDownloadTime,LastDownloadTime
		from SensorDownloadHistory
		where SerialNumber=@SerialNumber
		and SensorTypeID = @SensorTypeID
		order by creationdate desc
	end
	else
	begin
		select LastSetupTime FirstDownloadTime,0 LastDownloadTime
		from devices
		where SerialNumber=@SerialNumber
	end
END

GO
/****** Object:  StoredProcedure [dbo].[spGetLastSensorLog]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetLastSensorLog]
	@SensorID int
AS
BEGIN
	SET NOCOUNT ON;

	declare @SQLString nvarchar(max)

	set @SQLString = '
     select value , max(sampletime) sampletime,istimestamp,timestampcomment
	from sensorlogs '

	if @SensorID>0

	set @SQLString+=' where sensorid = ' + cast( @SensorID as nvarchar)

	set @SQLString += ' Group by value, istimestamp,timestampcomment ' 

	exec (@SQLString)

END

GO
/****** Object:  StoredProcedure [dbo].[spGetMatrix]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetMatrix]
	@UserID int,
	@groupLevel int
AS
BEGIN
	SET NOCOUNT ON;

	declare @CustomerID int

	select @CustomerId=customerid
	from users
	where users.id=@UserID
   
   select PrivilageName,comment,isallowed,PermissionGroupActionMatrix.PermissionActionID,PermissionGroupActionMatrix.PermissionGroupID
   from PermissionGroupActionMatrix
   inner join PermissionGroupActions
   on PermissionGroupActions.PermissionActionID = PermissionGroupActionMatrix.PermissionActionID
   and PermissionGroupActions.PermissionGroupID = PermissionGroupActionMatrix.PermissionGroupID
   inner join PermissionGroups
   on PermissionGroups.id = PermissionGroupActions.PermissionGroupID
   where PermissionGroupActions.CustomerID=@CustomerId
   and PermissionGroups.levelid=@groupLevel
   order by PrivilageName

END

GO
/****** Object:  StoredProcedure [dbo].[spGetNextSerialNumber]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetNextSerialNumber]
	@AllocateNumber int = 0
AS
BEGIN
	SET NOCOUNT ON;

	declare @SerialNumber int
	declare @NewSerialNumber int

    if not exists(select top 1 serialnumber from SerialNumberGeneration)
	begin
		--get the max serial number and store its next value in the table of generation
		select @SerialNumber = max(serialnumber) 
		from devices
	end
	else
	begin
		--get the serial number from generation table
		select @SerialNumber = serialnumber
		from SerialNumberGeneration
	end

	--advance the serial number by 1 to allocate a new number
	set @NewSerialNumber = @SerialNumber + 1
	--if the allocate number exists then update it in the table of generation
	if @AllocateNumber>0
	begin
		update  SerialNumberGeneration
		set serialnumber = @NewSerialNumber + @AllocateNumber
	end
	--return the new serial number
	select @NewSerialNumber
END

GO
/****** Object:  StoredProcedure [dbo].[spGetNotificationContacts]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetNotificationContacts]
	@SerialNumber int,
	@SensorTypeID int=0
AS
BEGIN
	SET NOCOUNT ON;

    declare @DeviceID int
	declare @DeviceSensorID int
	
	select @DeviceID=id
	from devices
	where SerialNumber = @SerialNumber

	select @DeviceSensorID = ID
	from DeviceSensors
	where DeviceID=@DeviceID
	and SensorTypeID =@SensorTypeID

	if @DeviceSensorID>0
	begin
		select email,ReceiveEmail,ReceiveSMS
		,SMSResendInterval,CountryCode, PhoneNumber
		, isnull(AlarmEmailSent,0)EmailSent,isnull( AlarmSMSSent,0)SMSSent, DeviceAlarmNotifications.ContactID
		, isnull(ReceiveBatteryLow,0) ReceiveBatteryLow, isnull(BatteryLowValue,0)BatteryLowValue
		, isnull(BatteryEmailSent,0) BatteryEmailSent, isnull(BatterySMSSent,0) BatterySMSSent
		,SMSResendInterval
		from DeviceAlarmNotifications
		left join DeviceAlarmNotificationTrail
		on DeviceAlarmNotificationTrail.DeviceSensorID = DeviceAlarmNotifications.DeviceSensorID
		and DeviceAlarmNotificationTrail.ContactID = DeviceAlarmNotifications.ContactID
		inner join Contacts
		on DeviceAlarmNotifications.ContactID= Contacts.ID
		where DeviceAlarmNotifications.DeviceSensorID=@DeviceSensorID
		
		
	end
	else
	begin 
		if @DeviceSensorID=0
		begin
			select email,ReceiveEmail,ReceiveSMS
			,SMSResendInterval,CountryCode, PhoneNumber
			, 0 EmailSent,0 SMSSent, DeviceAlarmNotifications.ContactID
			, isnull(ReceiveBatteryLow,0) ReceiveBatteryLow, isnull(BatteryLowValue,0)BatteryLowValue
			, isnull(BatteryEmailSent,0) BatteryEmailSent, isnull(BatterySMSSent,0) BatterySMSSent
			,SMSResendInterval
			from DeviceAlarmNotifications
			inner join Contacts
			on DeviceAlarmNotifications.ContactID= Contacts.ID
			where DeviceAlarmNotifications.DeviceSensorID=@DeviceSensorID
		end
	end
END

GO
/****** Object:  StoredProcedure [dbo].[spGetPasswordExpiry]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetPasswordExpiry]
	@UserID int
AS
BEGIN
	SET NOCOUNT ON;

	declare @CustomerID int

	select @CustomerID = customerid
	from users
	where users.id=@UserID

	select isnull(passwordMinLength,8) pwdMinLength,	passwordExpiryByDays 
	from customersettings 
	where customerid=@customerid
   
END

GO
/****** Object:  StoredProcedure [dbo].[spGetPermissonRoles]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetPermissonRoles]
	@UserID int
AS
BEGIN
	SET NOCOUNT ON;

    select id , name
	from PermissionRoles


END

GO
/****** Object:  StoredProcedure [dbo].[spGetPrivilegesGroups]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetPrivilegesGroups]
	@UserID int
AS
BEGIN
	SET NOCOUNT ON;

	declare @CustomerID int

	select @CustomerID = customerid
	from users
	where users.id=@UserID

	select levelid groupLevel, name groupName,isnull(IsReadOnly,0)IsReadOnly,PermissionGroups.ID
	from PermissionGroups
	where PermissionGroups.levelid>0
	and (CustomerID=@CustomerID or CustomerID=0)
   
END

GO
/****** Object:  StoredProcedure [dbo].[spGetReportApproversReviewers]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetReportApproversReviewers]
	@ReportTemplateID int
AS
BEGIN
	SET NOCOUNT ON;

	select ReportTemplateProcessUsers.userid,users.username,processtypeid
	from ReportTemplateProcessUsers
	inner join users
	on users.id = ReportTemplateProcessUsers.userid
	where ReportTemplateID = @ReportTemplateID

END

GO
/****** Object:  StoredProcedure [dbo].[spGetReportContacts]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetReportContacts]
	@ReportTemplateID int
AS
BEGIN
	SET NOCOUNT ON;

	select Contacts.id ,Contacts.name
	from ReportContacts
	inner join Contacts
	on Contacts.id = ReportContacts.contactid
	where ReportContacts.ReportTemplateID = @ReportTemplateID

END

GO
/****** Object:  StoredProcedure [dbo].[spGetReportDevices]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetReportDevices]
	@ReportTemplateID int
AS
BEGIN
	SET NOCOUNT ON;

	select SerialNumber
	from ReportDevices
	where ReportTemplateID = @ReportTemplateID

END

GO
/****** Object:  StoredProcedure [dbo].[spGetReportProfiles]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetReportProfiles]
	@UserID int,
	@Filter varchar(50)
AS
BEGIN
	
	SET NOCOUNT ON;

	if @Filter = 'ALL'
	begin
		select reporttemplates.id profileid
		,(select username from users where id=reporttemplates.userid)createdby
		,reporttemplates.ProfileName name
		,isnull(reporttemplates.IsActive,0) activity
		,isnull(reporttemplates.Description,'')description,isnull(reporttemplates.TimeZoneOffset,0) offset,isnull(reporttemplates.PageSize,'')pagesize
		,isnull(reporttemplates.reportdateformat,'') formatdate
		,case isnull(reporttemplates.TemperatureUnit,1) when 0 then 'F' else 'C' end units
		,isnull(reporttemplates.emailaslink,0) reportAsLink, isnull(reporttemplates.EmailAsPDF,0)EmailAsPDF
		,(select name from ReportPeriodTypes where id= reporttemplates.PeriodTypeID) scheduletype
		,isnull(reporttemplates.PeriodTypeID,0)PeriodTypeID
		,isnull(periodtime,getdate())periodtime, isnull(periodday,0)periodday,isnull(periodmonth,0)periodmonth,isnull(periodstart,0)periodstart,isnull(periodend,0)periodend,isnull(creationdate,getdate())creationdate

		from reporttemplates
	
    end
	else if @Filter = 'CREATED_BY_YOU'
	begin
		select reporttemplates.id profileid
		,(select username from users where id=reporttemplates.userid)createdby
		,reporttemplates.ProfileName name
		,isnull(reporttemplates.IsActive,0) activity
		,isnull(reporttemplates.Description,'')description,isnull(reporttemplates.TimeZoneOffset,0) offset,isnull(reporttemplates.PageSize,'')pagesize
		,isnull(reporttemplates.reportdateformat,'') formatdate
		,case isnull(reporttemplates.TemperatureUnit,1) when 0 then 'F' else 'C' end units
		,isnull(reporttemplates.emailaslink,0) reportAsLink, isnull(reporttemplates.EmailAsPDF,0)EmailAsPDF
		,(select name from ReportPeriodTypes where id= reporttemplates.PeriodTypeID) scheduletype
		,isnull(reporttemplates.PeriodTypeID,0)PeriodTypeID
		,isnull(periodtime,getdate())periodtime, isnull(periodday,0)periodday,isnull(periodmonth,0)periodmonth,isnull(periodstart,0)periodstart,isnull(periodend,0)periodend,isnull(creationdate,getdate())creationdate

		from reporttemplates
		where reporttemplates.UserID = @UserID
	end
	else if @Filter = 'ACTIVE_PROFILES'
	begin
		select reporttemplates.id profileid
		,(select username from users where id=reporttemplates.userid)createdby
		,reporttemplates.ProfileName name
		,isnull(reporttemplates.IsActive,0) activity
		,isnull(reporttemplates.Description,'')description,isnull(reporttemplates.TimeZoneOffset,0) offset,isnull(reporttemplates.PageSize,'')pagesize
		,isnull(reporttemplates.reportdateformat,'') formatdate
		,case isnull(reporttemplates.TemperatureUnit,1) when 0 then 'F' else 'C' end units
		,isnull(reporttemplates.emailaslink,0) reportAsLink, isnull(reporttemplates.EmailAsPDF,0)EmailAsPDF
		,(select name from ReportPeriodTypes where id= reporttemplates.PeriodTypeID) scheduletype
		,isnull(reporttemplates.PeriodTypeID,0)PeriodTypeID
		,isnull(periodtime,getdate())periodtime, isnull(periodday,0)periodday,isnull(periodmonth,0)periodmonth,isnull(periodstart,0)periodstart,isnull(periodend,0)periodend,isnull(creationdate,getdate())creationdate

		from reporttemplates
		where IsActive=1
	end
	else if @Filter = 'ONE_TIME_PROFILES'
	begin
		select reporttemplates.id profileid
		,(select username from users where id=reporttemplates.userid)createdby
		,reporttemplates.ProfileName name
		,isnull(reporttemplates.IsActive,0) activity
		,isnull(reporttemplates.Description,'')description,isnull(reporttemplates.TimeZoneOffset,0) offset,isnull(reporttemplates.PageSize,'')pagesize
		,isnull(reporttemplates.reportdateformat,'') formatdate
		,case isnull(reporttemplates.TemperatureUnit,1) when 0 then 'F' else 'C' end units
		,isnull(reporttemplates.emailaslink,0) reportAsLink, isnull(reporttemplates.EmailAsPDF,0)EmailAsPDF
		,(select name from ReportPeriodTypes where id= reporttemplates.PeriodTypeID) scheduletype
		,isnull(reporttemplates.PeriodTypeID,0)PeriodTypeID
		,isnull(periodtime,getdate())periodtime, isnull(periodday,0)periodday,isnull(periodmonth,0)periodmonth,isnull(periodstart,0)periodstart,isnull(periodend,0)periodend,isnull(creationdate,getdate())creationdate

		from reporttemplates
		where PeriodTypeID = 4 --custom date and will be sent only once
	end
END

GO
/****** Object:  StoredProcedure [dbo].[spGetReportsArchive]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetReportsArchive]
	@UserID int,
	@ReportStartDate int,
	@ReportEndDate int
AS
BEGIN
	SET NOCOUNT ON;

	
	select ReportDeviceHistory.id,ReportTemplates.profilename
	,(select CONCAT(isnull(FirstName,''),isnull(LastName,'')) from users where users.id=ReportTemplates.UserID) generatedby
	,ReportDeviceHistory.CreationDate,ReportDevices.SerialNumber
	from ReportTemplates
	inner join ReportDevices
	on ReportDevices.ReportTemplateID = ReportTemplates.ID
	inner join ReportDeviceHistory
	on ReportDeviceHistory.SerialNumber = ReportDevices.SerialNumber
	where ReportTemplates.UserID = @UserID
	and ReportDeviceHistory.CreationDate>=@ReportStartDate
	and ReportDeviceHistory.CreationDate<=@ReportEndDate
	order by ReportDeviceHistory.CreationDate desc
   
END

GO
/****** Object:  StoredProcedure [dbo].[spGetReportsReviews]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetReportsReviews]
	@UserID int,
	@ReportType int
AS
BEGIN
	SET NOCOUNT ON;

	Declare @CustomerID int

	select @CustomerID = customerid
	from users
	where id=@UserID

	IF @ReportType=1 -- only pending
	BEGIN
		select ReportDeviceHistory.id,ReportTemplates.profilename
		,(select CONCAT(isnull(FirstName,''),isnull(LastName,'')) from users where users.id=ReportTemplates.UserID) generatedby
		,ReportDeviceHistory.CreationDate,ReportDevices.SerialNumber
		,(select name from ReportProcessTypes where ReportProcessTypes.ID = ReportTemplateProcessUsers.ProcessTypeID)processType
		,Users.Username
		from ReportTemplates
		inner join ReportDevices
		on ReportDevices.ReportTemplateID = ReportTemplates.ID
		inner join ReportDeviceHistory
		on ReportDeviceHistory.SerialNumber = ReportDevices.SerialNumber
		inner join ReportProcessUsers
		on ReportProcessUsers.ReportDeviceHistoryID = ReportDeviceHistory.ID
		inner join Users
		on Users.ID = ReportProcessUsers.UserID
		inner join ReportTemplateProcessUsers
		on ReportTemplateProcessUsers.ReportTemplateId = ReportTemplates.Id
		and ReportTemplateProcessUsers.UserId = Users.ID
		where Users.CustomerID = @CustomerID
		and ReportProcessUsers.IsConfirmed=0
		order by ReportDeviceHistory.CreationDate desc
    END
	else if @ReportType=2  -- waiting approval (review/approve)
	begin
		select ReportDeviceHistory.id,ReportTemplates.profilename
		,(select CONCAT(isnull(FirstName,''),isnull(LastName,'')) from users where users.id=ReportTemplates.UserID) generatedby
		,ReportDeviceHistory.CreationDate,ReportDevices.SerialNumber
		,(select name from ReportProcessTypes where ReportProcessTypes.ID = ReportTemplateProcessUsers.ProcessTypeID)processType
		,(select username from users where users.id= @UserID)username
		from ReportTemplates
		inner join ReportDevices
		on ReportDevices.ReportTemplateID = ReportTemplates.ID
		inner join ReportDeviceHistory
		on ReportDeviceHistory.SerialNumber = ReportDevices.SerialNumber
		inner join ReportProcessUsers
		on ReportProcessUsers.ReportDeviceHistoryID = ReportDeviceHistory.ID
		inner join ReportTemplateProcessUsers
		on ReportTemplateProcessUsers.ReportTemplateId = ReportTemplates.Id
		and ReportTemplateProcessUsers.UserId = @UserID
		where ReportProcessUsers.UserID = @UserID
		and ReportProcessUsers.IsConfirmed=0
		order by ReportDeviceHistory.CreationDate desc
	end
	else if @ReportType=3  -- waiting others approval
	begin
		select ReportDeviceHistory.id,ReportTemplates.profilename
		,(select CONCAT(isnull(FirstName,''),isnull(LastName,'')) from users where users.id=ReportTemplates.UserID) generatedby
		,ReportDeviceHistory.CreationDate,ReportDevices.SerialNumber
		,(select name from ReportProcessTypes where ReportProcessTypes.ID = ReportTemplateProcessUsers.ProcessTypeID)processType
		,Users.Username
		from ReportTemplates
		inner join ReportDevices
		on ReportDevices.ReportTemplateID = ReportTemplates.ID
		inner join ReportDeviceHistory
		on ReportDeviceHistory.SerialNumber = ReportDevices.SerialNumber
		inner join ReportProcessUsers
		on ReportProcessUsers.ReportDeviceHistoryID = ReportDeviceHistory.ID
		inner join Users
		on Users.ID = ReportProcessUsers.UserID
		inner join ReportTemplateProcessUsers
		on ReportTemplateProcessUsers.ReportTemplateId = ReportTemplates.Id
		and ReportTemplateProcessUsers.UserId = Users.ID
		where Users.CustomerID = @CustomerID
		and ReportProcessUsers.UserID <> @UserID
		and ReportProcessUsers.IsConfirmed=0
		order by ReportDeviceHistory.CreationDate desc
	end
END

GO
/****** Object:  StoredProcedure [dbo].[spGetSecureQuestions]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetSecureQuestions]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT SecureQuestions.ID,SecureQuestions.Name
	from SecureQuestions

END

GO
/****** Object:  StoredProcedure [dbo].[spGetSensorLocatorByID]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetSensorLocatorByID]
	@DeviceSensorID int
AS
BEGIN
	SET NOCOUNT ON;

    SELECT SerialNumber, SensorTypeID 
	FROM DeviceSensors 
	INNER JOIN Devices 
	ON Devices.ID=DeviceID 
	WHERE DeviceSensors.ID=@DeviceSensorID

END

GO
/****** Object:  StoredProcedure [dbo].[spGetSensorLogsData]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetSensorLogsData]

@StartTime int=0,
@Endtime int=0

AS
BEGIN
	SET NOCOUNT ON;

	declare @SQLString nvarchar(max)
	declare @WhereCond nvarchar(2000)

	set @WhereCond = ' 1=1 '

	if	@StartTime>0 and @EndTime>0 
	begin
		set @WhereCond = @WhereCond + ' and SampleTime >= ' + cast(@StartTime as nvarchar) + ' and SampleTime<= ' + cast(@EndTime as nvarchar)
	end

	set @SQLString = ' select id,DeviceSensorID,Value,SampleTime,IsTimeStamp,Comment ,isnull(AlarmStatus,0)AlarmStatus,IsDummy
	from SensorLogs
	where ' + @WhereCond + '
	order by sampletime asc '

	EXEC (@SQLString)	


END

GO
/****** Object:  StoredProcedure [dbo].[spGetSensorUnits]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetSensorUnits]
	@FamilyTypeID int
AS
BEGIN
	SET NOCOUNT ON;

	select SensorUnits.id,SensorUnits.name
	from SensorUnits
	inner join BaseSensorUnits
	on BaseSensorUnits.SensorUnitID=SensorUnits.ID
	where BaseSensorUnits.FamilyTypeID = @FamilyTypeID
    
END

GO
/****** Object:  StoredProcedure [dbo].[spGetSystemSettings]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetSystemSettings]
	@UserID int
	
AS
BEGIN
	SET NOCOUNT ON;

	declare @CustomerId int

	select @CustomerId = customerid
	from Users
	where id=@UserID

	select isCFREnabled,isnull(TemperatureUnit,1)TemperatureUnit,debugmode 
	,(select code from languages where languages.id=CustomerSettings.LanguageID)languageCode
	,timezonevalue,timezoneabbr,timezoneoffset,timezoneisdst,timezonetext
	from CustomerSettings
    where customerid=@CustomerID

END

GO
/****** Object:  StoredProcedure [dbo].[spGetUser]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetUser]
	@UserID int
AS
BEGIN
	SET NOCOUNT ON;

	select email,id userid, firstname,LastName,Username,customerid
	,isnull(mobilenumber,0)mobilenumber,isnull(countrycode,0)countrycode
	,isnull((select name from sites where sites.id=PermissionUserSiteDeviceGroup.siteid),'ALL') sitename
	,isnull((select name from PermissionGroups where PermissionGroups.id = PermissionUserSiteDeviceGroup.permissiongroupid)
	 , (select name from PermissionGroups where PermissionGroups.id = users.permissiongroupid))	 privilagelevel
	from users
	left join PermissionUserSiteDeviceGroup
	on PermissionUserSiteDeviceGroup.userid = users.id
	where id=@UserID

END

GO
/****** Object:  StoredProcedure [dbo].[spGetUserAnalytics]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetUserAnalytics]
	@UserID int,
	@DeviceSensorID int
AS
BEGIN
	SET NOCOUNT ON;

    SELECT isnull(LowLimit,0)LowLimit, isnull(HighLimit,0)HighLimit, isnull(ActivationEnergy ,83.14472)ActivationEnergy,IsActive
	,(select name from SensorTypes inner join  DeviceSensors on DeviceSensors.SensorTypeID = SensorTypes.ID where DeviceSensors.id=@DeviceSensorID) sensor
	FROM AnalyticsSensors 
	WHERE UserID=@UserID 
	AND DeviceSensorID=@DeviceSensorID

END

GO
/****** Object:  StoredProcedure [dbo].[spGetUserRecovery]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetUserRecovery]
	@UserID int
AS
BEGIN
	SET NOCOUNT ON;

	update users
	set PasswordResetGUID=NEWID()
	,PasswordResetExpiry = dateadd(hour,24,getdate())
	where id=@UserID

	select email,id userid, firstname,LastName,Username,PasswordResetGUID
	,isnull(mobilenumber,0)mobilenumber,isnull(countrycode,0)countrycode
	,(select name from sites where sites.id=PermissionUserSiteDeviceGroup.siteid) sitename
	,(select name from PermissionGroups where PermissionGroups.id = PermissionUserSiteDeviceGroup.permissiongroupid) privilagelevel
	from users
	left join PermissionUserSiteDeviceGroup
	on PermissionUserSiteDeviceGroup.userid = users.id
	where id=@UserID

END

GO
/****** Object:  StoredProcedure [dbo].[spGetUsers]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetUsers]
	@UserID int
AS
BEGIN
	SET NOCOUNT ON;

	declare @CustomerID int
	create table #temp (id INT,name varchar(50),parentid int,[level] int, treepath varchar(500))

	insert into #temp
	exec spGetUserSites @UserID

		
	select @CustomerID=customerid
	from users
	where id=@UserID

	select users.ID , users.Username, users.creationdate, users.email,users.languageid
		,(select languages.name from languages where languages.id= users.Languageid)defaultlanguage
		,defaultdateformat
		,PermissionGroups.name groupname , PermissionGroups.levelID
		,PermissionUserSiteDeviceGroup.siteid
		,(select count(userid) from PermissionUserSiteDeviceGroup where UserID=users.id) permissionCount
		, #temp.Name ,#temp.ParentID, #temp.level, #temp.treepath
		,isnull(users.InactivityTimeout,60)InactivityTimeout
	from users
		inner join PermissionUserSiteDeviceGroup
		on PermissionUserSiteDeviceGroup.UserID=users.id
		inner join PermissionGroups
		on PermissionGroups.id = PermissionUserSiteDeviceGroup.PermissionGroupID
		inner join #temp
		on  #temp.ID = PermissionUserSiteDeviceGroup.SiteID
	where isnull(users.deleted,0)=0
		and users.CustomerID=@CustomerID
	union all
	select users.ID , users.Username, users.creationdate, users.email,users.languageid
		,(select languages.name from languages where languages.id= users.Languageid)defaultlanguage
		,defaultdateformat
		,PermissionGroups.name groupname , PermissionGroups.levelID
		,0
		,0
		, '' ,0, 0,''
		,isnull(users.InactivityTimeout,60)
	from users
		inner join PermissionGroups
		on PermissionGroups.id = users.PermissionGroupID
	where isnull(users.deleted,0)=0
		and users.CustomerID=@CustomerID
	

	
END

GO
/****** Object:  StoredProcedure [dbo].[spGetUserSites]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetUserSites]
	-- Add the parameters for the stored procedure here
	@UserID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	


	WITH  tree
        AS (
            SELECT  id, [Name], ParentID,0 as level,
                    CAST(([Name]) AS VARCHAR(1000)) AS treepath
            FROM    sites
            WHERE   ParentId =0
            UNION ALL
              --recursive member
            SELECT  t.id, t.[Name], t.ParentID,a.level + 1,
                    CAST((a.treepath + '/' + t.Name) AS VARCHAR(1000)) AS treepath
            FROM    sites AS t
                    JOIN tree AS a
                    ON t.ParentId = a.id
					/*inner join (select  userid,siteid 
								from PermissionUserSiteDeviceGroup
								)permissionsites
					on permissionsites.SiteID = t.ID
			where permissionsites.UserID = @UserID*/
			where t.CustomerID = (select customerid from users where id=@UserID)
           )
	SELECT distinct * FROM tree

END

GO
/****** Object:  StoredProcedure [dbo].[spHideAlarmNotification]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spHideAlarmNotification]
	@AlarmID int,
	@SerialNumber int=0,
	@AlarmTypeID int=0,
	@DeviceSensorID int=0
AS
BEGIN
	SET NOCOUNT ON;

	if (@AlarmID <> 0) /* Hide by ID */
		update AlarmNotifications set Hide=1 where ID=@AlarmID
	ELSE
	BEGIN
		declare @DeviceID int
	
		select @DeviceID=id
		from devices
		where serialnumber=@SerialNumber

	
		update AlarmNotifications
		set Hide=1
		where DeviceID=@DeviceID 
		and AlarmTypeID = @AlarmTypeID 
		and DeviceSensorID=@DeviceSensorID
	END
END

GO
/****** Object:  StoredProcedure [dbo].[spHideAllAlarmNotifications]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spHideAllAlarmNotifications]

	@UserID int

AS
BEGIN
	SET NOCOUNT ON;

    update AlarmNotifications
	set Hide=1
	where AlarmNotifications.Hide=0
	and exists
	(select top 1 id
	 from devices
	 inner join PermissionUserSiteDeviceGroup
	 on PermissionUserSiteDeviceGroup.SiteID = devices.SiteID
	 where PermissionUserSiteDeviceGroup.UserID=@UserID
	 and AlarmNotifications.DeviceID=devices.ID
	)
END

GO
/****** Object:  StoredProcedure [dbo].[spInsertAlarmNotification]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spInsertAlarmNotification]
	@SerialNumber int,
	@AlarmTypeID int,
	@Value float,
	@StartTime int,
	@SensorTypeID int=0
AS
BEGIN
	SET NOCOUNT ON;

	declare @DeviceID int
	declare @DeviceSensorID int
	declare @AlarmNotificationID int
	declare @AlarmAlreadyVisible int

	select @DeviceID=id
	from devices
	where serialnumber=@SerialNumber

	select @DeviceSensorID = DeviceSensors.id
	from DeviceSensors
	where DeviceSensors.DeviceID=@DeviceID
	and DeviceSensors.SensorTypeID = @SensorTypeID

	select @AlarmNotificationID=ID FROM AlarmNotifications 
	where DeviceID=@DeviceID and AlarmTypeID = @AlarmTypeID and DeviceSensorID=@DeviceSensorID

    if @AlarmNotificationID IS NULL
	begin
		insert into AlarmNotifications(DeviceID,AlarmTypeID,Value,StartTime,DeviceSensorID)
		values(@DeviceID,@AlarmTypeID,@Value,@StartTime,@DeviceSensorID)

		set @AlarmNotificationID = @@Identity
		SET @AlarmAlreadyVisible = 0
	end
	else
	begin
		DECLARE @IsHidden INT
		select @IsHidden=Hide FROM AlarmNotifications WHERE ID=@AlarmNotificationID
		IF @IsHidden = 1
			SET @AlarmAlreadyVisible = 0
		else
			SET @AlarmAlreadyVisible = 1
		
		/*
		select @AlarmNotificationID=ID
		from AlarmNotifications
		where DeviceID=@DeviceID 
		and AlarmTypeID = @AlarmTypeID 
		and DeviceSensorID=@DeviceSensorID
		*/
		update AlarmNotifications
		set Value=@Value, Hide=0
		where ID=@AlarmNotificationID
	end

	if (@AlarmAlreadyVisible = 0)
		SELECT 0
	ELSE
		select AlarmNotifications.ID, Devices.Name AS DeviceName, AlarmNotifications.Value,AlarmNotifications.StartTime, AlarmNotifications.ClearTime, AlarmNotifications.AlarmReason,
		AlarmTypes.Name AS AlarmTypeName, AlarmNotifications.AlarmTypeID, AlarmNotifications.DeviceSensorID, Devices.SerialNumber
		FROM AlarmNotifications
		JOIN Devices ON Devices.ID = AlarmNotifications.DeviceID
		JOIN AlarmTypes ON AlarmTypes.ID = AlarmNotifications.AlarmTypeID
		WHERE AlarmNotifications.ID = @AlarmNotificationID
	/*
	devices.id, 
	,
	,(select AlarmTypes.name from AlarmTypes where AlarmTypes.id=AlarmNotifications.alarmtypeid)alarmtype
	,AlarmNotifications.AlarmTypeID,AlarmNotifications.DeviceSensorID,devices.SerialNumber
	from AlarmNotifications
	inner join devices
	on devices.id=AlarmNotifications.deviceid
	inner join PermissionUserSiteDeviceGroup
	on PermissionUserSiteDeviceGroup.siteid = devices.siteid
	where AlarmNotifications.id=@AlarmNotificationID
	*/
END

GO
/****** Object:  StoredProcedure [dbo].[spInsertDownloadHistory]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spInsertDownloadHistory]
	@SerialNumber int,
	@SensorTypeID int,
	@FirstDownloadTime int,
	@LastDownloadTime int
AS
BEGIN
	SET NOCOUNT ON;
		
		delete from SensorDownloadHistory
		where SerialNumber=@SerialNumber
		and SensorTypeID=@SensorTypeID

		insert into SensorDownloadHistory(SerialNumber, SensorTypeID, FirstDownloadTime, LastDownloadTime)
		values(@SerialNumber,@SensorTypeID,@FirstDownloadTime,@LastDownloadTime)
	
END

GO
/****** Object:  StoredProcedure [dbo].[spIsCommandAllowed]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spIsCommandAllowed]
	@UserID int,
	@Command nvarchar(50),
	@Entity nvarchar(50),
	@SiteID int=0
AS
BEGIN
	SET NOCOUNT ON;

	declare @CommandEntityId int
	declare @PermissionGroupID int
	declare @IsAllowed int
	declare @ActionTypeID int

	declare @CustomerID int

	select @CustomerID=customerid
	from Users
	where Users.id	= @UserID

	set @IsAllowed=0

	if not exists (select top 1 id 
					from PermissionActions 
					where Command=@Command
					and Entity=@Entity) 
		set @ActionTypeID=2
	else
	begin
		select @CommandEntityId = id,@ActionTypeID=ActionTypeID
		from PermissionActions
		where Command=@Command
		and Entity=@Entity
	end

	if @ActionTypeID =2
	   set @IsAllowed=1
	else
	begin
		if exists (select top 1 userid from PermissionUserSiteDeviceGroup where userid=@UserID and siteid=@SiteID)
		begin
			
			select @PermissionGroupID = PermissionUserSiteDeviceGroup.PermissionGroupID
			from PermissionUserSiteDeviceGroup
			inner join PermissionGroupActions
			on PermissionGroupActions.PermissionGroupID = PermissionUserSiteDeviceGroup.PermissionGroupID
			inner join PermissionActions
			on PermissionActions.ID = PermissionGroupActions.PermissionActionID
			where PermissionUserSiteDeviceGroup.UserID = @UserID
			and PermissionActions.Command=@Command
			and PermissionActions.Entity = @Entity
			and PermissionGroupActions.CustomerID=@CustomerID
			and PermissionUserSiteDeviceGroup.SiteID=@SiteID
		end
		else
		begin

			select @PermissionGroupID = isnull(Users.PermissionGroupID,0)
			from Users
			where id=@UserID

			if @PermissionGroupID=0 and @SiteID =0
				set @IsAllowed=1

		end
		--checks if no userid and the action is type 2 then return allowed
		if @userID=0 and @ActionTypeID=2 
		begin
			set @IsAllowed=1
		end
		else
		begin
			if @CommandEntityId>0 and @PermissionGroupID>0
			begin
				if @ActionTypeID=2 --this is always allowed for application activity
				begin
					set @IsAllowed=1
				end
				else
				begin
					select @IsAllowed = IsAllowed
					from PermissionGroupActions
					where PermissionGroupID=@PermissionGroupID
					and PermissionActionID = @CommandEntityId
				end
			end
		end
	end
	select @IsAllowed


END

GO
/****** Object:  StoredProcedure [dbo].[spIsDeviceAutoSetup]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spIsDeviceAutoSetup]

	@SerialNumber int

	
AS
BEGIN
	SET NOCOUNT ON;

	declare @CustomerId int
	declare @DeviceTypeID int
	
	select	@DeviceTypeID=isnull(DeviceTypeID,0)
			,@CustomerId = CustomerID
	from devices
	where SerialNumber=@SerialNumber

	if not exists (select top 1 id from devices where serialnumber =@SerialNumber)
		or @DeviceTypeID=0
	begin

		select SetupName,DeviceAutoSetup.customerid
			,(select name from DeviceTypes where DeviceTypes.ID =@DeviceTypeID )devicetype
		from DeviceAutoSetup
		inner join DeviceSetupFiles
		on DeviceSetupFiles.SetupID = DeviceAutoSetup.SetupID
		where DeviceAutoSetup.customerid= @CustomerID
		and DeviceAutoSetup.DeviceTypeID = @DeviceTypeID
		and IsAutoSetup=1
		and isnull(DeviceAutoSetup.SetupID,'')<>''
	end
	
END

GO
/****** Object:  StoredProcedure [dbo].[spPasswordRecovery]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spPasswordRecovery]
	@Email nvarchar(255)
AS
BEGIN
	SET nocount on
	declare @userID int

    if exists(select top 1 id from users where email=@Email)
	begin
		select @userID=id
		from users 
		where email=@Email 
		

		--checks if the token NOT expired 
		if exists(select top 1 id 
				  from users 
				  where PasswordResetGUID is not null 
				  and isnull(PasswordResetExpiry,getdate())>getdate()
				  and id=@userID)
		begin
			select 0
		end
		else
		begin
			select questionid,(select name from SecureQuestions where id=questionid)question
			from userSecureQuestions
			where userid=@userID
		end
	end
	else
	begin
		select 0
	end

END

GO
/****** Object:  StoredProcedure [dbo].[spRegisterLanRcv]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spRegisterLanRcv]
	-- Add the parameters for the stored procedure here
	@CustomerID INT,
	@UserName NVARCHAR(50),
	@Password NVARCHAR(50),
	@MacAddress NVARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @passCheck INT;
	DECLARE @DeviceTypeID INT;

	SET @passCheck = 0;

    IF NOT EXISTS (SELECT TOP 1 dbo.Devices.ID FROM dbo.Devices WHERE CustomerID=@CustomerID AND MacAddress = @MacAddress)
	BEGIN
		
		SELECT @DeviceTypeID = dbo.DeviceTypes.ID
		FROM dbo.DeviceTypes
		WHERE dbo.DeviceTypes.Name = 'LAN-R';
    
		INSERT dbo.Devices
		        ( CustomerID ,
		          DeviceTypeID ,
		          MacAddress ,
		          UserName,
				  UserPassword
		        )
		VALUES  ( @CustomerID ,
		          @DeviceTypeID,
				  @MacAddress ,
		          @UserName, 
				  @Password 
		        );
		
		SET @passCheck = 1;
	END  

	SELECT @passCheck;

	SET NOCOUNT OFF;
END

GO
/****** Object:  StoredProcedure [dbo].[spRelateDeviceToSite]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spRelateDeviceToSite]
	@SerialNumber int,
	@SiteID int
AS
BEGIN
	SET NOCOUNT ON;

    update devices
	set SiteID = @SiteID
	where SerialNumber=@SerialNumber
END

GO
/****** Object:  StoredProcedure [dbo].[spResetPassword]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spResetPassword]
	@NewPassword NVARCHAR(255),
	@GuidReset UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @IsCredentialValid INT;
	DECLARE @IsResetPass INT;
	Declare @UserName nvarchar(50);

	SET @IsCredentialValid=0;
	SET @IsResetPass = 0;

    --first checks if the guid is valid
	IF EXISTS (SELECT TOP 1 dbo.Users.ID 
				FROM Users 
				WHERE Users.PasswordResetGUID =@GuidReset  )
	BEGIN
    
		select @UserName = username
		from Users 
		WHERE Users.PasswordResetGUID =@GuidReset

		--then check for valid credentials
		SELECT @IsCredentialValid = dbo.fnValidateCredentialPolicy(@UserName,@NewPassword);

		IF @IsCredentialValid = 0
		BEGIN
			--then clean the guid
			--and update the new password
			UPDATE Users
			SET PasswordResetGUID = null
			, UserPassword = @NewPassword
			, PasswordResetExpiry=null
			WHERE Users.Username=@UserName;
			
			SET @IsResetPass = 1;
		
		END
		ELSE
		BEGIN
			set @IsResetPass = @IsCredentialValid
		END
	END
  
	SELECT @IsResetPass;  
END

GO
/****** Object:  StoredProcedure [dbo].[spSaveAlarmNotifications]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spSaveAlarmNotifications]
	@ContactID int,
	@SerialNumber int,
	@ReceiveEmail tinyint,
	@ReceiveSMS tinyint,
	@ReceiveBatteryLow tinyint,
	@BatteryLowValue int =0
AS
BEGIN
	SET NOCOUNT ON;

	Declare @DeviceID int

	select @DeviceID=ID
	from Devices
	where SerialNumber=@SerialNumber

    if exists(select top 1 deviceid from DeviceAlarmNotifications where deviceid=@DeviceID and contactid=@ContactID)
	begin
		update DeviceAlarmNotifications
		set ReceiveEmail = @ReceiveEmail,
		ReceiveSMS = @ReceiveSMS,
		ReceiveBatteryLow = @ReceiveBatteryLow,
		BatteryLowValue=@BatteryLowValue
		where deviceid=@DeviceID 
		and contactid=@ContactID
	end
	else
	begin
		insert DeviceAlarmNotifications(DeviceID,ContactID,ReceiveEmail,ReceiveSMS,ReceiveBatteryLow,BatteryLowValue)
		values(@DeviceID,@ContactID,@ReceiveEmail,@ReceiveSMS,@ReceiveBatteryLow,@BatteryLowValue)

		insert into DeviceAlarmNotificationTrail(DeviceID,ContactID,DeviceSensorID)
		select @DeviceID,@ContactID,id
		from DeviceSensors
		where DeviceID=@DeviceID

	end

	select @@RowCount
END

GO
/****** Object:  StoredProcedure [dbo].[spSaveMatrix]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spSaveMatrix]
	@UserID int,
	@PermissionActionID int,
	@PermissionGroupID int,
	@IsAllowed tinyInt
AS
BEGIN
	SET NOCOUNT ON;

	declare @CustomerID int

	select @CustomerID=customerid
	from users
	where id=@UserID

    if exists (select top 1 permissionactionid 
				from PermissionGroupActions 
				where PermissionActionID=@PermissionActionID 
				and PermissionGroupID=@PermissionGroupID 
				and CustomerID=@CustomerID)
	begin
		update PermissionGroupActions
		set IsAllowed=@IsAllowed
		where PermissionActionID=@PermissionActionID 
			and PermissionGroupID=@PermissionGroupID 
			and CustomerID=@CustomerID
	end
	else
	begin
		insert into PermissionGroupActions(PermissionActionID,PermissionGroupID,CustomerID,IsAllowed)
		values(@PermissionActionID,@PermissionGroupID,@CustomerID,@IsAllowed)
	end


END

GO
/****** Object:  StoredProcedure [dbo].[spSaveReport]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spSaveReport]
	@SerialNumber int,
	@image image
	AS
BEGIN
	SET NOCOUNT ON;

	insert into ReportDeviceHistory(SerialNumber, PDFStream,CreationDate)
	values(@SerialNumber,@image,DATEDIFF(second,{d '1970-01-01'},GETDATE()))

END

GO
/****** Object:  StoredProcedure [dbo].[spSaveSystemSettings]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spSaveSystemSettings]
	@UserID int,
	@isCFREnabled int,
	@TemperatureUnit int,
	@debugmode int,
	@LanguageCode varchar(10),
	@timezonevalue varchar(20),
	@timezoneabbr varchar(5),
	@timezoneoffset int,
	@timezoneisdst int,
	@timezonetext varchar(255)
AS
BEGIN
	SET NOCOUNT ON;

	declare @CustomerId int
	declare @LanguageID int

	select @LanguageId=id 
	from languages
	where code=@LanguageCode

	select @CustomerId = customerid
	from Users
	where id=@UserID

	update customersettings
	set isCFREnabled=@isCFREnabled,
	TemperatureUnit=@TemperatureUnit,
	debugmode=@debugmode,
	LanguageID=@LanguageID,
	timezonevalue=@timezonevalue,
	timezoneabbr=@timezoneabbr,
	timezoneoffset=@timezoneoffset,
	timezoneisdst=@timezoneisdst,
	timezonetext=@timezonetext
	where CustomerID=@CustomerId

	select isCFREnabled,isnull(TemperatureUnit,1)TemperatureUnit,isnull(debugmode,0) debugMode
	,(select code from languages where languages.id=CustomerSettings.LanguageID)languageCode
	,timezonevalue,timezoneabbr,timezoneoffset,timezoneisdst,timezonetext
	from CustomerSettings
    where customerid=@CustomerID

END

GO
/****** Object:  StoredProcedure [dbo].[spSaveUser]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spSaveUser]

	@UserID int,
	@UserName varchar(50),
	@FirstName varchar(32),
	@LastName varchar(32),
	@Email varchar(255),
	@countryCode varchar(4),
	@mobileNumber varchar(10)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	update users
	set username=@userName,
	firstName=@FirstName,
	lastname=@LastName,
	email=@Email,
	countrycode=@countryCode,
	mobilenumber=@mobileNumber
	where ID=@UserID
	and username<>'admin'

	select email,id userid, firstname,LastName,Username
	,isnull(mobilenumber,0)mobilenumber,isnull(countrycode,0)countrycode
	,isnull((select name from sites where sites.id=PermissionUserSiteDeviceGroup.siteid),'ALL') sitename
	,isnull((select name from PermissionGroups where PermissionGroups.id = PermissionUserSiteDeviceGroup.permissiongroupid)
	 , (select name from PermissionGroups where PermissionGroups.id = users.permissiongroupid))	 privilagelevel
	from users
	left join PermissionUserSiteDeviceGroup
	on PermissionUserSiteDeviceGroup.userid = users.id
	where id=@UserID
    
END

GO
/****** Object:  StoredProcedure [dbo].[spSecurityAnswerCheck]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spSecurityAnswerCheck]
	@Email nvarchar(255),
	@Answer nvarchar(500)
AS
BEGIN
	SET NOCOUNT ON;

    declare @userID int
	declare @StoreAnswer nvarchar(500)
	declare @token uniqueidentifier 
	--checks first if this email exists for this customer
    if exists(select top 1 id from users where email=@Email )
	begin
		select @userID=id
		from users 
		where email=@Email 
		
		--get the stored answer 
		/*select @StoreAnswer=answer
		from userSecureQuestions
		where userid=@userID
		--checks if the answer is identical
		if @StoreAnswer = @Answer
		begin*/
			set @token = newid()

			update users
			set passwordResetGUID=@token
			,PasswordResetExpiry = dateadd(day,1, getdate())
			where id=@userID

			select 1,@token token,username,email
			from users
			where id=@userID
		/*end
		else
		begin
			select 0
		end*/
	end
	else
	begin
		select 0
	end
END

GO
/****** Object:  StoredProcedure [dbo].[spSetCalibrationCertificateDefault]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spSetCalibrationCertificateDefault]
	@UserID int,
	@calibtemperature float,
	@calibhumidity float,
	@calibfourtecwaxseal int,
	@calibmanufacture varchar(100),
	@calibmodel varchar(100),
	@calibnist int,
	@calibproductname varchar(255),
	@calibserialnumber int
AS
BEGIN
	SET NOCOUNT ON;

	declare @CustomerID int

	select @CustomerID=customerid
	from users
	where id=@UserID
   
    update customersettings
	set calibtemperature=@calibtemperature,
	calibhumidity=@calibhumidity,
	calibfourtecwaxseal=@calibfourtecwaxseal,
	calibmanufacture=@calibmanufacture,
	calibmodel=@calibmodel,
	calibnist=@calibnist,
	calibproductname=@calibproductname,
	calibserialnumber=@calibserialnumber
	where customerid= @CustomerID

END

GO
/****** Object:  StoredProcedure [dbo].[spSetupUser]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spSetupUser]

	@GUID uniqueidentifier,
	@UserName nvarchar(50),
	@userPassword nvarchar(255),
	@timezonevalue varchar(20),
	@timezoneabbr varchar(5),
	@timezoneoffset int,
	@timezoneisdst int,
	@timezonetext varchar(255),
	@CountryCode varchar(4),
	@FirstName varchar(32),
	@LastName varchar(32),
	@MobileNumber varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @CustomerID INT;
	DECLARE @ActionID INT;
	DECLARE @credentialValid INT;
	declare @PasswordResetExpiry datetime
	declare @ExpiryValid int
	declare @UserID int
	
	SELECT @CustomerID = dbo.Users.CustomerID,@PasswordResetExpiry=PasswordResetExpiry,@UserID=id
	FROM dbo.Users
	WHERE PasswordResetGUID=@GUID;

	set @ExpiryValid = DateDiff(day,@PasswordResetExpiry,getdate())

	if exists(select top 1 users.ID from users where users.PasswordResetGUID=@GUID) 
		and @ExpiryValid>0
	begin

		--checks if the username and password are valid 
		SELECT @credentialValid = dbo.fnValidateCredentialPolicy(@UserName,@userPassword);

		IF @credentialValid=0
		BEGIN

			if not exists(select top 1 users.id from users where users.username=@UserName and users.customerid=@CustomerID
							and users.id<>@UserID)
			begin
				--insert the new user with the related customer
				update Users
				set Username= @UserName,
					UserPassword = @UserPassword,
					PasswordCreationDate=GETDATE(),
					timezonevalue=@timezonevalue,
					timezoneabbr=@timezoneabbr,
					timezoneoffset=@timezoneoffset,
					timezoneisdst=@timezoneisdst,
					timezonetext=@timezonetext,
					CountryCode=@CountryCode,
					FirstName=@FirstName,
					LastName = @LastName,
					MobileNumber=@MobileNumber
				where id= @UserID;
			
			
				--save the password in history table
				insert UserPasswordHistory(UserID,UserPassword,ChangedDate)
				values(@userID,@UserPassword,GETDATE());

				--CFR
				exec spCFRAuditTrail @UserID,'setup','user',0,'';

			end
			else
			begin	
				set @UserID = -100;
			end
		END
		else -- Credential not Valid   
		begin
			set @UserID = (-1) * @credentialValid;
		end
		
		if @UserID>0
		begin
			select id userid,defaultDateFormat,
			(select permissiongroups.Name from permissiongroups where permissiongroups.id=users.permissiongroupid) GroupName,
			email,
			(select Languages.Name from languages where languages.id = users.languageID)LanguageName
			from users
			where id=@userID;
		end
		else
		begin
			select @UserID userid
		end
	end
	else
	begin
		select -99 --guid not valid
	end
	set nocount off;
END

GO
/****** Object:  StoredProcedure [dbo].[spSnoozeAlarm]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spSnoozeAlarm]
	@SerialNumber int,
	@DeviceSensorID int,
	@Silent int
AS
BEGIN
	SET NOCOUNT ON;

    declare @DeviceID int

	select @DeviceID=devices.id
	from devices
	where serialnumber=@SerialNumber

	update AlarmNotifications
	set Silent=@Silent
	where DeviceID=@DeviceID
	and DeviceSensorID=@DeviceSensorID

	select @@rowcount
END

GO
/****** Object:  StoredProcedure [dbo].[spSystemActionsAudit]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spSystemActionsAudit]
	@UserID int,
	@Command nvarchar(50),
	@Entity nvarchar(50),
	@SerialNumber int=0
AS
BEGIN
	SET NOCOUNT ON;

	declare @PermissionActionID int
	declare @DeviceID int
	Declare @IsCFREnabled int

	select @IsCFREnabled=isnull(IsCFREnabled,0)
	from CustomerSettings
	inner join users
	on users.customerid=CustomerSettings.customerid

	if @IsCFREnabled=1
	begin
		select @PermissionActionID=id
		from PermissionActions
		where Command=@Command
		and Entity=@Entity

		set @DeviceID=0

		if @SerialNumber>0
		begin
			select @DeviceID = id
			from devices
			where serialnumber=@SerialNumber
		end

		insert into SystemActionsAudit(UserID,PermissionActionID,DeviceID)
		values(@UserID,@PermissionActionID,@DeviceID);
	end

END

GO
/****** Object:  StoredProcedure [dbo].[spUpdateAlarmNotification]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUpdateAlarmNotification]
	@SerialNumber int,
	@AlarmTypeID int,
	@SensorTypeID int=0
AS
BEGIN
	SET NOCOUNT ON;
	declare @DeviceID int
	declare @DeviceSensorID int

	select @DeviceID=id
	from devices
	where serialnumber=@SerialNumber

	select @DeviceSensorID = DeviceSensors.id
	from DeviceSensors
	where DeviceSensors.DeviceID=@DeviceID
	and DeviceSensors.SensorTypeID = @SensorTypeID
	
	update AlarmNotifications
	set ClearTime=DATEDIFF(SECOND,{d '1970-01-01'}, getdate())
	where DeviceID=@DeviceID
	and AlarmTypeID=@AlarmTypeID
	and DeviceSensorID=@DeviceSensorID
	
END

GO
/****** Object:  StoredProcedure [dbo].[spUpdateAlarmReason]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUpdateAlarmReason]
	@AlarmID int,
	@SerialNumber int, 
	@AlarmTypeID int,
	@Reason nvarchar(255),
	@DeviceSensorID int=0
AS
BEGIN
	SET NOCOUNT ON;

	if (@AlarmID <> 0)
		update AlarmNotifications set AlarmReason=@Reason where ID=@AlarmID
	ELSE
	BEGIN
		declare @DeviceID int
	
		select @DeviceID=id
		from devices
		where serialnumber=@SerialNumber

	
		update AlarmNotifications
		set AlarmReason=@Reason
		where DeviceID=@DeviceID 
		and AlarmTypeID = @AlarmTypeID 
		and DeviceSensorID=@DeviceSensorID
	END
END

GO
/****** Object:  StoredProcedure [dbo].[spUpdateAnalyticsSensors]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUpdateAnalyticsSensors]
	@UserID int,
	@DeviceSensorID int,
	@ActivationEnergy float,
	@LowLimit float,
	@HighLimit float
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE AnalyticsSensors 
	SET ActivationEnergy=@ActivationEnergy
	,LowLimit=@LowLimit
    ,HighLimit=@HighLimit 
	WHERE UserID=@UserID 
	AND DeviceSensorID=@DeviceSensorID

END

GO
/****** Object:  StoredProcedure [dbo].[spUpdateCalibrationExpiryReminder]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[spUpdateCalibrationExpiryReminder]
	@UserID int,
	@IsReminderOn int,
	@ReminderInMonth int
AS
BEGIN
	SET NOCOUNT ON;

	
	update users 
	set IsCalibrationReminderActive = @IsReminderOn,
	CalibrationReminderInMonths = @ReminderInMonth
	where id=@UserID
   
END

GO
/****** Object:  StoredProcedure [dbo].[spUpdateContact]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUpdateContact]
	@ContactID int,
	@Name nvarchar(50),
	@Title nvarchar(10),
	@Phone nvarchar(50),
	@Email nvarchar(255),
	@WeekdayStart int,
	@WorkdayStart time,
	@WorkdayEnd time,
	@OutOfOfficeStart int,
	@OutOfOfficeEnd int,
	@SMSResendInterval int
AS
BEGIN
	SET NOCOUNT ON;

	Update Contacts
	set name=@name
	, Title = @Title
	, Phone = @Phone
	, Email = @Email
	, WeekdayStart = @WeekdayStart
	, WorkdayStart = @WorkdayStart
	, WorkdayEnd = @WorkdayEnd
	, OutOfOfficeStart = @OutOfOfficeStart
	, OutOfOfficeEnd = @OutOfOfficeEnd
	, SMSResendInterval = @SMSResendInterval

	where id=@ContactID

	select @@ROWCOUNT;

END

GO
/****** Object:  StoredProcedure [dbo].[spUpdateCustomerEmailSent]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUpdateCustomerEmailSent]
	@SerialNumber int
AS
BEGIN
	SET NOCOUNT ON;

    declare @CustomerID int

	select @Customerid = customerid
	from devices
	where serialNumber = @SerialNumber

	update customers
	set EmailSentDate=getdate()
	where id=@CustomerID

END

GO
/****** Object:  StoredProcedure [dbo].[spUpdateDeviceMode]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUpdateDeviceMode]
	@SerialNumber int,
	@InFwUpdate int
AS
BEGIN
	SET NOCOUNT ON;

    
	update devices
	set InFwUpdate=@InFwUpdate
	where SerialNumber= @SerialNumber


END

GO
/****** Object:  StoredProcedure [dbo].[spUpdateDeviceSetup]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUpdateDeviceSetup]
	
	@UserID int,
	@DeviceType varchar(255),
	@SetupID varchar(255),
	@IsAutoSetup int

AS
BEGIN
	SET NOCOUNT ON;

	Declare @CustomerID int
	Declare @DeviceTypeID int

	select @DeviceTypeID=id
	from DeviceTypes
	where name = @DeviceType

	select @CustomerID=customerid
	from users
	where id=@UserID

	update DeviceAutoSetup
	set IsAutoSetup=@IsAutoSetup,
		SetupID=@SetupID
	where DeviceTypeID = @DeviceTypeID 
	and CustomerID=@CustomerID
	

END

GO
/****** Object:  StoredProcedure [dbo].[spUpdateDeviceTimerRun]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUpdateDeviceTimerRun]

	@SerialNumber int	
AS
BEGIN
	SET NOCOUNT ON;

	declare @TimerRunEnabled tinyint
	Declare @DeviceID int
	declare @result int

	set @result=0

	select @TimerRunEnabled = TimerRunEnabled,@DeviceID=Devices.ID
	from DeviceMicroX
	inner join Devices
	on DeviceMicroX.DeviceID =Devices.ID
	where Devices.SerialNumber=@SerialNumber

    --checks if the device was in timer run
	if @TimerRunEnabled = 1
	begin
		--then reset the timer run enable and timer start
		update DeviceMicroX 
		set TimerRunEnabled=0
			,TimerStart=0
		where DeviceID = @DeviceID

		update devices
		set IsRunning=1
		where SerialNumber=@SerialNumber
		--return 1
		set @result = 1
	end
	
	select @result
END

GO
/****** Object:  StoredProcedure [dbo].[spUpdateMongoDBBackup]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUpdateMongoDBBackup]
	@SerialNumber int
AS
BEGIN
	SET NOCOUNT ON;

	declare @Customerid int
	declare @MongoDataBackUp int

	select @CustomerID=customerid
	from devices
	where serialnumber=@SerialNumber

	select @MongoDataBackUp=isnull(MongoDataBackUp,0)
	from customers
	where id=@CustomerID

	if @MongoDataBackUp=0 
	begin
		update customers
		set MongoDataBackUp=1
		where id=@CustomerID
	end
	else
	begin 
		if @MongoDataBackUp=1
		begin
			update customers
			set MongoDataBackUp=2
			where id=@CustomerID
		end
	end
END

GO
/****** Object:  StoredProcedure [dbo].[spUpdateNotificationBatteryEmailSent]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spUpdateNotificationBatteryEmailSent]
	@SerialNumber int, 
	@ContactID int
AS
BEGIN
	SET NOCOUNT ON;

    declare @DeviceID int
	declare @BatteryEmailSent int

	select @DeviceID = id
	from devices
	where serialnumber= @SerialNumber

	select @BatteryEmailSent = BatteryEmailSent
	from DeviceAlarmNotifications
	where DeviceID=@DeviceID
	and ContactID=@ContactID

	if @BatteryEmailSent>0 
	begin
		update DeviceAlarmNotifications
		set BatteryEmailSent = 0
		where DeviceID=@DeviceID
		and ContactID=@ContactID
	end
	else
	begin
		update DeviceAlarmNotifications
		set BatteryEmailSent = DATEDIFF(SECOND,{d '1970-01-01'}, getdate())
		where DeviceID=@DeviceID
		and ContactID=@ContactID
	end
END

GO
/****** Object:  StoredProcedure [dbo].[spUpdateNotificationBatterySMSSent]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUpdateNotificationBatterySMSSent]
	@SerialNumber int, 
	@ContactID int,
	@SMSID nvarchar(255)
AS
BEGIN
	SET NOCOUNT ON;

    declare @DeviceID int
	declare @CustomerID int
	declare @BatterySMSSent int

	select @DeviceID = id
	from devices
	where serialnumber= @SerialNumber

	--get the batterysmssent field and checks if has value greater than 0
	-- then set it to 0
	-- otherwise set the current datetime
	select @BatterySMSSent = BatterySMSSent
	from DeviceAlarmNotifications
	where DeviceID=@DeviceID
	and ContactID=@ContactID

	if @BatterySMSSent>0
	begin 
		update DeviceAlarmNotifications
		set BatterySMSSent = 0
		where DeviceID=@DeviceID
		and ContactID=@ContactID
	end
	else
	begin
		update DeviceAlarmNotifications
		set BatterySMSSent = DATEDIFF(SECOND,{d '1970-01-01'}, getdate())
		where DeviceID=@DeviceID
		and ContactID=@ContactID
	end

	if not exists(select top 1 deviceid from DeviceSMSTrail where DeviceID=@DeviceID and contactID=@ContactID and SMSID=@SMSID)
	begin
		insert into DeviceSMSTrail(DeviceID,ContactID,SMSID,IsBattery,LastUpdated)
		values(@DeviceID,@ContactID,@SMSID,1,GetDate())
	end

	select @CustomerID=CustomerID
	from Contacts
	where id=@ContactID

	update Customers
	set SMSCounter= SMSCounter+1
	where id=@CustomerID
END

GO
/****** Object:  StoredProcedure [dbo].[spUpdateNotificationEmailSent]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUpdateNotificationEmailSent]
	@SerialNumber int, 
	@ContactID int,
	@SensorTypeID int
AS
BEGIN
	SET NOCOUNT ON;

	declare @DeviceID int
	declare @AlarmEmailSent int
	declare @DeviceSensorID int

	select @DeviceID = id
	from devices
	where serialnumber= @SerialNumber

	select @DeviceSensorID = ID
	from DeviceSensors
	where DeviceID=@DeviceID
	and SensorTypeID =@SensorTypeID

	select @AlarmEmailSent=AlarmEmailSent
	from DeviceAlarmNotificationTrail
	where DeviceSensorID=@DeviceSensorID
	and ContactID=@ContactID

	if @AlarmEmailSent>0
	begin
		update DeviceAlarmNotificationTrail
		set AlarmEmailSent = 0
		where ContactID=@ContactID
		and DeviceSensorID=@DeviceSensorID
	end
	else
	begin
		
		insert into DeviceAlarmNotificationTrail(DeviceSensorID,ContactID,AlarmEmailSent,AlarmSMSSent)
		values(@DeviceSensorID,@ContactID,DATEDIFF(SECOND,{d '1970-01-01'}, getdate()),0)
	end

END

GO
/****** Object:  StoredProcedure [dbo].[spUpdateNotificationSMSSent]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUpdateNotificationSMSSent]
	@SerialNumber int, 
	@ContactID int,
	@SMSID nvarchar(255),
	@SensorTypeID int
AS
BEGIN
	SET NOCOUNT ON;

    declare @DeviceID int
	declare @CustomerID int
	declare @AlarmSMSSent int
	declare @DeviceSensorID int

	select @DeviceID = id
	from devices
	where serialnumber= @SerialNumber

	select @DeviceSensorID = ID
	from DeviceSensors
	where DeviceID=@DeviceID
	and SensorTypeID =@SensorTypeID

	--checks if the alarmsmssent is greater than 0 then update to 0
	--else update to current datetime
	select @AlarmSMSSent=AlarmSMSSent
	from DeviceAlarmNotificationTrail
	where ContactID=@ContactID
	and DeviceSensorID=@DeviceSensorID

	if @AlarmSMSSent>0 
	begin
		update DeviceAlarmNotificationTrail
		set AlarmSMSSent = 0
		where ContactID=@ContactID
		and DeviceSensorID=@DeviceSensorID
	end
	else
	begin

		insert into DeviceAlarmNotificationTrail(DeviceSensorID,ContactID,AlarmEmailSent,AlarmSMSSent)
		values(@DeviceSensorID,@ContactID,0,DATEDIFF(SECOND,{d '1970-01-01'}, getdate()))
	end

	if not exists(select top 1 deviceid from DeviceSMSTrail where DeviceID=@DeviceID and contactID=@ContactID and SMSID=@SMSID)
	begin
		insert into DeviceSMSTrail(DeviceID,ContactID,SMSID,DeviceSensorID,LastUpdated)
		values(@DeviceID,@ContactID,@SMSID,@DeviceSensorID,getdate())
	end

	select @CustomerID=CustomerID
	from Contacts
	where id=@ContactID

	update Customers
	set SMSCounter= SMSCounter+1
	where id=@CustomerID
END

GO
/****** Object:  StoredProcedure [dbo].[spUpdatePasswordExpiry]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spUpdatePasswordExpiry]
	@UserID int,
	@PasswordMinLength int,
	@PasswordExpiryByDays int
AS
BEGIN
	SET NOCOUNT ON;

	declare @CustomerID int

	select @CustomerID = customerid
	from users
	where users.id=@UserID

	update customersettings
	set passwordMinLength=@PasswordMinLength,
	passwordExpiryByDays=@PasswordExpiryByDays
	where customerid=@customerid
	
    select passwordMinLength,passwordExpiryByDays
	from customersettings
	where customerid=@customerid
END

GO
/****** Object:  StoredProcedure [dbo].[spUpdateSite]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUpdateSite]
	@SiteID int,
	@ParentID int,
	@Name nvarchar(50)
AS
BEGIN
	SET NOCOUNT ON;

	--checks if the site is the root, then do not allow to change his parent
	if exists(select top 1 id from sites where id=@SiteID and parentid=0)
	begin 
		set @ParentID=0
	end

	--checks if try to change the parent to himself
	if @SiteID=@ParentID
	begin
		select @ParentID=parentid
		from sites
		where id=@SiteID
	end

    update Sites
	set name=@Name,
	parentID=@ParentID
	where id=@SiteID
	

	select id,parentid,name,treeorder
	from sites
	where id=@SiteID

END

GO
/****** Object:  StoredProcedure [dbo].[spUpdateSMSStatus]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spUpdateSMSStatus]
	@SMSID nvarchar(255),
	@SMSStatus nvarchar(255)
AS
BEGIN
	SET NOCOUNT ON;

    update DeviceSMSTrail
	set SMSStatus = @SMSStatus
	,LastUpdated=getdate()
	where SMSID = @SMSID

END

GO
/****** Object:  StoredProcedure [dbo].[spUpdateUser]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUpdateUser]
	@UserID int,
	@UserName nvarchar(50), 
	@Email nvarchar(255),
	@LanguageID int, 
	@defaultDateFormat nvarchar(50)
	
AS
BEGIN
	SET NOCOUNT ON;

	declare @CustomerID int

	select @CustomerID = customerid
	from users
	where users.id=@UserID;

	if not exists(select top 1 users.id from users where users.username=@UserName and users.customerid=@CustomerID)
	begin

		
		update users
		set UserName = @UserName
		,Email=@Email
		,LanguageID = @LanguageID
		,defaultDateFormat=@defaultDateFormat
		where id=@UserID
		and username<>'admin'

		select @@Rowcount;
	end
	else
	begin
		select 0;
	end
END

GO
/****** Object:  StoredProcedure [dbo].[spUpsertAnalyticsSensors]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUpsertAnalyticsSensors]
	@UserID int,
	@DeviceSensorID int,
	@IsActive int =1
AS
BEGIN
	SET NOCOUNT ON;

	IF NOT EXISTS (SELECT top 1 UserID FROM AnalyticsSensors WHERE UserID=@UserID AND DeviceSensorID=@DeviceSensorID)
                
		INSERT INTO AnalyticsSensors (UserID, DeviceSensorID, IsActive) 
		VALUES (@UserID, @DeviceSensorID, @IsActive) 

	ELSE 

		UPDATE AnalyticsSensors 
		SET IsActive=@IsActive 
		WHERE UserID=@UserID 
		AND DeviceSensorID=@DeviceSensorID

END

GO
/****** Object:  StoredProcedure [dbo].[spUpsertContact]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUpsertContact]
	@UserID int,
	@ContactID int,
	@ContactName varchar(50),
	@CountryCode varchar(10),
	@Email varchar(255),
	@MobilePhone varchar(50),
	@SMSResend int,
	@Title varchar(10),
	@WorkingHourMode varchar(50),
	@From int,
	@To int,
	@WorkingSun tinyint,
	@WorkingMon tinyint,
	@WorkingTue tinyint,
	@WorkingWed tinyint,
	@WorkingThu tinyint,
	@WorkingFri tinyint,
	@WorkingSat tinyint
AS
BEGIN
	SET NOCOUNT ON;

	declare @CustomerID int
	declare @WorkingHoursID int

	select @WorkingHoursID=id
	from ContactsWorkHoursMode
	where name=@WorkingHourMode

	select @CustomerID=customerid
	from Users
	where users.id=@UserID

	if @ContactID>0
	begin
		update Contacts
		set name=@ContactName
			,Title=@Title
			,Email=@Email
			,CountryCode=@CountryCode
			,PhoneNumber=@MobilePhone
			,WorkingHoursID=@WorkingHoursID
			,WorkdayStart = @From
			,WorkdayEnd = @To
			,SMSResendInterval = @SMSResend
			,WorkingSun=@WorkingSun
			,WorkingMon=@WorkingMon
			,WorkingTue=@WorkingTue
			,WorkingWed=@WorkingWed
			,WorkingThu=@WorkingThu
			,WorkingFri=@WorkingFri
			,WorkingSat=@WorkingSat
		where Contacts.ID=@ContactID
	end
	else
	begin

		INSERT INTO Contacts
				   ([CustomerID] ,[Name] ,[Title] ,[Email] ,[CountryCode],[PhoneNumber] ,[WorkingHoursID] ,[WorkdayStart] ,[WorkdayEnd]
				   ,[SMSResendInterval] ,[WorkingSun] ,[WorkingMon] ,[WorkingTue] ,[WorkingWed] ,[WorkingThu] ,[WorkingFri] ,[WorkingSat])
		VALUES   (@CustomerID,@ContactName,@Title ,@Email,@CountryCode ,@MobilePhone ,@WorkingHoursID ,@From ,@To
				   ,@SMSResend ,@WorkingSun ,@WorkingMon ,@WorkingTue ,@WorkingWed ,@WorkingThu ,@WorkingFri  ,@WorkingSat)

		set @ContactID = @@IDENTITY

	end

	delete from ContactDistributionGroups
	where contactid=@ContactID

	select @ContactID

END

GO
/****** Object:  StoredProcedure [dbo].[spUpsertContactGroup]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUpsertContactGroup]
	@ContactId int,
	@GroupID int
AS
BEGIN
	SET NOCOUNT ON;

	insert into ContactDistributionGroups(ContactID,GroupID)
	values(@ContactId,@GroupID)

END

GO
/****** Object:  StoredProcedure [dbo].[spUpsertDefinedSensor]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUpsertDefinedSensor]
	@UserID int,
	@DefinedSensorID int,
	@BaseSensorID int,
	@Digits int,
	@FamilyTypeID int,
	@Log1 float,
	@Log2 float,
	@Name varchar(50),
	@SensorUnitID int,
	@Ref1 float,
	@Ref2 float
AS
BEGIN
	SET NOCOUNT ON;

	declare @CustomerID int

	select @CustomerID = customerid
	from users
	where id=@UserID

    if @DefinedSensorID>0
	begin
		
		update DefinedSensors
		set name=@Name
		,decimalPlaces = @Digits
		,log1=@Log1
		,log2=@Log2
		,ref1=@Ref1
		,ref2=@Ref2
		where customerid = @CustomerID 
		and basesensorid=@BaseSensorID 
		and familyTypeID = @FamilyTypeID
		and SensorUnitID =@SensorUnitID

	end
	else
	begin
	

		INSERT INTO [dbo].[DefinedSensors]
			   ([CustomerID]
			   ,[BaseSensorID]
			   ,[FamilyTypeID]
			   ,[SensorUnitID]
			   ,[Name]
			   ,[DecimalPlaces]
			   ,[Log1]
			   ,[Log2]
			   ,[Ref1]
			   ,[Ref2])
		 VALUES
			   (@CustomerID
			   ,@BaseSensorID
			   ,@FamilyTypeID
			   ,@SensorUnitID
			   ,@Name
			   ,@Digits
			   ,@Log1
			   ,@Log2
			   ,@Ref1
			   ,@Ref2)



	end
END

GO
/****** Object:  StoredProcedure [dbo].[spUpsertDevice]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUpsertDevice]
	@SerialNumber int,
	@UserID int,
	@Name nvarchar(50),
	@DeviceTypeName nvarchar(50),
	@SiteID int,
	@StatusID int,
	@IsRunning tinyint,
	@FirmwareVersion float,
	@BuildNumber int,
	@MacAddress nvarchar(50)='',
	@BatteryLevel float,
	@FirmwareRevision nchar(10),
	@PCBAssembly nchar(10),
	@PCBVersion nchar(10),
	@IsResetDownload int=0,
	@AlarmDelay int=0,
	@InCalibrationMode int=0
AS
BEGIN
	
	SET NOCOUNT ON;

	declare @DeviceId int
	declare @DeviceTypeID int
	declare @CustomerID int

	if @BuildNumber<0
		set @BuildNumber=0

	if exists(select top 1 customerid
			  from users
			where id=@UserID)
	begin
		select @CustomerID = customerid
		from users
		where id=@UserID
	end
	else
	begin
		set @CustomerID=0
	end

	select @DeviceTypeID = id
	from DeviceTypes
	where Name= @DeviceTypeName

	if @SiteID<0
	begin
		select @SiteID=isnull(siteid,0)
		from devices
		where serialnumber=@SerialNumber

		if @SiteID<1
		begin
			set @SiteID=1
		end
	end

   if exists(select top 1 id from devices where serialnumber=@SerialNumber)
   begin 
		update devices
		set Name=@Name,
		SiteID=@SiteID,
		StatusID=@StatusID,
		FirmwareVersion=@FirmwareVersion,
		BuildNumber=@BuildNumber,
		BatteryLevel=@BatteryLevel,
		FirmwareRevision=@FirmwareRevision,
		PCBAssembly=@PCBAssembly,
		PCBVersion = @PCBVersion,
		IsRunning=@IsRunning ,
		AlarmDelay= @AlarmDelay,
		InCalibrationMode=@InCalibrationMode,
		DeviceTypeID = @DeviceTypeID
		where serialnumber=@SerialNumber

		select top 1 @DeviceID = id
		from devices 
		where serialnumber=@SerialNumber
   end
   else
   begin

	INSERT INTO Devices
           (SerialNumber
           ,CustomerID
           ,Name
           ,DeviceTypeID
           ,SiteID
           ,StatusID
           ,MacAddress
           ,FirmwareVersion
		   ,BuildNumber
           ,BatteryLevel
           ,FirmwareRevision
		   ,PCBAssembly
		   ,PCBVersion
		   ,IsRunning
		   ,IsInAlarm
		   ,AlarmDelay
		   ,InCalibrationMode
           )
     VALUES
           (
			@SerialNumber,
			@CustomerId,
			@Name,
			@DeviceTypeID,
			@SiteID,
			@StatusID,
			@MacAddress,
			@FirmwareVersion,
			@BuildNumber,
			@BatteryLevel,
			@FirmwareRevision,
			@PCBAssembly,
			@PCBVersion,
			@IsRunning,
			0,
			@AlarmDelay,
			@InCalibrationMode
		   );


		set @DeviceId = @@IDENTITY
	end

	/*if @DeviceId>0
	begin 
		delete from devicesensors
		where deviceid=@DeviceId
	end*/

	if @IsResetDownload>0
	begin
		delete from SensorDownloadHistory
		where serialNumber = @SerialNumber

		update devices
		set LastSetupTime = dbo.fnUNIX_TIMESTAMP(getdate())
		where serialnumber=@SerialNumber
	end
	
	select @DeviceId deviceid
END

GO
/****** Object:  StoredProcedure [dbo].[spUpsertDeviceAlarmData]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUpsertDeviceAlarmData]
	
	@SerialNumber int,
	@SensorTypeID int,
	@SensorUnitName nvarchar(50),
	@AlarmDate int,
	@AlarmValue float
	
AS
BEGIN
	SET NOCOUNT ON;

	declare @DeviceID int
	declare @DeviceSensorID int
	declare @SensorUnitID int

	select @DeviceID = id
	from devices
	where serialnumber=@Serialnumber

	select @SensorUnitID = id
	from SensorUnits
	where name= @SensorUnitName


	select @DeviceSensorID = id
	from DeviceSensors
	where DeviceID=@DeviceID
	and SensorTypeID = @SensorTypeID
	and SensorUnitID = @SensorUnitID

	--update the device isInAlarm
	update devices 
	set IsInAlarm = 1
	where id=@DeviceID

	--then update the alarm notifications

    --an alarm was recorded on the last sample
	if @AlarmDate>0
	begin
		--add alarm notification
		--alarm type id 1 is for sensor
		if exists(select top 1 id from alarmnotifications 
					where deviceid=@DeviceID 
					and alarmtypeid=1 
					and devicesensorid=@DeviceSensorID)
		begin
			update alarmnotifications
			set value=@AlarmValue
			,StartTime=@AlarmDate
			where deviceid=@DeviceID 
			and alarmtypeid=1 
			and devicesensorid=@DeviceSensorID
		end
		else
		begin 
			insert into alarmnotifications(deviceid,alarmtypeid,value,starttime,devicesensorid)
			values(@DeviceID,1,@AlarmValue,@AlarmDate,@DeviceSensorID)
		end
	end

END

GO
/****** Object:  StoredProcedure [dbo].[spUpsertDeviceLogger]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spUpsertDeviceLogger]
	@DeviceID int,
	@Interval int,
	@CelsiusMode int
AS
BEGIN
	
	SET NOCOUNT ON;

    if exists (select top 1 deviceid from devicelogger where deviceid=@deviceid)
	begin 
		update devicelogger
		set interval = @interval,
		CelsiusMode = @CelsiusMode
		where deviceid=@deviceid
	end
	else
	begin
		insert into devicelogger(deviceid,interval,CelsiusMode)
		values(@deviceid,@Interval,@CelsiusMode);
	end

	select @@RowCount

END

GO
/****** Object:  StoredProcedure [dbo].[spUpsertDeviceMicroLite]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUpsertDeviceMicroLite]
	@DeviceID int, 
	@ShowMinMax tinyint,
	@ShowPast24H tinyint,
	@MemorySize int,
	@EnableLEDOnAlarm tinyint,
	@LCDConfig tinyint,
	@StopOnDisconnect tinyint,
	@StopOnKeyPress tinyint,
	@UnitRequiresSetup tinyint,
	@AveragePoints tinyint
AS
BEGIN
	SET NOCOUNT ON;

    if exists(select top 1 deviceid from DeviceMicroLite where deviceid=@DeviceID)
	begin
		update DeviceMicroLite
		set 
		ShowMinMax=@ShowMinMax,
		ShowPast24H=@ShowPast24H,
		MemorySize=@MemorySize,
		EnableLEDOnAlarm=@EnableLEDOnAlarm,
		LCDConfig=@LCDConfig,
		StopOnDisconnect=@StopOnDisconnect,
		StopOnKeyPress=@StopOnKeyPress,
		UnitRequiresSetup=@UnitRequiresSetup,
		AveragePoints=@AveragePoints
		where deviceid=@DeviceID
	end
	else
	begin
		insert into DeviceMicroLite(DeviceID,ShowMinMax,ShowPast24H,MemorySize,EnableLEDOnAlarm,LCDConfig,StopOnDisconnect,StopOnKeyPress,UnitRequiresSetup,AveragePoints)
		values(@DeviceID,@ShowMinMax,@ShowPast24H,@MemorySize,@EnableLEDOnAlarm,@LCDConfig,@StopOnDisconnect,@StopOnKeyPress,@UnitRequiresSetup,@AveragePoints)
	end
    
	select @@rowcount
END

GO
/****** Object:  StoredProcedure [dbo].[spUpsertDeviceMicroLog]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUpsertDeviceMicroLog]
	@DeviceID int,
	@AveragePoints int,
	@DeepSleepMode tinyint,
	@EnableLEDOnAlarm tinyint,
	@LCDConfiguration tinyint,
	@MemorySize int,
	@ShowMinMax tinyint,
	@ShowPast24HMinMax tinyint,
	@StopOnDisconnect tinyint,
	@StopOnKeyPress tinyint
AS
BEGIN
	SET NOCOUNT ON;

	if exists(select top 1 deviceid from DeviceMicroLog where deviceid=@DeviceID)
	begin
		update DeviceMicroLog
		set AveragePoints=@AveragePoints,
		DeepSleepMode=@DeepSleepMode,
		EnableLEDOnAlarm=@EnableLEDOnAlarm,
		LCDConfiguration=@LCDConfiguration,
		MemorySize=@MemorySize,
		ShowMinMax=@ShowMinMax,
		ShowPast24HMinMax=@ShowPast24HMinMax,
		StopOnDisconnect=@StopOnDisconnect,
		StopOnKeyPress=@StopOnKeyPress
		where deviceid=@DeviceID
	end
	else
	begin
		insert into DeviceMicroLog(DeviceID,AveragePoints,DeepSleepMode,EnableLEDOnAlarm,LCDConfiguration,
									MemorySize,ShowMinMax,ShowPast24HMinMax,StopOnDisconnect,StopOnKeyPress)
		values(@DeviceID,@AveragePoints,@DeepSleepMode,@EnableLEDOnAlarm,@LCDConfiguration,@MemorySize,
				@ShowMinMax,@ShowPast24HMinMax,@StopOnDisconnect,@StopOnKeyPress)
	end
    
	select @@rowcount
END

GO
/****** Object:  StoredProcedure [dbo].[spUpsertDeviceMicroX]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spUpsertDeviceMicroX]
	@DeviceID int,
	@CyclicMode tinyint,
	@PushToRun tinyint,
	@TimerRunEnabled tinyint,
	@TimerStart int,
	@BoomerangEnabled tinyint,
	@BoomerangAuthor nvarchar(50),
	@BoomerangCelsiusMode tinyint,
	@BoomerangComment nvarchar(50),
	@BoomerangContacts nvarchar(200),
	@BoomerangDisplayAlarmLevels tinyint,
	@BoomerangUTC nvarchar(10),
	@LoggerUTC int
AS
BEGIN
	SET NOCOUNT ON;

	

    if exists(select top 1 deviceid from devicemicrox where deviceid=@deviceid)
	begin

		if len(@BoomerangUTC)=0
		begin
			select @BoomerangUTC = BoomerangUTC
			from DeviceMicroX
			where deviceid=@deviceid
		end

		if @LoggerUTC<>-99
		begin
			update devicemicrox
			set CyclicMode=@CyclicMode,
			PushToRun=@PushToRun,
			TimerRunEnabled=@TimerRunEnabled,
			TimerStart=@TimerStart,
			BoomerangEnabled=@BoomerangEnabled,
			BoomerangAuthor=@BoomerangAuthor,
			BoomerangCelsiusMode=@BoomerangCelsiusMode,
			BoomerangComment=@BoomerangComment,
			BoomerangContacts=@BoomerangContacts,
			BoomerangDisplayAlarmLevels=@BoomerangDisplayAlarmLevels,
			BoomerangUTC =@BoomerangUTC,
			LoggerUTC = @LoggerUTC
			where deviceid=@deviceid
		end
		else
		begin
			update devicemicrox
			set CyclicMode=@CyclicMode,
			PushToRun=@PushToRun,
			TimerRunEnabled=@TimerRunEnabled,
			TimerStart=@TimerStart,
			BoomerangEnabled=@BoomerangEnabled,
			BoomerangAuthor=@BoomerangAuthor,
			BoomerangCelsiusMode=@BoomerangCelsiusMode,
			BoomerangComment=@BoomerangComment,
			BoomerangContacts=@BoomerangContacts,
			BoomerangDisplayAlarmLevels=@BoomerangDisplayAlarmLevels,
			BoomerangUTC =@BoomerangUTC
			where deviceid=@deviceid
		end
	end
	else
	begin
		if @LoggerUTC<>-99
		begin
			insert into devicemicrox(deviceid,CyclicMode,PushToRun,TimerRunEnabled,TimerStart,BoomerangEnabled,
									BoomerangAuthor,BoomerangCelsiusMode,BoomerangComment,BoomerangContacts,
									BoomerangDisplayAlarmLevels,BoomerangUTC,LoggerUTC)
			values(@deviceid,@CyclicMode,@PushToRun,@TimerRunEnabled,@TimerStart,@BoomerangEnabled,@BoomerangAuthor,
					@BoomerangCelsiusMode,@BoomerangComment,@BoomerangContacts,@BoomerangDisplayAlarmLevels,@BoomerangUTC,@LoggerUTC)
		end
		else
		begin
			insert into devicemicrox(deviceid,CyclicMode,PushToRun,TimerRunEnabled,TimerStart,BoomerangEnabled,
									BoomerangAuthor,BoomerangCelsiusMode,BoomerangComment,BoomerangContacts,
									BoomerangDisplayAlarmLevels,BoomerangUTC,LoggerUTC)
			values(@deviceid,@CyclicMode,@PushToRun,@TimerRunEnabled,@TimerStart,@BoomerangEnabled,@BoomerangAuthor,
					@BoomerangCelsiusMode,@BoomerangComment,@BoomerangContacts,@BoomerangDisplayAlarmLevels,@BoomerangUTC,0)
		end
	end

	select @@RowCount
END

GO
/****** Object:  StoredProcedure [dbo].[spUpsertDeviceSensors]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUpsertDeviceSensors]
	
	@DeviceID int,
	@SensorTypeID int,
	@SensorUnitName nvarchar(50),
	@IsEnabled tinyint,
	@IsAlarmEnabled int,
	@LowValue float,
	@HighValue float,
	@MinValue float,
	@MaxValue float,
	@SensorAliasName nvarchar(50),
	@SensorIndex int,
	
	@IsDefinedSensor int=0,
	@DefineSensorName nvarchar(50)='',
	@CustomUnitName nvarchar(50)='',
	@DecimalPlaces tinyint=0,
	@Gain float=0,
	@Offset float=0,
	@Log1 float=0,
	@Log2 float=0,
	@Ref1 float=0,
	@Ref2 float=0,
	@CalibrationExpiry int=0,
	@PreLowValue float=0,
	@PreHighValue float=0,
	@PreDelayValue float=0,
	@DelayValue float=0,
	@BuzzDuration int=0
AS
BEGIN
	SET NOCOUNT ON;

	declare @DeviceSensorID int
	declare @SensorUnitID int
	declare @FamilyTypeID int
	
	select @SensorUnitID = id
	from SensorUnits
	where name= @SensorUnitName

	select @FamilyTypeID=FamilyTypeID
	from DeviceTypes
	inner join Devices
	on Devices.DeviceTypeID = DeviceTypes.ID
	where Devices.ID = @DeviceID

	
    if exists (select top 1 id 
				from DeviceSensors 
				where DeviceID=@DeviceID
				and SensorTypeID = @SensorTypeID
				-- and SensorUnitID = @SensorUnitID
				)
	begin
		update DeviceSensors
		set SensorTypeID=@SensorTypeID,
		SensorUnitID = @SensorUnitID,
		IsEnabled = @IsEnabled,
		IsAlarmEnabled = @IsAlarmEnabled,
		CalibrationExpiry = @CalibrationExpiry,
		MinValue=@MinValue,
		MaxValue=@MaxValue,
		SensorName = @SensorAliasName,
		SensorIndex = @SensorIndex
		where DeviceID=@DeviceID
		and SensorTypeID = @SensorTypeID
		--and SensorUnitID = @SensorUnitID

		select @DeviceSensorID = id
		from DeviceSensors
		where DeviceID=@DeviceID
		and SensorTypeID = @SensorTypeID
		and SensorUnitID = @SensorUnitID

		update DeviceAlarmNotificationTrail
		set AlarmEmailSent=0
		,AlarmSMSSent=0
		where DeviceSensorID = @DeviceSensorID
	end
	else
	begin
		insert into DeviceSensors(DeviceID,SensorTypeID,SensorUnitID,IsEnabled,IsAlarmEnabled,CalibrationExpiry,MinValue,MaxValue,SensorName,SensorIndex)
		values(@DeviceID,@SensorTypeID,@SensorUnitID,@IsEnabled,@IsAlarmEnabled,@CalibrationExpiry,@MinValue,@MaxValue,@SensorAliasName,@SensorIndex);

		set @DeviceSensorID = @@IDENTITY;

	end

	/*if @IsDefinedSensor =1
	begin
		
		if exists (select top 1 id from DefinedSensors where DeviceSensors.ID=@DeviceSensorID)
		begin
			update DefinedSensors
			set Name=@CustomUnitName,
			Gain=@Gain,
			Offset=@Offset,
			Log1=@Log1,
			Log2=@Log2,
			Ref1=@Ref1,
			Ref2=@Ref2
			where id=@DefineSensorID
		end
		else
		begin
			insert into DefinedSensors(FamilyTypeID,BaseSensorID,SensorUnitID,name,DecimalPlaces,Gain,Offset,Log1,Log2,Ref1,Ref2)
			values(@FamilyTypeID,@BaseSensorID,@SensorUnitID,@DefineSensorName,@DecimalPlaces,@Gain,@Offset,@Log1,@Log2,@Ref1,@Ref2)
		end
	end*/
	
	if @IsAlarmEnabled =1 
	begin
		if exists (select top 1 id from SensorAlarm where DeviceSensorID = @DeviceSensorID)
		begin
			update SensorAlarm
			set PreLowValue = @PreLowValue,
			LowValue = @LowValue,
			PreHighValue=@PreHighValue,
			HighValue=@HighValue,
			PreDelayValue=@PreDelayValue,
			DelayValue=@DelayValue,
			BuzzDuration=@BuzzDuration
			where DeviceSensorID=@DeviceSensorID;

		end
		else
		begin
			insert into SensorAlarm(DeviceSensorID,PreLowValue,LowValue,PreHighValue,HighValue,PreDelayValue,DelayValue,BuzzDuration)
			values(@DeviceSensorID,@PreLowValue,@LowValue,@PreHighValue,@HighValue,@PreDelayValue,@DelayValue,@BuzzDuration);
		end
	end
	

	select 1

END

GO
/****** Object:  StoredProcedure [dbo].[spUpsertGroup]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUpsertGroup]
	@UserID int,
	@GroupID int,
	@ContactID int,
	@IsUser tinyint
	--@GroupContacts varchar(1000)
AS
BEGIN
	SET NOCOUNT ON;
	declare @CustomerID int

	select @CustomerID = customerid
	from users
	where users.id=@UserID

    /*create table #tempTable(contactid int)
	
	insert into #tempTable
	select * from dbo.FnSplit(@GroupContacts,',')*/

	
	insert into ContactDistributionGroups(GroupID,ContactID,IsUser)
	values(@GroupID,@ContactID,@IsUser)
	/*select @GroupID,contactid
	from #tempTable*/


END

GO
/****** Object:  StoredProcedure [dbo].[spUpsertPicoLite]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE  [dbo].[spUpsertPicoLite]
	@DeviceID int,
	@LEDConfig int,
	@RunDelay int =0,
	@RunTime int=0,
	@SetupTime int=0,
	@StopOnKeyPress int=0
AS
BEGIN
	SET NOCOUNT ON;

    if exists(select top 1 deviceid from DevicePicoLite where DeviceID=@DeviceID)
	begin
		update DevicePicoLite
		set LEDConfig=@LEDConfig,
		RunDelay =@RunDelay,
		RunTime=@RunTime,
		SetupTime=@SetupTime,
		StopOnKeyPress=@StopOnKeyPress
		where deviceid=@DeviceID
	end
	else
	begin
		insert into DevicePicoLite(DeviceID,LEDConfig,RunDelay,RunTime,SetupTime,StopOnKeyPress)
		values(@DeviceID,@LEDConfig,@RunDelay,@RunTime,@SetupTime,@StopOnKeyPress)
	end

	select @@rowcount 

END

GO
/****** Object:  StoredProcedure [dbo].[spUpsertReportProfile]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUpsertReportProfile]
	@ReportID int = 0,
	@UserID int,
	@ProfileName varchar(32),
	@Description varchar(125),
	@IsActive int,
	@PageSize varchar(10),
	@DateFormat varchar(15),
	@TemperatureUnit int,
	@TimeZoneOffset int,
	@PeriodTypeID int,
	@PeriodTime datetime = '0:00',
	@PeriodDay int = 0,
	@PeriodMonth int = 0,
	@PeriodStart int = 0,
	@PeriodEnd int =0,
	@EmailAsLink int = 0,
	@EmailAsPDF int = 0,
	@ReportHeaderLink nvarchar(1000)
AS
BEGIN
	SET NOCOUNT ON;

	if @ReportID>0
	begin
		update ReportTemplates
		set Description=@Description,
		IsActive=@IsActive,
		PageSize=@PageSize,
		ReportDateFormat=@DateFormat,
		TemperatureUnit=@TemperatureUnit,
		TimeZoneOffset=@TimeZoneOffset,
		PeriodTypeID=@PeriodTypeID,
		PeriodTime=@PeriodTime,
		PeriodDay=@PeriodDay,
		PeriodMonth=@PeriodMonth,
		PeriodStart=@PeriodStart,
		PeriodEnd=@PeriodEnd,
		EmailAsLink=@EmailAsLink,
		EmailAsPDF=@EmailAsPDF,
		ReportHeaderLink=@ReportHeaderLink
		where Id= @ReportID

		select @ReportID

	end
	else
	begin
		INSERT INTO [dbo].[ReportTemplates]
           ([UserID]
           ,[ProfileName]
           ,[Description]
           ,[IsActive]
           ,[PageSize]
           ,[ReportDateFormat]
           ,[TemperatureUnit]
           ,[TimeZoneOffset]
           ,[PeriodTypeID]
           ,[PeriodTime]
           ,[PeriodDay]
		   ,[PeriodMonth]
           ,[PeriodStart]
           ,[PeriodEnd]
           ,[EmailAsLink]
           ,[EmailAsPDF]
		   ,[ReportHeaderLink])
     VALUES
           (@UserID
           ,@ProfileName
           ,@Description
           ,@IsActive
           ,@PageSize
           ,@DateFormat
           ,@TemperatureUnit
           ,@TimeZoneOffset
           ,@PeriodTypeID
           ,@PeriodTime
           ,@PeriodDay
		   ,@PeriodMonth
           ,@PeriodStart
           ,@PeriodEnd
           ,@EmailAsLink
           ,@EmailAsPDF
		   ,@ReportHeaderLink)

		   select @@IDENTITY

	end
	
END

GO
/****** Object:  StoredProcedure [dbo].[spUpsertSensorAlarmNotification]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUpsertSensorAlarmNotification]

	@DeviceSensorID int,
	@ContactID int,
	@ReceiveEmail int,
	@ReceiveSMS int
AS
BEGIN
	SET NOCOUNT ON;

	if exists (select top 1 devicesensorid from DeviceAlarmNotifications where DeviceSensorID = @DeviceSensorID 
					and ContactID=@ContactID)
	begin
		update DeviceAlarmNotifications
		set ReceiveEmail=@ReceiveEmail
		,ReceiveSMS=@ReceiveSMS
		where DeviceSensorID = @DeviceSensorID 
		and ContactID=@ContactID
	end
	else 
	begin
		insert into DeviceAlarmNotifications(DeviceSensorID,ContactID, ReceiveEmail,ReceiveSMS)
		values(@DeviceSensorID,@ContactID,@ReceiveEmail,@ReceiveSMS)
	end
   
END

GO
/****** Object:  StoredProcedure [dbo].[spUserGuidIsValid]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spUserGuidIsValid]
	@GUID varchar(200)
AS
BEGIN
	SET NOCOUNT ON;
	declare @PasswordResetExpiry datetime
    declare @ExpiryValid int
	declare @guidConverted uniqueidentifier

	SELECT  @guidConverted= CAST(
        SUBSTRING(@GUID, 1, 8) + '-' + SUBSTRING(@GUID, 9, 4) + '-' + SUBSTRING(@GUID, 13, 4) + '-' +
        SUBSTRING(@GUID, 17, 4) + '-' + SUBSTRING(@GUID, 21, 12)
        AS UNIQUEIDENTIFIER)

	select @PasswordResetExpiry=PasswordResetExpiry
	from users
	where PasswordResetGUID=@guidConverted

	set @ExpiryValid = DateDiff(day,getdate(),@PasswordResetExpiry)

	if exists(select top 1 users.ID from users where users.PasswordResetGUID=@guidConverted) 
		and @ExpiryValid>0
	begin
		select 1
	end
	else
	begin
		select 0
	end

END

GO
/****** Object:  StoredProcedure [dbo].[spValidateLogin]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spValidateLogin]
	@UserName NVARCHAR(50),
	@Password NVARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @UserID int
	declare @concurrentUsersLimit int
	declare @customerID int
	declare @MaxStorageAllowed bigint
	declare @DataStorageExpiry int

	SELECT @UserID=ID, @customerID=CustomerID FROM Users WHERE Username=@UserName AND UserPassword=Upper(@Password)
	IF @UserID IS NULL
	BEGIN
		SELECT 0
		RETURN 0
	END
	ELSE
	BEGIN
		/*	
			Create a new API_KEY for the session and store it for further verifications 
			Utilize PasswordResetGUID field for the task (we do not modify the table definition)
		*/
		UPDATE Users SET PasswordResetGUID=NEWID() WHERE ID=@UserID

		select @concurrentUsersLimit = ConcurrentUsersLimit,@MaxStorageAllowed=MaxStorageAllowed,@DataStorageExpiry=DataStorageExpiry
		from CustomerSettings
		where CustomerID = @customerID

		/*	Retrieve User ID, Full Name, User Role, API_KEY */ 
		SELECT Users.ID UserID, Users.FirstName, Users.LastName , PasswordResetGUID AS API_KEY
		,@concurrentUsersLimit concurrentUsersLimit ,Devices.SerialNumber,Devices.SiteID
		,(select IsCFREnabled from CustomerSettings where CustomerSettings.CustomerID =Users.CustomerID ) isCfr
		,(users.roleid - 1) roleid,@customerID customerid,@MaxStorageAllowed maxStorageAllowed,@DataStorageExpiry DataStorageExpiry
		FROM Users 
		/*inner join PermissionUserSiteDeviceGroup
		on PermissionUserSiteDeviceGroup.UserID = Users.ID*/
		left join Devices
		on Devices.CustomerID = Users.CustomerID
		WHERE Users.ID=@UserID
		and isnull(Users.IsActive,0)=1
		and isnull(Users.deleted,0)=0
		RETURN @UserID
	END
END

GO
/****** Object:  StoredProcedure [dbo].[spValidateResetPassword]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spValidateResetPassword]
	@CustomerID INT,
	@Email NVARCHAR(255),
	@SequreQuestionID INT,
	@SecureAnswer NVARCHAR(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @UserID INT;
	
	SET @UserID = 0;

	--checks if the email is exists
	IF EXISTS (SELECT TOP 1 dbo.Users.ID FROM dbo.Users WHERE dbo.Users.CustomerID=@CustomerID AND dbo.Users.Email=@Email)
	BEGIN
		SELECT @UserID = dbo.Users.ID 
		FROM dbo.Users 
		WHERE dbo.Users.CustomerID=@CustomerID 
		AND dbo.Users.Email=@Email;
		--checks if the answer is valid according to what was saved in database
		IF EXISTS(SELECT TOP 1 dbo.UserSecureQuestions.UserID FROM dbo.UserSecureQuestions WHERE UserID=@UserID AND QuestionID = @SequreQuestionID AND Answer=@SecureAnswer)
		begin
			--create guid for reset password flag
			UPDATE dbo.Users
			SET PasswordResetGUID = NEWID()
			WHERE dbo.Users.ID = @UserID;
			
		END
	END 

	--return the guid
	--(the email will be sent on server side with the guid as parameter)
	SELECT PasswordResetGUID 
	FROM dbo.Users
	WHERE dbo.Users.ID = @UserID;

	SET NOCOUNT OFF;
END
    

GO
/****** Object:  StoredProcedure [dbo].[spWipeSensorLogsData]    Script Date: 09/09/2015 08:25:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spWipeSensorLogsData]
	
AS
BEGIN
	SET NOCOUNT ON;

    delete from sensorlogs

	select @@RowCount
END

GO
