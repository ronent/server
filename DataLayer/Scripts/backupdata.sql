-- MySQL dump 10.13  Distrib 5.6.24, for Win64 (x86_64)
--
-- Host: localhost    Database: fourtec
-- ------------------------------------------------------
-- Server version	5.6.26-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `alarmcontacts`
--

LOCK TABLES `alarmcontacts` WRITE;
/*!40000 ALTER TABLE `alarmcontacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `alarmcontacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `alarmnotifications`
--

LOCK TABLES `alarmnotifications` WRITE;
/*!40000 ALTER TABLE `alarmnotifications` DISABLE KEYS */;
INSERT INTO `alarmnotifications` VALUES (4,1150,1,65.671875,1432215750,NULL,NULL,4295,0,1),(5,1148,1,60.953125,1432827547,NULL,NULL,4285,0,1),(6,2148,1,81.601563,1433785980,1434282267,NULL,5290,0,1),(7,1144,1,28.914063,1435827070,NULL,NULL,4278,0,1),(8,2147,1,90.460938,1436095986,1436096007,NULL,5284,0,1),(9,1147,1,26.621094,1438780753,1437484478,NULL,4283,0,0),(10,2151,1,77.2390625,1441815749,NULL,NULL,NULL,0,0),(11,2151,1,77.2390625,1441815749,NULL,NULL,NULL,0,0),(12,2151,1,75.83984375,1441899663,NULL,NULL,5297,0,0);
/*!40000 ALTER TABLE `alarmnotifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `alarmtypes`
--

LOCK TABLES `alarmtypes` WRITE;
/*!40000 ALTER TABLE `alarmtypes` DISABLE KEYS */;
INSERT INTO `alarmtypes` VALUES (1,'Sensor'),(2,'Battery');
/*!40000 ALTER TABLE `alarmtypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `analyticssensors`
--

LOCK TABLES `analyticssensors` WRITE;
/*!40000 ALTER TABLE `analyticssensors` DISABLE KEYS */;
INSERT INTO `analyticssensors` VALUES (1,4259,1,NULL,NULL,NULL,NULL,NULL),(1,4260,1,NULL,NULL,NULL,NULL,NULL),(1,4261,0,NULL,NULL,NULL,NULL,NULL),(1,4262,0,NULL,NULL,NULL,NULL,NULL),(1,4263,0,NULL,NULL,NULL,NULL,NULL),(1,4264,0,NULL,NULL,NULL,NULL,NULL),(1,4265,0,NULL,NULL,NULL,NULL,NULL),(1,4283,0,NULL,NULL,83.14472,33,12),(1,5283,1,NULL,NULL,NULL,NULL,NULL),(1,5289,0,NULL,NULL,83.14472,33,12),(1,5290,0,NULL,NULL,NULL,NULL,NULL),(36,5280,1,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `analyticssensors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `audittrail`
--

LOCK TABLES `audittrail` WRITE;
/*!40000 ALTER TABLE `audittrail` DISABLE KEYS */;
INSERT INTO `audittrail` VALUES (1,1,28,'2014-09-21 10:15:35.047000',1137,''),(2,1,28,'2014-09-21 13:49:44.637000',1139,''),(3,1,28,'2014-09-21 13:50:07.260000',1139,''),(4,1,28,'2014-12-09 07:37:05.093000',1142,''),(5,1,28,'2014-12-09 07:43:33.543000',1144,''),(6,1,28,'2014-12-09 13:51:10.703000',1142,''),(7,1,28,'2014-12-09 13:52:04.957000',1144,''),(8,1,28,'2014-12-09 13:55:57.793000',1142,''),(9,1,28,'2014-12-14 12:54:10.103000',1142,''),(10,1,28,'2014-12-14 12:54:12.917000',1142,''),(11,1,28,'2014-12-14 13:02:10.640000',1142,''),(12,1,28,'2015-04-15 13:27:18.410000',1146,''),(13,1,28,'2015-04-19 07:27:41.027000',1143,''),(14,1,28,'2015-04-19 07:28:51.457000',1143,''),(15,1,28,'2015-04-19 07:49:20.930000',1146,''),(16,1,28,'2015-04-19 11:16:32.180000',1148,''),(17,1,28,'2015-04-19 11:17:02.693000',1148,''),(18,1,28,'2015-04-19 11:19:31.290000',1148,''),(19,1,28,'2015-04-19 11:19:48.993000',1148,''),(20,1,17,'2015-04-19 12:05:20.980000',NULL,''),(21,1,17,'2015-04-19 12:05:40.130000',NULL,''),(22,1,17,'2015-04-19 12:06:33.757000',NULL,''),(23,1,17,'2015-04-19 12:06:46.463000',NULL,''),(24,1,17,'2015-04-19 12:08:23.560000',NULL,''),(25,1,17,'2015-04-19 12:08:44.140000',NULL,''),(26,1,28,'2015-04-20 06:52:22.367000',1148,''),(27,1,28,'2015-04-20 06:53:00.667000',1148,''),(28,1,28,'2015-04-20 07:27:31.667000',1150,''),(1012,1,16,'2015-04-30 13:37:50.487000',NULL,''),(1013,1,16,'2015-04-30 14:07:23.077000',NULL,''),(1014,1,16,'2015-04-30 14:08:23.100000',NULL,''),(1015,1,16,'2015-04-30 14:11:58.223000',NULL,''),(1016,1,16,'2015-04-30 14:15:06.007000',NULL,''),(1017,1,16,'2015-04-30 15:14:10.963000',NULL,''),(1018,1,16,'2015-04-30 15:14:23.110000',NULL,''),(1019,1,16,'2015-05-03 14:23:49.093000',NULL,''),(1020,1,28,'2015-06-08 16:06:03.060000',2148,''),(1021,1,28,'2015-06-08 16:06:16.507000',2148,''),(1022,1,28,'2015-06-11 19:04:26.777000',2148,''),(1023,1,28,'2015-06-17 10:01:30.900000',1150,''),(1024,1,28,'2015-06-17 10:02:24.233000',1150,''),(1025,1,28,'2015-06-28 16:30:10.720000',2147,''),(1026,1,28,'2015-06-28 16:30:51.467000',2147,''),(1027,1,28,'2015-06-30 10:22:55.500000',1144,''),(1028,1,28,'2015-07-02 08:30:27.497000',1144,''),(1029,1,28,'2015-07-05 11:32:57.203000',2147,''),(1030,1,28,'2015-07-07 17:14:15.623000',1147,''),(1031,1,28,'2015-07-07 17:20:28.923000',1147,''),(1032,1,28,'2015-07-07 17:23:01.717000',1147,''),(1033,1,28,'2015-07-08 14:52:20.390000',1147,''),(1034,1,28,'2015-07-08 15:25:47.840000',1147,''),(1035,1,31,'2015-07-08 15:43:54.317000',1147,''),(1036,1,31,'2015-07-08 15:44:26.680000',1147,''),(1037,1,34,'2015-07-13 11:13:14.500000',1147,''),(1038,1,31,'2015-07-13 11:34:13.957000',1147,''),(1039,1,31,'2015-07-13 14:54:09.823000',1147,''),(1040,1,31,'2015-07-15 12:00:19.390000',1147,''),(1041,1,31,'2015-07-15 12:01:47.860000',1147,''),(1042,1,31,'2015-07-15 12:46:55.913000',1147,''),(1043,1,31,'2015-07-15 13:25:03.360000',1147,''),(1044,1,31,'2015-07-15 13:38:44.013000',1147,''),(1045,1,28,'2015-07-16 16:31:49.930000',1147,''),(1046,1,28,'2015-07-16 16:32:16.497000',1147,''),(1047,1,28,'2015-07-16 16:32:27.920000',1147,''),(1048,1,28,'2015-07-16 16:32:36.707000',1147,''),(1049,1,28,'2015-07-16 16:40:45.410000',1147,''),(1050,1,28,'2015-07-19 13:28:45.033000',1147,''),(1051,1,28,'2015-07-19 13:34:15.150000',1147,''),(1052,1,31,'2015-07-21 09:52:16.437000',1147,''),(1053,1,31,'2015-07-21 11:31:15.127000',1147,''),(1054,1,28,'2015-07-21 13:14:38.103000',1147,''),(1055,1,31,'2015-07-26 06:47:19.220000',1147,''),(1056,1,16,'2015-07-26 12:45:10.967000',NULL,''),(1057,1,28,'2015-08-04 14:00:46.243000',2147,''),(1058,1,16,'2015-08-11 07:26:58.183000',NULL,''),(1059,1,28,'2015-08-16 07:16:29.130000',2147,''),(1060,1,28,'2015-08-16 07:16:57.637000',2147,''),(1061,1,28,'2015-08-16 08:19:35.310000',2147,''),(1062,1,28,'2015-08-16 08:46:30.820000',2147,''),(1063,1,28,'2015-08-16 08:46:44.610000',2147,''),(1064,1,28,'2015-08-17 05:05:50.493000',1147,''),(1065,1,28,'2015-08-17 05:06:25.207000',1147,''),(1066,1,28,'2015-08-17 05:07:33.577000',2152,''),(1067,1,28,'2015-08-17 05:09:06.223000',2152,''),(1068,1,28,'2015-08-17 05:09:18.080000',2152,''),(1069,1,19,'2015-09-10 13:42:43.000000',NULL,'');
/*!40000 ALTER TABLE `audittrail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `audittrailarchive`
--

LOCK TABLES `audittrailarchive` WRITE;
/*!40000 ALTER TABLE `audittrailarchive` DISABLE KEYS */;
/*!40000 ALTER TABLE `audittrailarchive` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `basesenorsfamily`
--

LOCK TABLES `basesenorsfamily` WRITE;
/*!40000 ALTER TABLE `basesenorsfamily` DISABLE KEYS */;
INSERT INTO `basesenorsfamily` VALUES (1,1),(1,2),(2,1),(2,2);
/*!40000 ALTER TABLE `basesenorsfamily` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `basesensors`
--

LOCK TABLES `basesensors` WRITE;
/*!40000 ALTER TABLE `basesensors` DISABLE KEYS */;
INSERT INTO `basesensors` VALUES (1,'Current 4-20 mA'),(2,'Voltage 0-10 V'),(3,'Voltage 0-1 V'),(4,'Voltage 0-50 mV'),(5,'Pulse');
/*!40000 ALTER TABLE `basesensors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `basesensorunits`
--

LOCK TABLES `basesensorunits` WRITE;
/*!40000 ALTER TABLE `basesensorunits` DISABLE KEYS */;
INSERT INTO `basesensorunits` VALUES (1,2,1),(1,3,1),(1,5,1),(1,6,1),(1,7,1),(1,8,1),(1,9,1),(2,2,2),(2,3,2),(2,5,2),(2,6,2),(2,7,2),(2,8,2),(2,9,2);
/*!40000 ALTER TABLE `basesensorunits` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Lab'),(2,'Track'),(3,'Pharm');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `contactdistributiongroups`
--

LOCK TABLES `contactdistributiongroups` WRITE;
/*!40000 ALTER TABLE `contactdistributiongroups` DISABLE KEYS */;
INSERT INTO `contactdistributiongroups` VALUES (1,1,0),(1,2,0),(1,4,0);
/*!40000 ALTER TABLE `contactdistributiongroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `contacts`
--

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
INSERT INTO `contacts` VALUES (1,3,'Ronen','Mr','ronentz@gmail.com','972','8231144',NULL,80000,170000,NULL,NULL,3,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,3,'Guy','Mr','guyk@fourtec.com','972','7943215',NULL,80000,170000,NULL,NULL,3,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,3,'contact 1','mr','ronentz1@gmail.com','ZM','0528231144',1,9,17,NULL,NULL,3,NULL,0,1,1,1,1,1,0,0);
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `contactsworkhoursmode`
--

LOCK TABLES `contactsworkhoursmode` WRITE;
/*!40000 ALTER TABLE `contactsworkhoursmode` DISABLE KEYS */;
INSERT INTO `contactsworkhoursmode` VALUES (1,'Midnight - Midnight'),(2,'09:00 - 17:00'),(3,'Weekends only'),(4,'CUSTOM');
/*!40000 ALTER TABLE `contactsworkhoursmode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` VALUES (3,'Fourtec','ronent@fourtec.com','Rosh HaAyin',NULL,NULL,NULL,NULL,'2015-09-10 09:37:18',0,5,'2015-09-10 09:42:17.000000',NULL,NULL);
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `customersettings`
--

LOCK TABLES `customersettings` WRITE;
/*!40000 ALTER TABLE `customersettings` DISABLE KEYS */;
INSERT INTO `customersettings` VALUES (2,3,1,1,0,10000000,90,1597651200,30,5,NULL,10,90,1436313600,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `customersettings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `definedsensors`
--

LOCK TABLES `definedsensors` WRITE;
/*!40000 ALTER TABLE `definedsensors` DISABLE KEYS */;
INSERT INTO `definedsensors` VALUES (1,3,2,1,'2','x10',2,0,0,1,10,10,100);
/*!40000 ALTER TABLE `definedsensors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `devicealarmnotifications`
--

LOCK TABLES `devicealarmnotifications` WRITE;
/*!40000 ALTER TABLE `devicealarmnotifications` DISABLE KEYS */;
INSERT INTO `devicealarmnotifications` VALUES (4267,1,1,0,0,0,NULL,NULL),(4268,1,1,0,0,0,NULL,NULL);
/*!40000 ALTER TABLE `devicealarmnotifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `devicealarmnotificationtrail`
--

LOCK TABLES `devicealarmnotificationtrail` WRITE;
/*!40000 ALTER TABLE `devicealarmnotificationtrail` DISABLE KEYS */;
/*!40000 ALTER TABLE `devicealarmnotificationtrail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `deviceautosetup`
--

LOCK TABLES `deviceautosetup` WRITE;
/*!40000 ALTER TABLE `deviceautosetup` DISABLE KEYS */;
INSERT INTO `deviceautosetup` VALUES (1,3,1,'',0,0,1),(2,3,2,'',0,0,1.1),(3,3,3,'',0,0,2.5),(4,3,4,'',0,0,2.5),(5,3,5,'',0,0,1.6),(6,3,6,'',0,0,1.6),(7,3,7,'',0,0,1.6),(8,3,9,'',0,0,1.6),(9,3,10,'',0,0,1.6),(10,3,11,'',0,0,1.6),(11,3,12,'',0,0,1.11),(12,3,13,'',0,0,1.26);
/*!40000 ALTER TABLE `deviceautosetup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `devicefamilytypes`
--

LOCK TABLES `devicefamilytypes` WRITE;
/*!40000 ALTER TABLE `devicefamilytypes` DISABLE KEYS */;
INSERT INTO `devicefamilytypes` VALUES (1,'MicroLite',1),(2,'MicroLogProII',1),(3,'DaqLink',1),(4,'DataNet',0),(5,'PicoLite',0),(6,'SiMi',0);
/*!40000 ALTER TABLE `devicefamilytypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `devicegroups`
--

LOCK TABLES `devicegroups` WRITE;
/*!40000 ALTER TABLE `devicegroups` DISABLE KEYS */;
INSERT INTO `devicegroups` VALUES (1,'Lan-Rcv'),(2,'PicoLite'),(3,'MicroLite'),(4,'MicroLogPro2'),(5,'DaqLinq'),(6,'SiMi');
/*!40000 ALTER TABLE `devicegroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `devicelogger`
--

LOCK TABLES `devicelogger` WRITE;
/*!40000 ALTER TABLE `devicelogger` DISABLE KEYS */;
INSERT INTO `devicelogger` VALUES (1136,10,0),(1137,10,1),(1138,60,1),(1139,5,1),(1140,60,1),(1141,10,1),(1142,9,1),(1143,3600,1),(1144,4,1),(1145,60,1),(1146,2,1),(1147,1,1),(1148,20,1),(1149,60,1),(1150,5,1),(2146,60,1),(2147,5,1),(2148,10,1),(2149,420,1),(2150,60,1),(2151,10,2),(2152,60,1);
/*!40000 ALTER TABLE `devicelogger` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `devicemicrolite`
--

LOCK TABLES `devicemicrolite` WRITE;
/*!40000 ALTER TABLE `devicemicrolite` DISABLE KEYS */;
INSERT INTO `devicemicrolite` VALUES (1139,0,1,32000,1,4,1,0,1,1),(1141,0,0,32000,0,2,0,0,1,0),(1144,0,0,32000,0,2,0,0,1,4),(1146,0,0,32000,0,1,0,1,0,0),(1147,0,1,8000,1,1,0,0,0,2),(2146,0,0,32000,0,1,1,0,0,1),(2150,1,0,8000,0,1,0,0,1,0),(2151,1,0,32000,0,1,0,0,0,0);
/*!40000 ALTER TABLE `devicemicrolite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `devicemicrolog`
--

LOCK TABLES `devicemicrolog` WRITE;
/*!40000 ALTER TABLE `devicemicrolog` DISABLE KEYS */;
INSERT INTO `devicemicrolog` VALUES (1046,0,1,0,6,52000,0,0,0,1,NULL),(1047,0,1,1,1,32000,1,0,0,0,NULL),(1048,0,1,0,1,52000,0,0,0,0,NULL),(1062,0,1,0,1,32000,0,0,0,1,NULL),(1063,0,1,1,1,32000,0,0,0,0,NULL),(1064,0,1,0,1,32000,0,0,0,0,NULL),(1065,7,1,0,2,32000,1,0,0,1,NULL),(1068,0,1,0,6,52000,0,0,0,0,NULL),(1074,0,1,0,1,52000,0,0,0,0,NULL),(1075,0,0,1,1,52000,0,0,0,1,NULL),(1079,2,1,1,1,32000,1,0,0,1,NULL),(1080,7,0,0,6,52000,0,0,0,1,NULL),(1082,4,1,0,2,32000,0,0,0,0,NULL),(1083,7,1,1,1,32000,1,1,0,0,NULL),(1088,0,1,0,2,52000,0,0,0,1,NULL),(1094,0,0,0,1,52000,0,0,0,0,NULL),(1102,0,1,0,1,32000,0,0,0,1,NULL),(1103,0,1,0,1,32000,0,0,0,0,NULL),(1104,2,1,1,1,32000,0,1,0,1,NULL),(1105,0,1,0,1,52000,0,0,0,0,NULL),(1111,0,1,0,6,32000,0,1,0,0,NULL),(1112,0,1,1,2,32000,0,0,0,0,NULL),(1117,0,1,0,1,32000,1,1,0,1,NULL),(1118,0,1,0,6,52000,0,0,0,1,NULL),(1119,3,1,0,1,32000,0,0,0,0,NULL),(1122,0,1,0,1,32000,0,0,0,0,NULL),(1123,3,1,1,1,32000,0,0,0,0,NULL),(1124,0,1,0,1,52000,0,0,0,0,NULL),(1125,0,1,0,1,32000,0,0,0,1,NULL),(1126,0,1,0,6,52000,0,0,0,1,NULL),(1127,0,1,0,6,52000,0,0,0,1,NULL),(1128,0,1,0,1,32000,0,0,0,1,NULL),(1129,4,1,1,1,32000,1,0,0,0,NULL),(1135,0,1,0,6,52000,0,0,0,1,NULL),(1136,0,1,0,6,52000,0,0,0,1,NULL),(1137,0,1,0,1,52000,0,0,0,0,NULL),(1139,1,1,1,4,32000,0,1,1,0,NULL),(1141,0,1,0,2,32000,0,0,0,0,NULL),(1142,1,1,0,1,52000,0,0,0,0,NULL),(1144,4,1,0,2,32000,0,0,0,0,NULL),(1146,0,1,0,1,32000,0,0,0,1,NULL),(1147,2,1,1,1,8000,0,1,0,0,NULL),(1148,0,1,0,1,52000,0,0,0,0,NULL),(1149,0,1,0,1,52000,0,0,0,0,NULL),(1150,0,1,0,1,52000,0,0,0,0,NULL),(2146,1,1,0,1,32000,0,0,1,0,NULL),(2147,0,1,0,1,52000,0,0,0,0,NULL),(2148,0,1,0,1,52000,0,0,0,0,NULL),(2150,0,1,0,1,8000,1,0,0,0,NULL),(2151,0,1,0,1,32000,1,0,0,0,NULL);
/*!40000 ALTER TABLE `devicemicrolog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `devicemicrox`
--

LOCK TABLES `devicemicrox` WRITE;
/*!40000 ALTER TABLE `devicemicrox` DISABLE KEYS */;
INSERT INTO `devicemicrox` VALUES (1136,1,1,0,0,1,'admin',1,'','ronent@fourtec.com;nissan@gmail.com;issim@fourtec.com',0,'',NULL),(1137,0,0,0,0,0,'',1,'','',0,'',NULL),(1138,1,1,0,0,1,'admin',1,'','ronent@fourtec.com',0,'',NULL),(1139,1,0,0,0,1,'nissim',0,'','nissim@fourtec.com',0,'',NULL),(1140,0,0,0,0,1,'nissim',1,'','nissim@fourtec.com',0,'',NULL),(1141,0,0,0,0,1,'FOURIER\\Nissim',1,'123456789012345','nissim@fourtec.com',1,'',0),(1142,0,0,0,0,0,'',1,'','',0,'',2),(1143,1,0,0,0,1,'FOURIER\\Ronen',1,'','ronent@fourtec.com',0,'',3),(1144,0,0,0,0,1,'FOURIER\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\Nissim',1,'','nissim@fourtec.com',0,'',3),(1145,0,1,0,0,0,'',1,'','',0,'',2),(1146,0,0,0,0,0,'',1,'','',0,'',0),(1147,0,0,0,0,1,'FOURIER',1,'','yuvalk@fourtec.com',0,'',3),(1148,1,0,0,0,0,'',1,'','',0,'',3),(1149,0,0,0,0,0,'',1,'','',0,'',0),(1150,1,0,0,0,0,'',1,'','',0,'',3),(2146,1,0,0,0,1,'FOURIER\\hagaizamir',1,'fourtec','hagaiz@fourtec.com',1,'',0),(2147,0,0,0,0,0,'',1,'','',0,'',0),(2148,1,0,0,0,0,'',1,'','',0,'',9),(2149,1,0,0,0,0,'',1,'','',0,'',3),(2150,0,0,0,0,0,'',1,'','',0,'',2),(2151,0,0,0,0,0,'',1,'','',0,'',3),(2152,0,0,0,0,0,'',1,'','',0,'',3);
/*!40000 ALTER TABLE `devicemicrox` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `devicepicolite`
--

LOCK TABLES `devicepicolite` WRITE;
/*!40000 ALTER TABLE `devicepicolite` DISABLE KEYS */;
INSERT INTO `devicepicolite` VALUES (1138,0,0,0,0,0),(1140,0,0,0,0,0),(1143,0,0,0,0,0),(1145,0,0,0,0,0),(2149,0,0,0,0,0),(2152,0,0,0,0,1);
/*!40000 ALTER TABLE `devicepicolite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `devices`
--

LOCK TABLES `devices` WRITE;
/*!40000 ALTER TABLE `devices` DISABLE KEYS */;
INSERT INTO `devices` VALUES (1136,1212121,3,'MicroLogPro2',4,4,2,0,NULL,'',2.3,-1,NULL,100,'2','3','1',NULL,NULL,'0','0',0,0,NULL,0,0,0,NULL,NULL,0),(1137,12345678,3,'fourtec',4,1,2,1,NULL,'',2.4,-1,NULL,100,'2','3','0',NULL,NULL,'0','0',0,0,NULL,0,1411294534,0,NULL,NULL,0),(1138,9204916,3,'Fourtec',2,1,2,1,NULL,'',1.1,-1,NULL,100,'1','0','0',NULL,NULL,'0','0',0,0,NULL,0,0,0,NULL,NULL,0),(1139,9135225,3,'fourtec',9,1,2,0,NULL,'',1.4,-1,NULL,100,'1','3','0',NULL,NULL,'0','0',0,0,NULL,0,1411307407,0,NULL,NULL,0),(1140,1001574,3,'Test',2,5,2,1,NULL,'',1.1,-1,NULL,100,'1','0','0',NULL,NULL,'0','0',0,0,NULL,0,0,0,NULL,NULL,0),(1141,9194950,3,'123456789012345',11,40,2,0,NULL,'',1.4,-1,NULL,76,'1','3','0',NULL,NULL,'0','0',0,0,NULL,0,0,0,NULL,NULL,0),(1142,7008284,3,'Comment',4,1,2,0,NULL,'',2.1,-1,NULL,148,'2','3','1',NULL,NULL,'0','0',0,0,NULL,0,1418562129,0,NULL,NULL,0),(1143,123,3,'tryout',2,37,2,0,NULL,'',1.7,-1,NULL,1,'1','0','0',NULL,NULL,'0','0',0,0,NULL,0,1429428460,0,NULL,NULL,0),(1144,9161549,3,'testQA Mic',11,4,1,0,NULL,'',1.4,-1,NULL,100,'1','3','0',NULL,NULL,'0','0',0,0,NULL,1,1435825825,0,'2015-07-02 05:51:10.000000',0,0),(1145,1020520,3,'PicoLite',12,1,2,1,NULL,'',1.11,11,NULL,100,'3','0','0',NULL,NULL,'0','0',0,0,NULL,0,1420963213,0,NULL,NULL,0),(1146,9178903,3,'fourtec',10,2,2,0,NULL,'',1.2,-1,NULL,78,'1','3','1',NULL,NULL,'0','0',0,0,NULL,0,1429104438,0,NULL,NULL,0),(1147,913742,3,'Devicie :)',11,40,2,0,NULL,'',1.5,-1,NULL,100,'1','3','0',NULL,NULL,'0','0',0,0,NULL,1,1439787948,0,'2015-07-21 13:16:58.000000',0,0),(1148,7008383,3,'MicroLogProII',4,2,1,1,NULL,'',2.5,-1,NULL,100,'2','3','1',NULL,NULL,'0','0',0,0,NULL,1,1429512779,0,'2015-06-01 07:26:07.000000',NULL,0),(1149,72145454,3,'fourtec',13,1,2,0,NULL,'',2.4,-1,NULL,100,'2','3','0',NULL,NULL,'0','0',0,0,NULL,0,0,0,NULL,NULL,0),(1150,7008373,3,'MicroLog Pro II',13,2,2,1,NULL,'',2.4,-1,NULL,100,'2','3','0',NULL,NULL,'0','0',0,0,NULL,1,1434535344,0,'2015-06-17 12:48:24.000000',NULL,0),(2146,9130644,3,'fourtec',10,1,2,1,NULL,'',1.3,-1,NULL,93,'1','3','0',NULL,NULL,'0','0',0,0,NULL,0,0,0,NULL,NULL,0),(2147,7008001,3,'Fourtec',4,1,2,1,NULL,'',2.5,-1,NULL,100,'2','3','3',NULL,NULL,'0','0',0,0,NULL,0,1439714804,0,'2015-08-05 11:49:36.000000',0,0),(2148,7008435,3,'fourtec-yossi',4,1,1,1,NULL,'',2.5,-1,NULL,100,'2','3','3',NULL,NULL,'0','0',0,0,NULL,1,1434049466,0,'2015-06-14 08:50:27.000000',NULL,0),(2149,1015966,3,'Fourtec',12,1,2,1,NULL,'',1.11,0,NULL,100,'5','0','0',NULL,NULL,'0','0',0,0,NULL,0,0,0,'2015-06-30 10:47:42.000000',NULL,0),(2150,9139488,3,'',11,1,2,0,NULL,'',1.4,-1,NULL,95,'1','3','0',NULL,NULL,'0','0',0,0,NULL,0,0,0,'2014-03-25 22:01:36.000000',0,0),(2151,45941,3,'Fourtec-ronen',10,1,1,0,NULL,'',1.6,0,NULL,100,'1','3','3',NULL,NULL,'0','0',0,0,NULL,1,16688,0,'2015-09-10 12:41:03.000000',0,0),(2152,1015972,3,'Fourtec',12,1,2,1,NULL,'',1.11,0,NULL,100,'5','0','0',NULL,NULL,'0','0',0,0,NULL,0,1439788156,0,NULL,0,0);
/*!40000 ALTER TABLE `devices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `devicesensors`
--

LOCK TABLES `devicesensors` WRITE;
/*!40000 ALTER TABLE `devicesensors` DISABLE KEYS */;
INSERT INTO `devicesensors` VALUES (4253,1136,'',2,9,NULL,1,0,0,-40,176,NULL),(4254,1136,'',3,6,NULL,1,0,0,0,100,NULL),(4255,1136,'',4,9,NULL,1,1,0,-58,302,NULL),(4256,1136,'',5,3,NULL,0,0,0,4,20,NULL),(4257,1136,'',6,2,NULL,0,0,0,0,10,NULL),(4258,1136,'',7,9,NULL,1,0,0,-58,302,NULL),(4259,1137,'',2,8,NULL,1,0,0,-40,80,NULL),(4260,1137,'',3,6,NULL,1,0,0,0,100,NULL),(4261,1137,'',4,8,NULL,0,0,0,-50,150,NULL),(4262,1137,'',5,3,NULL,0,0,0,4,20,NULL),(4263,1137,'',6,2,NULL,0,0,0,0,10,NULL),(4264,1137,'',7,8,NULL,0,0,0,-50,150,NULL),(4265,1138,'',1,8,NULL,1,0,0,-40,40,NULL),(4266,1139,'',5,3,NULL,0,0,0,4,20,NULL),(4267,1139,'',6,2,NULL,0,0,0,0,10,NULL),(4268,1139,'',7,8,NULL,1,0,0,-50,150,NULL),(4269,1140,'',1,8,NULL,1,0,0,-40,40,NULL),(4270,1141,'',1,8,NULL,1,1,0,-40,80,NULL),(4271,1142,'',2,8,NULL,1,0,0,-40,80,NULL),(4272,1142,'',3,6,NULL,1,0,0,0,100,NULL),(4273,1142,'',4,8,NULL,0,0,0,-50,150,NULL),(4274,1142,'',5,3,NULL,0,0,0,4,20,NULL),(4275,1142,'',6,2,NULL,0,0,0,0,10,NULL),(4276,1142,'',7,8,NULL,0,0,0,-50,150,NULL),(4277,1143,'',1,8,NULL,1,0,0,-40,40,NULL),(4278,1144,'',1,8,0,1,1,0,-40,80,NULL),(4279,1145,'',1,8,NULL,1,1,0,-40,80,NULL),(4280,1146,'',2,8,NULL,1,0,0,-40,80,NULL),(4281,1146,'',3,6,NULL,1,0,0,0,100,NULL),(4282,1146,'',4,8,NULL,0,0,0,-50,150,NULL),(4283,1147,'',1,8,0,1,1,0,-40,80,NULL),(4284,1148,'',2,8,NULL,1,0,0,-40,80,NULL),(4285,1148,'',3,6,NULL,1,1,0,0,100,NULL),(4286,1148,'',4,8,NULL,0,0,0,-50,150,NULL),(4287,1148,'',5,3,NULL,0,0,0,4,20,NULL),(4288,1148,'',6,2,NULL,0,0,0,0,10,NULL),(4289,1148,'',7,8,NULL,0,0,0,-50,150,NULL),(4290,1149,'',1,8,NULL,1,0,0,-40,80,NULL),(4291,1149,'',5,3,NULL,0,0,0,4,20,NULL),(4292,1149,'',6,2,NULL,0,0,0,0,10,NULL),(4293,1149,'',7,8,NULL,0,0,0,-50,150,NULL),(4294,1150,'',1,8,NULL,1,0,0,-40,80,NULL),(4295,1150,'',5,3,NULL,0,0,0,4,20,NULL),(4296,1150,'',6,2,NULL,0,0,0,0,10,NULL),(4297,1150,'',7,8,NULL,0,0,0,-50,150,NULL),(5280,2146,'',2,8,NULL,1,0,0,-40,80,NULL),(5281,2146,'',3,6,NULL,1,1,0,0,100,NULL),(5282,2146,'',4,8,NULL,0,0,0,-50,150,NULL),(5283,2147,'',2,8,0,1,0,0,-40,80,NULL),(5284,2147,'',3,6,1,1,0,0,0,100,NULL),(5285,2147,'',4,8,2,1,0,0,-50,150,NULL),(5286,2147,'',5,3,3,0,0,0,4,20,NULL),(5287,2147,'',6,2,3,0,0,0,0,10,NULL),(5288,2147,'',7,8,3,1,0,0,-50,150,NULL),(5289,2148,'',2,8,NULL,1,0,0,-40,80,NULL),(5290,2148,'',3,6,NULL,1,1,0,0,100,NULL),(5291,2148,'',4,8,NULL,1,0,0,-50,150,NULL),(5295,2149,'',1,8,0,1,0,0,-40,80,NULL),(5296,2150,'',1,8,0,1,0,0,-40,80,NULL),(5297,2151,'',2,9,0,1,1,0,-40,176,NULL),(5298,2151,'',3,6,1,1,1,0,0,100,NULL),(5299,2151,'',4,9,2,1,0,0,-58,302,NULL),(5300,2152,'',1,8,0,1,0,0,-40,80,NULL),(5292,2148,'',5,3,NULL,0,0,0,4,20,NULL),(5293,2148,'',6,2,NULL,0,0,0,0,10,NULL),(5294,2148,'',7,8,NULL,0,0,0,-50,150,NULL);
/*!40000 ALTER TABLE `devicesensors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `devicesetupfiles`
--

LOCK TABLES `devicesetupfiles` WRITE;
/*!40000 ALTER TABLE `devicesetupfiles` DISABLE KEYS */;
/*!40000 ALTER TABLE `devicesetupfiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `devicesmstrail`
--

LOCK TABLES `devicesmstrail` WRITE;
/*!40000 ALTER TABLE `devicesmstrail` DISABLE KEYS */;
/*!40000 ALTER TABLE `devicesmstrail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `devicestatuses`
--

LOCK TABLES `devicestatuses` WRITE;
/*!40000 ALTER TABLE `devicestatuses` DISABLE KEYS */;
INSERT INTO `devicestatuses` VALUES (1,'connected'),(2,'disconnected');
/*!40000 ALTER TABLE `devicestatuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `devicetypes`
--

LOCK TABLES `devicetypes` WRITE;
/*!40000 ALTER TABLE `devicetypes` DISABLE KEYS */;
INSERT INTO `devicetypes` VALUES (1,'LAN-R',0,'DataNet','LAN-R',NULL,1,0,6,NULL),(2,'PicoLite',0,'MicroX','PicoLite','PICOLITE-16K',1.1,0,5,1),(3,'EC800 16Bit Temperature',0,'MicroX','MicroLogProII','EC800A',2.5,0,2,1),(4,'EC850 Temperature & RH',0,'MicroX','MicroLogProII','EC850A',2.5,0,2,1),(5,'MicroLite II External',0,'MicroX','MicroLiteII','LITE5032P-EXT-A',1.6,0,1,1),(6,'MicroLite II Voltage 0-10 V',0,'MicroX','MicroLiteII','LITE5032P-V-A',1.6,0,1,1),(7,'MicroLite II Current 4-20 mA',0,'MicroX','MicroLiteII','LITE5032P-4/20A',1.6,0,1,1),(9,'MicroLite II External Temperature',0,'MicroX','MicroLiteII','LITE5032P-EXT-A',1.6,0,1,1),(10,'MicroLite II Temperature & RH',0,'MicroX','MicroLiteII','LITE5032P-RH-A',1.6,0,1,1),(11,'MicroLite II Temperature',0,'MicroX','MicroLiteII','LITE5008P-A/LITE5032P-A',1.6,0,1,1),(12,'PicoLite V2',0,'MicroX','PicoLite','PICOLITE-16K',1.11,0,5,1),(13,'EC800 Temperature',0,'MicroX','MicroLogProII','EC800A',1.26,0,2,1);
/*!40000 ALTER TABLE `devicetypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `emailsettings`
--

LOCK TABLES `emailsettings` WRITE;
/*!40000 ALTER TABLE `emailsettings` DISABLE KEYS */;
/*!40000 ALTER TABLE `emailsettings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `emailtemplates`
--

LOCK TABLES `emailtemplates` WRITE;
/*!40000 ALTER TABLE `emailtemplates` DISABLE KEYS */;
/*!40000 ALTER TABLE `emailtemplates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `groups`
--

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` VALUES (1,'group1',3);
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `languages`
--

LOCK TABLES `languages` WRITE;
/*!40000 ALTER TABLE `languages` DISABLE KEYS */;
INSERT INTO `languages` VALUES (1,'English',1,'en'),(1002,'Spanish',0,'es'),(1003,'Italian',0,'it'),(1004,'Portuguese',0,'pt'),(1005,'German',0,'de'),(1006,'Russian',0,'ru'),(1007,'Chinese',0,'zh'),(1008,'Japanese',0,'ja'),(1009,'French',0,'fr');
/*!40000 ALTER TABLE `languages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `measurementunits`
--

LOCK TABLES `measurementunits` WRITE;
/*!40000 ALTER TABLE `measurementunits` DISABLE KEYS */;
INSERT INTO `measurementunits` VALUES (1,'Volt'),(2,'mA');
/*!40000 ALTER TABLE `measurementunits` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `notificationtypes`
--

LOCK TABLES `notificationtypes` WRITE;
/*!40000 ALTER TABLE `notificationtypes` DISABLE KEYS */;
INSERT INTO `notificationtypes` VALUES (1,'Email'),(2,'SMS');
/*!40000 ALTER TABLE `notificationtypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `permissionactions`
--

LOCK TABLES `permissionactions` WRITE;
/*!40000 ALTER TABLE `permissionactions` DISABLE KEYS */;
INSERT INTO `permissionactions` VALUES (1,2,'Validate','Login',0),(2,1,'Create','Customers',0),(3,1,'Get','Contacts',0),(7,1,'Category','Devices',0),(8,2,'Site','Devices',0),(9,2,'Device','Devices',0),(10,2,'Get','Devices',0),(11,2,'Get','GroupPermissions',0),(12,2,'Get','Languages',0),(13,2,'Get','SecuredQuestions',0),(14,2,'Get','SensorLogs',0),(15,2,'Get','Sites',0),(16,2,'Create','Sites',1),(17,2,'Update','Sites',1),(18,2,'Delete','Sites',1),(19,1,'Create','Users',1),(20,2,'Get','Users',0),(21,1,'CreateAdmin','Users',1),(22,1,'Update','Users',1),(23,1,'Setup','User',1),(24,1,'Delete','Users',1),(25,2,'Get','Categories',0),(26,4,'Download','Device',1),(27,4,'DownloadData','Device',0),(28,4,'Setup','Device',1),(29,4,'Status','Device',0),(30,4,'Run','Device',1),(31,4,'Stop','Device',1),(32,4,'CancelDownload','Device',0),(33,4,'ResetCalibration','Device',1),(34,4,'UpdateFirmware','Device',1),(36,4,'Sensors','Device',1),(37,4,'Calibration','Device',1),(38,4,'MarkTimeStamp','Device',1),(39,2,'Backup','DBMaintain',0),(40,2,'Restore','DBMaintain',0),(41,2,'PasswordRecovery','User',0),(43,2,'SecureAnswerCheck','User',0),(44,2,'ResetPassword','User',0),(45,4,'setuptemplate','Device',0),(46,2,'SMSDelivery','SMS',0),(47,4,'Snooze','Device',0),(48,2,'logout','Login',0),(49,2,'DeviceSite','Device',0),(51,2,'Create','Site',0),(1050,2,'getarchive','reports',0),(1052,2,'getcalibrationlist','Calibration',0),(1053,2,'savecalibration','Calibration',0),(1054,2,'loadcalibration','Calibration',0),(1055,2,'getsetuplist','Device',0),(1056,2,'getdewpointdata','Analytics',0),(1057,2,'gethistogramdata','Analytics',0),(1058,4,'loadsetup','Device',0),(1059,4,'restoredevicefactorycalibration','Device',0),(1060,4,'resetdevicecalibration','Device',0),(1061,4,'calibrate','Calibration',0),(1062,2,'getmktdata','Analytics',0),(1063,2,'calculatemkt','Analytics',0),(1065,2,'getstatisticsdata','Analytics',0),(1066,2,'gettabledata','Analytics',0),(1067,2,'getgraphdata','Analytics',0),(1068,2,'getsensors','Analytics',0),(1069,2,'removesensor','Analytics',0),(1070,2,'addsensor','Analytics',0),(1071,2,'moresensordata','Device',0),(1074,2,'count','Alarms',0),(1075,2,'get','Alarms',0),(1076,1,'update','Alarms',0),(1077,1,'hide','Alarms',0),(1078,1,'hideAll','Alarms',0),(1080,1,'setreason','Alarms',0),(1081,2,'getusers','User',0),(1082,1,'certificate','Calibration',0),(1083,2,'lastsample','Alarms',0),(1084,2,'isonline','Alarms',0),(1085,2,'updatesensor','Analytics',0),(1086,1,'delete','Reports',0),(1087,2,'getreviews','Reports',0),(1088,2,'count','Reports',0),(1089,1,'approval','Reports',0),(1090,2,'addprofile','Reports',0),(1091,2,'getapprovalusers','Reports',0),(1092,2,'getdistributionlist','Reports',0),(1093,2,'getprofiles','Reports',0),(1094,2,'fileupload','Reports',0),(1095,1,'updateprofiles','Reports',0),(2087,2,'getuser','User',0),(2088,2,'getAccountBalance','User',0),(2089,2,'saveUser','User',0),(2090,2,'changepwd','User',0),(2091,2,'getSystemSettings','User',0),(2092,2,'savesystemsettings','User',0),(2093,2,'getdeviceautosetup','User',0),(2094,1,'applydeviceautosetup','User',0),(2095,2,'getsetupfiles','User',0),(2096,1,'deletesetupfiles','User',0),(2097,2,'getcalibrationcertificatedefaults','User',0),(2098,2,'setcalibrationcertificatedefaults','User',0),(2099,2,'fwupdates','User',0),(2100,1,'startFwUpdate','User',0),(2101,2,'getcustomsensors','Device',0),(2102,2,'getdevicefamily','Device',0),(2103,2,'getsensorunits','Device',0),(2104,2,'getbins','Analytics',0),(3101,2,'getbasesensor','Device',0),(3104,2,'getsites','Alarms',0),(3105,2,'getdevices','Alarms',0),(3106,2,'updatedevicenotifications','Alarms',0),(3107,2,'getcalibrationexpiryreminder','Alarms',0),(3108,2,'updatecalibrationexpiryreminder','Alarms',0),(3109,2,'getactivity','User',0),(3110,2,'getpwdconfig','User',0),(3111,2,'getprivilegesgroups','User',0),(3115,2,'getmatrix','User',0),(3117,4,'initcalibrationmode','Device',0);
/*!40000 ALTER TABLE `permissionactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `permissionactiontypes`
--

LOCK TABLES `permissionactiontypes` WRITE;
/*!40000 ALTER TABLE `permissionactiontypes` DISABLE KEYS */;
INSERT INTO `permissionactiontypes` VALUES (1,'Admin Control'),(2,'Application Activity'),(3,'Data Management'),(4,'Device Control');
/*!40000 ALTER TABLE `permissionactiontypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `permissiongroupactionmatrix`
--

LOCK TABLES `permissiongroupactionmatrix` WRITE;
/*!40000 ALTER TABLE `permissiongroupactionmatrix` DISABLE KEYS */;
INSERT INTO `permissiongroupactionmatrix` VALUES (27,1,'Download Data','Refers to the manual download command, not the actual function'),(27,2,'Download Data','Refers to the manual download command, not the actual function'),(27,3,'Download Data','Refers to the manual download command, not the actual function'),(27,4,'Download Data','Refers to the manual download command, not the actual function'),(27,5,'Download Data','Refers to the manual download command, not the actual function'),(28,1,'Setup Device','Refers to setup of all device properties including alarm.'),(28,2,'Setup Device','Refers to setup of all device properties including alarm.'),(28,3,'Setup Device','Refers to setup of all device properties including alarm.'),(30,1,'Run Device','Includes ability to run/stop single or all devices'),(30,2,'Run Device','Includes ability to run/stop single or all devices'),(30,3,'Run Device','Includes ability to run/stop single or all devices'),(31,1,'Stop Device','Includes ability to run/stop single or all devices'),(31,2,'Stop Device','Includes ability to run/stop single or all devices'),(31,3,'Stop Device','Includes ability to run/stop single or all devices'),(34,1,'Firmware Update','Includes ability to update firmware on multiple devices'),(34,2,'Firmware Update','Includes ability to update firmware on multiple devices'),(45,1,'Save Setup File',NULL),(45,2,'Save Setup File',NULL),(45,3,'Save Setup File',NULL),(1058,1,'Load Setup File',NULL),(1058,2,'Load Setup File',NULL),(1058,3,'Load Setup File',NULL),(1059,1,'Restore Default Calibration',NULL),(1059,2,'Restore Default Calibration',NULL),(1060,1,'Reset Calibration',NULL),(1060,2,'Reset Calibration',NULL),(1061,1,'Calibrate Device',NULL),(1061,2,'Calibrate Device',NULL);
/*!40000 ALTER TABLE `permissiongroupactionmatrix` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `permissiongroupactions`
--

LOCK TABLES `permissiongroupactions` WRITE;
/*!40000 ALTER TABLE `permissiongroupactions` DISABLE KEYS */;
INSERT INTO `permissiongroupactions` VALUES (15,1,3,1),(15,2,3,1),(15,3,3,1),(15,4,3,1),(15,5,3,1),(16,1,3,1),(16,2,3,1),(17,1,3,1),(17,2,3,1),(18,1,3,1),(18,2,3,1),(27,1,3,0),(27,2,3,1),(27,3,3,1),(27,4,3,1),(27,5,3,1),(27,6,3,1),(28,1,3,0),(28,2,3,1),(28,3,3,1),(28,6,3,1),(30,1,3,0),(30,2,3,1),(30,3,3,1),(30,6,3,1),(31,1,3,0),(31,2,3,1),(31,3,3,1),(31,6,3,1),(34,1,3,0),(34,2,3,1),(34,6,3,1),(45,1,3,0),(45,2,3,1),(45,6,3,1),(1053,1,3,1),(1053,2,3,1),(1054,1,3,1),(1054,2,3,1),(1058,1,3,0),(1058,2,3,1),(1058,3,3,1),(1059,1,3,0),(1059,2,3,1),(1060,1,3,0),(1060,2,3,1),(1061,1,3,0),(1061,2,3,1),(1080,1,3,1),(1080,2,3,1),(1080,3,3,1),(1082,1,3,1),(1086,1,3,1),(1086,2,3,1),(1089,1,3,1),(1089,2,3,1),(1090,1,3,1),(1090,2,3,1),(2094,1,3,1),(2094,2,3,1),(2094,6,3,1),(2096,1,3,1),(2096,2,3,1),(2096,6,3,1),(3117,1,3,1),(3117,2,3,1),(3117,6,3,1);
/*!40000 ALTER TABLE `permissiongroupactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `permissiongroups`
--

LOCK TABLES `permissiongroups` WRITE;
/*!40000 ALTER TABLE `permissiongroups` DISABLE KEYS */;
INSERT INTO `permissiongroups` VALUES (1,0,'Administrator',1,NULL,1,1),(2,0,'Sepervisor',0,NULL,2,1),(3,0,'Power User',0,NULL,3,1),(4,0,'User',0,NULL,4,1),(5,0,'Distributer',0,NULL,5,1),(6,0,'Super User',1,NULL,0,1);
/*!40000 ALTER TABLE `permissiongroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `permissionlevels`
--

LOCK TABLES `permissionlevels` WRITE;
/*!40000 ALTER TABLE `permissionlevels` DISABLE KEYS */;
INSERT INTO `permissionlevels` VALUES (1,'View'),(2,'Edit');
/*!40000 ALTER TABLE `permissionlevels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `permissionroles`
--

LOCK TABLES `permissionroles` WRITE;
/*!40000 ALTER TABLE `permissionroles` DISABLE KEYS */;
INSERT INTO `permissionroles` VALUES (1,'Super User'),(2,'Administrator'),(3,'Supervisor'),(4,'Power USer'),(5,'User');
/*!40000 ALTER TABLE `permissionroles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `permissionusersitedevicegroup`
--

LOCK TABLES `permissionusersitedevicegroup` WRITE;
/*!40000 ALTER TABLE `permissionusersitedevicegroup` DISABLE KEYS */;
/*!40000 ALTER TABLE `permissionusersitedevicegroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `reportbasetemplates`
--

LOCK TABLES `reportbasetemplates` WRITE;
/*!40000 ALTER TABLE `reportbasetemplates` DISABLE KEYS */;
INSERT INTO `reportbasetemplates` VALUES (1,'MicroX',NULL,'A4','dd-MM-yy','Celsius','HH:mm:ss'),(2,'DataNet',NULL,'A4','dd-MM-yy','Celsius','HH:mm:ss');
/*!40000 ALTER TABLE `reportbasetemplates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `reportcontacts`
--

LOCK TABLES `reportcontacts` WRITE;
/*!40000 ALTER TABLE `reportcontacts` DISABLE KEYS */;
INSERT INTO `reportcontacts` VALUES (15,2),(16,2);
/*!40000 ALTER TABLE `reportcontacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `reportdevicehistory`
--

LOCK TABLES `reportdevicehistory` WRITE;
/*!40000 ALTER TABLE `reportdevicehistory` DISABLE KEYS */;
/*!40000 ALTER TABLE `reportdevicehistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `reportdevices`
--

LOCK TABLES `reportdevices` WRITE;
/*!40000 ALTER TABLE `reportdevices` DISABLE KEYS */;
INSERT INTO `reportdevices` VALUES (6,14,9135225),(8,14,9204916),(7,14,12345678),(10,15,9204916),(9,15,12345678),(13,16,1020520),(11,16,7008284),(12,16,72145454);
/*!40000 ALTER TABLE `reportdevices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `reportperiodtypes`
--

LOCK TABLES `reportperiodtypes` WRITE;
/*!40000 ALTER TABLE `reportperiodtypes` DISABLE KEYS */;
INSERT INTO `reportperiodtypes` VALUES (1,'DAILY'),(2,'WEEKLY'),(3,'MONTHLY'),(4,'ONCE');
/*!40000 ALTER TABLE `reportperiodtypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `reportprocesscfr`
--

LOCK TABLES `reportprocesscfr` WRITE;
/*!40000 ALTER TABLE `reportprocesscfr` DISABLE KEYS */;
/*!40000 ALTER TABLE `reportprocesscfr` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `reportprocesstypes`
--

LOCK TABLES `reportprocesstypes` WRITE;
/*!40000 ALTER TABLE `reportprocesstypes` DISABLE KEYS */;
INSERT INTO `reportprocesstypes` VALUES (1,'Review'),(2,'Approve');
/*!40000 ALTER TABLE `reportprocesstypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `reportprocessusers`
--

LOCK TABLES `reportprocessusers` WRITE;
/*!40000 ALTER TABLE `reportprocessusers` DISABLE KEYS */;
INSERT INTO `reportprocessusers` VALUES (2,1,1),(2,32,0);
/*!40000 ALTER TABLE `reportprocessusers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `reportsections`
--

LOCK TABLES `reportsections` WRITE;
/*!40000 ALTER TABLE `reportsections` DISABLE KEYS */;
INSERT INTO `reportsections` VALUES (1,'Info'),(2,'Profile'),(3,'Summary'),(4,'Logger Data'),(5,'TimeStamps Data'),(6,'Sensor Graph');
/*!40000 ALTER TABLE `reportsections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `reportsectionsorder`
--

LOCK TABLES `reportsectionsorder` WRITE;
/*!40000 ALTER TABLE `reportsectionsorder` DISABLE KEYS */;
/*!40000 ALTER TABLE `reportsectionsorder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `reporttemplateprocessusers`
--

LOCK TABLES `reporttemplateprocessusers` WRITE;
/*!40000 ALTER TABLE `reporttemplateprocessusers` DISABLE KEYS */;
INSERT INTO `reporttemplateprocessusers` VALUES (15,1,2),(15,2,1),(16,2,2),(16,12,1);
/*!40000 ALTER TABLE `reporttemplateprocessusers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `reporttemplates`
--

LOCK TABLES `reporttemplates` WRITE;
/*!40000 ALTER TABLE `reporttemplates` DISABLE KEYS */;
INSERT INTO `reporttemplates` VALUES (1,1,'boomerang','bla bla',NULL,NULL,'A4',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,32,'test2','bla',NULL,NULL,'A4',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,1,'new profile','dsc',NULL,1,'LETTER','MM-dd-yyyy',0,3,2,'2015-08-16 12:35:15.680000',-1,-1,0,0,0,0,NULL),(4,1,'daily - temp','daily - temp',NULL,0,'A4','dd-MM-yy',0,-12,1,'1970-01-01 12:00:00.000000',-1,-1,0,0,0,0,'2015-08-16 11:42:19'),(5,1,'weekly - temp','',NULL,0,'A4','dd-MM-yy',0,-12,2,'2015-08-16 11:48:56.207000',2,-1,0,0,0,0,'2015-08-16 11:49:47'),(6,1,'weekly - temp','',NULL,0,'A4','dd-MM-yy',0,-12,2,'2015-08-16 11:50:10.223000',2,-1,0,0,0,0,'2015-08-16 11:50:57'),(7,1,'monthly - temp','',NULL,1,'A4','dd-MM-yy',1,-12,3,'2015-08-16 12:37:41.920000',-1,-1,0,0,0,0,'2015-08-16 11:52:39'),(8,1,'stam 1','',NULL,0,'A4','dd-MM-yy',0,-12,4,'2015-08-16 12:02:32.467000',-1,-1,1438689727,1439467327,0,0,'2015-08-16 12:03:21'),(9,1,'active=true','',NULL,1,'A4','dd-MM-yy',0,-12,1,'1970-01-01 12:00:00.000000',-1,-1,0,0,0,0,'2015-08-16 12:13:08'),(10,1,'aaaaaaa','',NULL,1,'A4','dd-MM-yy',1,-12,4,'2015-08-16 12:53:00.290000',-1,-1,1438549200,1440968400,0,0,'2015-08-16 12:51:44'),(11,1,'full','optional',NULL,1,'LETTER','dd-MM-yy',1,-7,4,'2015-08-16 12:54:38.090000',-1,-1,1438722000,1440622800,0,0,'2015-08-16 12:55:25'),(12,1,'asdasd','sadasd',NULL,1,'A4','dd-MM-yy',0,-12,1,'1970-01-01 12:00:00.000000',-1,-1,0,0,0,0,'2015-08-16 12:56:53'),(13,1,'fdgfdgdf','gfdg',NULL,1,'A4','dd-MM-yy',0,-12,4,'2015-08-16 12:59:03.287000',-1,-1,1439729973,1439729973,0,0,'2015-08-16 12:59:50'),(14,1,'hgfghfgh','gfh',NULL,1,'A4','dd-MM-yy',0,-12,1,'1970-01-01 12:00:00.000000',-1,-1,0,0,0,0,'2015-08-16 13:00:17'),(15,1,'fromfromfromfrom','',NULL,1,'A4','dd-MM-yy',0,-12,1,'1970-01-01 12:00:00.000000',-1,-1,0,0,0,0,'2015-08-16 13:02:39'),(16,1,'sadasdasd','',NULL,1,'A4','dd-MM-yy',0,-12,1,'1970-01-01 12:00:00.000000',-1,-1,0,0,0,0,'2015-08-16 13:06:32'),(17,1,'stam 2','bla bla',NULL,0,'A4','dd-MM-yy',0,-12,2,'2015-08-16 13:57:36.880000',-1,-1,0,0,0,0,'2015-08-16 13:10:30');
/*!40000 ALTER TABLE `reporttemplates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `securequestions`
--

LOCK TABLES `securequestions` WRITE;
/*!40000 ALTER TABLE `securequestions` DISABLE KEYS */;
INSERT INTO `securequestions` VALUES (1,'What was your childhood nickname?'),(2,'In what city did you meet your spouse/significant other?'),(3,'What is the name of your favorite childhood friend?'),(4,'What street did you live on in third grade?'),(5,'What school did you attend for sixth grade?'),(6,'What is the first name of the boy or girl that you first kissed?'),(7,'In what city or town was your first job?'),(8,'Where were you when you first heard about 9/11?'),(9,'What is your grandmother\'s first name?'),(10,'What is your pet\'s name?');
/*!40000 ALTER TABLE `securequestions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `sensoralarm`
--

LOCK TABLES `sensoralarm` WRITE;
/*!40000 ALTER TABLE `sensoralarm` DISABLE KEYS */;
INSERT INTO `sensoralarm` VALUES (3191,4285,0,20,0,60,0,0,0),(3192,5290,0,20,0,75,0,0,0),(3193,5284,0,0,0,70,0,0,0),(3194,4278,0,22,0,28,0,0,0),(3195,4283,0,24,0,26,0,0,0),(3196,5297,0,-40,0,40,0,0,0),(3197,5298,0,0,0,75,0,0,0);
/*!40000 ALTER TABLE `sensoralarm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `sensordownloadhistory`
--

LOCK TABLES `sensordownloadhistory` WRITE;
/*!40000 ALTER TABLE `sensordownloadhistory` DISABLE KEYS */;
INSERT INTO `sensordownloadhistory` VALUES (16,1001574,1,1411297188,1411299160,'2014-09-21 14:33:00'),(19,9204916,1,1411057808,1411314575,'2014-09-21 15:49:56'),(35,7008284,2,1418562130,1418569933,'2014-12-16 07:47:49'),(36,7008284,3,1418562130,1418569933,'2014-12-16 07:47:50'),(37,1020520,1,1420963224,1420963302,'2015-01-11 08:01:46'),(38,9178903,2,1429439280,1429440422,'2015-04-19 08:40:40'),(39,9178903,3,1429439280,1429440422,'2015-04-19 08:40:41'),(66,72145454,1,1410103278,1410103278,'2015-04-20 06:56:39'),(1076,9130644,2,1430573421,1431533421,'2015-05-13 17:13:16'),(1077,9130644,3,1430573421,1431533421,'2015-05-13 17:13:21'),(1194,7008383,2,1432652567,1433143907,'2015-06-01 10:32:09'),(1195,7008383,3,1432652567,1433143907,'2015-06-01 10:32:15'),(2393,7008435,2,1434293767,1434304317,'2015-06-14 11:52:07'),(2394,7008435,3,1434293767,1434304317,'2015-06-14 11:52:08'),(2395,7008435,4,1434293767,1434304317,'2015-06-14 11:52:09'),(3394,7008373,7,1434535343,1434536628,'2015-06-17 10:23:50'),(3403,7008373,1,1434536799,1434556624,'2015-06-17 15:57:07'),(3425,1015966,1,1435600993,1435825776,'2015-07-02 08:30:47'),(3574,9139488,1,1395312156,1395792096,'2015-07-19 13:41:06'),(3641,9161549,1,1435825826,1435827070,'2015-07-26 13:44:29'),(3679,45941,2,1441899653,1441899663,'2015-09-16 05:23:16'),(3680,45941,3,1441899653,1441899663,'2015-09-16 05:23:18'),(3681,45941,4,1441899653,1441899663,'2015-09-16 05:23:20');
/*!40000 ALTER TABLE `sensordownloadhistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `sensorlogshistory`
--

LOCK TABLES `sensorlogshistory` WRITE;
/*!40000 ALTER TABLE `sensorlogshistory` DISABLE KEYS */;
/*!40000 ALTER TABLE `sensorlogshistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `sensortypes`
--

LOCK TABLES `sensortypes` WRITE;
/*!40000 ALTER TABLE `sensortypes` DISABLE KEYS */;
INSERT INTO `sensortypes` VALUES (1,'Internal NTC',3),(2,'DigitalTemperature',2),(3,'Humidity',2),(4,'DewPoint',1),(5,'Current4_20mA',0),(6,'Voltage0_10V',NULL),(7,'ExternalNTC',NULL),(8,'PT100',NULL),(9,'Custom Sensor',NULL);
/*!40000 ALTER TABLE `sensortypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `sensorunits`
--

LOCK TABLES `sensorunits` WRITE;
/*!40000 ALTER TABLE `sensorunits` DISABLE KEYS */;
INSERT INTO `sensorunits` VALUES (1,'mV'),(2,'V'),(3,'mA'),(4,'Pulse'),(5,'pH'),(6,'RH'),(7,'mS'),(8,'C'),(9,'F'),(10,'Custom');
/*!40000 ALTER TABLE `sensorunits` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `serialnumbergeneration`
--

LOCK TABLES `serialnumbergeneration` WRITE;
/*!40000 ALTER TABLE `serialnumbergeneration` DISABLE KEYS */;
/*!40000 ALTER TABLE `serialnumbergeneration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `sirenroutedevices`
--

LOCK TABLES `sirenroutedevices` WRITE;
/*!40000 ALTER TABLE `sirenroutedevices` DISABLE KEYS */;
/*!40000 ALTER TABLE `sirenroutedevices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `sites`
--

LOCK TABLES `sites` WRITE;
/*!40000 ALTER TABLE `sites` DISABLE KEYS */;
INSERT INTO `sites` VALUES (1,0,3,'root',0,0,NULL,'root'),(2,1,3,'Lab 1',1,0,NULL,'root'),(3,1,3,'Laboratory',2,0,NULL,'root'),(4,2,3,'Level 2',1,0,NULL,'root/Lab 1'),(5,4,3,'Level 3',2,0,NULL,'root/Lab 1/Level 2'),(37,1,3,'Warehouse',0,0,NULL,'root'),(39,1,3,'Blood Bank',0,0,NULL,'root'),(40,1,3,'Pharmacy',0,0,NULL,'root'),(49,40,3,'Stam1',0,0,NULL,'root/Pharmacy'),(50,40,3,'Stam2',0,0,NULL,'root/Pharmacy'),(51,40,3,'Stam3',0,0,NULL,'root/Pharmacy');
/*!40000 ALTER TABLE `sites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `smssettings`
--

LOCK TABLES `smssettings` WRITE;
/*!40000 ALTER TABLE `smssettings` DISABLE KEYS */;
/*!40000 ALTER TABLE `smssettings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `sysdiagrams`
--

--
-- Dumping data for table `systemactionsaudit`
--

LOCK TABLES `systemactionsaudit` WRITE;
/*!40000 ALTER TABLE `systemactionsaudit` DISABLE KEYS */;
INSERT INTO `systemactionsaudit` VALUES (1,1,29,'2013-11-14 11:12:04',1014),(2,1,28,'2013-11-14 11:12:43',1014),(3,1,37,'2013-11-14 11:23:09',1005),(4,1,26,'2013-11-14 11:26:32',1005),(5,1,29,'2013-11-14 11:26:36',1005),(6,1,28,'2013-11-14 11:26:44',1005),(7,1,29,'2013-11-14 11:26:48',1005),(8,1,30,'2013-11-14 11:33:42',1005),(9,1,29,'2013-11-14 12:20:08',1014),(10,1,34,'2013-11-14 12:45:43',1014),(11,1,29,'2013-11-14 12:46:45',1014),(12,1,29,'2013-11-14 13:40:39',1014),(13,1,29,'2013-11-14 13:40:42',1014),(14,1,29,'2013-11-14 14:28:47',1014),(15,1,29,'2013-11-14 14:33:43',1014),(16,1,28,'2013-11-14 14:33:58',1014),(17,1,29,'2013-11-14 14:34:08',1014),(18,1,29,'2013-11-14 14:34:43',1014),(19,1,28,'2013-11-14 14:34:58',1014),(20,1,29,'2013-11-14 14:35:04',1014),(21,1,29,'2013-11-14 14:35:47',1014),(22,1,28,'2013-11-14 14:36:04',1014),(23,1,26,'2013-11-14 14:36:11',1014),(24,1,30,'2013-11-14 14:36:17',1014),(25,1,31,'2013-11-14 14:36:21',1014),(26,1,33,'2013-11-14 14:36:40',1014),(27,1,30,'2013-11-14 14:36:49',1014),(28,1,38,'2013-11-14 14:52:04',1014),(29,1,38,'2013-11-14 14:57:37',1014),(30,1,38,'2013-11-14 14:57:39',1014),(31,1,38,'2013-11-14 14:57:47',1014),(32,1,38,'2013-11-14 14:57:54',1014),(33,1,38,'2013-11-14 15:03:41',1014),(34,1,38,'2013-11-14 15:03:43',1014),(35,1,29,'2013-11-14 15:03:45',1014),(36,1,31,'2013-11-14 15:04:06',1014),(37,1,29,'2013-11-14 15:04:08',1014),(38,1,28,'2013-11-14 15:04:16',1014),(39,1,26,'2013-11-14 15:04:35',1014),(40,1,29,'2013-11-14 16:46:55',1014),(41,1,30,'2013-11-14 17:00:56',1014),(42,1,29,'2013-11-18 15:28:26',0),(43,1,30,'2013-11-18 17:22:18',1017),(44,1,30,'2013-11-18 17:30:13',1007),(45,1,29,'2013-11-19 14:34:41',1014),(46,1,29,'2013-11-19 14:37:25',1014),(47,1,29,'2013-11-19 14:37:55',1014),(48,1,29,'2013-11-19 14:38:28',1014),(49,1,29,'2013-11-19 14:39:14',1014),(50,1,29,'2013-11-19 14:39:39',1014),(51,1,29,'2013-11-19 14:54:24',1007),(52,1,29,'2013-11-19 14:54:50',1017),(53,1,29,'2013-11-19 14:55:23',1017),(54,1,29,'2013-11-19 14:56:43',1017),(55,1,29,'2013-11-19 14:57:32',1017),(56,1,29,'2013-11-19 14:58:52',1017),(57,1,29,'2013-11-19 16:30:40',1007),(58,1,29,'2013-11-20 07:52:35',1017),(59,1,29,'2013-11-20 07:54:34',1017),(60,1,29,'2013-11-20 07:54:42',1017),(61,1,29,'2013-11-20 08:01:54',1017),(62,1,29,'2013-11-20 08:02:10',1007),(63,1,29,'2013-11-20 08:02:35',1007),(64,1,29,'2013-11-20 08:03:46',1007),(65,1,29,'2013-11-20 08:03:58',1007),(66,1,29,'2013-11-20 08:04:12',1017),(67,1,29,'2013-11-20 08:04:29',1008),(68,1,29,'2013-11-20 08:05:51',1008),(69,1,29,'2013-11-20 08:06:31',1008),(70,1,29,'2013-11-20 08:11:20',1008),(71,1,29,'2013-11-20 08:11:32',1008),(72,1,29,'2013-11-20 08:11:43',1008),(73,1,29,'2013-11-20 08:22:32',1008),(74,1,29,'2013-11-20 13:48:11',1008),(75,1,29,'2013-11-20 14:21:00',1008),(76,1,29,'2013-11-20 14:36:17',1008),(77,1,29,'2013-11-20 14:36:42',1008),(78,1,29,'2013-11-20 14:38:28',1008),(79,1,29,'2013-11-20 14:38:39',1008),(80,1,29,'2013-11-20 14:41:54',1008),(81,1,29,'2013-11-20 14:44:36',1008),(82,1,29,'2013-11-20 14:46:08',1008),(83,1,29,'2013-11-20 14:47:07',1008),(84,1,29,'2013-11-20 14:48:26',1008),(85,1,29,'2013-11-21 08:22:42',1007),(86,1,29,'2013-11-21 14:36:21',1007),(87,1,29,'2013-11-21 14:36:38',1008),(88,1,29,'2013-11-24 09:16:23',1007),(89,1,29,'2013-11-24 09:16:50',1007),(90,1,29,'2013-11-24 10:17:22',1007),(91,1,29,'2013-11-24 10:19:12',1007),(92,1,29,'2013-11-24 10:20:56',1007),(93,1,29,'2013-11-24 11:06:41',1007),(94,1,29,'2013-11-24 11:07:12',1017),(95,1,29,'2013-11-24 11:07:47',1018),(96,1,29,'2013-11-24 11:08:44',1008),(97,1,29,'2013-11-24 11:11:04',1018),(98,1,29,'2013-11-24 11:11:41',1018),(99,1,29,'2013-11-24 11:13:05',1018),(100,1,29,'2013-11-24 11:13:21',1008),(101,1,29,'2013-11-24 11:13:27',1018),(102,1,29,'2013-11-24 11:13:49',1008),(103,1,29,'2013-11-24 11:13:53',1007),(104,1,29,'2013-11-24 11:14:12',1008),(105,1,29,'2013-11-24 11:24:41',1008),(106,1,29,'2013-11-24 11:49:24',1008),(107,1,29,'2013-11-24 12:25:38',1008),(108,1,29,'2013-11-24 12:33:30',1008),(109,1,29,'2013-11-24 12:34:41',1008),(110,1,29,'2013-11-24 12:37:24',1008),(111,1,29,'2013-11-24 12:39:06',1008),(112,1,29,'2013-11-24 12:40:59',1008),(113,1,29,'2013-11-24 12:50:33',1008),(114,1,29,'2013-11-24 14:08:56',1008),(115,1,29,'2013-11-24 14:11:40',1008),(116,1,29,'2013-11-24 14:18:46',1008),(117,1,29,'2013-11-24 14:21:32',1008),(118,1,29,'2013-11-24 14:29:51',1008),(119,1,29,'2013-11-24 14:33:53',1008),(120,1,29,'2013-11-24 14:50:48',1008),(121,1,29,'2013-11-24 15:03:26',1008),(122,1,29,'2013-11-24 15:04:14',1008),(123,1,28,'2013-11-24 15:04:40',1008),(124,1,29,'2013-11-24 15:04:50',1008),(125,1,28,'2013-11-24 15:04:55',1008),(126,1,29,'2013-11-24 15:05:01',1008),(127,1,28,'2013-11-24 15:05:18',1008),(128,1,29,'2013-11-24 15:05:21',1008),(129,1,28,'2013-11-24 15:05:57',1008),(130,1,29,'2013-11-24 15:06:00',1008),(131,1,28,'2013-11-24 15:06:17',1008),(132,1,29,'2013-11-24 15:06:19',1008),(133,1,28,'2013-11-24 15:06:27',1008),(134,1,29,'2013-11-24 15:06:30',1008),(135,1,29,'2013-11-24 15:07:05',1008),(136,1,29,'2013-11-24 15:09:03',1008),(137,1,28,'2013-11-24 15:10:45',1008),(138,1,29,'2013-11-24 15:11:00',1008),(139,1,29,'2013-11-24 15:11:03',1008),(140,1,28,'2013-11-24 15:11:19',1008),(141,1,29,'2013-11-24 15:11:23',1008),(142,1,29,'2013-11-24 15:13:19',1008),(143,1,28,'2013-11-24 15:13:33',1008),(144,1,29,'2013-11-24 15:13:37',1008),(145,1,28,'2013-11-24 15:13:53',1008),(146,1,29,'2013-11-24 15:13:55',1008),(147,1,28,'2013-11-24 15:14:01',1008),(148,1,29,'2013-11-24 15:14:04',1008),(149,1,29,'2013-11-24 15:18:23',1008),(150,1,28,'2013-11-24 15:18:31',1008),(151,1,29,'2013-11-24 15:18:34',1008),(152,1,28,'2013-11-24 15:18:41',1008),(153,1,29,'2013-11-24 15:18:44',1008),(154,1,28,'2013-11-24 15:18:50',1008),(155,1,29,'2013-11-24 15:18:53',1008),(156,1,28,'2013-11-24 15:19:20',1008),(157,1,29,'2013-11-24 15:19:22',1008),(158,1,28,'2013-11-24 15:19:43',1008),(159,1,29,'2013-11-24 15:19:45',1008),(160,1,28,'2013-11-24 15:19:59',1008),(161,1,29,'2013-11-24 15:20:05',1008),(162,1,29,'2013-11-24 15:20:12',1017),(163,1,29,'2013-11-24 15:20:25',1017),(164,1,29,'2013-11-24 15:20:41',1008),(165,1,28,'2013-11-24 15:20:45',1008),(166,1,29,'2013-11-24 15:56:13',1014),(167,1,26,'2013-11-24 15:56:48',1014),(168,1,30,'2013-11-24 15:57:15',1014),(169,1,31,'2013-11-24 15:57:23',1014),(170,1,30,'2013-11-24 15:57:28',1014),(171,1,38,'2013-11-24 15:57:30',1014),(172,1,38,'2013-11-24 15:57:34',1014),(173,1,31,'2013-11-24 15:57:39',1014),(174,1,29,'2013-11-24 15:58:03',1008),(175,1,28,'2013-11-24 15:58:22',1008),(176,1,29,'2013-11-24 15:58:26',1008),(177,1,26,'2013-11-24 15:58:37',1008),(178,1,30,'2013-11-24 15:59:02',1008),(179,1,31,'2013-11-24 16:00:46',1008),(180,1,30,'2013-11-24 16:01:42',1008),(181,1,30,'2013-11-24 16:01:48',1014),(182,1,31,'2013-11-24 16:12:55',1008),(183,1,30,'2013-11-24 16:26:32',1017),(184,1,30,'2013-11-24 17:19:48',1008),(185,1,31,'2013-11-25 15:00:56',1008),(186,1,29,'2013-11-25 15:01:03',1008),(187,1,28,'2013-11-25 15:02:25',1008),(188,1,30,'2013-11-25 15:03:35',1008),(189,1,31,'2013-11-25 15:48:11',1008),(190,1,38,'2013-11-25 17:05:53',1014),(191,1,29,'2013-11-25 17:05:59',1014),(192,1,31,'2013-11-25 17:06:17',1014),(193,1,29,'2013-11-25 17:06:19',1014),(194,1,30,'2013-11-25 17:16:35',1014),(195,1,29,'2013-11-26 08:23:01',1020),(196,1,29,'2013-11-26 08:24:12',1020),(197,1,26,'2013-11-26 08:26:47',1020),(198,1,31,'2013-11-26 08:28:27',1020),(199,1,29,'2013-11-26 09:48:16',1020),(200,1,29,'2013-11-26 09:49:49',1020),(201,1,29,'2013-11-26 09:50:52',1020),(202,1,29,'2013-11-26 09:54:36',1020),(203,1,29,'2013-11-26 09:56:13',1020),(204,1,29,'2013-11-26 10:27:57',1020),(205,1,29,'2013-11-26 10:41:16',1020),(206,1,29,'2013-11-26 10:47:51',1020),(207,1,29,'2013-11-26 10:49:57',1020),(208,1,29,'2013-11-26 10:50:02',1020),(209,1,29,'2013-11-26 10:54:45',1014),(210,1,31,'2013-11-26 10:54:53',1014),(211,1,30,'2013-11-26 10:54:56',1014),(212,1,30,'2013-11-26 10:55:14',1008),(213,1,30,'2013-11-26 10:55:56',1020),(214,1,29,'2013-11-26 10:56:49',1008),(215,1,31,'2013-11-26 10:57:00',1008),(216,1,30,'2013-11-26 10:57:08',1008),(217,1,31,'2013-11-26 11:00:48',1014),(218,1,29,'2013-11-26 11:00:50',1014),(219,1,29,'2013-11-27 10:42:22',1020),(220,1,31,'2013-11-27 10:43:10',1020),(221,1,29,'2013-11-27 10:43:12',1020),(222,1,28,'2013-11-27 10:43:19',1020),(223,1,29,'2013-11-27 10:43:30',1020),(224,1,28,'2013-11-27 10:43:44',1020),(225,1,29,'2013-11-27 10:43:58',1020),(226,1,29,'2013-11-27 10:46:37',1020),(227,1,28,'2013-11-27 10:47:23',1020),(228,1,29,'2013-11-27 10:47:28',1020),(229,1,29,'2013-11-27 10:57:16',1020),(230,1,28,'2013-11-27 10:57:26',1020),(231,1,29,'2013-11-27 11:02:56',1020),(232,1,28,'2013-11-27 11:03:35',1020),(233,1,29,'2013-11-27 11:06:26',1020),(234,1,28,'2013-11-27 11:06:38',1020),(235,1,29,'2013-11-27 11:07:13',1020),(236,1,28,'2013-11-27 11:07:48',1020),(237,1,29,'2013-11-27 11:08:00',1020),(238,1,29,'2013-11-27 11:22:52',1020),(239,1,28,'2013-11-27 11:23:03',1020),(240,1,29,'2013-11-27 11:25:07',1020),(241,1,28,'2013-11-27 11:25:25',1020),(242,1,29,'2013-11-27 11:26:13',1020),(243,1,29,'2013-11-27 13:28:45',1020),(244,1,29,'2013-11-27 13:29:21',1020),(245,1,29,'2013-11-27 13:31:52',1020),(246,1,29,'2013-11-27 13:35:33',1020),(247,1,29,'2013-11-27 13:55:04',1020),(248,1,29,'2013-11-27 13:55:35',1020),(249,1,28,'2013-11-27 13:55:46',1020),(250,1,29,'2013-11-27 13:55:49',1020),(251,1,28,'2013-11-27 13:56:28',1020),(252,1,29,'2013-11-28 07:44:32',1020),(253,1,28,'2013-11-28 07:48:16',1020),(254,1,29,'2013-11-28 07:48:20',1020),(255,1,29,'2013-11-28 07:49:37',1020),(256,1,28,'2013-11-28 07:50:04',1020),(257,1,29,'2013-11-28 07:57:47',1020),(258,1,28,'2013-11-28 07:58:04',1020),(259,1,29,'2013-11-28 07:58:09',1020),(260,1,29,'2013-11-28 07:58:33',1020),(261,1,29,'2013-11-28 08:14:25',1020),(262,1,29,'2013-11-28 08:14:53',1020),(263,1,28,'2013-11-28 08:15:11',1020),(264,1,29,'2013-11-28 08:18:55',1020),(265,1,29,'2013-11-28 08:19:13',1020),(266,1,28,'2013-11-28 08:19:26',1020),(267,1,29,'2013-11-28 08:20:46',1020),(268,1,29,'2013-11-28 08:21:24',1020),(269,1,29,'2013-11-28 08:55:26',1020),(270,1,29,'2013-11-28 09:09:15',1020),(271,1,28,'2013-11-28 09:09:36',1020),(272,1,29,'2013-11-28 09:09:40',1020),(273,1,29,'2013-11-28 09:11:47',1020),(274,1,28,'2013-11-28 09:13:40',1020),(275,1,29,'2013-11-28 09:13:44',1020),(276,1,29,'2013-11-28 09:15:02',1020),(277,1,29,'2013-11-28 09:16:26',1020),(278,1,28,'2013-11-28 09:16:45',1020),(279,1,29,'2013-11-28 09:16:47',1020),(280,1,29,'2013-11-28 09:17:19',1020),(281,1,29,'2013-11-28 09:23:23',1020),(282,1,28,'2013-11-28 09:23:46',1020),(283,1,29,'2013-11-28 09:23:57',1020),(284,1,29,'2013-11-28 09:47:10',1020),(285,1,29,'2013-11-28 10:43:56',1020),(286,1,29,'2013-11-28 10:44:45',1020),(287,1,29,'2013-11-28 11:06:22',1020),(288,1,29,'2013-11-28 11:08:37',1020),(289,1,29,'2013-11-28 11:09:36',1020),(290,1,29,'2013-11-28 11:10:58',1020),(291,1,29,'2013-11-28 11:22:27',1020),(292,1,29,'2013-11-28 11:23:14',1020),(293,1,29,'2013-11-28 11:27:30',1020),(294,1,29,'2013-11-28 11:28:49',1020),(295,1,28,'2013-11-28 11:29:14',1020),(296,1,29,'2013-11-28 11:29:17',1020),(297,1,29,'2013-11-28 11:38:12',1020),(298,1,29,'2013-11-28 11:41:06',1020),(299,1,29,'2013-11-28 11:47:43',1019),(300,1,29,'2013-11-28 11:49:09',1019),(301,1,31,'2013-11-28 12:07:21',1020),(302,1,29,'2013-11-28 12:07:24',1020),(303,1,29,'2013-12-01 09:15:14',1014),(304,1,28,'2013-12-01 09:15:29',1014),(305,1,29,'2013-12-01 09:15:32',1014),(306,1,28,'2013-12-01 09:15:38',1014),(307,1,29,'2013-12-01 09:15:40',1014),(308,1,29,'2013-12-01 09:16:02',1014),(309,1,29,'2013-12-01 09:16:49',1014),(310,1,29,'2013-12-01 09:18:38',1014),(311,1,28,'2013-12-01 09:18:53',1014),(312,1,29,'2013-12-01 09:18:56',1014),(313,1,28,'2013-12-01 09:19:17',1014),(314,1,29,'2013-12-01 09:19:19',1014),(315,1,29,'2013-12-01 09:19:35',1014),(316,1,28,'2013-12-01 09:19:48',1014),(317,1,29,'2013-12-01 09:19:52',1014),(318,1,29,'2013-12-01 09:21:34',1014),(319,1,28,'2013-12-01 09:21:58',1014),(320,1,29,'2013-12-01 09:22:01',1014),(321,1,28,'2013-12-01 09:22:24',1014),(322,1,29,'2013-12-01 09:22:29',1014),(323,1,29,'2013-12-01 09:28:08',1014),(324,1,29,'2013-12-01 09:51:59',1014),(325,1,29,'2013-12-01 10:36:36',1014),(326,1,29,'2013-12-01 10:40:44',1014),(327,1,29,'2013-12-01 10:41:32',1014),(328,1,29,'2013-12-01 10:42:04',1014),(329,1,29,'2013-12-01 10:46:22',1014),(330,1,29,'2013-12-01 10:50:40',1014),(331,1,29,'2013-12-01 10:53:04',1014),(332,1,29,'2013-12-01 10:54:25',1014),(333,1,29,'2013-12-01 10:56:15',1014),(334,1,29,'2013-12-01 10:59:45',1014),(335,1,29,'2013-12-01 11:01:51',1014),(336,1,29,'2013-12-01 11:05:14',1014),(337,1,29,'2013-12-01 11:09:06',1014),(338,1,29,'2013-12-01 11:20:14',1014),(339,1,29,'2013-12-01 11:20:43',1014),(340,1,29,'2013-12-01 11:28:52',1014),(341,1,29,'2013-12-01 11:34:45',1014),(342,1,29,'2013-12-01 11:48:38',1014),(343,1,29,'2013-12-01 11:49:19',1014),(344,1,29,'2013-12-01 11:54:54',1014),(345,1,29,'2013-12-01 11:59:13',1014),(346,1,29,'2013-12-01 12:00:47',1014),(347,1,29,'2013-12-01 12:01:41',1014),(348,1,29,'2013-12-01 12:02:15',1014),(349,1,29,'2013-12-01 12:02:41',1014),(350,1,29,'2013-12-01 12:10:51',1014),(351,1,28,'2013-12-01 12:11:35',1014),(352,1,29,'2013-12-01 12:11:41',1014),(353,1,29,'2013-12-01 12:12:01',1014),(354,1,29,'2013-12-01 12:39:23',1014),(355,1,28,'2013-12-01 12:39:53',1014),(356,1,29,'2013-12-01 12:45:59',1014),(357,1,28,'2013-12-01 12:46:15',1014),(358,1,29,'2013-12-01 12:51:43',1014),(359,1,28,'2013-12-01 12:51:56',1014),(360,1,29,'2013-12-01 12:53:10',1014),(361,1,28,'2013-12-01 12:53:20',1014),(362,1,29,'2013-12-01 12:55:10',1014),(363,1,29,'2013-12-01 12:56:22',1014),(364,1,29,'2013-12-01 13:00:55',1014),(365,1,29,'2013-12-01 13:07:05',1014),(366,1,29,'2013-12-01 13:07:15',1014),(367,1,29,'2013-12-01 13:18:31',1008),(368,1,28,'2013-12-01 13:18:40',1008),(369,1,29,'2013-12-01 13:31:31',1014),(370,1,29,'2013-12-01 13:35:46',1014),(371,1,29,'2013-12-01 13:36:47',1014),(372,1,29,'2013-12-01 13:46:30',1007),(373,1,29,'2013-12-01 13:47:33',1007),(374,1,29,'2013-12-01 13:48:56',1007),(375,1,29,'2013-12-01 13:49:29',1007),(376,1,29,'2013-12-01 13:50:21',1007),(377,1,29,'2013-12-01 13:59:36',1007),(378,1,29,'2013-12-01 14:11:01',1007),(379,1,29,'2013-12-01 14:17:59',1007),(380,1,29,'2013-12-01 14:44:26',1008),(381,1,28,'2013-12-01 14:44:37',1008),(382,1,29,'2013-12-01 14:45:20',1008),(383,1,28,'2013-12-01 14:45:49',1008),(384,1,29,'2013-12-01 14:45:55',1008),(385,1,28,'2013-12-01 14:46:41',1008),(386,1,29,'2013-12-01 14:48:27',1008),(387,1,29,'2013-12-01 14:48:54',1008),(388,1,29,'2013-12-01 14:50:05',1008),(389,1,28,'2013-12-01 14:50:37',1008),(390,1,29,'2013-12-01 16:15:28',1008),(391,1,28,'2013-12-01 16:15:44',1008),(392,1,29,'2013-12-01 16:15:53',1008),(393,1,28,'2013-12-01 16:16:30',1008),(394,1,29,'2013-12-01 16:17:00',1014),(395,1,28,'2013-12-01 16:17:13',1014),(396,1,29,'2013-12-01 16:17:29',1014),(397,1,28,'2013-12-01 16:18:10',1014),(398,1,29,'2013-12-01 16:18:14',1014),(399,1,29,'2013-12-01 16:21:10',1008),(400,1,28,'2013-12-01 16:21:37',1008),(401,1,29,'2013-12-01 16:21:44',1008),(402,1,28,'2013-12-01 16:22:14',1008),(403,1,29,'2013-12-01 16:30:10',1008),(404,1,28,'2013-12-01 16:30:28',1008),(405,1,29,'2013-12-01 16:30:34',1008),(406,1,29,'2013-12-01 16:32:25',1007),(407,1,29,'2013-12-01 16:49:07',1007),(408,1,29,'2013-12-01 16:51:38',1007),(409,1,29,'2013-12-01 16:53:47',1007),(410,1,29,'2013-12-01 16:57:55',1007),(411,1,29,'2013-12-01 17:01:00',1008),(412,1,29,'2013-12-01 17:01:13',1008),(413,1,28,'2013-12-01 17:02:15',1008),(414,1,29,'2013-12-01 17:02:24',1008),(415,1,29,'2013-12-01 17:02:32',1008),(416,1,29,'2013-12-01 17:07:18',1008),(417,1,28,'2013-12-01 17:07:28',1008),(418,1,29,'2013-12-01 17:07:33',1008),(419,1,29,'2013-12-01 17:07:39',1008),(420,1,29,'2013-12-01 17:10:34',1008),(421,1,29,'2013-12-01 17:12:19',1008),(422,1,29,'2013-12-01 17:13:21',1008),(423,1,29,'2013-12-01 17:14:17',1008),(424,1,29,'2013-12-01 17:15:11',1008),(425,1,29,'2013-12-01 17:20:25',1008),(426,1,29,'2013-12-01 17:22:04',1008),(427,1,29,'2013-12-01 17:23:32',1008),(428,1,29,'2013-12-02 07:49:07',1008),(429,1,29,'2013-12-02 07:55:04',1008),(430,1,28,'2013-12-02 07:55:43',1008),(431,1,29,'2013-12-02 07:55:46',1008),(432,1,29,'2013-12-02 07:59:56',1008),(433,1,28,'2013-12-02 08:00:17',1008),(434,1,29,'2013-12-02 08:00:24',1008),(435,1,29,'2013-12-02 08:00:44',1008),(436,1,28,'2013-12-02 08:01:17',1008),(437,1,29,'2013-12-02 08:01:20',1008),(438,1,28,'2013-12-02 08:01:49',1008),(439,1,29,'2013-12-02 08:01:51',1008),(440,1,29,'2013-12-02 08:02:07',1008),(441,1,28,'2013-12-02 08:02:35',1008),(442,1,29,'2013-12-02 08:02:37',1008),(443,1,26,'2013-12-02 08:02:59',1008),(444,1,29,'2013-12-02 08:04:08',1017),(445,1,28,'2013-12-02 08:04:33',1017),(446,1,29,'2013-12-02 08:04:35',1017),(447,1,29,'2013-12-02 08:04:54',1017),(448,1,28,'2013-12-02 08:05:15',1017),(449,1,29,'2013-12-02 08:05:17',1017),(450,1,29,'2013-12-02 08:28:03',1017),(451,1,28,'2013-12-02 08:28:34',1017),(452,1,29,'2013-12-02 08:30:01',1017),(453,1,29,'2013-12-02 08:34:27',1017),(454,1,29,'2013-12-02 08:35:54',1017),(455,1,28,'2013-12-02 08:36:15',1017),(456,1,29,'2013-12-02 08:36:18',1017),(457,1,29,'2013-12-02 08:36:44',1017),(458,1,29,'2013-12-02 08:45:37',1017),(459,1,28,'2013-12-02 08:45:51',1017),(460,1,29,'2013-12-02 08:45:55',1017),(461,1,29,'2013-12-02 08:48:53',1017),(462,1,28,'2013-12-02 08:49:09',1017),(463,1,29,'2013-12-02 08:49:23',1017),(464,1,28,'2013-12-02 08:49:35',1017),(465,1,29,'2013-12-02 08:49:43',1017),(466,1,29,'2013-12-02 08:51:52',1017),(467,1,28,'2013-12-02 08:52:07',1017),(468,1,29,'2013-12-02 08:53:21',1017),(469,1,28,'2013-12-02 08:53:32',1017),(470,1,29,'2013-12-02 08:53:34',1017),(471,1,29,'2013-12-02 08:53:50',1017),(472,1,29,'2013-12-02 08:53:56',1017),(473,1,29,'2013-12-02 14:02:30',1017),(474,1,28,'2013-12-02 14:02:51',1017),(475,1,29,'2013-12-02 14:03:09',1017),(476,1,28,'2013-12-02 14:03:27',1017),(477,1,29,'2013-12-02 14:03:34',1017),(478,1,28,'2013-12-02 14:03:54',1017),(479,1,29,'2013-12-02 14:03:57',1017),(480,1,28,'2013-12-02 14:04:05',1017),(481,1,29,'2013-12-02 14:04:25',1007),(482,1,29,'2013-12-02 14:05:26',1007),(483,1,29,'2013-12-02 14:06:36',1007),(484,1,29,'2013-12-02 14:08:21',1017),(485,1,28,'2013-12-02 14:08:24',1017),(486,1,29,'2013-12-02 14:08:41',1007),(487,1,29,'2013-12-02 15:25:59',1007),(488,1,29,'2013-12-02 15:32:13',1007),(489,1,29,'2013-12-02 16:06:45',1007),(490,1,29,'2013-12-02 16:08:41',1007),(491,1,29,'2013-12-02 16:13:05',1007),(492,1,29,'2013-12-02 16:16:20',1007),(493,1,29,'2013-12-02 16:16:34',1007),(494,1,29,'2013-12-02 16:31:41',1007),(495,1,29,'2013-12-02 16:39:14',1007),(496,1,29,'2013-12-02 16:45:55',1007),(497,1,29,'2013-12-02 16:47:05',1007),(498,1,29,'2013-12-02 17:03:38',1007),(499,1,29,'2013-12-02 17:05:16',1008),(500,1,28,'2013-12-02 17:05:43',1008),(501,1,29,'2013-12-02 17:05:46',1008),(502,1,29,'2013-12-02 17:06:04',1007),(503,1,29,'2013-12-02 17:15:23',1007),(1496,1,29,'2013-12-02 17:24:07',1007),(1497,1,29,'2013-12-03 07:40:46',1008),(1498,1,29,'2013-12-03 07:41:07',1007),(1499,1,29,'2013-12-03 07:48:23',1007),(1500,1,29,'2013-12-03 07:50:29',1007),(1501,1,29,'2013-12-03 08:45:28',1007),(1502,1,29,'2013-12-03 09:03:29',1007),(1503,1,29,'2013-12-03 09:04:53',1007),(1504,1,29,'2013-12-03 09:05:47',1007),(1505,1,29,'2013-12-03 09:06:17',1007),(1506,1,29,'2013-12-03 09:06:45',1007),(1507,1,29,'2013-12-03 09:07:27',1019),(1508,1,29,'2013-12-03 09:08:13',1008),(1509,1,30,'2013-12-03 09:08:30',1008),(1510,1,29,'2013-12-03 09:08:42',1008),(1511,1,29,'2013-12-03 09:09:09',1007),(1512,1,29,'2013-12-03 09:14:24',1007),(1513,1,29,'2013-12-03 09:23:40',1007),(1514,1,29,'2013-12-03 09:27:44',1007),(1515,1,29,'2013-12-03 09:31:31',1007),(1516,1,29,'2013-12-03 10:45:01',1007),(1517,1,29,'2013-12-03 10:48:27',1007),(1518,1,29,'2013-12-03 10:49:11',1007),(1519,1,29,'2013-12-03 10:49:33',1007),(1520,1,29,'2013-12-03 10:50:11',1007),(1521,1,29,'2013-12-03 10:50:47',1007),(1522,1,29,'2013-12-03 10:50:51',1014),(1523,1,29,'2013-12-03 10:50:56',1008),(1524,1,29,'2013-12-03 10:51:14',1007),(1525,1,29,'2013-12-03 10:51:25',1014),(1526,1,29,'2013-12-03 10:51:30',1007),(1527,1,29,'2013-12-03 10:51:43',1007),(1528,1,29,'2013-12-03 10:51:50',1007),(1529,1,29,'2013-12-03 10:52:03',1007),(1530,1,29,'2013-12-03 10:52:06',1014),(1531,1,29,'2013-12-03 10:52:10',1007),(1532,1,29,'2013-12-03 10:52:35',1007),(1533,1,29,'2013-12-03 10:53:05',1007),(1534,1,29,'2013-12-03 10:53:09',1014),(1535,1,29,'2013-12-03 10:53:12',1008),(1536,1,29,'2013-12-03 10:53:18',1007),(1537,1,29,'2013-12-03 10:54:04',1008),(1538,1,29,'2013-12-03 10:54:08',1014),(1539,1,29,'2013-12-03 10:54:14',1007),(1540,1,29,'2013-12-03 10:55:42',1007),(1541,1,29,'2013-12-03 10:55:56',1007),(1542,1,29,'2013-12-03 10:56:07',1007),(1543,1,29,'2013-12-03 10:56:15',1007),(1544,1,29,'2013-12-03 10:56:53',1007),(1545,1,29,'2013-12-03 10:57:07',1007),(1546,1,29,'2013-12-03 15:26:29',1007),(1547,1,31,'2013-12-03 15:31:01',1008),(1548,1,26,'2013-12-03 15:31:28',1008),(1549,1,30,'2013-12-03 15:55:28',1008),(1550,1,31,'2013-12-03 15:59:49',1008),(1551,1,29,'2013-12-03 15:59:50',1008),(1552,1,28,'2013-12-03 15:59:56',1008),(1553,1,30,'2013-12-03 16:00:01',1008),(1554,1,26,'2013-12-03 16:01:02',1014),(1555,1,29,'2013-12-03 16:01:39',1014),(1556,1,31,'2013-12-03 16:02:22',1014),(1557,1,29,'2013-12-03 16:02:26',1014),(1558,1,28,'2013-12-03 16:02:34',1014),(1559,1,30,'2013-12-03 16:02:39',1014),(1560,1,29,'2013-12-03 16:28:55',1014),(1561,1,31,'2013-12-03 16:29:29',1014),(1562,1,29,'2013-12-03 16:29:37',1014),(1563,1,28,'2013-12-03 16:29:47',1014),(1564,1,29,'2013-12-03 16:29:49',1014),(1565,1,29,'2013-12-03 16:30:01',1014),(1566,1,29,'2013-12-03 16:31:24',1014),(1567,1,28,'2013-12-03 16:31:30',1014),(1568,1,29,'2013-12-03 16:31:36',1014),(1569,1,29,'2013-12-03 16:38:28',1007),(1570,1,29,'2013-12-04 07:54:48',1020),(1571,1,28,'2013-12-04 07:55:20',1020),(1572,1,29,'2013-12-04 07:55:35',1020),(1573,1,29,'2013-12-04 07:56:48',1020),(1574,1,28,'2013-12-04 07:57:09',1020),(1575,1,29,'2013-12-04 07:57:11',1020),(1576,1,28,'2013-12-04 07:57:35',1020),(1577,1,29,'2013-12-04 07:57:39',1020),(1578,1,28,'2013-12-04 07:58:12',1020),(1579,1,29,'2013-12-04 07:58:18',1020),(1580,1,28,'2013-12-04 07:59:03',1020),(1581,1,29,'2013-12-04 08:02:27',1020),(1582,1,28,'2013-12-04 08:02:39',1020),(1583,1,29,'2013-12-04 08:02:43',1020),(1584,1,29,'2013-12-04 10:26:50',1020),(1585,1,29,'2013-12-04 10:39:56',1020),(1586,1,29,'2013-12-04 10:45:53',1020),(1587,1,29,'2013-12-04 10:50:29',1020),(1588,1,29,'2013-12-04 10:54:15',1020),(1589,1,29,'2013-12-04 10:54:44',1020),(1590,1,28,'2013-12-04 10:54:56',1020),(1591,1,29,'2013-12-04 10:55:15',1020),(1592,1,29,'2013-12-04 10:58:17',1020),(1593,1,28,'2013-12-04 10:58:28',1020),(1594,1,29,'2013-12-04 11:00:37',1020),(1595,1,29,'2013-12-04 14:23:26',1020),(1596,1,29,'2013-12-04 14:35:55',1008),(1597,1,31,'2013-12-04 14:36:00',1008),(1598,1,26,'2013-12-04 14:37:03',1008),(1599,1,29,'2013-12-08 08:32:12',1008),(1600,1,28,'2013-12-08 08:32:22',1008),(1601,1,29,'2013-12-08 08:32:26',1008),(1602,1,28,'2013-12-08 08:32:34',1008),(1603,1,29,'2013-12-08 08:32:52',1008),(1604,1,26,'2013-12-08 08:33:39',1008),(1605,1,29,'2013-12-08 08:52:27',1008),(1606,1,29,'2013-12-08 08:56:17',1008),(1607,1,28,'2013-12-08 08:56:23',1008),(1608,1,30,'2013-12-08 08:56:29',1008),(1609,1,31,'2013-12-08 08:58:29',1008),(1610,1,26,'2013-12-08 08:59:23',1008),(1611,1,29,'2013-12-08 09:00:09',1008),(1612,1,29,'2013-12-08 09:00:37',1020),(1613,1,28,'2013-12-08 09:00:47',1020),(1614,1,29,'2013-12-08 09:00:49',1020),(1615,1,28,'2013-12-08 09:01:01',1020),(1616,1,29,'2013-12-08 09:01:04',1020),(1617,1,28,'2013-12-08 09:01:08',1020),(1618,1,29,'2013-12-08 09:01:12',1020),(1619,1,29,'2013-12-08 09:02:01',1020),(1620,1,29,'2013-12-08 09:02:21',1020),(1621,1,31,'2013-12-08 15:12:11',1021),(1622,1,30,'2013-12-08 15:12:22',1021),(1623,1,31,'2013-12-08 15:12:25',1021),(1624,1,26,'2013-12-08 15:12:28',1021),(1625,1,29,'2013-12-08 15:12:33',1021),(1626,1,26,'2013-12-08 15:13:20',1021),(1627,1,29,'2013-12-08 15:13:56',1021),(1628,1,28,'2013-12-08 15:14:13',1021),(1629,1,29,'2013-12-08 15:14:16',1021),(1630,1,30,'2013-12-08 15:14:24',1021),(1631,1,31,'2013-12-08 15:14:55',1021),(1632,1,26,'2013-12-08 15:14:58',1021),(1633,1,29,'2013-12-08 15:15:30',1017),(1634,1,28,'2013-12-08 15:15:37',1017),(1635,1,30,'2013-12-08 15:15:41',1017),(1636,1,29,'2013-12-08 15:16:09',1019),(1637,1,28,'2013-12-08 15:16:16',1019),(1638,1,30,'2013-12-08 15:16:20',1019),(1639,1,26,'2013-12-08 15:16:49',1019),(1640,1,29,'2013-12-08 15:17:28',1019),(1641,1,28,'2013-12-08 15:17:37',1019),(1642,1,29,'2013-12-08 15:17:40',1019),(1643,1,28,'2013-12-08 15:17:44',1019),(1644,1,30,'2013-12-08 15:17:49',1019),(1645,1,29,'2013-12-08 15:21:24',1022),(1646,1,28,'2013-12-08 15:21:37',1022),(1647,1,30,'2013-12-08 15:21:40',1022),(1648,1,31,'2013-12-08 15:21:50',1022),(1649,1,26,'2013-12-08 15:21:52',1022),(1650,1,30,'2013-12-08 15:21:59',1022),(1651,1,31,'2013-12-08 15:22:13',1022),(1652,1,26,'2013-12-08 15:22:15',1022),(1653,1,29,'2013-12-08 16:38:23',1021),(1654,1,26,'2013-12-08 16:38:30',1021),(1655,1,30,'2013-12-08 16:38:39',1021),(1656,1,31,'2013-12-08 16:39:33',1021),(1657,1,29,'2013-12-08 16:39:38',1021),(1658,1,31,'2013-12-08 16:40:58',1021),(1659,1,29,'2013-12-08 16:40:59',1021),(1660,1,31,'2013-12-08 16:41:17',1021),(1661,1,29,'2013-12-08 16:42:10',1021),(1662,1,28,'2013-12-08 16:42:13',1021),(1663,1,30,'2013-12-08 16:42:17',1021),(1664,1,31,'2013-12-08 16:42:36',1021),(1665,1,26,'2013-12-08 16:42:55',1021),(1666,1,30,'2013-12-08 16:44:03',1021),(1667,1,31,'2013-12-08 16:44:26',1021),(1668,1,26,'2013-12-08 16:44:30',1021),(1669,1,26,'2013-12-08 16:45:01',1021),(1670,1,30,'2013-12-08 16:45:34',1021),(1671,1,31,'2013-12-08 16:45:41',1021),(1672,1,26,'2013-12-08 16:45:45',1021),(1673,1,26,'2013-12-08 16:46:21',1007),(1674,1,30,'2013-12-08 16:46:26',1007),(1675,1,29,'2013-12-08 16:46:38',1007),(1676,1,29,'2013-12-08 16:48:21',1007),(1677,1,30,'2013-12-08 16:49:17',1008),(1678,1,31,'2013-12-08 16:49:30',1008),(1679,1,29,'2013-12-08 16:49:32',1008),(1680,1,28,'2013-12-08 16:49:34',1008),(1681,1,30,'2013-12-08 16:49:39',1008),(1682,1,31,'2013-12-08 16:49:52',1008),(1683,1,26,'2013-12-08 16:49:56',1008),(1684,1,29,'2013-12-08 17:04:14',1008),(1685,1,30,'2013-12-08 17:04:21',1008),(1686,1,31,'2013-12-08 17:04:49',1008),(1687,1,29,'2013-12-08 17:04:55',1008),(1688,1,28,'2013-12-08 17:04:59',1008),(1689,1,30,'2013-12-08 17:05:04',1008),(1690,1,31,'2013-12-08 17:07:29',1008),(1691,1,29,'2013-12-08 17:07:31',1008),(1692,1,29,'2013-12-08 17:24:33',1024),(1693,1,28,'2013-12-08 17:24:42',1024),(1694,1,30,'2013-12-08 17:24:46',1024),(1695,1,31,'2013-12-08 17:25:10',1024),(1696,1,26,'2013-12-08 17:25:21',1024),(1697,1,29,'2013-12-08 17:26:21',1024),(1698,1,28,'2013-12-08 17:26:30',1024),(1699,1,28,'2013-12-08 17:26:32',1024),(1700,1,30,'2013-12-08 17:26:35',1024),(1701,1,31,'2013-12-08 17:26:41',1024),(1702,1,29,'2013-12-09 09:19:44',1024),(1703,1,28,'2013-12-09 09:19:52',1024),(1704,1,29,'2013-12-09 09:19:55',1024),(1705,1,28,'2013-12-09 09:20:10',1024),(1706,1,30,'2013-12-09 09:20:14',1024),(1707,1,31,'2013-12-09 09:20:25',1024),(1708,1,26,'2013-12-09 09:20:27',1024),(1709,1,31,'2013-12-09 09:40:18',1024),(1710,1,30,'2013-12-09 09:40:23',1024),(1711,1,31,'2013-12-09 09:40:25',1024),(1712,1,29,'2013-12-09 09:40:26',1024),(1713,1,28,'2013-12-09 09:40:31',1024),(1714,1,29,'2013-12-09 09:40:36',1024),(1715,1,28,'2013-12-09 09:40:40',1024),(1716,1,30,'2013-12-09 09:40:44',1024),(1717,1,30,'2013-12-10 08:33:22',1021),(1718,1,31,'2013-12-10 08:33:43',1021),(1719,1,29,'2013-12-10 08:33:47',1021),(1720,1,28,'2013-12-10 08:33:51',1021),(1721,1,30,'2013-12-10 08:33:56',1021),(1722,1,31,'2013-12-10 08:34:11',1021),(1723,1,29,'2013-12-10 08:34:16',1021),(1724,1,28,'2013-12-10 08:34:19',1021),(1725,1,30,'2013-12-10 08:34:25',1021),(1726,1,31,'2013-12-10 08:34:47',1021),(1727,1,31,'2013-12-10 08:39:15',1024),(1728,1,29,'2013-12-10 08:39:18',1024),(1729,1,29,'2013-12-10 08:39:30',1024),(1730,1,31,'2013-12-10 08:39:36',1024),(1731,1,29,'2013-12-10 08:39:39',1024),(1732,1,31,'2013-12-10 08:40:01',1024),(1733,1,31,'2013-12-10 08:41:07',1024),(1734,1,30,'2013-12-10 08:41:53',1022),(1735,1,31,'2013-12-10 08:42:00',1024),(1736,1,31,'2013-12-10 08:42:02',1022),(1737,1,29,'2013-12-10 08:42:09',1024),(1738,1,29,'2013-12-10 08:42:10',1022),(1739,1,28,'2013-12-10 08:42:15',1022),(1740,1,30,'2013-12-10 08:42:19',1022),(1741,1,31,'2013-12-10 08:42:37',1022),(1742,1,26,'2013-12-10 08:42:45',1022),(1743,1,29,'2013-12-10 08:42:55',1024),(1744,1,31,'2013-12-10 08:43:04',1024),(1745,1,29,'2013-12-10 08:43:10',1022),(1746,1,31,'2013-12-10 08:43:16',1024),(1747,1,30,'2013-12-10 08:43:21',1022),(1748,1,31,'2013-12-10 08:43:37',1022),(1749,1,31,'2013-12-10 08:43:50',1014),(1750,1,29,'2013-12-10 08:43:52',1014),(1751,1,29,'2013-12-10 08:44:09',1014),(1752,1,28,'2013-12-10 08:44:21',1014),(1753,1,29,'2013-12-10 08:44:48',1014),(1754,1,28,'2013-12-10 08:44:53',1014),(1755,1,29,'2013-12-10 08:44:55',1014),(1756,1,28,'2013-12-10 08:45:00',1014),(1757,1,29,'2013-12-10 08:45:03',1014),(1758,1,30,'2013-12-10 08:45:06',1014),(1759,1,38,'2013-12-10 08:45:24',1014),(1760,1,31,'2013-12-10 08:45:29',1014),(1761,1,26,'2013-12-10 08:45:32',1014),(1762,1,29,'2013-12-10 08:46:07',1022),(1763,1,28,'2013-12-10 08:46:20',1022),(1764,1,30,'2013-12-10 08:46:23',1022),(1765,1,30,'2013-12-10 09:02:32',1014),(1766,1,31,'2013-12-10 10:05:04',1022),(1767,1,26,'2013-12-10 10:05:15',1022),(1768,1,31,'2013-12-10 10:30:03',1022),(1769,1,26,'2013-12-10 10:30:07',1022),(1770,1,29,'2013-12-11 08:10:58',1024),(1771,1,29,'2013-12-11 08:17:23',1024),(1772,1,29,'2013-12-11 08:22:05',1024),(1773,1,29,'2013-12-11 08:35:21',1024),(1774,1,29,'2013-12-11 08:38:36',1024),(1775,1,29,'2013-12-11 08:44:38',1024),(1776,1,28,'2013-12-11 08:44:56',1024),(1777,1,29,'2013-12-11 12:51:51',1024),(1778,1,29,'2013-12-12 08:14:36',1024),(1779,1,29,'2013-12-12 08:21:19',1024),(1780,1,28,'2013-12-12 08:21:28',1024),(1781,1,29,'2013-12-12 11:07:03',1024),(1782,1,29,'2013-12-12 11:10:06',1024),(1783,1,29,'2013-12-12 11:38:07',1024),(1784,1,29,'2013-12-12 11:42:30',1024),(1785,1,28,'2013-12-12 11:42:41',1024),(1786,1,29,'2013-12-12 11:46:53',1024),(1787,1,28,'2013-12-12 11:46:57',1024),(1788,1,29,'2013-12-12 11:50:54',1024),(1789,1,28,'2013-12-12 11:50:57',1024),(1790,1,29,'2013-12-12 11:51:32',1024),(1791,1,28,'2013-12-12 11:51:41',1024),(1792,1,30,'2013-12-12 11:51:49',1024),(1793,1,31,'2013-12-12 11:51:58',1024),(1794,1,26,'2013-12-12 11:52:10',1024),(1795,1,29,'2013-12-15 09:56:31',1024),(1796,1,28,'2013-12-15 09:56:39',1024),(1797,1,30,'2013-12-15 09:56:47',1024),(1798,1,31,'2013-12-15 09:57:00',1024),(1799,1,29,'2013-12-15 10:20:04',1024),(1800,1,28,'2013-12-15 10:20:12',1024),(1801,1,30,'2013-12-15 10:20:19',1024),(1802,1,31,'2013-12-15 10:20:32',1024),(1803,1,30,'2013-12-15 12:36:42',1024),(1804,1,31,'2013-12-16 07:38:37',1024),(1805,1,30,'2013-12-17 14:12:03',1024),(1806,1,31,'2013-12-17 14:12:10',1024),(1807,1,29,'2013-12-17 14:14:23',1024),(1808,1,28,'2013-12-17 14:14:28',1024),(1809,1,30,'2013-12-17 14:14:59',1024),(1810,1,31,'2013-12-17 14:15:44',1024),(1811,1,31,'2013-12-17 14:27:19',1024),(1812,1,26,'2013-12-17 14:27:37',1024),(1813,1,30,'2013-12-17 17:27:27',1024),(1814,1,31,'2013-12-17 17:27:35',1024),(1815,1,29,'2013-12-17 17:27:47',1024),(1816,1,28,'2013-12-17 17:27:52',1024),(1817,1,30,'2013-12-17 17:27:58',1024),(1818,1,31,'2013-12-17 17:28:12',1024),(1819,1,26,'2013-12-17 17:28:18',1024),(1820,1,30,'2013-12-18 12:18:17',1024),(1821,1,31,'2013-12-18 12:18:26',1024),(1822,1,29,'2013-12-18 12:18:30',1024),(1823,1,28,'2013-12-18 12:18:35',1024),(1824,1,30,'2013-12-18 12:18:39',1024),(1825,1,31,'2013-12-18 12:18:58',1024),(1826,1,26,'2013-12-18 12:19:07',1024),(1827,1,30,'2013-12-18 12:33:04',1024),(1828,1,31,'2013-12-18 12:33:09',1024),(1829,1,29,'2013-12-18 12:33:12',1024),(1830,1,29,'2013-12-18 12:33:41',1024),(1831,1,28,'2013-12-18 12:33:44',1024),(1832,1,30,'2013-12-18 12:33:48',1024),(1833,1,31,'2013-12-18 12:33:58',1024),(1834,1,26,'2013-12-18 12:34:03',1024),(1835,1,29,'2013-12-18 14:48:59',1024),(1836,1,28,'2013-12-18 14:49:06',1024),(1837,1,30,'2013-12-18 14:49:16',1024),(1838,1,31,'2013-12-18 14:49:28',1024),(1839,1,29,'2013-12-18 14:49:44',1024),(1840,1,28,'2013-12-18 14:49:52',1024),(1841,1,30,'2013-12-18 14:50:00',1024),(1842,1,31,'2013-12-18 14:50:20',1024),(1843,1,29,'2013-12-18 14:50:25',1024),(1844,1,28,'2013-12-18 14:50:39',1024),(1845,1,30,'2013-12-18 14:50:50',1024),(1846,1,31,'2013-12-18 14:51:02',1024),(1847,1,29,'2013-12-18 14:51:04',1024),(1848,1,31,'2013-12-18 14:51:30',1024),(1849,1,29,'2013-12-18 14:51:33',1024),(1850,1,28,'2013-12-18 14:51:52',1024),(1851,1,30,'2013-12-18 14:52:06',1024),(1852,1,31,'2013-12-18 14:52:21',1024),(1853,1,30,'2013-12-18 14:52:42',1021),(1854,1,31,'2013-12-18 14:52:51',1021),(1855,1,29,'2013-12-18 14:52:54',1021),(1856,1,28,'2013-12-18 14:53:04',1021),(1857,1,30,'2013-12-18 14:53:11',1021),(1858,1,31,'2013-12-18 14:53:18',1021),(1859,1,29,'2013-12-18 14:53:42',1006),(1860,1,30,'2013-12-18 14:53:47',1006),(1861,1,38,'2013-12-18 14:53:57',1006),(1862,1,31,'2013-12-18 14:54:04',1006),(1863,1,29,'2013-12-18 14:54:06',1006),(1864,1,28,'2013-12-18 14:54:10',1006),(1865,1,30,'2013-12-18 14:54:14',1006),(1866,1,38,'2013-12-18 14:54:21',1006),(1867,1,31,'2013-12-18 14:54:25',1006),(1868,1,30,'2013-12-18 17:01:43',1024),(1869,1,31,'2013-12-18 17:01:48',1024),(1870,1,29,'2013-12-18 17:01:52',1024),(1871,1,28,'2013-12-18 17:01:58',1024),(1872,1,30,'2013-12-18 17:02:04',1024),(1873,1,31,'2013-12-18 17:02:10',1024),(1874,1,30,'2013-12-18 17:02:25',1006),(1875,1,38,'2013-12-18 17:02:28',1006),(1876,1,31,'2013-12-18 17:02:32',1006),(1877,1,30,'2013-12-18 17:13:54',1024),(1878,1,31,'2013-12-18 17:14:12',1024),(1879,1,29,'2013-12-18 17:14:14',1024),(1880,1,28,'2013-12-18 17:14:21',1024),(1881,1,30,'2013-12-19 09:36:42',1008),(1882,1,31,'2013-12-19 09:38:03',1008),(1883,1,29,'2013-12-19 09:38:42',1008),(1884,1,29,'2013-12-19 09:39:35',1008),(1885,1,28,'2013-12-19 09:39:43',1008),(1886,1,30,'2013-12-19 09:39:53',1008),(1887,1,29,'2013-12-19 09:40:16',1008),(1888,1,31,'2013-12-19 09:40:42',1008),(1889,1,30,'2013-12-19 11:32:23',1021),(1890,1,30,'2013-12-19 11:33:13',1006),(1891,1,38,'2013-12-19 11:33:20',1006),(1892,1,31,'2013-12-19 11:33:27',1006),(1893,1,30,'2013-12-19 11:33:30',1006),(1894,1,30,'2013-12-19 11:39:40',1021),(1895,1,31,'2013-12-19 11:40:35',1021),(1896,1,29,'2013-12-19 11:42:31',1025),(1897,1,29,'2013-12-19 11:44:36',1025),(1898,1,28,'2013-12-19 11:44:48',1025),(1899,1,30,'2013-12-19 11:44:55',1025),(1900,1,38,'2013-12-19 11:45:30',1025),(1901,1,38,'2013-12-19 11:49:26',1025),(1902,1,38,'2013-12-19 12:08:15',1025),(1903,1,30,'2013-12-19 12:08:40',1008),(1904,1,31,'2013-12-19 12:09:39',1008),(1905,1,29,'2013-12-19 12:09:44',1008),(1906,1,28,'2013-12-19 12:09:49',1008),(1907,1,30,'2013-12-19 12:09:54',1008),(1908,1,31,'2013-12-19 12:10:21',1008),(1909,1,26,'2013-12-19 12:10:25',1008),(1910,1,38,'2013-12-19 12:24:16',1025),(1911,1,30,'2013-12-19 12:24:33',1008),(1912,1,29,'2013-12-19 12:27:24',1008),(1913,1,31,'2013-12-19 12:27:45',1008),(1914,1,29,'2013-12-19 12:27:47',1008),(1915,1,28,'2013-12-19 12:27:51',1008),(1916,1,30,'2013-12-19 12:28:08',1008),(1917,1,29,'2013-12-19 12:28:32',1008),(1918,1,31,'2013-12-19 12:28:42',1008),(1919,1,30,'2013-12-19 12:28:53',1008),(1920,1,31,'2013-12-19 12:29:25',1008),(1921,1,26,'2013-12-19 12:29:41',1008),(1922,1,29,'2013-12-19 12:39:36',1008),(1923,1,29,'2013-12-19 12:42:16',1008),(1924,1,29,'2013-12-19 12:45:19',1008),(1925,1,28,'2013-12-19 12:45:27',1008),(1926,1,29,'2013-12-19 12:45:34',1008),(1927,1,30,'2013-12-19 12:45:51',1008),(1928,1,31,'2013-12-19 14:23:21',1008),(1929,1,26,'2013-12-19 14:23:46',1008),(1930,1,30,'2013-12-25 13:46:27',1008),(1931,1,31,'2013-12-25 13:51:18',1008),(1932,1,29,'2013-12-25 13:51:20',1008),(1933,1,28,'2013-12-25 13:51:24',1008),(1934,1,30,'2013-12-25 13:51:27',1008),(1935,1,31,'2013-12-25 13:51:56',1008),(1936,1,30,'2013-12-25 13:57:04',1008),(1937,1,31,'2013-12-25 13:57:10',1008),(1938,1,29,'2013-12-25 13:57:12',1008),(1939,1,28,'2013-12-25 13:57:16',1008),(1940,1,30,'2013-12-25 13:57:21',1008),(1941,1,31,'2013-12-25 13:57:56',1008),(1942,1,26,'2013-12-25 13:58:15',1008),(1943,1,29,'2013-12-25 14:07:12',1008),(1944,1,28,'2013-12-25 14:07:15',1008),(1945,1,30,'2013-12-25 14:07:19',1008),(1946,1,31,'2013-12-25 14:45:31',1008),(1947,1,30,'2013-12-25 14:46:34',1008),(1948,1,31,'2013-12-25 16:40:08',1008),(1949,1,29,'2013-12-25 16:59:30',1008),(1950,1,28,'2013-12-25 16:59:33',1008),(1951,1,30,'2013-12-25 16:59:38',1008),(1952,1,31,'2013-12-25 16:59:42',1008),(1953,1,29,'2013-12-26 07:45:54',1008),(1954,1,28,'2013-12-26 07:45:58',1008),(1955,1,30,'2013-12-26 07:46:03',1008),(1956,1,31,'2013-12-26 07:46:38',1008),(1957,1,26,'2013-12-26 07:46:50',1008),(1958,1,29,'2013-12-26 12:06:05',1008),(1959,1,30,'2013-12-26 12:17:56',1008),(1960,1,31,'2013-12-26 12:18:02',1008),(1961,1,29,'2013-12-26 12:18:04',1008),(1962,1,28,'2013-12-26 12:18:08',1008),(1963,1,30,'2013-12-26 12:18:13',1008),(1964,1,31,'2013-12-26 12:18:53',1008),(1965,1,26,'2013-12-26 12:19:18',1008),(1966,1,29,'2013-12-26 13:22:49',1026),(1967,1,28,'2013-12-26 13:23:14',1026),(1968,1,30,'2013-12-26 13:23:28',1026),(1969,1,38,'2013-12-26 13:23:41',1026),(1970,1,31,'2013-12-26 13:24:17',1026),(1971,1,26,'2013-12-26 13:24:21',1026),(1972,1,29,'2013-12-26 13:24:43',1026),(1973,1,28,'2013-12-26 13:25:15',1026),(1974,1,30,'2013-12-26 13:29:05',1026),(1975,1,31,'2013-12-26 13:29:22',1026),(1976,1,29,'2013-12-26 13:29:24',1026),(1977,1,28,'2013-12-26 13:29:30',1026),(1978,1,30,'2013-12-26 13:29:35',1026),(1979,1,38,'2013-12-26 13:31:46',1026),(1980,1,31,'2013-12-26 13:33:35',1026),(1981,1,30,'2013-12-26 13:40:31',1026),(1982,1,31,'2013-12-26 13:40:57',1026),(1983,1,29,'2013-12-29 13:15:21',1008),(1984,1,28,'2013-12-29 13:15:25',1008),(1985,1,30,'2013-12-29 13:15:30',1008),(1986,1,31,'2013-12-29 13:15:42',1008),(1987,1,29,'2013-12-30 09:40:43',1026),(1988,1,29,'2013-12-30 10:26:49',1026),(1989,1,29,'2013-12-30 10:29:48',1026),(1990,1,28,'2013-12-30 10:30:29',1026),(1991,1,29,'2013-12-30 10:30:46',1026),(1992,1,29,'2013-12-30 10:33:49',1026),(1993,1,29,'2013-12-30 10:37:29',1026),(1994,1,29,'2013-12-30 10:38:27',1026),(1995,1,29,'2013-12-30 10:39:44',1026),(1996,1,29,'2013-12-30 10:50:06',1026),(1997,1,29,'2013-12-30 10:50:51',1026),(1998,1,28,'2013-12-30 10:51:28',1026),(1999,1,29,'2013-12-30 10:59:57',1026),(2000,1,29,'2013-12-30 11:00:00',1026),(2001,1,29,'2013-12-30 11:08:08',1026),(2002,1,30,'2013-12-30 11:08:55',1026),(2003,1,31,'2013-12-30 11:57:20',1026),(2004,1,26,'2013-12-30 11:57:24',1026),(2005,1,26,'2013-12-30 12:03:40',1026),(2006,1,26,'2013-12-30 12:13:10',1026),(2007,1,26,'2013-12-30 12:16:27',1026),(2008,1,26,'2013-12-30 13:33:41',1026),(2009,1,26,'2013-12-30 14:22:18',1026),(2010,1,26,'2013-12-30 14:35:41',1026),(2011,1,26,'2013-12-30 14:37:23',1026),(2012,1,26,'2013-12-30 14:41:54',1026),(2013,1,26,'2013-12-30 15:05:46',1026),(2014,1,26,'2013-12-30 15:07:02',1026),(2015,1,26,'2013-12-30 15:14:33',1026),(2016,1,26,'2013-12-30 15:23:08',1026),(2017,1,26,'2013-12-30 15:23:43',1026),(2018,1,26,'2013-12-30 15:24:09',1026),(2019,1,29,'2013-12-30 15:27:46',1026),(2020,1,29,'2013-12-30 15:29:05',1026),(2021,1,30,'2013-12-30 15:29:12',1026),(2022,1,31,'2013-12-30 15:43:17',1026),(2023,1,26,'2013-12-30 15:43:27',1026),(2024,1,29,'2013-12-30 15:44:29',1026),(2025,1,29,'2013-12-30 15:56:12',1026),(2026,1,30,'2013-12-30 15:56:24',1026),(2027,1,26,'2013-12-30 15:57:54',1026),(2028,1,30,'2013-12-30 15:58:42',1026),(2029,1,31,'2013-12-30 15:59:14',1026),(2030,1,26,'2013-12-30 15:59:19',1026),(2031,1,26,'2013-12-30 16:32:40',1026),(2032,1,26,'2013-12-30 16:36:45',1026),(2033,1,29,'2013-12-31 08:22:12',1026),(2034,1,30,'2013-12-31 08:22:42',1026),(2035,1,31,'2013-12-31 08:22:58',1026),(2036,1,26,'2013-12-31 08:23:18',1026),(2037,1,30,'2013-12-31 08:25:14',1026),(2038,1,31,'2013-12-31 08:25:46',1026),(2039,1,29,'2014-01-02 14:33:38',1026),(2040,1,28,'2014-01-02 14:33:47',1026),(2041,1,30,'2014-01-02 14:33:51',1026),(2042,1,29,'2014-01-02 14:34:11',1027),(2043,1,28,'2014-01-02 14:34:19',1027),(2044,1,30,'2014-01-02 14:34:24',1027),(2045,1,29,'2014-01-02 14:34:55',1020),(2046,1,28,'2014-01-02 14:35:04',1020),(2047,1,30,'2014-01-02 14:35:11',1020),(2048,1,29,'2014-01-02 14:35:30',1028),(2049,1,31,'2014-01-02 14:35:55',1028),(2050,1,29,'2014-01-02 14:35:57',1028),(2051,1,28,'2014-01-02 14:36:00',1028),(2052,1,30,'2014-01-02 14:36:04',1028),(2053,1,31,'2014-01-02 14:36:45',1027),(2054,1,26,'2014-01-02 14:37:13',1027),(2055,1,31,'2014-01-02 14:38:43',1026),(2056,1,26,'2014-01-02 14:38:59',1026),(2057,1,31,'2014-01-02 14:41:03',1028),(2058,1,26,'2014-01-02 14:41:07',1028),(2059,1,31,'2014-01-02 14:42:23',1020),(2060,1,26,'2014-01-02 14:42:29',1020),(2061,1,29,'2014-01-02 14:50:59',1020),(2062,1,26,'2014-01-02 14:53:37',1028),(2063,1,26,'2014-01-02 15:12:47',1020),(2064,1,26,'2014-01-02 15:18:32',1020),(2065,1,29,'2014-01-05 17:16:45',1008),(2066,1,28,'2014-01-05 17:16:52',1008),(2067,1,30,'2014-01-05 17:17:09',1008),(2068,1,29,'2014-01-05 17:18:39',1008),(2069,1,28,'2014-01-05 17:18:44',1008),(2070,1,29,'2014-01-05 17:19:08',1008),(2071,1,28,'2014-01-05 17:19:24',1008),(2072,1,31,'2014-01-05 17:21:29',1008),(2073,1,30,'2014-01-06 14:35:19',1026),(2074,1,30,'2014-01-06 15:18:52',1028),(2075,1,31,'2014-01-06 15:19:00',1028),(2076,1,29,'2014-01-06 15:19:03',1028),(2077,1,28,'2014-01-06 15:19:14',1028),(2078,1,30,'2014-01-06 15:19:30',1028),(2079,1,31,'2014-01-06 15:20:50',1028),(2080,1,26,'2014-01-06 15:20:57',1028),(2081,1,31,'2014-01-06 15:25:42',1026),(2082,1,26,'2014-01-06 15:25:46',1026),(2083,1,30,'2014-01-06 15:27:59',1029),(2084,1,29,'2014-01-06 15:28:16',1029),(2085,1,38,'2014-01-06 15:31:11',1029),(2086,1,31,'2014-01-06 15:42:13',1029),(2087,1,26,'2014-01-06 15:42:17',1029),(2088,1,29,'2014-01-06 15:53:04',1030),(2089,1,28,'2014-01-06 15:53:33',1030),(2090,1,30,'2014-01-06 15:53:40',1030),(2091,1,38,'2014-01-06 15:53:43',1030),(2092,1,31,'2014-01-06 16:53:46',1030),(2093,1,26,'2014-01-06 16:53:51',1030),(2094,1,26,'2014-01-06 16:54:13',1030),(2095,1,26,'2014-01-06 17:05:59',1029),(2096,1,30,'2014-01-06 17:06:54',1029),(2097,1,38,'2014-01-06 17:06:59',1029),(2098,1,38,'2014-01-06 17:07:02',1029),(2099,1,31,'2014-01-06 17:07:06',1029),(2100,1,26,'2014-01-06 17:07:08',1029),(2101,1,29,'2014-01-06 17:08:41',1029),(2102,1,29,'2014-01-06 17:12:27',1029),(2103,1,29,'2014-01-06 17:14:17',1029),(2104,1,26,'2014-01-06 17:16:38',1030),(2105,1,30,'2014-01-06 17:18:58',1030),(2106,1,30,'2014-01-06 17:22:55',1028),(2107,1,30,'2014-01-06 17:24:40',1028),(2108,1,31,'2014-01-06 17:24:54',1028),(2109,1,30,'2014-01-06 17:25:17',1028),(2110,1,31,'2014-01-06 17:26:23',1028),(2111,1,31,'2014-01-07 09:17:09',1030),(2112,1,26,'2014-01-07 09:17:15',1030),(2113,1,26,'2014-01-07 10:05:45',1030),(2114,1,26,'2014-01-07 10:08:42',1030),(2115,1,30,'2014-02-05 14:50:53',1031),(2116,1,31,'2014-02-05 14:53:28',1031),(2117,1,26,'2014-02-19 08:44:26',1032),(2118,1,29,'2014-02-19 08:45:13',1032),(2119,1,29,'2014-02-19 14:05:32',1033),(2120,1,29,'2014-02-19 14:10:26',1033),(2121,1,29,'2014-02-19 14:10:29',1033),(2122,1,29,'2014-02-19 14:37:29',1033),(2123,1,29,'2014-02-19 14:43:19',1033),(2124,1,28,'2014-02-19 14:44:48',1033),(2125,1,29,'2014-02-19 14:44:57',1033),(2126,1,29,'2014-02-19 15:00:53',1033),(2127,1,28,'2014-02-19 15:03:32',1033),(2128,1,29,'2014-02-19 15:05:08',1033),(2129,1,29,'2014-02-19 15:05:52',1033),(2130,1,28,'2014-02-19 15:06:01',1033),(2131,1,29,'2014-02-24 08:45:40',1031),(2132,1,29,'2014-02-24 08:45:53',1031),(2133,1,29,'2014-02-24 08:47:02',1031),(2134,1,29,'2014-02-24 08:50:06',1031),(2135,1,29,'2014-02-24 08:54:47',1031),(2136,1,28,'2014-02-24 08:55:19',1031),(2137,1,30,'2014-02-24 09:00:18',1031),(2138,1,29,'2014-02-24 09:02:12',1031),(2139,1,30,'2014-02-24 09:02:25',1031),(2140,1,31,'2014-02-24 09:04:59',1031),(2141,1,26,'2014-02-24 09:05:17',1031),(2142,1,26,'2014-02-24 09:12:35',1031),(2143,1,26,'2014-02-24 09:29:10',1031),(2144,1,26,'2014-02-24 09:37:34',1031),(2145,1,26,'2014-02-24 09:47:43',1031),(2146,1,26,'2014-02-24 09:50:05',1031),(2147,1,26,'2014-02-24 09:52:30',1031),(2148,1,26,'2014-02-24 10:00:42',1031),(2149,1,26,'2014-02-24 10:08:37',1031),(2150,1,29,'2014-02-24 15:43:51',1031),(2151,1,28,'2014-02-24 15:44:17',1031),(2152,1,30,'2014-02-24 15:44:23',1031),(2153,1,31,'2014-02-24 15:48:42',1031),(2154,1,26,'2014-02-24 15:48:49',1031),(2155,1,29,'2014-03-10 17:19:03',1035),(2156,1,29,'2014-03-12 07:54:21',1031),(2157,1,26,'2014-03-12 07:54:37',1031),(2158,1,29,'2014-03-12 07:55:06',1031),(2159,1,29,'2014-03-12 07:55:39',1036),(2160,1,26,'2014-03-12 07:55:54',1036),(2161,1,29,'2014-03-12 07:56:00',1036),(2162,1,29,'2014-03-12 07:56:12',1036),(2163,1,31,'2014-03-23 14:12:31',0),(2164,1,30,'2014-03-23 14:15:45',0),(2165,1,31,'2014-03-23 14:24:09',0),(2166,1,26,'2014-03-31 08:28:05',0),(2167,1,26,'2014-03-31 17:24:17',1037),(2168,1,26,'2014-03-31 17:25:51',1031),(2169,1,26,'2014-03-31 17:26:50',1038),(2170,1,26,'2014-03-31 17:29:01',1038),(2171,1,26,'2014-03-31 17:34:11',1038),(2172,1,26,'2014-04-01 08:05:09',1038),(2173,1,26,'2014-04-01 08:05:22',1031),(2174,1,26,'2014-04-01 08:05:33',1037),(2175,1,26,'2014-04-01 08:14:47',1038),(2176,1,26,'2014-04-01 08:14:52',1037),(2177,1,26,'2014-04-01 08:14:59',1037),(2178,1,26,'2014-04-01 08:15:03',1031),(2179,1,29,'2014-04-01 08:15:19',1038),(2180,1,26,'2014-04-01 08:15:28',1038),(2181,1,30,'2014-04-01 17:28:44',1038),(2182,1,31,'2014-04-01 17:29:28',1038),(2183,1,30,'2014-04-01 17:29:56',1031),(2184,1,31,'2014-04-01 17:30:15',1031),(2185,1,26,'2014-04-02 07:57:51',1031),(2186,1,30,'2014-04-02 07:59:34',1031),(2187,1,31,'2014-04-02 08:02:38',1031),(2188,1,30,'2014-04-02 08:03:02',1031),(2189,1,31,'2014-04-02 08:03:35',1031),(2190,1,30,'2014-04-02 08:04:13',1037),(2191,1,31,'2014-04-02 08:04:49',1037),(2192,1,29,'2014-04-02 08:04:53',1037),(2193,1,29,'2014-04-02 08:05:46',1037),(2194,1,29,'2014-04-02 08:09:07',1037),(2195,1,29,'2014-04-02 08:10:10',1037),(2196,1,29,'2014-04-02 08:27:19',1031),(2197,1,28,'2014-04-02 08:32:00',1031),(2198,1,29,'2014-04-02 08:39:39',1031),(2199,1,28,'2014-04-02 08:40:10',1031),(2200,1,29,'2014-04-02 08:40:15',1031),(2201,1,30,'2014-04-02 08:40:38',1031),(2202,1,31,'2014-04-02 08:40:54',1031),(2203,1,30,'2014-04-02 08:46:40',1031),(2204,1,30,'2014-04-02 08:48:33',1031),(2205,1,31,'2014-04-02 08:48:51',1031),(2206,1,30,'2014-04-02 09:10:48',1031),(2207,1,31,'2014-04-02 09:10:56',1031),(2208,1,30,'2014-04-02 09:13:09',1031),(2209,1,31,'2014-04-02 09:24:23',1031),(2210,1,29,'2014-04-02 09:24:38',1031),(2211,1,28,'2014-04-02 09:24:53',1031),(2212,1,30,'2014-04-02 09:25:01',1031),(2213,1,31,'2014-04-02 09:25:14',1031),(2214,1,30,'2014-04-02 09:56:46',1031),(2215,1,31,'2014-04-02 09:56:53',1031),(2216,1,30,'2014-04-02 14:21:25',1031),(2217,1,31,'2014-04-02 14:21:34',1031),(2218,1,29,'2014-04-02 14:21:41',1031),(2219,1,28,'2014-04-02 14:22:05',1031),(2220,1,30,'2014-04-02 14:22:12',1031),(2221,1,31,'2014-04-02 14:22:23',1031),(2222,1,30,'2014-04-02 14:23:43',1031),(2223,1,31,'2014-04-02 14:23:59',1031),(2224,1,26,'2014-04-02 15:02:20',1031),(2225,1,29,'2014-04-03 09:17:59',1031),(2226,1,29,'2014-04-03 09:19:14',1024),(2227,1,29,'2014-04-03 09:19:33',1026),(2228,1,29,'2014-04-03 09:20:45',1038),(2229,1,29,'2014-04-03 09:21:06',1039),(2230,1,29,'2014-04-03 09:22:21',1039),(2231,1,29,'2014-04-03 09:23:51',1039),(2232,1,30,'2014-04-03 09:24:14',1039),(2233,1,31,'2014-04-03 09:25:01',1039),(2234,1,26,'2014-04-06 11:31:40',1040),(2235,1,29,'2014-04-06 11:32:21',1040),(2236,1,29,'2014-04-10 10:44:46',1024),(2237,1,31,'2014-04-17 16:52:00',1043),(2238,1,29,'2014-04-17 16:52:04',1043),(2239,1,28,'2014-04-17 16:52:48',1043),(2240,1,29,'2014-04-17 16:53:04',1043),(2241,1,28,'2014-04-17 16:53:16',1043),(2242,1,30,'2014-04-17 16:53:21',1043),(2243,1,29,'2014-04-22 08:09:41',1031),(2244,1,30,'2014-04-22 08:10:04',1031),(2245,1,31,'2014-04-22 08:10:24',1031),(2246,1,29,'2014-04-22 08:11:22',1031),(2247,1,28,'2014-04-22 08:12:28',1031),(2248,1,30,'2014-04-22 08:12:36',1031),(2249,1,31,'2014-04-22 08:13:59',1031),(2250,1,29,'2014-04-22 08:14:01',1031),(2251,1,28,'2014-04-22 08:14:19',1031),(2252,1,30,'2014-04-22 08:14:25',1031),(2253,1,29,'2014-04-22 13:59:49',1031),(2254,1,29,'2014-04-22 14:41:32',1031),(2255,1,29,'2014-04-23 13:53:48',1031),(2256,1,31,'2014-04-23 13:59:29',1031),(2257,1,26,'2014-04-23 13:59:53',1031),(2258,1,29,'2014-04-23 13:59:59',1031),(2259,1,28,'2014-04-23 14:01:02',1031),(2260,1,30,'2014-04-23 14:01:17',1031),(2261,1,29,'2014-04-27 14:46:54',1041),(2262,1,29,'2014-04-27 14:47:29',1041),(2263,1,29,'2014-04-27 14:50:43',1024),(2264,1,29,'2014-04-27 14:51:37',1024),(2265,1,26,'2014-04-27 14:54:55',1041),(2266,1,26,'2014-04-27 14:58:39',1041),(2267,1,26,'2014-04-27 15:00:25',1031),(2268,1,30,'2014-05-22 09:40:22',1045),(2269,1,31,'2014-05-22 09:40:37',1045),(2270,1,30,'2014-05-22 09:41:38',1045),(2271,1,38,'2014-05-22 09:41:48',1045),(2272,1,38,'2014-05-22 09:41:53',1045),(2273,1,30,'2014-05-28 16:39:51',1046),(2274,1,31,'2014-05-28 16:40:01',1046),(2275,1,26,'2014-05-28 17:10:51',1046),(2276,1,26,'2014-05-28 17:15:03',1046),(2277,1,30,'2014-06-15 07:59:29',1065),(2278,1,30,'2014-06-15 08:02:15',1046),(2279,1,31,'2014-06-15 08:06:21',1046),(2280,1,30,'2014-06-15 08:38:04',1046),(2281,1,31,'2014-06-15 08:49:36',1046),(2282,1,30,'2014-06-15 09:00:39',1046),(2283,1,31,'2014-06-15 09:01:20',1046),(2284,1,28,'2014-07-15 17:18:53',1075),(2285,1,28,'2014-07-15 17:21:54',1075),(2286,1,28,'2014-07-16 11:24:14',1075),(2287,1,28,'2014-07-16 11:25:06',1075),(2288,1,28,'2014-07-16 11:28:32',1075),(2289,1,28,'2014-07-16 11:29:10',1075),(2290,1,28,'2014-07-16 11:35:17',1075),(2291,1,28,'2014-07-16 11:36:27',1075),(2292,1,28,'2014-07-16 11:42:34',1075),(2293,1,28,'2014-07-16 11:43:54',1075),(2294,1,28,'2014-07-16 11:45:04',1075),(2295,1,28,'2014-07-16 11:45:58',1075),(2296,1,28,'2014-07-16 11:46:40',1075),(2297,1,28,'2014-07-16 11:47:03',1075),(2298,1,28,'2014-07-16 11:49:13',1075),(2299,1,28,'2014-07-16 12:05:25',1075),(2300,1,28,'2014-07-16 12:20:38',1075),(2301,1,28,'2014-07-17 09:48:31',1075),(2302,1,28,'2014-07-17 09:59:03',1075),(2303,1,28,'2014-07-17 13:25:29',1068),(2304,1,28,'2014-07-17 13:25:32',1068),(2305,1,28,'2014-07-17 13:48:04',1068),(2306,1,28,'2014-07-17 13:48:04',1068),(2307,1,28,'2014-07-17 13:51:52',1068),(2308,1,28,'2014-07-17 13:51:53',1068),(2309,1,28,'2014-07-17 14:17:15',1068),(2310,1,28,'2014-07-17 14:17:16',1068),(2311,1,28,'2014-07-17 14:17:30',1068),(2312,1,28,'2014-07-17 14:24:25',1068),(2313,1,28,'2014-07-17 15:30:08',1068),(2314,1,28,'2014-07-17 15:34:35',1068),(2315,1,28,'2014-07-17 15:35:39',1068),(2316,1,28,'2014-07-17 15:35:53',1068),(2317,1,28,'2014-07-17 16:09:17',1068),(2318,1,28,'2014-07-17 16:23:07',1068),(2319,1,28,'2014-07-17 16:24:18',1068),(2320,1,28,'2014-07-17 16:32:06',1068),(2321,1,28,'2014-07-17 16:32:28',1068),(2322,1,28,'2014-07-17 16:51:45',1068),(2323,1,28,'2014-07-17 16:52:06',1068),(2324,1,28,'2014-07-17 17:17:43',1068),(2325,1,28,'2014-07-17 17:18:25',1068),(2326,1,28,'2014-07-20 08:40:37',1068),(2327,1,28,'2014-07-20 08:41:28',1068),(2328,1,28,'2014-07-21 11:12:29',1075),(2329,1,28,'2014-07-21 11:12:31',1075),(2330,1,28,'2014-07-21 11:13:55',1075),(2331,1,28,'2014-07-21 11:17:25',1075),(2332,1,28,'2014-07-21 11:17:52',1075),(2333,1,28,'2014-07-21 11:18:43',1075),(2334,1,28,'2014-07-21 11:37:25',1075),(2335,1,28,'2014-07-21 11:55:03',1075),(2336,1,28,'2014-07-21 11:55:03',1075),(2337,1,28,'2014-07-21 11:55:32',1075),(2338,1,28,'2014-07-21 11:56:06',1075),(2339,1,28,'2014-07-21 11:56:07',1075),(2340,1,28,'2014-07-21 11:56:27',1075),(2341,1,28,'2014-07-21 11:56:29',1075),(2342,1,28,'2014-07-21 11:57:21',1075),(2343,1,28,'2014-07-21 11:58:01',1075),(2344,1,28,'2014-07-21 12:10:04',1075),(2345,1,28,'2014-07-21 14:49:44',1075),(2346,1,28,'2014-07-21 14:49:48',1075),(2347,1,28,'2014-07-21 14:50:58',1075),(2348,1,28,'2014-07-21 14:52:44',1075),(2349,1,28,'2014-07-21 14:54:13',1075),(2350,1,28,'2014-07-21 14:54:42',1075),(2351,1,28,'2014-07-21 14:55:46',1075),(2352,1,28,'2014-07-21 15:09:13',1075),(2353,1,28,'2014-07-21 15:11:49',1075),(2354,1,28,'2014-07-21 16:45:13',1075),(2355,1,28,'2014-07-21 16:45:17',1075),(2356,1,28,'2014-07-21 16:50:14',1075),(2357,1,28,'2014-07-21 17:09:52',1075),(2358,1,28,'2014-07-21 17:15:09',1075),(2359,1,28,'2014-07-21 17:15:12',1075),(2360,1,28,'2014-07-21 17:26:56',1075),(2361,1,28,'2014-07-21 17:28:02',1075),(2362,1,28,'2014-07-21 17:29:14',1075),(2363,1,28,'2014-07-22 10:48:09',1075),(2364,1,28,'2014-07-22 10:48:57',1075),(2365,1,28,'2014-07-22 10:49:11',1075),(2366,1,28,'2014-07-22 10:50:32',1075),(2367,1,28,'2014-07-22 10:50:42',1075),(2368,1,28,'2014-07-22 11:30:45',1075),(2369,1,28,'2014-07-22 12:19:42',1075),(2370,1,28,'2014-07-22 13:35:08',1075),(2371,1,28,'2014-07-22 20:09:12',1063),(2372,1,28,'2014-07-24 09:13:45',1075),(2373,1,28,'2014-07-24 09:14:05',1075),(2374,1,28,'2014-07-24 09:14:26',1075),(2375,1,28,'2014-07-24 09:17:48',1075),(2376,1,28,'2014-07-24 09:17:51',1075),(2377,1,28,'2014-07-24 09:18:37',1075),(2378,1,28,'2014-07-24 09:44:31',1075),(2379,1,28,'2014-07-24 09:44:53',1075),(2380,1,28,'2014-07-24 11:19:33',1070),(2381,1,28,'2014-07-24 11:28:59',1075),(2382,1,28,'2014-07-24 11:29:02',1075),(2383,1,28,'2014-07-24 11:29:29',1075),(2384,1,28,'2014-07-24 11:37:44',1075),(2385,1,28,'2014-07-24 11:37:47',1075),(2386,1,28,'2014-07-24 11:38:32',1075),(2387,1,28,'2014-07-24 12:09:38',1075),(2388,1,28,'2014-07-24 12:09:43',1075),(2389,1,28,'2014-07-24 12:10:57',1075),(2390,1,28,'2014-07-24 12:11:00',1075),(2391,1,28,'2014-07-27 09:22:26',1075),(2392,1,28,'2014-07-27 09:59:40',1075),(2393,1,28,'2014-07-27 09:59:42',1075),(2394,1,28,'2014-07-28 09:35:51',1070),(2395,1,28,'2014-07-28 09:35:51',1070),(2396,1,28,'2014-07-28 16:34:45',1075),(2397,1,28,'2014-07-28 16:34:46',1075),(2398,1,28,'2014-07-28 16:35:04',1075),(2399,1,28,'2014-07-28 16:37:57',1075),(2400,1,28,'2014-07-28 16:51:52',1075),(2401,1,28,'2014-07-28 17:00:15',1075),(2402,1,28,'2014-07-28 17:00:18',1075),(2403,1,28,'2014-07-29 16:55:35',1075),(2404,1,28,'2014-07-29 16:56:02',1075),(2405,1,28,'2014-07-29 16:56:04',1075),(2406,1,28,'2014-07-29 17:12:00',1070),(2407,1,28,'2014-07-29 17:12:03',1070),(2408,1,28,'2014-07-29 17:22:08',1075),(2409,1,28,'2014-07-29 17:24:38',1075),(2410,1,28,'2014-07-29 17:25:14',1075),(2411,1,28,'2014-07-29 17:25:16',1075),(2412,1,28,'2014-07-29 17:34:28',1079),(2413,1,28,'2014-07-29 18:37:50',1070),(2414,1,28,'2014-07-29 18:44:02',1079),(2415,1,28,'2014-07-29 19:04:28',1079),(2416,1,28,'2014-07-29 19:13:48',1079),(2417,1,28,'2014-07-30 09:22:02',1075),(2418,1,28,'2014-07-30 09:22:04',1075),(2419,1,28,'2014-07-30 09:35:34',1075),(2420,1,28,'2014-07-30 09:35:35',1075),(2421,1,28,'2014-07-30 09:36:41',1075),(2422,1,28,'2014-07-30 09:36:43',1075),(2423,1,28,'2014-07-30 09:49:12',1079),(2424,1,28,'2014-07-30 09:49:12',1079),(2425,1,28,'2014-07-30 09:49:34',1079),(2426,1,28,'2014-07-30 09:49:35',1079),(2427,1,28,'2014-07-30 10:16:52',1075),(2428,1,28,'2014-07-30 10:16:53',1075),(2429,1,28,'2014-07-30 10:43:11',1075),(2430,1,28,'2014-07-30 10:43:13',1075),(2431,1,28,'2014-07-30 10:43:39',1075),(2432,1,28,'2014-07-30 11:21:47',1070),(2433,1,28,'2014-07-30 11:21:47',1070),(2434,1,28,'2014-07-30 14:19:35',1079),(2435,1,28,'2014-07-30 14:19:37',1079),(2436,1,28,'2014-07-30 14:21:07',1079),(2437,1,28,'2014-07-30 14:21:08',1079),(2438,1,28,'2014-07-31 07:46:22',1094),(2439,1,28,'2014-07-31 11:20:50',1048),(2440,1,28,'2014-07-31 11:20:51',1048),(2441,1,28,'2014-07-31 11:23:53',1048),(2442,1,28,'2014-07-31 11:30:41',1048),(2443,1,28,'2014-07-31 11:30:42',1048),(2444,1,28,'2014-07-31 11:33:28',1089),(2445,1,28,'2014-07-31 11:35:39',1089),(2446,1,28,'2014-07-31 11:35:39',1089),(2447,1,28,'2014-07-31 11:39:04',1089),(2448,1,28,'2014-07-31 11:39:06',1089),(2449,1,28,'2014-07-31 11:39:30',1089),(2450,1,28,'2014-07-31 13:11:34',1079),(2451,1,28,'2014-07-31 13:11:36',1079),(2452,1,28,'2014-08-05 11:49:00',1048),(2453,1,28,'2014-08-05 11:49:01',1048),(2454,1,28,'2014-08-05 11:49:11',1048),(2455,1,28,'2014-08-05 11:49:38',1048),(2456,1,28,'2014-08-05 11:50:27',1048),(2457,1,28,'2014-08-05 11:51:07',1048),(2458,1,28,'2014-08-05 11:52:01',1048),(2459,1,28,'2014-08-07 10:56:06',1070),(2460,1,28,'2014-08-07 10:56:06',1070),(2461,1,28,'2014-08-07 11:16:49',1070),(2462,1,28,'2014-08-07 11:16:49',1070),(2463,1,28,'2014-08-07 11:56:42',1079),(2464,1,28,'2014-08-07 11:56:44',1079),(2465,1,28,'2014-08-10 09:19:25',1070),(2466,1,28,'2014-08-10 09:31:48',1070),(2467,1,28,'2014-08-10 09:33:31',1070),(2468,1,28,'2014-08-10 09:34:00',1070),(2469,1,28,'2014-08-10 09:34:00',1070),(2470,1,28,'2014-08-10 10:11:35',1047),(2471,1,28,'2014-08-10 10:23:02',1047),(2472,1,28,'2014-08-10 10:23:03',1047),(2473,1,28,'2014-08-10 10:45:53',1075),(2474,1,28,'2014-08-10 11:06:04',1075),(2475,1,28,'2014-08-10 11:12:41',1075),(2476,1,28,'2014-08-10 11:18:11',1075),(2477,1,28,'2014-08-10 11:25:49',1075),(2478,1,28,'2014-08-10 11:26:39',1075),(2479,1,28,'2014-08-10 11:33:31',1075),(2480,1,28,'2014-08-14 08:56:44',1070),(2481,1,28,'2014-08-14 08:56:46',1070),(2482,1,28,'2014-08-14 09:01:22',1070),(2483,1,28,'2014-08-14 09:03:28',1070),(2484,1,28,'2014-08-14 09:19:22',1070),(2485,1,28,'2014-08-14 09:33:28',1070),(2486,1,28,'2014-08-14 11:30:32',1047),(2487,1,28,'2014-08-14 11:30:34',1047),(2488,1,28,'2014-08-14 11:32:35',1047),(2489,1,28,'2014-08-14 11:32:36',1047),(2490,1,28,'2014-08-14 13:45:29',1075),(2491,1,28,'2014-08-14 13:49:26',1105),(2492,1,28,'2014-08-14 14:44:42',1070),(2493,1,28,'2014-08-14 14:57:42',1047),(2494,1,28,'2014-08-14 14:58:19',1047),(2495,1,28,'2014-08-14 14:58:21',1047),(2496,1,28,'2014-08-14 15:00:06',1047),(2497,1,28,'2014-08-14 15:05:05',1070),(2498,1,28,'2014-08-14 15:05:07',1070),(2499,1,28,'2014-08-14 15:06:32',1070),(2500,1,28,'2014-08-14 15:20:20',1070),(2501,1,28,'2014-08-14 15:28:29',1047),(2502,1,28,'2014-08-14 15:28:48',1047),(2503,1,28,'2014-08-14 15:28:50',1047),(2504,1,28,'2014-08-14 15:36:36',1047),(2505,1,28,'2014-08-14 15:36:37',1047),(2506,1,28,'2014-08-14 15:39:54',1047),(2507,1,28,'2014-08-14 15:39:55',1047),(2508,1,28,'2014-08-14 15:40:41',1047),(2509,1,28,'2014-08-14 15:40:42',1047),(2510,1,28,'2014-08-14 15:46:06',1047),(2511,1,28,'2014-08-14 15:46:09',1047),(2512,1,28,'2014-08-14 15:46:30',1047),(2513,1,28,'2014-08-14 15:48:45',1047),(2514,1,28,'2014-08-14 15:48:47',1047),(2515,1,28,'2014-08-18 10:54:41',1047),(2516,1,28,'2014-08-18 11:55:51',1047),(2517,1,28,'2014-08-18 11:55:53',1047),(2518,1,28,'2014-08-18 11:56:12',1047),(2519,1,28,'2014-08-18 11:56:14',1047),(2520,1,28,'2014-08-18 11:58:03',1047),(2521,1,28,'2014-08-18 11:58:05',1047),(2522,35,28,'2014-08-19 14:00:12',1104),(2523,1,28,'2014-08-21 10:03:12',1104),(2524,1,28,'2014-08-21 10:03:27',1104),(2525,1,28,'2014-08-21 10:03:28',1104),(2526,1,28,'2014-08-21 10:29:54',1111),(2527,1,28,'2014-08-21 10:30:21',1111),(2528,1,28,'2014-08-21 10:30:22',1111),(2529,1,28,'2014-08-21 10:34:51',1111),(2530,1,28,'2014-08-21 10:34:52',1111),(2531,1,28,'2014-08-21 10:35:16',1111),(2532,1,28,'2014-08-21 10:35:17',1111),(2533,1,28,'2014-08-21 10:37:53',1111),(2534,1,28,'2014-08-21 10:37:53',1111),(2535,1,28,'2014-08-21 10:38:37',1111),(2536,1,28,'2014-08-21 10:38:38',1111),(2537,1,28,'2014-08-21 10:39:02',1111),(2538,1,28,'2014-08-21 10:39:03',1111),(2539,1,28,'2014-08-21 10:41:52',1111),(2540,1,28,'2014-08-21 10:41:53',1111),(2541,1,28,'2014-08-21 10:42:15',1111),(2542,1,28,'2014-08-21 10:42:15',1111),(2543,1,28,'2014-08-21 10:43:55',1111),(2544,1,28,'2014-08-21 10:44:11',1111),(2545,1,28,'2014-08-21 10:44:12',1111),(2546,1,28,'2014-08-21 10:45:13',1111),(2547,1,28,'2014-08-21 10:45:14',1111),(2548,1,28,'2014-08-21 11:01:47',1111),(2549,1,28,'2014-08-21 11:01:48',1111),(2550,1,28,'2014-08-21 11:02:04',1111),(2551,1,28,'2014-08-21 11:02:05',1111),(2552,1,28,'2014-08-21 11:04:45',1111),(2553,1,28,'2014-08-21 11:04:45',1111),(2554,1,28,'2014-08-21 11:04:59',1111),(2555,1,28,'2014-08-21 11:05:00',1111),(2556,1,28,'2014-08-21 12:33:05',1047),(2557,1,28,'2014-08-21 12:33:20',1047),(2558,1,28,'2014-08-21 12:33:33',1047),(2559,1,28,'2014-08-21 12:36:44',1104),(2560,1,28,'2014-08-21 12:36:45',1104),(2561,1,28,'2014-08-21 12:39:06',1109),(2562,1,28,'2014-08-24 09:24:32',1111),(2563,1,28,'2014-08-24 09:24:49',1111),(2564,1,28,'2014-08-24 09:24:50',1111),(2565,1,28,'2014-08-24 16:03:42',1110),(2566,1,28,'2014-08-24 16:03:43',1110),(2567,1,28,'2014-08-26 08:27:48',1084),(2568,1,28,'2014-08-26 08:28:31',1084),(2569,1,28,'2014-08-26 08:28:32',1084),(2570,1,28,'2014-08-26 08:34:14',1084),(2571,1,28,'2014-08-26 08:35:16',1084),(2572,1,28,'2014-08-26 08:37:02',1084),(2573,1,28,'2014-08-26 08:37:03',1084),(2574,1,28,'2014-08-26 08:45:16',1084),(2575,1,28,'2014-08-26 08:48:49',1104),(2576,1,28,'2014-08-26 08:48:52',1104),(2577,1,28,'2014-08-26 08:51:49',1075),(2578,1,28,'2014-09-02 10:42:15',1112),(2579,1,28,'2014-09-02 10:42:19',1112),(2580,1,28,'2014-09-02 17:44:26',1112),(2581,1,28,'2014-09-02 17:44:32',1112),(2582,1,28,'2014-09-03 09:36:39',1112),(2583,1,28,'2014-09-03 09:36:44',1112),(2584,1,28,'2014-09-03 10:28:01',1112),(2585,1,28,'2014-09-03 10:28:28',1112),(2586,1,28,'2014-09-03 10:28:33',1112),(2587,1,28,'2014-09-03 10:29:28',1112),(2588,1,28,'2014-09-03 10:29:32',1112),(2589,1,28,'2014-09-03 11:25:47',1112),(2590,1,28,'2014-09-03 17:49:48',1118),(2591,1,28,'2014-09-03 17:51:40',1114),(2592,1,28,'2014-09-03 17:51:40',1114),(2593,1,28,'2014-09-03 17:55:20',1118),(2594,1,28,'2014-09-03 17:56:38',1114),(2595,1,28,'2014-09-03 17:56:38',1114),(2596,1,28,'2014-09-03 18:30:46',1112),(2597,1,28,'2014-09-03 18:35:46',1119),(2598,1,28,'2014-09-03 18:35:48',1119),(2599,1,28,'2014-09-03 18:36:56',1119),(2600,1,28,'2014-09-03 18:36:58',1119),(2601,1,28,'2014-09-03 18:38:09',1119),(2602,1,28,'2014-09-03 18:40:53',1119),(2603,1,28,'2014-09-03 18:41:02',1119),(2604,1,28,'2014-09-04 09:07:28',1046),(2605,1,28,'2014-09-04 09:07:36',1046),(2606,1,28,'2014-09-04 09:08:10',1112),(2607,1,28,'2014-09-04 09:08:15',1112),(2608,1,28,'2014-09-04 09:28:23',1046),(2609,1,28,'2014-09-04 09:29:04',1046),(2610,1,28,'2014-09-04 11:09:29',1046),(2611,1,28,'2014-09-04 11:18:45',1112),(2612,1,28,'2014-09-04 11:19:34',1119),(2613,1,28,'2014-09-04 11:19:35',1119),(2614,1,28,'2014-09-04 11:28:32',1112),(2615,1,28,'2014-09-04 11:28:38',1112),(2616,1,28,'2014-09-04 11:30:40',1119),(2617,1,28,'2014-09-04 11:30:45',1119),(2618,1,28,'2014-09-04 11:31:21',1119),(2619,1,28,'2014-09-04 11:34:01',1119),(2620,1,28,'2014-09-04 11:34:05',1119),(2621,1,28,'2014-09-04 11:35:20',1119),(2622,1,28,'2014-09-04 11:35:25',1119),(2623,1,28,'2014-09-04 11:35:40',1119),(2624,1,28,'2014-09-04 11:35:44',1119),(2625,1,28,'2014-09-04 12:17:39',1119),(2626,1,28,'2014-09-04 12:17:46',1119),(2627,1,28,'2014-09-04 12:19:07',1119),(2628,1,28,'2014-09-04 12:21:09',1046),(2629,1,28,'2014-09-04 12:21:09',1046),(2630,1,28,'2014-09-04 12:22:59',1046),(2631,1,28,'2014-09-04 12:23:46',1046),(2632,1,28,'2014-09-04 12:24:05',1046),(2633,1,28,'2014-09-10 10:19:45',1070),(2634,1,28,'2014-09-10 15:01:24',1112),(2635,1,28,'2014-09-10 15:01:29',1112),(2636,1,28,'2014-09-10 15:28:31',1112),(2637,1,28,'2014-09-10 15:29:00',1112),(2638,1,28,'2014-09-10 15:29:44',1112),(2639,1,28,'2014-09-10 15:36:09',1112),(2640,1,28,'2014-09-10 15:36:41',1112),(2641,1,28,'2014-09-11 12:28:20',1102),(2642,1,28,'2014-09-11 12:28:22',1102),(2643,1,28,'2014-09-11 12:30:40',1112),(2644,1,28,'2014-09-11 12:30:57',1112),(2645,1,28,'2014-09-11 12:33:18',1112),(2646,1,28,'2014-09-11 12:33:34',1112),(2647,1,28,'2014-09-11 12:33:42',1112),(2648,1,28,'2014-09-11 12:35:08',1112),(2649,1,28,'2014-09-11 12:35:30',1112),(2650,1,28,'2014-09-11 12:35:33',1112),(2651,1,28,'2014-09-11 12:36:06',1112),(2652,1,28,'2014-09-11 12:36:27',1112),(2653,1,28,'2014-09-11 13:36:53',1112),(2654,1,28,'2014-09-11 13:37:13',1112),(2655,1,28,'2014-09-11 13:37:59',1112),(2656,1,28,'2014-09-11 13:38:22',1112),(2657,1,28,'2014-09-11 13:39:38',1112),(2658,1,28,'2014-09-11 13:52:01',1118),(2659,1,28,'2014-09-11 13:52:01',1118),(2660,1,28,'2014-09-11 13:52:21',1118),(2661,1,28,'2014-09-11 14:20:29',1047),(2662,1,28,'2014-09-11 14:21:26',1047),(2663,1,28,'2014-09-11 14:21:28',1047),(2664,1,28,'2014-09-11 15:09:09',1047),(2665,1,28,'2014-09-11 15:25:47',1048),(2666,1,28,'2014-09-11 15:25:49',1048),(2667,1,28,'2014-09-11 15:26:01',1048),(2668,1,28,'2014-09-11 15:26:11',1048),(2669,1,28,'2014-09-11 15:26:19',1048),(2670,1,28,'2014-09-11 15:53:58',1048),(2671,1,28,'2014-09-11 15:54:01',1048),(2672,1,28,'2014-09-14 12:00:10',1121),(2673,1,28,'2014-09-14 12:00:11',1121),(2674,1,28,'2014-09-14 12:02:23',1121),(2675,1,28,'2014-09-14 12:04:57',1121),(2676,1,28,'2014-09-14 12:05:13',1121),(2677,1,28,'2014-09-14 12:05:13',1121),(2678,1,28,'2014-09-14 13:00:26',1121),(2679,1,28,'2014-09-16 11:00:07',1129),(2680,1,28,'2014-09-16 11:00:09',1129),(2681,1,28,'2014-09-16 16:24:36',1120),(2682,1,28,'2014-09-16 16:50:02',1120),(2683,1,28,'2014-09-16 17:02:31',1129),(2684,1,28,'2014-09-16 17:06:48',1120),(2685,1,28,'2014-09-16 17:10:05',1127),(2686,1,28,'2014-09-17 22:13:26',1120),(2687,1,28,'2014-09-17 22:13:26',1120),(2688,1,28,'2014-09-17 22:15:28',1120),(2689,1,28,'2014-09-17 22:15:28',1120),(2690,1,28,'2014-09-17 22:16:10',1120),(2691,1,28,'2014-09-17 22:19:51',1120),(2692,1,28,'2014-09-17 22:19:51',1120),(2693,1,28,'2014-09-17 22:20:26',1120),(2694,1,28,'2014-09-17 22:20:26',1120),(2695,1,28,'2014-09-18 09:09:49',1121),(2696,1,28,'2014-09-18 09:28:44',1123),(2697,1,28,'2014-09-18 09:33:30',1123),(2698,1,28,'2014-09-18 09:45:55',1123),(2699,1,28,'2014-09-18 09:47:32',1123),(2700,1,28,'2014-09-18 11:36:42',1121),(2701,1,28,'2014-09-18 11:36:43',1121),(2702,1,28,'2014-09-18 11:49:41',1127),(2703,1,28,'2014-09-18 11:50:16',1127),(2704,1,28,'2014-09-18 11:53:55',1127),(2705,1,28,'2014-09-18 12:11:24',1123),(2706,1,28,'2014-09-18 12:18:43',1129),(2707,1,28,'2014-09-18 12:23:37',1129),(2708,1,28,'2014-09-18 14:19:51',1132),(2709,1,28,'2014-09-18 14:19:52',1132),(2710,1,28,'2014-09-18 14:23:01',1132),(2711,1,28,'2014-09-18 14:25:01',1132),(2712,1,28,'2014-09-18 14:25:01',1132),(2713,1,28,'2014-09-18 14:44:48',1133),(2714,1,28,'2014-09-18 14:44:49',1133),(2715,1,28,'2014-09-18 14:45:20',1133),(2716,1,28,'2014-09-18 14:49:05',1129),(2717,1,28,'2014-09-18 14:49:48',1129),(2718,1,28,'2014-09-18 14:50:33',1133),(2719,1,28,'2014-09-18 14:51:04',1127),(2720,1,28,'2014-09-18 14:54:20',1133),(2721,1,28,'2014-09-18 14:55:27',1120),(2722,1,28,'2014-09-18 14:56:17',1120),(2723,1,28,'2014-09-18 14:56:18',1120),(2724,1,28,'2014-09-18 15:06:40',1120),(2725,1,28,'2014-09-18 15:23:08',1120),(2726,1,28,'2014-09-18 15:23:09',1127),(2727,1,28,'2014-09-18 15:27:57',1127),(2728,1,28,'2014-09-18 15:27:57',1127),(2729,1,28,'2014-09-18 15:29:11',1127),(2730,1,28,'2014-09-18 15:31:31',1127),(2731,1,28,'2014-09-18 15:31:31',1127),(2732,1,28,'2014-09-18 15:31:38',1127),(2733,1,28,'2014-09-18 15:31:59',1127),(2734,1,28,'2014-09-18 15:31:59',1127),(2735,1,28,'2014-09-18 15:32:06',1127),(2736,1,28,'2014-09-18 15:43:42',1129),(2737,1,28,'2014-09-18 15:47:06',1129),(2738,1,28,'2014-09-18 15:47:14',1129),(2739,1,28,'2014-09-18 15:47:50',1129),(2740,1,28,'2014-09-18 15:49:22',1129),(2741,1,28,'2014-09-18 15:52:41',1120),(2742,1,28,'2014-09-18 15:55:04',1120),(2743,1,28,'2014-09-18 15:59:44',1120),(2744,1,28,'2014-09-18 16:01:15',1120),(2745,1,28,'2014-09-18 16:01:54',1120),(2746,1,28,'2014-09-18 16:29:01',1132),(2747,1,28,'2014-09-18 16:30:02',1132),(2748,1,28,'2014-09-18 16:39:17',1129),(2749,1,28,'2014-09-18 16:41:48',1129),(2750,1,28,'2014-09-18 16:57:59',1120),(2751,1,28,'2014-09-18 16:58:35',1120),(2752,1,28,'2014-09-18 16:58:35',1120),(2753,1,28,'2014-09-18 16:59:38',1120),(2754,1,28,'2014-09-18 16:59:38',1120),(2755,1,28,'2014-09-18 17:02:07',1120),(2756,1,28,'2014-09-18 17:02:07',1120),(2757,1,28,'2014-09-18 17:02:19',1120),(2758,1,28,'2014-09-18 17:02:19',1120),(2759,1,28,'2014-09-18 17:02:35',1120),(2760,1,28,'2014-09-18 17:02:35',1120),(2761,1,28,'2014-09-18 17:02:50',1120),(2762,1,28,'2014-09-18 17:02:50',1120),(2763,1,28,'2014-09-18 17:04:17',1120),(2764,1,28,'2014-09-18 17:04:17',1120),(2765,1,28,'2014-09-18 17:12:11',1120),(2766,1,28,'2014-09-18 17:12:11',1120),(2767,1,28,'2014-09-18 17:13:56',1120),(2768,1,28,'2014-09-18 17:13:56',1120),(2769,1,28,'2014-09-18 17:14:53',1120),(2770,1,28,'2014-09-18 17:14:53',1120),(2771,1,28,'2014-09-18 17:16:33',1120),(2772,1,28,'2014-09-18 17:16:33',1120),(2773,1,28,'2014-09-18 17:16:44',1120),(2774,1,28,'2014-09-18 17:16:44',1120),(2775,1,28,'2014-09-18 17:17:09',1120),(2776,1,28,'2014-09-18 17:17:09',1120),(2777,1,28,'2014-09-18 17:34:23',1120),(2778,1,28,'2014-09-18 17:34:23',1120),(2779,1,28,'2014-09-18 17:35:29',1120),(2780,1,28,'2014-09-18 17:35:29',1120),(2781,1,28,'2014-09-18 17:36:57',1120),(2782,1,28,'2014-09-18 17:36:57',1120),(2783,1,28,'2014-09-18 17:37:43',1120),(2784,1,28,'2014-09-18 17:37:43',1120),(2785,1,28,'2014-09-18 17:40:02',1120),(2786,1,28,'2014-09-18 17:40:02',1120),(2787,1,28,'2014-09-18 17:41:18',1120),(2788,1,28,'2014-09-18 17:41:18',1120),(2789,1,28,'2014-09-18 17:43:31',1120),(2790,1,28,'2014-09-18 17:43:31',1120),(2791,1,28,'2014-09-18 17:46:22',1120),(2792,1,28,'2014-09-18 17:46:22',1120),(2793,1,28,'2014-09-18 17:46:43',1120),(2794,1,28,'2014-09-18 17:46:43',1120),(2795,1,28,'2014-09-18 17:53:33',1120),(2796,1,28,'2014-09-18 17:53:33',1120),(2797,1,28,'2014-09-18 17:54:29',1120),(2798,1,28,'2014-09-18 17:54:29',1120),(2799,1,28,'2014-09-18 17:54:42',1120),(2800,1,28,'2014-09-18 17:54:42',1120),(2801,1,28,'2014-09-18 17:55:59',1120),(2802,1,28,'2014-09-18 17:55:59',1120),(2803,1,28,'2014-09-18 17:56:17',1120),(2804,1,28,'2014-09-18 17:56:17',1120),(2805,1,28,'2014-09-18 18:08:36',1120),(2806,1,28,'2014-09-21 09:48:44',1126),(2807,1,28,'2014-09-21 09:49:29',1126),(2808,1,28,'2014-09-21 09:49:58',1126),(2809,1,28,'2014-09-21 10:15:35',1137),(2810,1,28,'2014-09-21 10:15:35',1137),(2811,1,28,'2014-09-21 13:49:45',1139),(2812,1,28,'2014-09-21 13:50:07',1139),(2813,1,28,'2014-12-09 07:37:05',1142),(2814,1,28,'2014-12-09 07:43:33',1144),(2815,1,28,'2014-12-09 13:51:11',1142),(2816,1,28,'2014-12-09 13:52:05',1144),(2817,1,28,'2014-12-09 13:55:57',1142),(2818,1,28,'2014-12-09 13:55:58',1142),(2819,1,28,'2014-12-14 12:54:10',1142),(2820,1,28,'2014-12-14 12:54:13',1142),(2821,1,28,'2014-12-14 13:02:10',1142),(2822,1,28,'2014-12-14 13:02:11',1142),(2823,1,28,'2015-01-11 08:00:14',1145),(2824,1,28,'2015-01-11 08:00:14',1145),(2825,1,28,'2015-04-15 13:27:18',1146),(2826,1,28,'2015-04-19 07:27:41',1143),(2827,1,28,'2015-04-19 07:27:41',1143),(2828,1,28,'2015-04-19 07:28:51',1143),(2829,1,28,'2015-04-19 07:49:21',1146),(2830,1,28,'2015-04-19 11:16:32',1148),(2831,1,28,'2015-04-19 11:16:59',1148),(2832,1,28,'2015-04-19 11:17:02',1148),(2833,1,28,'2015-04-19 11:19:31',1148),(2834,1,28,'2015-04-19 11:19:47',1148),(2835,1,28,'2015-04-19 11:19:49',1148),(2836,1,28,'2015-04-20 06:52:22',1148),(2837,1,28,'2015-04-20 06:53:00',1148),(2838,1,28,'2015-04-20 06:53:01',1148),(2839,1,28,'2015-04-20 07:27:32',1150),(3825,1,28,'2015-06-08 16:06:03',2148),(3826,1,28,'2015-06-08 16:06:16',2148),(3827,1,28,'2015-06-08 16:06:16',2148),(3828,1,28,'2015-06-11 19:04:27',2148),(3829,1,28,'2015-06-17 10:01:31',1150),(3830,1,28,'2015-06-17 10:02:24',1150),(3831,1,28,'2015-06-17 10:02:24',1150),(3832,1,28,'2015-06-28 16:30:10',2147),(3833,1,28,'2015-06-28 16:30:51',2147),(3834,1,28,'2015-06-30 10:22:55',1144),(3835,1,28,'2015-06-30 10:22:55',1144),(3836,1,28,'2015-07-02 08:30:26',1144),(3837,1,28,'2015-07-02 08:30:27',1144),(3838,1,28,'2015-07-02 08:51:28',1144),(3839,1,28,'2015-07-05 11:28:24',2147),(3840,1,28,'2015-07-05 11:32:57',2147),(3841,1,28,'2015-07-05 11:32:57',2147),(3851,1,28,'2015-07-07 17:14:15',1147),(3852,1,28,'2015-07-07 17:20:29',1147),(3853,1,28,'2015-07-07 17:23:02',1147),(3855,1,28,'2015-07-08 14:52:20',1147),(3857,1,28,'2015-07-08 15:25:48',1147),(3858,1,31,'2015-07-08 15:43:54',1147),(3859,1,31,'2015-07-08 15:44:27',1147),(3901,1,34,'2015-07-13 11:13:14',1147),(3902,1,31,'2015-07-13 11:34:14',1147),(3903,1,31,'2015-07-13 14:54:10',1147),(3904,1,31,'2015-07-15 12:00:19',1147),(3905,1,31,'2015-07-15 12:01:48',1147),(3906,1,31,'2015-07-15 12:46:56',1147),(3908,1,31,'2015-07-15 13:25:03',1147),(3909,1,31,'2015-07-15 13:38:44',1147),(3910,1,1061,'2015-07-16 16:29:46',1147),(3911,1,28,'2015-07-16 16:31:50',1147),(3912,1,28,'2015-07-16 16:31:50',1147),(3913,1,28,'2015-07-16 16:32:16',1147),(3914,1,28,'2015-07-16 16:32:28',1147),(3915,1,28,'2015-07-16 16:32:28',1147),(3916,1,28,'2015-07-16 16:32:37',1147),(3917,1,28,'2015-07-16 16:40:45',1147),(3918,1,28,'2015-07-19 13:28:45',1147),(3919,1,28,'2015-07-19 13:28:45',1147),(3920,1,28,'2015-07-19 13:34:15',1147),(3921,1,1061,'2015-07-21 06:25:29',1147),(3922,1,1061,'2015-07-21 06:27:40',1147),(3923,1,1061,'2015-07-21 06:34:45',1147),(3924,1,1061,'2015-07-21 07:07:41',1147),(3925,1,1061,'2015-07-21 08:09:59',1147),(3926,1,1061,'2015-07-21 08:47:02',1147),(3927,1,1061,'2015-07-21 08:50:17',1147),(3928,1,1061,'2015-07-21 08:53:40',1147),(3929,1,1061,'2015-07-21 08:56:37',1147),(3930,1,1061,'2015-07-21 08:57:29',1147),(3931,1,1061,'2015-07-21 09:01:57',1147),(3932,1,1061,'2015-07-21 09:05:08',1147),(3933,1,1061,'2015-07-21 09:06:26',1147),(3934,1,1061,'2015-07-21 09:07:38',1147),(3935,1,1061,'2015-07-21 09:08:47',1147),(3936,1,1061,'2015-07-21 09:09:51',1147),(3937,1,1061,'2015-07-21 09:15:36',1147),(3938,1,1061,'2015-07-21 09:19:25',1147),(3939,1,31,'2015-07-21 09:52:16',1147),(3940,1,1061,'2015-07-21 09:52:43',1147),(3941,1,1061,'2015-07-21 09:53:54',1147),(3942,1,1061,'2015-07-21 09:59:31',1147),(3943,1,1061,'2015-07-21 10:00:19',1147),(3944,1,1061,'2015-07-21 10:09:42',1147),(3945,1,1061,'2015-07-21 10:15:01',1147),(3946,1,1061,'2015-07-21 10:28:13',1147),(3947,1,1061,'2015-07-21 10:31:05',1147),(3948,1,1061,'2015-07-21 10:35:10',1147),(3949,1,1061,'2015-07-21 10:36:20',1147),(3950,1,1061,'2015-07-21 10:59:06',1147),(3951,1,1061,'2015-07-21 11:29:20',1147),(3952,1,31,'2015-07-21 11:31:15',1147),(3953,1,28,'2015-07-21 13:14:37',1147),(3954,1,28,'2015-07-21 13:14:38',1147),(3955,1,1061,'2015-07-21 13:17:00',1147),(3956,1,31,'2015-07-26 06:47:19',1147),(3957,1,28,'2015-08-04 14:00:46',2147),(3958,1,28,'2015-08-04 14:00:46',2147),(3959,1,28,'2015-08-16 07:16:29',2147),(3960,1,28,'2015-08-16 07:16:29',2147),(3961,1,28,'2015-08-16 07:16:58',2147),(3962,1,28,'2015-08-16 08:19:35',2147),(3963,1,28,'2015-08-16 08:46:31',2147),(3964,1,28,'2015-08-16 08:46:44',2147),(3965,1,28,'2015-08-16 08:46:45',2147),(3966,1,28,'2015-08-17 05:05:49',1147),(3967,1,28,'2015-08-17 05:05:50',1147),(3968,1,28,'2015-08-17 05:06:25',1147),(3969,1,28,'2015-08-17 05:07:13',2152),(3970,1,28,'2015-08-17 05:07:13',2152),(3971,1,28,'2015-08-17 05:07:32',2152),(3972,1,28,'2015-08-17 05:07:33',2152),(3973,1,28,'2015-08-17 05:09:06',2152),(3974,1,28,'2015-08-17 05:09:17',2152),(3975,1,28,'2015-08-17 05:09:18',2152),(3976,1,28,'2015-09-10 12:40:41',2151),(3977,1,28,'2015-09-10 12:40:54',2151),(3978,1,28,'2015-09-10 12:40:57',2151),(3979,1,28,'2015-09-10 12:41:09',2151);
/*!40000 ALTER TABLE `systemactionsaudit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `timezones`
--

LOCK TABLES `timezones` WRITE;
/*!40000 ALTER TABLE `timezones` DISABLE KEYS */;
/*!40000 ALTER TABLE `timezones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `userpasswordhistory`
--

LOCK TABLES `userpasswordhistory` WRITE;
/*!40000 ALTER TABLE `userpasswordhistory` DISABLE KEYS */;
INSERT INTO `userpasswordhistory` VALUES (1,'admin','2013-08-20');
/*!40000 ALTER TABLE `userpasswordhistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,3,'admin','3e89f4eac84c8f52e0c0bc85a35b2241','2013-08-20 14:40:38','admin@admin.com',1,6,0,'2013-08-20',1,1,'54bd7038-5c74-11e5-a3ad-000d3a2010a6',NULL,NULL,NULL,'Sys','Admin',NULL,NULL,NULL,1,6,NULL,NULL,NULL,NULL,NULL,NULL),(25,3,'ronent','827CCB0EEA8A706C4C34A16891F84E7B','2013-10-15 13:18:43','ronent@fourtec.com',2,0,0,NULL,1,1,NULL,NULL,'yyyy-MM-dd',0,'ronen','tzur',NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL),(32,3,'yossih','3e89f4eac84c8f52e0c0bc85a35b2241','2013-10-15 13:48:26','yossi@fourtec.com',4,2,0,NULL,1,1,'F648A634-5466-4220-B50E-EA89EE0E7831',NULL,'yyyy-MM-dd',0,'Yossi','Halfin',NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL),(36,3,'Hagai','827CCB0EEA8A706C4C34A16891F84E7B','2015-05-12 18:15:47','hagaiz@fourtec.com',2,1,0,NULL,1,1,'3D38C03D-687D-4D05-9473-8FA996C99132',NULL,NULL,0,'Hagai','Zamir',NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL),(37,3,'Amanda','827CCB0EEA8A706C4C34A16891F84E7B','2015-05-12 18:17:16','amanda@fourtec.com',2,2,0,NULL,1,1,NULL,NULL,NULL,0,'Amanda','Peled',NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL),(45,3,NULL,NULL,'2015-09-10 13:42:43','ronentz@gmail.com',5,3,0,NULL,0,1,'d7d86709-57c1-11e5-a3ad-000d3a2010a6','2015-09-17 13:42:43.000000','yyyy-MM-dd',0,NULL,NULL,NULL,NULL,NULL,0,0,60,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `usersecurequestions`
--

LOCK TABLES `usersecurequestions` WRITE;
/*!40000 ALTER TABLE `usersecurequestions` DISABLE KEYS */;
INSERT INTO `usersecurequestions` VALUES (1,1,'blabla');
/*!40000 ALTER TABLE `usersecurequestions` ENABLE KEYS */;
UNLOCK TABLES;

