-- MySQL dump 10.13  Distrib 5.6.24, for Win64 (x86_64)
--
-- Host: localhost    Database: fourtec
-- ------------------------------------------------------
-- Server version	5.6.26-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alarmcontacts`
--

DROP TABLE IF EXISTS `alarmcontacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alarmcontacts` (
  `AlarmID` int(11) NOT NULL,
  `ContactID` int(11) NOT NULL,
  PRIMARY KEY (`AlarmID`,`ContactID`),
  KEY `FK_contact_alarm` (`ContactID`),
  CONSTRAINT `FK_alarm_contact` FOREIGN KEY (`AlarmID`) REFERENCES `sensoralarm` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_contact_alarm` FOREIGN KEY (`ContactID`) REFERENCES `contacts` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `alarmnotifications`
--

DROP TABLE IF EXISTS `alarmnotifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alarmnotifications` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DeviceID` int(11) NOT NULL,
  `AlarmTypeID` int(11) NOT NULL,
  `Value` double NOT NULL,
  `StartTime` int(11) NOT NULL DEFAULT '0',
  `ClearTime` int(11) DEFAULT NULL,
  `AlarmReason` varchar(255) DEFAULT NULL,
  `DeviceSensorID` int(11) DEFAULT NULL,
  `Silent` int(11) DEFAULT '0',
  `Hide` tinyint(3) unsigned DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `FK_AlertNotifications_NotificationTypes` (`AlarmTypeID`),
  KEY `FK_AlarmNotifications_Devices` (`DeviceID`),
  CONSTRAINT `FK_AlarmNotifications_Devices` FOREIGN KEY (`DeviceID`) REFERENCES `devices` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_AlertNotifications_NotificationTypes` FOREIGN KEY (`AlarmTypeID`) REFERENCES `alarmtypes` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `alarmtypes`
--

DROP TABLE IF EXISTS `alarmtypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alarmtypes` (
  `ID` int(11) NOT NULL,
  `Name` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `analyticssensors`
--

DROP TABLE IF EXISTS `analyticssensors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `analyticssensors` (
  `UserID` int(11) NOT NULL,
  `DeviceSensorID` int(11) NOT NULL,
  `IsActive` tinyint(1) NOT NULL,
  `LoggerName` char(32) DEFAULT NULL,
  `Measurement` char(32) DEFAULT NULL,
  `ActivationEnergy` double DEFAULT NULL,
  `LowLimit` double DEFAULT NULL,
  `HighLimit` double DEFAULT NULL,
  PRIMARY KEY (`UserID`,`DeviceSensorID`),
  KEY `IX_AnalyticsSensors` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `audittrail`
--

DROP TABLE IF EXISTS `audittrail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `audittrail` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` int(11) DEFAULT NULL,
  `PermissionActionID` int(11) DEFAULT NULL,
  `ActivityTimeStamp` datetime(6) DEFAULT NULL,
  `DeviceID` int(11) DEFAULT NULL,
  `Reason` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_AuditTrail_Users` (`UserID`),
  CONSTRAINT `FK_AuditTrail_Users` FOREIGN KEY (`UserID`) REFERENCES `users` (`ID`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1070 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `audittrailarchive`
--

DROP TABLE IF EXISTS `audittrailarchive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `audittrailarchive` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` int(11) NOT NULL,
  `ActionID` int(11) NOT NULL,
  `ActivityTimeStamp` datetime(6) NOT NULL,
  `DeviceID` int(11) DEFAULT NULL,
  `Reason` varchar(500) DEFAULT NULL,
  `ArchiveDate` date DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `basesenorsfamily`
--

DROP TABLE IF EXISTS `basesenorsfamily`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `basesenorsfamily` (
  `BaseSensorID` int(11) NOT NULL,
  `FamilyTypeID` int(11) NOT NULL,
  PRIMARY KEY (`BaseSensorID`,`FamilyTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `basesensors`
--

DROP TABLE IF EXISTS `basesensors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `basesensors` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `basesensorunits`
--

DROP TABLE IF EXISTS `basesensorunits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `basesensorunits` (
  `BaseSensorID` int(11) NOT NULL,
  `SensorUnitID` int(11) NOT NULL,
  `FamilyTypeID` int(11) NOT NULL,
  PRIMARY KEY (`BaseSensorID`,`SensorUnitID`,`FamilyTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `contactdistributiongroups`
--

DROP TABLE IF EXISTS `contactdistributiongroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contactdistributiongroups` (
  `GroupID` int(11) NOT NULL,
  `ContactID` int(11) NOT NULL,
  `IsUser` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`GroupID`,`ContactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CustomerID` int(11) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `Title` char(10) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `CountryCode` varchar(10) DEFAULT NULL,
  `PhoneNumber` varchar(50) DEFAULT NULL,
  `WorkingHoursID` int(11) DEFAULT NULL,
  `WorkdayStart` int(11) DEFAULT NULL,
  `WorkdayEnd` int(11) DEFAULT NULL,
  `OutOfOfficeStart` int(11) DEFAULT NULL,
  `OutOfOfficeEnd` int(11) DEFAULT NULL,
  `SMSResendInterval` int(11) DEFAULT NULL,
  `IsCertificateDefault` tinyint(1) DEFAULT NULL,
  `Deleted` int(11) DEFAULT '0',
  `WorkingSun` tinyint(4) DEFAULT NULL,
  `WorkingMon` tinyint(4) DEFAULT NULL,
  `WorkingTue` tinyint(4) DEFAULT NULL,
  `WorkingWed` tinyint(4) DEFAULT NULL,
  `WorkingThu` tinyint(4) DEFAULT NULL,
  `WorkingFri` tinyint(4) DEFAULT NULL,
  `WorkingSat` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_Contacts_email` (`Email`,`CustomerID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `contactsworkhoursmode`
--

DROP TABLE IF EXISTS `contactsworkhoursmode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contactsworkhoursmode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `CompanyAddress` varchar(255) DEFAULT NULL,
  `Industry` varchar(255) DEFAULT NULL,
  `PhoneNumber` varchar(10) DEFAULT NULL,
  `Prefix` varchar(10) DEFAULT NULL,
  `VerificationGUID` varchar(64) DEFAULT NULL,
  `CreationDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IsActive` tinyint(3) unsigned DEFAULT '1',
  `SMSCounter` int(11) DEFAULT '0',
  `DisableDate` datetime(6) DEFAULT NULL,
  `MongoDataBackUp` tinyint(3) unsigned DEFAULT NULL,
  `EmailSentDate` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_Customers` (`Email`),
  UNIQUE KEY `VerificationGUID` (`VerificationGUID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `customersettings`
--

DROP TABLE IF EXISTS `customersettings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customersettings` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CustomerID` int(11) NOT NULL,
  `IsCFREnabled` tinyint(1) DEFAULT '0',
  `LanguageID` int(11) DEFAULT NULL,
  `IsSMSEnabled` tinyint(1) DEFAULT '0',
  `MaxStorageAllowed` bigint(20) DEFAULT '0',
  `CapicityPrecentEmail` int(11) DEFAULT NULL,
  `DataStorageExpiry` bigint(20) DEFAULT NULL,
  `PasswordExpiryByDays` int(11) DEFAULT '30',
  `ConcurrentUsersLimit` int(11) DEFAULT '0',
  `LANRConcurrentLimit` int(11) DEFAULT NULL,
  `MaxUsersAllowed` int(11) DEFAULT NULL,
  `subscriptiontime` int(11) DEFAULT NULL,
  `subscriptionstart` int(11) DEFAULT NULL,
  `TemperatureUnit` int(11) DEFAULT NULL,
  `timezonevalue` varchar(20) DEFAULT NULL,
  `timezoneabbr` varchar(5) DEFAULT NULL,
  `timezoneoffset` int(11) DEFAULT NULL,
  `timezoneisdst` int(11) DEFAULT NULL,
  `timezonetext` varchar(255) DEFAULT NULL,
  `debugmode` int(11) DEFAULT NULL,
  `calibtemperature` float DEFAULT NULL,
  `calibhumidity` float DEFAULT NULL,
  `calibproductname` varchar(255) DEFAULT NULL,
  `calibmodel` varchar(100) DEFAULT NULL,
  `calibmanufacture` varchar(100) DEFAULT NULL,
  `calibserialnumber` int(11) DEFAULT NULL,
  `calibnist` int(11) DEFAULT NULL,
  `calibfourtecwaxseal` int(11) DEFAULT NULL,
  `passwordMinLength` int(11) DEFAULT '8',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_SystemSettings` (`ID`),
  KEY `FK_SystemSettings_Customers` (`CustomerID`),
  KEY `FK_SystemSettings_Languages` (`LanguageID`),
  CONSTRAINT `FK_SystemSettings_Customers` FOREIGN KEY (`CustomerID`) REFERENCES `customers` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_SystemSettings_Languages` FOREIGN KEY (`LanguageID`) REFERENCES `languages` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `definedsensors`
--

DROP TABLE IF EXISTS `definedsensors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `definedsensors` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CustomerID` int(11) NOT NULL,
  `BaseSensorID` int(11) NOT NULL,
  `FamilyTypeID` int(11) NOT NULL,
  `SensorUnitID` char(10) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `DecimalPlaces` tinyint(3) unsigned DEFAULT '0',
  `Gain` double DEFAULT '0',
  `Offset` double DEFAULT '0',
  `Log1` double DEFAULT NULL,
  `Log2` double DEFAULT NULL,
  `Ref1` double DEFAULT NULL,
  `Ref2` double DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_DeviceSenosrID` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `devicealarmnotifications`
--

DROP TABLE IF EXISTS `devicealarmnotifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devicealarmnotifications` (
  `DeviceSensorID` int(11) NOT NULL,
  `ContactID` int(11) NOT NULL,
  `ReceiveEmail` tinyint(3) unsigned DEFAULT '0',
  `ReceiveSMS` tinyint(3) unsigned DEFAULT '0',
  `ReceiveBatteryLow` tinyint(3) unsigned DEFAULT '0',
  `BatteryLowValue` int(11) DEFAULT '0',
  `BatteryEmailSent` tinyint(3) unsigned DEFAULT NULL,
  `BatterySMSSent` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`DeviceSensorID`,`ContactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `devicealarmnotificationtrail`
--

DROP TABLE IF EXISTS `devicealarmnotificationtrail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devicealarmnotificationtrail` (
  `DeviceSensorID` int(11) NOT NULL,
  `ContactID` int(11) NOT NULL,
  `AlarmEmailSent` int(11) DEFAULT '0',
  `AlarmSMSSent` int(11) DEFAULT '0',
  PRIMARY KEY (`DeviceSensorID`,`ContactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `deviceautosetup`
--

DROP TABLE IF EXISTS `deviceautosetup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deviceautosetup` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CustomerID` int(11) NOT NULL,
  `DeviceTypeID` int(11) NOT NULL,
  `SetupID` varchar(255) DEFAULT NULL,
  `UsageCounter` int(11) DEFAULT '0',
  `IsAutoSetup` int(11) DEFAULT '0',
  `FirmwareVersion` float DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ix_deviceType_SetupName` (`DeviceTypeID`,`CustomerID`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `devicefamilytypes`
--

DROP TABLE IF EXISTS `devicefamilytypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devicefamilytypes` (
  `ID` int(11) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `HasExternalSensor` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `devicegroups`
--

DROP TABLE IF EXISTS `devicegroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devicegroups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `devicelogger`
--

DROP TABLE IF EXISTS `devicelogger`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devicelogger` (
  `DeviceID` int(11) NOT NULL,
  `Interval` int(11) DEFAULT '0',
  `CelsiusMode` tinyint(3) unsigned DEFAULT '1',
  PRIMARY KEY (`DeviceID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `devicemicrolite`
--

DROP TABLE IF EXISTS `devicemicrolite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devicemicrolite` (
  `DeviceID` int(11) NOT NULL,
  `ShowMinMax` tinyint(3) unsigned DEFAULT NULL,
  `ShowPast24H` tinyint(3) unsigned DEFAULT NULL,
  `MemorySize` int(11) DEFAULT NULL,
  `EnableLEDOnAlarm` tinyint(3) unsigned DEFAULT NULL,
  `LCDConfig` tinyint(3) unsigned DEFAULT NULL,
  `StopOnDisconnect` tinyint(3) unsigned DEFAULT NULL,
  `StopOnKeyPress` tinyint(3) unsigned DEFAULT NULL,
  `UnitRequiresSetup` tinyint(3) unsigned DEFAULT NULL,
  `AveragePoints` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`DeviceID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `devicemicrolog`
--

DROP TABLE IF EXISTS `devicemicrolog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devicemicrolog` (
  `DeviceID` int(11) NOT NULL,
  `AveragePoints` int(11) DEFAULT NULL,
  `DeepSleepMode` tinyint(3) unsigned DEFAULT NULL,
  `EnableLEDOnAlarm` tinyint(3) unsigned DEFAULT NULL,
  `LCDConfiguration` tinyint(3) unsigned DEFAULT NULL,
  `MemorySize` int(11) DEFAULT NULL,
  `ShowMinMax` tinyint(3) unsigned DEFAULT NULL,
  `ShowPast24HMinMax` tinyint(3) unsigned DEFAULT NULL,
  `StopOnDisconnect` tinyint(3) unsigned DEFAULT NULL,
  `StopOnKeyPress` tinyint(3) unsigned DEFAULT NULL,
  `UnitRequiresSetup` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`DeviceID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `devicemicrox`
--

DROP TABLE IF EXISTS `devicemicrox`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devicemicrox` (
  `DeviceID` int(11) NOT NULL,
  `CyclicMode` tinyint(3) unsigned DEFAULT NULL,
  `PushToRun` tinyint(3) unsigned DEFAULT NULL,
  `TimerRunEnabled` tinyint(3) unsigned DEFAULT NULL,
  `TimerStart` int(11) DEFAULT NULL,
  `BoomerangEnabled` tinyint(3) unsigned DEFAULT NULL,
  `BoomerangAuthor` varchar(50) DEFAULT NULL,
  `BoomerangCelsiusMode` tinyint(3) unsigned DEFAULT NULL,
  `BoomerangComment` varchar(50) DEFAULT NULL,
  `BoomerangContacts` varchar(200) DEFAULT NULL,
  `BoomerangDisplayAlarmLevels` tinyint(3) unsigned DEFAULT NULL,
  `BoomerangUTC` varchar(10) DEFAULT NULL,
  `LoggerUTC` int(11) DEFAULT NULL,
  PRIMARY KEY (`DeviceID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `devicepicolite`
--

DROP TABLE IF EXISTS `devicepicolite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devicepicolite` (
  `DeviceID` int(11) NOT NULL,
  `LEDConfig` int(11) DEFAULT NULL,
  `RunDelay` int(11) DEFAULT NULL,
  `RunTime` int(11) DEFAULT NULL,
  `SetupTime` int(11) DEFAULT NULL,
  `StopOnKeyPress` int(11) DEFAULT NULL,
  PRIMARY KEY (`DeviceID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `devices`
--

DROP TABLE IF EXISTS `devices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devices` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SerialNumber` int(11) NOT NULL,
  `CustomerID` int(11) NOT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `DeviceTypeID` int(11) DEFAULT NULL,
  `SiteID` int(11) NOT NULL,
  `StatusID` int(11) NOT NULL,
  `IsRunning` tinyint(3) unsigned DEFAULT '0',
  `CategoryID` int(11) DEFAULT NULL,
  `MacAddress` varchar(50) DEFAULT NULL,
  `FirmwareVersion` float DEFAULT NULL,
  `BuildNumber` int(11) DEFAULT '0',
  `StackVersion` char(10) DEFAULT NULL,
  `BatteryLevel` double DEFAULT NULL,
  `PCBAssembly` char(10) DEFAULT NULL,
  `PCBVersion` char(10) DEFAULT NULL,
  `FirmwareRevision` char(10) DEFAULT NULL,
  `TimeZoneID` int(11) DEFAULT NULL,
  `IsLocked` tinyint(1) DEFAULT NULL,
  `Longitude` varchar(50) DEFAULT '0',
  `Latitude` varchar(50) DEFAULT '0',
  `IconXCoordinate` int(11) DEFAULT '0',
  `IconYCoordinate` int(11) DEFAULT '0',
  `ParentID` int(11) DEFAULT NULL,
  `IsInAlarm` tinyint(3) unsigned DEFAULT '0',
  `LastSetupTime` int(11) DEFAULT '0',
  `AlarmDelay` int(11) DEFAULT NULL,
  `LastStoredSampleTime` datetime(6) DEFAULT NULL,
  `InCalibrationMode` tinyint(3) unsigned DEFAULT '0',
  `InFwUpdate` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_SerialNumber` (`SerialNumber`),
  KEY `IX_MacAddress` (`MacAddress`)
) ENGINE=InnoDB AUTO_INCREMENT=2153 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `devicesensors`
--

DROP TABLE IF EXISTS `devicesensors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devicesensors` (
  `ID` int(11) NOT NULL,
  `DeviceID` int(11) NOT NULL,
  `SensorName` varchar(50) DEFAULT NULL,
  `SensorTypeID` int(11) NOT NULL,
  `SensorUnitID` int(11) NOT NULL,
  `SensorIndex` int(11) DEFAULT NULL,
  `IsEnabled` tinyint(3) unsigned DEFAULT '1',
  `IsAlarmEnabled` tinyint(3) unsigned DEFAULT NULL,
  `CalibrationExpiry` int(11) DEFAULT NULL,
  `MinValue` double DEFAULT NULL,
  `MaxValue` double DEFAULT NULL,
  `DefinedSensorID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `devicesetupfiles`
--

DROP TABLE IF EXISTS `devicesetupfiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devicesetupfiles` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CustomerID` int(11) NOT NULL,
  `DeviceTypeID` int(11) NOT NULL,
  `SetupID` varchar(255) NOT NULL,
  `SetupName` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `devicesmstrail`
--

DROP TABLE IF EXISTS `devicesmstrail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devicesmstrail` (
  `DeviceID` int(11) NOT NULL,
  `ContactID` int(11) NOT NULL,
  `SMSID` varchar(255) NOT NULL,
  `DeviceSensorID` int(11) DEFAULT '0',
  `SMSStatus` varchar(255) DEFAULT NULL,
  `IsBattery` tinyint(3) unsigned DEFAULT '0',
  `LastUpdated` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`DeviceID`,`ContactID`,`SMSID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `devicestatuses`
--

DROP TABLE IF EXISTS `devicestatuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devicestatuses` (
  `ID` int(11) NOT NULL,
  `Name` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `devicetypes`
--

DROP TABLE IF EXISTS `devicetypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devicetypes` (
  `ID` int(11) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `MaxSamples` int(11) NOT NULL,
  `ReportType` varchar(50) DEFAULT NULL,
  `ImageName` varchar(50) DEFAULT NULL,
  `PartNumber` varchar(255) DEFAULT NULL,
  `FWVersion` float DEFAULT NULL,
  `BuildNumber` int(11) DEFAULT NULL,
  `FamilyTypeID` int(11) DEFAULT NULL,
  `PCBVersion` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `emailsettings`
--

DROP TABLE IF EXISTS `emailsettings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emailsettings` (
  `ID` int(11) NOT NULL,
  `EmailFrom` varchar(50) DEFAULT NULL,
  `SMTPServer` varchar(50) DEFAULT NULL,
  `Port` tinyint(3) unsigned DEFAULT NULL,
  `IsAuthRequired` tinyint(1) DEFAULT NULL,
  `TimeoutInterval` int(11) DEFAULT NULL,
  `IsSSL` tinyint(1) DEFAULT NULL,
  `Username` varchar(50) DEFAULT NULL,
  `UserPassword` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `emailtemplates`
--

DROP TABLE IF EXISTS `emailtemplates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emailtemplates` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `EmailSettingID` int(11) DEFAULT NULL,
  `CustomerID` int(11) DEFAULT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `Subject` varchar(50) DEFAULT NULL,
  `Body` varchar(2000) DEFAULT NULL,
  `IsDefault` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_EmailTemplates_Customers` (`CustomerID`),
  KEY `FK_EmailTemplates_EmailSettings` (`EmailSettingID`),
  CONSTRAINT `FK_EmailTemplates_Customers` FOREIGN KEY (`CustomerID`) REFERENCES `customers` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_EmailTemplates_EmailSettings` FOREIGN KEY (`EmailSettingID`) REFERENCES `emailsettings` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL,
  `CustomerID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS `languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `languages` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL,
  `isDefault` tinyint(1) NOT NULL DEFAULT '0',
  `code` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1010 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `measurementunits`
--

DROP TABLE IF EXISTS `measurementunits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `measurementunits` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `notificationtypes`
--

DROP TABLE IF EXISTS `notificationtypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notificationtypes` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permissionactions`
--

DROP TABLE IF EXISTS `permissionactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissionactions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ActionTypeID` int(11) DEFAULT NULL,
  `Command` varchar(50) NOT NULL,
  `Entity` varchar(50) NOT NULL,
  `IsCFRAction` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_CommandEntity` (`Command`,`Entity`),
  KEY `FK_PermissionActions_PermissionActionTypes` (`ActionTypeID`),
  CONSTRAINT `FK_PermissionActions_PermissionActionTypes` FOREIGN KEY (`ActionTypeID`) REFERENCES `permissionactiontypes` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3118 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permissionactiontypes`
--

DROP TABLE IF EXISTS `permissionactiontypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissionactiontypes` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permissiongroupactionmatrix`
--

DROP TABLE IF EXISTS `permissiongroupactionmatrix`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissiongroupactionmatrix` (
  `PermissionActionID` int(11) NOT NULL,
  `PermissionGroupID` int(11) NOT NULL,
  `PrivilageName` varchar(50) NOT NULL,
  `Comment` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`PermissionActionID`,`PermissionGroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permissiongroupactions`
--

DROP TABLE IF EXISTS `permissiongroupactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissiongroupactions` (
  `PermissionActionID` int(11) NOT NULL,
  `PermissionGroupID` int(11) NOT NULL,
  `CustomerID` int(11) DEFAULT NULL,
  `IsAllowed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`PermissionActionID`,`PermissionGroupID`),
  KEY `FK_PermissionGroupActions_PermissionGroups` (`PermissionGroupID`),
  CONSTRAINT `FK_PermissionGroupActions_PermissionActions` FOREIGN KEY (`PermissionActionID`) REFERENCES `permissionactions` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_PermissionGroupActions_PermissionGroups` FOREIGN KEY (`PermissionGroupID`) REFERENCES `permissiongroups` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permissiongroups`
--

DROP TABLE IF EXISTS `permissiongroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissiongroups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CustomerID` int(11) DEFAULT NULL,
  `Name` varchar(255) NOT NULL,
  `IsAdmin` tinyint(3) unsigned DEFAULT NULL,
  `ParentID` int(11) DEFAULT NULL,
  `LevelID` int(11) DEFAULT NULL,
  `IsReadOnly` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permissionlevels`
--

DROP TABLE IF EXISTS `permissionlevels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissionlevels` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permissionroles`
--

DROP TABLE IF EXISTS `permissionroles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissionroles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permissionusersitedevicegroup`
--

DROP TABLE IF EXISTS `permissionusersitedevicegroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissionusersitedevicegroup` (
  `UserID` int(11) NOT NULL,
  `SiteID` int(11) NOT NULL,
  `PermissionGroupID` int(11) NOT NULL,
  PRIMARY KEY (`UserID`,`SiteID`,`PermissionGroupID`),
  KEY `FK_PermissionUserSiteDeviceType_Sites` (`SiteID`),
  KEY `FK_PermissionUserSiteDeviceType_PermissionGroups` (`PermissionGroupID`),
  CONSTRAINT `FK_PermissionUserSiteDeviceType_PermissionGroups` FOREIGN KEY (`PermissionGroupID`) REFERENCES `permissiongroups` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_PermissionUserSiteDeviceType_Sites` FOREIGN KEY (`SiteID`) REFERENCES `sites` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_PermissionUserSiteDeviceType_users` FOREIGN KEY (`UserID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `reportbasetemplates`
--

DROP TABLE IF EXISTS `reportbasetemplates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reportbasetemplates` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Type` varchar(50) NOT NULL,
  `ImageHeader` tinyblob,
  `PageSize` varchar(12) NOT NULL,
  `DateFormat` varchar(15) NOT NULL,
  `TemperatureUnit` varchar(50) NOT NULL,
  `TimeFormat` varchar(10) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `reportcontacts`
--

DROP TABLE IF EXISTS `reportcontacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reportcontacts` (
  `ReportTemplateID` int(11) NOT NULL,
  `ContactID` int(11) NOT NULL,
  PRIMARY KEY (`ReportTemplateID`,`ContactID`),
  KEY `FK_ReportContacts_Contacts` (`ContactID`),
  CONSTRAINT `FK_ReportContacts_Contacts` FOREIGN KEY (`ContactID`) REFERENCES `contacts` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_ReportContacts_ReportTemplates` FOREIGN KEY (`ReportTemplateID`) REFERENCES `reporttemplates` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `reportdevicehistory`
--

DROP TABLE IF EXISTS `reportdevicehistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reportdevicehistory` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SerialNumber` int(11) NOT NULL,
  `PDFStream` tinyblob NOT NULL,
  `CreationDate` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `reportdevices`
--

DROP TABLE IF EXISTS `reportdevices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reportdevices` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ReportTemplateID` int(11) NOT NULL,
  `SerialNumber` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_ReportDevices` (`ReportTemplateID`,`SerialNumber`),
  CONSTRAINT `FK_ReportDevices_ReportTemplates` FOREIGN KEY (`ReportTemplateID`) REFERENCES `reporttemplates` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `reportperiodtypes`
--

DROP TABLE IF EXISTS `reportperiodtypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reportperiodtypes` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `reportprocesscfr`
--

DROP TABLE IF EXISTS `reportprocesscfr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reportprocesscfr` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ReportID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `Comment` varchar(125) DEFAULT NULL,
  `CreationDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `SignatureLocationX` double DEFAULT NULL,
  `SignatureLocationY` double DEFAULT NULL,
  `PDFStream` tinyblob,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_ReportUser` (`UserID`,`ReportID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `reportprocesstypes`
--

DROP TABLE IF EXISTS `reportprocesstypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reportprocesstypes` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `reportprocessusers`
--

DROP TABLE IF EXISTS `reportprocessusers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reportprocessusers` (
  `ReportDeviceHistoryID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `IsConfirmed` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ReportDeviceHistoryID`,`UserID`),
  CONSTRAINT `FK_ReportProcessUsers_ReportDevicesHistory` FOREIGN KEY (`ReportDeviceHistoryID`) REFERENCES `reportdevicehistory` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `reportsections`
--

DROP TABLE IF EXISTS `reportsections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reportsections` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `reportsectionsorder`
--

DROP TABLE IF EXISTS `reportsectionsorder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reportsectionsorder` (
  `ReportTemplateID` int(11) NOT NULL,
  `ReportSectionID` int(11) NOT NULL,
  `SectionOrder` int(11) NOT NULL,
  PRIMARY KEY (`ReportTemplateID`,`ReportSectionID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `reporttemplateprocessusers`
--

DROP TABLE IF EXISTS `reporttemplateprocessusers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reporttemplateprocessusers` (
  `ReportTemplateId` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `ProcessTypeId` int(11) NOT NULL,
  PRIMARY KEY (`ReportTemplateId`,`UserID`,`ProcessTypeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `reporttemplates`
--

DROP TABLE IF EXISTS `reporttemplates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reporttemplates` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` int(11) NOT NULL,
  `ProfileName` varchar(32) DEFAULT NULL,
  `Description` varchar(125) DEFAULT NULL,
  `ReportHeaderLink` varchar(1000) DEFAULT NULL,
  `IsActive` int(11) DEFAULT NULL,
  `PageSize` varchar(10) DEFAULT NULL,
  `ReportDateFormat` varchar(15) DEFAULT NULL,
  `TemperatureUnit` int(11) DEFAULT NULL,
  `TimeZoneOffset` int(11) DEFAULT '0',
  `PeriodTypeID` int(11) DEFAULT NULL,
  `PeriodTime` datetime(6) DEFAULT NULL,
  `PeriodDay` int(11) DEFAULT NULL,
  `PeriodMonth` int(11) DEFAULT NULL,
  `PeriodStart` int(11) DEFAULT NULL,
  `PeriodEnd` int(11) DEFAULT NULL,
  `EmailAsLink` int(11) DEFAULT '0',
  `EmailAsPDF` int(11) DEFAULT '0',
  `creationdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  KEY `FK_ReportTemplates_Users` (`UserID`),
  KEY `FK_ReportTemplates_ReportPeriodTypes` (`PeriodTypeID`),
  CONSTRAINT `FK_ReportTemplates_ReportPeriodTypes` FOREIGN KEY (`PeriodTypeID`) REFERENCES `reportperiodtypes` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_ReportTemplates_Users` FOREIGN KEY (`UserID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `securequestions`
--

DROP TABLE IF EXISTS `securequestions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `securequestions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(500) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sensoralarm`
--

DROP TABLE IF EXISTS `sensoralarm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sensoralarm` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DeviceSensorID` int(11) NOT NULL,
  `PreLowValue` double DEFAULT NULL,
  `LowValue` double DEFAULT NULL,
  `PreHighValue` double DEFAULT NULL,
  `HighValue` double DEFAULT NULL,
  `PreDelayValue` double DEFAULT NULL,
  `DelayValue` double DEFAULT NULL,
  `BuzzDuration` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3198 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sensordownloadhistory`
--

DROP TABLE IF EXISTS `sensordownloadhistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sensordownloadhistory` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SerialNumber` int(11) NOT NULL,
  `SensorTypeID` int(11) NOT NULL,
  `FirstDownloadTime` int(11) NOT NULL,
  `LastDownloadTime` int(11) NOT NULL,
  `CreationDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3682 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sensorlogshistory`
--

DROP TABLE IF EXISTS `sensorlogshistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sensorlogshistory` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SensorID` int(11) DEFAULT NULL,
  `LogDate` date DEFAULT NULL,
  `SourcePath` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sensortypes`
--

DROP TABLE IF EXISTS `sensortypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sensortypes` (
  `ID` int(11) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `UnitID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sensorunits`
--

DROP TABLE IF EXISTS `sensorunits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sensorunits` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `serialnumbergeneration`
--

DROP TABLE IF EXISTS `serialnumbergeneration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `serialnumbergeneration` (
  `SerialNumber` int(11) NOT NULL,
  `CreationDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sirenroutedevices`
--

DROP TABLE IF EXISTS `sirenroutedevices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sirenroutedevices` (
  `SourceDeviceID` int(11) NOT NULL,
  `DestinationDeviceID` int(11) NOT NULL,
  PRIMARY KEY (`SourceDeviceID`,`DestinationDeviceID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sites`
--

DROP TABLE IF EXISTS `sites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sites` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ParentID` int(11) NOT NULL,
  `CustomerID` int(11) DEFAULT NULL,
  `Name` varchar(50) NOT NULL,
  `TreeOrder` int(11) DEFAULT '0',
  `Deleted` int(11) DEFAULT '0',
  `IconName` varchar(255) DEFAULT NULL,
  `path` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `smssettings`
--

DROP TABLE IF EXISTS `smssettings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `smssettings` (
  `ID` int(11) NOT NULL,
  `CustomerID` int(11) DEFAULT NULL,
  `COMPort` tinyint(3) unsigned DEFAULT NULL,
  `BaudRate` int(11) DEFAULT NULL,
  `TimeoutInterval` int(11) DEFAULT NULL,
  `IsUnlockPinRequired` tinyint(1) DEFAULT NULL,
  `PinCode` tinyint(3) unsigned DEFAULT NULL,
  KEY `FK_SMSSettings_Customers` (`CustomerID`),
  CONSTRAINT `FK_SMSSettings_Customers` FOREIGN KEY (`CustomerID`) REFERENCES `customers` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sysdiagrams`
--

DROP TABLE IF EXISTS `sysdiagrams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sysdiagrams` (
  `name` varchar(160) NOT NULL,
  `principal_id` int(11) NOT NULL,
  `diagram_id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) DEFAULT NULL,
  `definition` longblob,
  PRIMARY KEY (`diagram_id`),
  UNIQUE KEY `UK_principal_name` (`principal_id`,`name`)
) ENGINE=InnoDB AUTO_INCREMENT=1004 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `systemactionsaudit`
--

DROP TABLE IF EXISTS `systemactionsaudit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `systemactionsaudit` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` int(11) NOT NULL,
  `PermissionActionID` int(11) NOT NULL,
  `CreationDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DeviceID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3980 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `timezones`
--

DROP TABLE IF EXISTS `timezones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `timezones` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Value` decimal(2,1) DEFAULT NULL,
  `City` varchar(50) DEFAULT NULL,
  `IsDayLightSaving` tinyint(1) DEFAULT NULL,
  `DayLightSavingValue` decimal(2,1) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userpasswordhistory`
--

DROP TABLE IF EXISTS `userpasswordhistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userpasswordhistory` (
  `UserID` int(11) NOT NULL,
  `UserPassword` varchar(255) NOT NULL,
  `ChangedDate` date NOT NULL,
  PRIMARY KEY (`UserID`,`UserPassword`),
  CONSTRAINT `FK_UserPasswordHistory_Users` FOREIGN KEY (`UserID`) REFERENCES `users` (`ID`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CustomerID` int(11) NOT NULL,
  `Username` varchar(50) DEFAULT NULL,
  `UserPassword` varchar(255) DEFAULT NULL,
  `CreationDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `Email` varchar(255) DEFAULT NULL,
  `RoleID` int(11) DEFAULT NULL,
  `PermissionGroupID` int(11) DEFAULT NULL,
  `IsAuthenticated` tinyint(1) DEFAULT '0',
  `PasswordCreationDate` date DEFAULT NULL,
  `IsActive` tinyint(1) DEFAULT '0',
  `LanguageID` int(11) DEFAULT '1',
  `PasswordResetGUID` varchar(64) DEFAULT NULL,
  `PasswordResetExpiry` datetime(6) DEFAULT NULL,
  `defaultDateFormat` varchar(50) DEFAULT NULL,
  `Deleted` int(11) DEFAULT '0',
  `FirstName` varchar(32) DEFAULT NULL,
  `LastName` varchar(32) DEFAULT NULL,
  `mobilenumber` varchar(10) DEFAULT NULL,
  `DialCode` varchar(10) DEFAULT NULL,
  `countrycode` varchar(4) DEFAULT NULL,
  `IsCalibrationReminderActive` int(11) DEFAULT '0',
  `CalibrationReminderInMonths` int(11) DEFAULT '0',
  `InactivityTimeout` int(11) DEFAULT NULL,
  `timezonevalue` varchar(20) DEFAULT NULL,
  `timezoneabbr` varchar(5) DEFAULT NULL,
  `timezoneoffset` int(11) DEFAULT NULL,
  `timezoneisdst` int(11) DEFAULT NULL,
  `timezonetext` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `PasswordResetGUID` (`PasswordResetGUID`),
  UNIQUE KEY `IX_Email` (`Email`),
  UNIQUE KEY `IX_UserName` (`Username`),
  KEY `FK_Users_Customers` (`CustomerID`),
  CONSTRAINT `FK_Users_Customers` FOREIGN KEY (`CustomerID`) REFERENCES `customers` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `usersecurequestions`
--

DROP TABLE IF EXISTS `usersecurequestions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usersecurequestions` (
  `UserID` int(11) NOT NULL,
  `QuestionID` int(11) NOT NULL,
  `Answer` varchar(500) NOT NULL,
  PRIMARY KEY (`UserID`,`QuestionID`),
  KEY `FK_UserSecureQuestions_SecureQuestions` (`QuestionID`),
  CONSTRAINT `FK_UserSecureQuestions_SecureQuestions` FOREIGN KEY (`QuestionID`) REFERENCES `securequestions` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_UserSecureQuestions_Users` FOREIGN KEY (`UserID`) REFERENCES `users` (`ID`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'fourtec'
--
/*!50003 DROP FUNCTION IF EXISTS `fnAppEmailCheck` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  FUNCTION `fnAppEmailCheck`(
	-- Add the para... *** SQLINES FOR EVALUATION USE ONLY *** 
	p_email VARCHAR(255)
) RETURNS tinyint(4)
BEGIN
	 DECLARE v_val_ tinyint;  
     IF p_email IS NOT NULL THEN   
          SET p_email = LOWER(p_email);
     END IF;  
          SET v_val_ = 0;  
          IF p_email like '[a-z,0-9,_,-]%@[a-z,0-9,_,-]%.[a-z][a-z]%'  
             AND p_email NOT like '%@%@%'  
             AND CHARINDEX('.@',p_email) = 0  
             AND CHARINDEX('..',p_email) = 0  
             AND CHARINDEX(',',p_email) = 0  
             AND RIGHT(p_email,1) between 'a' AND 'z' THEN  
               SET v_val_=1;
          END IF;  
     RETURN v_val_;  

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `fnGetBaseDeviceFields` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  FUNCTION `fnGetBaseDeviceFields`() RETURNS longtext CHARSET utf8
BEGIN
	
	-- returns the f... *** SQLINES FOR EVALUATION USE ONLY *** 
	-- assuming that... *** SQLINES FOR EVALUATION USE ONLY *** 

	return ' devices.id,devices.serialnumber,devices.name
		,(select devicetypes.name from devicetypes where devicetypes.id= devices.devicetypeid)devicetype
		,(select devicestatuses.name from devicestatuses where devicestatuses.id = devices.statusid)devicestatus
		,islocked,firmwareversion
		-- ,case isnull(TempatureScale,0) when 1 then ''C'' when 2 then ''F'' end tempaturescale
		,batterylevel
		-- ,samplerateinsec,sampleavgpoints
		,(select timezones.value from timezones where timezones.id = devices.timezoneid)timezone
		,longitude,latitude,iconxcoordinate,iconycoordinate ';

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `fnIsCFRAction` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  FUNCTION `fnIsCFRAction`(
	p_UserID int,
	p_Command nvarchar(50),
	p_Entity nvarchar(50)
) RETURNS int(11)
BEGIN

	declare v_Per_ int;

	select  PermissionActions.id into v_Per_ 
	from PermissionUserActions
	inner join PermissionActions
	on PermissionUserActions.CommandEntityID = PermissionActions.ID
	where PermissionActions.Command=p_Command
	and PermissionActions.Entity = p_Entity
	and PermissionUserActions.UserID = p_UserID
	and PermissionUserActions.IsAllowed=1
	and PermissionActions.IsCFRAction=1;

	return v_Per_;
	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `fnIsCFREnabled` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  FUNCTION `fnIsCFREnabled`(
	-- Add the para... *** SQLINES FOR EVALUATION USE ONLY *** 
	p_UserID int
) RETURNS tinyint(4)
BEGIN
	
	DECLARE v_CFR_ TINYINT;

	SELECT customersettings.IsCFREnabled INTO v_CFR_
	FROM customersettings 
	INNER JOIN Users
	ON Users.CustomerID = customersettings.CustomerID
	WHERE Users.ID=p_UserID;

	RETURN v_CFR_;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `fnIsExternalSensor` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  FUNCTION `fnIsExternalSensor`(
	p_SensorTypeID int
	
) RETURNS int(11)
BEGIN
	
	
	RETURN case p_SensorTypeID
	       when 1 then 0
		   when 2 then 0
		   when 3 then 0
		   when 4 then 0
		   when 5 then 1
		   when 6 then 1
		   when 7 then 1
		   when 8 then 1
		   when 9 then 1
		end;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `fnRemovePatternFromString` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  FUNCTION `fnRemovePatternFromString`(
	p_BUFFER LONGTEXT, 
	p_PATTERN VARCHAR(128)
) RETURNS int(11)
BEGIN
	DECLARE v_POS_ INT DEFAULT PATINDEX(p_PATTERN, p_BUFFER);
    WHILE v_POS_ > 0 DO
        SET p_BUFFER = INSERT(p_BUFFER, v_POS_, 1, '');
        SET v_POS_ = PATINDEX(p_PATTERN, p_BUFFER);
    END WHILE;
    RETURN CHAR_LENGTH(RTRIM(p_BUFFER));

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `fnUNIX_TIMESTAMP` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  FUNCTION `fnUNIX_TIMESTAMP`(
p_ctimestamp datetime(3)
) RETURNS int(11)
BEGIN
  /* Function b... *** SQLINES FOR EVALUATION USE ONLY *** */
  declare v_ret_ integer;
   
  set v_ret_ = DATEDIFF(p_ctimestamp,{d '1970-01-01'});
   
  return v_ret_;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `fnValidateCredentialPolicy` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  FUNCTION `fnValidateCredentialPolicy`(
	p_UserName NVARCHAR(50),
	p_Password NVARCHAR(255)
) RETURNS int(11)
BEGIN
	
	DECLARE v_Val_ INT;
	DECLARE v_len_ INT;
	DECLARE v_len2 INT;
	

	SET v_Val_=0;

	-- Username poli... *** SQLINES FOR EVALUATION USE ONLY *** 
    -- checks alphan... *** SQLINES FOR EVALUATION USE ONLY *** 
	IF PATINDEX('%[a-zA-Z0-9]%',p_UserName)=0 THEN
		SET v_Val_=1;
	-- checks for ma... *** SQLINES FOR EVALUATION USE ONLY *** 
	ELSEIF CHAR_LENGTH(RTRIM(p_UserName))>20 THEN 
		SET v_Val_=2;
	-- checks for sp... *** SQLINES FOR EVALUATION USE ONLY *** 
	ELSEIF PATINDEX('%[-]%',p_UserName)=1 OR PATINDEX('%[-]%',p_UserName)=CHAR_LENGTH(RTRIM(p_UserName))
	 OR PATINDEX('%[.]%',p_UserName)=1 OR PATINDEX('%[.]%',p_UserName)=CHAR_LENGTH(RTRIM(p_UserName))
	 OR PATINDEX('%[_]%',p_UserName)=1 OR PATINDEX('%[_]%',p_UserName)=CHAR_LENGTH(RTRIM(p_UserName)) THEN
		SET v_Val_=3;

	END IF;
	

	-- Password poli... *** SQLINES FOR EVALUATION USE ONLY *** 
	-- minimum lengt... *** SQLINES FOR EVALUATION USE ONLY *** 
	-- maximum lengt... *** SQLINES FOR EVALUATION USE ONLY *** 
	IF PATINDEX('%[A-Za-z0-9]%',p_Password)=0
		OR PATINDEX('%[a-z]%',p_Password)=0
		OR PATINDEX('%[0-9]%',p_Password)=0 THEN
		SET v_Val_=5;
	-- checks not ha... *** SQLINES FOR EVALUATION USE ONLY *** 
	ELSEIF PATINDEX('%[/]%',p_Password)>0 OR PATINDEX('%[]%',p_Password)>0
	 OR PATINDEX('%["]%',p_Password)>0 OR PATINDEX(Concat('%[' , CHR(39) , ']%'),p_Password)>0 THEN
		SET v_Val_=6;
	-- checks passwo... *** SQLINES FOR EVALUATION USE ONLY *** 
	ELSE
		set v_len_ = fnRemovePatternFromString(p_Password,'%[!@#$^&*(){}-_=+":;|,.<>?~`]%');
		set v_len2 = fnRemovePatternFromString(p_Password,'%[a-zA-Z0-9]%');
		IF v_len_ != v_len2 and char_length(rtrim(p_Password))!=v_len_ THEN
			SET v_Val_=7;
		END IF;
	END IF;
     
	RETURN v_Val_;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `GetAncestry` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  FUNCTION `GetAncestry`(GivenID INT) RETURNS varchar(1024) CHARSET utf8
BEGIN


 DECLARE rv VARCHAR(1024);
    DECLARE cm CHAR(1);
    DECLARE ch INT;

    SET rv = '';
    SET cm = '';
    SET ch = GivenID;
    WHILE ch > 0 DO
        SELECT IFNULL(parentid,-1) INTO ch FROM
        (SELECT parentid FROM sites WHERE id = ch) A;
        IF ch > 0 THEN
            SET rv = CONCAT(rv,cm,ch);
            SET cm = ',';
        END IF;
    END WHILE;
    RETURN rv;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `GetFamilyTree` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  FUNCTION `GetFamilyTree`(GivenID INT) RETURNS varchar(1024) CHARSET utf8
BEGIN

DECLARE rv,q,queue,queue_children VARCHAR(1024);
    DECLARE queue_length,front_id,pos INT;

    SET rv = '';
    SET queue = GivenID;
    SET queue_length = 1;

    WHILE queue_length > 0 DO
        SET front_id = FORMAT(queue,0);
        IF queue_length = 1 THEN
            SET queue = '';
        ELSE
            SET pos = LOCATE(',',queue) + 1;
            SET q = SUBSTR(queue,pos);
            SET queue = q;
        END IF;
        SET queue_length = queue_length - 1;

        SELECT IFNULL(qc,'') INTO queue_children
        FROM (SELECT GROUP_CONCAT(id) qc
        FROM sites WHERE parentid = front_id) A;

        IF LENGTH(queue_children) = 0 THEN
            IF LENGTH(queue) = 0 THEN
                SET queue_length = 0;
            END IF;
        ELSE
            IF LENGTH(rv) = 0 THEN
                SET rv = queue_children;
            ELSE
                SET rv = CONCAT(rv,',',queue_children);
            END IF;
            IF LENGTH(queue) = 0 THEN
                SET queue = queue_children;
            ELSE
                SET queue = CONCAT(queue,',',queue_children);
            END IF;
            SET queue_length = LENGTH(queue) - LENGTH(REPLACE(queue,',','')) + 1;
        END IF;
    END WHILE;

    RETURN rv;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `GetParentIDByID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  FUNCTION `GetParentIDByID`(GivenID INT) RETURNS int(11)
BEGIN
DECLARE rv INT;

    SELECT IFNULL(parentid,-1) INTO rv FROM
    (SELECT parentid FROM sites WHERE id = GivenID) A;
    RETURN rv;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spAddDeviceSetupFile` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spAddDeviceSetupFile`(
	p_UserID int,
	p_DeviceType varchar(255),
	p_SetupID varchar(255),
	p_SetupName varchar(50))
BEGIN
	

	Declare v_Cus_ int;
	declare v_Dev_ int;

	select id into v_Dev_
	from DeviceTypes
	where name=p_DeviceType;

	select customerid into v_Cus_
	from users
	where id=p_UserID;

    Insert into DeviceSetupFiles(CustomerID,DeviceTypeID,SetupID, SetupName)
	values(v_Cus_, v_Dev_,p_SetupID,p_SetupName);


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spAddGroup` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spAddGroup`(
	p_UserID int,
	p_GroupID int,
	p_GroupName varchar(50))
BEGIN

	declare v_Cus_ int;

	select customerid into v_Cus_
	from users
	where id=p_UserID;

	if p_GroupID>0
	then
		update groups
		set name=p_GroupName
		where id=p_GroupID;

		delete from ContactDistributionGroups
		where GroupID = p_GroupID;

		select p_GroupID;
	else

		insert into groups(name,customerid)
		values(p_GroupName, v_Cus_);

		select @@IDENTITY;
	end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spAddNewSample` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spAddNewSample`(
	p_DeviceSensorID int,
	p_Value double,
	p_SampleTime int,
	p_IsTimeStamp tinyint unsigned,
	p_Comment nvarchar(50),
	p_AlarmStatus tinyint unsigned,
	p_IsDummy tinyint unsigned/* =0 */)
BEGIN

	declare v_ret_ tinyint unsigned;

	set v_ret_=0;

	if not exists(select  id from sensorlogs where devicesensorid=p_DeviceSensorID and value=p_Value and SampleTime=p_SampleTime limit 1)
	then
		insert into sensorlogs(devicesensorid, value, sampletime, istimestamp, comment, alarmstatus,IsDummy)
		values(p_DeviceSensorID,p_Value,p_SampleTime,p_IsTimeStamp,p_Comment,p_AlarmStatus,p_IsDummy);

		set v_ret_=1;
	end if;
	
	select v_ret_;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spAddPermissionGroup` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spAddPermissionGroup`(
	p_UserID int,
	p_Name varchar(255),
	p_ParentName varchar(255))
BEGIN

	declare v_Par_ int;
	declare v_Cus_ int;
	declare v_Lev_ int;

	select customerid into v_Cus_
	from Users
	where id=p_UserID;

	select id,levelid into v_Par_, v_Lev_
	from PermissionGroups
	where customerID = 0
	and name =p_ParentName;

	if v_Par_>0
	then
		insert into PermissionGroups(name,IsAdmin,ParentID,LevelID,IsReadOnly,CustomerID)
		values(p_Name,0,v_Par_,v_Lev_,0,v_Cus_);
	end if;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spAddPermissionGroupSite` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spAddPermissionGroupSite`(
	p_UserID int,
	p_PermissionGroupID int,
	p_SiteID int)
BEGIN

    insert into PermissionUserSiteDeviceGroup(UserID,SiteID,PermissionGroupID)
	values(p_UserID,p_SiteID,p_PermissionGroupID);


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spAddReportApprovers` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spAddReportApprovers`(
    p_ReportTemplateID int,
	p_UserID int)
BEGIN

	if exists (select  ReportTemplateID from ReportTemplateProcessUsers where ReportTemplateID=p_ReportTemplateID limit 1)
	then
		delete from ReportTemplateProcessUsers where ReportTemplateID=p_ReportTemplateID;
	end if;
	
	insert into ReportTemplateProcessUsers(ReportTemplateID,UserId,ProcessTypeId)
	values(p_ReportTemplateID,p_UserID,2);
	

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spAddReportContact` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spAddReportContact`(
    p_ReportTemplateID int,
	p_ContactID int)
BEGIN

	if exists (select  ContactID from ReportContacts where ReportTemplateID=p_ReportTemplateID limit 1)
	then
		delete from ReportContacts where ReportTemplateID=p_ReportTemplateID;
	end if;
	
	insert into ReportContacts(ReportTemplateID,ContactID)
	values(p_ReportTemplateID,p_ContactID);
	

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spAddReportDevice` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spAddReportDevice`(
    p_ReportTemplateID int,
	p_SerialNumber int)
BEGIN

	if exists (select  id from ReportDevices where ReportTemplateID=p_ReportTemplateID limit 1)
	then
		delete from ReportDevices where ReportTemplateID=p_ReportTemplateID;
	end if;
	
	insert into ReportDevices(ReportTemplateID,SerialNumber)
	values(p_ReportTemplateID,p_SerialNumber);
	

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spAddReportReviewer` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spAddReportReviewer`(
    p_ReportTemplateID int,
	p_UserID int)
BEGIN

	
	
	insert into ReportTemplateProcessUsers(ReportTemplateID,UserId,ProcessTypeId)
	values(p_ReportTemplateID,p_UserID,1);
	

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spAdminCreateUser` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spAdminCreateUser`( 
	p_AdminUserID INT,
	p_PermissionRoleID INT,
	p_InactivityTimeout int,
	p_PermissionGroupID int /* =0 */ ,
	p_Email nvarchar(255))
BEGIN

	DECLARE v_Cus_ INT;
	declare v_lan_ int;
	declare v_use_ int;
	-- declare @IsCF... *** SQLINES FOR EVALUATION USE ONLY *** 
	declare v_Act_ int;
	declare v_Per_ int;
	declare v_Max_ int;
	declare v_Use2 int;
    declare v_password_expiry  datetime;

	set v_password_expiry=date_add(now(),interval 7 day);

	set v_use_ = 0;
	-- set @IsCFREna... *** SQLINES FOR EVALUATION USE ONLY *** 

	-- get the cust... *** SQLINES FOR EVALUATION USE ONLY *** 
    SELECT Users.CustomerID INTO v_Cus_
	FROM Users
	WHERE ID=p_AdminUserID;

	-- get the langa... *** SQLINES FOR EVALUATION USE ONLY *** 
	select Languages.ID into v_lan_
	from Languages
	where Languages.isDefault=1;

	-- checks if the... *** SQLINES FOR EVALUATION USE ONLY *** 
	select MaxUsersAllowed into v_Max_
	from customersettings
	where customersettings.customerid=v_Cus_;

	select count(id) into v_Use2
	from users
	where customerid=v_Cus_
	and ifnull(deleted,0)=0;

	if (v_Cus_ > 0 and v_Use2<v_Max_)

	then
		-- insert the ne... *** SQLINES FOR EVALUATION USE ONLY *** 
		insert Users(CustomerID,RoleID,PermissionGroupID,Email,LanguageID,defaultDateFormat,PasswordResetGUID,PasswordResetExpiry,isactive,InactivityTimeout)
		values(v_Cus_, p_PermissionRoleID,p_PermissionGroupID,p_Email,v_lan_,'yyyy-MM-dd',UUID(),v_password_expiry,0,p_InactivityTimeout);

		set v_use_ = LAST_INSERT_ID();

		-- CFR
		call spCFRAuditTrail (p_AdminUserID,'create','users',0, '');

	end if;

	select id userid,PasswordResetGUID
	from users
	where id=v_use_;



END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spAdminUpdateUser` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spAdminUpdateUser`( 
	p_AdminUserID INT,
	p_UserID int,
	p_PermissionRoleID INT,
	p_InactivityTimeout int,
	p_PermissionGroupID int  ,
	p_Email nvarchar(255))
BEGIN

	declare v_Per_ int;

	update users
	set email = p_Email,
	RoleID = p_PermissionRoleID,
	InactivityTimeout = p_InactivityTimeout,
	PermissionGroupID = p_PermissionGroupID
	where id=p_UserID;

	select id into v_Per_
	from PermissionActions
	where command='update'
	and entity='users'
	and iscfraction=1;

	-- CFR
	call spCFRAuditTrail( p_AdminUserID,v_Per_,0, '');

	delete from PermissionUserSiteDeviceGroup
	where userid=p_UserID;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spApproveReport` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spApproveReport`(
	p_UserID int,
	p_ReportDeviceHistoryID int)
BEGIN

	update ReportProcessUsers
	set isConfirmed=1
	where ReportDeviceHistoryID = p_ReportDeviceHistoryID
	and UserID = p_UserID;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spCFRAuditTrail` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spCFRAuditTrail`(
	p_UserID int,
	p_Command nvarchar(50),
	p_Entity nvarchar(50),
	p_SerialNumber int /* = 0 */,
	p_Reason nvarchar(500)/* ='' */)
BEGIN

	declare v_IsC_ int;
	declare v_Per_ int;
	declare v_Dev_ int;

	select ID into v_Dev_
	from devices
	where serialnumber=p_SerialNumber;

	select id into v_Per_
	from PermissionActions
	where Command=p_Command
	and Entity=p_Entity
	and IsCFRAction=1;

	-- checks if nee... *** SQLINES FOR EVALUATION USE ONLY *** 
	set v_IsC_ =fnIsCFREnabled(p_UserID);
    
	IF v_IsC_ = 1 and v_Per_>0
	THEN
			
		INSERT AuditTrail
				( UserID ,
					PermissionActionID ,
					ActivityTimeStamp ,
					DeviceID ,
					Reason
				)
		values  (   p_UserID ,
					v_Per_ , 
					NOW() , 
					v_Dev_ , 
					p_Reason 
				); 

	END IF;  -- CFR Enabled

	-- select @@Row... *** SQLINES FOR EVALUATION USE ONLY *** 


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spChangePassword` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spChangePassword`(
	p_UserID int,
	p_CurrentPassword varchar(32),
	p_NewPassword varchar(32))
BEGIN
	-- SET NOCOUNT ... *** SQLINES FOR EVALUATION USE ONLY *** 
	-- interfering ... *** SQLINES FOR EVALUATION USE ONLY *** 

	if exists(select id from users where id=p_UserID and UserPassword=p_CurrentPassword limit 1)
	then
		update users
		set UserPassword=p_NewPassword
		where id=p_UserID;

		select 1;
	else
		select 0;
	end if;

   
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spCleanDataTables` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spCleanDataTables`()
begin

truncate table alarmnotifications;

truncate table audittrail;

truncate table audittrailarchive;

truncate table definedsensors;

truncate table devicelogger;

truncate table devicemicrolite;

truncate table devicemicrolite;

truncate table devicemicrox;

truncate table devicepicolite;

delete from  SensorAlarm;

truncate table sensordownloadhistory;

delete from devicesensors;

delete from devices;


delete from PermissionUserSiteDeviceGroup
where siteid not in(select id from sites where parentid=0);

delete from sites
where ParentID<>0;

end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spCreateAdminUser` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spCreateAdminUser`(
	p_GUID char(36),
	p_UserName nvarchar(50),
	p_UserPassword nvarchar(255),
	p_Email nvarchar(255),
	p_QuestionID int,
	p_Answer nvarchar(500))
BEGIN
	-- SET NOCOUNT ... *** SQLINES FOR EVALUATION USE ONLY *** 
	-- interfering ... *** SQLINES FOR EVALUATION USE ONLY *** 

	declare v_cus_ int;
	declare v_gro_ int;
	declare v_lan_ int;
	declare v_use_ int;
	-- init the user... *** SQLINES FOR EVALUATION USE ONLY *** 
	set v_use_=0;

	-- first check t... *** SQLINES FOR EVALUATION USE ONLY *** 
	if exists(select customers.ID from Customers where Customers.VerificationGUID=p_GUID limit 1)
	then
		-- then get the ... *** SQLINES FOR EVALUATION USE ONLY *** 
		select Customers.ID into v_cus_
		from Customers
		where Customers.VerificationGUID = p_GUID;

		-- checks if the... *** SQLINES FOR EVALUATION USE ONLY *** 
		if not exists(select  Users.ID 
						from Users 
						where Users.CustomerID = v_cus_
						and Users.Username = p_UserName limit 1)
		then
			-- check for val... *** SQLINES FOR EVALUATION USE ONLY *** 
			if exists (select  SecureQuestions.ID from SecureQuestions where SecureQuestions.ID=p_QuestionID limit 1)
				and char_length(rtrim(p_Answer))>0
			then
				-- get the group... *** SQLINES FOR EVALUATION USE ONLY *** 
				select PermissionGroups.ID into v_gro_
				from PermissionGroups
				where PermissionGroups.Name = 'Administrators';
				-- get the langa... *** SQLINES FOR EVALUATION USE ONLY *** 
				select Languages.ID into v_lan_
				from Languages
				where Languages.isDefault=1;
				-- insert the ne... *** SQLINES FOR EVALUATION USE ONLY *** 
				insert Users(Username,CustomerID,UserPassword,IsAdmin,Email,GroupID,LanguageID,PasswordCreationDate)
				values(p_UserName,v_cus_,p_UserPassword,1,p_Email,v_gro_,v_lan_,NOW());

				set v_use_ = last_insert_id();

				-- insert the se... *** SQLINES FOR EVALUATION USE ONLY *** 
				insert UserSecureQuestions(UserID,QuestionID,Answer)
				values(v_use_,p_QuestionID,p_Answer);
				-- save the pass... *** SQLINES FOR EVALUATION USE ONLY *** 
				insert UserPasswordHistory(UserID,UserPassword,ChangedDate)
				values(v_use_,p_UserPassword,NOW());
			end if;	
		end if;
	end if;
	
    select v_use_ as userid;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spCreateNewContact` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spCreateNewContact`(
	p_UserID int,
	p_Name nvarchar(50),
	p_Title nvarchar(10),
	p_Phone nvarchar(50),
	p_Email nvarchar(255),
	p_WeekdayStart int,
	p_WorkdayStart time(6),
	p_WorkdayEnd time(6),
	p_SMSResendInterval int)
BEGIN
	-- SET NOCOUNT ... *** SQLINES FOR EVALUATION USE ONLY *** 
	-- interfering ... *** SQLINES FOR EVALUATION USE ONLY *** 

    declare v_Cus_ int;

	select users.customerid into v_Cus_
	from users
	where users.id = p_UserID;
	
	INSERT INTO Contacts
           (CustomerID
		   ,`Name`
           ,`Title`
           ,`Phone`
           ,`Email`
           ,`WeekDayStart`
           ,`WorkdayStart`
           ,`WorkdayEnd`
           ,`SMSResendInterval`
           )
     VALUES
           (v_Cus_
		   ,p_Name
           ,p_Title
           ,p_Phone
           ,p_Email
           ,p_WeekdayStart
           ,p_WorkdayStart
           ,p_WorkdayEnd
           ,p_SMSResendInterval
           );

	select FOUND_ROWS();


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spCreateNewCustomer` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spCreateNewCustomer`(
	p_GUID varchar(255),
	p_CompanyName varchar(255),
	p_Address varchar(255),
	p_CountryCode varchar(4),
	p_FirstName varchar(32),
	p_LastName varchar(32),
	p_Industry varchar(255),
	p_MobileNumber varchar(10),
	p_PhoneNumber varchar(10),
	p_Prefix varchar(10),
	p_timezonevalue varchar(20),
	p_timezoneabbr varchar(5),
	p_timezoneoffset int,
	p_timezoneisdst int,
	p_timezonetext varchar(255),
	p_Title varchar(50),
	p_Username varchar(50),
	p_Password varchar(255),
	p_DialCode varchar(10))
BEGIN
	

	declare v_cus_ int;
	declare v_Cus2 varchar(50);
	-- dbo.fnAppEmai... *** SQLINES FOR EVALUATION USE ONLY *** 
	if exists (select id from Customers where VerificationGUID=p_GUID limit 1) 
	then
		
		select id,email into v_cus_, v_Cus2
		from customers
		where VerificationGUID=p_GUID;

		update Customers
		set VerificationGUID=null
		where VerificationGUID=p_GUID;

		if not exists(select  id from customersettings where customerid=v_cus_ limit 1)
		then 
			insert into customersettings(customerid)
			values(v_cus_);
		end if;

		update customers
		set name=p_CompanyName,
		CompanyAddress=p_Address,
		Industry=p_Industry,
		PhoneNumber=p_PhoneNumber,
		Prefix = p_Prefix
		where id=v_cus_;

		update CustomerSettings
		set timezonevalue=p_timezonevalue,
		timezoneabbr=p_timezoneabbr,
		timezoneoffset=p_timezoneoffset,
		timezoneisdst=p_timezoneisdst,
		timezonetext=p_timezonetext,
		IsCFREnabled=0,
		MaxStorageAllowed=1000,
		DataStorageExpiry = DATEDIFF(dateadd(day,60,now()),{d '1970-01-01'}),
		ConcurrentUsersLimit=5,
		MaxUsersAllowed=5,
		subscriptiontime=60,
		subscriptionstart=DATEDIFF(now(),{d '1970-01-01'}),
		passwordMinLength=8
		where customerid=v_cus_;

		if not exists(select  id from users where email=v_Cus2 and username=p_Username and customerid=v_cus_ limit 1)
		then
			insert into users(customerid,Username,UserPassword,email,RoleID,PermissionGroupID
							,IsActive,LanguageID,firstname,lastname,mobilenumber,countrycode,
							timezonevalue,timezoneabbr,timezoneoffset,timezoneisdst,timezonetext,DialCode)
			values(v_cus_,p_Username,p_Password,v_Cus2,2,1,1,1,p_FirstName,p_LastName,p_MobileNumber,p_CountryCode,
					p_timezonevalue,p_timezoneabbr,p_timezoneoffset,p_timezoneisdst,p_timezonetext,p_DialCode);
		end if;

		/*insert Cust... *** SQLINES FOR EVALUATION USE ONLY *** t,MaxStorageAllowed,LanguageID)
		values(@customerid,@MaxUsers,@MaxStorage,1)*/

		insert into PermissionGroupActions(PermissionActionID, PermissionGroupID, CustomerID, IsAllowed)
		select PermissionActionID,PermissionGroupID,v_cus_,1
		from PermissionGroupActionMatrix
		where not exists(select permissionactionid
						 from PermissionGroupActions 
						 where PermissionGroupActions.PermissionActionID=PermissionGroupActionMatrix.PermissionActionID
						 and PermissionGroupActions.PermissionGroupID=PermissionGroupActionMatrix.PermissionGroupID
						 and customerid = v_cus_);

		
		select v_Cus2;

	else
		select '';
	end if;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spCreateNewSite` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spCreateNewSite`( 
	p_UserID int,
	p_Name nvarchar(50),
	p_ParentID int)
BEGIN
	-- SET NOCOUNT ... *** SQLINES FOR EVALUATION USE ONLY *** 
	-- interfering ... *** SQLINES FOR EVALUATION USE ONLY *** 

	declare v_sit_ int;
	declare v_Per_ int;
	declare v_Cus_ int;

	select customerid into v_Cus_
	from users
	where id=p_UserID;

    insert into sites(parentid,name,CustomerID)
	values(p_ParentID,p_Name,v_Cus_);
    
	set v_sit_ = last_insert_id();

	/*select @Per... *** SQLINES FOR EVALUATION USE ONLY *** issionUserSiteDeviceGroup
	where UserID=@UserID
	and SiteID = @ParentID

	insert into PermissionUserSiteDeviceGroup(userid,siteid,PermissionGroupID)
	values(@UserID,@SiteID,@PermissionGroupID)*/

	
	if v_sit_>0
	then
		select v_sit_;
	else 
		select 0;
	end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spCustomerGuidIsValid` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spCustomerGuidIsValid`(
	p_GUID varchar(200))
BEGIN
	declare v_gui_ char(36);

	SELECT  v_gui_= CAST(
        SUBSTRING(p_GUID, 1, 8) + '-' + SUBSTRING(p_GUID, 9, 4) + '-' + SUBSTRING(p_GUID, 13, 4) + '-' +
        SUBSTRING(p_GUID, 17, 4) + '-' + SUBSTRING(p_GUID, 21, 12)
        AS CHAR(36));

	if exists(select  customers.ID from customers where customers.VerificationGUID=v_gui_ limit 1) 
	then
		select 1;
	else
		select 0;
	end if;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spCustomerSignUp` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spCustomerSignUp`(
	p_CustomerEmail varchar(50))
BEGIN

	declare v_Cus_ int;

    insert into customers(name,email,VerificationGUID)
	values('',p_CustomerEmail,UUID());

	set v_Cus_= last_insert_id();

	select VerificationGUID,v_Cus_ customerid
	from Customers
	where id=v_Cus_;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spdeAssociateDevices` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spdeAssociateDevices`( 
	p_SerialNumber int)
BEGIN

    update Devices
	set CustomerID=0
	where serialnumber=p_SerialNumber;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spDeleteAnalyticsSensors` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spDeleteAnalyticsSensors`(
	p_UserID int,
	p_DeviceSensorID int /* = 0 */)
BEGIN

	if p_DeviceSensorID>0
	THEN
	
		DELETE from AnalyticsSensors 
		WHERE UserID=p_UserID 
		AND DeviceSensorID=p_DeviceSensorID;

	ELSE
	
		Update AnalyticsSensors 
		set IsActive=0
		WHERE UserID=p_UserID ;

	END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spDeleteContact` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spDeleteContact`(
	p_ContactID int)
BEGIN
	

    update contacts
	set deleted=1
	where contacts.id = p_ContactID;

	

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spDeleteDefinedSensor` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spDeleteDefinedSensor`(
	p_DefinedSensorID int)
BEGIN
			
		delete from DefinedSensors
		where id = p_DefinedSensorID;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spDeleteGroup` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spDeleteGroup`(
	p_GroupID int)
BEGIN

	delete from Groups
	where id=p_GroupID;


	delete from ContactDistributionGroups
	where GroupID=p_GroupID;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spDeleteReport` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spDeleteReport`(
	p_ReportID int)
BEGIN

	delete from ReportDeviceHistory
	where id=p_ReportID;
	
   
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spDeleteSetupFile` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spDeleteSetupFile`(
	p_UserID int,
	p_SetupID varchar(255))
BEGIN

	declare v_Cus_ int;

	select customerid into v_Cus_
	from users
	where id=p_UserID;
   
    delete from DeviceSetupFiles
	where CustomerID=v_Cus_
	and SetupID = p_SetupID;

	update DeviceAutoSetup
	set SetupID=''
	,IsAutoSetup=0
	where CustomerID=v_Cus_
	and SetupID=p_SetupID;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spDeleteSite` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spDeleteSite`(
	p_SiteID int)
BEGIN

    if not exists (select  id from sites where parentid=p_SiteID limit 1) and
	   not exists (select  id from devices where siteid=p_SiteID limit 1)

	then
		delete from PermissionUserSiteDeviceGroup
		where siteid=p_SiteID;

		/*delete from... *** SQLINES FOR EVALUATION USE ONLY *** */

		delete from sites
		where id=p_SiteID;

		select 1;
	else
		select 0;
	end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spDeleteUser` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spDeleteUser`( 
	p_UserID int)
BEGIN
	-- SET NOCOUNT ... *** SQLINES FOR EVALUATION USE ONLY *** 
	-- interfering ... *** SQLINES FOR EVALUATION USE ONLY *** 

    update users
	set users.deleted=1
	where users.id = p_UserID;

	select FOUND_ROWS();
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spDisableAccount` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spDisableAccount`(
	p_SerialNumber int)
BEGIN

    declare v_Cus_ int;
	declare v_was_ tinyint unsigned;
	declare v_Ema_ int;
	declare v_Ema2 datetime(3);
	declare v_day_ int;

	select customerid into v_Cus_
	from devices
	where serialNumber=p_SerialNumber;

	-- checks first ... 
	if exists(select  id from customers where id=v_Cus_ and ifnull(IsActive,1)=1 limit 1)
	then

		update customers
		set IsActive=0
		,DisableDate = now()
		where id=v_Cus_;

		set v_was_=0;
	else

		set v_was_=1;
	end if;
	
	
	select ifnull(EmailSentDate,now()-1) into v_Ema2
	from customers
	where id=v_Cus_;
	-- if it is lowe...
	set v_day_ =  TIMESTAMPDIFF ( day , v_Ema2 , now() );
	
	set v_Ema_=1;

	if v_day_>0
	then
		set v_Ema_=0;
	end if;
	

	select v_was_ wasDisabled,DisableDate ,ifnull( MongoDataBackUp,0)MongoDataBackUp,v_Ema_ EmailWasSentToday,email,name
	from customers
	where id=v_Cus_;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetActivity` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetActivity`(
	p_UserID int)
BEGIN

	declare v_Cus_ int;

	select customerid into v_Cus_
	from users
	where users.id=p_UserID;

	select 
	(select count(id) from users where customerid=v_Cus_ and ifnull(isactive,0)=1 and ifnull(deleted,0)=0)activeUsers,
	(select ConcurrentUsersLimit from customersettings where customerid=v_Cus_) availableUsers;
   
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetAlarmNotifications` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetAlarmNotifications`(
	p_UserID int)
BEGIN

	declare v_Cus_ int;

	select customerid into v_Cus_
	from users
	where id=p_UserID;

    select DISTINCT AlarmNotifications.ID, devices.name,AlarmNotifications.value,AlarmNotifications.starttime, AlarmNotifications.cleartime
	,AlarmNotifications.alarmreason
	,(select AlarmTypes.name from AlarmTypes where AlarmTypes.id=AlarmNotifications.alarmtypeid)alarmtype
	,AlarmNotifications.AlarmTypeID,AlarmNotifications.DeviceSensorID,devices.SerialNumber
	,case 
	when (select SensorName from DeviceSensors where AlarmNotifications.DeviceSensorID = DeviceSensors.id)='' then (select name from SensorTypes where sensortypes.id = (select SensorTypeID from DeviceSensors where AlarmNotifications.DeviceSensorID = DeviceSensors.id))
	else  (select SensorName from DeviceSensors where AlarmNotifications.DeviceSensorID = DeviceSensors.id)
	end sensorName
	from AlarmNotifications
	inner join devices
	on devices.id=AlarmNotifications.deviceid
	-- inner join Pe... *** SQLINES FOR EVALUATION USE ONLY *** 
	-- on Permission... *** SQLINES FOR EVALUATION USE ONLY *** 
	-- where Permiss... *** SQLINES FOR EVALUATION USE ONLY *** 
	where devices.CustomerID=v_Cus_
	and AlarmNotifications.Hide=0;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetAllContacts` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetAllContacts`(
	p_UserID int)
BEGIN

    declare v_Cus_ int;

	select users.CustomerID into v_Cus_
	from users
	where id=p_UserID;

	select 0 isSystem, contacts.id contactId, contacts.name, '' username
	,contacts.title,contacts.PhoneNumber,contacts.email,contacts.countryCode
	,(select name  from ContactsWorkHoursMode where ContactsWorkHoursMode.id= contacts.WorkingHoursID)workHoursMode
	,contacts.WorkdayStart,contacts.WorkdayEnd
	,contacts.SMSResendInterval smsResends
	,ifnull(WorkingSun,0)WorkingSun,ifnull(WorkingMon,0)WorkingMon,ifnull(WorkingTue,0)WorkingTue
	,ifnull(WorkingWed,0)WorkingWed,ifnull(WorkingThu,0)WorkingThu,ifnull(WorkingFri,0)WorkingFri
	,ifnull(WorkingSat,0)WorkingSat
	from Contacts
	where contacts.customerid=v_Cus_
	and ifnull(contacts.deleted,0)=0
	union
	select 1,users.id,concat(users.FirstName,' ',users.lastname),username
	,'',users.mobilenumber,Users.Email,Users.countrycode
	,'',0,23,1
	,1,1,1,1,1,1,1
	from users
	where users.CustomerID=v_Cus_
	and ifnull(users.deleted,0)=0;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetAllDeviceSensorsDownloaded` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetAllDeviceSensorsDownloaded`(
	p_SerialNumber int)
BEGIN

	/*Declare @Cu... *** SQLINES FOR EVALUATION USE ONLY *** ID
	from Devices
	where Devices.serialNumber = @SerialNumber

    --get device sensors related to customer and downloaded data
	select DeviceSensors.id
	from SensorDownloadHistory
	inner join Devices
	on Devices.SerialNumber = SensorDownloadHistory.SerialNumber
	inner join DeviceSensors
	on DeviceSensors.DeviceID = Devices.ID
	where Devices.CustomerID=@CustomerID*/

	select SensorTypeID
	from DeviceSensors
	inner join Devices
	on Devices.Id = DeviceSensors.DeviceID
	where SerialNumber = p_SerialNumber;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetAllowedUsers` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetAllowedUsers`(
	p_SerialNumber int)
BEGIN

	-- get all allow... *** SQLINES FOR EVALUATION USE ONLY *** 

	select distinct userid
	from permissionusersitedevicegroup
	inner join devices
	on devices.siteid=permissionusersitedevicegroup.siteid
	where devices.serialnumber=p_SerialNumber;

    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetAllSerialNumbers` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetAllSerialNumbers`(
	p_SerialNumber int)
BEGIN

    declare v_Cus_ int;

	select customerid into v_Cus_
	from devices
	where serialnumber=p_SerialNumber;

	select serialNumber
	from devices
	where customerid=v_Cus_;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetAnalyticsSensors` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetAnalyticsSensors`(
	p_UserID int)
BEGIN

    SELECT DeviceSensorID, IsActive, case char_length(rtrim(Devices.Name)) when 0 then '[No name defined]' else Devices.Name end name, SensorTypes.Name, SensorUnits.Name, Devices.SerialNumber, SensorTypeID 
    FROM AnalyticsSensors 
	INNER JOIN DeviceSensors ON DeviceSensors.ID=AnalyticsSensors.DeviceSensorID 
	INNER JOIN Devices ON Devices.ID=DeviceID 
	INNER JOIN SensorTypes ON SensorTypes.ID=DeviceSensors.SensorTypeID 
	INNER JOIN SensorUnits ON SensorUnits.ID=DeviceSensors.SensorUnitID 
	WHERE UserID=p_UserID;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetApproversReviewersList` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetApproversReviewersList`(
	p_UserID int)
BEGIN

	declare v_Cus_ int;

	select customerid into v_Cus_
	from users
	where id=p_UserID;

    select id,username name
	from Users
	where PermissionGroupID<=3
	and CustomerID=v_Cus_;
    

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetAuditTrailData` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetAuditTrailData`(
	p_UserID int,
	p_start int,
	p_end int)
BEGIN

	declare v_Cus_ int;
	declare v_sta_ datetime(3);
	declare v_end_ datetime(3);
	declare v_Uni_ datetime(3);

	set v_Uni_ = from_unixtime(1);
	set v_sta_ = date_add(v_Uni_,interval p_start second);
	set v_end_ = Date_Add(v_Uni_,interval p_end second);

	select customerid into v_Cus_
	from users
	where id=p_UserID;

	select AuditTrail.id, AuditTrail.userid, username
	,PermissionGroupActionMatrix.PrivilageName actionName
	,(select PermissionActionTypes.name from PermissionActionTypes where PermissionActionTypes.id=PermissionActions.ActionTypeID) actionType
	,AuditTrail.reason additionalInfo
	,ifnull((select partnumber from devicetypes where devicetypes.id = Devices.devicetypeid),'')product
	,devices.serialNumber
	,AuditTrail.ActivityTimeStamp
	from AuditTrail
	inner join PermissionActions
	on PermissionActions.ID = AuditTrail.PermissionActionID
	left join Devices
	on Devices.id = AuditTrail.DeviceID
	inner join PermissionGroupActionMatrix
	on PermissionGroupActionMatrix.PermissionActionID = PermissionActions.ID
	inner join Users
	on Users.id= AuditTrail.UserID
	where users.customerid=v_Cus_
	and AuditTrail.ActivityTimeStamp>=v_sta_
	and AuditTrail.ActivityTimeStamp<=v_end_;


   
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetBaseSensor` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetBaseSensor`(
	p_FamilyTypeID int)
BEGIN

	select BaseSensors.id,BaseSensors.name
	from BaseSensors
	inner join BaseSenorsFamily
	on BaseSenorsFamily.BaseSensorID = BaseSensors.ID
	where BaseSenorsFamily.FamilyTypeID = p_FamilyTypeID;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetCalibrationCertificateDefault` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetCalibrationCertificateDefault`(
	p_UserID int)
BEGIN

	declare v_Cus_ int;

	select customerid into v_Cus_
	from users
	where id=p_UserID;
   
    select ifnull(calibtemperature,0) temperature,ifnull(calibhumidity,0) humidity,ifnull(calibfourtecwaxseal,0) includeinreport
	,ifnull(calibmanufacture,'') manufacture,ifnull(calibmodel,'') model,ifnull(calibnist,0) nist,ifnull(calibproductname,'') productname,ifnull(calibserialnumber,0) serialnumber
	from customersettings
	where customerid= v_Cus_;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetCalibrationExpiryReminder` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetCalibrationExpiryReminder`(
	p_UserID int)
BEGIN

	declare v_Cus_ int;

	select customerid into v_Cus_
	from users
	where users.id=p_UserID;

	select ifnull(IsCalibrationReminderActive,0) reminderIsOn,
	case ifnull(CalibrationReminderInMonths,0)
		when 3 then '3_MONTH'
		when 6 then '6_MONTH'
		when 9 then '9_MONTH'
		when 12 then '12_MONTH'
		end reminderMode
	from users
	where id=p_UserID;
   
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetCategories` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetCategories`()
BEGIN
	
	select id, name 
	from categories;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetContact` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetContact`(
	p_ContactId int)
BEGIN

    SELECT `ID`
      ,`Name`
      ,ifnull(`Title`,'')Title
      ,ifnull(`Phone`,0)Phone
      ,ifnull(`Email`,'')Email
      ,ifnull(`WeekDayStart`,0)WeekDayStart
      ,ifnull(`WorkdayStart`,'')WorkdayStart
      ,ifnull(`WorkdayEnd`,'')WorkdayEnd
      ,ifnull(`OutOfOfficeStart`,0)OutOfOfficeStart
      ,ifnull(`OutOfOfficeEnd`,0)OutOfOfficeEnd
      ,ifnull(`SMSResendInterval`,0)SMSResendInterval
      
  FROM Contacts
  where ifnull(deleted,0)=0
  and id=p_ContactId;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetCustomer` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetCustomer`(
	p_Email nvarchar(255))
BEGIN

    select name,email,creationdate,isactive,smscounter
	from customers
	where email=p_Email;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetCustomerBalance` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetCustomerBalance`(
	p_UserID int)
BEGIN

	declare v_Cus_ int;

	select customerid into v_Cus_
	from users
	where id=p_UserID;

	select ifnull(MaxStorageAllowed,0) bundleStorage,0 usageStorage
	, ifnull(MaxUsersAllowed,0) bundleUsers,ifnull((select count(id) from users where customerid=v_Cus_),0) usageUsers
	,ifnull(subscriptiontime,0) bundleExpiry, (timestampdiff(day, date_add(FROM_UNIXTIME(1),interval subscriptionstart second),now())) usageExpiry
	,CustomerID
	from CustomerSettings
	where CustomerID=v_Cus_;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetCustomerByDeviceID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetCustomerByDeviceID`(
	p_DeviceID int)
BEGIN

    select customerid
	from devices
	where id=p_DeviceID;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetCustomerByDeviceSensorID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetCustomerByDeviceSensorID`(
	p_DeviceSensorID int)
BEGIN

    select customerid
	from devices
	inner join DeviceSensors
	on devices.ID = DeviceSensors.DeviceID
	where DeviceSensors.id=p_DeviceSensorID;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetCustomerBySerialNumber` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetCustomerBySerialNumber`(
	p_SerialNumber int)
BEGIN

    select customerid
	from devices
	where SerialNumber=p_SerialNumber;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetCustomerIdByUser` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetCustomerIdByUser`(
	p_UserID int)
BEGIN

    select customerid
	from users
	where id=p_UserID;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetCustomerStorage` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetCustomerStorage`(
	p_SerialNumber int)
BEGIN

    select ifnull(MaxStorageAllowed,1)MaxStorageAllowed, ifnull(DataStorageExpiry,0)DataStorageExpiry
	,CustomerSettings.CustomerID
	from Devices 
	left join CustomerSettings
	on Devices.CustomerID=CustomerSettings.CustomerID
	where Devices.SerialNumber = p_SerialNumber;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetDefinedSensors` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetDefinedSensors`(
	p_UserID int)
BEGIN

	declare v_Cus_ int;

	select customerid into v_Cus_
	from users
	where users.id=p_UserID;

	select DefinedSensors.id  sensorId
	, BaseSensors.name basesensor,BaseSensors.id baseSensorId,DecimalPlaces digits
	,(select name from DeviceFamilyTypes where DeviceFamilyTypes.id = DefinedSensors.FamilyTypeID) familyName
	,DefinedSensors.FamilyTypeID familyId,log1 output1 , log2 output2,DefinedSensors.Name sensorName
	,(select name from SensorUnits where SensorUnits.id = DefinedSensors.SensorUnitID)unitName
	, DefinedSensors.SensorUnitID unitId
	,ref1 real1, ref2 real2

	from DefinedSensors
	inner join BaseSensors
	on BaseSensors.ID = DefinedSensors.BaseSensorID
	where DefinedSensors.CustomerID = v_Cus_;
   
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetDefinedSensorsByDevice` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetDefinedSensorsByDevice`(
	p_SerialNumber int)
BEGIN

    select definedsensors.ID, concat(basesensors.Name,' ',SensorUnits.Name,' ', definedsensors.name) DefinedSensorName
	from devices
	inner join devicetypes
	on devices.devicetypeid=devicetypes.id
	left join definedsensors
	on definedsensors.FamilyTypeID = devicetypes.FamilyTypeID
	inner join SensorUnits
	on SensorUnits.ID = definedsensors.SensorUnitID
	inner join basesensors
	on basesensors.id = definedsensors.basesensorid
	where serialnumber=p_SerialNumber;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetDeviceAlarmNotifications` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetDeviceAlarmNotifications`( 
	p_ContactID int)
BEGIN

    
	select ifnull(ReceiveEmail,0)ReceiveEmail,ifnull(ReceiveSMS,0)ReceiveSMS,DeviceAlarmNotifications.DeviceSensorID
	,(select DeviceSensors.DeviceID from DeviceSensors where DeviceSensors.id = DeviceAlarmNotifications.DeviceSensorID) deviceId
	from DeviceAlarmNotifications
	where DeviceAlarmNotifications.ContactID = p_ContactID;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetDeviceAutoSetup` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetDeviceAutoSetup`(

	p_UserID int)
BEGIN

	Declare v_Cus_ int;

	select customerid into v_Cus_
	from users
	where id=p_UserID;

	if not exists (select  id from DeviceAutoSetup where customerid=v_Cus_ limit 1)
	then
		insert into DeviceAutoSetup(customerid,devicetypeid,setupid,usagecounter,isautosetup,firmwareversion)
		select v_Cus_,DeviceTypes.id,'',0,0,fwversion
		from DeviceTypes;
	end if;

	select DeviceAutoSetup.id, (select name from DeviceTypes where id=DeviceAutoSetup.devicetypeid) deviceType
	,ifnull(setupid,'')setupid, usagecounter,isautosetup,firmwareversion,devicetypeid
	from DeviceAutoSetup
	where customerid = v_Cus_;
	

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetDeviceFamilyTypes` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetDeviceFamilyTypes`()
BEGIN

	select id,name
	from DeviceFamilyTypes
	where HasExternalSensor=1;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetDevices` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetDevices`(
	p_SerialNumber int)
BEGIN

    

	
    
		select devices.id,devices.serialnumber,devices.name,devices.siteid,isrunning
		,ifnull((select count(hide) from AlarmNotifications where AlarmNotifications.deviceid = devices.id and hide=0),1)CurrentAlarmState
		,ifnull(InCalibrationMode,0)InCalibrationMode
		, devicetypes.name devicetype
		, devicetypes.imagename devicetypename
		, devicetypes.partnumber partnumber
		,(select devicestatuses.name from devicestatuses where devicestatuses.id = devices.statusid)devicestatus
		, case when devicetypes.fwversion>devices.firmwareversion 
		  then 1
		  else 
			case when devicetypes.buildnumber>devices.buildnumber
			then 1
			else 0
			end
		  end FirmwareUpdateNeeded
		,islocked,firmwareversion,devices.buildnumber,devicetypes.fwversion,DeviceTypes.ID devicetypeid,ifnull(devices.InFwUpdate,0)InFwUpdate
		,batterylevel
		,(select timezones.value from timezones where timezones.id = devices.timezoneid)timezone
		,ifnull(longitude,0)longitude,ifnull(latitude,0)latitude,ifnull(iconxcoordinate,0)iconxcoordinate,ifnull(iconycoordinate,0)iconycoordinate
		-- DeviceMicroX
		,DeviceMicroX.CyclicMode, DeviceMicroX.PushToRun,DeviceMicroX.TimerRunEnabled, DeviceMicroX.TimerStart
		,DeviceMicroX.BoomerangEnabled, DeviceMicroX.BoomerangAuthor, DeviceMicroX.BoomerangCelsiusMode, DeviceMicroX.BoomerangComment,DeviceMicroX.BoomerangContacts, DeviceMicroX.BoomerangDisplayAlarmLevels,DeviceMicroX.BoomerangUTC
		-- DeviceLogger
		,DeviceLogger.`Interval` samplerateinsec,DeviceLogger.CelsiusMode -- case isnull(C... *** SQLINES FOR EVALUATION USE ONLY *** 
		-- DeviceMicroLo... *** SQLINES FOR EVALUATION USE ONLY *** 
		,DeviceMicroLog.AveragePoints sampleavgpoints,DeviceMicroLog.DeepSleepMode,DeviceMicroLog.EnableLEDOnAlarm, DeviceMicroLog.MemorySize, DeviceMicroLog.ShowMinMax, DeviceMicroLog.ShowPast24HMinMax, DeviceMicroLog.StopOnDisconnect, DeviceMicroLog.StopOnKeyPress, DeviceMicroLog.UnitRequiresSetup 
		,DeviceMicroLog.LCDConfiguration micrologLCDConfig
		-- DevicePicoLit... *** SQLINES FOR EVALUATION USE ONLY *** 
		, DevicePicoLite.LEDConfig, DevicePicoLite.RunDelay, DevicePicoLite.RunTime, DevicePicoLite.SetupTime
		,ifnull(DevicePicoLite.StopOnKeyPress,0) picoStopOnKeyPress
		-- DeviceMicroLi... *** SQLINES FOR EVALUATION USE ONLY *** 
		,ifnull(DeviceMicroLite.ShowMinMax,0) microliteShowMinMax,ifnull(DeviceMicroLite.ShowPast24H,0) microliteShowPast24H,DeviceMicroLite.MemorySize microliteMemorySize,DeviceMicroLite.EnableLEDOnAlarm microliteEnableLEDOnAlarm,DeviceMicroLite.LCDConfig microliteLCDConfig
		,DeviceMicroLite.StopOnDisconnect microliteStopOnDisconnect,ifnull(DeviceMicroLite.StopOnKeyPress,0) microliteStopOnKeyPress,DeviceMicroLite.UnitRequiresSetup microliteUnitRequiresSetup, DeviceMicroLite.AveragePoints microliteAveragePoints
		,ifnull(devices.alarmdelay,0)alarmdelay
		,DeviceMicroX.LoggerUTC

		-- -------------... *** SQLINES FOR EVALUATION USE ONLY *** 
		,devicesensors.id deviceSensorID,devicesensors.SensorIndex
		,case(devicesensors.sensortypeid)
		  when 9
			then 
				DefinedSensors.Name
		  else
				case 
					when char_length(rtrim(ifnull(devicesensors.sensorname,''))) =0 then SensorTypes.name 
					else devicesensors.sensorname 
				end 
		  end sensorname 
		,fnIsExternalSensor(devicesensors.sensortypeid) IsExternalSensor
		,devicesensors.sensortypeid
		, case(devicesensors.sensortypeid)
		  when 9
			then
				(select SensorUnits.name from SensorUnits where SensorUnits.id = DefinedSensors.SensorUnitID)
			else
				(select SensorUnits.name from SensorUnits where SensorUnits.id = devicesensors.SensorUnitID)
		  end measurementunit
		,(select SensorTypes.name from SensorTypes where SensorTypes.id = devicesensors.sensortypeid) sensortype
		,case IsAlarmEnabled when 0 then null else PreLowValue end PreLowValue
		,case IsAlarmEnabled when 0 then null else LowValue end LowValue
		,case IsAlarmEnabled when 0 then null else PreHighValue end PreHighValue
		,case IsAlarmEnabled when 0 then null else HighValue end HighValue
		,case IsAlarmEnabled when 0 then null else PreDelayValue end PreDelayValue
		,case IsAlarmEnabled when 0 then null else DelayValue end DelayValue
		,case IsAlarmEnabled when 0 then null else BuzzDuration end BuzzDuration
		,IsAlarmEnabled,ifnull(devicesensors.MinValue,0)minvalue,ifnull(devicesensors.`MaxValue`,0)`maxvalue`,CalibrationExpiry,isenabled
		,ifnull(AlarmNotifications.Silent,0)Silent
		,DefinedSensors.FamilyTypeID
		,ifnull(DefinedSensors.DecimalPlaces,0)DecimalPlaces
		,DefinedSensors.Gain, DefinedSensors.Offset
		,ifnull(DefinedSensors.Log1,0)Log1, ifnull(DefinedSensors.Log2,0)Log2
		,ifnull(DefinedSensors.Ref1,0)Ref1, ifnull(DefinedSensors.Ref2,0)Ref2
		-- -------------... *** SQLINES FOR EVALUATION USE ONLY *** 

		from devices 

		-- -------------... *** SQLINES FOR EVALUATION USE ONLY *** 
		inner join devicesensors
		on devicesensors.deviceid =devices.id
		left join SensorAlarm
		on SensorAlarm.DeviceSensorID=DeviceSensors.ID
		left join AlarmNotifications
		on AlarmNotifications.DeviceSensorID = devicesensors.ID
		inner join SensorTypes
		on devicesensors.SensorTypeID = SensorTypes.ID
		left join DefinedSensors
		on DefinedSensors.ID = devicesensors.DefinedSensorID
		-- -------------... *** SQLINES FOR EVALUATION USE ONLY *** 

		inner join devicetypes
		on devicetypes.id= devices.devicetypeid
		left join DeviceMicroX
		on DeviceMicroX.DeviceID = Devices.ID
		left join DeviceLogger
		on DeviceLogger.DeviceID = Devices.ID
		left join DeviceMicroLog
		on DeviceMicroLog.DeviceID = Devices.ID
		left join DevicePicoLite
		on DevicePicoLite.DeviceID = Devices.ID
		left join DeviceMicroLite
		on DeviceMicroLite.DeviceID= Devices.ID  

	  where devices.serialnumber = p_SerialNumber; 
		
	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetDevicesByCustomer` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetDevicesByCustomer`(
	p_UserID int)
BEGIN

    declare v_Cus_ int;

	select   customerid into v_Cus_
	from users
	where id=p_UserID;

	select devices.id,devices.serialnumber,devices.name,devices.siteid,isrunning,ifnull(InCalibrationMode,0)InCalibrationMode
		, devicetypes.name devicetype
		,ifnull((select count(hide) from AlarmNotifications where AlarmNotifications.deviceid = devices.id and hide=0),1)CurrentAlarmState
		, devicetypes.imagename devicetypename
		, devicetypes.partnumber partnumber
		,(select devicestatuses.name from devicestatuses where devicestatuses.id = devices.statusid)devicestatus
		, case when devicetypes.fwversion>devices.firmwareversion 
		  then 1
		  else 
			case when devicetypes.buildnumber>devices.buildnumber
			then 1
			else 0
			end
		  end FirmwareUpdateNeeded
		,islocked,firmwareversion,devices.buildnumber,devicetypes.fwversion,DeviceTypes.ID devicetypeid,ifnull(devices.InFwUpdate,0)InFwUpdate
		,batterylevel
		,(select timezones.value from timezones where timezones.id = devices.timezoneid)timezone
		,ifnull(longitude,0)longitude,ifnull(latitude,0)latitude,ifnull(iconxcoordinate,0)iconxcoordinate,ifnull(iconycoordinate,0)iconycoordinate
		-- DeviceMicroX
		,DeviceMicroX.CyclicMode, DeviceMicroX.PushToRun,DeviceMicroX.TimerRunEnabled, DeviceMicroX.TimerStart
		,DeviceMicroX.BoomerangEnabled, DeviceMicroX.BoomerangAuthor, DeviceMicroX.BoomerangCelsiusMode, DeviceMicroX.BoomerangComment,DeviceMicroX.BoomerangContacts, DeviceMicroX.BoomerangDisplayAlarmLevels,DeviceMicroX.BoomerangUTC
		-- DeviceLogger
		,`DeviceLogger.Interval` samplerateinsec,DeviceLogger.CelsiusMode -- case isnull(C... *** SQLINES FOR EVALUATION USE ONLY *** 
		-- DeviceMicroLo... *** SQLINES FOR EVALUATION USE ONLY *** 
		,DeviceMicroLog.AveragePoints sampleavgpoints,DeviceMicroLog.DeepSleepMode,DeviceMicroLog.EnableLEDOnAlarm, DeviceMicroLog.MemorySize, DeviceMicroLog.ShowMinMax, DeviceMicroLog.ShowPast24HMinMax, DeviceMicroLog.StopOnDisconnect, DeviceMicroLog.StopOnKeyPress, DeviceMicroLog.UnitRequiresSetup 
		,DeviceMicroLog.LCDConfiguration micrologLCDConfig
		-- DevicePicoLit... *** SQLINES FOR EVALUATION USE ONLY *** 
		, DevicePicoLite.LEDConfig, DevicePicoLite.RunDelay, DevicePicoLite.RunTime, DevicePicoLite.SetupTime
		,ifnull(DevicePicoLite.StopOnKeyPress,0) picoStopOnKeyPress
		-- DeviceMicroLi... *** SQLINES FOR EVALUATION USE ONLY *** 
		,ifnull(DeviceMicroLite.ShowMinMax,0) microliteShowMinMax,ifnull(DeviceMicroLite.ShowPast24H,0) microliteShowPast24H,DeviceMicroLite.MemorySize microliteMemorySize,DeviceMicroLite.EnableLEDOnAlarm microliteEnableLEDOnAlarm,DeviceMicroLite.LCDConfig microliteLCDConfig
		,DeviceMicroLite.StopOnDisconnect microliteStopOnDisconnect,ifnull(DeviceMicroLite.StopOnKeyPress,0) microliteStopOnKeyPress,DeviceMicroLite.UnitRequiresSetup microliteUnitRequiresSetup, DeviceMicroLite.AveragePoints microliteAveragePoints
		,ifnull(devices.alarmdelay,0)alarmdelay
		,DeviceMicroX.LoggerUTC
		from devices 
		inner join devicetypes
		on devicetypes.id= devices.devicetypeid
		inner join PermissionUserSites
		on devices.siteid = PermissionUserSites.siteid 
		left join DeviceMicroX
		on DeviceMicroX.DeviceID = Devices.ID
		left join DeviceLogger
		on DeviceLogger.DeviceID = Devices.ID
		left join DeviceMicroLog
		on DeviceMicroLog.DeviceID = Devices.ID
		left join DevicePicoLite
		on DevicePicoLite.DeviceID = Devices.ID
		left join DeviceMicroLite
		on DeviceMicroLite.DeviceID= Devices.ID
	where devices.customerid=v_Cus_;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetDevicesBySite` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetDevicesBySite`(
	p_SiteID int,
	p_UserID int)
BEGIN
	-- SET NOCOUNT ... *** SQLINES FOR EVALUATION USE ONLY *** 
	-- interfering ... *** SQLINES FOR EVALUATION USE ONLY *** 

	declare v_Cus_ int;

	select customerid into v_Cus_
	from users
	where users.id=p_UserID;

	select devices.id,devices.serialnumber,devices.name,devices.siteid,isrunning,ifnull(InCalibrationMode,0)InCalibrationMode
		, devicetypes.name devicetype
		,ifnull((select count(hide) from AlarmNotifications where AlarmNotifications.deviceid = devices.id and hide=0),1)CurrentAlarmState
		, devicetypes.imagename devicetypename
		, devicetypes.partnumber partnumber
		,(select devicestatuses.name from devicestatuses where devicestatuses.id = devices.statusid)devicestatus
		, case when devicetypes.fwversion>devices.firmwareversion 
		  then 1
		  else 
			case when devicetypes.buildnumber>devices.buildnumber
			then 1
			else 0
			end
		  end FirmwareUpdateNeeded
		,islocked,firmwareversion,devices.buildnumber,devicetypes.fwversion,DeviceTypes.ID devicetypeid,ifnull(devices.InFwUpdate,0)InFwUpdate
		,batterylevel
		,(select timezones.value from timezones where timezones.id = devices.timezoneid)timezone
		,ifnull(longitude,0)longitude,ifnull(latitude,0)latitude,ifnull(iconxcoordinate,0)iconxcoordinate,ifnull(iconycoordinate,0)iconycoordinate
		-- DeviceMicroX
		,DeviceMicroX.CyclicMode, DeviceMicroX.PushToRun,DeviceMicroX.TimerRunEnabled, DeviceMicroX.TimerStart
		,DeviceMicroX.BoomerangEnabled, DeviceMicroX.BoomerangAuthor, DeviceMicroX.BoomerangCelsiusMode, DeviceMicroX.BoomerangComment,DeviceMicroX.BoomerangContacts, DeviceMicroX.BoomerangDisplayAlarmLevels,DeviceMicroX.BoomerangUTC
		-- DeviceLogger
		,`DeviceLogger.Interval` samplerateinsec,DeviceLogger.CelsiusMode -- case isnull(C... *** SQLINES FOR EVALUATION USE ONLY *** 
		-- DeviceMicroLo... *** SQLINES FOR EVALUATION USE ONLY *** 
		,DeviceMicroLog.AveragePoints sampleavgpoints,DeviceMicroLog.DeepSleepMode,DeviceMicroLog.EnableLEDOnAlarm, DeviceMicroLog.MemorySize, DeviceMicroLog.ShowMinMax, DeviceMicroLog.ShowPast24HMinMax, DeviceMicroLog.StopOnDisconnect, DeviceMicroLog.StopOnKeyPress, DeviceMicroLog.UnitRequiresSetup 
		,DeviceMicroLog.LCDConfiguration micrologLCDConfig
		-- DevicePicoLit... *** SQLINES FOR EVALUATION USE ONLY *** 
		, DevicePicoLite.LEDConfig, DevicePicoLite.RunDelay, DevicePicoLite.RunTime, DevicePicoLite.SetupTime
		,ifnull(DevicePicoLite.StopOnKeyPress,0) picoStopOnKeyPress
		-- DeviceMicroLi... *** SQLINES FOR EVALUATION USE ONLY *** 
		,ifnull(DeviceMicroLite.ShowMinMax,0) microliteShowMinMax,ifnull(DeviceMicroLite.ShowPast24H,0) microliteShowPast24H,DeviceMicroLite.MemorySize microliteMemorySize,DeviceMicroLite.EnableLEDOnAlarm microliteEnableLEDOnAlarm,DeviceMicroLite.LCDConfig microliteLCDConfig
		,DeviceMicroLite.StopOnDisconnect microliteStopOnDisconnect,ifnull(DeviceMicroLite.StopOnKeyPress,0) microliteStopOnKeyPress,DeviceMicroLite.UnitRequiresSetup microliteUnitRequiresSetup, DeviceMicroLite.AveragePoints microliteAveragePoints
		,ifnull(devices.alarmdelay,0)alarmdelay
		,DeviceMicroX.LoggerUTC

		-- device senso... *** SQLINES FOR EVALUATION USE ONLY *** 
		-- -------------... *** SQLINES FOR EVALUATION USE ONLY *** 
		,devicesensors.id deviceSensorID,devicesensors.SensorIndex
		,case(devicesensors.sensortypeid)
		  when 9
			then 
				DefinedSensors.Name
		  else
				case 
					when char_length(rtrim(ifnull(devicesensors.sensorname,''))) =0 then SensorTypes.name 
					else devicesensors.sensorname 
				end 
		  end sensorname 
		,dbo.fnIsExternalSensor(devicesensors.sensortypeid) IsExternalSensor
		,devicesensors.sensortypeid
		, case(devicesensors.sensortypeid)
		  when 9
			then
				(select SensorUnits.name from SensorUnits where SensorUnits.id = DefinedSensors.SensorUnitID)
			else
				(select SensorUnits.name from SensorUnits where SensorUnits.id = devicesensors.SensorUnitID)
		  end measurementunit
		,(select SensorTypes.name from SensorTypes where SensorTypes.id = devicesensors.sensortypeid) sensortype
		,case IsAlarmEnabled when 0 then null else PreLowValue end PreLowValue
		,case IsAlarmEnabled when 0 then null else LowValue end LowValue
		,case IsAlarmEnabled when 0 then null else PreHighValue end PreHighValue
		,case IsAlarmEnabled when 0 then null else HighValue end HighValue
		,case IsAlarmEnabled when 0 then null else PreDelayValue end PreDelayValue
		,case IsAlarmEnabled when 0 then null else DelayValue end DelayValue
		,case IsAlarmEnabled when 0 then null else BuzzDuration end BuzzDuration
		,IsAlarmEnabled,ifnull(devicesensors.MinValue,0)minvalue,ifnull(`devicesensors.MaxValue`,0)`maxvalue`,CalibrationExpiry,isenabled
		,ifnull(AlarmNotifications.Silent,0)Silent
		,DefinedSensors.FamilyTypeID
		,ifnull(DefinedSensors.DecimalPlaces,0)DecimalPlaces
		,DefinedSensors.Gain, DefinedSensors.Offset
		,ifnull(DefinedSensors.Log1,0)Log1, ifnull(DefinedSensors.Log2,0)Log2
		,ifnull(DefinedSensors.Ref1,0)Ref1, ifnull(DefinedSensors.Ref2,0)Ref2
		-- -------------... *** SQLINES FOR EVALUATION USE ONLY *** 

		from devices 
		
		-- -------------... *** SQLINES FOR EVALUATION USE ONLY *** 
		inner join devicesensors
		on devicesensors.deviceid =devices.id
		left join SensorAlarm
		on SensorAlarm.DeviceSensorID=DeviceSensors.ID
		left join AlarmNotifications
		on AlarmNotifications.DeviceSensorID = devicesensors.ID
		inner join SensorTypes
		on devicesensors.SensorTypeID = SensorTypes.ID
		left join DefinedSensors
		on DefinedSensors.ID = devicesensors.DefinedSensorID
		-- -------------... *** SQLINES FOR EVALUATION USE ONLY *** 

		inner join devicetypes
		on devicetypes.id= devices.devicetypeid
		inner join PermissionUserSites
		on devices.siteid = PermissionUserSites.siteid 
		left join DeviceMicroX
		on DeviceMicroX.DeviceID = Devices.ID
		left join DeviceLogger
		on DeviceLogger.DeviceID = Devices.ID
		left join DeviceMicroLog
		on DeviceMicroLog.DeviceID = Devices.ID
		left join DevicePicoLite
		on DevicePicoLite.DeviceID = Devices.ID
		left join DeviceMicroLite
		on DeviceMicroLite.DeviceID= Devices.ID
		/*left join P... *** SQLINES FOR EVALUATION USE ONLY *** iteid=PermissionUserSiteDeviceGroup.siteid
		and PermissionUserSiteDeviceGroup.userid=  @UserID*/
		where devices.CustomerID = v_Cus_;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetDevicesByUser` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetDevicesByUser`(
	p_UserID int)
BEGIN

		declare v_Use_ nvarchar(50);
		declare v_Cus_ int;

		select username,customerid into v_Use_, v_Cus_
		from users
		where id=p_UserID;

    	select devices.id,devices.serialnumber,devices.name,devices.siteid,isrunning,ifnull(InCalibrationMode,0)InCalibrationMode
		-- ,Hide as Curr... *** SQLINES FOR EVALUATION USE ONLY *** 
		,ifnull((select count(hide) from AlarmNotifications where AlarmNotifications.deviceid = devices.id and hide=0),1)CurrentAlarmState
		, devicetypes.name devicetype
		, devicetypes.imagename devicetypename
		, devicetypes.partnumber partnumber
		,(select devicestatuses.name from devicestatuses where devicestatuses.id = devices.statusid)devicestatus
		, case when devicetypes.fwversion>devices.firmwareversion 
		  then 1
		  else 
			case when devicetypes.buildnumber>devices.buildnumber
			then 1
			else 0
			end
		  end FirmwareUpdateNeeded
		,islocked,firmwareversion,devices.buildnumber,devicetypes.fwversion,DeviceTypes.ID devicetypeid,ifnull(devices.InFwUpdate,0)InFwUpdate
		,batterylevel
		,(select timezones.value from timezones where timezones.id = devices.timezoneid)timezone
		,ifnull(longitude,0)longitude,ifnull(latitude,0)latitude,ifnull(iconxcoordinate,0)iconxcoordinate,ifnull(iconycoordinate,0)iconycoordinate
		-- DeviceMicroX
		,DeviceMicroX.CyclicMode, DeviceMicroX.PushToRun,DeviceMicroX.TimerRunEnabled, DeviceMicroX.TimerStart
		,DeviceMicroX.BoomerangEnabled, DeviceMicroX.BoomerangAuthor, DeviceMicroX.BoomerangCelsiusMode, DeviceMicroX.BoomerangComment,DeviceMicroX.BoomerangContacts, DeviceMicroX.BoomerangDisplayAlarmLevels,DeviceMicroX.BoomerangUTC
		-- DeviceLogger
		,DeviceLogger.`Interval` samplerateinsec,DeviceLogger.CelsiusMode -- case isnull(C... *** SQLINES FOR EVALUATION USE ONLY *** 
		-- DeviceMicroLo... *** SQLINES FOR EVALUATION USE ONLY *** 
		,DeviceMicroLog.AveragePoints sampleavgpoints,DeviceMicroLog.DeepSleepMode,DeviceMicroLog.EnableLEDOnAlarm, DeviceMicroLog.MemorySize, DeviceMicroLog.ShowMinMax, DeviceMicroLog.ShowPast24HMinMax, DeviceMicroLog.StopOnDisconnect,ifnull(DeviceMicroLog.StopOnKeyPress,0)StopOnKeyPress, DeviceMicroLog.UnitRequiresSetup 
		,DeviceMicroLog.LCDConfiguration micrologLCDConfig
		-- DevicePicoLit... *** SQLINES FOR EVALUATION USE ONLY *** 
		, DevicePicoLite.LEDConfig, DevicePicoLite.RunDelay, DevicePicoLite.RunTime, DevicePicoLite.SetupTime
		,ifnull(DevicePicoLite.StopOnKeyPress,0) picoStopOnKeyPress
		-- DeviceMicroLi... *** SQLINES FOR EVALUATION USE ONLY *** 
		,ifnull(DeviceMicroLite.ShowMinMax,0) microliteShowMinMax,ifnull(DeviceMicroLite.ShowPast24H,0) microliteShowPast24H,DeviceMicroLite.MemorySize microliteMemorySize,DeviceMicroLite.EnableLEDOnAlarm microliteEnableLEDOnAlarm,DeviceMicroLite.LCDConfig microliteLCDConfig
		,DeviceMicroLite.StopOnDisconnect microliteStopOnDisconnect,ifnull(DeviceMicroLite.StopOnKeyPress,0) microliteStopOnKeyPress,DeviceMicroLite.UnitRequiresSetup microliteUnitRequiresSetup, DeviceMicroLite.AveragePoints microliteAveragePoints
		,ifnull(devices.alarmdelay,0)alarmdelay
		,DeviceMicroX.LoggerUTC

		-- device senso... *** SQLINES FOR EVALUATION USE ONLY *** 
		-- -------------... *** SQLINES FOR EVALUATION USE ONLY *** 
		,devicesensors.id deviceSensorID,devicesensors.SensorIndex
		,case(devicesensors.sensortypeid)
		  when 9
			then 
				DefinedSensors.Name
		  else
				case 
					when char_length(rtrim(ifnull(devicesensors.sensorname,''))) =0 then SensorTypes.name 
					else devicesensors.sensorname 
				end 
		  end sensorname 
		,fnIsExternalSensor(devicesensors.sensortypeid) IsExternalSensor
		,devicesensors.sensortypeid
		, case(devicesensors.sensortypeid)
		  when 9
			then
				(select SensorUnits.name from SensorUnits where SensorUnits.id = DefinedSensors.SensorUnitID)
			else
				(select SensorUnits.name from SensorUnits where SensorUnits.id = devicesensors.SensorUnitID)
		  end measurementunit
		,(select SensorTypes.name from SensorTypes where SensorTypes.id = devicesensors.sensortypeid) sensortype
		,case IsAlarmEnabled when 0 then null else PreLowValue end PreLowValue
		,case IsAlarmEnabled when 0 then null else LowValue end LowValue
		,case IsAlarmEnabled when 0 then null else PreHighValue end PreHighValue
		,case IsAlarmEnabled when 0 then null else HighValue end HighValue
		,case IsAlarmEnabled when 0 then null else PreDelayValue end PreDelayValue
		,case IsAlarmEnabled when 0 then null else DelayValue end DelayValue
		,case IsAlarmEnabled when 0 then null else BuzzDuration end BuzzDuration
		,IsAlarmEnabled,ifnull(devicesensors.MinValue,0)minvalue,ifnull(devicesensors.`MaxValue`,0)`maxvalue`,CalibrationExpiry,isenabled
		,ifnull(AlarmNotifications.Silent,0)Silent
		,DefinedSensors.FamilyTypeID
		,ifnull(DefinedSensors.DecimalPlaces,0)DecimalPlaces
		,DefinedSensors.Gain, DefinedSensors.Offset
		,ifnull(DefinedSensors.Log1,0)Log1, ifnull(DefinedSensors.Log2,0)Log2
		,ifnull(DefinedSensors.Ref1,0)Ref1, ifnull(DefinedSensors.Ref2,0)Ref2
		-- -------------... *** SQLINES FOR EVALUATION USE ONLY *** 

		from devices 
		-- -------------... *** SQLINES FOR EVALUATION USE ONLY *** 
		inner join devicesensors
		on devicesensors.deviceid =devices.id
		left join SensorAlarm
		on SensorAlarm.DeviceSensorID=DeviceSensors.ID
		left join AlarmNotifications
		on AlarmNotifications.DeviceSensorID = devicesensors.ID
		inner join SensorTypes
		on devicesensors.SensorTypeID = SensorTypes.ID
		left join DefinedSensors
		on DefinedSensors.ID = devicesensors.DefinedSensorID
		-- -------------... *** SQLINES FOR EVALUATION USE ONLY *** 

		inner join devicetypes
		on devicetypes.id= devices.devicetypeid
		left join DeviceMicroX
		on DeviceMicroX.DeviceID = Devices.ID
		left join DeviceLogger
		on DeviceLogger.DeviceID = Devices.ID
		left join DeviceMicroLog
		on DeviceMicroLog.DeviceID = Devices.ID
		left join DevicePicoLite
		on DevicePicoLite.DeviceID = Devices.ID
		left join DeviceMicroLite
		on DeviceMicroLite.DeviceID= Devices.ID
		/*left join P... *** SQLINES FOR EVALUATION USE ONLY *** iteid=PermissionUserSiteDeviceGroup.siteid
		and PermissionUserSiteDeviceGroup.userid=  @UserID*/
		where devices.CustomerID = v_Cus_
		order by devices.id;
	
	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetDeviceSensorID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetDeviceSensorID`(
	p_SerialNumber int,
	p_SensorTypeID int)
BEGIN
	

    declare v_Dev_ int;
	declare v_Dev2 int;

	set v_Dev2=0;
	-- get the devic... *** SQLINES FOR EVALUATION USE ONLY *** 
	select id into v_Dev_
	from devices
	where SerialNumber = p_SerialNumber;

	if v_Dev_>0
	then
		-- get the devic... *** SQLINES FOR EVALUATION USE ONLY *** 
		select id into v_Dev2
		from DeviceSensors
		where deviceid = v_Dev_
		and SensorTypeID = p_SensorTypeID;
	end if;

	select v_Dev2;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetDeviceSensorsID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetDeviceSensorsID`(
	p_SerialNumber int)
BEGIN

    declare v_Dev_ int;
	declare v_Dev2 int;
	declare v_Las_ int;

	set v_Dev2=0;
	-- get the devic... *** SQLINES FOR EVALUATION USE ONLY *** 
	select id,ifnull(LastSetupTime,fnUNIX_TIMESTAMP(now()-1)) into v_Dev_, v_Las_
	from devices
	where SerialNumber = p_SerialNumber;

	if v_Dev_>0
	then
		-- get the devic... *** SQLINES FOR EVALUATION USE ONLY *** 
		select SensorTypeID,id,v_Las_ LastSetupTime
		from DeviceSensors
		where deviceid = v_Dev_
		and isEnabled=1;
	end if;

	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetDeviceSetupFiles` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetDeviceSetupFiles`(
	p_UserID int,
	p_DeviceTypeID int/* =0 */)
BEGIN

    declare v_Cus_ int;
	
	select customerid into v_Cus_
	from users
	where id=p_UserID;

	if p_DeviceTypeID=0 
	then
	
		select (select name from devicetypes where devicetypes.id = DeviceSetupFiles.DeviceTypeID) devicetype
		, SetupID,SetupName
		from DeviceSetupFiles
		where CustomerID = v_Cus_;

	else
		select (select name from devicetypes where devicetypes.id = DeviceSetupFiles.DeviceTypeID) devicetype
		, SetupID,SetupName
		from DeviceSetupFiles
		where CustomerID = v_Cus_
		and DeviceSetupFiles.DeviceTypeID = p_DeviceTypeID;
	end if;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetDistributionList` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetDistributionList`(
	p_UserID int)
BEGIN

	declare v_Cus_ int;

	select customerid into v_Cus_
	from users
	where id=p_UserID;

    select id,name
	from Contacts
	where CustomerID=v_Cus_;
    

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetDownloadData` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetDownloadData`(
	p_DeviceSensorID int,
	p_IsLastDownload tinyint unsigned/* =0 */,
	p_StartTime int/* =0 */,
	p_EndTime int/* =0 */,
	p_AlarmStatus int/* =0 */)
BEGIN

	declare v_SQL_ longtext;
	Declare v_Fir_ int;
	declare v_Las_ int;
	
	declare v_Whe_ nvarchar(2000);

	set v_Whe_ = CONCAT(' SensorLogs.DeviceSensorID = ' , cast(p_DeviceSensorID as CHAR(1)));

	if p_IsLastDownload>0 
	then
		select  FirstDownloadTime,LastDownloadTime into v_Fir_, v_Las_
		from SensorDownloadHistory
		where DeviceSensorID=p_DeviceSensorID
		order by creationdate desc limit 1;

		set v_Whe_ = CONCAT(v_Whe_ , ' and SampleTime >= ' , cast(v_Fir_ as char(1)) , ' and SampleTime<= ' , cast(v_Las_ as char(1)));
	end if;
	
	if p_StartTime>0 and p_EndTime>0
	then
		set v_Whe_ = CONCAT(v_Whe_ , ' and SampleTime >= ' , cast(p_StartTime as char(1)) , ' and SampleTime<= ' , cast(p_EndTime as char(1)));
	end if;

	if p_AlarmStatus > 0
	then
		set v_Whe_ = CONCAT(v_Whe_ , ' and AlarmStatus = ' , cast(p_AlarmStatus as char(1)));
	end if;
	-- dateadd(secon... *** SQLINES FOR EVALUATION USE ONLY *** 
	set v_SQL_ = CONCAT(' select Value,SampleTime,IsTimeStamp,Comment TimeStampComment
	,isnull(AlarmStatus,0)AlarmStatus,IsDummy
	from SensorLogs
	where ' , v_Whe_ , '
	order by sampletime asc ');

	SET @stmt_str =  v_SQL_;
	PREPARE stmt FROM @stmt_str;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetGroupContacts` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetGroupContacts`(
	p_UserID int)
BEGIN

    declare v_Cus_ int;

	select users.CustomerID into v_Cus_
	from users
	where id=p_UserID;

	select groups.id GroupID ,groups.name groupName
	,  0 isSystem, contacts.id contactId, contacts.name, '' username
	,contacts.title,contacts.PhoneNumber,contacts.email,contacts.countryCode
	,(select name  from ContactsWorkHoursMode where ContactsWorkHoursMode.id= contacts.WorkingHoursID)workHoursMode
	,contacts.WorkdayStart,contacts.WorkdayEnd
	,contacts.SMSResendInterval smsResends
	,ifnull(WorkingSun,0)WorkingSun,ifnull(WorkingMon,0)WorkingMon,ifnull(WorkingTue,0)WorkingTue
	,ifnull(WorkingWed,0)WorkingWed,ifnull(WorkingThu,0)WorkingThu,ifnull(WorkingFri,0)WorkingFri
	,ifnull(WorkingSat,0)WorkingSat
	from Groups
	inner join ContactDistributionGroups
	on ContactDistributionGroups.groupid=Groups.ID
	inner join contacts
	on contacts.id=ContactDistributionGroups.contactid
	where contacts.CustomerID=v_Cus_
	and ifnull(contacts.deleted,0)=0

	union all
	select groups.id GroupID ,groups.name groupName
	,0,0,'','','',0,'','','',0,0,0,0,0,0,0,0,0,0
	from groups 
	where customerid=v_Cus_

	union all
	
	select groups.id  ,groups.name 
	,  1 , users.id contactId, users.username, '' 
	,'',users.mobilenumber,users.email,users.countryCode
	,''	,0,0
	,0,0,0,0,0,0,0,0
	from groups 
	inner join ContactDistributionGroups
	on ContactDistributionGroups.groupid=Groups.ID
	inner join users
	on users.id=ContactDistributionGroups.contactid
	where groups.customerid=v_Cus_

	order by groupName;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetGroupPermissions` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetGroupPermissions`()
BEGIN
	-- SET NOCOUNT ... *** SQLINES FOR EVALUATION USE ONLY *** 
	-- interfering ... *** SQLINES FOR EVALUATION USE ONLY *** 

    -- Insert state... *** SQLINES FOR EVALUATION USE ONLY *** 
	SELECT PermissionGroups.id, PermissionGroups.Name
	from PermissionGroups
	where PermissionGroups.isadmin=0;
	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetGroupsByContactID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetGroupsByContactID`(
	p_ContactID int)
BEGIN

    select ContactDistributionGroups.GroupID
	from ContactDistributionGroups
	where contactid=p_ContactID;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetLanguages` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetLanguages`()
BEGIN
	-- SET NOCOUNT ... *** SQLINES FOR EVALUATION USE ONLY *** 
	-- interfering ... *** SQLINES FOR EVALUATION USE ONLY *** 

    -- Insert state... *** SQLINES FOR EVALUATION USE ONLY *** 
	SELECT languages.id,languages.name
	from languages;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetLastSensorDownload` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetLastSensorDownload`(
	p_SerialNumber int,
	p_SensorTypeID int)
BEGIN

    if exists (select  id from SensorDownloadHistory 
				where SerialNumber=p_SerialNumber
				and SensorTypeID = p_SensorTypeID limit 1)
	then
		select  FirstDownloadTime,LastDownloadTime
		from SensorDownloadHistory
		where SerialNumber=p_SerialNumber
		and SensorTypeID = p_SensorTypeID
		order by creationdate desc limit 1;
	else
		select LastSetupTime FirstDownloadTime,0 LastDownloadTime
		from devices
		where SerialNumber=p_SerialNumber;
	end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetLastSensorLog` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetLastSensorLog`(
	p_SensorID int)
BEGIN

	declare v_SQL_ longtext;

	set v_SQL_ = '
     select value , max(sampletime) sampletime,istimestamp,timestampcomment
	from sensorlogs ';

	if p_SensorID>0 then

		set v_SQL_= Concat(v_SQL_,' where sensorid = ' , cast( p_SensorID as char(1)));
	end if;
	set v_SQL_ = Concat(v_sql_, ' Group by value, istimestamp,timestampcomment ' );

	set @stmt_str =  v_SQL_;
	prepare stmt from @stmt_str;
	execute stmt;
	deallocate prepare stmt;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetMatrix` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetMatrix`(
	p_UserID int,
	p_groupLevel int)
BEGIN

	declare v_Cus_ int;

	select customerid into v_Cus_
	from users
	where users.id=p_UserID;
   
   select PrivilageName,comment,isallowed,PermissionGroupActionMatrix.PermissionActionID,PermissionGroupActionMatrix.PermissionGroupID
   from PermissionGroupActionMatrix
   inner join PermissionGroupActions
   on PermissionGroupActions.PermissionActionID = PermissionGroupActionMatrix.PermissionActionID
   and PermissionGroupActions.PermissionGroupID = PermissionGroupActionMatrix.PermissionGroupID
   inner join PermissionGroups
   on PermissionGroups.id = PermissionGroupActions.PermissionGroupID
   where PermissionGroupActions.CustomerID=v_Cus_
   and PermissionGroups.levelid=p_groupLevel
   order by PrivilageName;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetNextSerialNumber` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetNextSerialNumber`(
	p_AllocateNumber int /* = 0 */)
BEGIN

	declare v_Ser_ int;
	declare v_New_ int;

    if not exists(select  serialnumber from SerialNumberGeneration limit 1)
	then
		-- get the max s... *** SQLINES FOR EVALUATION USE ONLY *** 
		select max(serialnumber) into v_Ser_ 
		from devices;
	else
		-- get the seria... *** SQLINES FOR EVALUATION USE ONLY *** 
		select serialnumber into v_Ser_
		from SerialNumberGeneration;
	end if;

	-- advance the s... *** SQLINES FOR EVALUATION USE ONLY *** 
	set v_New_ = v_Ser_ + 1;
	-- if the alloca... *** SQLINES FOR EVALUATION USE ONLY *** 
	if p_AllocateNumber>0
	then
		update  SerialNumberGeneration
		set serialnumber = v_New_ + p_AllocateNumber;
	end if;
	-- return the ne... *** SQLINES FOR EVALUATION USE ONLY *** 
	select v_New_;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetNotificationContacts` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetNotificationContacts`(
	p_SerialNumber int,
	p_SensorTypeID int/* =0 */)
BEGIN

    declare v_Dev_ int;
	declare v_Dev2 int;
	
	select id into v_Dev_
	from devices
	where SerialNumber = p_SerialNumber;

	select ID into v_Dev2
	from DeviceSensors
	where DeviceID=v_Dev_
	and SensorTypeID =p_SensorTypeID;

	if v_Dev2>0
	then
		select email,ReceiveEmail,ReceiveSMS
		,SMSResendInterval,CountryCode, PhoneNumber
		, ifnull(AlarmEmailSent,0)EmailSent,ifnull( AlarmSMSSent,0)SMSSent, DeviceAlarmNotifications.ContactID
		, ifnull(ReceiveBatteryLow,0) ReceiveBatteryLow, ifnull(BatteryLowValue,0)BatteryLowValue
		, ifnull(BatteryEmailSent,0) BatteryEmailSent, ifnull(BatterySMSSent,0) BatterySMSSent
		,SMSResendInterval
		from DeviceAlarmNotifications
		left join DeviceAlarmNotificationTrail
		on DeviceAlarmNotificationTrail.DeviceSensorID = DeviceAlarmNotifications.DeviceSensorID
		and DeviceAlarmNotificationTrail.ContactID = DeviceAlarmNotifications.ContactID
		inner join Contacts
		on DeviceAlarmNotifications.ContactID= Contacts.ID
		where DeviceAlarmNotifications.DeviceSensorID=v_Dev2;
		
		
	else
		if v_Dev2=0
		then
			select email,ReceiveEmail,ReceiveSMS
			,SMSResendInterval,CountryCode, PhoneNumber
			, 0 EmailSent,0 SMSSent, DeviceAlarmNotifications.ContactID
			, ifnull(ReceiveBatteryLow,0) ReceiveBatteryLow, ifnull(BatteryLowValue,0)BatteryLowValue
			, ifnull(BatteryEmailSent,0) BatteryEmailSent, ifnull(BatterySMSSent,0) BatterySMSSent
			,SMSResendInterval
			from DeviceAlarmNotifications
			inner join Contacts
			on DeviceAlarmNotifications.ContactID= Contacts.ID
			where DeviceAlarmNotifications.DeviceSensorID=v_Dev2;
		end if;
	end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetPasswordExpiry` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetPasswordExpiry`(
	p_UserID int)
BEGIN

	declare v_Cus_ int;

	select customerid into v_Cus_
	from users
	where users.id=p_UserID;

	select ifnull(passwordMinLength,8) pwdMinLength,	passwordExpiryByDays 
	from customersettings 
	where customerid=v_Cus_;
   
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetPermissonRoles` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetPermissonRoles`(
	p_UserID int)
BEGIN

    select id , name
	from PermissionRoles
where id>1;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetPrivilegesGroups` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetPrivilegesGroups`(
	p_UserID int)
BEGIN

	declare v_Cus_ int;

	select customerid into v_Cus_
	from users
	where users.id=p_UserID;

	select levelid groupLevel, name groupName,ifnull(IsReadOnly,0)IsReadOnly,PermissionGroups.ID
	from PermissionGroups
	where PermissionGroups.levelid>0
	and (CustomerID=v_Cus_ or CustomerID=0);
   
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetReportApproversReviewers` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetReportApproversReviewers`(
	p_ReportTemplateID int)
BEGIN

	select ReportTemplateProcessUsers.userid,users.username,processtypeid
	from ReportTemplateProcessUsers
	inner join users
	on users.id = ReportTemplateProcessUsers.userid
	where ReportTemplateID = p_ReportTemplateID;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetReportContacts` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetReportContacts`(
	p_ReportTemplateID int)
BEGIN

	select Contacts.id ,Contacts.name
	from ReportContacts
	inner join Contacts
	on Contacts.id = ReportContacts.contactid
	where ReportContacts.ReportTemplateID = p_ReportTemplateID;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetReportDevices` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetReportDevices`(
	p_ReportTemplateID int)
BEGIN

	select SerialNumber
	from ReportDevices
	where ReportTemplateID = p_ReportTemplateID;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetReportProfiles` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetReportProfiles`(
	p_UserID int,
	p_Filter varchar(50))
BEGIN
	

	if p_Filter = 'ALL'
	then
		select reporttemplates.id profileid
		,(select username from users where id=reporttemplates.userid)createdby
		,reporttemplates.ProfileName name
		,ifnull(reporttemplates.IsActive,0) activity
		,ifnull(reporttemplates.Description,'')description,ifnull(reporttemplates.TimeZoneOffset,0) offset,ifnull(reporttemplates.PageSize,'')pagesize
		,ifnull(reporttemplates.reportdateformat,'') formatdate
		,case ifnull(reporttemplates.TemperatureUnit,1) when 0 then 'F' else 'C' end units
		,ifnull(reporttemplates.emailaslink,0) reportAsLink, ifnull(reporttemplates.EmailAsPDF,0)EmailAsPDF
		,(select name from ReportPeriodTypes where id= reporttemplates.PeriodTypeID) scheduletype
		,ifnull(reporttemplates.PeriodTypeID,0)PeriodTypeID
		,ifnull(periodtime,now())periodtime, ifnull(periodday,0)periodday,ifnull(periodmonth,0)periodmonth,ifnull(periodstart,0)periodstart,ifnull(periodend,0)periodend,ifnull(creationdate,now())creationdate

		from reporttemplates;
	
	else if p_Filter = 'CREATED_BY_YOU'
	then
		select reporttemplates.id profileid
		,(select username from users where id=reporttemplates.userid)createdby
		,reporttemplates.ProfileName name
		,ifnull(reporttemplates.IsActive,0) activity
		,ifnull(reporttemplates.Description,'')description,ifnull(reporttemplates.TimeZoneOffset,0) offset,ifnull(reporttemplates.PageSize,'')pagesize
		,ifnull(reporttemplates.reportdateformat,'') formatdate
		,case ifnull(reporttemplates.TemperatureUnit,1) when 0 then 'F' else 'C' end units
		,ifnull(reporttemplates.emailaslink,0) reportAsLink, ifnull(reporttemplates.EmailAsPDF,0)EmailAsPDF
		,(select name from ReportPeriodTypes where id= reporttemplates.PeriodTypeID) scheduletype
		,ifnull(reporttemplates.PeriodTypeID,0)PeriodTypeID
		,ifnull(periodtime,now())periodtime, ifnull(periodday,0)periodday,ifnull(periodmonth,0)periodmonth,ifnull(periodstart,0)periodstart,ifnull(periodend,0)periodend,ifnull(creationdate,now())creationdate

		from reporttemplates
		where reporttemplates.UserID = p_UserID;
	else if p_Filter = 'ACTIVE_PROFILES'
	then
		select reporttemplates.id profileid
		,(select username from users where id=reporttemplates.userid)createdby
		,reporttemplates.ProfileName name
		,ifnull(reporttemplates.IsActive,0) activity
		,ifnull(reporttemplates.Description,'')description,ifnull(reporttemplates.TimeZoneOffset,0) offset,ifnull(reporttemplates.PageSize,'')pagesize
		,ifnull(reporttemplates.reportdateformat,'') formatdate
		,case ifnull(reporttemplates.TemperatureUnit,1) when 0 then 'F' else 'C' end units
		,ifnull(reporttemplates.emailaslink,0) reportAsLink, ifnull(reporttemplates.EmailAsPDF,0)EmailAsPDF
		,(select name from ReportPeriodTypes where id= reporttemplates.PeriodTypeID) scheduletype
		,ifnull(reporttemplates.PeriodTypeID,0)PeriodTypeID
		,ifnull(periodtime,now())periodtime, ifnull(periodday,0)periodday,ifnull(periodmonth,0)periodmonth,ifnull(periodstart,0)periodstart,ifnull(periodend,0)periodend,ifnull(creationdate,now())creationdate

		from reporttemplates
		where IsActive=1;
	else if p_Filter = 'ONE_TIME_PROFILES'
	then
		select reporttemplates.id profileid
		,(select username from users where id=reporttemplates.userid)createdby
		,reporttemplates.ProfileName name
		,ifnull(reporttemplates.IsActive,0) activity
		,ifnull(reporttemplates.Description,'')description,ifnull(reporttemplates.TimeZoneOffset,0) offset,ifnull(reporttemplates.PageSize,'')pagesize
		,ifnull(reporttemplates.reportdateformat,'') formatdate
		,case ifnull(reporttemplates.TemperatureUnit,1) when 0 then 'F' else 'C' end units
		,ifnull(reporttemplates.emailaslink,0) reportAsLink, ifnull(reporttemplates.EmailAsPDF,0)EmailAsPDF
		,(select name from ReportPeriodTypes where id= reporttemplates.PeriodTypeID) scheduletype
		,ifnull(reporttemplates.PeriodTypeID,0)PeriodTypeID
		,ifnull(periodtime,now())periodtime, ifnull(periodday,0)periodday,ifnull(periodmonth,0)periodmonth,ifnull(periodstart,0)periodstart,ifnull(periodend,0)periodend,ifnull(creationdate,now())creationdate

		from reporttemplates
		where PeriodTypeID = 4; -- custom date a... *** SQLINES FOR EVALUATION USE ONLY *** 
	end if;
 end if;
 end if;
	end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetReportsArchive` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetReportsArchive`(
	p_UserID int,
	p_ReportStartDate int,
	p_ReportEndDate int)
BEGIN

	
	select ReportDeviceHistory.id,ReportTemplates.profilename
	,(select CONCAT(ifnull(FirstName,''),ifnull(LastName,'')) from users where users.id=ReportTemplates.UserID) generatedby
	,ReportDeviceHistory.CreationDate,ReportDevices.SerialNumber
	from ReportTemplates
	inner join ReportDevices
	on ReportDevices.ReportTemplateID = ReportTemplates.ID
	inner join ReportDeviceHistory
	on ReportDeviceHistory.SerialNumber = ReportDevices.SerialNumber
	where ReportTemplates.UserID = p_UserID
	and ReportDeviceHistory.CreationDate>=p_ReportStartDate
	and ReportDeviceHistory.CreationDate<=p_ReportEndDate
	order by ReportDeviceHistory.CreationDate desc;
   
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetReportsReviews` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetReportsReviews`(
	p_UserID int,
	p_ReportType int)
BEGIN

	Declare v_Cus_ int;

	select customerid into v_Cus_
	from users
	where id=p_UserID;

	IF p_ReportType=1 -- only pending
	THEN
		select ReportDeviceHistory.id,ReportTemplates.profilename
		,(select CONCAT(ifnull(FirstName,''),ifnull(LastName,'')) from users where users.id=ReportTemplates.UserID) generatedby
		,ReportDeviceHistory.CreationDate,ReportDevices.SerialNumber
		,(select name from ReportProcessTypes where ReportProcessTypes.ID = ReportTemplateProcessUsers.ProcessTypeID)processType
		,Users.Username
		from ReportTemplates
		inner join ReportDevices
		on ReportDevices.ReportTemplateID = ReportTemplates.ID
		inner join ReportDeviceHistory
		on ReportDeviceHistory.SerialNumber = ReportDevices.SerialNumber
		inner join ReportProcessUsers
		on ReportProcessUsers.ReportDeviceHistoryID = ReportDeviceHistory.ID
		inner join Users
		on Users.ID = ReportProcessUsers.UserID
		inner join ReportTemplateProcessUsers
		on ReportTemplateProcessUsers.ReportTemplateId = ReportTemplates.Id
		and ReportTemplateProcessUsers.UserId = Users.ID
		where Users.CustomerID = v_Cus_
		and ReportProcessUsers.IsConfirmed=0
		order by ReportDeviceHistory.CreationDate desc;
	else if p_ReportType=2  -- waiting appr... *** SQLINES FOR EVALUATION USE ONLY *** 
	then
		select ReportDeviceHistory.id,ReportTemplates.profilename
		,(select CONCAT(ifnull(FirstName,''),ifnull(LastName,'')) from users where users.id=ReportTemplates.UserID) generatedby
		,ReportDeviceHistory.CreationDate,ReportDevices.SerialNumber
		,(select name from ReportProcessTypes where ReportProcessTypes.ID = ReportTemplateProcessUsers.ProcessTypeID)processType
		,(select username from users where users.id= p_UserID)username
		from ReportTemplates
		inner join ReportDevices
		on ReportDevices.ReportTemplateID = ReportTemplates.ID
		inner join ReportDeviceHistory
		on ReportDeviceHistory.SerialNumber = ReportDevices.SerialNumber
		inner join ReportProcessUsers
		on ReportProcessUsers.ReportDeviceHistoryID = ReportDeviceHistory.ID
		inner join ReportTemplateProcessUsers
		on ReportTemplateProcessUsers.ReportTemplateId = ReportTemplates.Id
		and ReportTemplateProcessUsers.UserId = p_UserID
		where ReportProcessUsers.UserID = p_UserID
		and ReportProcessUsers.IsConfirmed=0
		order by ReportDeviceHistory.CreationDate desc;
	else if p_ReportType=3  -- waiting othe... *** SQLINES FOR EVALUATION USE ONLY *** 
	then
		select ReportDeviceHistory.id,ReportTemplates.profilename
		,(select CONCAT(ifnull(FirstName,''),ifnull(LastName,'')) from users where users.id=ReportTemplates.UserID) generatedby
		,ReportDeviceHistory.CreationDate,ReportDevices.SerialNumber
		,(select name from ReportProcessTypes where ReportProcessTypes.ID = ReportTemplateProcessUsers.ProcessTypeID)processType
		,Users.Username
		from ReportTemplates
		inner join ReportDevices
		on ReportDevices.ReportTemplateID = ReportTemplates.ID
		inner join ReportDeviceHistory
		on ReportDeviceHistory.SerialNumber = ReportDevices.SerialNumber
		inner join ReportProcessUsers
		on ReportProcessUsers.ReportDeviceHistoryID = ReportDeviceHistory.ID
		inner join Users
		on Users.ID = ReportProcessUsers.UserID
		inner join ReportTemplateProcessUsers
		on ReportTemplateProcessUsers.ReportTemplateId = ReportTemplates.Id
		and ReportTemplateProcessUsers.UserId = Users.ID
		where Users.CustomerID = v_Cus_
		and ReportProcessUsers.UserID <> p_UserID
		and ReportProcessUsers.IsConfirmed=0
		order by ReportDeviceHistory.CreationDate desc;
	end if;
 end if;
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetSecureQuestions` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetSecureQuestions`()
BEGIN
	-- SET NOCOUNT ... *** SQLINES FOR EVALUATION USE ONLY *** 
	-- interfering ... *** SQLINES FOR EVALUATION USE ONLY *** 

    -- Insert state... *** SQLINES FOR EVALUATION USE ONLY *** 
	SELECT SecureQuestions.ID,SecureQuestions.Name
	from SecureQuestions;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetSensorLocatorByID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetSensorLocatorByID`(
	p_DeviceSensorID int)
BEGIN

    SELECT SerialNumber, SensorTypeID 
	FROM DeviceSensors 
	INNER JOIN Devices 
	ON Devices.ID=DeviceID 
	WHERE DeviceSensors.ID=p_DeviceSensorID;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetSensorLogsData` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetSensorLogsData`(

p_StartTime int/* =0 */,
p_Endtime int/* =0 */)
BEGIN

	declare v_SQL_ longtext;
	declare v_Whe_ nvarchar(2000);

	set v_Whe_ = ' 1=1 ';

	if	p_StartTime>0 and p_Endtime>0 
	then
		set v_Whe_ = CONCAT(v_Whe_ , ' and SampleTime >= ' , cast(p_StartTime as char(1)) , ' and SampleTime<= ' , cast(p_Endtime as char(1)));
	end if;

	set v_SQL_ = CONCAT(' select id,DeviceSensorID,Value,SampleTime,IsTimeStamp,Comment ,isnull(AlarmStatus,0)AlarmStatus,IsDummy
	from SensorLogs
	where ' , v_Whe_ , '
	order by sampletime asc ');

	SET @stmt_str =  v_SQL_;
	PREPARE stmt FROM @stmt_str;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;	


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetSensorUnits` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetSensorUnits`(
	p_FamilyTypeID int)
BEGIN

	select SensorUnits.id,SensorUnits.name
	from SensorUnits
	inner join BaseSensorUnits
	on BaseSensorUnits.SensorUnitID=SensorUnits.ID
	where BaseSensorUnits.FamilyTypeID = p_FamilyTypeID;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetSystemSettings` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetSystemSettings`(
	p_UserID int)
BEGIN

	declare v_Cus_ int;

	select customerid into v_Cus_
	from Users
	where id=p_UserID;

	select ifnull(isCFREnabled,0)isCFREnabled,ifnull(TemperatureUnit,1)TemperatureUnit,ifnull(debugmode ,0)debugmode
	,(select code from languages where languages.id=CustomerSettings.LanguageID)languageCode
	,timezonevalue,timezoneabbr,ifnull(timezoneoffset,0)timezoneoffset,ifnull(timezoneisdst,0)timezoneisdst,timezonetext
	from CustomerSettings
    where customerid=v_Cus_;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetUser` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetUser`(
	p_UserID int)
BEGIN

	select email,id userid, firstname,LastName,Username,customerid
	,ifnull(mobilenumber,0)mobilenumber,ifnull(countrycode,'')countrycode,ifnull(dialcode,'')dialcode
	,ifnull((select name from sites where sites.id=PermissionUserSiteDeviceGroup.siteid),'ALL') sitename
	,ifnull((select name from PermissionGroups where PermissionGroups.id = PermissionUserSiteDeviceGroup.permissiongroupid)
	 , (select name from PermissionGroups where PermissionGroups.id = users.permissiongroupid))	 privilagelevel
	from users
	left join PermissionUserSiteDeviceGroup
	on PermissionUserSiteDeviceGroup.userid = users.id
	where id=p_UserID;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetUserAnalytics` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetUserAnalytics`(
	p_UserID int,
	p_DeviceSensorID int)
BEGIN

    SELECT ifnull(LowLimit,0)LowLimit, ifnull(HighLimit,0)HighLimit, ifnull(ActivationEnergy ,83.14472)ActivationEnergy,IsActive
	,(select name from SensorTypes inner join  DeviceSensors on DeviceSensors.SensorTypeID = SensorTypes.ID where DeviceSensors.id=p_DeviceSensorID) sensor
	FROM AnalyticsSensors 
	WHERE UserID=p_UserID 
	AND DeviceSensorID=p_DeviceSensorID;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetUserRecovery` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetUserRecovery`(
	p_UserID int)
BEGIN

	update users
	set PasswordResetGUID=UUID()
	,PasswordResetExpiry = dateadd(hour,24,now())
	where id=p_UserID;

	select email,id userid, firstname,LastName,Username,PasswordResetGUID
	,ifnull(mobilenumber,0)mobilenumber,ifnull(countrycode,0)countrycode
	,(select name from sites where sites.id=PermissionUserSiteDeviceGroup.siteid) sitename
	,(select name from PermissionGroups where PermissionGroups.id = PermissionUserSiteDeviceGroup.permissiongroupid) privilagelevel
	from users
	left join PermissionUserSiteDeviceGroup
	on PermissionUserSiteDeviceGroup.userid = users.id
	where id=p_UserID;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetUsers` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetUsers`(
	p_UserID int)
BEGIN

	declare v_Cus_ int;
	
		
	select customerid into v_Cus_
	from users
	where id=p_UserID;

	select users.ID , users.Username, users.creationdate, users.email,users.languageid
		,(select languages.name from languages where languages.id= users.Languageid)defaultlanguage
		,defaultdateformat
		,PermissionGroups.name groupname , PermissionGroups.levelID
		,PermissionUserSiteDeviceGroup.siteid
		,(select count(userid) from PermissionUserSiteDeviceGroup where UserID=users.id) permissionCount
		, tempUserSites.Name ,tempUserSites.ParentID, tempUserSites.level, ''
		,ifnull(users.InactivityTimeout,60)InactivityTimeout
        ,RoleID
	from users
		inner join PermissionUserSiteDeviceGroup
		on PermissionUserSiteDeviceGroup.UserID=users.id
		inner join PermissionGroups
		on PermissionGroups.id = PermissionUserSiteDeviceGroup.PermissionGroupID
		inner join (SELECT id,name,GetParentIDByID(id) parentid,LENGTH( GetAncestry(id)) - LENGTH(REPLACE(GetAncestry(id), ',', ''))+1 `level`
		FROM sites 
		where customerid=v_Cus_)tempUserSites
		on  tempUserSites.ID = PermissionUserSiteDeviceGroup.SiteID
	where ifnull(users.deleted,0)=0
		and users.CustomerID=v_Cus_
	union all
	select users.ID , users.Username, users.creationdate, users.email,users.languageid
		,(select languages.name from languages where languages.id= users.Languageid)defaultlanguage
		,defaultdateformat
		,PermissionGroups.name groupname , PermissionGroups.levelID
		,0
		,0
		, '' ,0, 0,''
		,ifnull(users.InactivityTimeout,60)
        ,RoleID
	from users
		inner join PermissionGroups
		on PermissionGroups.id = users.PermissionGroupID
	where ifnull(users.deleted,0)=0
		and users.CustomerID=v_Cus_;
	

	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spGetUserSites` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spGetUserSites`(
	p_UserID int)
begin	

	declare cnt, n int;
    select count(*) into n from sites where parentid=0;
    update sites a, sites b set a.path = b.name where b.parentid=0 and a.parentid = b.id;
    select count(*) into cnt from sites where path is null;
    while cnt > n do
        update sites a, sites b set a.path = concat(b.path, '/', b.name) where b.path is not null and a.parentid = b.id;
        select count(*) into cnt from sites where path is null;
    end while;

	update sites
    set path=name
    where parentid=0;

	SELECT id,name,GetParentIDByID(id) parentid,LENGTH( GetAncestry(id)) - LENGTH(REPLACE(GetAncestry(id), ',', ''))+1 `level`,case parentid when 0 then path else concat(path,'/',name) end treepath
	FROM sites
	where customerid = (select customerid from users where id=p_UserID);

end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spHideAlarmNotification` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spHideAlarmNotification`(
	p_AlarmID int,
	p_SerialNumber int/* =0 */,
	p_AlarmTypeID int/* =0 */,
	p_DeviceSensorID int/* =0 */)
BEGIN

declare v_Dev_ int;

	if (p_AlarmID <> 0) then /* Hide by ID... *** SQLINES FOR EVALUATION USE ONLY *** */
		update AlarmNotifications set Hide=1 where ID=p_AlarmID;
	
	ELSE
		select id into v_Dev_
		from devices
		where serialnumber=p_SerialNumber;

	
		update AlarmNotifications
		set Hide=1
		where DeviceID=v_Dev_ 
		and AlarmTypeID = p_AlarmTypeID 
		and DeviceSensorID=p_DeviceSensorID;
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spHideAllAlarmNotifications` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spHideAllAlarmNotifications`(

	p_UserID int)
BEGIN

    update AlarmNotifications
	set Hide=1
	where AlarmNotifications.Hide=0
	and exists
	(select  id
	 from devices
	 inner join PermissionUserSiteDeviceGroup
	 on PermissionUserSiteDeviceGroup.SiteID = devices.SiteID
	 where PermissionUserSiteDeviceGroup.UserID=p_UserID
	 and AlarmNotifications.DeviceID=devices.ID
     limit 1
	);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spInsertAlarmNotification` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spInsertAlarmNotification`(
	p_SerialNumber int,
	p_AlarmTypeID int,
	p_Value double,
	p_StartTime int,
	p_SensorTypeID int/* =0 */)
BEGIN

	declare v_Dev_ int;
	declare v_Dev2 int;
	declare v_Ala_ int;
	declare v_Ala2 int;
	DECLARE v_IsH_ INT;

	select id into v_Dev_
	from devices
	where serialnumber=p_SerialNumber;

	select DeviceSensors.id into v_Dev2
	from DeviceSensors
	where DeviceSensors.DeviceID=v_Dev_
	and DeviceSensors.SensorTypeID = p_SensorTypeID;

	select ID into v_Ala_ FROM AlarmNotifications 
	where DeviceID=v_Dev_ and AlarmTypeID = p_AlarmTypeID and DeviceSensorID=v_Dev2;

    if v_Ala_ IS NULL
	then
		insert into AlarmNotifications(DeviceID,AlarmTypeID,Value,StartTime,DeviceSensorID)
		values(v_Dev_,p_AlarmTypeID,p_Value,p_StartTime,v_Dev2);

		set v_Ala_ = @@Identity;
		SET v_Ala2 = 0;
	else
		
		select Hide into v_IsH_ FROM AlarmNotifications WHERE ID=v_Ala_;
		IF v_IsH_ = 1 THEN
			SET v_Ala2 = 0;
		else
			SET v_Ala2 = 1;
		END IF;
		
		/*
		select @... *** SQLINES FOR EVALUATION USE ONLY *** tions
		where DeviceID=@DeviceID 
		and AlarmTypeID = @AlarmTypeID 
		and DeviceSensorID=@DeviceSensorID
		*/
		update AlarmNotifications
		set Value=p_Value, Hide=0
		where ID=v_Ala_;
	end if;

	if (v_Ala2 = 0) then
		SELECT 0;
	ELSE
	
		select AlarmNotifications.ID, Devices.Name AS DeviceName, AlarmNotifications.Value,AlarmNotifications.StartTime, AlarmNotifications.ClearTime, AlarmNotifications.AlarmReason,
		AlarmTypes.Name AS AlarmTypeName, AlarmNotifications.AlarmTypeID, AlarmNotifications.DeviceSensorID, Devices.SerialNumber
		FROM AlarmNotifications
		JOIN Devices ON Devices.ID = AlarmNotifications.DeviceID
		JOIN AlarmTypes ON AlarmTypes.ID = AlarmNotifications.AlarmTypeID
		WHERE AlarmNotifications.ID = v_Ala_;
	end if;
    /*
	devices.i... *** SQLINES FOR EVALUATION USE ONLY *** Types where AlarmTypes.id=AlarmNotifications.alarmtypeid)alarmtype
	,AlarmNotifications.AlarmTypeID,AlarmNotifications.DeviceSensorID,devices.SerialNumber
	from AlarmNotifications
	inner join devices
	on devices.id=AlarmNotifications.deviceid
	inner join PermissionUserSiteDeviceGroup
	on PermissionUserSiteDeviceGroup.siteid = devices.siteid
	where AlarmNotifications.id=@AlarmNotificationID
	*/
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spInsertDownloadHistory` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spInsertDownloadHistory`(
	p_SerialNumber int,
	p_SensorTypeID int,
	p_FirstDownloadTime int,
	p_LastDownloadTime int)
BEGIN
		
		delete from SensorDownloadHistory
		where SerialNumber=p_SerialNumber
		and SensorTypeID=p_SensorTypeID;

		insert into SensorDownloadHistory(SerialNumber, SensorTypeID, FirstDownloadTime, LastDownloadTime)
		values(p_SerialNumber,p_SensorTypeID,p_FirstDownloadTime,p_LastDownloadTime);
	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spIsCommandAllowed` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spIsCommandAllowed`(
	p_UserID int,
	p_Command nvarchar(50),
	p_Entity nvarchar(50),
	p_SiteID int/* =0 */)
BEGIN

	declare v_Com_ int;
	declare v_Per_ int;
	declare v_IsA_ int;
	declare v_Act_ int;

	declare v_Cus_ int;

	select customerid into v_Cus_
	from Users
	where Users.id	= p_UserID;

	set v_IsA_=0;

	if not exists (select  id 
					from PermissionActions 
					where Command=p_Command
					and Entity=p_Entity limit 1) then 
		set v_Act_=2;
	else
		select id,ActionTypeID into v_Com_, v_Act_
		from PermissionActions
		where Command=p_Command
		and Entity=p_Entity;
	end if;

	if v_Act_ =2 then
	   set v_IsA_=1;
	else
		if exists (select userid from PermissionUserSiteDeviceGroup where userid=p_UserID and siteid=p_SiteID limit 1)
		then
			
			select PermissionUserSiteDeviceGroup.PermissionGroupID into v_Per_
			from PermissionUserSiteDeviceGroup
			inner join PermissionGroupActions
			on PermissionGroupActions.PermissionGroupID = PermissionUserSiteDeviceGroup.PermissionGroupID
			inner join PermissionActions
			on PermissionActions.ID = PermissionGroupActions.PermissionActionID
			where PermissionUserSiteDeviceGroup.UserID = p_UserID
			and PermissionActions.Command=p_Command
			and PermissionActions.Entity = p_Entity
			and PermissionGroupActions.CustomerID=v_Cus_
			and PermissionUserSiteDeviceGroup.SiteID=p_SiteID;
		else

			select ifnull(Users.PermissionGroupID,0) into v_Per_
			from Users
			where id=p_UserID;

			if v_Per_=0 and p_SiteID =0 then
				set v_IsA_=1;
			end if;

		end if;
		-- checks if no ... *** SQLINES FOR EVALUATION USE ONLY *** 
		if p_UserID=0 and v_Act_=2 
		then
			set v_IsA_=1;
		else
			if v_Com_>0 and v_Per_>0
			then
				if v_Act_=2 -- this is alway... *** SQLINES FOR EVALUATION USE ONLY *** 
				then
					set v_IsA_=1;
				else
					select IsAllowed into v_IsA_
					from PermissionGroupActions
					where PermissionGroupID=v_Per_
					and PermissionActionID = v_Com_;
				end if;
			end if;
		end if;
	end if;
	select v_IsA_;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spIsDeviceAutoSetup` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spIsDeviceAutoSetup`(

	p_SerialNumber int)
BEGIN

	declare v_Cus_ int;
	declare v_Dev_ int;
	
	select	ifnull(DeviceTypeID,0)
			, CustomerID into v_Dev_, v_Cus_
	from devices
	where SerialNumber=p_SerialNumber;

	if not exists (select  id from devices where serialnumber =p_SerialNumber limit 1)
		or v_Dev_=0
	then

		select SetupName,DeviceAutoSetup.customerid
			,(select name from DeviceTypes where DeviceTypes.ID =v_Dev_ )devicetype
		from DeviceAutoSetup
		inner join DeviceSetupFiles
		on DeviceSetupFiles.SetupID = DeviceAutoSetup.SetupID
		where DeviceAutoSetup.customerid= v_Cus_
		and DeviceAutoSetup.DeviceTypeID = v_Dev_
		and IsAutoSetup=1
		and ifnull(DeviceAutoSetup.SetupID,'')<>'';
	end if;
	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spPasswordRecovery` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spPasswordRecovery`(
	p_Email nvarchar(255))
BEGIN
	declare v_use_ int;

    if exists(select  id from users where email=p_Email limit 1)
	then
		select id into v_use_
		from users 
		where email=p_Email; 
		

		-- checks if the... *** SQLINES FOR EVALUATION USE ONLY *** 
		if exists(select  id 
				  from users 
				  where PasswordResetGUID is not null 
				  and ifnull(PasswordResetExpiry,now())>now()
				  and id=v_use_ limit 1)
		then
			select 0;
		else
			select questionid,(select name from SecureQuestions where id=questionid)question
			from userSecureQuestions
			where userid=v_use_;
		end if;
	else
		select 0;
	end if;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spRegisterLanRcv` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spRegisterLanRcv`(
	-- Add the para... *** SQLINES FOR EVALUATION USE ONLY *** 
	p_CustomerID INT,
	p_UserName NVARCHAR(50),
	p_Password NVARCHAR(50),
	p_MacAddress NVARCHAR(50))
BEGIN
	-- SET NOCOUNT ... *** SQLINES FOR EVALUATION USE ONLY *** 
	-- interfering ... *** SQLINES FOR EVALUATION USE ONLY *** 

	DECLARE v_pas_ INT;
	DECLARE v_Dev_ INT;

	SET v_pas_ = 0;

    IF NOT EXISTS (SELECT dbo.Devices.ID FROM Devices WHERE CustomerID=p_CustomerID AND MacAddress = p_MacAddress limit 1)
	THEN
		
		SELECT dbo.DeviceTypes.ID INTO v_Dev_
		FROM DeviceTypes
		WHERE dbo.DeviceTypes.Name = 'LAN-R';
    
		INSERT dbo.Devices
		        ( CustomerID ,
		          DeviceTypeID ,
		          MacAddress ,
		          UserName,
				  UserPassword
		        )
		SELECT  ( p_CustomerID ,
		          v_Dev_,
				  p_MacAddress ,
		          p_UserName, 
				  p_Password 
		        );
		
		SET v_pas_ = 1;
	END IF;  

	SELECT v_pas_;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spRelateDeviceToSite` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spRelateDeviceToSite`(
	p_SerialNumber int,
	p_SiteID int)
BEGIN

    update devices
	set SiteID = p_SiteID
	where SerialNumber=p_SerialNumber;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spResetPassword` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spResetPassword`(
	p_NewPassword NVARCHAR(255),
	p_GuidReset CHAR(36))
BEGIN

	DECLARE v_IsC_ INT;
	DECLARE v_IsR_ INT;
	Declare v_Use_ nvarchar(50);

	SET v_IsC_=0;
	SET v_IsR_ = 0;

    -- first checks ... *** SQLINES FOR EVALUATION USE ONLY *** 
	IF EXISTS (SELECT  dbo.Users.ID 
				FROM Users 
				WHERE Users.PasswordResetGUID =p_GuidReset limit 1 )
	THEN
    
		select username into v_Use_
		from Users 
		WHERE Users.PasswordResetGUID =p_GuidReset;

		-- then check fo... *** SQLINES FOR EVALUATION USE ONLY *** 
		SELECT v_IsC_ = dbo.fnValidateCredentialPolicy(v_Use_,p_NewPassword);

		IF v_IsC_ = 0
		THEN
			-- then clean th... *** SQLINES FOR EVALUATION USE ONLY *** 
			-- and update th... *** SQLINES FOR EVALUATION USE ONLY *** 
			UPDATE Users
			SET PasswordResetGUID = null
			, UserPassword = p_NewPassword
			, PasswordResetExpiry=null
			WHERE Users.Username=v_Use_;
			
			SET v_IsR_ = 1;
		
		ELSE
			set v_IsR_ = v_IsC_;
		END IF;
	END IF;
  
	SELECT v_IsR_;  
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spSaveAlarmNotifications` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spSaveAlarmNotifications`(
	p_ContactID int,
	p_SerialNumber int,
	p_ReceiveEmail tinyint unsigned,
	p_ReceiveSMS tinyint unsigned,
	p_ReceiveBatteryLow tinyint unsigned,
	p_BatteryLowValue int /* =0 */)
BEGIN

	Declare v_Dev_ int;

	select ID into v_Dev_
	from Devices
	where SerialNumber=p_SerialNumber;

    if exists(select  deviceid from DeviceAlarmNotifications where deviceid=v_Dev_ and contactid=p_ContactID limit 1)
	then
		update DeviceAlarmNotifications
		set ReceiveEmail = p_ReceiveEmail,
		ReceiveSMS = p_ReceiveSMS,
		ReceiveBatteryLow = p_ReceiveBatteryLow,
		BatteryLowValue=p_BatteryLowValue
		where deviceid=v_Dev_ 
		and contactid=p_ContactID;
	else
		insert DeviceAlarmNotifications(DeviceID,ContactID,ReceiveEmail,ReceiveSMS,ReceiveBatteryLow,BatteryLowValue)
		select(v_Dev_,p_ContactID,p_ReceiveEmail,p_ReceiveSMS,p_ReceiveBatteryLow,p_BatteryLowValue);

		insert into DeviceAlarmNotificationTrail(DeviceID,ContactID,DeviceSensorID)
		select v_Dev_,p_ContactID,id
		from DeviceSensors
		where DeviceID=v_Dev_;

	end if;

	select FOUND_ROWS();
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spSaveMatrix` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spSaveMatrix`(
	p_UserID int,
	p_PermissionActionID int,
	p_PermissionGroupID int,
	p_IsAllowed tinyInt UNSIGNED)
BEGIN

	declare v_Cus_ int;

	select customerid into v_Cus_
	from users
	where id=p_UserID;

    if exists (select  permissionactionid 
				from PermissionGroupActions 
				where PermissionActionID=p_PermissionActionID 
				and PermissionGroupID=p_PermissionGroupID 
				and CustomerID=v_Cus_ limit 1)
	then
		update PermissionGroupActions
		set IsAllowed=p_IsAllowed
		where PermissionActionID=p_PermissionActionID 
			and PermissionGroupID=p_PermissionGroupID 
			and CustomerID=v_Cus_;
	else
		insert into PermissionGroupActions(PermissionActionID,PermissionGroupID,CustomerID,IsAllowed)
		values(p_PermissionActionID,p_PermissionGroupID,v_Cus_,p_IsAllowed);
	end if;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spSaveReport` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spSaveReport`(
	p_SerialNumber int,
	p_image longblob)
BEGIN

	insert into ReportDeviceHistory(SerialNumber, PDFStream,CreationDate)
	values(p_SerialNumber,p_image,DATEDIFF(NOW(),{d '1970-01-01'}));

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spSaveSystemSettings` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spSaveSystemSettings`(
	p_UserID int,
	p_isCFREnabled int,
	p_TemperatureUnit int,
	p_debugmode int,
	p_LanguageCode varchar(10),
	p_timezonevalue varchar(20),
	p_timezoneabbr varchar(5),
	p_timezoneoffset int,
	p_timezoneisdst int,
	p_timezonetext varchar(255))
BEGIN

	declare v_Cus_ int;
	declare v_Lan_ int;

	select id into v_Lan_ 
	from languages
	where code=p_LanguageCode;

	select customerid into v_Cus_
	from Users
	where id=p_UserID;

	update customersettings
	set isCFREnabled=p_isCFREnabled,
	TemperatureUnit=p_TemperatureUnit,
	debugmode=p_debugmode,
	LanguageID=v_Lan_,
	timezonevalue=p_timezonevalue,
	timezoneabbr=p_timezoneabbr,
	timezoneoffset=p_timezoneoffset,
	timezoneisdst=p_timezoneisdst,
	timezonetext=p_timezonetext
	where CustomerID=v_Cus_;

	select isCFREnabled,ifnull(TemperatureUnit,1)TemperatureUnit,ifnull(debugmode,0) debugMode
	,(select code from languages where languages.id=CustomerSettings.LanguageID)languageCode
	,timezonevalue,timezoneabbr,timezoneoffset,timezoneisdst,timezonetext
	from CustomerSettings
    where customerid=v_Cus_;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spSaveUser` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spSaveUser`(

	p_UserID int,
	p_UserName varchar(50),
	p_FirstName varchar(32),
	p_LastName varchar(32),
	p_Email varchar(255),
	p_countryCode varchar(4),
	p_mobileNumber varchar(10))
BEGIN
	-- SET NOCOUNT ... *** SQLINES FOR EVALUATION USE ONLY *** 
	-- interfering ... *** SQLINES FOR EVALUATION USE ONLY *** 

	update users
	set username=p_UserName,
	firstName=p_FirstName,
	lastname=p_LastName,
	email=p_Email,
	countrycode=p_countryCode,
	mobilenumber=p_mobileNumber
	where ID=p_UserID
	and username<>'admin';

	select email,id userid, firstname,LastName,Username
	,ifnull(mobilenumber,0)mobilenumber,ifnull(countrycode,0)countrycode
	,ifnull((select name from sites where sites.id=PermissionUserSiteDeviceGroup.siteid),'ALL') sitename
	,ifnull((select name from PermissionGroups where PermissionGroups.id = PermissionUserSiteDeviceGroup.permissiongroupid)
	 , (select name from PermissionGroups where PermissionGroups.id = users.permissiongroupid))	 privilagelevel
	from users
	left join PermissionUserSiteDeviceGroup
	on PermissionUserSiteDeviceGroup.userid = users.id
	where id=p_UserID;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spSecurityAnswerCheck` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spSecurityAnswerCheck`(
	p_Email nvarchar(255),
	p_Answer nvarchar(500))
BEGIN

    declare v_use_ int;
	declare v_Sto_ nvarchar(500);
	declare v_tok_ char(36); 
	-- checks first ... *** SQLINES FOR EVALUATION USE ONLY *** 
    if exists(select  id from users where email=p_Email limit 1)
	then
		select id into v_use_
		from users 
		where email=p_Email; 
		
		-- get the store... *** SQLINES FOR EVALUATION USE ONLY *** 
		/*select @Sto... *** SQLINES FOR EVALUATION USE ONLY *** 		where userid=@userID
		--checks if the answer is identical
		if @StoreAnswer = @Answer
		begin*/
			set v_tok_ = uuid();

			update users
			set passwordResetGUID=v_tok_
			,PasswordResetExpiry = dateadd(day,1, now())
			where id=v_use_;

			select 1,v_tok_ token,username,email
			from users
			where id=v_use_;
		/*end
		else
... *** SQLINES FOR EVALUATION USE ONLY *** */
	else
		select 0;
	end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spSetCalibrationCertificateDefault` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spSetCalibrationCertificateDefault`(
	p_UserID int,
	p_calibtemperature double,
	p_calibhumidity double,
	p_calibfourtecwaxseal int,
	p_calibmanufacture varchar(100),
	p_calibmodel varchar(100),
	p_calibnist int,
	p_calibproductname varchar(255),
	p_calibserialnumber int)
BEGIN

	declare v_Cus_ int;

	select customerid into v_Cus_
	from users
	where id=p_UserID;
   
    update customersettings
	set calibtemperature=p_calibtemperature,
	calibhumidity=p_calibhumidity,
	calibfourtecwaxseal=p_calibfourtecwaxseal,
	calibmanufacture=p_calibmanufacture,
	calibmodel=p_calibmodel,
	calibnist=p_calibnist,
	calibproductname=p_calibproductname,
	calibserialnumber=p_calibserialnumber
	where customerid= v_Cus_;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spSetupUser` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spSetupUser`(

	p_GUID char(36),
	p_UserName nvarchar(50),
	p_userPassword nvarchar(255),
	p_timezonevalue varchar(20),
	p_timezoneabbr varchar(5),
	p_timezoneoffset int,
	p_timezoneisdst int,
	p_timezonetext varchar(255),
	p_CountryCode varchar(4),
	p_FirstName varchar(32),
	p_LastName varchar(32),
	p_MobileNumber varchar(10),
	p_DialCode varchar(10))
BEGIN
	-- SET NOCOUNT ... *** SQLINES FOR EVALUATION USE ONLY *** 
	-- interfering ... *** SQLINES FOR EVALUATION USE ONLY *** 

	DECLARE v_Cus_ INT;
	DECLARE v_Act_ INT;
	DECLARE v_cre_ INT;
	declare v_Pas_ datetime(3);
	declare v_Exp_ int;
	declare v_Use_ int;
	
	SELECT dbo.Users.CustomerID,PasswordResetExpiry,id INTO v_Cus_, v_Pas_, v_Use_
	FROM Users
	WHERE PasswordResetGUID=p_GUID;

	set v_Exp_ = TIMESTAMPDIFF(day,now(),v_Pas_);

	if exists(select users.ID from users where users.PasswordResetGUID=p_GUID limit 1) 
		and v_Exp_>0
	then

		-- checks if the... *** SQLINES FOR EVALUATION USE ONLY *** 
		SELECT v_cre_ = dbo.fnValidateCredentialPolicy(p_UserName,p_userPassword);

		IF v_cre_=0
		THEN

			if not exists(select  users.id from users where users.username=p_UserName and users.customerid=v_Cus_
							and users.id<>v_Use_ limit 1)
			then
				-- insert the ne... *** SQLINES FOR EVALUATION USE ONLY *** 
				update Users
				set Username= p_UserName,
					UserPassword = p_userPassword,
					PasswordCreationDate=NOW(),
					timezonevalue=p_timezonevalue,
					timezoneabbr=p_timezoneabbr,
					timezoneoffset=p_timezoneoffset,
					timezoneisdst=p_timezoneisdst,
					timezonetext=p_timezonetext,
					CountryCode=p_CountryCode,
					FirstName=p_FirstName,
					LastName = p_LastName,
					MobileNumber=p_MobileNumber,
					DialCode=p_DialCode,
					isactive=1
				where id= v_Use_;
			
			
				-- save the pass... *** SQLINES FOR EVALUATION USE ONLY *** 
				insert UserPasswordHistory(UserID,UserPassword,ChangedDate)
				values(v_Use_,p_userPassword,NOW());

				-- CFR
				call spCFRAuditTrail( v_Use_,'setup','user',0,'');

			else
				set v_Use_ = -100;
			end if;
		else -- Credential n... *** SQLINES FOR EVALUATION USE ONLY *** 
			set v_Use_ = (-1) * v_cre_;
		end if;
		
		if v_Use_>0
		then
			select id userid,defaultDateFormat,
			(select permissiongroups.Name from permissiongroups where permissiongroups.id=users.permissiongroupid) GroupName,
			email,
			(select Languages.Name from languages where languages.id = users.languageID)LanguageName
			from users
			where id=v_Use_;
		else
			select v_Use_ userid;
		end if;
	else
		select -99; -- guid not vali... *** SQLINES FOR EVALUATION USE ONLY *** 
	end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spSnoozeAlarm` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spSnoozeAlarm`(
	p_SerialNumber int,
	p_DeviceSensorID int,
	p_Silent int)
BEGIN

    declare v_Dev_ int;

	select devices.id into v_Dev_
	from devices
	where serialnumber=p_SerialNumber;

	update AlarmNotifications
	set Silent=p_Silent
	where DeviceID=v_Dev_
	and DeviceSensorID=p_DeviceSensorID;

	select Found_rows();
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spSystemActionsAudit` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spSystemActionsAudit`(
	p_UserID int,
	p_Command nvarchar(50),
	p_Entity nvarchar(50),
	p_SerialNumber int/* =0 */)
BEGIN

	declare v_Per_ int;
	declare v_Dev_ int;
	Declare v_IsC_ int;

	select ifnull(IsCFREnabled,0) into v_IsC_
	from CustomerSettings
	where customerid=(select customerid from users where id=p_UserID);

	if v_IsC_=1
	then
		select id into v_Per_
		from PermissionActions
		where Command=p_Command
		and Entity=p_Entity;

		set v_Dev_=0;

		if p_SerialNumber>0
		then
			select id into v_Dev_
			from devices
			where serialnumber=p_SerialNumber;
		end if;

		insert into SystemActionsAudit(UserID,PermissionActionID,DeviceID)
		values(p_UserID,v_Per_,v_Dev_);
	end if;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spUpdateAlarmNotification` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spUpdateAlarmNotification`(
	p_SerialNumber int,
	p_AlarmTypeID int,
	p_SensorTypeID int/* =0 */)
BEGIN
	declare v_Dev_ int;
	declare v_Dev2 int;

	select id into v_Dev_
	from devices
	where serialnumber=p_SerialNumber;

	select DeviceSensors.id into v_Dev2
	from DeviceSensors
	where DeviceSensors.DeviceID=v_Dev_
	and DeviceSensors.SensorTypeID = p_SensorTypeID;
	
	update AlarmNotifications
	set ClearTime=DATEDIFF(now(),{d '1970-01-01'} )
	where DeviceID=v_Dev_
	and AlarmTypeID=p_AlarmTypeID
	and DeviceSensorID=v_Dev2;
	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spUpdateAlarmReason` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spUpdateAlarmReason`(
	p_AlarmID int,
	p_SerialNumber int, 
	p_AlarmTypeID int,
	p_Reason nvarchar(255),
	p_DeviceSensorID int/* =0 */)
BEGIN

declare v_Dev_ int;

	if (p_AlarmID <> 0) then
		update AlarmNotifications set AlarmReason=p_Reason where ID=p_AlarmID;
	
	ELSE
		
	
		select id into v_Dev_
		from devices
		where serialnumber=p_SerialNumber;

	
		update AlarmNotifications
		set AlarmReason=p_Reason
		where DeviceID=v_Dev_ 
		and AlarmTypeID = p_AlarmTypeID 
		and DeviceSensorID=p_DeviceSensorID;
        
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spUpdateAnalyticsSensors` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spUpdateAnalyticsSensors`(
	p_UserID int,
	p_DeviceSensorID int,
	p_ActivationEnergy double,
	p_LowLimit double,
	p_HighLimit double)
BEGIN

	UPDATE AnalyticsSensors 
	SET ActivationEnergy=p_ActivationEnergy
	,LowLimit=p_LowLimit
    ,HighLimit=p_HighLimit 
	WHERE UserID=p_UserID 
	AND DeviceSensorID=p_DeviceSensorID;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spUpdateCalibrationExpiryReminder` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spUpdateCalibrationExpiryReminder`(
	p_UserID int,
	p_IsReminderOn int,
	p_ReminderInMonth int)
BEGIN

	
	update users 
	set IsCalibrationReminderActive = p_IsReminderOn,
	CalibrationReminderInMonths = p_ReminderInMonth
	where id=p_UserID;
   
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spUpdateContact` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spUpdateContact`(
	p_ContactID int,
	p_Name nvarchar(50),
	p_Title nvarchar(10),
	p_Phone nvarchar(50),
	p_Email nvarchar(255),
	p_WeekdayStart int,
	p_WorkdayStart time(6),
	p_WorkdayEnd time(6),
	p_OutOfOfficeStart int,
	p_OutOfOfficeEnd int,
	p_SMSResendInterval int)
BEGIN

	Update Contacts
	set name=p_Name
	, Title = p_Title
	, Phone = p_Phone
	, Email = p_Email
	, WeekdayStart = p_WeekdayStart
	, WorkdayStart = p_WorkdayStart
	, WorkdayEnd = p_WorkdayEnd
	, OutOfOfficeStart = p_OutOfOfficeStart
	, OutOfOfficeEnd = p_OutOfOfficeEnd
	, SMSResendInterval = p_SMSResendInterval

	where id=p_ContactID;

	select FOUND_ROWS();

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spUpdateCustomerEmailSent` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spUpdateCustomerEmailSent`(
	p_SerialNumber int)
BEGIN

    declare v_Cus_ int;

	select customerid into v_Cus_
	from devices
	where serialNumber = p_SerialNumber;

	update customers
	set EmailSentDate=now()
	where id=v_Cus_;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spUpdateDeviceMode` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spUpdateDeviceMode`(
	p_SerialNumber int,
	p_InFwUpdate int)
BEGIN

    
	update devices
	set InFwUpdate=p_InFwUpdate
	where SerialNumber= p_SerialNumber;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spUpdateDeviceSetup` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spUpdateDeviceSetup`(
	
	p_UserID int,
	p_DeviceType varchar(255),
	p_SetupID varchar(255),
	p_IsAutoSetup int)
BEGIN

	Declare v_Cus_ int;
	Declare v_Dev_ int;

	select id into v_Dev_
	from DeviceTypes
	where name = p_DeviceType;

	select customerid into v_Cus_
	from users
	where id=p_UserID;

	update DeviceAutoSetup
	set IsAutoSetup=p_IsAutoSetup,
		SetupID=p_SetupID
	where DeviceTypeID = v_Dev_ 
	and CustomerID=v_Cus_;
	

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spUpdateDeviceTimerRun` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spUpdateDeviceTimerRun`(

	p_SerialNumber int)
BEGIN

	declare v_Tim_ tinyint unsigned;
	Declare v_Dev_ int;
	declare v_res_ int;

	set v_res_=0;

	select TimerRunEnabled,Devices.ID into v_Tim_, v_Dev_
	from DeviceMicroX
	inner join Devices
	on DeviceMicroX.DeviceID =Devices.ID
	where Devices.SerialNumber=p_SerialNumber;

    -- checks if the... *** SQLINES FOR EVALUATION USE ONLY *** 
	if v_Tim_ = 1
	then
		-- then reset th... *** SQLINES FOR EVALUATION USE ONLY *** 
		update DeviceMicroX 
		set TimerRunEnabled=0
			,TimerStart=0
		where DeviceID = v_Dev_;

		update devices
		set IsRunning=1
		where SerialNumber=p_SerialNumber;
		-- return 1
		set v_res_ = 1;
	end if;
	
	select v_res_;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spUpdateMongoDBBackup` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spUpdateMongoDBBackup`(
	p_SerialNumber int)
BEGIN

	declare v_Cus_ int;
	declare v_Mon_ int;

	select customerid into v_Cus_
	from devices
	where serialnumber=p_SerialNumber;

	select ifnull(MongoDataBackUp,0) into v_Mon_
	from customers
	where id=v_Cus_;

	if v_Mon_=0 
	then
		update customers
		set MongoDataBackUp=1
		where id=v_Cus_;
	else
		if v_Mon_=1
		then
			update customers
			set MongoDataBackUp=2
			where id=v_Cus_;
		end if;
	end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spUpdateNotificationBatteryEmailSent` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spUpdateNotificationBatteryEmailSent`(
	p_SerialNumber int, 
	p_ContactID int)
BEGIN

    declare v_Dev_ int;
	declare v_Bat_ int;

	select id into v_Dev_
	from devices
	where serialnumber= p_SerialNumber;

	select BatteryEmailSent into v_Bat_
	from DeviceAlarmNotifications
	where DeviceID=v_Dev_
	and ContactID=p_ContactID;

	if v_Bat_>0 
	then
		update DeviceAlarmNotifications
		set BatteryEmailSent = 0
		where DeviceID=v_Dev_
		and ContactID=p_ContactID;
	else
		update DeviceAlarmNotifications
		set BatteryEmailSent = DATEDIFF(now(),{d '1970-01-01'} )
		where DeviceID=v_Dev_
		and ContactID=p_ContactID;
	end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spUpdateNotificationBatterySMSSent` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spUpdateNotificationBatterySMSSent`(
	p_SerialNumber int, 
	p_ContactID int,
	p_SMSID nvarchar(255))
BEGIN

    declare v_Dev_ int;
	declare v_Cus_ int;
	declare v_Bat_ int;

	select id into v_Dev_
	from devices
	where serialnumber= p_SerialNumber;

	-- get the batte... *** SQLINES FOR EVALUATION USE ONLY *** 
	-- then set it ... *** SQLINES FOR EVALUATION USE ONLY *** 
	-- otherwise se... *** SQLINES FOR EVALUATION USE ONLY *** 
	select BatterySMSSent into v_Bat_
	from DeviceAlarmNotifications
	where DeviceID=v_Dev_
	and ContactID=p_ContactID;

	if v_Bat_>0
	then 
		update DeviceAlarmNotifications
		set BatterySMSSent = 0
		where DeviceID=v_Dev_
		and ContactID=p_ContactID;
	else
		update DeviceAlarmNotifications
		set BatterySMSSent = DATEDIFF(now(),{d '1970-01-01'})
		where DeviceID=v_Dev_
		and ContactID=p_ContactID;
	end if;

	if not exists(select deviceid from DeviceSMSTrail where DeviceID=v_Dev_ and contactID=p_ContactID and SMSID=p_SMSID limit 1)
	then
		insert into DeviceSMSTrail(DeviceID,ContactID,SMSID,IsBattery,LastUpdated)
		values(v_Dev_,p_ContactID,p_SMSID,1,NOW());
	end if;

	select CustomerID into v_Cus_
	from Contacts
	where id=p_ContactID;

	update Customers
	set SMSCounter= SMSCounter+1
	where id=v_Cus_;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spUpdateNotificationEmailSent` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spUpdateNotificationEmailSent`(
	p_SerialNumber int, 
	p_ContactID int,
	p_SensorTypeID int)
BEGIN

	declare v_Dev_ int;
	declare v_Ala_ int;
	declare v_Dev2 int;

	select id into v_Dev_
	from devices
	where serialnumber= p_SerialNumber;

	select ID into v_Dev2
	from DeviceSensors
	where DeviceID=v_Dev_
	and SensorTypeID =p_SensorTypeID;

	select AlarmEmailSent into v_Ala_
	from DeviceAlarmNotificationTrail
	where DeviceSensorID=v_Dev2
	and ContactID=p_ContactID;

	if v_Ala_>0
	then
		update DeviceAlarmNotificationTrail
		set AlarmEmailSent = 0
		where ContactID=p_ContactID
		and DeviceSensorID=v_Dev2;
	else
		
		insert into DeviceAlarmNotificationTrail(DeviceSensorID,ContactID,AlarmEmailSent,AlarmSMSSent)
		values(v_Dev2,p_ContactID,DATEDIFF(now(),{d '1970-01-01'} ),0);
	end if;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spUpdateNotificationSMSSent` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spUpdateNotificationSMSSent`(
	p_SerialNumber int, 
	p_ContactID int,
	p_SMSID nvarchar(255),
	p_SensorTypeID int)
BEGIN

    declare v_Dev_ int;
	declare v_Cus_ int;
	declare v_Ala_ int;
	declare v_Dev2 int;

	select id into v_Dev_
	from devices
	where serialnumber= p_SerialNumber;

	select ID into v_Dev2
	from DeviceSensors
	where DeviceID=v_Dev_
	and SensorTypeID =p_SensorTypeID;

	-- checks if the... *** SQLINES FOR EVALUATION USE ONLY *** 
	-- else update t... *** SQLINES FOR EVALUATION USE ONLY *** 
	select AlarmSMSSent into v_Ala_
	from DeviceAlarmNotificationTrail
	where ContactID=p_ContactID
	and DeviceSensorID=v_Dev2;

	if v_Ala_>0 
	then
		update DeviceAlarmNotificationTrail
		set AlarmSMSSent = 0
		where ContactID=p_ContactID
		and DeviceSensorID=v_Dev2;
	else

		insert into DeviceAlarmNotificationTrail(DeviceSensorID,ContactID,AlarmEmailSent,AlarmSMSSent)
		values(v_Dev2,p_ContactID,0,DATEDIFF(now(),{d '1970-01-01'}));
	end if;

	if not exists(select  deviceid from DeviceSMSTrail where DeviceID=v_Dev_ and contactID=p_ContactID and SMSID=p_SMSID limit 1)
	then
		insert into DeviceSMSTrail(DeviceID,ContactID,SMSID,DeviceSensorID,LastUpdated)
		values(v_Dev_,p_ContactID,p_SMSID,v_Dev2,now());
	end if;

	select CustomerID into v_Cus_
	from Contacts
	where id=p_ContactID;

	update Customers
	set SMSCounter= SMSCounter+1
	where id=v_Cus_;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spUpdatePasswordExpiry` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spUpdatePasswordExpiry`(
	p_UserID int,
	p_PasswordMinLength int,
	p_PasswordExpiryByDays int)
BEGIN

	declare v_Cus_ int;

	select customerid into v_Cus_
	from users
	where users.id=p_UserID;

	update customersettings
	set passwordMinLength=p_PasswordMinLength,
	passwordExpiryByDays=p_PasswordExpiryByDays
	where customerid=v_Cus_;
	
    select passwordMinLength,passwordExpiryByDays
	from customersettings
	where customerid=v_Cus_;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spUpdateSite` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spUpdateSite`(
	p_SiteID int,
	p_ParentID int,
	p_Name nvarchar(50))
BEGIN

	-- checks if the... *** SQLINES FOR EVALUATION USE ONLY *** 
	if exists(select id from sites where id=p_SiteID and parentid=0 limit 1)
	then 
		set p_ParentID=0;
	end if;

	-- checks if try... *** SQLINES FOR EVALUATION USE ONLY *** 
	if p_SiteID=p_ParentID
	then
		select parentid into p_ParentID
		from sites
		where id=p_SiteID;
	end if;

    update Sites
	set name=p_Name,
	parentID=p_ParentID
	where id=p_SiteID;
	

	select id,parentid,name,treeorder
	from sites
	where id=p_SiteID;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spUpdateSMSStatus` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spUpdateSMSStatus`(
	p_SMSID nvarchar(255),
	p_SMSStatus nvarchar(255))
BEGIN

    update DeviceSMSTrail
	set SMSStatus = p_SMSStatus
	,LastUpdated=now()
	where SMSID = p_SMSID;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spUpdateUser` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spUpdateUser`(
	p_UserID int,
	p_UserName nvarchar(50), 
	p_Email nvarchar(255),
	p_LanguageID int, 
	p_defaultDateFormat nvarchar(50))
BEGIN

	declare v_Cus_ int;

	select customerid into v_Cus_
	from users
	where users.id=p_UserID;

	if not exists(select  users.id from users where users.username=p_UserName and users.customerid=v_Cus_ limit 1)
	then

		
		update users
		set UserName = p_UserName
		,Email=p_Email
		,LanguageID = p_LanguageID
		,defaultDateFormat=p_defaultDateFormat
		where id=p_UserID
		and username<>'admin';

		select FOUND_ROWS();
	else
		select 0;
	end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spUpsertAnalyticsSensors` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spUpsertAnalyticsSensors`(
	p_UserID int,
	p_DeviceSensorID int,
	p_IsActive int /* =1 */)
BEGIN

	IF NOT EXISTS (SELECT  UserID FROM AnalyticsSensors WHERE UserID=p_UserID AND DeviceSensorID=p_DeviceSensorID limit 1) THEN
                
		INSERT INTO AnalyticsSensors (UserID, DeviceSensorID, IsActive) 
		VALUES (p_UserID, p_DeviceSensorID, p_IsActive) ;

	ELSE 

		UPDATE AnalyticsSensors 
		SET IsActive=p_IsActive 
		WHERE UserID=p_UserID 
		AND DeviceSensorID=p_DeviceSensorID;
	END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spUpsertContact` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spUpsertContact`(
	p_UserID int,
	p_ContactID int,
	p_ContactName varchar(50),
	p_CountryCode varchar(10),
	p_Email varchar(255),
	p_MobilePhone varchar(50),
	p_SMSResend int,
	p_Title varchar(10),
	p_WorkingHourMode varchar(50),
	p_From int,
	p_To int,
	p_WorkingSun tinyint unsigned,
	p_WorkingMon tinyint unsigned,
	p_WorkingTue tinyint unsigned,
	p_WorkingWed tinyint unsigned,
	p_WorkingThu tinyint unsigned,
	p_WorkingFri tinyint unsigned,
	p_WorkingSat tinyint unsigned)
BEGIN

	declare v_Cus_ int;
	declare v_Wor_ int;

	select id into v_Wor_
	from ContactsWorkHoursMode
	where name=p_WorkingHourMode;

	select customerid into v_Cus_
	from Users
	where users.id=p_UserID;

	if p_ContactID>0
	then
		update Contacts
		set name=p_ContactName
			,Title=p_Title
			,Email=p_Email
			,CountryCode=p_CountryCode
			,PhoneNumber=p_MobilePhone
			,WorkingHoursID=v_Wor_
			,WorkdayStart = p_From
			,WorkdayEnd = p_To
			,SMSResendInterval = p_SMSResend
			,WorkingSun=p_WorkingSun
			,WorkingMon=p_WorkingMon
			,WorkingTue=p_WorkingTue
			,WorkingWed=p_WorkingWed
			,WorkingThu=p_WorkingThu
			,WorkingFri=p_WorkingFri
			,WorkingSat=p_WorkingSat
		where Contacts.ID=p_ContactID;
	else

		INSERT INTO Contacts
				   (`CustomerID` ,`Name` ,`Title` ,`Email` ,`CountryCode`,`PhoneNumber` ,`WorkingHoursID` ,`WorkdayStart` ,`WorkdayEnd`
				   ,`SMSResendInterval` ,`WorkingSun` ,`WorkingMon` ,`WorkingTue` ,`WorkingWed` ,`WorkingThu` ,`WorkingFri` ,`WorkingSat`)
		VALUES   (v_Cus_,p_ContactName,p_Title ,p_Email,p_CountryCode ,p_MobilePhone ,v_Wor_ ,p_From ,p_To
				   ,p_SMSResend ,p_WorkingSun ,p_WorkingMon ,p_WorkingTue ,p_WorkingWed ,p_WorkingThu ,p_WorkingFri  ,p_WorkingSat);

		set p_ContactID = last_insert_id();

	end if;

	delete from ContactDistributionGroups
	where contactid=p_ContactID;

	select p_ContactID;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spUpsertContactGroup` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spUpsertContactGroup`(
	p_ContactId int,
	p_GroupID int)
BEGIN

	insert into ContactDistributionGroups(ContactID,GroupID)
	values(p_ContactId,p_GroupID);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spUpsertDefinedSensor` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spUpsertDefinedSensor`(
	p_UserID int,
	p_DefinedSensorID int,
	p_BaseSensorID int,
	p_Digits int,
	p_FamilyTypeID int,
	p_Log1 double,
	p_Log2 double,
	p_Name varchar(50),
	p_SensorUnitID int,
	p_Ref1 double,
	p_Ref2 double)
BEGIN

	declare v_Cus_ int;

	select customerid into v_Cus_
	from users
	where id=p_UserID;

    if p_DefinedSensorID>0
	then
		
		update DefinedSensors
		set name=p_Name
		,decimalPlaces = p_Digits
		,log1=p_Log1
		,log2=p_Log2
		,ref1=p_Ref1
		,ref2=p_Ref2
		where customerid = v_Cus_ 
		and basesensorid=p_BaseSensorID 
		and familyTypeID = p_FamilyTypeID
		and SensorUnitID =p_SensorUnitID;

	else
	

		INSERT INTO DefinedSensors
			   (`CustomerID`
			   ,`BaseSensorID`
			   ,`FamilyTypeID`
			   ,`SensorUnitID`
			   ,`Name`
			   ,`DecimalPlaces`
			   ,`Log1`
			   ,`Log2`
			   ,`Ref1`
			   ,`Ref2`)
		 VALUES
			   (v_Cus_
			   ,p_BaseSensorID
			   ,p_FamilyTypeID
			   ,p_SensorUnitID
			   ,p_Name
			   ,p_Digits
			   ,p_Log1
			   ,p_Log2
			   ,p_Ref1
			   ,p_Ref2);



	end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spUpsertDevice` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spUpsertDevice`(
	p_SerialNumber int,
	p_UserID int,
	p_Name nvarchar(50),
	p_DeviceTypeName nvarchar(50),
	p_SiteID int,
	p_StatusID int,
	p_IsRunning tinyint unsigned,
	p_FirmwareVersion double,
	p_BuildNumber int,
	p_MacAddress nvarchar(50)/* ='' */,
	p_BatteryLevel double,
	p_FirmwareRevision nchar(10),
	p_PCBAssembly nchar(10),
	p_PCBVersion nchar(10),
	p_IsResetDownload int/* =0 */,
	p_AlarmDelay int/* =0 */,
	p_InCalibrationMode int/* =0 */)
BEGIN
	

	declare v_Dev_ int;
	declare v_Dev2 int;
	declare v_Cus_ int;

	if p_BuildNumber<0 then
		set p_BuildNumber=0;
	end if;

	if exists(select customerid
			  from users
			where id=p_UserID limit 1)
	then
		select customerid into v_Cus_
		from users
		where id=p_UserID;
	else
		set v_Cus_=0;
	end if;

	select id into v_Dev2
	from DeviceTypes
	where Name= p_DeviceTypeName;

	if p_SiteID<0
	then
		select ifnull(siteid,0) into p_SiteID
		from devices
		where serialnumber=p_SerialNumber;

		if p_SiteID<1
		then
			set p_SiteID=1;
		end if;
	end if;

   if exists(select  id from devices where serialnumber=p_SerialNumber limit 1)
   then 
		update devices
		set Name=p_Name,
		SiteID=p_SiteID,
		StatusID=p_StatusID,
		FirmwareVersion=p_FirmwareVersion,
		BuildNumber=p_BuildNumber,
		BatteryLevel=p_BatteryLevel,
		FirmwareRevision=p_FirmwareRevision,
		PCBAssembly=p_PCBAssembly,
		PCBVersion = p_PCBVersion,
		IsRunning=p_IsRunning ,
		AlarmDelay= p_AlarmDelay,
		InCalibrationMode=p_InCalibrationMode,
		DeviceTypeID = v_Dev2
		where serialnumber=p_SerialNumber;

		select  id into v_Dev_
		from devices 
		where serialnumber=p_SerialNumber limit 1;
        
   else

	INSERT INTO Devices
           (SerialNumber
           ,CustomerID
           ,Name
           ,DeviceTypeID
           ,SiteID
           ,StatusID
           ,MacAddress
           ,FirmwareVersion
		   ,BuildNumber
           ,BatteryLevel
           ,FirmwareRevision
		   ,PCBAssembly
		   ,PCBVersion
		   ,IsRunning
		   ,IsInAlarm
		   ,AlarmDelay
		   ,InCalibrationMode
           )
     VALUES
           (
			p_SerialNumber,
			v_Cus_,
			p_Name,
			v_Dev2,
			p_SiteID,
			p_StatusID,
			p_MacAddress,
			p_FirmwareVersion,
			p_BuildNumber,
			p_BatteryLevel,
			p_FirmwareRevision,
			p_PCBAssembly,
			p_PCBVersion,
			p_IsRunning,
			0,
			p_AlarmDelay,
			p_InCalibrationMode
		   );


		set v_Dev_ = last_insert_id();
	end if;

	/*if @DeviceI... *** SQLINES FOR EVALUATION USE ONLY *** here deviceid=@DeviceId
	end*/

	if p_IsResetDownload>0
	then
		delete from SensorDownloadHistory
		where serialNumber = p_SerialNumber;

		update devices
		set LastSetupTime = fnUNIX_TIMESTAMP(now())
		where serialnumber=p_SerialNumber;
	end if;
	
	select v_Dev_ deviceid;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spUpsertDeviceAlarmData` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spUpsertDeviceAlarmData`(
	
	p_SerialNumber int,
	p_SensorTypeID int,
	p_SensorUnitName nvarchar(50),
	p_AlarmDate int,
	p_AlarmValue double)
BEGIN

	declare v_Dev_ int;
	declare v_Dev2 int;
	declare v_Sen_ int;

	select id into v_Dev_
	from devices
	where serialnumber=p_SerialNumber;

	select id into v_Sen_
	from SensorUnits
	where name= p_SensorUnitName;


	select id into v_Dev2
	from DeviceSensors
	where DeviceID=v_Dev_
	and SensorTypeID = p_SensorTypeID
	and SensorUnitID = v_Sen_;

	-- update the de... *** SQLINES FOR EVALUATION USE ONLY *** 
	update devices 
	set IsInAlarm = 1
	where id=v_Dev_;

	-- then update t... *** SQLINES FOR EVALUATION USE ONLY *** 

    -- an alarm was ... *** SQLINES FOR EVALUATION USE ONLY *** 
	if p_AlarmDate>0
	then
		-- add alarm not... *** SQLINES FOR EVALUATION USE ONLY *** 
		-- alarm type id... *** SQLINES FOR EVALUATION USE ONLY *** 
		if exists(select  id from alarmnotifications 
					where deviceid=v_Dev_ 
					and alarmtypeid=1 
					and devicesensorid=v_Dev2 limit 1)
		then
			update alarmnotifications
			set value=p_AlarmValue
			,StartTime=p_AlarmDate
			where deviceid=v_Dev_ 
			and alarmtypeid=1 
			and devicesensorid=v_Dev2;
		else
			insert into alarmnotifications(deviceid,alarmtypeid,value,starttime,devicesensorid)
			values(v_Dev_,1,p_AlarmValue,p_AlarmDate,v_Dev2);
		end if;
	end if;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spUpsertDeviceLogger` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spUpsertDeviceLogger`(
	p_DeviceID int,
	p_Interval int,
	p_CelsiusMode int)
BEGIN
	

    if exists (select  deviceid from devicelogger where deviceid=p_DeviceID limit 1)
	then 
		update devicelogger
		set `interval` = p_Interval,
		CelsiusMode = p_CelsiusMode
		where deviceid=p_DeviceID;
	else
		insert into devicelogger(deviceid,`interval`,CelsiusMode)
		values(p_DeviceID,p_Interval,p_CelsiusMode);
	end if;

	select FOUND_ROWS();

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spUpsertDeviceMicroLite` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spUpsertDeviceMicroLite`(
	p_DeviceID int, 
	p_ShowMinMax tinyint unsigned,
	p_ShowPast24H tinyint unsigned,
	p_MemorySize int,
	p_EnableLEDOnAlarm tinyint unsigned,
	p_LCDConfig tinyint unsigned,
	p_StopOnDisconnect tinyint unsigned,
	p_StopOnKeyPress tinyint unsigned,
	p_UnitRequiresSetup tinyint unsigned,
	p_AveragePoints tinyint unsigned)
BEGIN

    if exists(select  deviceid from DeviceMicroLite where deviceid=p_DeviceID limit 1)
	then
		update DeviceMicroLite
		set 
		ShowMinMax=p_ShowMinMax,
		ShowPast24H=p_ShowPast24H,
		MemorySize=p_MemorySize,
		EnableLEDOnAlarm=p_EnableLEDOnAlarm,
		LCDConfig=p_LCDConfig,
		StopOnDisconnect=p_StopOnDisconnect,
		StopOnKeyPress=p_StopOnKeyPress,
		UnitRequiresSetup=p_UnitRequiresSetup,
		AveragePoints=p_AveragePoints
		where deviceid=p_DeviceID;
	else
		insert into DeviceMicroLite(DeviceID,ShowMinMax,ShowPast24H,MemorySize,EnableLEDOnAlarm,LCDConfig,StopOnDisconnect,StopOnKeyPress,UnitRequiresSetup,AveragePoints)
		values(p_DeviceID,p_ShowMinMax,p_ShowPast24H,p_MemorySize,p_EnableLEDOnAlarm,p_LCDConfig,p_StopOnDisconnect,p_StopOnKeyPress,p_UnitRequiresSetup,p_AveragePoints);
	end if;
    
	select Found_rows();
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spUpsertDeviceMicroLog` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spUpsertDeviceMicroLog`(
	p_DeviceID int,
	p_AveragePoints int,
	p_DeepSleepMode tinyint unsigned,
	p_EnableLEDOnAlarm tinyint unsigned,
	p_LCDConfiguration tinyint unsigned,
	p_MemorySize int,
	p_ShowMinMax tinyint unsigned,
	p_ShowPast24HMinMax tinyint unsigned,
	p_StopOnDisconnect tinyint unsigned,
	p_StopOnKeyPress tinyint unsigned)
BEGIN

	if exists(select  deviceid from DeviceMicroLog where deviceid=p_DeviceID limit 1)
	then
		update DeviceMicroLog
		set AveragePoints=p_AveragePoints,
		DeepSleepMode=p_DeepSleepMode,
		EnableLEDOnAlarm=p_EnableLEDOnAlarm,
		LCDConfiguration=p_LCDConfiguration,
		MemorySize=p_MemorySize,
		ShowMinMax=p_ShowMinMax,
		ShowPast24HMinMax=p_ShowPast24HMinMax,
		StopOnDisconnect=p_StopOnDisconnect,
		StopOnKeyPress=p_StopOnKeyPress
		where deviceid=p_DeviceID;
	else
		insert into DeviceMicroLog(DeviceID,AveragePoints,DeepSleepMode,EnableLEDOnAlarm,LCDConfiguration,
									MemorySize,ShowMinMax,ShowPast24HMinMax,StopOnDisconnect,StopOnKeyPress)
		values(p_DeviceID,p_AveragePoints,p_DeepSleepMode,p_EnableLEDOnAlarm,p_LCDConfiguration,p_MemorySize,
				p_ShowMinMax,p_ShowPast24HMinMax,p_StopOnDisconnect,p_StopOnKeyPress);
	end if;
    
	select Found_rows();
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spUpsertDeviceMicroX` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spUpsertDeviceMicroX`(
	p_DeviceID int,
	p_CyclicMode tinyint unsigned,
	p_PushToRun tinyint unsigned,
	p_TimerRunEnabled tinyint unsigned,
	p_TimerStart int,
	p_BoomerangEnabled tinyint unsigned,
	p_BoomerangAuthor nvarchar(50),
	p_BoomerangCelsiusMode tinyint unsigned,
	p_BoomerangComment nvarchar(50),
	p_BoomerangContacts nvarchar(200),
	p_BoomerangDisplayAlarmLevels tinyint unsigned,
	p_BoomerangUTC nvarchar(10),
	p_LoggerUTC int)
BEGIN

	

    if exists(select  deviceid from devicemicrox where deviceid=p_DeviceID limit 1)
	then

		if char_length(rtrim(p_BoomerangUTC))=0
		then
			select BoomerangUTC into p_BoomerangUTC
			from DeviceMicroX
			where deviceid=p_DeviceID;
		end if;

		if p_LoggerUTC<>-99
		then
			update devicemicrox
			set CyclicMode=p_CyclicMode,
			PushToRun=p_PushToRun,
			TimerRunEnabled=p_TimerRunEnabled,
			TimerStart=p_TimerStart,
			BoomerangEnabled=p_BoomerangEnabled,
			BoomerangAuthor=p_BoomerangAuthor,
			BoomerangCelsiusMode=p_BoomerangCelsiusMode,
			BoomerangComment=p_BoomerangComment,
			BoomerangContacts=p_BoomerangContacts,
			BoomerangDisplayAlarmLevels=p_BoomerangDisplayAlarmLevels,
			BoomerangUTC =p_BoomerangUTC,
			LoggerUTC = p_LoggerUTC
			where deviceid=p_DeviceID;
		else
			update devicemicrox
			set CyclicMode=p_CyclicMode,
			PushToRun=p_PushToRun,
			TimerRunEnabled=p_TimerRunEnabled,
			TimerStart=p_TimerStart,
			BoomerangEnabled=p_BoomerangEnabled,
			BoomerangAuthor=p_BoomerangAuthor,
			BoomerangCelsiusMode=p_BoomerangCelsiusMode,
			BoomerangComment=p_BoomerangComment,
			BoomerangContacts=p_BoomerangContacts,
			BoomerangDisplayAlarmLevels=p_BoomerangDisplayAlarmLevels,
			BoomerangUTC =p_BoomerangUTC
			where deviceid=p_DeviceID;
		end if;
	else
		if p_LoggerUTC<>-99
		then
			insert into devicemicrox(deviceid,CyclicMode,PushToRun,TimerRunEnabled,TimerStart,BoomerangEnabled,
									BoomerangAuthor,BoomerangCelsiusMode,BoomerangComment,BoomerangContacts,
									BoomerangDisplayAlarmLevels,BoomerangUTC,LoggerUTC)
			values(p_DeviceID,p_CyclicMode,p_PushToRun,p_TimerRunEnabled,p_TimerStart,p_BoomerangEnabled,p_BoomerangAuthor,
					p_BoomerangCelsiusMode,p_BoomerangComment,p_BoomerangContacts,p_BoomerangDisplayAlarmLevels,p_BoomerangUTC,p_LoggerUTC);
		else
			insert into devicemicrox(deviceid,CyclicMode,PushToRun,TimerRunEnabled,TimerStart,BoomerangEnabled,
									BoomerangAuthor,BoomerangCelsiusMode,BoomerangComment,BoomerangContacts,
									BoomerangDisplayAlarmLevels,BoomerangUTC,LoggerUTC)
			values(p_DeviceID,p_CyclicMode,p_PushToRun,p_TimerRunEnabled,p_TimerStart,p_BoomerangEnabled,p_BoomerangAuthor,
					p_BoomerangCelsiusMode,p_BoomerangComment,p_BoomerangContacts,p_BoomerangDisplayAlarmLevels,p_BoomerangUTC,0);
		end if;
	end if;

	select FOUND_ROWS();
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spUpsertDeviceSensors` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spUpsertDeviceSensors`(
	
	p_DeviceID int,
	p_SensorTypeID int,
	p_SensorUnitName nvarchar(50),
	p_IsEnabled tinyint unsigned,
	p_IsAlarmEnabled int,
	p_LowValue double,
	p_HighValue double,
	p_MinValue double,
	p_MaxValue double,
	p_SensorAliasName nvarchar(50),
	p_SensorIndex int,
	
	p_IsDefinedSensor int/* =0 */,
	p_DefineSensorName nvarchar(50)/* ='' */,
	p_CustomUnitName nvarchar(50)/* ='' */,
	p_DecimalPlaces tinyint unsigned/* =0 */,
	p_Gain double/* =0 */,
	p_Offset double/* =0 */,
	p_Log1 double/* =0 */,
	p_Log2 double/* =0 */,
	p_Ref1 double/* =0 */,
	p_Ref2 double/* =0 */)
BEGIN

	declare v_Dev_ int;
	declare v_Sen_ int;
	declare v_Fam_ int;
	declare p_CalibrationExpiry int;
    declare p_PreLowValue float;
    declare p_PreHighValue float;
    declare p_PreDelayValue float;
    declare p_DelayValue float;
    declare p_BuzzDuration int;
    
    set p_BuzzDuration=0;    
    set p_DelayValue=0;
    set p_PreDelayValue=0;
    set p_PreLowValue=0;
    set p_PreHighValue=0;
    
    set p_CalibrationExpiry=0;
    
	select id into v_Sen_
	from SensorUnits
	where name= p_SensorUnitName;

	select FamilyTypeID into v_Fam_
	from DeviceTypes
	inner join Devices
	on Devices.DeviceTypeID = DeviceTypes.ID
	where Devices.ID = p_DeviceID;

	
    if exists (select  id 
				from DeviceSensors 
				where DeviceID=p_DeviceID
				and SensorTypeID = p_SensorTypeID limit 1
				-- and SensorUn... *** SQLINES FOR EVALUATION USE ONLY *** 
				)
	then
		update DeviceSensors
		set SensorTypeID=p_SensorTypeID,
		SensorUnitID = v_Sen_,
		IsEnabled = p_IsEnabled,
		IsAlarmEnabled = p_IsAlarmEnabled,
		CalibrationExpiry = p_CalibrationExpiry,
		MinValue=p_MinValue,
		`MaxValue`=p_MaxValue,
		SensorName = p_SensorAliasName,
		SensorIndex = p_SensorIndex
		where DeviceID=p_DeviceID
		and SensorTypeID = p_SensorTypeID;
		-- and SensorUni... *** SQLINES FOR EVALUATION USE ONLY *** 

		select id into v_Dev_
		from DeviceSensors
		where DeviceID=p_DeviceID
		and SensorTypeID = p_SensorTypeID
		and SensorUnitID = v_Sen_;

		update DeviceAlarmNotificationTrail
		set AlarmEmailSent=0
		,AlarmSMSSent=0
		where DeviceSensorID = v_Dev_;
	else
		insert into DeviceSensors(DeviceID,SensorTypeID,SensorUnitID,IsEnabled,IsAlarmEnabled,CalibrationExpiry,MinValue,`MaxValue`,SensorName,SensorIndex)
		values(p_DeviceID,p_SensorTypeID,v_Sen_,p_IsEnabled,p_IsAlarmEnabled,p_CalibrationExpiry,p_MinValue,p_MaxValue,p_SensorAliasName,p_SensorIndex);

		set v_Dev_ = last_insert_id();

	end if;

	/*if @IsDefin... *** SQLINES FOR EVALUATION USE ONLY *** op 1 id from DefinedSensors where DeviceSensors.ID=@DeviceSensorID)
		begin
			update DefinedSensors
			set Name=@CustomUnitName,
			Gain=@Gain,
			Offset=@Offset,
			Log1=@Log1,
			Log2=@Log2,
			Ref1=@Ref1,
			Ref2=@Ref2
			where id=@DefineSensorID
		end
		else
		begin
			insert into DefinedSensors(FamilyTypeID,BaseSensorID,SensorUnitID,name,DecimalPlaces,Gain,Offset,Log1,Log2,Ref1,Ref2)
			values(@FamilyTypeID,@BaseSensorID,@SensorUnitID,@DefineSensorName,@DecimalPlaces,@Gain,@Offset,@Log1,@Log2,@Ref1,@Ref2)
		end
	end*/
	
	if p_IsAlarmEnabled =1 
	then
		if exists (select id from SensorAlarm where DeviceSensorID = v_Dev_ limit 1)
		then
			update SensorAlarm
			set PreLowValue = p_PreLowValue,
			LowValue = p_LowValue,
			PreHighValue=p_PreHighValue,
			HighValue=p_HighValue,
			PreDelayValue=p_PreDelayValue,
			DelayValue=p_DelayValue,
			BuzzDuration=p_BuzzDuration
			where DeviceSensorID=v_Dev_;

		else
			insert into SensorAlarm(DeviceSensorID,PreLowValue,LowValue,PreHighValue,HighValue,PreDelayValue,DelayValue,BuzzDuration)
			values(v_Dev_,p_PreLowValue,p_LowValue,p_PreHighValue,p_HighValue,p_PreDelayValue,p_DelayValue,p_BuzzDuration);
		end if;
	end if;
	

	select 1;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spUpsertGroup` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spUpsertGroup`(
	p_UserID int,
	p_GroupID int,
	p_ContactID int,
	p_IsUser tinyint unsigned)
BEGIN
	declare v_Cus_ int;

	select customerid into v_Cus_
	from users
	where users.id=p_UserID;

    /*create tabl... *** SQLINES FOR EVALUATION USE ONLY *** #tempTable
	select * from dbo.FnSplit(@GroupContacts,',')*/

	
	insert into ContactDistributionGroups(GroupID,ContactID,IsUser)
	values(p_GroupID,p_ContactID,p_IsUser);
	/*select @Gro... *** SQLINES FOR EVALUATION USE ONLY *** */


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spUpsertPicoLite` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spUpsertPicoLite`(
	p_DeviceID int,
	p_LEDConfig int,
	p_RunDelay int /* =0 */,
	p_RunTime int/* =0 */,
	p_SetupTime int/* =0 */,
	p_StopOnKeyPress int/* =0 */)
BEGIN

    if exists(select  deviceid from DevicePicoLite where DeviceID=p_DeviceID limit 1)
	then
		update DevicePicoLite
		set LEDConfig=p_LEDConfig,
		RunDelay =p_RunDelay,
		RunTime=p_RunTime,
		SetupTime=p_SetupTime,
		StopOnKeyPress=p_StopOnKeyPress
		where deviceid=p_DeviceID;
	else
		insert into DevicePicoLite(DeviceID,LEDConfig,RunDelay,RunTime,SetupTime,StopOnKeyPress)
		values(p_DeviceID,p_LEDConfig,p_RunDelay,p_RunTime,p_SetupTime,p_StopOnKeyPress);
	end if;

	select Found_rows(); 

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spUpsertReportProfile` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spUpsertReportProfile`(
	p_ReportID int /* = 0 */,
	p_UserID int,
	p_ProfileName varchar(32),
	p_Description varchar(125),
	p_IsActive int,
	p_PageSize varchar(10),
	p_DateFormat varchar(15),
	p_TemperatureUnit int,
	p_TimeZoneOffset int,
	p_PeriodTypeID int,
	p_PeriodTime datetime(3) /* = '0:00' */,
	p_PeriodDay int /* = 0 */,
	p_PeriodMonth int /* = 0 */,
	p_PeriodStart int /* = 0 */,
	p_PeriodEnd int /* =0 */,
	p_EmailAsLink int /* = 0 */,
	p_EmailAsPDF int /* = 0 */,
	p_ReportHeaderLink nvarchar(1000))
BEGIN

	if p_ReportID>0
	then
		update ReportTemplates
		set Description=p_Description,
		IsActive=p_IsActive,
		PageSize=p_PageSize,
		ReportDateFormat=p_DateFormat,
		TemperatureUnit=p_TemperatureUnit,
		TimeZoneOffset=p_TimeZoneOffset,
		PeriodTypeID=p_PeriodTypeID,
		PeriodTime=p_PeriodTime,
		PeriodDay=p_PeriodDay,
		PeriodMonth=p_PeriodMonth,
		PeriodStart=p_PeriodStart,
		PeriodEnd=p_PeriodEnd,
		EmailAsLink=p_EmailAsLink,
		EmailAsPDF=p_EmailAsPDF,
		ReportHeaderLink=p_ReportHeaderLink
		where Id= p_ReportID;

		select p_ReportID;

	else
		INSERT INTO ReportTemplates
           (`UserID`
           ,`ProfileName`
           ,`Description`
           ,`IsActive`
           ,`PageSize`
           ,`ReportDateFormat`
           ,`TemperatureUnit`
           ,`TimeZoneOffset`
           ,`PeriodTypeID`
           ,`PeriodTime`
           ,`PeriodDay`
		   ,`PeriodMonth`
           ,`PeriodStart`
           ,`PeriodEnd`
           ,`EmailAsLink`
           ,`EmailAsPDF`
		   ,`ReportHeaderLink`)
     VALUES
           (p_UserID
           ,p_ProfileName
           ,p_Description
           ,p_IsActive
           ,p_PageSize
           ,p_DateFormat
           ,p_TemperatureUnit
           ,p_TimeZoneOffset
           ,p_PeriodTypeID
           ,p_PeriodTime
           ,p_PeriodDay
		   ,p_PeriodMonth
           ,p_PeriodStart
           ,p_PeriodEnd
           ,p_EmailAsLink
           ,p_EmailAsPDF
		   ,p_ReportHeaderLink);

		   select last_insert_id();

	end if;
	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spUpsertSensorAlarmNotification` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spUpsertSensorAlarmNotification`(

	p_DeviceSensorID int,
	p_ContactID int,
	p_ReceiveEmail int,
	p_ReceiveSMS int)
BEGIN

	if exists (select devicesensorid from DeviceAlarmNotifications where DeviceSensorID = p_DeviceSensorID 
					and ContactID=p_ContactID limit 1)
	then
		update DeviceAlarmNotifications
		set ReceiveEmail=p_ReceiveEmail
		,ReceiveSMS=p_ReceiveSMS
		where DeviceSensorID = p_DeviceSensorID 
		and ContactID=p_ContactID;
	else 
		insert into DeviceAlarmNotifications(DeviceSensorID,ContactID, ReceiveEmail,ReceiveSMS)
		values(p_DeviceSensorID,p_ContactID,p_ReceiveEmail,p_ReceiveSMS);
	end if;
   
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spUserGuidIsValid` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spUserGuidIsValid`(
	p_GUID varchar(200))
BEGIN
	declare v_Pas_ datetime(3);
    declare v_Exp_ int;
	declare v_gui_ char(36);

	SELECT  v_gui_= CAST(
        SUBSTRING(p_GUID, 1, 8) + '-' + SUBSTRING(p_GUID, 9, 4) + '-' + SUBSTRING(p_GUID, 13, 4) + '-' +
        SUBSTRING(p_GUID, 17, 4) + '-' + SUBSTRING(p_GUID, 21, 12)
        AS CHAR(36));

	select PasswordResetExpiry into v_Pas_
	from users
	where PasswordResetGUID=v_gui_;

	set v_Exp_ = TIMESTAMPDIFF(day,now(),v_Pas_);

	if exists(select  users.ID from users where users.PasswordResetGUID=v_gui_ limit 1) 
		and v_Exp_>0
	then
		select 1;
	else
		select 0;
	end if;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spValidateLogin` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spValidateLogin`(
	p_UserName NVARCHAR(50),
	p_Password NVARCHAR(50))
BEGIN
	-- SET NOCOUNT ... *** SQLINES FOR EVALUATION USE ONLY *** 
	-- interfering ... *** SQLINES FOR EVALUATION USE ONLY *** 

	declare v_Use_ int;
	declare v_con_ int;
	declare v_cus_ int;
	declare v_Max_ bigint;
	declare v_Dat_ int;

	SELECT ID, CustomerID INTO v_Use_, v_cus_ FROM Users WHERE Username=p_UserName AND UserPassword=Upper(p_Password);
	IF v_Use_ IS NULL
	THEN
		SELECT 0;
		
	ELSE
		/*	
			Create... *** SQLINES FOR EVALUATION USE ONLY ***  for further verifications 
			Utilize PasswordResetGUID field for the task (we do not modify the table definition)
		*/
		UPDATE Users SET PasswordResetGUID=UUID() WHERE ID=v_Use_;

		select ifnull(ConcurrentUsersLimit,1),ifnull(MaxStorageAllowed,0),ifnull(DataStorageExpiry,0) into v_con_, v_Max_, v_Dat_
		from CustomerSettings
		where CustomerID = v_cus_;

		/*	Retrieve U... *** SQLINES FOR EVALUATION USE ONLY *** */ 
		SELECT Users.ID UserID, Users.FirstName, Users.LastName , PasswordResetGUID AS API_KEY
		,v_con_ concurrentUsersLimit ,Devices.SerialNumber,Devices.SiteID
		,(select IsCFREnabled from CustomerSettings where CustomerSettings.CustomerID =Users.CustomerID ) isCfr
		,(users.roleid - 1) roleid,v_cus_ customerid,v_Max_ maxStorageAllowed,v_Dat_ DataStorageExpiry
		FROM Users 
		/*inner join ... *** SQLINES FOR EVALUATION USE ONLY *** onUserSiteDeviceGroup.UserID = Users.ID*/
		left join Devices
		on Devices.CustomerID = Users.CustomerID
		WHERE Users.ID=v_Use_
		and ifnull(Users.IsActive,0)=1
		and ifnull(Users.deleted,0)=0;
		
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spValidateResetPassword` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spValidateResetPassword`(
	p_CustomerID INT,
	p_Email NVARCHAR(255),
	p_SequreQuestionID INT,
	p_SecureAnswer NVARCHAR(255))
BEGIN
	-- SET NOCOUNT ... *** SQLINES FOR EVALUATION USE ONLY *** 
	-- interfering ... *** SQLINES FOR EVALUATION USE ONLY *** 

	DECLARE v_Use_ INT;
	
	SET v_Use_ = 0;

	-- checks if the... *** SQLINES FOR EVALUATION USE ONLY *** 
	IF EXISTS (SELECT  dbo.Users.ID FROM Users WHERE dbo.Users.CustomerID=p_CustomerID AND dbo.Users.Email=p_Email limit 1)
	THEN
		SELECT dbo.Users.ID INTO v_Use_ 
		FROM Users 
		WHERE dbo.Users.CustomerID=p_CustomerID 
		AND dbo.Users.Email=p_Email;
		-- checks if the... *** SQLINES FOR EVALUATION USE ONLY *** 
		IF EXISTS(SELECT  dbo.UserSecureQuestions.UserID FROM UserSecureQuestions WHERE UserID=v_Use_ AND QuestionID = p_SequreQuestionID AND Answer=p_SecureAnswer limit 1)
		then
			-- create guid f... *** SQLINES FOR EVALUATION USE ONLY *** 
			UPDATE Users
			SET PasswordResetGUID = UUID()
			WHERE dbo.Users.ID = v_Use_;
			
		END IF;
	END IF; 

	-- return the gu... *** SQLINES FOR EVALUATION USE ONLY *** 
	-- (the email wi... *** SQLINES FOR EVALUATION USE ONLY *** 
	SELECT PasswordResetGUID 
	FROM Users
	WHERE dbo.Users.ID = v_Use_;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spWipeSensorLogsData` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `spWipeSensorLogsData`()
BEGIN

    delete from sensorlogs;

	select FOUND_ROWS();
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-17  5:06:30
