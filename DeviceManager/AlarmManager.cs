﻿using Base.Devices.Management.EventsAndExceptions;
using Base.Sensors.Samples;
using Infrastructure;
using Infrastructure.MailService;
using Server.Infrastructure.SMS;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Server.Infrastructure;

namespace DeviceLayer
{
    public class AlarmManager
    {
        #region Properties
        public ConcurrentDictionary<string, DeviceAlarm> Devices { get; private set; }

        
        #endregion
        #region Constructor
        private static readonly Lazy<AlarmManager> lazy = new Lazy<AlarmManager>(() => new AlarmManager());

        public static AlarmManager Instance
        {
            get { return lazy.Value; }
        }

        private AlarmManager() { Devices = new ConcurrentDictionary<string, DeviceAlarm>(); }
        #endregion
        #region Methods
        public void AlarmCheck(SampleAddedEventArgs args)
        {
            var alarmStatusChanged = false;
            //checks if the device is exists in the devices list
            DeviceAlarm device;
            if (Devices.TryGetValue(args.SerialNumber, out device))
            {
                //if exists then it was in alarm
                //if in normal mode
                if (args.Sample.AlarmStatus == Base.Sensors.Alarms.eAlarmStatus.Normal)
                {
                    //checks if the sensor was in alarm
                    if (device.IsSensorInAlarm(args.Type))
                    {
                        Log4Tech.Log.Instance.Write("Device #{0} is normalized", this, Log4Tech.Log.LogSeverity.DEBUG, args.SerialNumber);
                        Log4Tech.Log.Instance.Write("Save alarm in database", this);
                        DataManager.Instance.UpdateAlarmNotification(args.SerialNumber, DataManager.AlarmType.Sensor, args.Type);
                        //if not in alarm then send email/sms
                        Log4Tech.Log.Instance.Write("Checking if need to send notification", this);
                        sendNotificationEmailSMS(args, Server.Infrastructure.SMS.SMSManager.AlarmStatus.Normalized);
                        //remove from the list of sensor alarms managed by device alarm
                        device.RemoveSensorInAlarm(args.Type);
                        //update the device in the list
                        DeviceAlarm removeDevice;
                        if (Devices.TryRemove(args.SerialNumber, out removeDevice))
                        {
                            Devices.TryAdd(args.SerialNumber, device);
                        }
                        alarmStatusChanged = true;
                    }
                }
                else
                {
                    Log4Tech.Log.Instance.Write("Alarm has indicated for #{0} on {1}", this, Log4Tech.Log.LogSeverity.DEBUG, args.SerialNumber, args.Type);
                    Log4Tech.Log.Instance.Write("Save alarm in database", this);
                    var alarm = DataManager.Instance.InsertAlarmNotification(args.SerialNumber, DataManager.AlarmType.Sensor, Convert.ToDouble(args.Sample.Value), args.Sample.Date, args.Type);
                    //push to GUI this alarm
                    if (alarm != null && alarm.Alarms != null && alarm.Alarms.Count > 0)
                    {
                        DeviceAlarm tempDevice;
                        //only if the sensor was not in alarm already
                        if (!Devices.TryGetValue(args.SerialNumber, out tempDevice))
                            //then raise the flag
                            alarmStatusChanged = true;

                        //then send email/sms
                        Log4Tech.Log.Instance.Write("Checking if need to send notification", this);
                        sendNotificationEmailSMS(args, SMSManager.AlarmStatus.Alarmed);
                        //add the current sensor in alarm
                        device.AddSensorInAlarm(args.Type);
                        DeviceAlarm removeDevice;
                        if (Devices.TryRemove(args.SerialNumber, out removeDevice))
                            Devices.TryAdd(args.SerialNumber, device);
                    }
                }
            }
            else
            {
                //if not exist in the list and in alarm
                if (args.Sample.AlarmStatus != Base.Sensors.Alarms.eAlarmStatus.Normal)
                {
                    Log4Tech.Log.Instance.Write("Alarm has indicated for #{0} on {1}", this, Log4Tech.Log.LogSeverity.DEBUG, args.SerialNumber, args.Type);
                    Log4Tech.Log.Instance.Write("Save alarm in database", this);
                    var alarm = DataManager.Instance.InsertAlarmNotification(args.SerialNumber, DataManager.AlarmType.Sensor, Convert.ToDouble(args.Sample.Value), args.Sample.Date, args.Type);
                    //push to GUI this alarm
                    //PushGUI.Instance.SendAlarm(alarm);
                    //then send email/sms
                    Log4Tech.Log.Instance.Write("Checking if need to send notification", this);
                    sendNotificationEmailSMS(args, SMSManager.AlarmStatus.Alarmed);
                    //add to the devices list
                    var newDevice = new DeviceAlarm();
                    newDevice.AddSensorInAlarm(args.Type);
                    Devices.TryAdd(args.SerialNumber, newDevice);

                    DeviceAlarm tempDevice;
                    //only if the sensor was not in alarm already
                    if (!Devices.TryGetValue(args.SerialNumber, out tempDevice))
                        //then raise the flag
                        alarmStatusChanged = true;
                }
                else
                {
                    Log4Tech.Log.Instance.Write("Device #{0} is normalized", this, Log4Tech.Log.LogSeverity.DEBUG, args.SerialNumber);
                    Log4Tech.Log.Instance.Write("Save alarm in database", this);
                    DataManager.Instance.UpdateAlarmNotification(args.SerialNumber, DataManager.AlarmType.Sensor, args.Type);
                    Log4Tech.Log.Instance.Write("Checking if need to send notification", this);
                    sendNotificationEmailSMS(args, Server.Infrastructure.SMS.SMSManager.AlarmStatus.Normalized);
                    //add to the devices list
                    var newDevice = new DeviceAlarm();
                    Devices.TryAdd(args.SerialNumber, newDevice);
                     DeviceAlarm tempDevice;
                        //only if the sensor was not in alarm already
                        if (!Devices.TryGetValue(args.SerialNumber, out tempDevice))
                            //then raise the flag
                            alarmStatusChanged = true;
                }
            }

            if(alarmStatusChanged)
                //send the device to GUI as device changed
                PushGUIManager.SendDevice(Convert.ToInt32(args.SerialNumber), PushGUIManager.GUIEvents.deviceChanged);
        }

        public void BatteryCheck(ConnectionEventArgs<Base.Devices.GenericDevice> args)
        {
            DeviceAlarm device;
            var isBatterChanged = false;
            //checks if the device exists in the list of devices
            if (Devices.TryGetValue(args.Device.Status.SerialNumber, out device))
            {
                //checks if the battery was in alarm and the current value is above the minimum
                if (device.BatteryInAlarm)
                {
                    //send battery normalized email/sms
                    if(checkBatteryLowNotifications(args))
                    {
                        DeviceAlarm newDevice = device;
                        newDevice.BatteryInAlarm = false;
                        Devices.TryUpdate(args.Device.Status.SerialNumber, newDevice, device);
                        isBatterChanged = true;
                    }
                }
                else
                {
                    //send email/sms
                    if(checkBatteryLowNotifications(args))
                    {
                        DeviceAlarm newDevice = device;
                        newDevice.BatteryInAlarm = true;
                        Devices.TryUpdate(args.Device.Status.SerialNumber, newDevice, device);
                        isBatterChanged = true;
                    }
                }
            }
            else
            {
                if (checkBatteryLowNotifications(args))
                {
                    DeviceAlarm newDevice = new DeviceAlarm();
                    newDevice.BatteryInAlarm = true;
                    Devices.TryAdd(args.Device.Status.SerialNumber, newDevice);
                    isBatterChanged = true;
                }
            }
            if (isBatterChanged)
                //send the device to GUI as device changed
                PushGUIManager.SendDevice(Convert.ToInt32(args.Device.Status.SerialNumber), PushGUIManager.GUIEvents.deviceChanged);
        }
        #endregion
        #region private methods
        
        private bool checkBatteryLowNotifications(ConnectionEventArgs<Base.Devices.GenericDevice> args)
        {
            var notificationSent = false;
            try
            {
                //checks if has contacts related to this device
                var result = DataManager.Instance.GetNotificationContacts(args.Device.Status.SerialNumber);
                 if (result.Result.ToString() != "OK") return false;

                if (((List<dynamic>) result.Contacts).Count > 0)
                {
                    ((List<dynamic>) result.Contacts).ForEach((contact) =>
                    {
                        //checks if the battery is lower than what was defined
                        if (contact.BatteryLowValue > 0 &&
                            args.Device.Battery.BatteryLevel < (byte) contact.BatteryLowValue
                            && Convert.ToInt32(contact.ReceiveBatteryLow.ToString()) == 1 &&
                            contact.BatteryEmailSent == 0)
                        {
                            //send email/sms
                            Log4Tech.Log.Instance.Write(
                                "[Notification] #{0} on {1} battery low triggered (Battery Level = {2}). Sending email to {3}",
                                this, Log4Tech.Log.LogSeverity.INFO, args.Device.Status.SerialNumber, args.Device.GetType(),
                                args.Device.Battery.BatteryLevel, contact.Email);
                            Log4Tech.Log.Instance.Write("Save alarm in database", this);
                            var alarm = DataManager.Instance.InsertAlarmNotification(args.Device.Status.SerialNumber,
                                DataManager.AlarmType.Battery, args.Device.Battery.BatteryLevel, DateTime.Now, 0);
                            //push to GUI this alarm
                            PushGUIManager.SendAlarm(alarm);
                            var subject = string.Format("#{1} ({0}) has low battery alarm = {2}",
                                args.Device.Status.Comment, args.Device.Status.SerialNumber, args.Device.Battery.BatteryLevel);
                            var emailBody = "";
                            Common.SendEmail(contact.Email.ToString(), subject, emailBody);
                            //update the email sent time
                            DataManager.Instance.UpdateNotificationBatteryEmailSent(args.Device.Status.SerialNumber, contact.ContactID);
                            //sends SMS
                            if (Convert.ToInt32(contact.ReceiveSMS.ToString()) == 1 && contact.SMSSent == 0)
                            {
                                long mobileNumber =
                                    Convert.ToInt64(string.Concat(contact.AreaCode.ToString(),
                                        contact.PhoneNumber.ToString()));
                                string bodySMS = string.Format("#{1} ({0}) has low battery alarm = {2}",
                                    args.Device.Status.Comment, args.Device.Status.SerialNumber, args.Device.Battery.BatteryLevel);
                                SMSManager.Instance.SendSMS(Server.Infrastructure.SMS.SMSManager.AlarmType.Battery,
                                    args.Device.Status.SerialNumber, contact.ContactID, contact.CountryCode, mobileNumber, bodySMS,
                                    contact.SMSResendInterval);
                            }
                            //set the flag
                            notificationSent = true;
                        }
                        else if (contact.BatteryLowValue > 0 &&
                                 args.Device.Battery.BatteryLevel > (byte) contact.BatteryLowValue
                                 && Convert.ToInt32(contact.ReceiveBatteryLow.ToString()) == 1)
                        {
                            Log4Tech.Log.Instance.Write(
                                "[Notification] #{0} on {1} battery normalized triggered (Battery Level = {2}). Sending email to {3}",
                                this, Log4Tech.Log.LogSeverity.INFO, args.Device.Status.SerialNumber, args.Device.GetType(),
                                args.Device.Battery.BatteryLevel, contact.Email);
                            Log4Tech.Log.Instance.Write("Save alarm in database", this);
                            DataManager.Instance.UpdateAlarmNotification(args.Device.Status.SerialNumber,
                                DataManager.AlarmType.Battery, 0);

                            var subject = string.Format("#{1} ({0}) has normalized battery",
                                args.Device.Status.Comment, args.Device.Status.SerialNumber);
                            var emailBody = "";
                            Common.SendEmail(contact.Email.ToString(), subject, emailBody);
                            //update the email sent time
                            DataManager.Instance.UpdateNotificationBatteryEmailSent(args.Device.Status.SerialNumber,
                                contact.ContactID);
                            //sends SMS
                            if (Convert.ToInt32(contact.ReceiveSMS.ToString()) == 1 && contact.SMSSent == 0)
                            {
                                long mobileNumber =
                                    Convert.ToInt64(string.Concat(contact.AreaCode.ToString(),
                                        contact.PhoneNumber.ToString()));
                                var bodySMS = string.Format("#{1} ({0}) has normalized battery",
                                    args.Device.Status.Comment, args.Device.Status.SerialNumber);
                                SMSManager.Instance.SendSMS(Server.Infrastructure.SMS.SMSManager.AlarmType.Battery,
                                    args.Device.Status.SerialNumber, contact.ContactID, contact.CountryCode, mobileNumber, bodySMS,
                                    contact.SMSResendInterval);
                            }
                            notificationSent = true;
                        }
                    });

                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in checkBatteryLowNotifications", this, ex);
            }
            return notificationSent;
        }

        private void sendNotificationEmailSMS(SampleAddedEventArgs args,SMSManager.AlarmStatus alarmStatus)
        {
            try
            {
                //first checks if any contact is related to this device
                var result = DataManager.Instance.GetNotificationContacts(args.SerialNumber, (int)args.Type);
                if (result.Result.ToString() != "OK") return;

                if (((List<dynamic>)result.Contacts).Count > 0)
                {
                    ((List<dynamic>)result.Contacts).ForEach((contact) =>
                    {
                        if (Convert.ToInt32(contact.ReceiveEmail.ToString()) == 1)//handle EMAIL
                        {
                            string subject = "";
                            string bodyEmail = "";
                            if (alarmStatus == SMSManager.AlarmStatus.Alarmed && contact.EmailSent == 0)
                            {
                                Log4Tech.Log.Instance.Write("[Notification] #{0} on {1} alarm triggered. Sending email to {2}", this, Log4Tech.Log.LogSeverity.INFO, args.SerialNumber, args.Type, contact.Email);
                                subject = string.Format("#{1} ({0}) is in {2}", args.Name, args.SerialNumber, args.Sample.AlarmStatus);
                                bodyEmail = string.Format("Device #{0} is in alarm on sensor {1} with value {2}",args.SerialNumber,args.Name,args.Sample.Value);
                            }
                            else if (alarmStatus == SMSManager.AlarmStatus.Normalized && contact.EmailSent > 0)
                            {
                                Log4Tech.Log.Instance.Write("[Notification] #{0} on {1} alarm normalized. Sending email to {2}", this, Log4Tech.Log.LogSeverity.INFO, args.SerialNumber, args.Type, contact.Email);
                                subject = string.Format("#{1} ({0}) is Normalized", args.Name, args.SerialNumber);
                                bodyEmail = string.Format("Device #{0} is normalized on sensor {1} with value {2}", args.SerialNumber, args.Name, args.Sample.Value);
                            }
                            if (bodyEmail.Length > 0)
                            {
                                Common.SendEmail(contact.Email.ToString(), subject, bodyEmail);
                                //update the email sent time
                                DataManager.Instance.UpdateNotificationEmailSent(args.SerialNumber, contact.ContactID, (int)args.Type);
                            }
                        }
                        if (Convert.ToInt32(contact.ReceiveSMS.ToString()) == 1)//handle SMS
                        {
                            long mobileNumber = Convert.ToInt64(contact.PhoneNumber.ToString());
                            var bodySms = "";
                            if (alarmStatus == SMSManager.AlarmStatus.Normalized && contact.SMSSent > 0)
                            {
                                //build the message to be sent to the contact
                                bodySms = string.Format("Device #{0} is back to normal state at {1}", args.SerialNumber, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                            }
                            else if (alarmStatus == SMSManager.AlarmStatus.Alarmed && contact.SMSSent == 0)
                            {
                                //build the message to be sent to the contact
                                bodySms = string.Format("Device #{0} on {1} is in {2} value - {3} at {4}", args.SerialNumber, args.Type, args.Sample.AlarmStatus, args.Sample.Value ,DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                            }
                            //only if the body of sms has value ,meaning he SMS should be sent
                            if (bodySms.Length <= 0) return;

                            Log4Tech.Log.Instance.Write("Sending SMS to {0} on device #{1} with body {2}", this, Log4Tech.Log.LogSeverity.DEBUG, string.Concat(contact.CountryCode, mobileNumber), args.SerialNumber, bodySms);
                            var conuntryCode=  DataManager.Instance.GetCountryCallingCode(contact.CountryName.ToString());
                            SMSManager.Instance.SendSMS(SMSManager.AlarmType.Sensor, args.SerialNumber, Convert.ToInt32(contact.ContactID.ToString()), conuntryCode, mobileNumber, bodySms, Convert.ToInt32(contact.SMSResendInterval.ToString()), (int)args.Type);
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in sendNotificationEmailSMS", this, ex);
            }
        }

//        private void sendEmail(string toEmail, string subject, string emailBody)
//        {
//            var fromMail = new MailAddress(ConfigurationManager.AppSettings["fromEmail"], "No-Reply");
//            var mail = new MailMessage
//            {
//                Body = emailBody,
//                From = fromMail,
//                IsBodyHtml = true,
//                Subject = subject,
//                To = { toEmail },
//            };
//            MailManager.SendEmail(mail);
//        }
        #endregion
    }
}
