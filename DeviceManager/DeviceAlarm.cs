﻿using Base.Sensors.Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeviceLayer
{
    public class DeviceAlarm
    {
        #region Properties
        public List<eSensorType> SensorsInAlarm { get; private set; }
        public bool BatteryInAlarm { get; set; }
        #endregion
        #region Constructor
        public DeviceAlarm()
        {
            SensorsInAlarm = new List<eSensorType>();
        }
        #endregion
        #region Methods
        public void AddSensorInAlarm(eSensorType type)
        {
            if (!SensorsInAlarm.Exists(sensorType=> type == sensorType ))
            {
                SensorsInAlarm.Add(type);
            }
        }

        public void RemoveSensorInAlarm(eSensorType type)
        {
            if (SensorsInAlarm.Exists(sensorType => type == sensorType))
            {
                SensorsInAlarm.Remove(type);
            }
        }
        
        public bool IsSensorInAlarm(eSensorType type)
        {
            return SensorsInAlarm.Exists(sensorType => type == sensorType);
        }
        #endregion
        #region private methods
        //private bool IsSensorExists(eSensorType type)
        //{

        //}
        #endregion
    }
}
