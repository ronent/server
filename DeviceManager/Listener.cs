﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Base.Devices;
using Base.Devices.Management.EventsAndExceptions;
using Base.Misc;
using Base.OpCodes;
using Base.Sensors.Samples;
using Base.Sensors.Types;
using Infrastructure;
using Log4Tech;
using Server.Infrastructure;
using Server.Infrastructure.Interfaces;

namespace DeviceLayer
{
    
    public class Listener : IDevice
    {
        private readonly int _listeningPort;
        private readonly ConcurrentDictionary<int, NetworkStream> _clients = new ConcurrentDictionary<int, NetworkStream>();
        private readonly Manager manager;
        //private TcpListener listener;
        //private EventListener eventListener;
        private readonly IWcfServer wcfServerCommunication;
        private const int waitForStatus = 5*1000;//in miliseonds

        private static ManualResetEventSlim mrEventSlim = new ManualResetEventSlim();
        private ConcurrentDictionary<int, Result> deviceLastStatus=new ConcurrentDictionary<int, Result>();
      
        public Listener()
        {
            DataManager.Instance.UpdateDevicesDisconnection();

            manager = new Manager(this);
            var wcfServer = new WcfServerManager();
            
            wcfServer.Start();
            wcfServerCommunication = wcfServer.Server;

            wcfServerCommunication.OnDeviceConnected += OnOnDeviceConnected;
            wcfServerCommunication.OnDeviceRemoved += OnOnDeviceRemoved;
            wcfServerCommunication.OnDownloadComplete += OnDownloadComplete;
            wcfServerCommunication.OnDeviceStatus += OnDeviceStatus;
            wcfServerCommunication.OnDownloadProgress += OnDownloadProgress;
            wcfServerCommunication.OnFirmwareProgress += OnFirmwareProgress;
            wcfServerCommunication.OnOnlineSample += OnOnlineSample;
            wcfServerCommunication.OnStatus += OnStatus;
        }

        #region Wcf Events

        private void OnStatus(byte[] eBytes)
        {
            var deviceStatus = (Tuple<GenericDevice, int, Result>)Common.ByteArrayToObject(eBytes);
            Log.Instance.Write("[WCF Server EVENT] Status for device #{0} is {1}", this, Log.LogSeverity.INFO, deviceStatus.Item1.Status.SerialNumber, deviceStatus.Item3);
            manager.Status(deviceStatus);
            if (!mrEventSlim.IsSet)
            {
                deviceLastStatus.AddOrUpdate(Convert.ToInt32(deviceStatus.Item1.Status.SerialNumber), deviceStatus.Item3,
                    (key, oldValue) => deviceStatus.Item3);
                mrEventSlim.Set();
            }
        }

        private void OnDeviceStatus(byte[] eBytes)
        {
           // var deviceStatus = (ConnectionEventArgs<GenericDevice>)Common.ByteArrayToObject(eBytes);
           // Log.Instance.Write("[WCF Server EVENT] Device Status for device #{0}", this, Log.LogSeverity.INFO, deviceStatus.Device.Status.SerialNumber);
           // manager.DeviceStatus(deviceStatus);
        }

        private void OnDownloadProgress(byte[] eBytes)
        {
            var progressDownload = (ProgressReportEventArgs) Common.ByteArrayToObject(eBytes);
            Log.Instance.Write("[WCF Server EVENT] Download Progress {0}% for device #{1}", this, Log.LogSeverity.INFO, progressDownload.Percent, progressDownload.SerialNumber);
            manager.DownloadStatus(progressDownload);
        }

        private void OnFirmwareProgress(byte[] eBytes)
        {
            var progressDownload = (ProgressReportEventArgs)Common.ByteArrayToObject(eBytes);
            Log.Instance.Write("[WCF Server EVENT] Firmware Progress {0}% for device #{1}", this, Log.LogSeverity.INFO, progressDownload.Percent, progressDownload.SerialNumber);
            manager.FWStatus(progressDownload);
        }

        private void OnOnlineSample(byte[] eBytes)
        {
            var onlineSample = (SampleAddedEventArgs) Common.ByteArrayToObject(eBytes);
            Log.Instance.Write("[WCF Server EVENT] Online Sample {0} for device #{1}", this, Log.LogSeverity.INFO, onlineSample.Sample.Value, onlineSample.SerialNumber);
            var device = manager.GetConnectedDevice(Convert.ToInt32(onlineSample.SerialNumber));
            if (device != null && device.Device.InCalibrationMode)
                manager.CalibrationSample(onlineSample);
            else
            {
                if (onlineSample.Sample.IsDummy) return;
                manager.NewSample(onlineSample);
                AlarmManager.Instance.AlarmCheck(onlineSample);
            }
        }

        private void OnDownloadComplete(byte[] eBytes)
        {
            var downloadComplete = (Base.OpCodes.Helpers.DownloadCompletedArgs)Common.ByteArrayToObject(eBytes);
            Log.Instance.Write("[WCF Server EVENT] Download Completed for device #{0}", this, Log.LogSeverity.INFO, downloadComplete.SerialNumber);
            manager.DownloadComplete(downloadComplete);
        }

        private void OnOnDeviceRemoved(byte[] eBytes)
        {
            var obj = (ConnectionEventArgs<GenericDevice>)Common.ByteArrayToObject(eBytes);
            Log.Instance.Write("[WCF Server EVENT] #{0} has been disconnected", this, Log.LogSeverity.INFO, obj.Device.Status.SerialNumber);
            deviceRemoved(obj);
        }

        private void OnOnDeviceConnected(byte[] eBytes)
        {
            var obj = (ConnectionEventArgs<GenericDevice>)Common.ByteArrayToObject(eBytes);
            Log.Instance.Write("[WCF Server EVENT] #{0} connected", this, Log.LogSeverity.INFO, obj.Device.Status.SerialNumber);
            deviceConnected(obj);
        }

        #endregion

        #region Methods

        public void SendMessageToClient(DeviceAction deviceAction)
        {
             wcfServerCommunication.Execute(deviceAction);
        }

        #endregion

        #region Functions

        private void deviceRemoved(ConnectionEventArgs<GenericDevice> e)
        {
            //take care of device removed
            manager.DeviceUpdate(e, DeviceManager.Statuses.Disconnected);
        }

        private void deviceConnected(ConnectionEventArgs<GenericDevice> e)
        {
            //checks if need to autosetup a device
            manager.CheckDeviceAutoSetup(Convert.ToInt32(e.Device.Status.SerialNumber));
            //take care of device connected
            manager.DeviceUpdate(e, DeviceManager.Statuses.Connected);
            //take down the flag of firmware update
            DataManager.Instance.UpdateDeviceFwFlag(Convert.ToInt32(e.Device.Status.SerialNumber), 0);
        }

        #endregion

        #region Not In Use
        //        private void init()
        //        {
        //            var ipAddre = IPAddress.Loopback;
        //            listener = new TcpListener(ipAddre, _listeningPort);
        //            listener.Start();
        //            Log.Instance.Write("Server is running listening on port " + _listeningPort, this);
        //
        //            Task.Factory.StartNew(startListen);
        //        }

        //        private async void startListen()
        //        {
        //            while (true)
        //            {
        //                //Log.Instance.Write("Waiting for connections...", this);
        //                try
        //                {
        //                    var tcpClient = await listener.AcceptTcpClientAsync();
        //                    var clientInfo = tcpClient.Client.RemoteEndPoint.ToString();
        //                    Log.Instance.Write("Got connection request from {0}", this, Log.LogSeverity.DEBUG, clientInfo);
        //                    
        //                    HandleConnectionAsync(tcpClient);
        //
        //                }
        //                catch (Exception ex)
        //                {
        //                    Log.Instance.WriteError("Fail in start the tcp listener", this, ex);
        //                }
        //            }
        //        }

        //        public NetworkStream GetConnectedClient(int serialNumber)
        //        {
        //            NetworkStream tempStream;
        //            return _clients.TryGetValue(serialNumber, out tempStream) ? tempStream : null;
        //        }

        //        public void SendEventAck(DeviceAction deviceAction, NetworkStream networkStream)
        //        {
        //            var formatter = new BinaryFormatter();
        //            formatter.Serialize(networkStream, deviceAction);
        //        }
        //        private void addClient(int serialNumber, NetworkStream client)
        //        {
        //            NetworkStream tempClient;
        //            if (_clients.TryGetValue(serialNumber, out tempClient))
        //                _clients.TryRemove(serialNumber, out tempClient);
        //
        //            _clients.TryAdd(serialNumber, client);
        //        }

        //        private void HandleConnectionAsync(TcpClient tcpClient)
        //        {
        //            try
        //            {
        //                var networkStream = tcpClient.GetStream();
        //                //parseClientMessage(networkStream);
        //
        //                //var ms = GetAllData(networkStream);
        //                parseClientMessage(networkStream);//,ms);
        //
        //                waitForClient(networkStream);
        //            }
        //            catch (Exception ex)
        //            {
        //                Log.Instance.WriteError("Fail in handle connection", this, ex);
        //            }
        //        }

        //        private void parseClientMessage(NetworkStream networkStream)
        //        {
        //            var formatter = new BinaryFormatter();
        //            try
        //            {
        //                var clientSource = formatter.Deserialize(networkStream);
        //                var type = clientSource.GetType();
        //
        //                if ((type.Name).Contains("ConnectionEventArgs"))
        //                {
        //                    var device = (ConnectionEventArgs<GenericDevice>) clientSource;
        //                    addClient(Convert.ToInt32(device.Device.Status.SerialNumber), networkStream);
        //                    Log.Instance.Write("[Agent Client] logger #{0} is {1}", this, Log.LogSeverity.DEBUG,
        //                        device.Device.Status.SerialNumber, device.State == eDeviceState.FOUND ? "Connected" : "Disconnected");
        //                    if (device.State == eDeviceState.FOUND)
        //                        deviceConnected(device);
        //                    else
        //                        deviceRemoved(device);
        //                }
        //                else if ((type.Name).Contains("DownloadCompletedArgs"))
        //                {
        //                    var downloadComplete = (Base.OpCodes.Helpers.DownloadCompletedArgs) clientSource;
        //                    manager.DownloadComplete(downloadComplete);
        //                }
        //                else if ((type.Name).Contains("ProgressReportEventArgs"))
        //                {
        //                    var progressDownload = (ProgressReportEventArgs) clientSource;
        //                    manager.DownloadStatus(progressDownload);
        //                }
        //                else if ((type.Name).Contains("SampleAddedEventArgs"))
        //                {
        //                    var onlineSample = (SampleAddedEventArgs) clientSource;
        //                    var device = manager.GetConnectedDevice(Convert.ToInt32(onlineSample.SerialNumber));
        //                    if (device != null && device.Device.InCalibrationMode)
        //                    {
        //                        manager.CalibrationSample(onlineSample);
        //                    }
        //                    else
        //                    {
        //                        if (onlineSample.Sample.IsDummy) return;
        //                        //report new sample
        //                        manager.NewSample(onlineSample);
        //                        AlarmManager.Instance.AlarmCheck(onlineSample);
        //                    }
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                formatter = null;
        //            }
        //            
        //        }
        //        private void waitForClient(NetworkStream networkStream)
        //        {
        //            Task.Factory.StartNew(() =>
        //            {
        //                while (networkStream.CanRead)
        //                {
        //                    if (networkStream.DataAvailable)
        //                    {
        //                      //  var ms = GetAllData(networkStream);
        //                        parseClientMessage(networkStream);
        //                    }
        //                }
        //            });
        //        }
        #endregion

        #region API
        public Result Setup(dynamic setup)
        {
            Result result;
            mrEventSlim.Reset();
            manager.Setup(setup);
            mrEventSlim.Wait(waitForStatus);
            deviceLastStatus.TryGetValue(Convert.ToInt32(setup.SerialNumber.ToString()),out result);
            return result;
        }

        public Result Download(string serialNumber)
        {
            return manager.Download(serialNumber);
        }

        public Result Stop(string serialNumber)
        {
            return manager.Stop(Convert.ToInt32(serialNumber));
        }

        public Result Run(string serialNumber)
        {
            return manager.Run(Convert.ToInt32(serialNumber));
        }

        public Result ResetCalibration(string serialNumber)
        {
            return manager.ResetCalibration(serialNumber);
        }

        public Result UpdateFirmware(string serialNumber)
        {
            return manager.UpdateFirmware(serialNumber);
        }

        public Result Calibration(string serialNumber, int sensorIndex, List<Tuple<decimal, decimal>> pairs)
        {
            Result result;
            mrEventSlim.Reset();
            manager.Calibration(serialNumber, sensorIndex, pairs);
            mrEventSlim.Wait(waitForStatus);
            deviceLastStatus.TryGetValue(Convert.ToInt32(serialNumber), out result);
            return result;
            //return manager.Calibration(serialNumber, sensorIndex, pairs);
        }

        public Result MarkTimeStamp(string serialNumber)
        {
            return manager.MarkTimeStamp(serialNumber);
        }

        public Result RestoreCalibration(string serialNumber)
        {
            return manager.RestoreCalibration(serialNumber);
        }

        public Result SaveCalibration(string serialNumber)
        {
            return manager.SaveCalibration(serialNumber);
        }

        public void PushConnectedDevice(string serialNumber)
        {
            manager.PushConnectedDevice(serialNumber);
        }

        public Result EnterDeviceToCalibrationMode(dynamic setup)
        {
            return manager.EnterDeviceToCalibrationMode(setup);
        }

        public bool IsDeviceConnected(string serialNumber, bool sendDisconnected = true)
        {
            return manager.IsDeviceConnected(serialNumber, sendDisconnected);
        }

        public int ConnectedDevices(int userId)
        {
            return manager.ConnectedDevices(userId);
        }

        public ConcurrentDictionary<int, dynamic> GetConnectedDevices()
        {
            return manager.GetConnectedDevices();
        }

        public dynamic GetConnectedDevice(int serialNumber)
        {
            return manager.GetConnectedDevice(serialNumber);
        }

        //        public dynamic Status(string serialNumber)
        //        {
        //            return manager.Status(Convert.ToInt32(serialNumber));
        //        }

        //        public IEnumerable<GenericSensor> GetSensors(string serialNumber)
        //        {
        //            return manager.GetSensors(serialNumber);
        //        }
        //        public GenericDevice GetDevice(string serialNumber)
        //        {
        //            return manager.GetDevice(serialNumber);
        //        }
//        public bool IsDeviceRunning(string serialNumber)
//        {
//            return manager.IsDeviceRunning(serialNumber);
//        }
        // public GenericDevice GetDevice(DeviceAction deviceAction)
        // {
        // return wcfServerCommunication.GetDevice(deviceAction);

        // var formatter = new BinaryFormatter();
        //            try
        //            {

        //                formatter.Serialize(networkStream, deviceAction);
        //                var clientSource = formatter.Deserialize(networkStream);
        //                var type = clientSource.GetType();
        //                if ((type.Name).Contains("GenericDevice"))
        //                  return (GenericeDevice) clientSource;
        //            }
        //            catch (Exception ex)
        //            {
        //                Log.Instance.WriteError("Fail in GetDevice", this, ex);
        //            }
        //            return null;
        /* var data = new byte[4096];
        using (var ms = new MemoryStream())
        {
            var formatter = new BinaryFormatter();
            formatter.Serialize(ms, deviceAction);
            data = ms.ToArray();
            await networkStream.WriteAsync(data, 0, data.Length);
            await networkStream.FlushAsync();

            var numBytesRead = await networkStream.ReadAsync(data, 0, 1024);
            while(numBytesRead>0)
               numBytesRead= await networkStream.ReadAsync(data,0,data.Length);
                
               
        }*/
        // }
        #endregion
    }
}
