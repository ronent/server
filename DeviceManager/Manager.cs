﻿using Base.Devices;
using Base.Devices.Management.EventsAndExceptions;
using Base.OpCodes;
using Base.OpCodes.Helpers;
using Base.Sensors.Samples;
using Infrastructure;
using Infrastructure.MailService;
using MicroXBase.Devices.Types;
using Newtonsoft.Json;
using Server.Infrastructure;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using Base.Misc;
using Infrastructure.Entities;
using Sample = Server.Infrastructure.Sample;

//using Sample = Server.Infrastructure.Sample;

namespace DeviceLayer
{
    public class Manager
    {

        private ConcurrentDictionary<int, dynamic> _connectedDevices;
        private Listener Parent;

        private readonly int ROW_SIZE = 144;
        #region Constructor

        public Manager(Listener listener)
        {
            Parent = listener;
            Init();
        }

        #endregion
        #region internal methods

        internal void DeviceUpdate(ConnectionEventArgs<GenericDevice> e, DeviceManager.Statuses status)
        {
            Task.Factory.StartNew(() =>
            {
                //save device status in the database
                var isRunning = false;
                var logger = (GenericLogger) e.Device;
                if (logger != null)
                    isRunning = logger.Status.IsRunning;

                saveDeviceStatus(new DeviceUpsert
                {
                    Device = e.Device,
                    Status = status,
                    UserID = e.UserId,
                    IsRunning = isRunning,
                    SiteID = -1
                });
            });
            
            //send to gui the device
            PushGUIManager.SendDevice(Convert.ToInt32(e.Device.Status.SerialNumber),
                status == DeviceManager.Statuses.Connected
                    ? PushGUIManager.GUIEvents.deviceConnected
                    : PushGUIManager.GUIEvents.deviceRemoved);

            var serialNumber = int.Parse(e.Device.Status.SerialNumber);
            switch (status)
            {
                case DeviceManager.Statuses.Connected:
                    handleConnectedDevice(serialNumber, e);
                    break;
                case DeviceManager.Statuses.Disconnected:
                    removeConnectedDevice(serialNumber);
                    break;
            }

            PushGUIManager.SendOnlineStatus(e.Device.Status.SerialNumber, e.UserId);
        }

        internal void PushConnectedDevice(string serialNumber)
        {
//            var device = GetDeviceFromClient(Convert.ToInt32(serialNumber));
//            if (device != null)
            PushGUIManager.SendDevice(Convert.ToInt32(serialNumber), PushGUIManager.GUIEvents.deviceConnected);

        }

        internal dynamic GetConnectedDevice(int serialNumber)
        {
            dynamic device;
            return _connectedDevices.TryGetValue(serialNumber, out device) ? device : null;
        }

        internal ConcurrentDictionary<int, dynamic> GetConnectedDevices()
        {
            return _connectedDevices;
        }

        internal int ConnectedDevices(int userId)
        {
            var count = 0;
            int customerId;
            //get the customer of that user
            if (Common.User2Customer.TryGetValue(userId, out customerId))
            {
                //go over the connected devices serial numbers
                foreach (var serialNumber in _connectedDevices.Keys )
                {
                    //checks if has a customer for the serial number
                    int tempCustomer;
                    if (Common.SerialNumber2Customer.TryGetValue(serialNumber, out tempCustomer))
                    {
                        //if identical then count the connected devices
                        if (customerId == tempCustomer)
                            count++;
                    }
                }
            }
            return count;
        }

        internal void CalibrationSample(SampleAddedEventArgs e)
        {
            PushGUIManager.SendCalibrationSample(e);
        }

        internal void NewSample(SampleAddedEventArgs e)
        {
            //first save sample for device
            Task.Factory.StartNew(() => saveSample(e));
            //then send the sample to gui
            PushGUIManager.SendNewSample(e);
            //checks if the device was in timer run
            //then reset the timer run and the is running
            //if(DataManager.Instance.IsTimerRunReset(Convert.ToInt32(e.SerialNumber)))
            //{
            //    GenericDevice device = SuperDeviceManager.FindDevice(e.SerialNumber);
            //    if (device!=null)
            //}
        }

        internal void DownloadComplete(DownloadCompletedArgs args)
        {
            try
            {
                Log4Tech.Log.Instance.Write("Checks if device #{0} has data on one of the sensors", this, Log4Tech.Log.LogSeverity.DEBUG, args.SerialNumber);
                if (args.Logger.Sensors.HasData)
                {
                    Task.Factory.StartNew(() =>
                        {
                            Log4Tech.Log.Instance.Write("Save samples from device #{0} in database", this, Log4Tech.Log.LogSeverity.DEBUG, args.SerialNumber);
                            saveDownloadedSamples(args);
                        });
                    //get the device
                    //var device = SuperDeviceManager.GetDevice(args.SerialNumber);
                   // if (device is MicroXLogger)
                   // {
                    Task.Factory.StartNew(() =>
                     {
                         var report = new ReportsManager();
                         report.GenerateBoomerang(args.Logger);
                     });
                  //  }
                    //save device alarm data if needed
                    Log4Tech.Log.Instance.Write("Checks if device #{0} is not in normal state", this, Log4Tech.Log.LogSeverity.DEBUG, args.SerialNumber);

                    if ((int)args.Logger.Sensors.AlarmManager.Status > 0)
                    {
                        Log4Tech.Log.Instance.Write("Device #{0} is not in normal state, update alarm in database", this, Log4Tech.Log.LogSeverity.DEBUG, args.SerialNumber);
                        DataManager.Instance.UpdateDeviceAlarmData(Convert.ToInt32(args.SerialNumber), args.EnabledSensors);

                        var alarms =
                            DataManager.Instance.GetAlarmNotificationsBySerialNumber(Convert.ToInt32(args.SerialNumber));
                        PushGUIManager.SendAlarm(alarms);
                    }
                   
                }
                //else
                //{
                 //   Log4Tech.Log.Instance.Write("#{0} doen't have data on one of the sensors", this, Log4Tech.Log.LogSeverity.DEBUG, args.SerialNumber);
                PushGUIManager.SendDownloadCompleteStatus(Convert.ToInt32(args.SerialNumber));
               // }
            }
            catch(Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in DownloadComplete", this, ex);
            }
        }

        internal void DownloadStatus(ProgressReportEventArgs e)
        {
            //DOWNLOAD FINISHED 100% WILL BE IGNORED
            //DOWNLOAD COMPLETE EVENT WILL SEND THE LAST PRECENT OF 100%
            if(e.Percent<=100)
                PushGUIManager.SendDownloadStatus(e);

//            var client = Parent.GetConnectedClient(Convert.ToInt32(e.SerialNumber));
//
//            if (client != null)
//            {
//                Parent.SendEventAck(new DeviceAction(),client);
//            }
        }

        internal void Status(Tuple<GenericDevice, int, Result> args)
        {
            var isRunning = false;
            var logger = (GenericLogger)args.Item1;
            if (logger != null)
                isRunning = logger.Status.IsRunning;

            saveDeviceStatus(new DeviceUpsert
            {
                Device = args.Item1,
                Status = DeviceManager.Statuses.Connected,
                UserID = args.Item2,
                IsRunning = isRunning,
                SiteID = -1
            });
            //send to gui the device
            Log4Tech.Log.Instance.Write("Gonna push device to GUI", this);
            PushGUIManager.SendDevice(Convert.ToInt32(args.Item1.Status.SerialNumber), PushGUIManager.GUIEvents.deviceChanged);
        }

        internal void DeviceStatus(ConnectionEventArgs<GenericDevice> e)
        {
            var isRunning = false;
            var logger = (GenericLogger) e.Device;
            if (logger != null)
                isRunning = logger.Status.IsRunning;

            saveDeviceStatus(new DeviceUpsert()
            {
                Device = e.Device,
                Status = DeviceManager.Statuses.Connected,
                UserID = e.UserId,
                IsRunning = isRunning
            });
            //send to gui the device
            Log4Tech.Log.Instance.Write("Gonna push device to GUI", this);
            PushGUIManager.SendDevice(Convert.ToInt32(e.Device.Status.SerialNumber), PushGUIManager.GUIEvents.deviceChanged);
        }

        internal Result Setup(dynamic setup)
        {
            //first check if the object contain serial number
            if (setup != null && setup.SerialNumber != null)
            {
                var serialNumber = setup.SerialNumber.ToString();

                //checks if the logger activation is set correctly
                int timerStart;
                int.TryParse(setup.LoggerTimerStart.ToString(), out timerStart);
                if (timerStart > 0)
                    setup.LoggerActivation = 3;

                DateTime ts;
                int offset;
                Common.GetUtcOffset(Convert.ToInt32(setup.UserID), DateTime.UtcNow, out ts, out offset);
                //getting the UTC from user settings and set it on the Logger UTC
                setup.LoggerUTC = offset;

                dynamic setupTemp = JsonConvert.SerializeObject(setup);
                var deviceAction = new DeviceAction
                {
                    SerialNumber = Convert.ToInt32(serialNumber),
                    Action = eDeviceActions.SETUP,
                    Setup = setupTemp
                };
               
                Parent.SendMessageToClient(deviceAction);

                Task.Factory.StartNew(() =>
                {
                    var sensorsName = new List<Tuple<int, string>>();
                    //go over the sensors
                    foreach (var sensor in setup.Sensors)
                    {
                        //get the sensor name
                        if (sensor.SensorName != null)
                            sensorsName.Add(new Tuple<int, string>((int) sensor.TypeID, sensor.SensorName.ToString()));
                    }
                    saveDeviceStatus(Convert.ToInt32(serialNumber), DeviceManager.Statuses.Connected, sensorsName,
                        Convert.ToInt32(setup.SiteID), Convert.ToInt32(setup.AlarmDelay));
                });
//                }
            }
            return Result.OK;
        }

        internal Result Download(string serialNumber)
        {
            var deviceAction = new DeviceAction
            {
                SerialNumber = Convert.ToInt32(serialNumber),
                Action = eDeviceActions.DOWNLOAD
            };
             Parent.SendMessageToClient(deviceAction);
             return Result.OK;
        }

        internal Result Stop(int serialNumber)
        {
//            var result = new Result(eResult.ERROR);
//            var device = GetDeviceFromClient(Convert.ToInt32(serialNumber));

//            if (device == null) return result;

            var deviceAction = new DeviceAction
            {
                SerialNumber = serialNumber,
                Action = eDeviceActions.STOP
            };

            Parent.SendMessageToClient(deviceAction);
//            saveDeviceStatus(device, DeviceManager.Statuses.Connected);
//            saveDeviceStatus(new DeviceUpsert(){Device = device, Status = DeviceManager.Statuses.Connected, IsRunning = false});

//            return result;
            return Result.OK;

//            var microXLogger = SuperDeviceManager.GetDevice(serialNumber) as MicroXLogger;
//            if (microXLogger != null)
//            {
//                var result = microXLogger.Functions.Stop().Result;
//                if (result.IsOK)
//                {
//                    saveDeviceStatus(microXLogger, DeviceManager.Statuses.Stopped);
//                    addConnectedDevice(Convert.ToInt32(serialNumber));
//                }
//                return result;
//            }
//            return null;
        }

        internal Result Run(int serialNumber)
        {
            var deviceAction = new DeviceAction
            {
                SerialNumber = serialNumber,
                Action = eDeviceActions.RUN
            };

            Parent.SendMessageToClient(deviceAction);
            return Result.OK;
        }

        internal Result ResetCalibration(string serialNumber)
        {
//            var result = new Result(eResult.ERROR);
//            var device = GetDeviceFromClient(Convert.ToInt32(serialNumber));
//            if (device == null) return result;
            var deviceAction = new DeviceAction
            {
                SerialNumber = Convert.ToInt32(serialNumber),
                Action = eDeviceActions.RESETCALIBRATION
            };
                
             Parent.SendMessageToClient(deviceAction);
//            return result;
             return Result.OK;

//            var microXLogger = SuperDeviceManager.GetDevice(serialNumber) as MicroXLogger;
//            if (microXLogger != null)
//                return microXLogger.Functions.ResetCalibration().Result;
//
//            return new Result(eResult.ERROR);
        }

        internal Result UpdateFirmware(string serialNumber)
        {
//            var result = new Result(eResult.ERROR);
//            var device = GetDeviceFromClient(Convert.ToInt32(serialNumber));
//            if (device == null) return result;

            DataManager.Instance.UpdateDeviceFwFlag(Convert.ToInt32(serialNumber), 1);
            PushGUIManager.SendDevice(Convert.ToInt32(serialNumber), PushGUIManager.GUIEvents.deviceChanged);

            var deviceAction = new DeviceAction
            {
                SerialNumber = Convert.ToInt32(serialNumber),
                Action = eDeviceActions.UPLOADFIRMWARE
            };
                
             Parent.SendMessageToClient(deviceAction);
             return Result.OK;

        }

        internal Result Calibration(string serialNumber, int sensorIndex, List<Tuple<decimal, decimal>> pairs)
        {
//            var result = new Result(eResult.ERROR);
//            var device = GetDeviceFromClient(Convert.ToInt32(serialNumber));
            //var sensorManager = (SuperDeviceManager.GetDevice(serialNumber) as GenericLogger).Sensors;
//            var genericLogger = device as GenericLogger;
//            if (genericLogger != null)
//            {
//                var sensorManager = genericLogger.Sensors;
//                var sensor = sensorManager[(eSensorIndex) sensorIndex];
//                create dynamically the amount of pairs
//                var pairList = new List<LoggerReference>();
                //go over the list of pairs 
                //assuming first item is logger and second item is reference
//                var enu = pairs.GetEnumerator();

//                while (enu.MoveNext())
//                {
//                    if (enu.Current != null)
//                        pairList.Add(new LoggerReference()
//                        {
//                            Logger = enu.Current.Item1,
//                            Reference = enu.Current.Item2
//                        });
//                }
                //create the configuration for the calibration
//                var config = new CalibrationConfiguration(sensorManager as MicroXSensorManager);
                //generate the calibration with the pair list 
//                var coefficients = sensor.GenerateCalibration(pairList.ToArray());
                //add the calibration to the config before send
//                config.Add(sensor.Index, coefficients);
                //send the calibration
                //var microXLogger = SuperDeviceManager.GetDevice(serialNumber) as MicroXLogger;
//                var microXLogger = device as MicroXLogger;
//                if (microXLogger == null) return result;

                var deviceAction = new DeviceAction
                {
                    SerialNumber = Convert.ToInt32(serialNumber),
                    Action = eDeviceActions.SENDCALIBRATION,
                    CalibrationPairs = pairs,
                    CalibrationSensorIndex = sensorIndex
                };

                 Parent.SendMessageToClient(deviceAction);
//            }
//            return result;
            return Result.OK;
        }

        internal void FWStatus(ProgressReportEventArgs e)
        {
            PushGUIManager.SendFWStatus(e);
        }

        internal Result MarkTimeStamp(string serialNumber)
        {
//            var result = new Result(eResult.ERROR);
//            var device = GetDeviceFromClient(Convert.ToInt32(serialNumber));
//            if (!(device is PicoLiteLogger)) return result;

            var deviceAction = new DeviceAction
            {
                SerialNumber = Convert.ToInt32(serialNumber),
                Action = eDeviceActions.MARKTIMESTAMP,
            };

             Parent.SendMessageToClient(deviceAction);
//            if (SuperDeviceManager.GetDevice(serialNumber) is PicoLiteLogger)
//            {
//                var picoLiteLogger = SuperDeviceManager.GetDevice(serialNumber) as PicoLiteLogger;
//                if (picoLiteLogger != null)
//                    return picoLiteLogger.Functions.MarkTimeStamp().Result;
//            }
//            return result;
             return Result.OK;
        }

        internal Result RestoreCalibration(string serialNumber)
        {
//            var result = new Result(eResult.ERROR);
//            var device = GetDeviceFromClient(Convert.ToInt32(serialNumber));
//            var genericMicroLogLogger = device as GenericMicroLogLogger;
//            if (genericMicroLogLogger == null) return result;
            var deviceAction = new DeviceAction
            {
                SerialNumber = Convert.ToInt32(serialNumber),
                Action = eDeviceActions.RESTOREDEFAULTCALIBRATION,
            };

            Parent.SendMessageToClient(deviceAction);
            return Result.OK;
//            var genericMicroLogLogger = SuperDeviceManager.GetDevice(serialNumber) as GenericMicroLogLogger;
//            if (genericMicroLogLogger != null)
//                return genericMicroLogLogger.Functions.RestoreDefaultCalibration().Result;

            
        }

        internal Result SaveCalibration(string serialNumber)
        {
//            var result = new Result(eResult.ERROR);
//            var device = GetDeviceFromClient(Convert.ToInt32(serialNumber));
//            var genericMicroLogLogger = device as GenericMicroLogLogger;
//            if (genericMicroLogLogger == null) return result;

            var deviceAction = new DeviceAction
            {
                SerialNumber = Convert.ToInt32(serialNumber),
                Action = eDeviceActions.SAVEDEFAULTCALIBRATION,
            };
           
            Parent.SendMessageToClient(deviceAction);

            return Result.OK;

//            var genericMicroLogLogger = SuperDeviceManager.GetDevice(serialNumber) as GenericMicroLogLogger;
//            if (genericMicroLogLogger != null)
//                return genericMicroLogLogger.Functions.SaveDefaultCalibration().Result;

        }

        internal Result EnterDeviceToCalibrationMode(dynamic setup)
        {
            var result = new Result(eResult.ERROR);
            var serialNumber = setup.SerialNumber.ToString();
//            GenericDevice genericDevice = null;
//            try
//            {
//                //checks if device is connected
//                var deviceAction = new DeviceAction
//                {
//                    SerialNumber = Convert.ToInt32(serialNumber),
//                    Action = eDeviceActions.GETDEVICE
//                };
//                genericDevice = Parent.GetDevice(deviceAction);
//
//                if (genericDevice == null)
//                    return Result.ERROR;
//            }
//            catch (Exception ex)
//            {
//                Log4Tech.Log.Instance.WriteError("Device is not connected -> ", this, ex);
//                return result;
//            }
            //set the current interval to 10 second
            setup.SampleRateInSec = 10;
            //before sending setup, try to stop the device if is running
            tryStopDevice(Convert.ToInt32(serialNumber));
            //send setup for the device
            result = Setup(setup);
            if (result.IsOK)
            {
                //try to run the device
                result = Run(Convert.ToInt32(setup.SerialNumber.ToString()));
                if (result.IsOK)
                {
                    //after run the device, flag the calibration mode
                    //and save in database
//                    saveDeviceStatus(genericDevice, DeviceManager.Statuses.InCalibrationMode);
                   // saveDeviceStatus(new DeviceUpsert() { Device = genericDevice, Status = DeviceManager.Statuses.Connected, IsRunning = true, IsInCalibrationMode = true});
                    addConnectedDevice(Convert.ToInt32(serialNumber));
                }
            }
            return Result.OK;
        }

        internal bool IsDeviceConnected(string serialNumber, bool sendDisconnected=true)
        {
            var isConnected = false;
            try
            {
                
                dynamic tempDevice;
                if (_connectedDevices.TryGetValue(Convert.ToInt32(serialNumber), out tempDevice))
                    isConnected = true;
                if (sendDisconnected)
                    PushGUIManager.SendDevice(Convert.ToInt32(serialNumber), PushGUIManager.GUIEvents.deviceRemoved);
            }
            catch
            {
                // ignored
            }
            return isConnected;
        }

        internal void CheckDeviceAutoSetup(int serialNumber)
        {
            //checks if the device is set for auto setup
            var response = DataManager.Instance.IsDeviceAutoSetup(serialNumber);
            if (response.SetupName.ToString().Length > 0)
            {
                var keyPath = string.Format("{0}/{1}/{2}", response.CustomerID, response.DeviceType, response.SetupName);
                //get the json from S3
                var json = S3Manager.Instance.GetSetupFileContent(keyPath);
                //take all params from json
                var setup = json.Device;
                //send setup
                var setupResult = new Result(eResult.OK);
                //checks if needs to stop the logger before sending setup
                if (setup.LoggerActivation != null && setup.LoggerActivation.ToString() == "2") //stop
                    setupResult = Stop(serialNumber);

                if (setupResult.IsOK)
                {
                    //run setup for the logger
                    setupResult = Setup(setup);
                    Log4Tech.Log.Instance.Write("Setup resulted {0}({2}) on device #{1} ", this,
                        Log4Tech.Log.LogSeverity.DEBUG, setupResult.IsOK, serialNumber,
                        setupResult.IsOK
                            ? ""
                            : setupResult.LastException != null
                                ? setupResult.LastException.Message
                                : setupResult.MessageLog);
                }
            }
        }

        #endregion

        #region private methods

        private void Init()
        {
            _connectedDevices = new ConcurrentDictionary<int, dynamic>();
        }

        private void tryStopDevice(int serialNumber)
        {
//            try
//            {
//                var deviceStatus = Status(serialNumber);
//                if (deviceStatus != null && deviceStatus.Running)
                    Stop(Convert.ToInt32(serialNumber));
//            }
//            catch
//            {
//                 ignored
//            }
        }

//        private dynamic getMicroXDeviceStatus(MicroXLogger device)
//        {
//            //get from the status the device parameters
//            dynamic status = new ExpandoObject();
//            try
//            {
//                status.DeviceType = device.DeviceTypeName;
//                status.BatteryLevel = device.Status.BatteryLevel;
//                status.BoomerangEnabled = device.Status.BoomerangEnabled;
//                status.Name = device.Status.Comment;
//                status.CyclicMode = device.Status.CyclicMode;
//                status.CelsiusMode = !device.Status.FahrenheitMode;
//                status.FirmwareVersion = device.Status.FirmwareVersion.ToString();
//                status.SampleRate = device.Status.Interval.Seconds;
//                status.PushToRun = device.Status.PushToRunMode;
//                status.Running = device.Status.IsRunning;
//                status.SerialNumber = device.Status.SerialNumber;
//                status.TimerRunEnabled = device.Status.TimerRunEnabled;
//                status.TimerStart = device.Status.TimerRunEnabled && device.Status.TimerStart.Date.Year > 1970 && device.Status.TimerStart.Date.Year <= DateTime.Now.Year ? Convert.ToInt32(device.Status.TimerStart.Subtract(new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds) : 0;
//                //get the additional parameters from the mirox device
//                if (device is GenericMicroLogLogger)
//                {
//                    var micro = (GenericMicroLogLogger)device;
//
//                    status.MemorySize = (int)micro.Status.MemorySize;
//                    status.EnableLEDOnAlarm = micro.Status.EnableLEDOnAlarm;
//                    status.LCDConfiguration = (int)micro.Status.LCDConfiguration;
//                    status.ShowMinMax = micro.Status.ShowMinMax;
//                    status.ShowPast24HMinMax = micro.Status.ShowPast24HMinMax;
//                    status.StopOnKeyPress = micro.Status.StopOnKeyPress;
//                    status.AveragePoints = micro.Status.AveragePoints;
//                    //for microlite of external type 
//                    if (device is MicroLite2ELogger)
//                    {
//                        status.StopOnDisconnect = micro.Status.StopOnDisconnect;    
//                    }
//                    //for microlog pro 2
//                    if (device is MicroLog16BitLogger)
//                    {
//                        status.DeepSleepMode = micro.Status.DeepSleepMode;    
//                    }
//                }
//                else if (device is PicoLite.Devices.V2.PicoLiteLogger)
//                {
//                    PicoLite.Devices.V2.PicoLiteLogger picoliteV2 = (PicoLite.Devices.V2.PicoLiteLogger)device;
//                    status.RunDelay = picoliteV2.Status.RunDelay;
//                    status.RunTime = picoliteV2.Status.RunTime.Year > 1970 ? Convert.ToInt32(picoliteV2.Status.RunTime.Subtract(new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds) : 0;
//                    status.SetupTime = picoliteV2.Status.SetupTime.Year > 1970 ? Convert.ToInt32(picoliteV2.Status.SetupTime.Subtract(new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds) : 0;
//                }
//                //boomerang fields
//                status.BoomerangGeneratedBy = device.Status.Boomerang.Author;
//                status.BoomerangCelsiusMode = device.Status.Boomerang.CelsiusMode;
//                status.BoomerangComment = device.Status.Boomerang.Comment;
//                status.BoomerangShowAlarm = device.Status.Boomerang.DisplayAlarmLevels;
//                status.BoomerangContactList = device.Status.Boomerang.Contacts.ToArray();
//                //iterate through all the sensors of this device
//                IEnumerator<GenericSensor> sensors;
//                status.Sensors = new ArrayList();
//                //checks if the device is type of microlite 2 external 
//                if (device is MicroLite2ELogger)
//                {
//                    //then get only the enable sensor 
//                    sensors = device.Sensors.GetAllEnabled().GetEnumerator();
//                }
//                else
//                {
//                    //otherwise get all sensors
//                    sensors = device.Sensors.GetAll().GetEnumerator();
//                }
//                
//                //iterate the sensors
//                while (sensors.MoveNext())
//                {
//                    var sensor = sensors.Current;
//                    //get the sensor to add to the list
//                    dynamic dynSensor = getSensor(device, sensor);
//                    
//                    //add to the sensor list
//                    status.Sensors.Add(dynSensor);
//                    //checks if the sensor is user defined
//                    if (sensor.Type == eSensorType.UserDefined)
//                    {
//                        dynSensor = getUserDefinedSensor(sensor, dynSensor);
//                        //get the base sensor to add to the list in case of user defined
//                        //if the logger is microlite external
//                        if (device is MicroLite2ELogger)
//                        {
//                            var detachableSensor = device.Sensors.GetDetachableByType((eSensorType)Convert.ToInt32( dynSensor.BaseTypeID));
//                            dynamic externalSensor = getSensor(device, detachableSensor);
//                            status.Sensors.Add(externalSensor);
//                        }
//                        else
//                        {
//                            //else add all detachable sensors
//                            var detachableSensors = device.Sensors.GetAllDetachables().GetEnumerator();
//                            while (detachableSensors.MoveNext())
//                            {
//                                var detachableSensor = detachableSensors.Current;
//                                dynamic externalSensor = getSensor(device, detachableSensor);
//                                status.Sensors.Add(externalSensor);
//                            }
//                        }
//                    }
//                }
//                //set the flag of connected
//                status.Connected = true;
//                status.IsOK = true;
//            }
//            catch (Exception ex)
//            {
//                Log4Tech.Log.Instance.WriteError("Fail in getMicroXDeviceStatus", this, ex);
//                status.IsOK = false;
//            }
//            return status;
//        }

//        private dynamic getSensor(MicroXLogger device, GenericSensor sensor)
//        {
//            dynamic dynSensor = new System.Dynamic.ExpandoObject();
//            dynSensor.IsAlarmEnabled = sensor.Alarm.Enabled;
//            if (sensor.Alarm.Enabled)
//            {
//                dynSensor.AlarmLowValue = sensor.Alarm.Low;
//                dynSensor.AlarmHighValue = sensor.Alarm.High;
//            }
//            else
//            {
//                dynSensor.AlarmLowValue = 0;
//                dynSensor.AlarmHighValue = 0;
//            }
//
//            dynSensor.AlarmStatus = sensor.Alarm.Status.ToString();
//            dynSensor.Minimum = sensor.Minimum;
//            dynSensor.Maximum = sensor.Maximum;
//            dynSensor.Index = sensor.Index;
//            dynSensor.Enabled = sensor.Enabled;
//            dynSensor.Type = sensor.Type.ToString();
//            dynSensor.TypeID = (int)sensor.Type;
//            dynSensor.Name = sensor.Name;
//
//            dynSensor.IsExternal = getIsExternalSensor(device, sensor, dynSensor);
//            return dynSensor;
//        }

//        private bool getIsExternalSensor(MicroXLogger device, GenericSensor sensor, dynamic dynSensor)
//        {
//            //checks if the sensor is external sensor
//            bool IsExternal = false;
//            foreach (var item in device.Sensors.GetAllDetachables())
//            {
//                if (item.Type == sensor.Type)
//                {
//                    IsExternal = true;
//                    break;
//                }
//            }
//            return IsExternal;
//        }

//        private dynamic getUserDefinedSensor(GenericSensor sensor, dynamic dynSensor)
//        {
//            Log4Tech.Log.Instance.Write("Sensor is User Defined", this);
//            UserDefinedSensor userDefinedSensor = sensor as UserDefinedSensor;
//            Log4Tech.Log.Instance.Write("Type-{0},Unit-{1},Name-{2},{3}", this, Log4Tech.Log.LogSeverity.DEBUG, userDefinedSensor.Type, userDefinedSensor.Unit.Name, userDefinedSensor.Name, sensor);
//            dynSensor.Name = userDefinedSensor.Name;
//            var unit = userDefinedSensor.Unit as UserDefinedUnit;
//            
//            if (unit.Type == ExternalSensorUnitEnum.Custom)
//            {
//                dynSensor.Unit = unit.Name;
//            }
//            else
//            {
//                dynSensor.Unit = unit.Type.ToString();
//            }
//
//            dynSensor.BaseTypeID = (int)userDefinedSensor.BaseType;
//            dynSensor.BaseType = userDefinedSensor.BaseType.ToString();
//            dynSensor.DecimalDigits = userDefinedSensor.SignificantFigures;
//            
//            if (userDefinedSensor.UDS != null && userDefinedSensor.UDS.References != null &&
//                userDefinedSensor.UDS.References.Length > 0)
//            {
//                dynSensor.Log1 = userDefinedSensor.UDS.References[0].Logger;
//                dynSensor.Log2 = userDefinedSensor.UDS.References[1].Logger;
//                dynSensor.Ref1 = userDefinedSensor.UDS.References[0].Reference;
//                dynSensor.Ref2 = userDefinedSensor.UDS.References[1].Reference;
//            }
//            else
//            {
//                dynSensor.Log1 = 0;
//                dynSensor.Log2 = 0;
//                dynSensor.Ref1 = 0;
//                dynSensor.Ref2 = 0;
//            }
//            return dynSensor;
//        }

//        private Result sendMicroLogDeviceSetup(GenericDevice device, dynamic setup)
//        {
//            
//            try
//            {
//                if (device is EC850B16Logger)
//                {
//                    Log4Tech.Log.Instance.Write("Send setup to EC850B16Device",this);
//                    return sendEC850ESetup(device, setup);
//                }
//                else if (device is EC800B16Logger)
//                {
//                    Log4Tech.Log.Instance.Write("Send setup to EC800B16Device", this);
//                    return sendEC800ESetup(device, setup);
//                }
//            }
//            catch (Exception ex)
//            {
//                Log4Tech.Log.Instance.WriteError("Fail in sendMicroLogDeviceSetup", this, ex);
//            }
//            return new Result(eResult.ERROR);
//        }

//        private Result sendEC850ESetup(GenericDevice device,dynamic setup)
//        {
//            
//            EC850B16SetupConfiguration micrologSetup = new EC850B16SetupConfiguration();
//            try
//            {
//                micrologSetup.MemorySize = ((EC850B16Logger)device).Status.MemorySize;
//                micrologSetup.DeepSleepMode = setup.DeepSleepMode == "1" ? true : false;
//                micrologSetup = microliteSetupFields(setup, micrologSetup);
//
//                dynamic sensors = (dynamic)setup.Sensors;
//                foreach (var sensor in sensors)
//                {
//                    if (Convert.ToInt16(sensor.TypeID.ToString()) == (int)eSensorType.ExternalNTC ||
//                        Convert.ToInt16(sensor.TypeID.ToString()) == (int)eSensorType.PT100 ||
//                        Convert.ToInt16(sensor.TypeID.ToString()) == (int)eSensorType.UserDefined ||
//                        Convert.ToInt16(sensor.TypeID.ToString()) == (int)eSensorType.Voltage0_10V ||
//                        Convert.ToInt16(sensor.TypeID.ToString()) == (int)eSensorType.Current4_20mA)
//                    {
//                        if (sensor.IsActive == "1")
//                        {
//                            micrologSetup.ExternalSensor.Type = (eSensorType)sensor.TypeID;
//                            if ((eSensorType)sensor.TypeID == eSensorType.UserDefined)
//                            {
//                                MicroLogUDSConfiguration Configuration = new MicroLogUDSConfiguration();
//                                /*LoggerReference[] pairs = 
//                                { 
//                                    new LoggerReference { Logger = sensor.Log1, Reference = sensor.Ref1},
//                                    new LoggerReference { Logger = sensor.Log2, Reference = sensor.Ref2},
//                                };
//
//                                Configuration.Set(pairs);*/
//
//                                Configuration.BaseType = (eSensorType)sensor.BaseSensorType;
//                                Configuration.Name = sensor.SensorName.ToString();
//                                Configuration.SignificantFigures = Convert.ToByte(sensor.DecimalDigits.ToString());
//                                Configuration.Unit = getUnit(sensor.Unit.ToString());
//
//                                if (Configuration.Unit == ExternalSensorUnitEnum.Custom)
//                                {
//                                    Configuration.CustomUnit = sensor.Unit.ToString();
//                                }
//
//                                micrologSetup.ExternalSensor.UserDefinedSensor = Configuration;
//                            }
//                            if (sensor.IsAlarmEnabled.ToString()=="1")
//                            {
//                                micrologSetup.ExternalSensor.Alarm.Enabled = true;
//                                micrologSetup.ExternalSensor.Alarm.Low = Convert.ToDecimal(sensor.LowValue.ToString());
//                                micrologSetup.ExternalSensor.Alarm.High = Convert.ToDecimal(sensor.HighValue.ToString());
//                            }
//                        }
//                        else
//                        {
//                            micrologSetup.ExternalSensor.Type = eSensorType.None;
//                        }
//                    }
//                    else if (Convert.ToInt16(sensor.TypeID.ToString()) == (int)eSensorType.DewPoint)
//                    {
//                        micrologSetup.FixedSensors.DewPointEnabled = sensor.IsActive == "1";
//                        if (sensor.IsAlarmEnabled.ToString()=="1")
//                        {
//                            micrologSetup.FixedSensors.DewPointAlarm.Enabled = true;
//                            micrologSetup.FixedSensors.DewPointAlarm.Low = Convert.ToDecimal(sensor.LowValue.ToString());
//                            micrologSetup.FixedSensors.DewPointAlarm.High = Convert.ToDecimal(sensor.HighValue.ToString());
//                        }
//                    }
//                    else if (Convert.ToInt16(sensor.TypeID.ToString()) == (int)eSensorType.Humidity)
//                    {
//                        micrologSetup.FixedSensors.HumidityEnabled = sensor.IsActive == "1";
//                        if (sensor.IsAlarmEnabled.ToString()=="1")
//                        {
//                            micrologSetup.FixedSensors.HumidityAlarm.Enabled = true;
//                            micrologSetup.FixedSensors.HumidityAlarm.Low = Convert.ToDecimal(sensor.LowValue.ToString());
//                            micrologSetup.FixedSensors.HumidityAlarm.High = Convert.ToDecimal(sensor.HighValue.ToString());
//                        }
//                    }
//                    else if (Convert.ToInt16(sensor.TypeID.ToString()) == (int)eSensorType.DigitalTemperature)
//                    {
//                        micrologSetup.FixedSensors.TemperatureEnabled = true;
//                        if (sensor.IsAlarmEnabled.ToString()=="1")
//                        {
//                            micrologSetup.FixedSensors.TemperatureAlarm.Enabled = true;
//                            micrologSetup.FixedSensors.TemperatureAlarm.Low = Convert.ToDecimal(sensor.LowValue.ToString());
//                            micrologSetup.FixedSensors.TemperatureAlarm.High = Convert.ToDecimal(sensor.HighValue.ToString());
//                        }
//                    }
//                }
//                if (micrologSetup.BoomerangEnabled)
//                {
//                    Log4Tech.Log.Instance.Write("Boomerang setup for microlog EC850", this);
//                    micrologSetup = boomerangSetup(setup, micrologSetup);
//                }
//                Log4Tech.Log.Instance.Write("Finally, Send setup for microlog EC850", this);
//                var ec850B16Logger = SuperDeviceManager.GetDevice(setup.SerialNumber.ToString()) as EC850B16Logger;
//                if (ec850B16Logger != null)
//                    return ec850B16Logger.Functions.SendSetup(micrologSetup).Result;
//            }
//            catch (Exception ex)
//            {
//                Log4Tech.Log.Instance.WriteError("Fail in sendEC850ESetup", this, ex);
//            }
//            return new Result(eResult.ERROR);
//        }
//
//        private Result sendEC800ESetup(GenericDevice device, dynamic setup)
//        {
//            EC800B16SetupConfiguration micrologSetup = new EC800B16SetupConfiguration();
//            try
//            {
//                micrologSetup.MemorySize = ((EC800B16Logger)device).Status.MemorySize;
//                micrologSetup.DeepSleepMode = setup.DeepSleepMode == "1" ? true : false;
//                micrologSetup = microliteSetupFields(setup, micrologSetup);
//                bool sensorWasActivated = false;
//                dynamic sensors = (dynamic)setup.Sensors;
//                foreach (var sensor in sensors)
//                {
//                    if (Convert.ToInt16(sensor.TypeID.ToString()) == (int)eSensorType.ExternalNTC ||
//                        Convert.ToInt16(sensor.TypeID.ToString()) == (int)eSensorType.PT100 ||
//                        Convert.ToInt16(sensor.TypeID.ToString()) == (int)eSensorType.UserDefined ||
//                        Convert.ToInt16(sensor.TypeID.ToString()) == (int)eSensorType.Voltage0_10V ||
//                        Convert.ToInt16(sensor.TypeID.ToString()) == (int)eSensorType.Current4_20mA)
//                    {
//                        
//                        if (sensor.IsActive == "1")
//                        {
//                            Log4Tech.Log.Instance.Write("#{0} external sensor {1} is been activated", this, Log4Tech.Log.LogSeverity.DEBUG, setup.SerialNumber, sensor.TypeID);
//                            micrologSetup.ExternalSensor.Type = (eSensorType)sensor.TypeID;
//                            sensorWasActivated = true;   
//                            if ((eSensorType)sensor.TypeID == eSensorType.UserDefined)
//                            {
//                                MicroLogUDSConfiguration Configuration = new MicroLogUDSConfiguration();
//                              /*  LoggerReference[] pairs = 
//                                { 
//                                    new LoggerReference { Logger = Convert.ToDecimal(sensor.Log1.ToString()), Reference = Convert.ToDecimal(sensor.Ref1.ToString())},
//                                    new LoggerReference { Logger = Convert.ToDecimal(sensor.Log2.ToString()), Reference = Convert.ToDecimal(sensor.Ref2.ToString())},
//                                };
//
//                                Configuration.Set(pairs);*/
//
//                                Configuration.BaseType = (eSensorType)sensor.BaseSensorType;
//                                Configuration.Name = sensor.SensorName.ToString();
//                                Configuration.SignificantFigures = Convert.ToByte(sensor.DecimalDigits.ToString());
//                                Configuration.Unit = getUnit(sensor.Unit.ToString());
//
//                                if (Configuration.Unit == ExternalSensorUnitEnum.Custom)
//                                {
//                                    Configuration.CustomUnit = sensor.CustomUnit.ToString();
//                                }
//
//                                micrologSetup.ExternalSensor.UserDefinedSensor = Configuration;
//                            }
//                            if (sensor.IsAlarmEnabled.ToString()=="1")
//                            {
//                                Log4Tech.Log.Instance.Write("#{0} external sensor {1} setting an alarm {2} - {3}", this, Log4Tech.Log.LogSeverity.DEBUG, setup.SerialNumber, sensor.TypeID, sensor.LowValue, sensor.HighValue);
//                                micrologSetup.ExternalSensor.Alarm.Enabled = true;
//                                micrologSetup.ExternalSensor.Alarm.Low = Convert.ToDecimal(sensor.LowValue.ToString());
//                                micrologSetup.ExternalSensor.Alarm.High = Convert.ToDecimal(sensor.HighValue.ToString());
//                            }
//                        }
//                        else
//                        {
//                            if (!sensorWasActivated)
//                            {
//                                Log4Tech.Log.Instance.Write("#{0} external sensor {1} is been de-activated", this, Log4Tech.Log.LogSeverity.DEBUG, setup.SerialNumber, sensor.TypeID);
//                                micrologSetup.ExternalSensor.Type = eSensorType.None;
//                            }
//                        }
//                    }
//                    else if (Convert.ToInt16(sensor.TypeID.ToString()) == (int)eSensorType.InternalNTC)
//                    {
//                        micrologSetup.TemperatureSensor.TemperatureEnabled = true;
//                        if (sensor.IsAlarmEnabled.ToString()=="1")
//                        {
//                            micrologSetup.TemperatureSensor.TemperatureAlarm.Enabled = true;
//                            micrologSetup.TemperatureSensor.TemperatureAlarm.Low = Convert.ToDecimal(sensor.LowValue.ToString());
//                            micrologSetup.TemperatureSensor.TemperatureAlarm.High = Convert.ToDecimal(sensor.HighValue.ToString());
//                        }
//                    }
//                }
//                if (micrologSetup.BoomerangEnabled)
//                {
//                    Log4Tech.Log.Instance.Write("Boomerang setup for microlog EC800",this);
//                    micrologSetup = boomerangSetup(setup, micrologSetup);
//                }
//                Log4Tech.Log.Instance.Write("Finally, Send setup for microlog EC800", this);
//                var ec800B16Logger = SuperDeviceManager.GetDevice(setup.SerialNumber.ToString()) as EC800B16Logger;
//                if (ec800B16Logger != null)
//                    return ec800B16Logger.Functions.SendSetup(micrologSetup).Result;
//            }
//            catch (Exception ex)
//            {
//                Log4Tech.Log.Instance.WriteError("Fail in sendEC800ESetup", this, ex);
//            }
//            return new Result(eResult.ERROR);
//        }
//
//        private Result sendMicroLiteDeviceSetup(GenericDevice genericDevice,dynamic setup)
//        {
//            try
//            {
//                if (genericDevice is MicroLite2E.Devices.MicroLite2ELogger)
//                {
//                    Log4Tech.Log.Instance.Write("Send setup to MicroLite2EDevice", this);
//                    return sendMicroLite2ESetup(genericDevice, setup);
//                }
//                if (genericDevice is MicroLite2T.Devices.MicroLite2TLogger)
//                {
//                    Log4Tech.Log.Instance.Write("Send setup to MicroLite2TDevice", this);
//                    return sendMicroLite2TSetup(genericDevice, setup);
//                }
//                if (genericDevice is MicroLite2TH.Devices.MicroLite2THLogger)
//                {
//                    Log4Tech.Log.Instance.Write("Send setup to MicroLite2THDevice", this);
//                    return sendMicroLite2THSetup(genericDevice, setup);
//                }
//            }
//            catch (Exception ex)
//            {
//                Log4Tech.Log.Instance.WriteError("Fail in sendMicroLiteDeviceSetup", this, ex);
//            }
//            return new Result(eResult.ERROR);
//        }

//        private Result sendMicroLite2THSetup(GenericDevice device, dynamic setup)
//        {
//            MicroLite2THSetupConfiguration microliteSetup = new MicroLite2THSetupConfiguration();
//            try
//            {
//                microliteSetup.MemorySize = ((MicroLiteLogger)device).Status.MemorySize; 
//                microliteSetup = microliteSetupFields(setup, microliteSetup);
//                dynamic sensors = setup.Sensors;
//                Log4Tech.Log.Instance.Write("Iterate sensors of microlite 2TH", this);
//                foreach(var sensor in sensors)
//                {
//                    if (Convert.ToInt16(sensor.TypeID.ToString()) == (int)eSensorType.DewPoint)
//                    {
//                        microliteSetup.FixedSensors.DewPointEnabled = sensor.IsActive == "1";
//                        if (sensor.IsAlarmEnabled.ToString()=="1")
//                        {
//                            microliteSetup.FixedSensors.DewPointAlarm.Enabled = true;
//                            microliteSetup.FixedSensors.DewPointAlarm.Low = Convert.ToDecimal(sensor.LowValue.ToString());
//                            microliteSetup.FixedSensors.DewPointAlarm.High = Convert.ToDecimal(sensor.HighValue.ToString());
//                        }
//                    }
//                    else if (Convert.ToInt16(sensor.TypeID.ToString()) == (int)eSensorType.Humidity)
//                    {
//                        microliteSetup.FixedSensors.HumidityEnabled = sensor.IsActive == "1";
//
//                        if (sensor.IsAlarmEnabled.ToString()=="1")
//                        {
//                            microliteSetup.FixedSensors.HumidityAlarm.Enabled = true;
//                            microliteSetup.FixedSensors.HumidityAlarm.Low = Convert.ToDecimal(sensor.LowValue.ToString());
//                            microliteSetup.FixedSensors.HumidityAlarm.High = Convert.ToDecimal(sensor.HighValue.ToString());
//                        }
//                    }
//                    else if (Convert.ToInt16(sensor.TypeID.ToString()) == (int)eSensorType.DigitalTemperature)
//                    {
//                        microliteSetup.FixedSensors.TemperatureEnabled = true;
//                        if (sensor.IsAlarmEnabled.ToString()=="1")
//                        {
//                            microliteSetup.FixedSensors.TemperatureAlarm.Enabled = true;
//                            microliteSetup.FixedSensors.TemperatureAlarm.Low = Convert.ToDecimal(sensor.LowValue.ToString());
//                            microliteSetup.FixedSensors.TemperatureAlarm.High = Convert.ToDecimal(sensor.HighValue.ToString());
//                        }
//                    }
//                }
//
//                if (microliteSetup.BoomerangEnabled)
//                {
//                    Log4Tech.Log.Instance.Write("Boomerang setup for microlite 2TH", this);
//                    microliteSetup = boomerangSetup(setup, microliteSetup);
//                }
//                Log4Tech.Log.Instance.Write("Finally, Send setup for microlite 2TH", this);
//                var microLiteLogger = SuperDeviceManager.GetDevice(setup.SerialNumber.ToString()) as MicroLogAPI.Devices.MicroLiteLogger;
//                if (
//                    microLiteLogger !=
//                    null)
//                    return microLiteLogger.Functions.SendSetup(microliteSetup).Result;
//            }
//            catch (Exception ex)
//            {
//                Log4Tech.Log.Instance.WriteError("Fail in sendMicroLite2THSetup", this, ex);
//            }
//            return new Result(eResult.ERROR);
//        }
//
//        private Result sendMicroLite2TSetup(GenericDevice device, dynamic setup)
//        {
//            MicroLite2TSetupConfiguration microliteSetup = new MicroLite2TSetupConfiguration();
//            try
//            {
//                microliteSetup.MemorySize = ((MicroLiteLogger)device).Status.MemorySize; 
//                microliteSetup = microliteSetupFields(setup, microliteSetup);
//
//                Log4Tech.Log.Instance.Write("Iterate sensors of microlite 2T", this);
//
//                dynamic sensors = (dynamic)setup.Sensors;
//                foreach (var sensor in sensors)
//                {
//                    microliteSetup.TemperatureSensor.TemperatureAlarm.Enabled = sensor.IsAlarmEnabled.ToString() == "1";
//
//                    if (microliteSetup.TemperatureSensor.TemperatureAlarm.Enabled)
//                    {
//                        microliteSetup.TemperatureSensor.TemperatureAlarm.Low = Convert.ToDecimal(sensor.LowValue.ToString());
//                        microliteSetup.TemperatureSensor.TemperatureAlarm.High = Convert.ToDecimal(sensor.HighValue.ToString());
//                    }
//                }
//                if (microliteSetup.BoomerangEnabled)
//                {
//                    Log4Tech.Log.Instance.Write("Boomerang setup for microlite 2T", this);
//                    microliteSetup = boomerangSetup(setup, microliteSetup);
//                }
//                Log4Tech.Log.Instance.Write("Finally, Send setup for microlite 2T", this);
//                var microLiteLogger = SuperDeviceManager.GetDevice(setup.SerialNumber.ToString()) as MicroLiteLogger;
//                if (
//                    microLiteLogger !=
//                    null)
//                    return microLiteLogger.Functions.SendSetup(microliteSetup).Result;
//            }
//            catch(Exception ex)
//            {
//                Log4Tech.Log.Instance.WriteError("Fail in sendMicroLite2TSetup", this, ex);
//            }
//            return new Result(eResult.ERROR);
//        }
//
//        private Result sendMicroLite2ESetup(GenericDevice device, dynamic setup)
//        {
//            
//            try
//            {
//                var microliteSetup = new MicroLite2ESetupConfiguration
//                {
//                    MemorySize = ((MicroLiteLogger) device).Status.MemorySize
//                };
//
//                microliteSetup = microliteSetupFields(setup, microliteSetup);
//                microliteSetup.StopOnDisconnect = setup.StopOnCapRemoval.ToString() == "1" ? true : false;
//
//                dynamic sensors = setup.Sensors;
//                Log4Tech.Log.Instance.Write("Iterate sensors of microlite 2E", this);
//                foreach (var sensor in sensors)
//                {
//                    //set the type of the sensor
//                    if (sensor.IsActive.ToString() == "1")
//                    {
//                        Log4Tech.Log.Instance.Write("sensor {0} is enabled (id={1})", this, Log4Tech.Log.LogSeverity.DEBUG, sensor.SensorType, sensor.TypeID);
//                        microliteSetup.ExternalSensor.Type = (eSensorType)sensor.TypeID;
//                        if (Convert.ToInt16(sensor.TypeID.ToString()) == (int)eSensorType.UserDefined)
//                        {
//                            var configuration = new MicroLogUDSConfiguration
//                            {
//                                BaseType = (eSensorType) sensor.BaseSensorID,
//                                Name = sensor.SensorName.ToString(),
//                                SignificantFigures = Convert.ToByte(sensor.DecimalDigits.ToString()),
//                                Unit = getUnit(sensor.MeasurementUnit.ToString()),
//                                CustomUnit = sensor.MeasurementUnit.ToString(),
//                                Gain = 0,
//                                Offset = 0
//                            };
//
//                            var referencePoints = new LoggerReference[2];
//                            referencePoints[0].Logger = sensor.Calibration.Log1;
//                            referencePoints[0].Reference = sensor.Calibration.Ref1;
//                            referencePoints[1].Logger = sensor.Calibration.Log2;
//                            referencePoints[1].Reference = sensor.Calibration.Ref2;
//                            configuration.Set(referencePoints);
//
//
//                            microliteSetup.ExternalSensor.UserDefinedSensor = configuration;
//                        }
//                        //enable the external sensor if needed
//                        microliteSetup.ExternalSensor.Alarm.Enabled = sensor.IsAlarmEnabled.ToString() == "1";
//                        //set the alarm values if the alarm is enabled
//                        if (microliteSetup.ExternalSensor.Alarm.Enabled)
//                        {
//                            Log4Tech.Log.Instance.Write("sensor {0} has alarm , low-{1} high-{2}", this, Log4Tech.Log.LogSeverity.DEBUG, sensor.Type, sensor.AlarmLowValue, sensor.AlarmHighValue);
//                            microliteSetup.ExternalSensor.Alarm.Low = Convert.ToDecimal(sensor.LowValue.ToString());
//                            microliteSetup.ExternalSensor.Alarm.High = Convert.ToDecimal(sensor.HighValue.ToString());
//                        }
//                    }
//                }
//                //if boomerang enabled then set the boomerang fields:
//                if (microliteSetup.BoomerangEnabled)
//                {
//                    Log4Tech.Log.Instance.Write("Boomerang setup for microlite 2E", this);
//                    microliteSetup = boomerangSetup(setup, microliteSetup);
//                }
//                Log4Tech.Log.Instance.Write("Finally, Send setup for microlite 2E", this);
//                var microLiteLogger = SuperDeviceManager.GetDevice(setup.SerialNumber.ToString()) as MicroLogAPI.Devices.MicroLiteLogger;
//                if (
//                    microLiteLogger !=
//                    null)
//                    return microLiteLogger.Functions.SendSetup(microliteSetup).Result;
//            }
//            catch(Exception ex)
//            {
//                Log4Tech.Log.Instance.WriteError("Fail in sendMicroLite2ESetup", this, ex);
//            }
//            return new Result(eResult.ERROR);
//        }

//        private ExternalSensorUnitEnum getUnit(string Unit)
//        {
//            try
//            {
//                ExternalSensorUnitEnum enumParse = (ExternalSensorUnitEnum)Enum.Parse(typeof(ExternalSensorUnitEnum), Unit);
//
//                if (Enum.IsDefined(typeof(ExternalSensorUnitEnum), enumParse))
//                    return enumParse;
//            }
//            catch { }
//
//            return ExternalSensorUnitEnum.Custom;
//        }
//
//        private MicroLogSetupConfiguration microliteSetupFields(dynamic setup,MicroLogSetupConfiguration microliteSetup)
//        {
//            try
//            {
//                Log4Tech.Log.Instance.Write("Setup fields for microlite", this);
//
//                microliteSetup.SerialNumber = setup.SerialNumber.ToString();
//                microliteSetup.BoomerangEnabled = setup.BoomerangEnabled.ToString() == "1" ? true : false;
//                microliteSetup.Comment = setup.Name.ToString();
//                microliteSetup.CyclicMode = setup.CyclicMode.ToString() == "1" ? true : false;
//                microliteSetup.FahrenheitMode = !(setup.CelsiusMode.ToString() == "1" ? true : false);
//                microliteSetup.Interval = new TimeSpan(0, 0, Convert.ToInt32(setup.SampleRateInSec.ToString()));//Convert.ToUInt16(setup.SampleRate.ToString());
//                microliteSetup.AveragePoints = Convert.ToByte(setup.SampleAvgPoints.ToString());
//                microliteSetup.EnableLEDOnAlarm = setup.EnableLEDOnAlarm.ToString() == "1" ? true : false;
//                microliteSetup.LCDConfiguration = (LCDConfigurationEnum)setup.LEDConfig;
//                
//                
//                if (microliteSetup is MicroLite2ESetupConfiguration)
//                {
//                    (microliteSetup as MicroLite2ESetupConfiguration).StopOnDisconnect = setup.StopOnCapRemoval.ToString() == "1";
//                    microliteSetup = (MicroLite2ESetupConfiguration)microliteSetup;
//                }
//
//                if (setup.ShowMinMaxSamplesOnLCD != null && setup.ShowMinMaxSamplesOnLCD.ToString() == "2")
//                    microliteSetup.ShowMinMax = true;
//                else if (setup.ShowMinMaxSamplesOnLCD != null && setup.ShowMinMaxSamplesOnLCD.ToString() == "1")
//                    microliteSetup.ShowPast24HMinMax = true;
//                
//                int keyPressActivation =0;
//                if(setup.KeyPressActivation!=null)
//                    keyPressActivation = Convert.ToInt32(setup.KeyPressActivation.ToString());
//                else if (setup.MagnetActivation!=null)
//                    keyPressActivation = Convert.ToInt32(setup.MagnetActivation.ToString());
//
//                if (keyPressActivation == 1)
//                {
//                    microliteSetup.StopOnKeyPress = true;
//                    microliteSetup.PushToRunMode = true;
//                }
//                else if (keyPressActivation==2)
//                {
//                    microliteSetup.StopOnKeyPress = false;
//                    microliteSetup.PushToRunMode = true;
//                }
//                else if(keyPressActivation==3)
//                {
//                    microliteSetup.StopOnKeyPress = true;
//                    microliteSetup.PushToRunMode = false;
//                }
//                else
//                {
//                    microliteSetup.StopOnKeyPress = false;
//                    microliteSetup.PushToRunMode = false;
//                }
//                //microliteSetup.StopOnKeyPress = setup.StopOnKeyPress.ToString() == "1" ? true : false;
//                //microliteSetup.PushToRunMode = setup.PushToRun.ToString() == "1" ? true : false;
//
//                //if (setup.TimerRunEnabled.ToString() == "1")
//                if (setup.LoggerActivation!=null && setup.LoggerActivation==3)//timer run enabled
//                {
//                    if (setup.LoggerTimerStart != null)
//                    {
//                        Log4Tech.Log.Instance.Write("#{0} got Timer Start {1}", this, Log4Tech.Log.LogSeverity.DEBUG,microliteSetup.SerialNumber , setup.LoggerTimerStart);
//                        microliteSetup.TimerStart = getTimerStart(Convert.ToInt32(setup.LoggerTimerStart), Convert.ToInt32(setup.LoggerUTC));
//                        microliteSetup.TimerRunEnabled = true;
//                        Log4Tech.Log.Instance.Write("#{0} set Timer Start {1}", this, Log4Tech.Log.LogSeverity.DEBUG,microliteSetup.SerialNumber, microliteSetup.TimerStart);
//                    }
//                }
//                return microliteSetup;
//            }
//            catch (Exception ex)
//            {
//                Log4Tech.Log.Instance.WriteError("Fail in microliteSetupFields", this, ex);
//            }
//            return null;
//        }

        private DateTime getTimerStart(int loggerTimerStart, int loggerUTC)
        {
            var d = new DateTime(1970, 1, 1, 0, 0, 0);
            var seconds = loggerTimerStart + (loggerUTC * 60 * 60);
            return d.AddSeconds(seconds);
        }

//        private MicroXSetupConfiguration boomerangSetup(dynamic setup, MicroXSetupConfiguration microliteSetup)
//        {
//            var contactList = new List<Base.Misc.BasicContact>();
//            try
//            {
//                //iterate through contact list
//                //assume that the emails came in this format: a@a.com,b@b.com
//                string[] arr = setup.BoomerangContacts.ToString().Split(';');
//
//                foreach (var t in arr)
//                {
//                    Log4Tech.Log.Instance.Write("Add {0} to contact list for boomerang", this, Log4Tech.Log.LogSeverity.DEBUG, t);
//                    contactList.Add(new Base.Misc.BasicContact() { EMail = t });
//                }
//
//                string generatedBy = setup.BoomerangGeneratedBy.ToString();
//                if (generatedBy.Length > 0)
//                {
//                    if (generatedBy.Contains(@"\"))
//                    {
//                        var firstPart = generatedBy.Substring(0, generatedBy.IndexOf(@"\"));
//                        var secondPart = generatedBy.Substring(generatedBy.LastIndexOf(@"\") + 1);
//                        generatedBy = string.Format("{0}/{1}", firstPart, secondPart);
//                    }
//                }
//
//                microliteSetup.Boomerang.Author = generatedBy;
//                microliteSetup.Boomerang.CelsiusMode = setup.BoomerangCelsiusMode.ToString() == "1";
//                microliteSetup.Boomerang.Contacts = contactList;
//                //microliteSetup.Boomerang.DisplayAlarmLevels = setup.BoomerangDisplayAlarmLevels.ToString() == "1" ? true : false;
//                //microliteSetup.Boomerang.Comment = setup.BoomerangComment.ToString();
//                return microliteSetup;
//            }
//            catch(Exception ex)
//            {
//                Log4Tech.Log.Instance.WriteError("Fail in boomerangSetup", this,  ex);
//            }
//            return null;
//        }
//
//        private Result sendPicoLiteSetup(GenericDevice device, dynamic setup)
//        {
//            if (device is PicoLite.Devices.V2.PicoLiteLogger)
//                return sendPicoLiteV2Setup(setup);
//            else 
//                return sendPicoLiteV1Setup(setup);
//        }

//        private Result sendPicoLiteV2Setup(dynamic setup)
//        {
//            Log4Tech.Log.Instance.Write("Prepare PicoLite V2 setup", this);
//
//            var picoliteSetup = new PicoLite.DataStructures.V2.PicoLiteV2SetupConfiguration();
//
//            try
//            {
//                picoliteSetup.SerialNumber = setup.SerialNumber.ToString();
//                picoliteSetup.BoomerangEnabled = setup.BoomerangEnabled.ToString() == "1" ? true : false;
//                picoliteSetup.Comment = setup.Name.ToString();
//                picoliteSetup.CyclicMode = setup.CyclicMode.ToString() == "1" ? true : false;
//                picoliteSetup.FahrenheitMode = !(setup.CelsiusMode.ToString() == "1" ? true : false);
//                picoliteSetup.Interval = new TimeSpan(0, 0, Convert.ToUInt16(setup.SampleRateInSec.ToString()));
//                picoliteSetup.TimerRunEnabled = (setup.LoggerActivation != null && setup.LoggerActivation == 3) ? true : false;
//
//                if (picoliteSetup.TimerRunEnabled)
//                    picoliteSetup.TimerStart = getTimerStart(Convert.ToInt32(setup.LoggerTimerStart), Convert.ToInt32(setup.LoggerUTC));
//
//                var keyPressActivation = Convert.ToInt32(setup.KeyPressActivation.ToString());
//
//                if (keyPressActivation == 1)
//                {
//                    picoliteSetup.StopOnKeyPress = true;
//                    picoliteSetup.PushToRunMode = true;
//                }
//                else if (keyPressActivation == 2)
//                {
//                    picoliteSetup.StopOnKeyPress = false;
//                    picoliteSetup.PushToRunMode = true;
//                }
//                else if (keyPressActivation == 3)
//                {
//                    picoliteSetup.StopOnKeyPress = true;
//                    picoliteSetup.PushToRunMode = false;
//                }
//                else
//                {
//                    picoliteSetup.StopOnKeyPress = false;
//                    picoliteSetup.PushToRunMode = false;
//                }
//
//                if (setup.Sensors[0].IsAlarmEnabled != null)
//                {
//                    if (setup.Sensors[0].IsAlarmEnabled.ToString() == "1")
//                    {
//                        picoliteSetup.TemperatureSensor.TemperatureAlarm.Enabled = true;
//                        picoliteSetup.TemperatureSensor.TemperatureAlarm.Low = Convert.ToDecimal(setup.Sensors[0].LowValue.ToString());
//                        picoliteSetup.TemperatureSensor.TemperatureAlarm.High = Convert.ToDecimal(setup.Sensors[0].HighValue.ToString());
//                    }
//                }
//                picoliteSetup.RunDelay = Convert.ToUInt16(Convert.ToInt32(setup.RunDelay)/60);
//                //if boomerang enabled then set the boomerang fields:
//                if (picoliteSetup.BoomerangEnabled)
//                {
//                    List<Base.Misc.BasicContact> contactList = new List<Base.Misc.BasicContact>();
//                    //iterate through contact list
////                    //assume that the emails came in this format: a@a.com;b@b.com
////                    string[] arr = setup.BoomerangContacts.ToString().Split(";".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
////                    Log4Tech.Log.Instance.Write("array of contact length {0}", this, Log4Tech.Log.LogSeverity.DEBUG, arr.Length);
////                    if (arr.Length > 1)
////                    {
////                        for (int i = 0; i < arr.Length; i++)
////                            contactList.Add(new Base.Misc.BasicContact { EMail = arr[i] });
////                    }
////                    else
////                        contactList.Add(new Base.Misc.BasicContact { EMail = arr[0] });
//
//                    foreach (var boomerangContact in setup.BoomerangContacts)
//                    {
//                        contactList.Add(new Base.Misc.BasicContact { EMail = boomerangContact });
//                    }
//
//                    if (contactList.Count > 0)
//                    {
//                        picoliteSetup.Boomerang.Author = setup.BoomerangGeneratedBy.ToString();
//                        picoliteSetup.Boomerang.CelsiusMode = setup.BoomerangCelsiusMode.ToString() == "1";
//                        picoliteSetup.Boomerang.Contacts = contactList;
//                    }
//                    else
//                        picoliteSetup.BoomerangEnabled = false;
//                }
//                var picoLiteLogger = SuperDeviceManager.GetDevice(setup.SerialNumber.ToString()) as PicoLite.Devices.V2.PicoLiteLogger;
//                if (picoLiteLogger !=
//                    null)
//                    return picoLiteLogger.Functions.SendSetup(picoliteSetup).Result;
//            }
//            catch(Exception ex)
//            {
//                Log4Tech.Log.Instance.WriteError("Fail in sendPicoLiteV2Setup", this, ex);
//            }
//            return new Result(eResult.ERROR);
//        }
//
//        private Result sendPicoLiteV1Setup( dynamic setup )
//        {
//            Log4Tech.Log.Instance.Write("Prepare PicoLite V1 setup", this);
//
//            var picoliteSetup = new PicoLite.DataStructures.V1.PicoLiteV1SetupConfiguration();
//
//            try
//            {
//                picoliteSetup.SerialNumber = setup.SerialNumber.ToString();
//                picoliteSetup.BoomerangEnabled = setup.BoomerangEnabled.ToString() == "1";
//                picoliteSetup.Comment = setup.Name.ToString();
//                picoliteSetup.CyclicMode = setup.CyclicMode.ToString() == "1";
//                picoliteSetup.FahrenheitMode = !(setup.CelsiusMode.ToString() == "1" ? true : false);
//                picoliteSetup.Interval = new TimeSpan(0, 0, Convert.ToUInt16(setup.SampleRateInSec.ToString()));
//                picoliteSetup.TimerRunEnabled = (setup.LoggerActivation != null && setup.LoggerActivation == 3) ? true : false;
//
//                if (picoliteSetup.TimerRunEnabled)
//                    picoliteSetup.TimerStart = getTimerStart(Convert.ToInt32(setup.LoggerTimerStart), Convert.ToInt32(setup.LoggerUTC));
//                
//                //push to run needs to be underneath the key press activation
//                //in case of v1 of picolite the key press should have only push to run
//                picoliteSetup.PushToRunMode = setup.KeyPressActivation.ToString() == "2" ? true : false;
//
//                if (setup.Sensors[0].IsAlarmEnabled != null)
//                {
//                    if (setup.Sensors[0].IsAlarmEnabled.ToString() == "1")
//                    {
//                        picoliteSetup.TemperatureSensor.TemperatureAlarm.Enabled = true;
//                        picoliteSetup.TemperatureSensor.TemperatureAlarm.Low = Convert.ToDecimal(setup.Sensors[0].LowValue.ToString());
//                        picoliteSetup.TemperatureSensor.TemperatureAlarm.High = Convert.ToDecimal(setup.Sensors[0].HighValue.ToString());
//                    }
//                }
//
//                //if boomerang enabled then set the boomerang fields:
//                if (picoliteSetup.BoomerangEnabled)
//                {
//                    List<Base.Misc.BasicContact> contactList = new List<Base.Misc.BasicContact>();
//                    //iterate through contact list
//                    //assume that the emails came in this format: a@a.com;b@b.com
//                    string[] arr = setup.BoomerangContacts.ToString().Split(";".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
//                    Log4Tech.Log.Instance.Write("array of contact length {0}", this, Log4Tech.Log.LogSeverity.DEBUG, arr.Length);
//                    if (arr.Length > 1)
//                    {
//                        for (int i = 0; i < arr.Length; i++)
//                            contactList.Add(new Base.Misc.BasicContact() { EMail = arr[i] });
//                    }
//                    else
//                        contactList.Add(new Base.Misc.BasicContact() { EMail = arr[0] });
//
//                    picoliteSetup.Boomerang.Author = setup.BoomerangGeneratedBy.ToString();
//                    picoliteSetup.Boomerang.CelsiusMode = setup.BoomerangCelsiusMode.ToString() == "1" ? true : false;
//                    picoliteSetup.Boomerang.Contacts = contactList;
//                }
//                var picoLiteLogger = SuperDeviceManager.GetDevice(setup.SerialNumber.ToString()) as PicoLite.Devices.V1.PicoLiteLogger;
//                if (picoLiteLogger !=
//                    null)
//                    return picoLiteLogger.Functions.SendSetup(picoliteSetup).Result;
//            }
//            catch(Exception ex)
//            {
//                Log4Tech.Log.Instance.WriteError("Fail in sendPicoLiteV1Setup", this, ex);
//            }
//            return new Result(eResult.ERROR);
//        }

        /// <summary>
        /// assuming those parameters are passed:
        /// deviceSensorID,value,sampleTime,isTimeStamp,timestampComment,alarmStatus,isDummy
        /// </summary>
        /// <param name="samples"></param>
        /// <returns></returns>
        private bool InsertBatch(List<Sample> samples)
        {
            if (samples != null && samples.Count > 0)
            {
                try
                {
                    //get the first sample deviceSensorID
                    var serialNumber = samples[0].SerialNumber;
                    //send to sample storage (mongodb) database as batch
//                    Log4Tech.Log.Instance.Write("Check if can save batch of {1} samples for #{0}", this, Log4Tech.Log.LogSeverity.DEBUG, serialNumber, samples.Count);
                    if (canSaveStorage(serialNumber, samples.Count))
                    {
                        var device = Common.DeviceApiManager.GetConnectedDevice(serialNumber);
                        var timeZone=0;
                        if (device != null)
                            timeZone = Convert.ToInt32(device.Device.LoggerUTC.ToString());
//                        int timeZone = DataManager.Instance.GetDeviceTimeZone(serialNumber);
                        if (timeZone == 0) return SampleManager.Instance.InsertBatch(samples);

                        var delta = timeZone * 3600;
                        foreach (var t in samples)
                            t.SampleTime = t.SampleTime - delta;
                        
                        return SampleManager.Instance.InsertBatch(samples);
                    }
                }
                catch(Exception ex)
                {
                    Log4Tech.Log.Instance.WriteError("Fail in InsertBatch", this, ex);
                }
            }
            return false;
        }

        /// <summary>
        /// Before insert , checks how much storage the customer has used
        /// then checks if according to definition of storage the data will be saved
        /// </summary>
        /// <param name="deviceSensorID"></param>
        /// <returns></returns>
        private bool canSaveStorage(int serialNumber, int rowsCountForInsert)
        {
            int customerId;
            if (!Common.SerialNumber2Customer.TryGetValue(serialNumber, out customerId))
            {
                customerId = DataManager.Instance.GetCustomerID(serialNumber);
                Common.SerialNumber2Customer.TryAdd(serialNumber, customerId);
            }
            int dataStorageExpiry;
            if (!Common.DataStorageExpiry.TryGetValue(customerId, out dataStorageExpiry))
            {
                var response = DataManager.Instance.GetCustomerStorage(serialNumber);
                dataStorageExpiry = Convert.ToInt32(response.DataStorageExpiry);
                Common.DataStorageExpiry.TryAdd(customerId, dataStorageExpiry);
            }
            //checks first if the storage expired
            if (dataStorageExpiry > DateTime.Now.Subtract(new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds)
            {
                //if not then get the max allowed storage
                //The storage is amount of samples allowed for store in MongoDB
                long maxStorageAllowed ;
                if (!Common.MaxStorageAllowed.TryGetValue(customerId, out maxStorageAllowed))
                {
                    maxStorageAllowed = DataManager.Instance.GetCustomerStorage(serialNumber).MaxStorageAllowed;
                    Common.MaxStorageAllowed.TryAdd(customerId, maxStorageAllowed);
                }
                //get the data storage saved so far at mongo db:
                // (The storage is based on amount of rows related to device sensors , multiple by row storage size)
                // get the device sensor list and execute each one for getting the rows count
                var mongoStorage = SampleManager.Instance.RowsCount(customerId);
                //calculate the storage
                Log4Tech.Log.Instance.Write("Samples count on MongoDB - {0}", this,
                    Log4Tech.Log.LogSeverity.DEBUG, mongoStorage);
                //if the storage in mongo is more than the row size storage then allow it
                if (mongoStorage < maxStorageAllowed && mongoStorage > 0)
                {
                    //now checks if the amount of rows that gonna insert is less than the max allowed
                    var samplesStorage = rowsCountForInsert; //* ROW_SIZE;
                    Log4Tech.Log.Instance.Write("Calculated storage to be inserted - {0} + {1} = {2}", this,
                        Log4Tech.Log.LogSeverity.DEBUG, samplesStorage, mongoStorage,
                        samplesStorage + mongoStorage);
                    if (mongoStorage + samplesStorage < maxStorageAllowed)
                    {
                        //if went this far then ok...
                        return true;
                    }
                    Log4Tech.Log.Instance.Write(
                        "Amount of samples to be inserted is exceeded maximum allowed - {0}", this,
                        Log4Tech.Log.LogSeverity.DEBUG, samplesStorage);
                }
                else if (mongoStorage == 0)
                    return true;
            }
            else
            {
                //data storage expired
                //need to disable account (customer)
                var customerDisableStatus = DataManager.Instance.GetCustomerDisableStatus(serialNumber);
                if (customerDisableStatus.Result != "OK") return false;

                if (Convert.ToInt32(customerDisableStatus.EmailWasSentToday) == 0)
                {
                    DataManager.Instance.UpdateCustomerEmailSent(serialNumber);

                    var mail = new MailMessage()
                    {
                        From = new MailAddress("ronent@fourtec.com", "Fourtec support"),
                        Body =
                            "Hi, {0} <br><br> Your subscription for our cloud solution has expired. <br> Please login to <a href='http://www.fourtec.com'>Fourtec</a> to extend your subscription.",
                        Subject = "Cloud Subscription has expired",
                        IsBodyHtml = true,
                    };
                    mail.To.Add(new MailAddress(customerDisableStatus.Email, customerDisableStatus.Name));
                    //send email of subscription expired
                    MailManager.SendEmail(mail);
                }

                //if pass 3 months , then backup the data at mongo db
                DateTime dtDisableDate = Convert.ToDateTime(customerDisableStatus.DisableDate);
                if (DateTime.Now.Subtract(dtDisableDate).TotalDays >= 90 &&
                    customerDisableStatus.MongoDataBackUp == 0) //no backup is done yet
                {
                    var response = DataManager.Instance.GetAllCustomerSerialNumbers(serialNumber);
                    if (response.IsOK)
                        if (SampleManager.Instance.Backup((List<int>) response.SerialNumbers))
                            DataManager.Instance.UpdateMongoDBBackup(serialNumber);
                }
                //if pass 6 months , then delete the backup
                if (DateTime.Now.Subtract(dtDisableDate).TotalDays >= 180 &&
                    customerDisableStatus.MongoDataBackUp == 1) //backup is made after 90 days and pass 180 days
                {
                    //delete the data
                }
            }
            return false;
        }

        private bool InsertSample(Sample sample)
        {
            try
            {
                var device = Common.DeviceApiManager.GetConnectedDevice(sample.SerialNumber);
                var timeZone = 0;
                if (device != null)
                    timeZone = Convert.ToInt32(device.Device.LoggerUTC.ToString());

//            int timeZone = DataManager.Instance.GetDeviceTimeZone(sample.SerialNumber);
                if (timeZone != 0)
                    sample.SampleTime = sample.SampleTime - timeZone*3600;

                if (canSaveStorage(sample.SerialNumber, 1))
                    return SampleManager.Instance.InsertSample(sample);
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in InsertSample", this, ex);
            }
            return false;
        }

        private void InsertLastDownloadTime(int serialNumber,int sensorTypeID , int firstDowloadtime, int lastDownloadTime)
        {
            DataManager.Instance.InsertSensorDownloadHistory(serialNumber,sensorTypeID,  firstDowloadtime, lastDownloadTime);
        }

        private void saveSample(SampleAddedEventArgs e)
        {
            try
            {
                //get the customerid by serial number
                int customerId;
                var serialNumber = Convert.ToInt32(e.SerialNumber);
                Common.SerialNumber2Customer.TryGetValue(serialNumber, out customerId);
                if (InsertSample(new Sample
                {
                    SerialNumber = Convert.ToInt32(e.SerialNumber),
                    SensorTypeID = (int) e.Type,
                    Value = e.Sample.IsDummy ? 0 : Convert.ToDouble(e.Sample.Value),
                    SampleTime = Convert.ToInt32(e.Sample.Date.Subtract(new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds),
                    IsTimeStamp = e.Sample.IsTimeStamp,
                    TimeStampComment = e.Sample.IsTimeStamp ? (e.Sample as TimeStamp).Comment : "",
                    AlarmStatus = (Int16) e.Sample.AlarmStatus,
                    IsDummy = Convert.ToInt16(e.Sample.IsDummy),
                    CustomerID = customerId
                }))
                    PushGUIManager.SendStorageStatus(serialNumber);
                else
                    Log4Tech.Log.Instance.Write("Sample for #{0} wasn't saved in MongoDB", this,
                        Log4Tech.Log.LogSeverity.DEBUG, e.SerialNumber);

                //save last sample value in database
                DataManager.Instance.UpdateSensorLastSample(Convert.ToInt32(e.SerialNumber),
                    (int) e.Type,
                    Convert.ToInt32(e.Sample.Date.Subtract(new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds),
                    e.Sample.IsDummy ? 0 : Convert.ToDouble(e.Sample.Value));
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in saveSample", this, ex);
            }
        }

        private void saveDownloadedSamples(DownloadCompletedArgs args)
        {
            //var serial_number = int.Parse(args.SerialNumber);
            //var last_stored_sample_time = DataManager.Instance.GetLastStoredSampleTime(serial_number);

            try
            {
                int customerId;
                Common.SerialNumber2Customer.TryGetValue(Convert.ToInt32(args.SerialNumber), out customerId);
                var sensor = args.EnabledSensors.GetEnumerator();
                Log4Tech.Log.Instance.Write("Start save #{0} samples for {1} sensors", this,
                    Log4Tech.Log.LogSeverity.DEBUG, args.EnabledSensors.ToList()[0].Samples.Count(),
                    args.EnabledSensors.Count());
                while (sensor.MoveNext())
                {
                    Log4Tech.Log.Instance.Write("Data for Sesnor {0} with {1} samples will be saved", this,
                        Log4Tech.Log.LogSeverity.DEBUG, sensor.Current.Type, sensor.Current.Samples.Count);
                    var sensorSamples = sensor.Current.Samples.OrderBy(p => p.Date).GetEnumerator();
                    byte downloadMark = 1;
                    //first get the device sensor id 
                    //var deviceSensorID = getDeviceSensorID(Convert.ToInt32(args.SerialNumber), (int)sensor.Current.Type);

                    int firstDownloadTime = 0, lastDownloadTime = 0, sampleTime = 0;
                    double sampleValue = 0;
                    var samples = new List<Sample>();
                    //iterate through the sensor samples
                    while (sensorSamples.MoveNext())
                    {
                        var sample = sensorSamples.Current;
                        sampleTime =
                            Convert.ToInt32(sample.Date.Subtract(new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds);
                        var comment = sample.IsTimeStamp ? (sample as TimeStamp).Comment : "";
                        var value = sample.IsDummy ? 0 : Convert.ToDouble(sample.Value);

                        sampleValue = value;

                        //insert the sample
                        //InsertSample(deviceSensorID, value, sampleTime, sample.Current.IsTimeStamp, comment
                        //             , (Int16)sample.Current.AlarmStatus, sample.Current.IsDummy ? 1 : 0);
                        samples.Add(new Sample()
                        {
                            SerialNumber = Convert.ToInt32(args.SerialNumber),
                            SensorTypeID = (int) sensor.Current.Type,
                            Value = value,
                            SampleTime = sampleTime,
                            IsTimeStamp = sample.IsTimeStamp,
                            TimeStampComment = comment,
                            AlarmStatus = (Int16) sample.AlarmStatus,
                            IsDummy = Convert.ToInt16(sample.IsDummy ? 1 : 0),
                            CustomerID = customerId
                        });

                        if (downloadMark == 1)
                        {
                            firstDownloadTime = sampleTime;
                            downloadMark = 0;
                        }
                    }

                    if (samples.Count > 0)
                    {
                        Log4Tech.Log.Instance.Write("Sending to MongoDB {0} samples", this,
                            Log4Tech.Log.LogSeverity.DEBUG, samples.Count);
                        if (InsertBatch(samples))
                        {
                            Log4Tech.Log.Instance.Write("Batch insert to MongoDB was done OK", this);
                            lastDownloadTime = sampleTime;
                            //save the last download time
                            if (lastDownloadTime > 0 && firstDownloadTime > 0)
                                InsertLastDownloadTime(Convert.ToInt32(args.SerialNumber), (int) sensor.Current.Type,
                                    firstDownloadTime, lastDownloadTime);

                            Log4Tech.Log.Instance.Write("Done saving for Sesnor {0} with {1} samples", this,
                                Log4Tech.Log.LogSeverity.DEBUG, sensor.Current.Type, sensor.Current.Samples.Count);

                            var batch_name = PushGUIManager.GUIEvents.batchAdded.ToString();
                            var sample_list = SampleManager.Instance.Find(Convert.ToInt32(args.SerialNumber),
                                (int) sensor.Current.Type);

                            PushGUIManager.SendBatch(batch_name, Convert.ToInt32(args.SerialNumber), (int)sensor.Current.Type,
                                sample_list.ToArray(), sensor.Current.Unit.Name);
                            PushGUIManager.SendStorageStatus(Convert.ToInt32(args.SerialNumber));
                        }

                        //save last sample value in database
                        DataManager.Instance.UpdateSensorLastSample(Convert.ToInt32(args.SerialNumber),
                            (int) sensor.Current.Type, sampleTime, sampleValue);
                    }
                }
                Log4Tech.Log.Instance.Write("Finished save all samples for all sensors", this);
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in saveDownloadedSamples", this, ex);
            }
        }

        private void saveDeviceStatus(int serialNumber, DeviceManager.Statuses status,
            List<Tuple<int, string>> sensorsName, int siteID, int alarmDelay)
        {
            try
            {
                DataManager.Instance.UpdateDeviceStatus(serialNumber, (int) status, siteID, 1, alarmDelay);
                
                foreach (var sensor in sensorsName)
                    DataManager.Instance.UpdateDeviceSensorName(serialNumber, sensor.Item2, sensor.Item1);

            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in saveDeviceStatus", this, ex);
            }
        }

        private void saveDeviceStatus(DeviceUpsert deviceUpsert)
        {
            try
            {
                //checks if the device is type of MicroXDevice
                var logger = deviceUpsert.Device as MicroXLogger;
                if (logger == null) return;

                Log4Tech.Log.Instance.Write("Save device #{0} into database", this, Log4Tech.Log.LogSeverity.DEBUG,
                    logger.Status.SerialNumber);
                //save the device in the database
                DataManager.Instance.UpsertDevice(deviceUpsert);
                //send the updated device to the GUI
                PushGUIManager.SendDevice(Convert.ToInt32(deviceUpsert.Device.Status.SerialNumber), PushGUIManager.GUIEvents.deviceChanged);
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in saveDeviceStatus", this, ex);
            }
        }

        private void addConnectedDevice(int serialNumber)
        {
            //save the connected device locally
            var device = DataManager.Instance.GetDevice(serialNumber);
            dynamic existsDevice;
            if(_connectedDevices.TryGetValue(serialNumber,out existsDevice))
                removeConnectedDevice(serialNumber);
            _connectedDevices.TryAdd(serialNumber, device);
        }

        private void removeConnectedDevice(int serialNumber)
        {
            //if disconnected then remove from the list
            dynamic device;
            _connectedDevices.TryRemove(serialNumber, out device);
        }

        private void handleConnectedDevice(int serialNumber, ConnectionEventArgs<GenericDevice> e)
        {
            addConnectedDevice(serialNumber);

            //                // Get the last sample time for the connected device!!!
            //                var dt = SampleManager.Instance.LastSampleValue(serial_number,e.Device.Status.c);
            //
            //                // Save the datetime to database
            //                DataManager.Instance.SetLastStoredSampleTime(serial_number, dt);

            if (!(e.Device is MicroXLogger)) return;

            Download(e.Device.Status.SerialNumber);

        }

        #endregion

       
    }
}
