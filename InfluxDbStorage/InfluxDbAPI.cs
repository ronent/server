﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Server.Infrastructure;

namespace InfluxDbStorage
{
    public class InfluxDbAPI
    {
        private const string DbName = "simio";
        private const string LoggersTable = "loggers";
        private const string ConnectionString = "http://10.10.10.58:8086";
        private readonly string _insertUrl;
        private readonly string _readUrl;

        public InfluxDbAPI()
        {
            _insertUrl = string.Format("{0}/write?db={1}&precision=s", ConnectionString, DbName);
            _readUrl = string.Format("{0}/query?db={1}&epoch=s&q=", ConnectionString, DbName);
        }

        #region Methods

        public async Task<List<dynamic>> Find(int serialNumber, int sensorTypeId)
        {
            using (var client = new HttpClient())
            {
                var sql = string.Format("select * from {0} where \"serialNumber\"='{1}' and \"sensorTypeId\"='{2}' ", serialNumber,serialNumber,sensorTypeId);
                var result = await client.GetAsync((_readUrl + sql));
                var responseString = result.Content.ReadAsStringAsync().Result;

                var response = JsonConvert.DeserializeObject<QueryResult>(responseString);
                return response == null ? null : convertToSamples(response, serialNumber, sensorTypeId);
            }
        }

        public bool InsertBatch(List<SensorSample> samples)
        {
            var builder = new StringBuilder();
            foreach (var sample in samples)
            {
                builder.Append(getInsertStatement(sample));
                builder.Append("\n");
            }
            var data = builder.ToString();
            return writeData(data).Result;
        }

        public bool InsertSample(SensorSample sample)
        {
            var data = getInsertStatement(sample);
            return writeData(data).Result;
        }

        #endregion

        #region private functions

        private async Task<bool> writeData(string data)
        {
            using (var client = new HttpClient())
            {
                var response = await client.PostAsync(_insertUrl, new StringContent(data));
                var responseString = response.Content.ReadAsStringAsync();
                return responseString.Result == string.Empty;
            }
        }

        private List<dynamic> convertToSamples(QueryResult result,int serialNumber, int sensorTypeId)
        {
            var points = new List<double>[result.results[0].series[0].values.Count];
            result.results[0].series[0].values.CopyTo(points);
            var xxx = points.OrderByDescending(x => x[0]).Take(150);
            var enumerable = xxx as List<double>[] ?? xxx.ToArray();
            var samples = new List<dynamic>();
            for (var i = 0; i < enumerable.Count(); i++)
            {
                dynamic sample = new ExpandoObject();
                sample.SerialNumber = serialNumber;
                sample.SensorTypeID = sensorTypeId;
                sample.Value = enumerable[i][1];
                sample.SampleTime = Convert.ToInt64(enumerable[i][0]);
                sample.IsTimeStamp = result.results[0].series[0].tags.isTimeStamp;
                sample.AlarmStatus = result.results[0].series[0].tags.alarmStatus;
                sample.IsDummy = result.results[0].series[0].tags.isDummy;
                sample.MeasurementUnit = "C";
                samples.Add(sample);
            }
            return samples;
        }

        private string getInsertStatement(SensorSample sample)
        {
            return string.Format(
                "{0},sensorTypeId={4},isTimeStamp={5},timeStampComment='{6}',alarmStatus={7},isDummy={8} {2}={3} {1}",
                sample.SerialNumber, sample.SampleTime,sample.SensorName, sample.Value, sample.SensorTypeID,
                sample.IsTimeStamp, sample.TimeStampComment, sample.AlarmStatus, sample.IsDummy);
        }

        #endregion

        //        private async Task<string> getResponse(string action)
//        {
//            var urlPost = string.Format("{0}/{1}?db={2}&precision=s", ConnectionString, action, DbName);
//
//            using (var client = new HttpClient())
//            {
//
////                for (var i = 0; i < 1000000; i++)
////                {
////                    var rand = new Random();
////                    var temp = 23.0 + rand.NextDouble();
//                    var data =
//                        string.Format(
//                            "{0},serialNumber=913742,sensorTypeId=1,isTimeStamp=0,timeStampComment='',alarmStatus=0,isDummy=0 value={2} {1}",
//                            LoggersTable,
//                            Convert.ToInt32(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds), 15);
//
//                    var response = await client.PostAsync(urlPost, new StringContent(data));
////                }
//                var responseString = response.Content.ReadAsStringAsync();
//                return responseString.Result;
//            }
//        }
    }
}
