﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace InfluxDbStorage
{
    public class Tags
    {
        public string alarmStatus { get; set; }
        public string isDummy { get; set; }
        public string isTimeStamp { get; set; }
        public string sensorTypeId { get; set; }
        public string serialNumber { get; set; }
        public string timeStampComment { get; set; }
    }

    public class Series
    {
        public string name { get; set; }
        public Tags tags { get; set; }
        public List<string> columns { get; set; }
        public List<List<double>> values { get; set; }
    }

    public class Result
    {
        public List<Series> series { get; set; }
    }

    public class QueryResult
    {
        public List<Result> results { get; set; }
    }

//        [JsonProperty(PropertyName = "name")]
//        public string Name { get; set; }
//
//        [JsonProperty(PropertyName = "tags")]
//        public string Tags { get; set; }
//
//        [JsonProperty(PropertyName = "columns")]
//        public string[] ColumnNames { get; set; }
//
//        [JsonProperty(PropertyName = "values")]
//        public List<object[]> Points { get; set; }
//    }
}
