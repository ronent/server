﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infrastructure.Interfaces;

namespace InfluxDbStorage
{
    public class SampleAPI : ISample
    {
        private readonly Manager _manager = new Manager();

        public bool InsertSample(Server.Infrastructure.SensorSample sample)
        {
           return  _manager.InsertSample(sample);
        }

        public bool InsertBatch(List<Server.Infrastructure.SensorSample> samples)
        {
            throw new NotImplementedException();
        }

        public DateTime LastSampleUTC(IEnumerable<int> serialNumber)
        {
            throw new NotImplementedException();
        }

        public DateTime LastSampleUTC(int serialNumber)
        {
            throw new NotImplementedException();
        }

        public dynamic GetSensorStatistics(int serialNumber, int sensorTypeID, int startTime, int endTime)
        {
            throw new NotImplementedException();
        }

        public bool BuildSampleAggregator(int serialNumber, int sensorTypeID, int startTime, int endTime, ISampleAggregator aggregator)
        {
            throw new NotImplementedException();
        }

        public List<dynamic> Find(int serialNumber, int sensorTypeID)
        {
            throw new NotImplementedException();
        }

        public List<dynamic> Find(int serialNumber, int sensorTypeID, bool lastDownloadData = true, short alarmStatus = 0)
        {
            throw new NotImplementedException();
        }

        public List<Tuple<int, double>> FindReduced(int serialNumber, int sensorTypeID, int startTime, int endTime, int maxCount)
        {
            throw new NotImplementedException();
        }

        public List<dynamic> FindPart(int serialNumber, int sensorTypeID, int startTime, int endTime, int offset = 0, int count = -1)
        {
            throw new NotImplementedException();
        }

        public long RowsCount(int serialNumber, int sensorTypeID)
        {
            throw new NotImplementedException();
        }

        public bool Backup(bool wipeData = false, int startTime = 0, int endTime = 0)
        {
            throw new NotImplementedException();
        }

        public bool Backup(List<int> serialNumbers)
        {
            throw new NotImplementedException();
        }

        public bool Restore()
        {
            throw new NotImplementedException();
        }

        public bool WipeData(List<int> serialNumbers)
        {
            throw new NotImplementedException();
        }


        public double LastSampleValue(int serialNumber, int sensorTypeID)
        {
            throw new NotImplementedException();
        }
    }
}
