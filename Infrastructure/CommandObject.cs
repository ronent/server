﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Infrastructure
{
    public class CommandObject
    {
        public string RequestID { get; set; }
        public string UserID { get; set; }
        public string Command { get; set; }
        public string Entity { get; set; }
        public string Data { get; set; }
        public string SiteID { get; set; }
    }
}
