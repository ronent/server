﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using Infrastructure;
using Infrastructure.MailService;
using Log4Tech;

namespace Server.Infrastructure
{
    public class Common
    {
        public static ConcurrentDictionary<int, int> User2Customer = new ConcurrentDictionary<int, int>();
        public static ConcurrentDictionary<int, string> User2TimeZone = new ConcurrentDictionary<int, string>();
        public static ConcurrentDictionary<int, int> SerialNumber2Customer = new ConcurrentDictionary<int, int>();
        public static ConcurrentDictionary<int,long> MaxStorageAllowed =new ConcurrentDictionary<int, long>();
        public static ConcurrentDictionary<int, int> DataStorageExpiry =new ConcurrentDictionary<int, int>();
        public static ConcurrentDictionary<int, int> LastSample = new ConcurrentDictionary<int, int>();
        public static DeviceManager DeviceApiManager = DeviceManager.Instance;

        public static string MeasurementNameBySensorTypeID(int typeID)
        {
            switch (typeID)
            {
                case 1:	//Internal NTC
                case 2:	//DigitalTemperature
                case 4:	//DewPoint
                case 7:	//ExternalNTC
                case 8:	//PT100
                    return "Temperature";

                case 3: //Humidity
                    return "Humidity";

                case 5:	//Current4_20mA
                    return "Current";

                case 6:	//Voltage0_10V
                    return "Voltage";
            }
            return null;
        }

        public static void SendEmail(string toEmail, string subject, string emailBody)
        {
            var fromMail = new MailAddress(ConfigurationManager.AppSettings["fromEmail"], "SiMiO");
            var mail = new MailMessage
            {
                Body = emailBody,
                From = fromMail,
                IsBodyHtml = true,
                Subject = subject,
                To = { toEmail },
            };
            MailManager.SendEmail(mail);
        }

        public static bool CheckDateRange(dynamic dateRange, out int start, out int end)
        {
            start = 0;
            end = 0;

            if (dateRange == null)
            {
                Log.Instance.Write("No date range specified", "Common", Log.LogSeverity.ERROR);
                return false;
            }

            bool translate = true;
            DateTime start_time = DateTime.MinValue, end_time = DateTime.Now;
            switch ((string)dateRange.range)
            {
                case "LAST24H":
                    start_time = DateTime.Now.AddHours(-24);
                    break;

                case "LAST3D":
                    start_time = DateTime.Now.AddDays(-3);
                    break;

                case "LAST7D":
                    start_time = DateTime.Now.AddDays(-7);
                    break;

                case "LAST30D":
                    start_time = DateTime.Now.AddDays(-30);
                    break;

                case "LAST_YEAR":
                    start_time = DateTime.Now.AddYears(-1);
                    break;

                case "ALL":
                    start_time = DateTime.MinValue;
                    break;

                default:
                    translate = false;
                    if (dateRange.fromDate != null)
                        start = (int)(dateRange.fromDate.Value / 1000);
                    else
                        return false;

                    if (dateRange.toDate != null)
                        end = (int)(dateRange.toDate.Value / 1000);
                    else
                        return false;

                    break;
            }

            if (translate)
            {
                if (start_time >= end_time)
                {
                    Log.Instance.Write("Invalid date range", "Common", Log.LogSeverity.ERROR);
                    return false;
                }

                start = Utc2TimeStamp(start_time);
                end = Utc2TimeStamp(end_time);
            }
            return true;
        }

        public static void GetUtcOffset(int userId,DateTime timestamp, out DateTime convertedTimestamp, out int offset)
        {
            offset = 0;
            convertedTimestamp = timestamp;
            string tempTimeZone;
            User2TimeZone.TryGetValue(Convert.ToInt32(userId), out tempTimeZone);
            if (tempTimeZone != null)
            {
                var utcOffset=0;
                try
                {
//                    foreach (var zone in TimeZoneInfo.GetSystemTimeZones())
//                    {
//                        Console.WriteLine("{0:00.00} {1}", zone.BaseUtcOffset.TotalHours, zone.Id);
//                    }
                    var tz = TimeZoneInfo.FindSystemTimeZoneById(tempTimeZone);

                    utcOffset = tz.BaseUtcOffset.Hours;

                    if (tz.IsDaylightSavingTime(timestamp))
                        utcOffset = utcOffset + 1;
                }
                catch (TimeZoneNotFoundException)
                {
                    
                }
                convertedTimestamp = timestamp.AddHours(utcOffset);
                offset = utcOffset;
            }
           
        }

        public static int Utc2TimeStamp(DateTime dt)
        {
            if (dt == DateTime.MaxValue)
                return 0;

            //return (int)(dt - new DateTime(1970, 1, 1).ToLocalTime()).TotalSeconds;
            return (int)(dt.ToUniversalTime() - new DateTime(1970, 1, 1)).TotalSeconds;
        }

        public static byte[] ObjectToByteArray(object obj)
        {
            var bf = new BinaryFormatter();
            using (var ms = new MemoryStream())
            {
                bf.Serialize(ms, obj);
                return ms.ToArray();
            }
        }

        public static object ByteArrayToObject(byte[] arrBytes)
        {
            var memStream = new MemoryStream();
            var binForm = new BinaryFormatter();
            memStream.Write(arrBytes, 0, arrBytes.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            var obj = binForm.Deserialize(memStream);
            return obj;
        }
    }
}
