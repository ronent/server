﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.DAL
{
    public class DBBase
    {
        protected DbProviderFactory _factory;
        public string _connString { get; private set; }



        protected void Init(string ConnectionString, string provider)
        {
            // FileInfo fi = new FileInfo(@"c:\Users\ronen\Documents\Visual Studio 2012\Projects\PandaBinaricOptions\BL\app.config");
            /*DataTable m_factories = DbProviderFactories.GetFactoryClasses();//GetFactory("System.Data.SqlClient");
            foreach (DataRow dr in m_factories.Rows)
            {
                Logger4Net.Debug(dr[2].ToString()); ;
            }*/
            _connString = ConnectionString;
            _factory = DbProviderFactories.GetFactory(provider);
            //foreach (DataRow dr in m_factories.Rows)
            //{
            //}
        }

        protected System.Data.DataTable ExecuteQuery(string query)
        {
            DataTable dt = new DataTable();
            using (DbConnection m_connection = _factory.CreateConnection())
            {
                m_connection.ConnectionString = _connString;
                m_connection.Open();
                using (DbCommand cmd = m_connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;

                    DbDataAdapter adapt = _factory.CreateDataAdapter();
                    adapt.SelectCommand = cmd;
                    adapt.Fill(dt);
                }
            }
            return dt;
        }

        protected object ExecuteScalar(string SPName, List<DbParameter> sqlParams)
        {
            using (DbConnection m_connection = _factory.CreateConnection())
            {
                m_connection.ConnectionString = _connString;
                m_connection.Open();
                using (DbCommand cmd = m_connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = SPName;
                    List<DbParameter>.Enumerator enu = sqlParams.GetEnumerator();

                    while (enu.MoveNext())
                    {
                        DbParameter param = enu.Current;
                        cmd.Parameters.Add(param);
                    }
                    return cmd.ExecuteScalar();
                }
            }
        }

        protected System.Data.DataTable Execute(string SPName, List<DbParameter> sqlParams)
        {
            Log4Tech.Log.Instance.Write("[Database] EXEC '{0}' {1}", this, Log4Tech.Log.LogSeverity.INFO, SPName, extractSqlParams(sqlParams));
            DataTable dt = new DataTable();
            using (DbConnection m_connection = _factory.CreateConnection())
            {
                m_connection.ConnectionString = _connString;
                m_connection.Open();
                using (DbCommand cmd = m_connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = SPName;
                    List<DbParameter>.Enumerator enu = sqlParams.GetEnumerator();

                    while (enu.MoveNext())
                    {
                        DbParameter param = enu.Current;
                        cmd.Parameters.Add(param);
                    }
                    DbDataAdapter adapt = _factory.CreateDataAdapter();
                    adapt.SelectCommand = cmd;
                    adapt.Fill(dt);
                }
            }
            return dt;
        }

        protected System.Data.DataTable Execute(string SPName)
        {
            DataTable dt = new DataTable();
            using (DbConnection m_connection = _factory.CreateConnection())
            {
                m_connection.ConnectionString = _connString;
                m_connection.Open();
                using (DbCommand cmd = m_connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = SPName;
                    DbDataAdapter adapt = _factory.CreateDataAdapter();
                    adapt.SelectCommand = cmd;
                    adapt.Fill(dt);
                }
            }
            return dt;
        }

        protected System.Data.DataSet ExecuteDataSet(string SPName)
        {
            DataSet ds = new DataSet();
            try
            {
                using (DbConnection m_connection = _factory.CreateConnection())
                {
                    m_connection.ConnectionString = _connString;
                    m_connection.Open();
                    using (DbCommand cmd = m_connection.CreateCommand())
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = SPName;
                        DbDataAdapter adapt = _factory.CreateDataAdapter();
                        adapt.SelectCommand = cmd;
                        adapt.Fill(ds);
                    }
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in ExecuteDataSet", this, ex);
            }
            return ds;
        }

        protected System.Data.DataSet ExecuteDataSet(string SPName, List<DbParameter> sqlParams)
        {
            DataSet ds = new DataSet();
            using (DbConnection m_connection = _factory.CreateConnection())
            {
                m_connection.ConnectionString = _connString;
                m_connection.Open();
                using (DbCommand cmd = m_connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = SPName;
                    List<DbParameter>.Enumerator enu = sqlParams.GetEnumerator();

                    while (enu.MoveNext())
                    {
                        DbParameter param = enu.Current;
                        cmd.Parameters.Add(param);
                    }
                    DbDataAdapter adapt = _factory.CreateDataAdapter();
                    adapt.SelectCommand = cmd;
                    adapt.Fill(ds);
                }
            }
            return ds;
        }

        protected DbParameter CreateParameter(string name, string value)
        {
            DbParameter param = _factory.CreateParameter();
            param.DbType = DbType.String;
            param.Direction = ParameterDirection.Input;
            param.ParameterName = name;
            param.Value = value;

            return param;
        }

        protected DbParameter CreateParameter(string name, object value, DbType dbtype)
        {
            DbParameter param = _factory.CreateParameter();
            param.DbType = dbtype;
            param.Direction = ParameterDirection.Input;
            param.ParameterName = name;
            param.Value = value;

            return param;
        }

        protected string extractSqlParams(List<DbParameter> sqlParams)
        {
            StringBuilder sb = new StringBuilder();

            sqlParams.ForEach( sql =>
                {
                    sb.AppendFormat("{0}:{1},", sql.ParameterName, sql.Value);
                }
                );
            if (sb.Length>0)
            {
                sb = sb.Remove(sb.Length - 1, 1);
            }
            return sb.ToString();
        }

        /*protected DbParameter CreateParameter(string name, object value, DbType dbtype, ParameterDirection direction)
        {
            DbParameter param = _factory.CreateParameter();
            param.DbType = dbtype;
            param.Direction = direction;
            param.ParameterName = name;
            param.Value = value;

            return param;
        }*/
    }
}
