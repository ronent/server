﻿using Base.Sensors.Management;
using Base.Sensors.Types;
using Infrastructure.Entities;
using Infrastructure.Interfaces;
using Infrastructure.Responses;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Reflection;
using Server.Infrastructure.Interfaces;

namespace Infrastructure
{
    public class DataManager : IData
    {
        private IData dataComm;

        public enum AlarmType
        {
            Sensor = 1,
            Battery
        }

        private DataManager()
        {
            loadDataDll();
        }

        private static readonly Lazy<DataManager> lazy = new Lazy<DataManager>(() => new DataManager());

        public static DataManager Instance
        {
            get { return lazy.Value; }
        }

        #region IData

        public dynamic CreateNewCustomer(dynamic newCustomer)
        {
            return dataComm.CreateNewCustomer(newCustomer);
        }

        public PostBack CreateNewAdminUser(AdminUser newAdminUser)
        {
            return (dataComm.CreateNewAdminUser(newAdminUser));
        }

        public bool UserCFRCheck(Int32 userid, String command, String entity, Int32 serialNumber = 0, String reason = "")
        {
            return dataComm.UserCFRCheck(userid, command, entity, serialNumber, reason);
        }

//        public dynamic CreateUser(BasicUser user)
//        {
//            return (dataComm.CreateUser(user));
//        }

        public Response RegsiterLanRcv(LanRcvRegisteration lanRcv)
        {
            return (dataComm.RegsiterLanRcv(lanRcv));
        }

        public dynamic ValidateLogin(string userName, string password)
        {
            return dataComm.ValidateLogin(userName, password);
        }

        public dynamic GetUserSites(int userID)
        {
            return dataComm.GetUserSites(userID);
        }

        public dynamic GetSiteById(int siteID)
        {
            return dataComm.GetSiteById(siteID);
        }

        public dynamic GetGroupPermissions()
        {
            return dataComm.GetGroupPermissions();
        }

        public dynamic SetupUser(dynamic userData)
        {
            return dataComm.SetupUser(userData);
        }

        public dynamic GetLanguages()
        {
            return dataComm.GetLanguages();
        }

        public dynamic GetSecureQuestions()
        {
            return dataComm.GetSecureQuestions();
        }

        public dynamic GetDevicesBySite(int siteID, int userID)
        {
            return dataComm.GetDevicesBySite(siteID, userID);
        }

        public ListCategories GetCategories()
        {
            return dataComm.GetCategories();
        }

        public dynamic GetDevicesByCategory(int categoryID, int userID)
        {
            return dataComm.GetDevicesByCategory(categoryID, userID);
        }

//        public DeviceList GetDevices(DeviceIDs parameters)
//        {
//            return dataComm.GetDevices(parameters);
//        }

        public dynamic GetDevice(int serialNumber)
        {
            return dataComm.GetDevice(serialNumber);
        }

        public dynamic GetUsers(int userId)
        {
            return dataComm.GetUsers(userId);
        }

        public void DeleteUser(int userId)
        {
            dataComm.DeleteUser(userId);
        }

        public dynamic CreateSite(dynamic site)
        {
            return dataComm.CreateSite(site);
        }

        public Response CreateContact(Contact contact)
        {
            return dataComm.CreateContact(contact);
        }

        public ContactList GetContacts(SingleContact contact)
        {
            return dataComm.GetContacts(contact);
        }

        public Response UpdateContact(Contact contact)
        {
            return dataComm.UpdateContact(contact);
        }

        public Response UpdateUser(User user)
        {
            return dataComm.UpdateUser(user);
        }

        public dynamic DeleteContact(int contactId)
        {
            return dataComm.DeleteContact(contactId);
        }

        public ResponseSensorLogs GetLastSensorLogs(SingleSensor sensor)
        {
            return dataComm.GetLastSensorLogs(sensor);
        }

        public bool isCommandAllowed(UserCommand cmd)
        {
            return dataComm.isCommandAllowed(cmd);
        }

        public Response UpsertDevice(DeviceUpsert device)
        {
            return dataComm.UpsertDevice(device);
        }

        public void SystemActionsAudit(int userid, string command, string entity, int serialnumber)
        {
            dataComm.SystemActionsAudit(userid, command, entity, serialnumber);
        }

        public dynamic GetAllowedUsers(int serialNumber)
        {
            return dataComm.GetAllowedUsers(serialNumber);
        }

        public Int32 GetDeviceSensorID(int serialNumber, int sensorTypeID)
        {
            return dataComm.GetDeviceSensorID(serialNumber, sensorTypeID);
        }

        public void InsertSensorDownloadHistory(int serialNumber, int sensorTypeID, int firstDownloadTime,
            int lastDownloadTime)
        {
            dataComm.InsertSensorDownloadHistory(serialNumber, sensorTypeID, firstDownloadTime, lastDownloadTime);
        }

        public dynamic GetLastSensorDownload(int serialNumber, int sensorTypeID)
        {
            return dataComm.GetLastSensorDownload(serialNumber, sensorTypeID);
        }

        public List<dynamic> GetDeviceSensorsID(int serialNumber)
        {
            return dataComm.GetDeviceSensorsID(serialNumber);
        }

        public int GetNextSerialNumber(int AllocateNumber)
        {
            return dataComm.GetNextSerialNumber(AllocateNumber);
        }

        public dynamic PasswordRecovery(dynamic email)
        {
            return dataComm.PasswordRecovery(email);
        }

        public dynamic ResetPassword(dynamic reset)
        {
            return dataComm.ResetPassword(reset);
        }

        public dynamic SecureAnswerCheck(dynamic answer)
        {
            return dataComm.SecureAnswerCheck(answer);
        }

        public void SaveReport(int serialNumber, string S3Url, int reportProfileId)
        {
            dataComm.SaveReport(serialNumber, S3Url, reportProfileId);
        }

        public dynamic SaveAlarmNotification(dynamic alarmNotifications)
        {
            return dataComm.SaveAlarmNotification(alarmNotifications);
        }

        public dynamic GetNotificationContacts(string serialNumber, int sensorTypeID = 0)
        {
            return dataComm.GetNotificationContacts(serialNumber, sensorTypeID);
        }

        public void UpdateNotificationEmailSent(string serialNumber, int contactID, int sensorTypeID)
        {
            dataComm.UpdateNotificationEmailSent(serialNumber, contactID, sensorTypeID);
        }

        public void UpdateNotificationBatteryEmailSent(string serialNumber, int contactID)
        {
            dataComm.UpdateNotificationBatteryEmailSent(serialNumber, contactID);
        }

        public void UpdateNotificationSMSSent(string serialNumber, int contactID, string MsgId, int sensorTypeID)
        {
            dataComm.UpdateNotificationSMSSent(serialNumber, contactID, MsgId, sensorTypeID);
        }

        public void UpdateNotificationBatterySMSSent(string serialNumber, int contactID, string MsgId)
        {
            dataComm.UpdateNotificationBatterySMSSent(serialNumber, contactID, MsgId);
        }

        public dynamic GetCustomer(string email)
        {
            return dataComm.GetCustomer(email);
        }

        public void UpdateSMSStatus(string smsID, string smsStatus)
        {
            dataComm.UpdateSMSStatus(smsID, smsStatus);
        }

        public List<int> GetDevicesByUser(string userid)
        {
            return dataComm.GetDevicesByUser(userid);
        }

        public dynamic GetDevicesByUser(int userID)
        {
            return dataComm.GetDevicesByUser(userID);
        }

        public dynamic InsertAlarmNotification(string serialNumber, DataManager.AlarmType alarmType, double value,
            DateTime startTime, eSensorType sensorType)
        {
            return dataComm.InsertAlarmNotification(serialNumber, alarmType, value, startTime, sensorType);
        }

        public void UpdateAlarmNotification(string serialNumber, DataManager.AlarmType alarmType, eSensorType sensorType)
        {
            dataComm.UpdateAlarmNotification(serialNumber, alarmType, sensorType);
        }

        public dynamic ReadMKTSensorData(int userID, int sensorID)
        {
            return dataComm.ReadMKTSensorData(userID, sensorID);
        }

        public void SaveMKTSensorData(int userID, int sensorID, double low, double high, double activationEnergy)
        {
            dataComm.SaveMKTSensorData(userID, sensorID, low, high, activationEnergy);
        }

        public bool GetSensorLocatorByID(int deviceSensorID, out int serialNumber, out int sensorTypeID)
        {
            return dataComm.GetSensorLocatorByID(deviceSensorID, out serialNumber, out sensorTypeID);
        }

        public dynamic GetAnalyticsSensors(int userid)
        {
            return dataComm.GetAnalyticsSensors(userid);
        }

        public void AddAnalyticsSensor(int userID, int sensorID, int isActive = 1)
        {
            dataComm.AddAnalyticsSensor(userID, sensorID, isActive);
        }

        public void RemoveAnalyticsSensor(int userID, int sensorID)
        {
            dataComm.RemoveAnalyticsSensor(userID, sensorID);
        }

        public void RemoveAnalyticsSensors(int userId)
        {
            dataComm.RemoveAnalyticsSensors(userId);
        }

        public dynamic GetAlarmNotifications(int userid)
        {
            return dataComm.GetAlarmNotifications(userid);
        }

        public int DeviceSerialNumberByAlarmID(int alarm_id)
        {
            return dataComm.DeviceSerialNumberByAlarmID(alarm_id);
        }

        public int[] AllAlarmedDevicesSerialNumbers(int userId)
        {
            return dataComm.AllAlarmedDevicesSerialNumbers(userId);
        }

        public void UpdateAlarmReason(int alarmID, int serialNumber, int alarmTypeID, string reason,
            eSensorType sensorType)
        {
            dataComm.UpdateAlarmReason(alarmID, serialNumber, alarmTypeID, reason, sensorType);
        }

        public void HideAlarm(string serialNumber, int alarmTypeID, eSensorType sensorType)
        {
            dataComm.HideAlarm(serialNumber, alarmTypeID, sensorType);
        }

        public void HideAlarmByID(int id)
        {
            dataComm.HideAlarmByID(id);
        }

        public void HideAllAlarms(int userID)
        {
            dataComm.HideAllAlarms(userID);
        }

        public DateTime GetLastStoredSampleTime(int serialNumber)
        {
            return dataComm.GetLastStoredSampleTime(serialNumber);
        }

        public void SetLastStoredSampleTime(int serialNumber, DateTime dt)
        {
            dataComm.SetLastStoredSampleTime(serialNumber, dt);
        }

        public dynamic UpdateSite(dynamic site)
        {
            return dataComm.UpdateSite(site);
        }

        public void SnoozeAlarm(string serialNumber, int deviceSensorID, int silent)
        {
            dataComm.SnoozeAlarm(serialNumber, deviceSensorID, silent);
        }

        public dynamic GetCustomerStorage(int serialNumber)
        {
            return dataComm.GetCustomerStorage(serialNumber);
        }

        public dynamic GetDeviceSensorsDownloaded(int deviceSensorID)
        {
            return dataComm.GetDeviceSensorsDownloaded(deviceSensorID);
        }

        public string GetDeviceFirmwareVersion(int serialNumber)
        {
            return dataComm.GetDeviceFirmwareVersion(serialNumber);
        }

        public void RelateDeviceToSite(int serialNumber, int siteID)
        {
            dataComm.RelateDeviceToSite(serialNumber, siteID);
        }

        public dynamic GetDefinedSensorsByDevice(int serialNumber)
        {
            return dataComm.GetDefinedSensorsByDevice(serialNumber);
        }

        public dynamic GetCustomerDisableStatus(int serialNumber)
        {
            return dataComm.GetCustomerDisableStatus(serialNumber);
        }

        public void UpdateCustomerEmailSent(int serialNumber)
        {
            dataComm.UpdateCustomerEmailSent(serialNumber);
        }

        public int GetDeviceTimeZone(int serialNumber)
        {
            return dataComm.GetDeviceTimeZone(serialNumber);
        }

        public dynamic GetAllCustomerSerialNumbers(int serialNumber)
        {
            return dataComm.GetAllCustomerSerialNumbers(serialNumber);
        }

        public void UpdateMongoDBBackup(int serialNumber)
        {
            dataComm.UpdateMongoDBBackup(serialNumber);
        }

        public void UpdateDeviceAlarmData(int serialNumber, IEnumerable<GenericSensor> sensors)
        {
            dataComm.UpdateDeviceAlarmData(serialNumber, sensors);
        }

        public dynamic DeleteSite(dynamic site)
        {
            return dataComm.DeleteSite(site);
        }

        public int GetCustomerID(int serialNumber)
        {
            return dataComm.GetCustomerID(serialNumber);
        }

        public int GetCustomerBySensor(int deviceSensorId)
        {
            return dataComm.GetCustomerBySensor(deviceSensorId);
        }

        public dynamic GetReportsArchive(int userId, int start, int end)
        {
            return dataComm.GetReportsArchive(userId, start, end);
        }

        public void DeleteReport(int reportId)
        {
            dataComm.DeleteReport(reportId);
        }

        public dynamic GetReportsReviews(int userId, int reportType)
        {
            return dataComm.GetReportsReviews(userId, reportType);
        }

        public dynamic ApproveReport(int userId, int reportDeviceHistoryId)
        {
            return dataComm.ApproveReport(userId, reportDeviceHistoryId);
        }

        public dynamic AddProfileReport(int userId, dynamic profile, bool isNew = true)
        {
            return dataComm.AddProfileReport(userId, profile, isNew);
        }

        public dynamic GetApprovalsReviewersList(int userId)
        {
            return dataComm.GetApprovalsReviewersList(userId);
        }

        public dynamic GetDistributionList(int userId)
        {
            return dataComm.GetDistributionList(userId);
        }

        public dynamic GetReportProfiles(int userId, string filter)
        {
            return dataComm.GetReportProfiles(userId, filter);
        }

        public dynamic GetUser(int userId)
        {
            return dataComm.GetUser(userId);
        }

        public dynamic GetCustomerBalance(int userId)
        {
            return dataComm.GetCustomerBalance(userId);
        }

        public dynamic SaveUser(dynamic user)
        {
            return dataComm.SaveUser(user);
        }

        public bool ChangePassword(dynamic user)
        {
            return dataComm.ChangePassword(user);
        }

        public dynamic GetSystemSettings(int userId)
        {
            return dataComm.GetSystemSettings(userId);
        }

        public dynamic SaveSystemSettings(dynamic settings)
        {
            return dataComm.SaveSystemSettings(settings);
        }

        public dynamic GetDeviceAutoSetup(int userId)
        {
            return dataComm.GetDeviceAutoSetup(userId);
        }

        public void AddDeviceSetupFile(dynamic setupFile)
        {
            dataComm.AddDeviceSetupFile(setupFile);
        }

        public dynamic GetDeviceSetupFiles(int userId)
        {
            return dataComm.GetDeviceSetupFiles(userId);
        }

        public dynamic ApplyDeviceAutoSetup(int userId, dynamic setup)
        {
            return dataComm.ApplyDeviceAutoSetup(userId, setup);
        }

        public void DeleteSetupFile(int userId, string fileId)
        {
            dataComm.DeleteSetupFile(userId, fileId);
        }

        public dynamic GetCalibrationCertificateDefault(int userId)
        {
            return dataComm.GetCalibrationCertificateDefault(userId);
        }

        public dynamic SetCalibrationCertificateDefault(int userId, dynamic calibrationDefault)
        {
            return dataComm.SetCalibrationCertificateDefault(userId, calibrationDefault);
        }

        public void UpdateDeviceFwFlag(int serialNumber, int fwUpdateFlag)
        {
            dataComm.UpdateDeviceFwFlag(serialNumber, fwUpdateFlag);
        }

        public dynamic GetDefinedSensors(int userId)
        {
            return dataComm.GetDefinedSensors(userId);
        }

        public dynamic GetDeviceFamilyTypes()
        {
            return dataComm.GetDeviceFamilyTypes();
        }

        public dynamic GetSensorUnits(int familyId)
        {
            return dataComm.GetSensorUnits(familyId);
        }

        public dynamic GetBaseSensor(int familyId)
        {
            return dataComm.GetBaseSensor(familyId);
        }

        public void UpdateDefinedSensor(int userId, dynamic definedSensor)
        {
            dataComm.UpdateDefinedSensor(userId, definedSensor);
        }

        public void DeleteDefinedSensor(int definedSensorId)
        {
            dataComm.DeleteDefinedSensor(definedSensorId);
        }

        public void UpdateSensorAlarmNotification(dynamic notifications)
        {
            dataComm.UpdateSensorAlarmNotification(notifications);
        }

        public dynamic GetCalibrationExpiryReminder(int userId)
        {
            return dataComm.GetCalibrationExpiryReminder(userId);
        }

        public void UpdateCalibrationExpiryReminder(int userId, dynamic reminder)
        {
            dataComm.UpdateCalibrationExpiryReminder(userId, reminder);
        }

        public dynamic IsDeviceAutoSetup(int serialNumber)
        {
            return dataComm.IsDeviceAutoSetup(serialNumber);
        }

//        public int GetCustomerIdByUser(int userId)
//        {
//            return dataComm.GetCustomerIdByUser(userId);
//        }

        public dynamic GetActivity(int userId)
        {
            return dataComm.GetActivity(userId);
        }

        public dynamic GetPasswordExpiry(int userId)
        {
            return dataComm.GetPasswordExpiry(userId);
        }

        public dynamic GetPrivilegesGroups(int userId)
        {
            return dataComm.GetPrivilegesGroups(userId);
        }

        public dynamic GetMatrix(int userId, int groupLevel)
        {
            return dataComm.GetMatrix(userId, groupLevel);
        }

        public dynamic UpdatePwdConfig(int userId, dynamic passwordExpiry)
        {
            return dataComm.UpdatePwdConfig(userId, passwordExpiry);
        }

        public void InsertDevicesBatch(string batchDevices)
        {
            dataComm.InsertDevicesBatch(batchDevices);
        }

        public dynamic GetAuditTrailData(int userId, int start, int end)
        {
            return dataComm.GetAuditTrailData(userId, start, end);
        }

        public dynamic GetAllContacts(int userId)
        {
            return dataComm.GetAllContacts(userId);
        }

        public dynamic GetGroups(int userId)
        {
            return dataComm.GetGroups(userId);
        }

        public dynamic UpsertContact(int userId, dynamic contactData)
        {
            return dataComm.UpsertContact(userId, contactData);
        }

        public void DeleteGroup(int groupId)
        {
            dataComm.DeleteGroup(groupId);
        }

        public void UpsertGroup(int userId, dynamic groupData)
        {
            dataComm.UpsertGroup(userId, groupData);
        }

        public dynamic GetDevicesByCustomer(int userId)
        {
            return dataComm.GetDevicesByCustomer(userId);
        }

        public void DeAssociateDevices(int serialNumber)
        {
            dataComm.DeAssociateDevices(serialNumber);
        }

        public void NewPermissionGroup(int userId, dynamic groupData)
        {
            dataComm.NewPermissionGroup(userId, groupData);
        }

        public dynamic GetPermissonRoles(int userId)
        {
            return dataComm.GetPermissonRoles(userId);
        }

        public void SaveMatrix(int userId, dynamic matrixData)
        {
            dataComm.SaveMatrix(userId, matrixData);
        }

        public dynamic AddUser(int userId, dynamic userData)
        {
           return dataComm.AddUser(userId, userData);
        }

        public dynamic GetUserRecover(int userId)
        {
            return dataComm.GetUserRecover(userId);
        }

        public dynamic GetDeviceAlarmNotifications(int contactId)
        {
            return dataComm.GetDeviceAlarmNotifications(contactId);
        }

        public dynamic UserGuidIsValid(string guid)
        {
            return dataComm.UserGuidIsValid(guid);
        }

        public dynamic SignupCustomer(string email)
        {
            return dataComm.SignupCustomer(email);
        }

        public dynamic CustomerGuidIsValid(string guid)
        {
            return dataComm.CustomerGuidIsValid(guid);
        }

        public void AdminUpdateUser(int userId, dynamic userData)
        {
            dataComm.AdminUpdateUser(userId,userData);
        }

        public int GetCountryCallingCode(string countryCode)
        {
            return dataComm.GetCountryCallingCode(countryCode);
        }

        public dynamic ResetUserPassword(dynamic newPasswordData)
        {
            return dataComm.ResetUserPassword(newPasswordData);
        }

        public dynamic GetUserByUserName(string userName)
        {
            return dataComm.GetUserByUserName(userName);
        }

        public dynamic GetCustomers()
        {
            return dataComm.GetCustomers();
        }

        public void UpdateUserCustomerId(int userId, int customerId)
        {
            dataComm.UpdateUserCustomerId(userId, customerId);
        }

        public void UpdateDeviceStatus(int serialNumber, int statusId, int siteId, int isResetDownload, int alarmDelay)
        {
            dataComm.UpdateDeviceStatus(serialNumber, statusId, siteId, isResetDownload, alarmDelay);
        }

        public void UpdateDeviceSensorName(int serialNumber, string sensorName, int sensorTypeId)
        {
            dataComm.UpdateDeviceSensorName(serialNumber, sensorName, sensorTypeId);
        }

        public void UpdateDevicesDisconnection()
        {
            dataComm.UpdateDevicesDisconnection();
        }

        public void UpdateSensorLastSample(int serialNumber, int sensorTypeId, int sampleTime, double sampleValue)
        {
            dataComm.UpdateSensorLastSample(serialNumber, sensorTypeId, sampleTime, sampleValue);
        }

        public dynamic GetAlarmNotificationsBySerialNumber(int serialNumber)
        {
            return dataComm.GetAlarmNotificationsBySerialNumber(serialNumber);
        }

        public dynamic GetReportEmails(int reportProfileId)
        {
            return dataComm.GetReportEmails(reportProfileId);
        }

        public dynamic GetReportsForExecution()
        {
            return dataComm.GetReportsForExecution();
        }
        #endregion

        private void loadDataDll()
        {
            //get the working directory of current application
            var dir = AppDomain.CurrentDomain.BaseDirectory;
            //go over all dlls in the folder
            foreach (var f in Directory.EnumerateFiles(dir, "*DataLayer.dll", SearchOption.TopDirectoryOnly))
            {
                try
                {
                    //loads the dll to get assembly info
                    var assembly = Assembly.LoadFile(f);
                    //looks for instances which implement an interface of IDataCommunication
                    var dataInstances = from t in assembly.GetTypes()
                        where (t.GetInterfaces().Contains(typeof (IData))
                               && t.GetConstructor(Type.EmptyTypes) != null)
                        select Activator.CreateInstance(t) as IData;

                    foreach (var data in dataInstances)
                    {
                        //assume there is only one
                        dataComm = data;
                        Log4Tech.Log.Instance.Write("[Data Layer] loaded successfully", this,
                            Log4Tech.Log.LogSeverity.INFO);
                    }

                    /* var deviceInstances = from t in assembly.GetTypes()
                                           where (t.GetInterfaces().Contains(typeof(IDeviceCommunication))
                                                    && t.GetConstructor(Type.EmptyTypes) != null)
                                           select Activator.CreateInstance(t) as IDeviceCommunication;

                     foreach (var device in deviceInstances)
                     {
                         //add the device communication dll by the assembly name as a key
                         deviceComm.AddOrUpdate(assembly.GetName().Name, device, (key, oldValue) => device);
                     }*/
                }
                catch
                {
                    // ignored
                }
            }
        }

    }
}
