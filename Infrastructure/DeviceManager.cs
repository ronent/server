﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Base.OpCodes;
using Log4Tech;
using Server.Infrastructure.Interfaces;

namespace Server.Infrastructure
{

    public class DeviceManager : IDevice
    {
        private IDevice deviceComm;

        #region singleton
        private static readonly Lazy<DeviceManager> lazy = new Lazy<DeviceManager>(() => new DeviceManager());

        public static DeviceManager Instance
        {
            get { return lazy.Value; }
        }

        private DeviceManager()
        {
            
        }

        #endregion

        public void Start()
        {
            loadDataDll();
            
        }

        public enum Types
        {
            LAN_Rcv = 1,
            PicoLite,
            Microlite,
            MicroLogPro2,
            MiniDataNetLogger,
            DaqLink,
            DataNetLogger
        }

        public enum Statuses
        {
            Connected = 1,
            Disconnected,
            AutoSetup,
            Running,
            Setup,
            InCalibrationMode,
            Stopped,
        }

        private string[] SensorColors =
        {
            "#363636",
            "#8a8a8a",
            "#afa132",
            "#d2691e",
            "#ff3030",
            "#7ac5cd",
            "#9f79ee",
            "#737373",
            "#00868b",
            "#00e5ee",
            "#696969",
            "#47C285",
            "#993366",
            "#47FFFF",
            "#66CC00",
            "#700000",
            "#CCB399",
            "#99B3CC",
            "#59773C",
            "#3366FF"
        };

        public enum TimeZones
        {

        }

        public enum TempatureScale
        {
            Celcisus = 1,
            Farenheight
        }

        #region IDevice Members

        public string GetSensorColor(int index)
        {
            return index >= SensorColors.Length ? "#000000" : SensorColors[index];
        }

        public Result Download(string serialNumber)
        {
            return deviceComm.Download(serialNumber);
        }

//        public dynamic Status(string serialNumber)
//        {
//            return deviceComm.Status(serialNumber);
//        }

        public Result Setup(dynamic setup)
        {
            return deviceComm.Setup(setup);
        }

        public Result Stop(string serialNumber)
        {
            return deviceComm.Stop(serialNumber);
        }

        public Result Run(string serialNumber)
        {
            return deviceComm.Run(serialNumber);
        }

        public Result ResetCalibration(string serialNumber)
        {
            return deviceComm.ResetCalibration(serialNumber);
        }

        public Result UpdateFirmware(string serialNumber)
        {
            return deviceComm.UpdateFirmware(serialNumber);
        }

//        public IEnumerable<GenericSensor> GetSensors(string serialNumber)
//        {
//            return deviceComm.GetSensors(serialNumber);
//        }

        public Result Calibration(string serialNumber, int sensorIndex, List<Tuple<decimal, decimal>> pairs)
        {
            return deviceComm.Calibration(serialNumber, sensorIndex, pairs);
        }

        public Result MarkTimeStamp(string serialNumber)
        {
            return deviceComm.MarkTimeStamp(serialNumber);
        }

        public Result RestoreCalibration(string serialNumber)
        {
            return deviceComm.RestoreCalibration(serialNumber);
        }

        public Result SaveCalibration(string serialNumber)
        {
            return deviceComm.SaveCalibration(serialNumber);
        }

        public void PushConnectedDevice(string serialNumber)
        {
            deviceComm.PushConnectedDevice(serialNumber);
        }

//        public GenericDevice GetDevice(string serialNumber)
//        {
//            return deviceComm.GetDevice(serialNumber);
//        }

        public Result EnterDeviceToCalibrationMode(dynamic setup)
        {
            return deviceComm.EnterDeviceToCalibrationMode(setup);
        }

        public bool IsDeviceConnected(string serialNumber, bool sendDisconnected = true)
        {
            return deviceComm.IsDeviceConnected(serialNumber,sendDisconnected);
        }

        public int ConnectedDevices(int userId)
        {
            return deviceComm.ConnectedDevices(userId);
        }

        public ConcurrentDictionary<int, dynamic> GetConnectedDevices()
        {
            return deviceComm.GetConnectedDevices();
        }

        public dynamic GetConnectedDevice(int serialNumber)
        {
            return deviceComm.GetConnectedDevice(serialNumber);
        }

//        public bool IsDeviceRunning(string serialNumber)
//        {
//            return deviceComm.IsDeviceRunning(serialNumber);
//        }

        #endregion
        private void loadDataDll()
        {
            //get the working directory of current application
            var dir = AppDomain.CurrentDomain.BaseDirectory;
            //go over all dlls in the folder
            foreach (var f in Directory.EnumerateFiles(dir, "*DeviceLayer.dll", SearchOption.TopDirectoryOnly))
            {
                try
                {
                    //loads the dll to get assembly info
                    var assembly = Assembly.LoadFile(f);
                    //looks for instances which implement an interface of IDataCommunication
                    var deviceInstances = from t in assembly.GetTypes()
                                          where (t.GetInterfaces().Contains(typeof(IDevice))
                                                   && t.GetConstructor(Type.EmptyTypes) != null)
                                          select Activator.CreateInstance(t) as IDevice;

                    foreach (var device in deviceInstances)
                    {
                        //assume there is only one
                        deviceComm = device;
                        Log4Tech.Log.Instance.Write("[Device Layer] loaded successfully", this, Log4Tech.Log.LogSeverity.INFO);
                    }
                }
                catch (Exception ex)
                {
                    Log4Tech.Log.Instance.WriteError("Fail in loadDataDll(DeviceManager) ", this, ex);
                    if(ex.InnerException!=null)
                        Log4Tech.Log.Instance.Write("loadDataDll(DeviceManager) Inner Exception - > {0}, {1}", this, Log.LogSeverity.ERROR, ex.InnerException.Message, ex.InnerException.StackTrace);
                }
            }
        }
    }
}
