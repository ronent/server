﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure
{
    public class Encryptor
    {
        #region Constructor
        private Encryptor()
        {
        }

        private static readonly Lazy<Encryptor> lazy = new Lazy<Encryptor>(() => new Encryptor());

        public static Encryptor Instance
        {
            get { return lazy.Value; }
        }
        #endregion
        #region Methods
        /// <summary>
        /// Encrypt the string to Sha26
        /// </summary>
        /// <param name="text">string to encrypt</param>
        /// <returns>Encrypted string</returns>
        public string getHashSha256(string text)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(text);
            SHA256Managed hashstring = new SHA256Managed();
            byte[] hash = hashstring.ComputeHash(bytes);
            StringBuilder hashString = new StringBuilder();
            foreach (byte x in hash)
            {
                hashString.AppendFormat("{0:x2}", x);
            }
            return hashString.ToString();
        }
        #endregion
    }
}
