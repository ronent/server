﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Entities
{
    public class Contact : SingleContact
    {
        public int UserID { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public int WeekdayStart { get; set; }
        public string WorkdayStart { get; set; }
        public string WorkdayEnd { get; set; }
        public int SMSResendInterval { get; set; }
        public int OutOfOfficeStart { get; set; }
        public int OutOfOfficeEnd { get; set; }
    }
}
