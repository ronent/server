﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Entities
{
    public class Customer 
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public int MaxUsers { get; set; }
        public int MaxStorage { get; set; }
    }
}
