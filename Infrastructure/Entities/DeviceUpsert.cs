﻿using Base.Devices;
using MicroXBase.Devices.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Server.Infrastructure;

namespace Infrastructure.Entities
{
    public class DeviceUpsert
    {
        public GenericDevice Device { get; set; }
        public int SiteID { get; set; }
        public int UserID { get; set; }
        public DeviceManager.Statuses Status { get; set; }
        public bool IsRunning { get; set; }
        public string BoomerangUTC { get; set; }
        public List<string> SensorsName { get; set; }
        public bool IsResetDownload { get; set; }
        public int AlarmDelay { get; set; }
        public int LoggerUTC { get; set; }
        public bool IsInCalibrationMode { get; set; }
    }
}
