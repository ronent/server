﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Entities
{
    public class User : SingleUser
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public int LanguageID { get; set; }
        public int SecureQuestionID { get; set; }
        public string SecureAnswer { get; set; }
        public string DefaultDateFormat { get; set; }

    }
}
