﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Entities
{
    public class UserCommand
    {
        public int UserID { get; set; }
        public string Command { get; set; }
        public string Entity { get; set; }
        public int SiteID { get; set; }
    }
}
