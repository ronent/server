﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using Amazon.SimpleWorkflow.Model;
using Base.Devices;
using Base.OpCodes;
using Base.Sensors.Types;

namespace Server.Infrastructure.Interfaces
{
    public interface IDevice
    {
        Result Download(string serialNumber);
        Result Setup(dynamic setup);
        //dynamic Status(string serialNumber);
        Result Stop(string serialNumber);
        Result Run(string serialNumber);
        Result ResetCalibration(string serialNumber);
        Result UpdateFirmware(string serialNumber);
        Result Calibration(string serialNumber, int sensorIndex, List<Tuple<decimal, decimal>> pairs);
//        IEnumerable<GenericSensor> GetSensors(string serialNumber);
        Result MarkTimeStamp(string serialNumber);
        Result RestoreCalibration(string serialNumber);
        Result SaveCalibration(string serialNumber);
        void PushConnectedDevice(string serialNumber);
//        GenericDevice GetDevice(string serialNumber);
        Result EnterDeviceToCalibrationMode(dynamic setup);
        bool IsDeviceConnected(string serialNumber, bool sendDisconnected = true);
        int ConnectedDevices(int userId);
        ConcurrentDictionary<int,dynamic> GetConnectedDevices();
        dynamic GetConnectedDevice(int serialNumber);

//        bool IsDeviceRunning(string serialNumber);

    }
}
