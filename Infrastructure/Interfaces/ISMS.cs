﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Infrastructure.Interfaces
{
    public interface ISMS
    {
        void SendSMS(Server.Infrastructure.SMS.SMSManager.AlarmType alarmType, string serialNumber, int contactID, int countryCode, long mobileNumber, string msgBody, int retryInterval, int sensorTypeID=0);
        bool CanCreate(string port,string uid);
    }
}
