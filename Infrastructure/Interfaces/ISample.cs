﻿using Server.Infrastructure;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Interfaces
{
    public interface ISample
    {
        bool InsertSample(Sample sample);
        bool InsertBatch(List<Sample> samples);
        DateTime CustomerLastSample(int customerId);
        //DateTime LastSampleUTC(int serialNumber);
        double LastSampleValue(int serialNumber, int sensorTypeID);
        dynamic GetSensorStatistics(int serialNumber, int sensorTypeID, int startTime, int endTime);

        bool BuildSampleAggregator(int serialNumber, int sensorTypeID, int startTime, int endTime,
            ISampleAggregator aggregator);

        List<dynamic> Find(int serialNumber, int sensorTypeID);
        List<dynamic> Find(int serialNumber, int sensorTypeID, bool lastDownloadData = true, short alarmStatus = 0);

        List<Tuple<int, double>> FindReduced(int serialNumber, int sensorTypeID, int startTime, int endTime,
            int maxCount);

        List<Sample> FindPart(int serialNumber, int sensorTypeID, int startTime, int endTime, int offset = 0,
            int count = -1);

        long RowsCount(int customerId);
        byte[] Backup(int customerId, bool wipeData = false, int startTime = 0, int endTime = 0);
        bool Backup(List<int> serialNumbers);
        bool Restore(int customerId, Stream zipFile);
        bool WipeData(List<int> serialNumbers);
    }

    public interface ISampleAggregator
    {
        void Add(uint time, double value);
    }
}
