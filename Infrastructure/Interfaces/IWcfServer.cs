﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Devices;
using Base.Misc;
using Base.OpCodes;

namespace Server.Infrastructure.Interfaces
{
    public delegate void WcfClientDelegate(byte[] eBytes);


    public interface IWcfServer
    {
        void Start();
        void Execute(DeviceAction action);
        event WcfClientDelegate OnDeviceConnected;
        event WcfClientDelegate OnDeviceRemoved;
        event WcfClientDelegate OnDownloadComplete;
        event WcfClientDelegate OnOnlineSample;
        event WcfClientDelegate OnDownloadProgress;
        event WcfClientDelegate OnDeviceStatus;
        event WcfClientDelegate OnFirmwareProgress;
        event WcfClientDelegate OnStatus;
    }
}
