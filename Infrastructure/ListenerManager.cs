﻿using Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure
{
    public class ListenerManager
    {
         private static readonly Lazy<ListenerManager> lazy = new Lazy<ListenerManager>(() => new ListenerManager());

         public static ListenerManager Instance 
         { 
             get { return lazy.Value; } 
         }

         private ListenerManager() { }

        public void Start()
        {
            //get the working directory of current application
            var dir = AppDomain.CurrentDomain.BaseDirectory;
            //go over all dlls in the folder
            foreach (var f in Directory.EnumerateFiles(dir, "*Listener.dll", SearchOption.TopDirectoryOnly))
            {
                try
                {
                    //loads the dll to get assembly info
                    var assembly = Assembly.LoadFile(f);
                    //looks for instances which implement an interface of IListenerBase
                    var instances = from t in assembly.GetTypes()
                                    where t.GetInterfaces().Contains(typeof(IListener))
                                             && t.GetConstructor(Type.EmptyTypes) != null
                                    select Activator.CreateInstance(t) as IListener;

                    foreach (var instance in instances)
                    {
                        //start the listener in a new thread            
                        Task.Factory.StartNew(delegate
                        {
                            instance.Start();
                            Log4Tech.Log.Instance.Write("[Listener] loaded successfully",this, Log4Tech.Log.LogSeverity.INFO);
                        });
                    }
                }
                catch (Exception)
                {
                }
            }
            
        }
    }
}
