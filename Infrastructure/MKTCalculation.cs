﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infrastructure.Interfaces;

namespace Server.Infrastructure
{
    public class MKTCalculation : ISampleAggregator
    {
        public const double DEFAULT_MKT_ACTIVATION_ENERGY = 83.14472;
        public const double GAS_CONSTANT = 8.3144621;
        public const double ZERO_KELVIN = 273.15;

        private double _PointSum = 0;
        private ulong _TotalTime = 0;
        private uint _LastTime = 0;

        public MKTCalculation()
        {
            SampleCount = 0;
            ActivationEnergy = DEFAULT_MKT_ACTIVATION_ENERGY;
        }

        public double ActivationEnergy { get; set; }
        public int SampleCount { get; private set; }

        public double MKT
        {
            get
            {
                var denominator = -Math.Log(_PointSum / _TotalTime);
                return ((ActivationEnergy / GAS_CONSTANT) / denominator)-ZERO_KELVIN;
            }
        }

        public void Add(uint time, double value)
        {
            if (SampleCount > 0)
            {
                var timeDelta = time = _LastTime;

                var factor = -ActivationEnergy / (GAS_CONSTANT * (value + ZERO_KELVIN));
                var point = Math.Exp(factor) * timeDelta;
                _TotalTime += timeDelta;
                _PointSum += point;
            }
            _LastTime = time;
            SampleCount++;
        }
    }

}
