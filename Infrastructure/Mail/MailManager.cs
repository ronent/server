﻿using SendGridMail;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.MailService
{
    public static class MailManager
    {
        #region Fields
        private static BlockingCollection<MailMessage> queue;
        private static volatile bool dequeueProccess;

        #endregion
        #region Constructor
        static MailManager()
        {
            queue = new BlockingCollection<MailMessage>();
            dequeueProccess = true;

            try
            {
                username = ConfigurationManager.AppSettings["smtpUser"];
                password = ConfigurationManager.AppSettings["smtpPassword"];
                from = new MailAddress(ConfigurationManager.AppSettings["smtpFrom"]);
                //start to listen
                proccess();
            }
            catch (Exception ex)
            {
                throw new Exception("MailManager: Error reading from config file.", ex);
            }
        }

        #endregion
        #region Properties
        private static string username;
        private static string password;
        private static MailAddress from;

        #endregion
        #region Methods
        public static void SendEmail(MailMessage msg)
        {
            queue.Add(msg);
        }

        #endregion
        #region Private Methods
        private static void proccess()
        {
            Task.Factory.StartNew(() =>
                {
                    while (dequeueProccess)
                    {
                        //var msg = queue.Take();
                        foreach (var message in queue.GetConsumingEnumerable())
                        {
                            try
                            {
                                //message.From = from;

                                var client = new SmtpClient
                                {
                                    Port = 587,
                                    Host = "smtp.sendgrid.net",
                                    //DeliveryMethod = SmtpDeliveryMethod.Network,
                                    //UseDefaultCredentials = false,
                                    Credentials = new NetworkCredential(username, password)
                                };

                                Log4Tech.Log.Instance.Write("Process email to {0} , subject {1}, from {2}",typeof(MailManager), Log4Tech.Log.LogSeverity.DEBUG, message.To, message.Subject, message.From);

                                client.Send(message);
                            }
                            catch (Exception ex)
                            {
                                Log4Tech.Log.Instance.WriteError("Fail in process email", typeof(MailManager), ex);
                            }
                        }

                        Thread.Sleep(250);
                    }
                });
        }
        #endregion
    }
}
