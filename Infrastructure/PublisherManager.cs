﻿using System;
using System.Configuration;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Server.Infrastructure
{
    public class PublisherManager
    {
        readonly int port = 0;
        TcpClient tcpClient;
        private string IP;

        private static readonly Lazy<PublisherManager> lazy = new Lazy<PublisherManager>(() => new PublisherManager());

        public static PublisherManager Instance 
         { 
             get { return lazy.Value; } 
         }

        private PublisherManager() 
        {
            int.TryParse(ConfigurationManager.AppSettings["publisherPort"], out port);
            IP = getIp();
            tcpClient = new TcpClient();
            connectToNodeJS();
            //Log4Tech.Log.Instance.Write("Publisher port {0}", this, Log4Tech.Log.LogSeverity.DEBUG, port);
        }

        private async void connectToNodeJS()
        {
            try
            {
                await tcpClient.ConnectAsync(IP, port);
                if (!tcpClient.Connected)
                    connectToNodeJS();
            }
            catch
            {
                //ignored
            }
            
        }

        public async void Publish(string msg)
        {
            try
            {

 //               TcpClient tcpClient = new TcpClient();
 //               string ip = ConfigurationManager.AppSettings["publisherAddress"];

                //try
                //{
                //    await tcpClient.ConnectAsync(ip, port);
                //}
                //catch 
                //{ 
                //}

                if (tcpClient.Connected)
                {
                    Log4Tech.Log.Instance.Write("Publisher sending to NodeJS - {0}", this,
                        Log4Tech.Log.LogSeverity.DEBUG, msg);
                    //socket.Send(msg, Encoding.ASCII);
                    var stream = tcpClient.GetStream();

                    msg = msg.Length + "#" + msg;
                    var resp = Encoding.Default.GetBytes(msg);
                    //write response back

                    await stream.WriteAsync(resp, 0, resp.Length);
                    //string message = socket.Receive(Encoding.ASCII, new TimeSpan(1000 * 5));
                    //var bytes = new byte[tcpClient.ReceiveBufferSize];
                    //var byteCount = stream.Read(bytes, 0, tcpClient.ReceiveBufferSize);
                    //Log4Tech.Log.Instance.Write("Received reply: {0}", this, Log4Tech.Log.LogSeverity.DEBUG, System.Text.Encoding.Default.GetString(resp));
                }
                else
                {
                    tcpClient = new TcpClient();
                    connectToNodeJS();
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in Publish", this, ex);
            }
        }

        private string getIp()
        {
            var ip = ConfigurationManager.AppSettings["publisherAddress"];
            if (ip == "localhost")
                return "127.0.0.1";
            var hostEntry = Dns.GetHostEntry(ip);
            if (hostEntry.AddressList.Length > 0)
            {
                ip = hostEntry.AddressList[0].ToString();
            }
            return ip;
        }
    }
}
