﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.OpCodes;
using Base.Sensors.Samples;
using Infrastructure;
using Newtonsoft.Json;

namespace Server.Infrastructure
{
    public class PushGUIManager
    {
        public enum GUIEvents
        {
            deviceConnected,
            deviceRemoved,
            sampleAdded,
            sampleDownload,
            downloadProgress,
            FWProgress,
            alarmNotification,
            deviceChanged,
            batchAdded,
            calibrationCertificate,
            calibrationSampleAdded,
            onlineStatus,
            storageChanged,
        }

        #region public methods

        public static void SendOnlineStatus(string serialNumber, int userId)
        {
            dynamic connectedDevices = new ExpandoObject();
            connectedDevices.EventName = GUIEvents.onlineStatus.ToString();
            connectedDevices.Status = Common.DeviceApiManager.ConnectedDevices(userId) > 0;
            connectedDevices.SerialNumber = serialNumber;
            string json = JsonConvert.SerializeObject(connectedDevices);
            PublisherManager.Instance.Publish(json);
        }

        public static void SendStorageStatus(int serialNumber)
        {
            dynamic result = new ExpandoObject();
            result.EventName = GUIEvents.storageChanged.ToString();
            int customerId;
            long maxStorageAllowed;
            Common.SerialNumber2Customer.TryGetValue(serialNumber, out customerId);
            Common.MaxStorageAllowed.TryGetValue(customerId, out maxStorageAllowed);

            var total = maxStorageAllowed;
            var used = SampleManager.Instance.RowsCount(customerId);
            result.Data = new ExpandoObject();
            result.Data.total = total;
            result.Data.used = used;
            result.Data.unitsName = "Samples";
            result.Result = "OK";
        }

        public static void SendDevice(int serialNumber, GUIEvents guiEvent)
        {
            dynamic connectedDevices = new ExpandoObject();
            try
            {
                var response = DataManager.Instance.GetDevice(serialNumber);
                dynamic dynDevice = response.Device;
                connectedDevices.Device = dynDevice;
                connectedDevices.EventName = guiEvent.ToString();
                //Log4Tech.Log.Instance.Write("Get allowed users for device #{0}", this, Log4Tech.Log.LogSeverity.DEBUG, dynDevice.SerialNumber);
                //build the list of users who are allowed to view this device
                //connectedDevices.Users = DataManager.Instance.GetAllowedUsers(Convert.ToInt32(dynDevice.SerialNumber.ToString())).Users;
                string json = JsonConvert.SerializeObject(connectedDevices);
                Log4Tech.Log.Instance.Write("Push device - {0}", "Push GUI", Log4Tech.Log.LogSeverity.DEBUG, json);
                //set the object to push to GUI 
                PublisherManager.Instance.Publish(json);
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in SendDevice", "Push GUI", ex);
            }
        }

        public static void SendDownloadSample(SampleAddedEventArgs e)
        {
            var guiDisplayStatus = GUIEvents.sampleDownload;
            SendSample(e, guiDisplayStatus.ToString());
        }

         public static void SendNewSample(SampleAddedEventArgs e)
        {
            var guiDisplayStatus = GUIEvents.sampleAdded;
            SendSample(e, guiDisplayStatus.ToString());
        }

         public static void SendCalibrationSample(SampleAddedEventArgs e)
        {
            var guiDisplayStatus = GUIEvents.calibrationSampleAdded;
            SendSample(e, guiDisplayStatus.ToString());
        }

         public static void SendDownloadStatus(ProgressReportEventArgs e)
        {
            dynamic download = new System.Dynamic.ExpandoObject();
            try
            {
                if (e.Stage == eStage.Running)
                {
                    download.SerialNumber = e.SerialNumber;
                    download.Precent = e.Percent;

                    var guiDisplayStatus = GUIEvents.downloadProgress;
                    download.EventName = guiDisplayStatus.ToString();
                    //build the list of users who are allowed to view this download
                    //download.Users = DataManager.Instance.GetAllowedUsers(Convert.ToInt32(download.SerialNumber)).Users;

                    //then serilize to JSON
                    string json = JsonConvert.SerializeObject(download);
                    //Log4Tech.Log.Instance.Write("Send Download Status {0}", this, Log4Tech.Log.LogSeverity.DEBUG, json);
                    //publish the response
                    PublisherManager.Instance.Publish(json);
                }
            }
            catch(Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in SendDownloadStatus", "Push GUI", ex);
            }
        }

         public static void SendDownloadCompleteStatus(int serialNumber)
        {
            dynamic download = new System.Dynamic.ExpandoObject();
            try
            {
                download.SerialNumber = serialNumber;
                download.Precent = 100;

                var guiDisplayStatus = GUIEvents.downloadProgress;
                download.EventName = guiDisplayStatus.ToString();
                //build the list of users who are allowed to view this download
                //download.Users = DataManager.Instance.GetAllowedUsers(Convert.ToInt32(download.SerialNumber)).Users;

                //then serilize to JSON
                string json = JsonConvert.SerializeObject(download);
                //Log4Tech.Log.Instance.Write("Send Download Status {0}", this, Log4Tech.Log.LogSeverity.DEBUG, json);
                //publish the response
                PublisherManager.Instance.Publish(json);
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in SendDownloadCompleteStatus", "Push GUI", ex);
            }
        }

         public static void SendFWStatus(ProgressReportEventArgs e)
        {
            dynamic FW = new System.Dynamic.ExpandoObject();
            try
            {
                FW.SerialNumber = e.SerialNumber;
                FW.Precent = e.Percent;

                var guiDisplayStatus = ((GUIEvents)GUIEvents.FWProgress);
                FW.EventName = guiDisplayStatus.ToString();
                //build the list of users who are allowed to view this download
              //  FW.Users = DataManager.Instance.GetAllowedUsers(Convert.ToInt32(FW.SerialNumber.ToString())).Users;
                //then serilize to JSON
                string json = JsonConvert.SerializeObject(FW);
                Log4Tech.Log.Instance.Write("Send FW Status {0}", "Push GUI", Log4Tech.Log.LogSeverity.DEBUG, json);
                //publish the response
                PublisherManager.Instance.Publish(json);
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in SendFWStatus", "Push GUI", ex);
            }
        }

         public static void SendAlarm(dynamic alarm)
        {
            var guiDisplayStatus = GUIEvents.alarmNotification;
            alarm = new ExpandoObject();
            try
            {
//                if (alarm == null)
//                    alarm = new System.Dynamic.ExpandoObject();
//                else
//                    //build the list of users who are allowed to view this download
//                    alarm.Users = DataManager.Instance.GetAllowedUsers(Convert.ToInt32(alarm.SerialNumber.ToString())).Users;

                alarm.EventName = guiDisplayStatus.ToString();
                //then serilize to JSON
                string json = JsonConvert.SerializeObject(alarm);
                //Log4Tech.Log.Instance.Write("Send Alarm Notification {0}", this, Log4Tech.Log.LogSeverity.DEBUG, json);
                //publish the response
                PublisherManager.Instance.Publish(json);
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in SendAlarm", "Push GUI", ex);
            }
        }

         public static void SendBatch(string eventName, int serialNumber, int sensorTypeID, dynamic[] samples, string unit)
        {
            dynamic batch = new System.Dynamic.ExpandoObject();
            try
            {
                //get the device sensor id
                batch.DeviceSensorID = DataManager.Instance.GetDeviceSensorID(serialNumber, sensorTypeID);
                batch.SerialNumber = serialNumber;
                batch.EventName = eventName;
                //batch.Type = e.Type.ToString();

                var count = Math.Min(150, samples.Length);
                var start_pos = samples.Length - count;
                batch.Data = new dynamic[count];
                for (int index = 0; index < count; index++)
                {
                    dynamic source = samples[index + start_pos];
                    batch.Data[index] = new System.Dynamic.ExpandoObject();
                    batch.Data[index].Date = (ulong)source.SampleTime;
                    batch.Data[index].Value = source.Value;
                }

                if (unit == "°C")
                {
                    batch.Unit = "&#8451;";
                }
                else if (unit == "°F")
                {
                    batch.Unit = "&#8457;";
                }
                else
                {
                    batch.Unit = unit;
                }
                //build the list of users who are allowed to view this online sample
               // batch.Users = DataManager.Instance.GetAllowedUsers(serialNumber).Users;

                //then serilize to JSON
                string json = JsonConvert.SerializeObject(batch);
                //Log4Tech.Log.Instance.Write("Send Sample {0}",this,  Log4Tech.Log.LogSeverity.DEBUG ,  json);
                //publish the response
                PublisherManager.Instance.Publish(json);
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in SendBatch", "Push GUI", ex);
            }
        }

        #endregion

        #region private methods

        private static void SendSample(SampleAddedEventArgs e,string eventName)
        {
            dynamic sample = new System.Dynamic.ExpandoObject();
            try
            {
                //init the alarm status
                var alarmDisplayStatus = e.Sample.AlarmStatus;
                var stringValue = alarmDisplayStatus.ToString();
                sample.AlarmStatus = stringValue;
                //get the device sensor id
                var serialNumber = Convert.ToInt32(e.SerialNumber);
                sample.DeviceSensorID = DataManager.Instance.GetDeviceSensorID(serialNumber, (int)e.Type);
                sample.Comment = e.Sample.IsTimeStamp ? (e.Sample as TimeStamp).Comment : "";

                var device = Common.DeviceApiManager.GetConnectedDevice(serialNumber);
                var timeZone = 0;
                if (device != null)
                    timeZone = Convert.ToInt32(device.Device.LoggerUTC.ToString());
//                var timeZone = DataManager.Instance.GetDeviceTimeZone(int.Parse(e.SerialNumber));
                var dt = e.Sample.Date;
                if (timeZone != 0)
                    dt = dt.AddHours(-timeZone);

                sample.SampleTime = (ulong)dt.Subtract(new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds;

                sample.IsTimeStamp = e.Sample.IsTimeStamp;
                sample.Value = e.Sample.Value;
                sample.SerialNumber = serialNumber.ToString();
                sample.EventName = eventName;
                sample.Type = e.Type.ToString();
                sample.Index = e.Index;

                if (e.Unit == "°C")
                {
                    sample.Unit = "&#8451;";
                }
                else if (e.Unit == "°F")
                {
                    sample.Unit = "&#8457;";
                }
                else
                {
                    sample.Unit = e.Unit;
                }
                //build the list of users who are allowed to view this online sample
                //sample.Users = DataManager.Instance.GetAllowedUsers(Convert.ToInt32(e.SerialNumber)).Users;

                //then serilize to JSON
                string json = JsonConvert.SerializeObject(sample);
                //Log4Tech.Log.Instance.Write("Send Sample {0}",this,  Log4Tech.Log.LogSeverity.DEBUG ,  json);
                //publish the response
                PublisherManager.Instance.Publish(json);
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in SendSample", "Push GUI", ex);
            }
        }

        #endregion
    }
}
