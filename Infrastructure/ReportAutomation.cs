﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Base.Sensors.Types;
using Infrastructure;
using ReportsGenerator;
using Timer = System.Timers.Timer;

namespace Server.Infrastructure
{
    public class ReportAutomation
    {
        private static readonly int _checkInterval = 30*1000; //1*60*60*1000;//1 hour in miliseconds
        private static readonly ReportsManager _reportManager=new ReportsManager();
        private static Timer _timer;

        public static void Start()
        {
            //loads the checker
            _timer = new Timer
            {
                Interval = _checkInterval
            };
            _timer.Elapsed += timer_Elapsed;
            _timer.Start();
            Log4Tech.Log.Instance.Write("Report Automation has started", "Report Automation",
                          Log4Tech.Log.LogSeverity.INFO);
        }

        private static void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            //check if there are any reports to be generated and not yet executed
            //first get those that are marked as once and not yet executed (for some reason...)
            //and also the rest that should be executed now based on the scheduler (daily , weekly, monthly)
            _timer.Stop();
            try
            {
                var response = DataManager.Instance.GetReportsForExecution();
                if (response.Result == "OK")
                {
                    foreach (var profile in response.Data)
                    {
                        foreach (var logger in profile.loggers)
                        {
                            var from = Convert.ToInt64(profile.PeriodStart.ToString());
                            var to = Convert.ToInt64(profile.PeriodEnd.ToString());

                            Image imgHeader = null;

                            if (!string.IsNullOrEmpty(profile.ReportHeaderLink.ToString()))
                            {
                                using (var wc = new WebClient())
                                {
                                    var keyPath = string.Format("uploads/{0}", profile.ReportHeaderLink.ToString());
                                    //get the link for the url
                                    var imageUrl = S3Manager.Instance.GetReportHeaderUrl(keyPath);
                                    var bytes = wc.DownloadData(imageUrl);
                                    using (var ms = new MemoryStream(bytes))
                                    {
                                        imgHeader = Image.FromStream(ms);
                                    }
                                }
                            }

                            string dateFormat = profile.ReportDateFormat.ToString();
                            var utcOffset = profile.TimeZoneOffset.ToString();

                            var reportParams = new ReportParams(Convert.ToSByte(utcOffset),
                                Convert.ToInt32(profile.ID.ToString()))
                            {
                                //add HH:mm:ss
                                DateFormat =
                                    dateFormat.IndexOf("HH", StringComparison.Ordinal) < 0
                                        ? string.Format("{0} HH:mm:ss", dateFormat)
                                        : dateFormat,
                                HeaderImage = imgHeader,
                                PageSize = profile.PageSize.ToString() == "A4" ? PaperKind.A4 : PaperKind.Letter,
                                TemperatureUnit =
                                    Convert.ToInt32(profile.TemperatureUnit.ToString()) == 0
                                        ? eTemperatueUnit.Celsius
                                        : eTemperatueUnit.Fahrenheit
                            };
                            //execute the report
                            _reportManager.GenerateReport(logger, Convert.ToInt32(from),
                                Convert.ToInt32(to),
                                reportParams, utcOffset);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in timer_Elapsed", "Report Automation", ex);
            }
            //continue checking every interval 
            
            _timer.Start();
        }

        public static void Execute(dynamic reportProfile)
        {
            Task.Factory.StartNew(() =>
            {
                //This will execute the report right now (schedule is set to once with date range)
                //go over each device and execute a report for it
                try
                {
                    foreach (var logger in reportProfile.loggers)
                    {
                        var from = Convert.ToInt64(reportProfile.scheduleInfo.from.ToString());
                        var to = Convert.ToInt64(reportProfile.scheduleInfo.to.ToString());

                        Image imgHeader;

                        using (var wc = new WebClient())
                        {
                            byte[] bytes = wc.DownloadData(reportProfile.header.imageSrc.ToString());
                            using (var ms = new MemoryStream(bytes))
                            {
                                imgHeader = Image.FromStream(ms);
                            }
                        }

                        string dateFormat = reportProfile.formatDate.ToString();
                        var utcOffset = reportProfile.timezone.offset.ToString();

                        var reportParams = new ReportParams(Convert.ToSByte(utcOffset),
                            Convert.ToInt32(reportProfile.profileId.ToString()))
                        {
                            //add HH:mm:ss
                            DateFormat =
                                dateFormat.IndexOf("HH", StringComparison.Ordinal) < 0
                                    ? string.Format("{0} HH:mm:ss", dateFormat)
                                    : dateFormat,
                            HeaderImage = imgHeader,
                            PageSize = reportProfile.pagesize.ToString() == "A4" ? PaperKind.A4 : PaperKind.Letter,
                            TemperatureUnit =
                                reportProfile.units.ToString() == "C"
                                    ? eTemperatueUnit.Celsius
                                    : eTemperatueUnit.Fahrenheit
                        };
                        //execute the report
                        //assuming the 'from' and 'to' are in miliseconds (so convert to seconds...)
                        _reportManager.GenerateReport(logger, Convert.ToInt32(from/1000), Convert.ToInt32(to/1000),
                            reportParams, utcOffset);
                    }
                }
                catch (Exception ex)
                {
                    Log4Tech.Log.Instance.WriteError("Fail in Execute", "Report Automation", ex);
                }
            });

        }
           
    }
}
