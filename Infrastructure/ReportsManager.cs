﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Base.Devices;
using Infrastructure;
using Infrastructure.MailService;
using MicroXBase.Devices.Types;
using ReportsGenerator;
using ReportsGenerator.CalibrationDataObjects;
using ReportsGenerator.DeviceDataObjects;

namespace Server.Infrastructure
{
    public class ReportsManager
    {

        #region Properties
        public string CalibrationUrl { get; set; }
        #endregion

        #region Fields
        
        private readonly ReportGenerator report;

        #endregion

        #region Events

        public event Action OnFinishReport;

        #endregion

        #region Constructor

        public ReportsManager()
        {
            report = new ReportGenerator();
            report.OnFinished += report_OnFinished;
            ReportGenerator.OnProgress += report_OnProgress;
        }

        #endregion

        #region Methods

        public void GenerateBoomerang(GenericDevice device)
        {
            try
            {
                //assuming this is a microx device
                var microx = device as MicroXLogger;
                Log4Tech.Log.Instance.Write("checks if device has boomerang and has contacts defined", this);
                //checks if boomerang is enabled and defined
                if (microx != null && (!microx.Status.BoomerangEnabled || microx.Status.Boomerang.Contacts.Count <= 0)) return;
                //then generate the report and send the email to the recipients
                var data = new DeviceData(device);

                Log4Tech.Log.Instance.Write("Boomerang is starting on device #{0}", this,
                    Log4Tech.Log.LogSeverity.DEBUG, device.Status.SerialNumber);
                //CancellationTokenSource ct = new CancellationTokenSource();

                //if (!(device as MicroXLogger).Status.Boomerang.CelsiusMode)
                //     data.ReportParams.TemperatureUnit = eTemperatueUnit.Fahrenheit;
                //report.GenerateBommerang(data, ct);
                report.GenerateBommerang(data);
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in Generate Boomerang", this, ex);
            }
        }

        public void GenerateReport(dynamic logger, int from, int to, ReportParams reportParams, string utcOffSet)
        {
            try
            {
                var serialNumber = Convert.ToInt32(logger.SerialNumber.ToString());
                var sensors = new List<ReportSensorInfo>();

                foreach (var sensor in logger.Sensors)
                {
                    var samples = SampleManager.Instance.FindPart(serialNumber,
                        Convert.ToInt32(sensor.TypeID.ToString()), from, to);
                    var x = ((IEnumerable<dynamic>) samples);
                    sensors.Add(new ReportSensorInfo(sensor, logger, x.ToList()));
                }

                var memoryCyclic = logger.CyclicMode == 0 ? eMemoryMode.NonCyclic : eMemoryMode.Cyclic;
                //generate the report
                var data = new ReportData(sensors, logger, memoryCyclic, reportParams, utcOffSet);

                var reportSections = new List<eReportSections>();
                reportSections.Add(eReportSections.ReportProfileArgs);
                reportSections.Add(eReportSections.ReportGeneralInfoArgs);
                reportSections.Add(eReportSections.ReportSensorsArgs);
                reportSections.Add(eReportSections.ReportAlarmArgs);
                if (logger.DeviceType.ToString() == "PicoLite" || logger.DeviceType.ToString() == "PicoLite V2")
                    reportSections.Add(eReportSections.ReportEventSummaryArgs);

                reportSections.Add(eReportSections.ReportChartArgs);
                reportSections.Add(eReportSections.ReportTimeStampsArgs);
                reportSections.Add(eReportSections.ReportSamplesArgs);

                report.Generate(data, reportSections);
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GenerateReport", this, ex);
            }
        }

        public void GenerateCertificate(dynamic calibrationData)
        {
            try
            {
                var serialNumber = calibrationData.SerialNumber.ToString();
                //assuming this is a microx device
                var sensors = new List<SensorCalibration>
                {
                    new SensorCalibration
                    {
                        InputType = calibrationData.sensorData.SensorType.ToString(),
                        PointNum = "Point 1",
                        ReferenceValue = calibrationData.sensorData.Calibration.Ref1,
                        LoggerValue = calibrationData.sensorData.Calibration.Log1,
                        AcceptableTolerance = ""
                    },
                    new SensorCalibration
                    {
                        InputType = calibrationData.sensorData.SensorType.ToString(),
                        PointNum = "Point 2",
                        ReferenceValue = calibrationData.sensorData.Calibration.Ref2,
                        LoggerValue = calibrationData.sensorData.Calibration.Log2,
                        AcceptableTolerance = ""
                    }
                };

                var sensorTypeId = Convert.ToInt32(calibrationData.sensorData.TypeID.ToString());
                if (sensorTypeId == 1 || sensorTypeId > 4)
                {
                    sensors.Add(new SensorCalibration
                    {
                        InputType = calibrationData.sensorData.SensorType.ToString(),
                        PointNum = "Point 3",
                        ReferenceValue = calibrationData.sensorData.Calibration.Ref3,
                        LoggerValue = calibrationData.sensorData.Calibration.Log3,
                        AcceptableTolerance = ""
                    });
                }

                var test = new TestEquipment
                {
                    CalibrationDate =
                        new DateTime(1970, 1, 1, 0, 0, 0).AddMilliseconds(
                            Convert.ToDouble(calibrationData.certificateInfo.date)),
                    Manufacturer = calibrationData.certificateInfo.manufacturer,
                    Model = calibrationData.certificateInfo.model,
                    IsNIST = true,
                    ProductName = calibrationData.certificateInfo.productName,
                    SN = calibrationData.certificateInfo.serialNumber
                };

                var data = new CalibrationData(test, sensors)
                {
                    CalibrationDate = DateTime.Now,
                    CertificateIssueDate = DateTime.Now,
                    DeviceSN = serialNumber,
                    ModelName = calibrationData.sensorData.DeviceType.ToString(),
                    EnvironmentHumidity = calibrationData.certificateInfo.humidity,
                    EnvironmentTemperature = calibrationData.certificateInfo.temperature,
                    PartNumber = "",
                    TechnicianName = ""
                };

                var reportSections = new List<eReportSections>
                {
                    eReportSections.CalibrationInfoArgs,
                    eReportSections.CalibrationEquipmentArgs,
                    eReportSections.CalibrationDataArgs,
                    eReportSections.CalibrationSummaryArgs
                };

                report.Generate(data, reportSections);
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GenerateCertificate", this, ex);
            }
        }

        #endregion

        #region Events

        private void report_OnProgress(ReportProgressArgs args)
        {
            Log4Tech.Log.Instance.Write("Report progress {0}", this, Log4Tech.Log.LogSeverity.DEBUG, args.Precent);
        }

        private void report_OnFinished(ReportFinishArgs args)
        {
            Log4Tech.Log.Instance.Write("Report is finished on device #{0}", this, Log4Tech.Log.LogSeverity.DEBUG,
                args.SerialNumber);

            if (report.calibrationData != null)
            {
                generateCalibrationUrl(args);
                if (OnFinishReport != null)
                    OnFinishReport();
            }

            if (args.generateBoomerang)
                processBoomerangReport(args.streamPDF, args.SerialNumber);
            if (args.generateReport)
                processReport(args);
        }

        #endregion

        #region Private Methods

        private void processReport(ReportFinishArgs args)
        {
            Task.Factory.StartNew(async () =>
            {
                var keyPath = string.Format("Reports/{0}/{1}.pdf", args.SerialNumber,
                        DateTime.Now.ToString("yyyy-MM-dd.HH-mm-ss"));
                
                try
                {
                    using (var ms = new MemoryStream())
                    {
                        await args.streamPDF.CopyToAsync(ms);
                        //save the physical pdf in S3
                        S3Manager.Instance.UploadBoomerangFile(keyPath, ms);
                    }
                    
                    //save the report in database for history
                    DataManager.Instance.SaveReport(Convert.ToInt32(args.SerialNumber), keyPath, args.ReportProfileID);
                    //send the report in an email with the link to the list of contacts/ approvers/ reviewers
                    //first get the list of persons linked to this report
                    var emailList = DataManager.Instance.GetReportEmails(args.ReportProfileID);
                    if (emailList.Result == "OK")
                    {
                        var reportUrl = S3Manager.Instance.GetReportPdfUrl(keyPath);
                        var fromMail = new MailAddress(ConfigurationManager.AppSettings["fromEmail"], "SiMiO");

                        //go over and send them individually(!)
                        foreach (var reportEmail in emailList.ReportEmails)
                        {
                            var emailAsPdf = reportEmail.EmailAsPDF.ToString() == "1";
                            var emailAsLink = reportEmail.EmailAsLink.ToString() == "1";
                            //checks which process type is it: reviewer - 1, approver -2 , contact - 3
                            //should have different body for each one
                            args.streamPDF.Position = 0;

                            var attch = new Attachment(args.streamPDF, "report.pdf");

                            var bodyEmail = getReportBody(reportEmail.Name.ToString(), reportUrl, args.SerialNumber,reportEmail.PeriodTypeId.ToString()
                                ,emailAsLink, emailAsPdf);

                            var mail = new MailMessage
                            {
                                Body = bodyEmail,
                                From = fromMail,
                                Subject =
                                    string.Format("Report for logger #{0}{1}", args.SerialNumber,
                                        reportEmail.ProcessTypeId.ToString() == "1"
                                            ? ", for you to review"
                                            : reportEmail.ProcessTypeId.ToString() == "2" ? ", for you to approve" : ""),
                                To = {reportEmail.Email.ToString()},
                            };

                            mail.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(bodyEmail, null,
                                MediaTypeNames.Text.Plain));
                            mail.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(bodyEmail, null,
                                MediaTypeNames.Text.Html));

                            if (emailAsPdf)
                                mail.Attachments.Add(attch);
                            
                            MailManager.SendEmail(mail);
                        }
                    }
                }
                catch (Exception e)
                {
                    Log4Tech.Log.Instance.WriteError("Fail in processReport", this, e);
                }
            });
        }

        private void generateCalibrationUrl(ReportFinishArgs args)
        {
            var keyPath = string.Format("{0}/{1}.pdf", args.SerialNumber,
                   DateTime.Now.ToString("yyyy-MM-dd.HH-mm-ss"));
            CalibrationUrl = S3Manager.Instance.UploadCertificateFile(keyPath, args.streamPDF);
        }

        private void processBoomerangReport(Stream stream, string serialNumber)
        {
            try
            {
                Task.Factory.StartNew(async() =>
                {
                    var ms = new MemoryStream();
                    try
                    {
                        await stream.CopyToAsync(ms);
                        var keyPath = string.Format("Boomerang/{0}/{1}.pdf", serialNumber,
                            DateTime.Now.ToString("yyyy-MM-dd.HH-mm-ss"));
                        S3Manager.Instance.UploadBoomerangFile(keyPath, ms);
                        //save the report in database for history
                        DataManager.Instance.SaveReport(Convert.ToInt32(serialNumber), keyPath,0);
                    }
                    catch (Exception e)
                    {
                        Log4Tech.Log.Instance.WriteError("Fail in saveBoomerangReport", this, e);
                    }
                });
                //look for the device
                var device = Common.DeviceApiManager.GetConnectedDevice(Convert.ToInt32( serialNumber)).Device;
                //if found any
                if (device != null)
                {
                    //if has contacts then 
                    if (device.BoomerangEnabled.ToString()=="1" && device.BoomerangContacts.Count > 0)
                    {
                        Log4Tech.Log.Instance.Write("Start building the email for boomerang", this);
                        var fromMail = new MailAddress(ConfigurationManager.AppSettings["fromEmail"], "SiMiO");
                        var toList = new MailAddressCollection();
                        //go over the contacts
                        foreach(var contact in device.BoomerangContacts)
                        {
                            toList.Add(contact);
                        }
                        //set the position of the stream to begining of file
                        stream.Position = 0;
                        //save to local server
                        var pdfFile = string.Format("{0}\\report_{1}_{2}.pdf",
                            ConfigurationManager.AppSettings["pdfFolder"], serialNumber,
                            DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss"));
                        using (var file = new FileStream(pdfFile, FileMode.Create, FileAccess.Write))
                        {
                            var bytes = new byte[stream.Length];
                            stream.Read(bytes, 0, (int) stream.Length);
                            file.Write(bytes, 0, bytes.Length);
                            stream.Close();
                        }
                        //convert the stream to PDF and attach it to the mail
                        var attch = new Attachment(pdfFile); //, MediaTypeNames.Application.Pdf);
                        //build the message for the email
                        var mail = new MailMessage()
                        {
                            Body =
                                getBoomerangEmailBody(device.Name.ToString(), serialNumber,
                                    Convert.ToBoolean(device.IsInAlarm.ToString())),
                            From = fromMail,
                            Subject =
                                getBoomerangEmailSubject(device.Name.ToString(), serialNumber,
                                   Convert.ToBoolean(device.IsInAlarm.ToString())),
                            To = {toList.ToString()},
                        };
                        mail.Attachments.Add(attch);
                        Log4Tech.Log.Instance.Write("End building the email, sending the email to {0}", this,
                            Log4Tech.Log.LogSeverity.DEBUG, toList.ToString());
                        //send email with the stream as PDF
                        MailManager.SendEmail(mail);
                    }
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in processPDFReport", this, ex);
            }
        }

        private string getReportBody(string name, string url, string serialNumber, string periodtypeId, bool sendAsAttachment = true,
            bool sendAsLink = true)
        {

            return
                string.Format(
                    "<p>Dear {3},</p> <p> Here is your {4} report</p>  <p>  This report was generated and sent by SiMiO for logger #{0} {1} {2}",
                    serialNumber, sendAsAttachment ? "</p><p> Attached is a copy of this report as PDF" : "",
                    sendAsLink
                        ? string.Format("</p> <p>You can download it from here: {0} </p>", url)
                        : "</p> <p>The report was backup in the system of SiMiO </p>", name, getPeriodReportString(periodtypeId));

        }

        private string getPeriodReportString(string periodTypeId)
        {
            switch (periodTypeId)
            {
                case "1":
                    return "daily";
                case "2":
                    return "weekly";
                case "3":
                    return "monthly";
                case "4":
                    return "one time";
            }
            return string.Empty;
        }

        private string getBoomerangEmailBody(string deviceName, string serialNumber, bool hasAlarms)
        {
            //build the body html for the boomerang
            return
                string.Format(
                    "Attached is a report sent by <b>SiMiO</b> weboomerang.<br>The data is from logger '{0}' with SN: {1}.{2} ",
                    deviceName, serialNumber, hasAlarms ? "<br>Alarms were recorded by this device." : "");
        }

        private string getBoomerangEmailSubject(string deviceName, string serialNumber, bool hasAlarms)
        {
            return string.Format("weboomerang Report for logger '{0}' with SN {1}.{2}", deviceName, serialNumber,
                hasAlarms ? "(Alarms were recorded)" : "");
        }

        #endregion
    }
}
