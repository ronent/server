﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure
{
    public class Response
    {
        public string RequestID { get; set; }
        public string Result { get; set; }
        public string Command { get; set; }
        public string Entity { get; set; }
    }
}
