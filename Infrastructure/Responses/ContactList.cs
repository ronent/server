﻿using Infrastructure.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Responses
{
    public class ContactList : Response
    {
        public List<Contact> Contacts { get; set; }
    }
}
