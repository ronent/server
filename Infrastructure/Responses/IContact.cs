﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Responses
{
    public interface IContact
    {
         int ID { get; set; }
         string Name { get; set; }
         string Title { get; set; }
         string Phone { get; set; }
         string Email { get; set; }
         int WeekdayStart { get; set; }
         string WorkdayStart { get; set; }
         string WorkdayEnd { get; set; }
         int SMSResendInterval { get; set; }
         int OutOfOfficeStart { get; set; }
         int OutOfOfficeEnd { get; set; }
    }
}
