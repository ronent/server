﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Responses
{
    public class Languages : Response
    {
        public List<Language> LanguageList { get; set; }
    }

    public class Language
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
