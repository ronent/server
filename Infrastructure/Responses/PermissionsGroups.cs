﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Responses
{
    public class PermissionsGroups : Response
    {
        public List<Group> GroupList { get; set; }
    }

    public class Group
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
