﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Responses
{
    public class PostBack : Response
    {
        public string PostBackURL { get; set; }
    }
}
