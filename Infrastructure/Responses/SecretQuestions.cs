﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Responses
{
    public class SecretQuestions : Response
    {
        public List<SecureQuestion> SecureQuestionList { get; set; }
    }

    public class SecureQuestion
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }

}
