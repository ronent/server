﻿using Infrastructure.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Responses
{
    public class SensorList : Response
    {
        public List<dynamic> Sensors { get; set; }
    }

    

}
