﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Responses
{
    public class SensorLog
    {
        public double Value { get; set; }
        public int SampleTime { get; set; }
        public bool IsTimeStamp { get; set; }
        public string TimeStampComment { get; set; }
    }
}
