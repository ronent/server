﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Amazon;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using Log4Tech;
using Newtonsoft.Json;

namespace Server.Infrastructure
{
    public class S3Manager
    {
        #region properties

        private enum BucketTypes
        {
            Setups,
            Calibrations,
            Reports,
            Certificates,
            ReportTemplatesHeader,
            DataBackup
        }

        private const string Setup_Bucket = "setup.templates";
        private const string Calibration_Bucket = "calibration.templates";
        private const string Report_Bucket = "report.files";
        private const string Certificate_Bucket = "calibration.certificates";
        private const string ReportTemplatesHeader_Bucket = "report.templates.header";
        private const string Loggers_Data_Backups = "mongo.data.backups";

        #endregion

        #region Fields

        private readonly AmazonS3Client S3;
        private readonly S3Region _region = S3Region.EU;
        #endregion

        #region singleton
        private static readonly Lazy<S3Manager> lazy = new Lazy<S3Manager>(() => new S3Manager());

        public static S3Manager Instance
        {
            get { return lazy.Value; }
        }

        private S3Manager()
        {
            S3 = new AmazonS3Client(RegionEndpoint.EUWest1);
        }

        #endregion

        #region Methods

        public string UploadBoomerangFile(string keyPath, Stream data)
        {
            var requestId = uploadFile(BucketTypes.Reports, keyPath, data, "application/pdf");
            return requestId != string.Empty ? downloadUrl(BucketTypes.Certificates, keyPath) : string.Empty;
        }

        public bool UploadCalibrationFile(string keyPath, string data, string dataType = "application/json")
        {
            var response = uploadFile(BucketTypes.Calibrations, keyPath, data, dataType);
            return response.Length > 0;
        }

        public string UploadSetupFile(string keyPath, string data, string dataType = "application/json")
        {
            return uploadFile(BucketTypes.Setups, keyPath, data, dataType);
        }

        public string UploadCertificateFile(string keyPath, Stream data, string dataType = "application/pdf")
        {
            var requestId = uploadFile(BucketTypes.Certificates, keyPath, data, dataType);
            return requestId!=string.Empty ? downloadUrl(BucketTypes.Certificates, keyPath) : string.Empty;
        }

        public dynamic UploadReportTemplatesHeader(int key, Stream data, string dataType = "image/png", Int64 contentLength = 0)
        {
            dynamic response = new ExpandoObject();
            response.imageUrl = "";
            var keyPath = string.Format("uploads/{0}.{1}",key,dataType.Split('/')[1]);
            var requestId = uploadFile(BucketTypes.ReportTemplatesHeader, keyPath, data, dataType);
            
            if (requestId == string.Empty) return response;

            response.imageUrl = downloadUrl(BucketTypes.ReportTemplatesHeader, keyPath);
            return response;
        }

        public bool CopyImageLogo(int key, string dataType)
        {
            try
            {
                var sourceKey = string.Format("/uploads/{0}.{1}", key, dataType);
                var destKey = string.Format("/used/{0}.{1}", key, dataType);
                //find the file by his key path
                var copy = new CopyObjectRequest
                {
                    ContentType = dataType,
                    DestinationBucket = ReportTemplatesHeader_Bucket,
                    DestinationKey = destKey,
                    SourceBucket = ReportTemplatesHeader_Bucket,
                    SourceKey = sourceKey
                };

                var response = S3.CopyObject(copy);
                //if found then copy to /used
                if (response != null)
                {
                    //delete upon success from /uploads
                    var deleteResponse = S3.DeleteObject(ReportTemplatesHeader_Bucket, sourceKey);
                    if (deleteResponse.DeleteMarker != string.Empty)
                        return true;
                }

            }
            catch
            {
                // ignored
            }
            return false;
        }

        public List<dynamic> GetCalibrationFileList(string keyPrefix)
        {
            return downloadFiles(BucketTypes.Calibrations, keyPrefix);
        }

        public List<dynamic> GetSetupFileList(string keyPrefix)
        {
            return downloadFiles(BucketTypes.Setups, keyPrefix);
        }

        public dynamic GetCalibrationFileContent(string keyPath)
        {
            return downloadFile(BucketTypes.Calibrations, keyPath);
        }

        public dynamic GetSetupFileContent(string keyPath)
        {
           return downloadFile(BucketTypes.Setups, keyPath);
        }

        public string GetCertificateUrl(string keyPath)
        {
            return downloadUrl(BucketTypes.Certificates, keyPath);
        }

        public string GetReportPdfUrl(string keyPath)
        {
            return downloadUrl(BucketTypes.Reports, keyPath);
        }

        public string GetReportHeaderUrl(string keyPath)
        {
            return downloadUrl(BucketTypes.ReportTemplatesHeader, keyPath);
        }

        public string UploadDataBackup(string keyPath, Stream data, string dataType = "application/zip")
        {
            var requestId = uploadFile(BucketTypes.DataBackup, keyPath, data, dataType);
            return requestId != string.Empty ? downloadUrl(BucketTypes.DataBackup, keyPath) : string.Empty;
        } 
        #endregion

        #region private methods

        private string downloadUrl(BucketTypes bucketType, string keyPath)
        {
            var expiryUrlRequest = new GetPreSignedUrlRequest()
            {
                BucketName = getBucketName(bucketType),
                Key = keyPath,
                Expires = (DateTime.Now.AddDays(7))
            };
            return S3.GetPreSignedURL(expiryUrlRequest);
        }

        private List<dynamic> downloadFiles(BucketTypes bucketType, string keyPrefix)
        {
            var templateList = new List<dynamic>();
            var listResponse = S3.ListObjects(getBucketName(bucketType), keyPrefix);
            var enu = listResponse.S3Objects.GetEnumerator();
            while (enu.MoveNext())
            {
                dynamic obj = new ExpandoObject();
                var file = enu.Current;
                if (file == null) continue;

                obj.ID = file.ETag.Substring(file.ETag.IndexOf('"') + 1, file.ETag.LastIndexOf('"') - 1);
                obj.Name = file.Key.Substring(file.Key.LastIndexOf('/') + 1);
                templateList.Add(obj);
            }
            return templateList;
        }

        private dynamic downloadFile(BucketTypes bucketType, string keyPath)
        {
            try
            {
                var request = new GetObjectRequest
                {
                    BucketName = getBucketName(bucketType),
                    Key = keyPath
                };
                var response = S3.GetObject(request);
                var reader = new StreamReader(response.ResponseStream);
                var json = reader.ReadToEnd();
                return JsonConvert.DeserializeObject<ExpandoObject>(json);
            }
            catch
            {
                //ignored
            }
            return null;
        }

        private string uploadFile(BucketTypes bucketType, string keyPath, Stream data, string dataType)
        {
            try
            {
                
                var put = new PutObjectRequest
                {
                    BucketName = getBucketName(bucketType),
                    Key = keyPath,
                    CannedACL = S3CannedACL.PublicRead,
                    ContentType = dataType,
                    InputStream = data
                   // FilePath = filePath
                };
                //put.Metadata.Add("Content-Length", contentLength.ToString());
                //put.Headers.ContentLength = contentLength;
                var response = S3.PutObject(put);

                return response.ResponseMetadata.RequestId;
            }
            catch
            {
                // ignored
            }
            return string.Empty;
        }

        private string uploadFile(BucketTypes bucketType, string keyPath, string data, string dataType)
        {
            try
            {
                var response = S3.PutObject(new PutObjectRequest
                {
                    BucketName = getBucketName(bucketType),
                    Key = keyPath,
                    CannedACL = S3CannedACL.PublicRead,
                    ContentType = dataType,
                    ContentBody = data
                });
                return response.ETag.Substring(1, response.ETag.LastIndexOf('"') - 1);
            }
            catch
            {
                // ignored
            }
            return string.Empty;
        }

        private string getBucketName(BucketTypes bucketTypes)
        {
            switch (bucketTypes)
            {
                case BucketTypes.Setups:
                    return Setup_Bucket;
                case BucketTypes.Calibrations:
                    return Calibration_Bucket;
                case BucketTypes.Reports:
                    return Report_Bucket;
                case BucketTypes.Certificates:
                    return Certificate_Bucket;
                case BucketTypes.ReportTemplatesHeader:
                    return ReportTemplatesHeader_Bucket;
                case BucketTypes.DataBackup:
                    return Loggers_Data_Backups;
                default:
                    return string.Empty;
            }
        }
        #endregion
    }
}
