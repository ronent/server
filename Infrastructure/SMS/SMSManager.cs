﻿using Infrastructure;
using Server.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Server.Infrastructure.SMS
{
    public class SMSManager : ISMS
    {
        #region Delegation
        public delegate bool SMSSentDelegate(int resendInterval);
        #endregion
        #region Enum
        public enum AlarmType
        {
            Sensor,
            Battery,
        }
        public enum AlarmStatus
        {
            Alarmed,
            Normalized
        }
        #endregion
        #region Fields
        private ISMS smsComm;
        
        #endregion
        #region Singleton
        private static readonly Lazy<SMSManager> lazy = new Lazy<SMSManager>(() => new SMSManager());

        public static SMSManager Instance
        {
            get { return lazy.Value; }
        }

        private SMSManager()
        {
//            Common.DeviceApiManager.OnNotSupportedDeviceConnected += Instance_OnNotSupportedDeviceConnected;
//            Common.DeviceApiManager.OnNotSupportedDeviceRemoved += Instance_OnNotSupportedDeviceRemoved;
        }
        #endregion
        #region Events
//        private bool Instance_OnNotSupportedDeviceRemoved(object sender, global::Infrastructure.Communication.ConnectionEventArgs e)
//        {
//            Log4Tech.Log.Instance.Write("Unspported device on port {0} is removed", this, Log4Tech.Log.LogSeverity.DEBUG, e.DeviceIO.ShortId);
//            return true;
//        }
//
//        bool Instance_OnNotSupportedDeviceConnected(object sender, global::Infrastructure.Communication.ConnectionEventArgs e)
//        {
//            Log4Tech.Log.Instance.Write("UnSupported Device added #{0} on port {1}", this, Log4Tech.Log.LogSeverity.DEBUG, e.UID, e.DeviceIO.ShortId);
//            if(CanCreate(e.DeviceIO.ShortId, e.UID))
//            {
//                Log4Tech.Log.Instance.Write("Sierra Wireless Modem is detected on port {0}", this, Log4Tech.Log.LogSeverity.DEBUG, e.DeviceIO.ShortId);
//                return true;
//            }
//            Log4Tech.Log.Instance.Write("No modem is detected on port {0}", this, Log4Tech.Log.LogSeverity.DEBUG, e.DeviceIO.ShortId);
//            return false;
//        }

        #endregion
        #region ISMS Members
        public void SendSMS(AlarmType alarmType, string serialNumber, int contactID, int countryCode, long mobileNumber, string msgBody, int retryInterval, int sensorTypeID = 0)
        {
            Log4Tech.Log.Instance.Write("Gonna try to send SMS to {0}-{1}", this, Log4Tech.Log.LogSeverity.INFO, countryCode, mobileNumber);
            smsComm.SendSMS(alarmType, serialNumber, contactID, countryCode, mobileNumber, msgBody, retryInterval, sensorTypeID);
        }

        public bool CanCreate(string port, string uid)
        {
            return smsComm.CanCreate(port, uid);
        }
        #endregion
        #region loader
        public void Start()
        {
            //get the working directory of current application
            var dir = AppDomain.CurrentDomain.BaseDirectory;
            //go over all dlls in the folder
            foreach (var f in Directory.EnumerateFiles(dir, "*SMS.dll", SearchOption.TopDirectoryOnly))
            {
                try
                {
                    //loads the dll to get assembly info
                    var assembly = Assembly.LoadFile(f);
                    //looks for instances which implement an interface of IDataCommunication
                    var smsInstances = from t in assembly.GetTypes()
                                       where (t.GetInterfaces().Contains(typeof(ISMS))
                                                && t.GetConstructor(Type.EmptyTypes) != null)
                                       select Activator.CreateInstance(t) as ISMS;

                    foreach (var sms in smsInstances)
                    {
                        //assume there is only one
                        smsComm = sms;
                        Log4Tech.Log.Instance.Write("[SMS API - {0}] loaded successfully", this, Log4Tech.Log.LogSeverity.INFO, assembly.FullName);
                    }
                }
                catch (Exception ex) { Log4Tech.Log.Instance.WriteError("Fail in loadDataDll(SMSManager) ", this, ex); }
            }
        }
        #endregion
    }
}
