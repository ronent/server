﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Server.Infrastructure
{
    [BsonIgnoreExtraElements]
    public class Sample
    {
        [BsonId]
        public ObjectId _id { get; set; }
        [BsonElement("SerialNumber")]
        public Int32 SerialNumber { get; set; }
        [BsonElement("SensorTypeID")]
        public Int32 SensorTypeID { get; set; }
        [BsonElement("Value")]
        public double Value { get; set; }
        [BsonElement("SampleTime")]
        public int SampleTime { get; set; }
        [BsonElement("IsTimeStamp")]
        public bool IsTimeStamp { get; set; }
        [BsonElement("TimeStampComment")]
        public string TimeStampComment { get; set; }
        [BsonElement("AlarmStatus")]
        public Int16 AlarmStatus { get; set; }
        [BsonElement("IsDummy")]
        public Int16 IsDummy { get; set; }
        [BsonElement("CustomerID")]
        public int CustomerID { get; set; }

        public dynamic ToDynamic()
        {
            dynamic sample = new System.Dynamic.ExpandoObject();
            sample.SerialNumber = SerialNumber;
            sample.SensorTypeID = SensorTypeID;
            sample.Value = Value;
            sample.SampleTime = (UInt64)SampleTime;
            sample.IsTimeStamp = IsTimeStamp;
            sample.AlarmStatus = AlarmStatus;
            sample.IsDummy = IsDummy;
            sample.MeasurementUnit = "C";
            sample.CustomerID = CustomerID;
            return sample;
        }
    }
}
