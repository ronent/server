﻿using Infrastructure.Interfaces;
using Server.Infrastructure;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure
{
    public class SampleManager : ISample
    {
        #region fields
        private ISample sampleComm;
        #endregion
        #region Constructor
        private SampleManager()
        {
            loadDataDll();
        }

        private static readonly Lazy<SampleManager> lazy = new Lazy<SampleManager>(() => new SampleManager());

        public static SampleManager Instance
        {
            get { return lazy.Value; }
        }
        #endregion
        #region ISample Members
        public bool InsertBatch(List<Sample> samples)
        {
            return sampleComm.InsertBatch(samples);
        }

        public bool InsertSample(Sample sample)
        {
            return sampleComm.InsertSample(sample);
        }

        public DateTime CustomerLastSample(int customerId)
        {
            return sampleComm.CustomerLastSample(customerId);
        }

//        public DateTime LastSampleUTC(int serialNumber)
//        {
//            return sampleComm.LastSampleUTC(serialNumber);
//        }

        public List<dynamic> Find(int serialNumber, int sensorTypeID)
        {
            return sampleComm.Find(serialNumber, sensorTypeID);
        }

        public List<dynamic> Find(int serialNumber, int sensorTypeID, bool lastDownloadData = true, short alarmStatus = 0)
        {
            return sampleComm.Find(serialNumber, sensorTypeID, lastDownloadData, alarmStatus);
        }

        public List<Tuple<int, double>> FindReduced(int serialNumber, int sensorTypeID, int startTime, int endTime, int maxCount)
        {
            return sampleComm.FindReduced(serialNumber, sensorTypeID, startTime, endTime, maxCount);
        }

        public List<Sample> FindPart(int serialNumber, int sensorTypeID, int startTime, int endTime, int offset = 0, int count = -1)
        {
            return sampleComm.FindPart(serialNumber, sensorTypeID, startTime, endTime, offset, count);
        }

        public bool BuildSampleAggregator(int serialNumber, int sensorTypeID, int startTime, int endTime, ISampleAggregator aggregator)
        {
            return sampleComm.BuildSampleAggregator(serialNumber, sensorTypeID, startTime, endTime, aggregator);
        }
        
        public dynamic GetSensorStatistics(int serialNumber, int sensorTypeID, int startTime, int endTime)
        {
            return sampleComm.GetSensorStatistics(serialNumber, sensorTypeID, startTime, endTime);
        }

        public byte[] Backup(int customerId, bool wipeData = false, int startTime = 0, int endTime = 0)
        {
            return sampleComm.Backup(customerId, wipeData, startTime, endTime);
        }

        public bool Restore(int customerId, Stream zipFile)
        {
            return sampleComm.Restore(customerId,zipFile);
        }

        public long RowsCount(int customerId)
        {
            return sampleComm.RowsCount(customerId);
        }

        public bool Backup(List<int> serialNumbers)
        {
            return sampleComm.Backup(serialNumbers);
        }

        public bool WipeData(List<int> serialNumbers)
        {
            return sampleComm.WipeData(serialNumbers);
        }

        public double LastSampleValue(int serialNumber, int sensorTypeID)
        {
            return sampleComm.LastSampleValue(serialNumber, sensorTypeID);
        }
        #endregion
        #region loader
        private void loadDataDll()
        {
            //get the working directory of current application
            var dir = AppDomain.CurrentDomain.BaseDirectory;
            //go over all dlls in the folder
            foreach (var f in Directory.EnumerateFiles(dir, "*Storage.dll", SearchOption.TopDirectoryOnly))
            {
                try
                {
                    //loads the dll to get assembly info
                    var assembly = Assembly.LoadFile(f);
                    //looks for instances which implement an interface of IDataCommunication
                    var deviceInstances = from t in assembly.GetTypes()
                                          where (t.GetInterfaces().Contains(typeof(ISample))
                                                   && t.GetConstructor(Type.EmptyTypes) != null)
                                          select Activator.CreateInstance(t) as ISample;

                    foreach (var device in deviceInstances)
                    {
                        //assume there is only one
                        sampleComm = device;
                        Log4Tech.Log.Instance.Write("[Samples Storage] loaded successfully", this, Log4Tech.Log.LogSeverity.INFO);
                    }
                }
                catch (Exception ex) { Log4Tech.Log.Instance.WriteError("Fail in loadDataDll(DeviceManager) ", this, ex); }
            }
        }
        #endregion
    }
}
