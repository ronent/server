﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Infrastructure
{
    public class SensorSample
	{
        public int SerialNumber { get; set; }
        public int SensorTypeID { get; set; }
        public double Value { get; set; }
        public int SampleTime { get; set; }
        public bool IsTimeStamp { get; set; }
        public string TimeStampComment { get; set; }
        public short AlarmStatus { get; set; }
        public short IsDummy { get; set; }
        public string SensorName { get; set; }
        public int CustomerID { get; set; }
	} 
}
