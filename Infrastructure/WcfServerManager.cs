﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Base.OpCodes;
using Infrastructure.Communication.Modules;
using Server.Infrastructure.Interfaces;

namespace Server.Infrastructure
{
    public class WcfServerManager
    {
        public IWcfServer Server { get; private set; }

        public  void Start()
        {
            loadDataDll();
            Server.Start();
        }

        private  void loadDataDll()
        {
            //get the working directory of current application
            var dir = AppDomain.CurrentDomain.BaseDirectory;
            //go over all dlls in the folder
            foreach (var f in Directory.EnumerateFiles(dir, "*Communication.dll", SearchOption.TopDirectoryOnly))
            {
                try
                {
                    //loads the dll to get assembly info
                    var assembly = Assembly.LoadFile(f);
                    //looks for instances which implement an interface of IDataCommunication
                    var deviceInstances = from t in assembly.GetTypes()
                                          where (t.GetInterfaces().Contains(typeof(IWcfServer))
                                                   && t.GetConstructor(Type.EmptyTypes) != null)
                                          select Activator.CreateInstance(t) as IWcfServer;

                    foreach (var device in deviceInstances)
                    {
                        //assume there is only one
                        Server = device;
                        Log4Tech.Log.Instance.Write("[Wcf Server] loaded successfully", this, Log4Tech.Log.LogSeverity.INFO);
                    }
                }
                catch (Exception ex)
                {
                    Log4Tech.Log.Instance.WriteError("Fail in loadDataDll(WcfServerManager) ", this, ex);
                }
            }
        }
    }
}
