﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace LicenseAPI
{
    internal class Encryptor
    {

        // This constant string is used as a "salt" value for the PasswordDeriveBytes function calls.
        // This size of the IV (in bytes) must = (keysize / 8).  Default keysize is 256, so the IV must be
        // 32 bytes long.  Using a 16 character string here gives us 32 bytes when converted to a byte array.
        private static readonly byte[] initVectorBytes = Encoding.ASCII.GetBytes("tu89geji340t89u2");
        // This constant is used to determine the keysize of the encryption algorithm.
        private const int keysize = 256;
        private string passPhrase = "F0u4t3c2oq4L8c3n2e2014D3moT3s5!@";

        public Encryptor()
        {

        }

        public byte[] Encrypt(string plainText)
        {
            try
            {
                // Check arguments. 
                if (plainText == null || plainText.Length <= 0)
                    throw new ArgumentNullException("plainText");

                byte[] encrypted;
                // Create an RijndaelManaged object 
                // with the specified key and IV. 
                using (RijndaelManaged rijAlg = new RijndaelManaged())
                {
                    rijAlg.Key = Encoding.ASCII.GetBytes(passPhrase);
                    rijAlg.IV = initVectorBytes;

                    // Create a decrytor to perform the stream transform.
                    ICryptoTransform encryptor = rijAlg.CreateEncryptor(rijAlg.Key, rijAlg.IV);

                    // Create the streams used for encryption. 
                    using (MemoryStream msEncrypt = new MemoryStream())
                    {
                        using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                        {
                            using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                            {

                                //Write all data to the stream.
                                swEncrypt.Write(plainText);
                            }
                            encrypted = msEncrypt.ToArray();
                        }
                    }
                }
                // Return the encrypted bytes from the memory stream. 
                return encrypted;
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in Encrypt", this, ex);
            }
            return null;
        }

        public string Decrypt(byte[] cipherText)
        {
            try
            {
                // Check arguments. 
                if (cipherText == null || cipherText.Length <= 0)
                    throw new ArgumentNullException("cipherText");

                // Declare the string used to hold 
                // the decrypted text. 
                string plaintext = null;

                // Create an RijndaelManaged object 
                // with the specified key and IV. 
                using (RijndaelManaged rijAlg = new RijndaelManaged())
                {
                    rijAlg.Key = Encoding.ASCII.GetBytes(passPhrase);
                    rijAlg.IV = initVectorBytes;

                    // Create a decrytor to perform the stream transform.
                    ICryptoTransform decryptor = rijAlg.CreateDecryptor(rijAlg.Key, rijAlg.IV);

                    // Create the streams used for decryption. 
                    using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                    {
                        using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                        {
                            using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                            {
                                // Read the decrypted bytes from the decrypting stream 
                                // and place them in a string.
                                plaintext = srDecrypt.ReadToEnd();
                                // return getBytes(plaintext);
                            }
                        }
                        //return msDecrypt.ToArray();
                    }

                }

                return plaintext;
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in Decrypt", this, ex);
            }
            return null;
        }
    }
}
