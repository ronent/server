﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LicenseAPI
{
    interface ILicenseAPI
    {
        string CreateLicense(LicenseDefintion licenseDefinition);
        LicenseDefintion ReadLicense(string licensePath);
        string ExtendLicense(string licensePath, DateTime extendedDate);
        bool IsLicenseExpired(string licensePath);

    }
}
