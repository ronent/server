﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LicenseAPI
{
     [Serializable]
    public enum Modules
    {
        DataNet,
        DaqLink,
        USBLoggers,
    }

    [Serializable]
    public class LicenseDefintion
    {
        public List<Modules> RegisteredModules { get; set; }
        public bool UseCFR { get; set; }
        public int MaxUsers { get; set; }
        public DateTime ExpiredDate { get; set; }
        public string ClientIP { get; set; }
    }
}
