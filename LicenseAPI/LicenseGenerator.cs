﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

using Log4Tech;

namespace LicenseAPI
{
    internal class LicenseGenerator
    {
        private Encryptor encrpytor = new Encryptor();
        private const string LICENSE_FILE_NAME = "license.lic";

        public LicenseGenerator()
        {
        }

        #region Methods
        public string EncryptLicense(LicenseDefintion licenseDefinition)
        {
            return getEncryptedLicense(licenseDefinition);
        }

        public LicenseDefintion DecryptLicense(string licensePath)
        {
            return getDecryptedLicense(licensePath);
        }

        public bool IsLicenseExpired(string licensePath)
        {
            Log4Tech.Log.Instance.Write("Try to decrypt the license", this);
            LicenseDefintion licDef = getDecryptedLicense(licensePath);
            if (licDef != null)
            {
                Log4Tech.Log.Instance.Write("Decryption was ok, check if not expired", this);
                if(Convert.ToInt32(licDef.ExpiredDate.Subtract(DateTime.Now).TotalSeconds) > 0)
                {
                    //get the local ip
                    string localIP = getLocalIPAddress();
                    Log4Tech.Log.Instance.Write("Local IP is {0}", this, Log4Tech.Log.LogSeverity.DEBUG, localIP);
                    if (licDef.ClientIP == localIP)
                        return false;
                }
            }
            Log4Tech.Log.Instance.Write("Decryption failed", this);
            return true;
        }

        public string ExtendLicense(string licensePath, DateTime extendedDate)
        {
            LicenseDefintion licDef = getDecryptedLicense(licensePath);
            if (licDef != null)
            {
                //checks if the given date is higher than the current license date
                if(licDef.ExpiredDate.Subtract(extendedDate).TotalSeconds<0)
                {
                    //then update the license
                    licDef.ExpiredDate = extendedDate;
                    //save the license encrypted
                    return getEncryptedLicense(licDef);
                }
            }
            return string.Empty;
        }
        #endregion
        #region private methods
        private string getLocalIPAddress()
        {
            try
            {
                IPHostEntry host;
                string localIP = "";
                host = Dns.GetHostEntry(Dns.GetHostName());
                foreach (IPAddress ip in host.AddressList)
                {
                    if (ip.AddressFamily == AddressFamily.InterNetwork)
                    {
                        localIP = ip.ToString();
                        break;
                    }
                }
                return localIP;
            }
            catch
            {
                return string.Empty;
            }
        }

        private string getEncryptedLicense(LicenseDefintion licenseDefinition)
        {
            //serialize the license defintion
            var serilizedLicense = Utilities.serializeObject<LicenseDefintion>(licenseDefinition);
            var str = System.Text.Encoding.Default.GetString(serilizedLicense);
            //encrypt it
            var encryptedLicense = encrpytor.Encrypt(str);
            //create license file , return the full path of the license file
            return createLicenseFile(encryptedLicense);
        }

        private LicenseDefintion getDecryptedLicense(string licensePath)
        {
            //open and read the file
            var cipherText = getCipherText(licensePath);
            //try to decrypt it
            var decryptedText = encrpytor.Decrypt(cipherText);
            //return the object of the license defintion
            return (LicenseDefintion)Utilities.deserializeString(decryptedText);
        }

        private string createLicenseFile(byte[] encryptedLicense)
        {
            try
            {
                var fullPath = string.Concat( System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), "\\" + LICENSE_FILE_NAME);
                File.WriteAllBytes(fullPath, encryptedLicense);
                return fullPath;
            }
            catch
            {

            }
            return string.Empty;
        }

        private byte[] getCipherText(string licensePath)
        {
            try
            {
                return File.ReadAllBytes(licensePath);
                
            }
            catch(Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in getCipherText", this, ex);
            }
            return null;
        }
        
        #endregion
    }
}
