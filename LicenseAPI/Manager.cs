﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LicenseAPI
{
    

    public class Manager : ILicenseAPI  
    {
        private LicenseGenerator generator;

        public Manager()
        {
            generator = new LicenseGenerator();
        }

        #region ILicenseAPI Members
        /// <summary>
        /// Create license file with the license defintion object
        /// </summary>
        /// <param name="licenseDefinition"></param>
        /// <returns>The full path for the license file (runtime directory)</returns>
        public string CreateLicense(LicenseDefintion licenseDefinition)
        {
            return generator.EncryptLicense(licenseDefinition);
        }

        /// <summary>
        /// Reads the encrypted license and try to decrypt it
        /// </summary>
        /// <param name="licensePath"></param>
        /// <returns>On success return the object of the license</returns>
        public LicenseDefintion ReadLicense(string licensePath)
        {
            return generator.DecryptLicense(licensePath);
        }

        /// <summary>
        /// On given license path, decrypt it and extend his expired date
        /// </summary>
        /// <param name="licensePath"></param>
        /// <param name="extendDate"></param>
        /// <returns>On success return the full license path</returns>
        public string ExtendLicense(string licensePath, DateTime extendedDate)
        {
            return generator.ExtendLicense(licensePath, extendedDate);
        }

        /// <summary>
        /// Checks if the license has expired
        /// </summary>
        /// <param name="licensePath"></param>
        /// <returns>True- expired</returns>
        public bool IsLicenseExpired(string licensePath)
        {
            return false;
            //return generator.IsLicenseExpired(licensePath);
        }

        #endregion
    }
}
