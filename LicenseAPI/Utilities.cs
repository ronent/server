﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace LicenseAPI
{
    internal class Utilities
    {
        public static byte[] getBytes(string str)
        {
            //byte[] bytes = new byte[str.Length * sizeof(char)];
            //System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            //return bytes;
            return Encoding.Default.GetBytes(str);
        }

        public static byte[] getBytes(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        public static byte[] serializeObject<T>(T toSerialize)
        {
            try
            {
                using (MemoryStream stream = new MemoryStream())
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    formatter.Serialize(stream, toSerialize);
                    return stream.ToArray();
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in serializeObject", "LicenseAPI", ex);
            }
            return null;
        }

        /*public static MemoryStream SerializeToStream(object o)
        {
            MemoryStream stream = new MemoryStream();
            IFormatter formatter = new BinaryFormatter();
            formatter.Serialize(stream, o);
            return stream;
        }

        public static object DeserializeFromStream(MemoryStream stream)
        {
            IFormatter formatter = new BinaryFormatter();
            stream.Seek(0, SeekOrigin.Begin);
            object o = formatter.Deserialize(stream);
            return o;
        }*/

        public static object deserializeString(string decryptedText)
        {
            try
            {
                //Log4Tech.Log.Instance.Write("decrypted text - {0}", "LicenseAPI", Log4Tech.Log.LogSeverity.DEBUG, decryptedText);
                var b = Utilities.getBytes(decryptedText);
                using (MemoryStream ms = new MemoryStream(b))
                {
                    IFormatter formatter = new BinaryFormatter();
                    ms.Seek(0, SeekOrigin.Begin);
                    object o = formatter.Deserialize(ms);
                    return o;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in deserializeString", "LicenseAPI",ex);
            }
            return null;
        }
    }
}
