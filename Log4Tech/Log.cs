﻿using Loggly;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Log4Tech
{
    public class Log
    {
        public event MessageLoggedDelegate OnEventLogged;
        #region Fields
        private LogLoggly logger;
        public enum LogSeverity
        {
            DEBUG,
            INFO,
            WARNING,
            ERROR
        }
        public bool IsWrite2Cloud { get; set; }
        public bool IsWrite2Console { get; set; }
        public bool IsWrite2File { get; set; }
        public bool TakeNetworkTime { get; set; }
        public string LogFolderPath { get; set; }
        public string LogFolder { get { return LogFolderPath.Replace("\\", @"\"); } }
        public string ApplicationTag { get; set; }
        
        #endregion
        #region Constructor
        private Log()
        {
            initialize();
        }

        private static readonly Lazy<Log> lazy = new Lazy<Log>(() => new Log());

        public static Log Instance
        {
            get { return lazy.Value; }
        }
        #endregion
        #region Methods
        [DebuggerStepThrough]
        public void WriteError(string msg, object sender, Exception ex, params object[] args)
        {
            var msgWrite = writeText(msg, ex, args);
            if (IsWrite2Cloud)
                logger.LogError(String.Format(msg, args), applicationTag(sender), ex);
            if (IsWrite2Console)
                write2Console(msgWrite);
            if (IsWrite2File)
                Write2File(msgWrite, LogSeverity.ERROR);

            if (OnEventLogged != null)
                OnEventLogged.BeginInvoke(typeof(Log), msgWrite, null, null);
        }

        [DebuggerStepThrough]
        public void Write(string msg, object sender, LogSeverity logSeverity = LogSeverity.DEBUG, params object[] args)
        {
            var msgWrite = writeText(msg, logSeverity, args);
            if ((bool)IsWrite2Cloud)
            {
                switch (logSeverity)
                {
                    case LogSeverity.DEBUG:
                        //logger.Log(String.Format(msg, args), applicationTag(sender));
                        break;
                    case LogSeverity.INFO:
                        logger.LogInfo(String.Format(msg, args), applicationTag(sender));
                        break;
                    case LogSeverity.WARNING:
                        logger.LogWarning(String.Format(msg, args), applicationTag(sender));
                        break;
                }
            }
            if (IsWrite2Console)
                write2Console(msgWrite);
            if (IsWrite2File)
                Write2File(msgWrite, logSeverity);

            if (OnEventLogged != null)
                OnEventLogged.BeginInvoke(typeof(Log), msgWrite, null, null);
        }

        #endregion
        #region private methods
        private void initialize()
        {
            LogglyConfiguration.Configure(c => c.AuthenticateWith("weboomerang", "Fourtec2013"));
            logger = new LogLoggly("7a4d9aef-4978-4a11-8cf4-e4cd6d4c4521");
            checkLogFolderPermission();
            ApplicationTag = string.Empty;
            
        }

        private string writeText(string msg, LogSeverity logSeverity, params object[] args)
        {
            return String.Concat(" [", logSeverity, "] ", String.Format(msg, args));
        }

        private string writeText(string msg, Exception ex, params object[] args)
        {
            var loaderExceptions = string.Empty;
            var exception = ex as ReflectionTypeLoadException;
            if (exception == null)
                return string.Concat(" [ERROR] ", string.Format(msg, args), " ", ex.Message, " ", ex.StackTrace,
                    loaderExceptions);
            loaderExceptions = "\n loader exception ->";
            var typeLoadException = exception;
            loaderExceptions = string.Concat(loaderExceptions, typeLoadException.LoaderExceptions[0].Message , " source -> ", typeLoadException.LoaderExceptions[0].Source , " stack trace -> ", typeLoadException.LoaderExceptions[0].StackTrace);
            return string.Concat(" [ERROR] ", string.Format(msg, args), " ", ex.Message, " ", ex.StackTrace , loaderExceptions);
        }

        private string applicationTag(object sender)
        {
            if (ApplicationTag.Length > 0)
                return ApplicationTag;
            else
                return sender.GetType().Assembly.FullName.Substring(0, sender.GetType().Assembly.FullName.IndexOf(","));
        }

        private void checkLogFolderPermission()
        {
            IsWrite2File = true;
            //checks if a folder was defined for log
            if (String.IsNullOrEmpty(LogFolderPath))
            {
                LogFolderPath = Path.GetDirectoryName(Assembly.GetAssembly(GetType()).Location) + "\\Logs";
            }
            //checks if the folder not exists then create it
            if (!Directory.Exists(LogFolderPath))
            {
                try
                {
                    Directory.CreateDirectory(LogFolderPath);
                }
                catch
                {
                    //if fails to create then do not write to a file
                    IsWrite2File = false;
                }
            }
            if (IsWrite2File)
            {
                LogFile.Instance.LogFolder = LogFolderPath;
            }
        }

        private void write2Console(string text)
        {
            //add timestamp to the message in front
            text = string.Format("{0} {1}",DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.ffff"), text);
            Console.WriteLine(text);
            System.Diagnostics.Debug.WriteLine(text);
        }

        bool firstTime = true;
        private void Write2File(string text,LogSeverity level)
        {
            if (firstTime)
            {
                firstTime = false;
                LogFile.Instance.LogFolder = this.LogFolder;
            }
            if (IsWrite2File)
            {
                LogFile.Instance.Write(text, level);
            }
        }
        #endregion
    }
}
