﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using NLog.Config;
using NLog.Targets;
using System.Threading;

namespace Log4Tech
{
    internal class LogFile
    {
        #region fields
        private Logger logFile;
        public string LogFolder { get; set; }
       
        #endregion
        #region constructor
        private static readonly Lazy<LogFile> lazy = new Lazy<LogFile>(() => new LogFile());

        public static LogFile Instance
        {
            get { return lazy.Value; }
        }

        private LogFile() { }

        #endregion
        #region methods
        internal void Write(string msg, Log4Tech.Log.LogSeverity level)
        {
            writeToLog(msg, level);
        }
        #endregion
        #region private methods
        
        private void writeToLog(string msg, Log4Tech.Log.LogSeverity level)
        {
            //ReaderWriterLockSlim l = new ReaderWriterLockSlim();
            // l.EnterWriteLock();
            try
            {
                if (logFile == null)
                    initialize();

                switch (level)
                {
                    case Log.LogSeverity.DEBUG:
                        logFile.Debug(msg);
                        break;
                    case Log.LogSeverity.INFO:
                        logFile.Info(msg);
                        break;
                    case Log.LogSeverity.WARNING:
                        logFile.Warn(msg);
                        break;
                    case Log.LogSeverity.ERROR:
                        logFile.Error(msg);
                        break;
                    default:
                        logFile.Trace(msg);
                        break;
                }
            }
            finally
            {
               // l.ExitWriteLock();
            }
        }

        private void initialize()
        {
            LogManager.Configuration = new LoggingConfiguration();

            if (String.IsNullOrEmpty(LogFolder))
                LogFolder = "${basedir}/logs";

            var wrappTarget = new NLog.Targets.FileTarget()
            {
                Layout = "${longdate} ${message}",
                FileName = LogFolder + "/log.txt",
                ArchiveFileName = LogFolder + "/archives/log.{#}.txt",
                ArchiveEvery = FileArchivePeriod.Day,
                ArchiveNumbering = ArchiveNumberingMode.Rolling,
                ArchiveAboveSize = 10485760,
                CreateDirs = true,
                DeleteOldFileOnStartup = false,
                //MaxArchiveFiles = 7,
                ConcurrentWrites = true,
                KeepFileOpen = false,
                Encoding = System.Text.Encoding.ASCII,
            };

            var fileTarget = new NLog.Targets.Wrappers.AsyncTargetWrapper()
            {
                Name = "File",
                QueueLimit = 5000,
                OverflowAction = NLog.Targets.Wrappers.AsyncTargetWrapperOverflowAction.Discard,
                 WrappedTarget = wrappTarget
            };
            var fileRule = new LoggingRule("*", LogLevel.Trace, fileTarget);
            LogManager.Configuration.AddTarget("file", fileTarget);
            LogManager.Configuration.LoggingRules.Add(fileRule);

           /* var syslogTarget = new NLog.Targets.Syslog()
            {
                Layout = "${longdate} ${message}",
                Port = 27249,
                Ssl = true,
                SyslogServer = "logs.papertrailapp.com",
                //Protocol = System.Net.Sockets.ProtocolType.Tcp,
                //  Facility = SyslogFacility.
            };

            var syslogRule = new LoggingRule("*", LogLevel.Trace, syslogTarget);
            LogManager.Configuration.AddTarget("syslog", syslogTarget);
            LogManager.Configuration.LoggingRules.Add(syslogRule);*/

            LogManager.ReconfigExistingLoggers();
            logFile = LogManager.GetCurrentClassLogger();
        }
        #endregion
    }
}
