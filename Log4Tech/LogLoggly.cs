using System;
using System.Collections.Generic;
using System.Threading;
using Loggly.Responses;
using Newtonsoft.Json;
using System.Net;
using System.Linq;
using System.Net.Sockets;
using System.IO;

namespace Loggly
{
    public class LogLoggly : ILogger, IRequestContext
    {
        #region Fields
        private string _url = "logs-01.loggly.com/";
        private readonly string TIMEFORMAT = "yyyy-MM-ddTHH:mm:ss.ffffZ";
        private readonly string _inputKey;
        private string _tags;
        private string _externalIP;
        private bool _takeNetworkTime;
        #endregion
        #region Constructor
        public LogLoggly(string inputKey, string alternativeUrl = null, bool takeNetworkTime=false)
        {
            if (!string.IsNullOrEmpty(alternativeUrl))
                _url = alternativeUrl;

            _inputKey = inputKey;
            _externalIP = getExternalIP().Trim();
            _takeNetworkTime = takeNetworkTime;
        }
        #endregion
        #region Methods
        public LogResponse LogSync(string message)
        {
            return LogSync(message, false);
        }

        public void Log(string message)
        {
            Log(message, false);
        }

        public void Log(string message, Action<LogResponse> callback)
        {
            Log(message, false, callback);
        }

        public string Url
        {
            get { return _url; }
        }

        public LogResponse LogSync(string message, bool json)
        {
            var synchronizer = new AutoResetEvent(false);

            LogResponse response = null;
            Log(message, json, r =>
            {
                response = r;
                synchronizer.Set();
            });

            synchronizer.WaitOne();
            return response;
        }

        public void Log(string message, string tags)
        {
            _tags = tags;
            Log(message, "debug",null);
        }

        public void Log(string message, string category, IDictionary<string, object> data)
        {
            var logEntry = new Dictionary<string, object>(data ?? new Dictionary<string, object>())
                        {
                            {"timestamp", getUTCTimeStamp()},   
                            {"message", message}, {"category", category}, {"IP", _externalIP}
                        };
            var jsonLogEntry = JsonConvert.SerializeObject(logEntry);
            Log(jsonLogEntry, true);
        }

        public void LogInfo(string message,string tags)
        {
            _tags = tags;
            Log(message,"info", null);
        }

        public void LogInfo(string message, IDictionary<string, object> data)
        {
            Log(message, "info", data);
        }

        public void LogWarning(string message, string tags)
        {
            _tags = tags;
            Log(message,"warning", null);
        }

        public void LogWarning(string message, IDictionary<string, object> data)
        {
            Log(message, "warning", data);
        }

        public void LogError(string message,string tags, Exception ex)
        {
            _tags = tags;
            LogError(message, ex, null);
        }

        public void LogError(string message, Exception ex, IDictionary<string, object> data)
        {
            var exceptionData = new Dictionary<string, object>(data ?? new Dictionary<string, object>())
                             {
                                {"Exception", ex.ToString()},
                                {"Exception Stack", ex.StackTrace}
                             };
            Log(message, "error", exceptionData);
        }

        public void Log(string message, bool json)
        {
            Log(message, json, null);
        }

        public void Log(string message, bool json, Action<LogResponse> callback)
        {
            var communicator = new Communicator(this);
            var callbackWrapper = callback == null ? (Action<Response>)null : r =>
            {
                if (r.Success)
                {
                    var res = JsonConvert.DeserializeObject<LogResponse>(r.Raw);
                    res.Success = true;
                    callback(res);
                }
                else
                {
                    var res = new LogResponse { Success = false };
                    callback(res);
                }
            };
            communicator.SendPayload(Communicator.POST, string.Concat("inputs/", _inputKey,"/tag/",_tags), message, json, callbackWrapper);
        }
        #endregion
        #region private methods
        private string getUTCTimeStamp()
        {
            if (_takeNetworkTime)
                return GetNetworkTime().ToString(TIMEFORMAT);
            else
                return DateTime.Now.ToUniversalTime().ToString(TIMEFORMAT);
        }

        private DateTime GetNetworkTime()
        {
            //default Windows time server
            const string ntpServer = "time.windows.com";
            // NTP message size - 16 bytes of the digest (RFC 2030)
            var ntpData = new byte[48];
            //Setting the Leap Indicator, Version Number and Mode values
            ntpData[0] = 0x1B; //LI = 0 (no warning), VN = 3 (IPv4 only), Mode = 3 (Client Mode)

            try
            {
                var addresses = Dns.GetHostEntry(ntpServer).AddressList;

                //The UDP port number assigned to NTP is 123
                var ipEndPoint = new IPEndPoint(addresses[0], 123);
                //NTP uses UDP
                var socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

                socket.Connect(ipEndPoint);

                //Stops code hang if NTP is blocked
                socket.ReceiveTimeout = 5000;

                socket.Send(ntpData);
                socket.Receive(ntpData);
                socket.Close();

                //Offset to get to the "Transmit Timestamp" field (time at which the reply 
                //departed the server for the client, in 64-bit timestamp format."
                const byte serverReplyTime = 40;

                //Get the seconds part
                ulong intPart = BitConverter.ToUInt32(ntpData, serverReplyTime);

                //Get the seconds fraction
                ulong fractPart = BitConverter.ToUInt32(ntpData, serverReplyTime + 4);

                //Convert From big-endian to little-endian
                intPart = SwapEndianness(intPart);
                fractPart = SwapEndianness(fractPart);

                var milliseconds = (intPart * 1000) + ((fractPart * 1000) / 0x100000000L);

                //**UTC** time
                var networkDateTime = (new DateTime(1900, 1, 1, 0, 0, 0, DateTimeKind.Utc)).AddMilliseconds((long)milliseconds);

                // return networkDateTime.ToLocalTime();
                return networkDateTime;
            }
            catch
            {

            }
            return DateTime.Now.ToUniversalTime();
        }

        // stackoverflow.com/a/3294698/162671
        private uint SwapEndianness(ulong x)
        {
            return (uint)(((x & 0x000000ff) << 24) +
                           ((x & 0x0000ff00) << 8) +
                           ((x & 0x00ff0000) >> 8) +
                           ((x & 0xff000000) >> 24));
        }

        private DateTime GetFastestNISTDate()
        {
            var result = DateTime.Now.ToUniversalTime();

            // Initialize the list of NIST time servers
            // http://tf.nist.gov/tf-cgi/servers.cgi
            string[] servers = new string[] {
                    "nist1-ny.ustiming.org",
                    "nist1-nj.ustiming.org",
                    "nist1-pa.ustiming.org",
                    "time-a.nist.gov",
                    "time-b.nist.gov",
                    "nist1.aol-va.symmetricom.com",
                    "nist1.columbiacountyga.gov",
                    "nist1-chi.ustiming.org",
                    "nist.expertsmi.com",
                    "nist.netservicesgroup.com"
            };

            // Try 5 servers in random order to spread the load
            Random rnd = new Random();
            foreach (string server in servers.OrderBy(s => rnd.NextDouble()).Take(5))
            {
                try
                {
                    // Connect to the server (at port 13) and get the response
                    string serverResponse = string.Empty;
                    using (var reader = new StreamReader(new System.Net.Sockets.TcpClient(server, 13).GetStream()))
                    {
                        serverResponse = reader.ReadToEnd();
                    }

                    // If a response was received
                    if (!string.IsNullOrEmpty(serverResponse))
                    {
                        // Split the response string ("55596 11-02-14 13:54:11 00 0 0 478.1 UTC(NIST) *")
                        string[] tokens = serverResponse.Split(' ');

                        // Check the number of tokens
                        if (tokens.Length >= 6)
                        {
                            // Check the health status
                            string health = tokens[5];
                            if (health == "0")
                            {
                                // Get date and time parts from the server response
                                string[] dateParts = tokens[1].Split('-');
                                string[] timeParts = tokens[2].Split(':');

                                // Create a DateTime instance
                                DateTime utcDateTime = new DateTime(
                                    Convert.ToInt32(dateParts[0]) + 2000,
                                    Convert.ToInt32(dateParts[1]), Convert.ToInt32(dateParts[2]),
                                    Convert.ToInt32(timeParts[0]), Convert.ToInt32(timeParts[1]),
                                    Convert.ToInt32(timeParts[2]));

                                // Convert received (UTC) DateTime value to the local timezone
                                result = utcDateTime.ToLocalTime();

                                return result;
                                // Response successfully received; exit the loop

                            }
                        }

                    }

                }
                catch
                {
                    // Ignore exception and try the next server
                }
            }
            return result;
        }

        private string getExternalIP()
        {
            WebClient client = new WebClient();
            string ip = "0.0.0.0";
            try
            {
                ip = client.DownloadString("http://icanhazip.com/");
            }
            catch { }
            return ip;
        }
        #endregion
    }
}