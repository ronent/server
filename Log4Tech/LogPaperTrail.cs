﻿using NLog;
using NLog.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace Log4Tech
{
    internal class LogPaperTrail
    {
        private Logger logPaperTrail;
        private BufferBlock<Tuple<string, Log4Tech.Log.LogSeverity>> bufferBlock = new BufferBlock<Tuple<string, Log4Tech.Log.LogSeverity>>();
        private bool waitForMessage;
        #region constructor
        private static readonly Lazy<LogPaperTrail> lazy = new Lazy<LogPaperTrail>(() => new LogPaperTrail());

        public static LogPaperTrail Instance
        {
            get { return lazy.Value; }
        }

        private LogPaperTrail() 
        { 
            waitForMessage = true;
            bufferBlock = new BufferBlock<Tuple<string, Log.LogSeverity>>();
            Task.Run(() =>
                {
                    asyncReceive().Wait();
                });
        }

        #endregion
        #region Methods
        internal void Write(string msg, Log4Tech.Log.LogSeverity level)
        {
            asyncSendMessage(msg, level).Wait();
        }
        #endregion
        #region private methods
        private async Task asyncSendMessage(string msg, Log4Tech.Log.LogSeverity level)
        {
            await bufferBlock.SendAsync(new Tuple<string, Log4Tech.Log.LogSeverity>(msg, level));
        }

        private async Task asyncReceive()
        {
            // Asynchronously receive the messages back from the block. 
            while (waitForMessage)
            {
                var rcv = await bufferBlock.ReceiveAsync();
                writeToLog(rcv.Item1, rcv.Item2);
                Task.Delay(1).Wait();
            }

        }

        private void writeToLog(string msg, Log4Tech.Log.LogSeverity level)
        {
            try
            {
                if (logPaperTrail == null)
                    initialize();

                switch (level)
                {
                    case Log.LogSeverity.DEBUG:
                        logPaperTrail.Debug(msg);
                        break;
                    case Log.LogSeverity.INFO:
                        logPaperTrail.Info(msg);
                        break;
                    case Log.LogSeverity.WARNING:
                        logPaperTrail.Warn(msg);
                        break;
                    case Log.LogSeverity.ERROR:
                        logPaperTrail.Error(msg);
                        break;
                    default:
                        logPaperTrail.Trace(msg);
                        break;
                }
            }
            finally
            {
            }
        }

        private void initialize()
        {
            LogManager.Configuration = new LoggingConfiguration();

            var syslogTarget = new NLog.Targets.Syslog()
            {
                Layout = "${longdate} ${message}",
                Port = 27249,
                Ssl = true,
                SyslogServer = "logs.papertrailapp.com",
                Protocol = System.Net.Sockets.ProtocolType.Tcp,
                //  Facility = SyslogFacility.
            };

            var syslogRule = new LoggingRule("*", LogLevel.Trace, syslogTarget);
            LogManager.Configuration.AddTarget("syslog", syslogTarget);
            LogManager.Configuration.LoggingRules.Add(syslogRule);


            LogManager.ReconfigExistingLoggers();
            logPaperTrail = LogManager.GetCurrentClassLogger();
        }
        #endregion
    }
}
