﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.IO.Ports;
using Infrastructure;
using System.Collections.Concurrent;

namespace ModemSMS
{
    public class GSMComManager
    {
        #region Delegation
        internal delegate void GSMConnectionStatusChangedEventHandler(object sender, GSMConnectionStatusEventArgs args);
        #endregion
        #region Events Declaration
        internal event GSMConnectionStatusChangedEventHandler GSMConnectionStatusChangedEvent;
        #endregion
        #region Members
        private mCore.SMS GSMCommunicator;
        private GSMInformation GSMInfo;
        private Queue<SMSEntity> SMSQueue;
        private bool IsConnected;
        private bool keepAlive;
        private bool processQueue;
        private ModemDetector modemDetection;
        private ConcurrentDictionary<string, SMSEntity> smsListManager;
        #endregion
        #region Constructor
        public GSMComManager()
        {
            IsConnected = false;
            processQueue = true;
            GSMCommunicator = new mCore.SMS();
            SMSQueue = new Queue<SMSEntity>();
            smsListManager = new ConcurrentDictionary<string, SMSEntity>();
            loadGSM();
            Task.Factory.StartNew(() => 
            {
                handleSMSQueue();
            });
        }

        #endregion
        #region public methods
        internal bool DetectModem(int port)
        {
            try
            {
                modemDetection = new ModemDetector();
                if (modemDetection.DiscoverModemBaudRate(port))
                {
                    Log4Tech.Log.Instance.Write("Sierra Wireless Modem baud rate is {0}, try to connect to this modem", this, Log4Tech.Log.LogSeverity.INFO, modemDetection.BaudRate);
                    connect(modemDetection.Port, modemDetection.BaudRate);
                    return true;
                }
                Log4Tech.Log.Instance.Write("Sierra Wireless Modem did not respond to baud rates, modem will not be connected", this, Log4Tech.Log.LogSeverity.INFO, modemDetection.BaudRate);
            }
            catch(Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in DetectModem", this, ex);
            }
            return false;
        }

        internal void PushSMS(Server.Infrastructure.SMS.SMSManager.AlarmType alarmType, string serialNumber, int contactID, int countryCode, long mobileNumber, string msgBody, int retryInterval, int sensorTypeID)
        {
            tryToSendSMS(alarmType, serialNumber, contactID, countryCode, mobileNumber, msgBody, retryInterval, sensorTypeID);
        }

        internal void CloseGSM()
        {
            processQueue = false;
            keepAlive = false;
            try
            {
                GSMCommunicator.Disconnect();
                GSMCommunicator.Dispose();
            }
            catch { }
        }

        internal void SendTestSMS(int contactID, int countryCode, long mobileNumber, string msgBody)
        {
            //checks if the GSM modem is connected
            if (IsConnected)
            {
                //modem is connected...
                //try to send the SMS
                Log4Tech.Log.Instance.Write("Send SMS through Modem GSM", this);
                GSMCommunicator.SendSMS(string.Format("{0}{1}", countryCode, mobileNumber), msgBody, true);
            }
            else
            {
                Log4Tech.Log.Instance.Write("Put SMS in a queue till connected to the modem", this);
                //if not then put the SMS details in a queue
                SMSQueue.Enqueue(new SMSEntity()
                {
                    ContactID = contactID,
                    CountryCode = countryCode,
                    MobileNumber = mobileNumber,
                    MsgBody = msgBody,
                    RetryInterval = 1,
                    SensorTypeID = 0,
                    SerialNumber = string.Empty,
                    SMSAlarmType = 0
                });
            }
        }

        #endregion
        #region Private methods
        private void tryToSendSMS(Server.Infrastructure.SMS.SMSManager.AlarmType alarmType, string serialNumber, int contactID, int countryCode, long mobileNumber, string msgBody, int retryInterval, int sensorTypeID)
        {
            //checks if the GSM modem is connected
            if (IsConnected)
            {
                //modem is connected...
                //try to send the SMS
                Log4Tech.Log.Instance.Write("Send SMS through Modem GSM", this);
                GSMCommunicator.SendSMS(string.Format("{0}{1}", countryCode, mobileNumber), msgBody, true);
            }
            else
            {
                Log4Tech.Log.Instance.Write("Put SMS in a queue till connected to the modem", this);
                //if not then put the SMS details in a queue
                SMSQueue.Enqueue(new SMSEntity()
                {
                    ContactID = contactID,
                    CountryCode = countryCode,
                    MobileNumber = mobileNumber,
                    MsgBody = msgBody,
                    RetryInterval = retryInterval,
                    SensorTypeID = sensorTypeID,
                    SerialNumber = serialNumber,
                    SMSAlarmType = alarmType
                });
            }
        }

        private void tryToConnect(GSMInformation gsmInfo)
        {
            this.GSMInfo = gsmInfo;
            //init the Modem parameters
            initGSMParamters();
            //keep the connection alive
            while (keepAlive)
            {
                try
                {
                    //if not connected then try to connect
                    if (!GSMCommunicator.IsConnected && GSMInfo.GSMEnabled)
                    {
                        try
                        {
                            if (GSMCommunicator.Connect())
                            {
                                if (GSMCommunicator.IsConnected)
                                {
                                    Log4Tech.Log.Instance.Write("Modem GSM is now connected", this);
                                    IsConnected = true;
                                    GSMConnectionStatusChangedEvent(this, new GSMConnectionStatusEventArgs(true));
                                }
                                else
                                {
                                    Log4Tech.Log.Instance.Write("Modem GSM is now disconnected", this);
                                    GSMConnectionStatusChangedEvent(this, new GSMConnectionStatusEventArgs(false));
                                }
                            }
                        }
                        catch (mCore.GeneralException gx)
                        {
                            Log4Tech.Log.Instance.WriteError("Fail in tryToConnect", this, gx);
                            GSMConnectionStatusChangedEvent(this, new GSMConnectionStatusEventArgs(false));
                            keepAlive = false;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log4Tech.Log.Instance.WriteError("Fail in tryToConnect", this, ex);
                    GSMConnectionStatusChangedEvent(this, new GSMConnectionStatusEventArgs(false));
                    keepAlive = false;
                }
                //go to sleep for a while (5 sec.)
                Thread.Sleep(1000);
            }
        }

        private void initGSMParamters()
        {
            GSMCommunicator.Port = "COM" + GSMInfo.PortNumber.ToString();
            GSMCommunicator.BaudRate = (mCore.BaudRate)(GSMInfo.BaudRate);
            
            if (GSMInfo.RequiresPin)
                GSMCommunicator.PIN = GSMInfo.PinCode;

            GSMCommunicator.DisableCheckPIN = !GSMInfo.RequiresPin;
        }

        private void loadGSM()
        {
            try
            {
                GSMCommunicator.ReadIntervalTimeout = 100;
                GSMCommunicator.DelayAfterPIN = 20000;
                GSMCommunicator.Queue().Enabled = true;
                GSMCommunicator.DataBits = mCore.DataBits.Eight;
                GSMCommunicator.Parity = mCore.Parity.None;
                GSMCommunicator.StopBits = mCore.StopBits.One;
                GSMCommunicator.FlowControl = mCore.FlowControl.None;
                GSMCommunicator.Timeout = 20000;
                //GSMCommunicator.NewMessageIndication = true;
                //-------------------------------------------------
                //Initialize incoming message concatenate setting
                //-------------------------------------------------
                if (GSMCommunicator.Inbox().Concatenate)
                {
                    GSMCommunicator.NewMessageConcatenate = true;
                }
                GSMCommunicator.NewUSSDIndication = true;
                //set the license for the GSM wireless modem
                GSMCommunicator.License().Company = "FOURIER-SYSTEMS";
                GSMCommunicator.License().LicenseType = "LITE-DISTRIBUTION";
                GSMCommunicator.License().Key = "WK4H-YLXC-T2KE-G31J";
                //listen on incoming messages (probably no need, but ...)
                GSMCommunicator.NewMessageReceived += GSMCommunicator_NewMessageReceived;
                GSMCommunicator.QueueSMSSending +=GSMCommunicator_QueueSMSSending;
                GSMCommunicator.QueueSMSSent += GSMCommunicator_QueueSMSSent;
                
                ///if the language for sending SMS is one of this languages then set the unicode accordingly 
                ///Japanese, Chinese Russian
                GSMCommunicator.Encoding = mCore.Encoding.Unicode_16Bit;
            }
            catch (System.Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in sendNotificationEmailSMS", this, ex);
            }
        }

        private void connect(int port, int baudRate)
        {
            var gsmInfo = new GSMInformation()
            {
                BaudRate = baudRate,
                RequiresPin = false,
                GSMEnabled = true,
                CommTimeout = 300,
                PortNumber = port,
            };
            //this will try to connect to exisitng com port of GSM modem
            Task.Factory.StartNew(() =>
            {
                keepAlive = true;
                tryToConnect(gsmInfo);
            });
        }

        private void handleSMSQueue()
        {
            while (processQueue)
            {
                try
                {
                    //checks first if the modem is connected before processing the queue
                    if (IsConnected)
                    {
                        //checks if the queue has something to process
                        foreach (var i in SMSQueue.ToList())
                        {
                            //take from the queue
                            SMSEntity smsEntity = SMSQueue.Dequeue();
                            Log4Tech.Log.Instance.Write("SMS will be sent to {0}-{1} with body '{2}'", this, Log4Tech.Log.LogSeverity.INFO, smsEntity.CountryCode, smsEntity.MobileNumber, smsEntity.MsgBody);
                            //set the retry times
                            GSMCommunicator.SendRetry = smsEntity.RetryInterval;
                            //try to send the SMS
                            Task.Factory.StartNew(() => 
                            {
                                SendSMS(smsEntity);
                            });
                        }
                    }
                    //let the cpu relax
                    Thread.Sleep(1000);
                }
                catch(Exception ex)
                {
                    Log4Tech.Log.Instance.WriteError("Fail in handleSMSQueue", this, ex);
                }
            }
        }

        private void SendSMS(SMSEntity smsEntity)
        {
            try
            {
                string smsKey = GSMCommunicator.SendSMS(string.Format("{0}{1}", smsEntity.CountryCode, smsEntity.MobileNumber), smsEntity.MsgBody, false);
                Log4Tech.Log.Instance.Write("SMS queue #{0} was sent to {1}-{2} successfully", this, Log4Tech.Log.LogSeverity.INFO, smsKey, smsEntity.CountryCode, smsEntity.MobileNumber);
                //update the database accordingly
                if (smsEntity.SMSAlarmType == Server.Infrastructure.SMS.SMSManager.AlarmType.Sensor)
                {
                    DataManager.Instance.UpdateNotificationSMSSent(smsEntity.SerialNumber, smsEntity.ContactID, smsKey, smsEntity.SensorTypeID);
                }
                else if (smsEntity.SMSAlarmType == Server.Infrastructure.SMS.SMSManager.AlarmType.Battery)
                {
                    DataManager.Instance.UpdateNotificationBatterySMSSent(smsEntity.SerialNumber, smsEntity.ContactID, smsKey);
                }
            }
            catch(Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in SendSMS", this, ex);
            }
        }

        #endregion
        #region Events
        void GSMCommunicator_NewMessageReceived(object sender, mCore.NewMessageReceivedEventArgs e)
        {

        }

        void GSMCommunicator_QueueSMSSent(object sender, mCore.QueueSMSSentEventArgs e)
        {
            /*SMSEntity smsEntity;
            try
            {
                //if result is ok
                if (e.SendResult)
                {
                    Log4Tech.Log.Instance.Write("SMS queue #{0} was sent to {1} successfully", this, Log4Tech.Log.LogSeverity.INFO, e.QueueMessageKey, e.DestinationNumber);
                    //get the sms info from the sms list manager by the key of the sms
                    if (smsListManager.TryGetValue(e.QueueMessageKey, out smsEntity))
                    {
                        //update the database accordingly
                        if (smsEntity.SMSAlarmType == Server.Infrastructure.SMS.SMSManager.AlarmType.Sensor)
                        {
                            DataManager.Instance.UpdateNotificationSMSSent(smsEntity.SerialNumber, smsEntity.ContactID, e.QueueMessageKey, smsEntity.SensorTypeID);
                        }
                        else if (smsEntity.SMSAlarmType == Server.Infrastructure.SMS.SMSManager.AlarmType.Battery)
                        {
                            DataManager.Instance.UpdateNotificationBatterySMSSent(smsEntity.SerialNumber, smsEntity.ContactID, e.QueueMessageKey);
                        }
                        //remove from the list after finish handling
                        smsListManager.TryRemove(e.QueueMessageKey, out smsEntity);
                    }
                }
                else
                {
                    Log4Tech.Log.Instance.Write("Sending SMS failed with error code #{0}, description - {1}", this, Log4Tech.Log.LogSeverity.INFO, e.ErrorCode, e.ErrorDescription);
                }
            }
            catch(Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in GSMCommunicator_QueueSMSSent", this, ex);
            }*/
        }

        private void GSMCommunicator_QueueSMSSending(object sender, mCore.QueueSMSSendingEventArgs e)
        {
            Log4Tech.Log.Instance.Write("Pull out from queue of SMS #{0} to {1}", this, Log4Tech.Log.LogSeverity.INFO,e.QueueMessageKey, e.DestinationNumber);
        }
        
        #endregion
    }
}
