﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModemSMS
{
    internal class GSMConnectionStatusEventArgs : EventArgs
    {
        public GSMConnectionStatusEventArgs(bool Connected)
        {
            this.Connected = Connected;
        }

        public bool Connected;
    }
}
