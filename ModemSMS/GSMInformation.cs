﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModemSMS
{
    internal class GSMInformation
    {
public bool GSMEnabled { get; set; }
        public int PortNumber { get;  set; }
        public int BaudRate { get;  set; }
        public int CommTimeout { get;  set; }
        public bool RequiresPin { get;  set; }
        public string PinCode { get;  set; }

        public GSMInformation()
        {

        }

       
    }
}
