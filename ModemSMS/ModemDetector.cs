﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModemSMS
{
    class ModemDetector
    {
        #region Members
        private int[] baudRates =
        {
           9600,
           14400,
           19200,
           38400,
           56000,
           57600,
           115200
        };
        private int dataBits = 8;
        private SerialPort _serialPort;
        private bool _continue;
        public int Port { get; private set; }
        public int BaudRate { get; private set; }
        #endregion
        #region Constructor
        public ModemDetector()
        {
            _continue = false;
        }
        #endregion
        #region Private Methods
        private void ReadFromPort()
        {
            try
            {
                string msg = _serialPort.ReadLine();
                Log4Tech.Log.Instance.Write("Modem respond  - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, msg);
                this.BaudRate = _serialPort.BaudRate;
            }
            catch (InvalidOperationException) { }
            catch (Exception) { }
            _continue = false;
        }

        internal bool DiscoverModemBaudRate(int port)
        {
            this.Port = port;
            try
            {
                for (int indexBaud = 0; indexBaud < baudRates.Length; indexBaud++)
                {
                    try
                    {
                        _serialPort = new SerialPort();
                        _serialPort.PortName = "COM" + this.Port;
                        _serialPort.BaudRate = baudRates[indexBaud];
                        _serialPort.Parity = Parity.None;
                        _serialPort.DataBits = dataBits;
                        _serialPort.StopBits = StopBits.One;
                        _serialPort.Handshake = Handshake.None;
                        _serialPort.ReadTimeout = 500;
                        _serialPort.WriteTimeout = 500;
                        //open the serial port
                        _serialPort.Open();
                        _continue = true;
                        Task.Factory.StartNew(()=>
                        {
                            ReadFromPort();
                        });
                        //if success then try to send AT command
                        _serialPort.WriteLine(String.Format("<{0}>{1}\r\n", "AT", "?"));

                        while (_continue)
                        {

                        }
                        if (this.BaudRate > 0)
                        {
                            return true;
                        }
                    }
                    catch (TimeoutException tx) 
                    {
                        Log4Tech.Log.Instance.WriteError("Fail in DiscoverModemBaudRate", this, tx);
                        break; 
                    }
                    catch (UnauthorizedAccessException ax) 
                    {
                        Log4Tech.Log.Instance.WriteError("Fail in DiscoverModemBaudRate", this, ax);
                        break; 
                    }
                    catch (Exception ex) 
                    { 
                        Log4Tech.Log.Instance.WriteError("Fail in DiscoverModemBaudRate", this, ex);
                        break;
                    }
                    finally
                    {
                        _serialPort.Close();
                        _serialPort.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in discover com port for modem", this, ex);
            }
            return false;
        }
        #endregion
        
    }
}
