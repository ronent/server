﻿using Server.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModemSMS
{
    public class SMSAPI : ISMS
    {
        private const string SIERRA_WIRELESS_PID = "PID_2008";
        private const string SIERRA_WIRELESS_VID = "VID_0557";
        #region Members
        GSMComManager manager;
        #endregion
        #region Constructor
        public SMSAPI()
        {
            manager = new GSMComManager();
            manager.GSMConnectionStatusChangedEvent += manager_GSMConnectionStatusChangedEvent;
        }

        #endregion
        #region Events
        void manager_GSMConnectionStatusChangedEvent(object sender, GSMConnectionStatusEventArgs args)
        {
            Log4Tech.Log.Instance.Write("[INFO] GSM modem is {0}", this, Log4Tech.Log.LogSeverity.INFO, args.Connected ? "connected" : "disconnected");
        }

        #endregion
        #region ISMS Members
        public void SendSMS(Server.Infrastructure.SMS.SMSManager.AlarmType alarmType, string serialNumber, int contactID, int countryCode, long mobileNumber, string msgBody, int retryInterval, int sensorTypeID = 0)
        {
            manager.PushSMS(alarmType, serialNumber, contactID, countryCode, mobileNumber, msgBody, retryInterval, sensorTypeID);
        }

        #endregion
        #region Public methods
        public bool CanCreate(string port, string uid)
        {
            //checks first if it is sierra wireless modem
            if (isSierraWirelessModem(uid))
            {
                //then check if can communicate with the modem by detecting the modem baud rate
                return manager.DetectModem(Convert.ToInt32(port.Substring(3)));
            }
            return false;
        }
        #endregion
        #region Private methods
        private bool isSierraWirelessModem(string uid)
        {
            if (uid.Split('&').Length > 1 && uid.Split('\\').Length>0)
            {
                //checks if this is a sierra wireless modem according the pid_vid of the id
                return uid.Split('\\')[1].Split('&')[0] == SIERRA_WIRELESS_VID && uid.Split('\\')[1].Split('&')[1] == SIERRA_WIRELESS_PID;
            }
            return false;
        }
        #endregion
    }
}
