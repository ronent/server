﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModemSMS
{
    public class SMSEntity
    {
        public Server.Infrastructure.SMS.SMSManager.AlarmType SMSAlarmType { get; set; }
        public string SerialNumber { get; set; }
        public int ContactID { get; set; }
        public int CountryCode { get; set; }
        public long MobileNumber { get; set; }
        public string MsgBody { get; set; }
        public int RetryInterval { get; set; }
        public int SensorTypeID { get; set; }
    }
}
