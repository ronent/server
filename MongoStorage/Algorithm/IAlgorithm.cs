﻿using System;
using System.Collections.Generic;
using MongoDB.Driver;
using Server.Infrastructure;

namespace MongoStorage.Algorithm
{
    interface IAlgorithm
    {
        IEnumerable<Tuple<int, double>> GetDownSamples(IFindFluent<Sample, Sample> data, int threshold);
    }
}
