﻿using System;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Driver;

namespace MongoStorage.Algorithm
{
    /// <summary>
    /// Largest-Triangle-Three-Buckets or LTTB
    /// https://github.com/sveinn-steinarsson/flot-downsample/
    /// </summary>
    public class LargestTriangleThreeBuckets //: IAlgorithm
    {
        //public static IEnumerable<Tuple<double, double>> LargestTriangleThreeBuckets(List<Tuple<double, double>> data, int threshold)
//        public IEnumerable<Tuple<int, double>> GetDownSamples(MongoCursor<Sample> data, int threshold, Sample first, Sample last,Sample[] arraySamples)
//        {
//            Log4Tech.Log.Instance.Write("Start Algorithm", "LTTB");
//            var dataArray = data.ToArray();
//            var sampled = new List<Tuple<int, double>>(threshold);
//
//            var dataLength = dataArray.Count();
//            if (threshold >= dataLength || threshold == 0)
//                return sampled; // Nothing to do
//
//            // Bucket size. Leave room for start and end data points
//            var every = (double)(dataLength - 2) / (threshold - 2);
//
//            var a = 0;
//            var maxAreaPoint = new Tuple<int, double>(0, 0);
//            var nextA = 0;
//
//            sampled.Add(new Tuple<int, double>(dataArray[a].SampleTime, dataArray[a].Value)); // Always add the first point
//
//            for (var i = 0; i < threshold - 2; i++)
//            {
//                // Calculate point average for next bucket (containing c)
//                double avgX = 0;
//                double avgY = 0;
//                var avgRangeStart = (int)(Math.Floor((i + 1) * every) + 1);
//                var avgRangeEnd = (int)(Math.Floor((i + 2) * every) + 1);
//                avgRangeEnd = avgRangeEnd < dataLength ? avgRangeEnd : dataLength;
//
//                var avgRangeLength = avgRangeEnd - avgRangeStart;
//
//                for (; avgRangeStart < avgRangeEnd; avgRangeStart++)
//                {
//                    avgX += dataArray[avgRangeStart].SampleTime; // * 1 enforces Number (value may be Date)
//                    avgY += dataArray[avgRangeStart].Value;
//                }
//                avgX /= avgRangeLength;
//
//                avgY /= avgRangeLength;
//
//                // Get the range for this bucket
//                var rangeOffs = (int)(Math.Floor((i + 0) * every) + 1);
//                var rangeTo = (int)(Math.Floor((i + 1) * every) + 1);
//
//                // Point a
//                var pointAx = dataArray[a].SampleTime; // enforce Number (value may be Date)
//                var pointAy = dataArray[a].Value;
//
//                double maxArea = -1;
//
//                for (; rangeOffs < rangeTo; rangeOffs++)
//                {
//                    // Calculate triangle area over three buckets
//                    var area = Math.Abs((pointAx - avgX) * (dataArray[rangeOffs].Value - pointAy) -
//                                           (pointAx - dataArray[rangeOffs].SampleTime) * (avgY - pointAy)
//                                      ) * 0.5;
//                    if (area > maxArea)
//                    {
//                        maxArea = area;
//                        maxAreaPoint = new Tuple<int, double>(dataArray[rangeOffs].SampleTime, dataArray[rangeOffs].Value); ;
//                        nextA = rangeOffs; // Next a is this b
//                    }
//                }
//
//                sampled.Add(maxAreaPoint); // Pick this point from the bucket
//                a = nextA; // This a is the next a (chosen b)
//            }
//
//            sampled.Add(new Tuple<int, double>(dataArray[dataLength - 1].SampleTime, dataArray[dataLength - 1].Value));  // Always add last
//
//            Log4Tech.Log.Instance.Write("End Algorithm", "LTTB");
//
//            return sampled;
//
//        }


    }
}
