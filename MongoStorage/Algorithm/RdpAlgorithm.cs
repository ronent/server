﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Log4Tech;
using MongoDB.Driver;
using Server.Infrastructure;

namespace MongoStorage.Algorithm
{
    
    // Ramer–Douglas–Peucker algorithm
    // https://en.wikipedia.org/wiki/Ramer%E2%80%93Douglas%E2%80%93Peucker_algorithm
    
    public class RdpAlgorithm : IAlgorithm
    {
        /// <summary>
        /// Ramer–Douglas–Peucker algorithm (WIKI : https://en.wikipedia.org/wiki/Ramer%E2%80%93Douglas%E2%80%93Peucker_algorithm)
        /// </summary>
        public IEnumerable<Tuple<int, double>> GetDownSamples(IFindFluent<Sample, Sample> data, int threshold)
        //public IEnumerable<Tuple<int, double>> GetDownSamples(List<Tuple<int, double>> data, int threshold)
        {
           // var stopWatch = new Stopwatch();
           // stopWatch.Start();
           // Log.Instance.Write("Start Algorithm", "DP");
            var target = new List<Tuple<int, double>>(threshold);
            try
            {
                //Check some preconditions
               
//                var source = data.GetEnumerator();

//            if (count < 3 || count <= threshold || threshold < 0)
//            {
////                while (source.MoveNext())
////                    target.Add(new Tuple<int, double>(source.Current.SampleTime, source.Current.Value)); 
//                return target;
//            }

                /////////////////////////////////////////////////////////////////////////////////////////
                // Log4Tech.Log.Instance.Write("before line bug fix", "DP");
                /////////////////////////////////////////////////////////////////////////////////////////
                target = straightLinebugfix(data).Result;

                /////////////////////////////////////////////////////////////////////////////////////////
                // Log4Tech.Log.Instance.Write("after line bug fix", "DP");
                /////////////////////////////////////////////////////////////////////////////////////////

                if (target.Count <= threshold)
                    return target;

                //init two lists which are containing the polynoms 
                var pointsToReturn = new List<Tuple<int, double>>();
                var polynoms = new List<Polynom> {new Polynom(target)};

                //Create and add the first polynom from the original list of points

                /////////////////////////////////////////////////////////////////////////////////////////
                //Log4Tech.Log.Instance.Write("before polynom loop", "DP");
                /////////////////////////////////////////////////////////////////////////////////////////

                //split the polnoms as long as the expected number of points is not reached
                for (var i = 0; i < threshold - 2; i++)
                {
                    var p = polynoms.OrderByDescending(polynom => polynom.MaxDistance).First();
                    polynoms.Insert(polynoms.IndexOf(p), p.SplitAtMaxDistance());
                }

                /////////////////////////////////////////////////////////////////////////////////////////
                // Log4Tech.Log.Instance.Write("after polynom loop", "DP");
                /////////////////////////////////////////////////////////////////////////////////////////

                //convert the edges of the polynoms back to a list of points
                pointsToReturn.Add(polynoms[0].FirstPoint);
                pointsToReturn.AddRange(polynoms.Select(polynom => polynom.LastPoint));

            //    stopWatch.Stop();
             //   Log.Instance.Write("End Algorithm return - #{0} ,took {1} ms.", "DP", Log.LogSeverity.DEBUG,
             //       pointsToReturn.Count, stopWatch.ElapsedMilliseconds);

                return pointsToReturn;
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("[Mongo API] Fail in GetDownSamples", this, ex);
            }
            return target;
        }

        private async Task<List<Tuple<int, double>>> straightLinebugfix(IFindFluent<Sample, Sample> points)
        {
            var aggregateList = new List<List<Tuple<int, double>>>();
            var currentList = new List<Tuple<int, double>>();
            try
            {
                var samples = await points.ToListAsync();
                //while (points.MoveNextAsync().Result)
                //await points.ForEachAsync((sample) =>
                foreach (var sample in samples)
                {
                    //foreach (var sample in points.Current)
                    {
                        // Console.WriteLine(sample.Value);

                        //var sample = points.Current.GetEnumerator().Current;
                        //for the first item only
                        if (currentList.Count == 0)
                            currentList.Add(new Tuple<int, double>(sample.SampleTime, sample.Value));
                        else
                        {

                            if (currentList.First().Item2 == sample.Value)
                                currentList.Add(new Tuple<int, double>(sample.SampleTime, sample.Value));
                            else
                            {
                                aggregateList.Add(currentList);
                                currentList = new List<Tuple<int, double>>
                            {
                                new Tuple<int, double>(sample.SampleTime, sample.Value)
                            };
                            }
                        }
                    }
                }

                aggregateList.Add(currentList);

                currentList = new List<Tuple<int, double>>();

                foreach (var item in aggregateList)
                {
                    if (!item.Any()) continue;

                    currentList.Add(item.First());

                    if (item.Count > 1)
                        currentList.Add(item.Last());
                }

               
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("[Mongo API] Fail in straightLinebugfix", this, ex);
            }
            return currentList;
        }

        /// <summary>
        /// Polynom which can be splitted
        /// </summary>
        private class Polynom
        {
            private readonly List<Tuple<int, double>> polynompoints = new List<Tuple<int, double>>();

            public Tuple<int,double> FirstPoint { get { return polynompoints.First(); } }
            public Tuple<int,double> LastPoint { get { return polynompoints.Last(); } }
            public double MaxDistance { get; private set; }

            private int maxDistancePointIndex { get; set; }

            public Polynom(IEnumerable<Tuple<int, double>> PolynomPoints)
            {
                polynompoints.AddRange(PolynomPoints);
                calcDistance();
            }

            /// <summary>
            /// calculate the point of the maximum distance within THIS polynom
            /// </summary>
            private void calcDistance()
            {
                MaxDistance = 0;

                for (var i = 0; i < polynompoints.Count - 1; i++)
                {
                    double currdist = perpendicularDistance(FirstPoint, LastPoint, polynompoints[i]);
                    if (!(currdist > MaxDistance)) 
                        continue;

                    maxDistancePointIndex = i;
                    MaxDistance = currdist;
                }

            }

            /// <summary>
            /// Split THIS polynom at the maximum distance index into two polynoms
            /// </summary>
            public Polynom SplitAtMaxDistance()
            {
                var pl = polynompoints.GetRange(0, maxDistancePointIndex + 1);
                polynompoints.RemoveRange(0, maxDistancePointIndex);
                calcDistance();
                return new Polynom(pl);
            }
        }

        /// <summary>
        /// Calculate the perpendicular distance of a given point
        /// </summary>        
        private static float perpendicularDistance(Tuple<int, double> firstPoint, Tuple<int, double> lastPoint, Tuple<int, double> currentPoint)
        {
            var area =
                Math.Abs(.5*
                         (firstPoint.Item1*lastPoint.Item2 + lastPoint.Item1*currentPoint.Item2 +
                          currentPoint.Item1*firstPoint.Item2 - lastPoint.Item1*firstPoint.Item2 -
                          currentPoint.Item1*lastPoint.Item2 - firstPoint.Item1*currentPoint.Item2));

            float tempTime = firstPoint.Item1 - lastPoint.Item1;
            var tempValue = (float)firstPoint.Item2 - (float)lastPoint.Item2;
            var bottom = Sqrt(tempTime * tempTime + tempValue * tempValue);
            var height = (float)area / bottom * 2;
            return height;
        }



        internal static float Sqrt(float z)
        {
            if (z == 0) return 0;
            FloatIntUnion u;
            u.tmp = 0;
            u.f = z;
            u.tmp -= 1 << 23; // Subtract 2^m. 
            u.tmp >>= 1; // Divide by 2. 
            u.tmp += 1 << 29; // Add ((b + 1) / 2) * 2^m. 
            return u.f;
        }

        [StructLayout(LayoutKind.Explicit)]
        private struct FloatIntUnion
        {
            [FieldOffset(0)]
            public float f;

            [FieldOffset(0)]
            public int tmp;
        }
    }
}
