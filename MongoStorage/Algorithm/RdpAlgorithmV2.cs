﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using MongoDB.Driver;

namespace MongoStorage.Algorithm
{
    class RdpAlgorithmV2 //: IAlgorithm
    {
//        /// <span class="code-SummaryComment"><summary></span>
//        /// Uses the Douglas Peucker algorithm to reduce the number of points.
//        /// <span class="code-SummaryComment"></summary></span>
//        /// <span class="code-SummaryComment"><param name="Points">The points.</param></span>
//        /// <span class="code-SummaryComment"><param name="Tolerance">The tolerance.</param></span>
//        /// <span class="code-SummaryComment"><returns></returns></span>
//        public IEnumerable<Tuple<int, double>> GetDownSamples(MongoCursor<Sample> data, int threshold, Sample first, Sample last,Sample[] arraySamples)
//        {
//            var returnPoints = new List<Tuple<int, double>>(threshold);
//            var count = data.Count();
//            if (count < 3 || count <= threshold || threshold < 0)
//            {
//                return returnPoints;
//            }
//
//            const int firstPoint = 0;
//            var lastPoint = (int)(count - 1);
//            var pointIndexsToKeep = new List<int> {firstPoint, lastPoint};
//
//            //Add the first and last index to the keepers
//
//            //The first and the last point cannot be the same
//            while (first.Value==last.Value)
//            {
//                lastPoint--;
//            }
//
//            DouglasPeuckerReduction(arraySamples, firstPoint, lastPoint,
//            threshold, ref pointIndexsToKeep);
//
//            pointIndexsToKeep.Sort();
//
//            returnPoints.AddRange(pointIndexsToKeep.Select(index => new Tuple<int, double>(arraySamples[index].SampleTime, arraySamples[index].Value)));
//
//            return returnPoints;
//        }
//
//        /// <span class="code-SummaryComment"><summary></span>
//        /// Douglases the peucker reduction.
//        /// <span class="code-SummaryComment"></summary></span>
//        /// <span class="code-SummaryComment"><param name="points">The points.</param></span>
//        /// <span class="code-SummaryComment"><param name="firstPoint">The first point.</param></span>
//        /// <span class="code-SummaryComment"><param name="lastPoint">The last point.</param></span>
//        /// <span class="code-SummaryComment"><param name="tolerance">The tolerance.</param></span>
//        /// <span class="code-SummaryComment"><param name="pointIndexsToKeep">The point index to keep.</param></span>
//        private static void DouglasPeuckerReduction(Sample[]  points, int firstPoint, int lastPoint, double tolerance,
//            ref List<int> pointIndexsToKeep)
//        {
//            double maxDistance = 0;
//            var indexFarthest = 0;
//            if (lastPoint - firstPoint > 1)
//            {
//                for (var index = firstPoint; index < lastPoint; index++)
//                {
//                    var distance = PerpendicularDistance(points[firstPoint].SampleTime,
//                        points[firstPoint].Value,
//                        points[lastPoint].SampleTime,
//                        points[lastPoint].Value,
//                        points[index].SampleTime,
//                        points[index].Value);
//                    if (!(distance > maxDistance)) continue;
//                    maxDistance = distance;
//                    indexFarthest = index;
//                }
//
//                //if (maxDistance > tolerance && indexFarthest != 0)
//                if (maxDistance > tolerance && indexFarthest != firstPoint) //CHANGE: condition was wrong.
//                {
//                    //Add the largest point that exceeds the tolerance
//                    pointIndexsToKeep.Add(indexFarthest);
//
//                    DouglasPeuckerReduction(points, firstPoint,
//                        indexFarthest, tolerance, ref pointIndexsToKeep);
//                    DouglasPeuckerReduction(points, indexFarthest,
//                        lastPoint, tolerance, ref pointIndexsToKeep);
//                }
//            }
//        }
//
//        private static double PerpendicularDistance
//            (int firstTime, double firstValue, int lastTime,double lastValue, int currentTime, double currentValue)
//        {
//
//            var area =
//                Math.Abs(.5*
//                         (firstTime * lastValue + lastTime * currentValue +
//                          currentTime * firstValue - lastTime * firstValue -
//                          currentTime * lastValue - firstTime * currentValue));
//
//            float tempTime = firstTime - lastTime;
//            var tempValue = (float)firstValue - (float)lastValue;
//            var bottom = Sqrt(tempTime*tempTime + tempValue*tempValue);
//            var height = (float) area/bottom*2;
//            return height;
//
//
//        }
//
//        internal static float Sqrt(float z)
//        {
//            if (z == 0) return 0;
//            FloatIntUnion u;
//            u.tmp = 0;
//            u.f = z;
//            u.tmp -= 1 << 23; // Subtract 2^m. 
//            u.tmp >>= 1; // Divide by 2. 
//            u.tmp += 1 << 29; // Add ((b + 1) / 2) * 2^m. 
//            return u.f;
//        }
//
//        [StructLayout(LayoutKind.Explicit)]
//        private struct FloatIntUnion
//        {
//            [FieldOffset(0)]
//            public float f;
//
//            [FieldOffset(0)]
//            public int tmp;
//        }
    }
}
