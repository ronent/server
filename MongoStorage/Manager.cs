﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Bson.IO;
using Infrastructure;
using Newtonsoft.Json;
using System.Configuration;
using System.Diagnostics;
using Server.Infrastructure;
using System.Collections;
using System.Collections.Concurrent;
using System.Dynamic;
using System.IO;
using System.IO.Compression;
using System.Threading;
using Infrastructure.Interfaces;
using Log4Tech;
using MongoDB.Bson.Serialization;
using MongoDB.Driver.Core.Operations;
using MongoStorage.Algorithm;

namespace MongoStorage
{
    public class Manager
    {
        #region Fields
        private IMongoClient mongoClient;
//        private MongoServer mongoServer;
        private IMongoDatabase mongoDatabase;
        private JsonWriterSettings jsonWriterSettings = new JsonWriterSettings { OutputMode = JsonOutputMode.Strict };
        private string mongoPath = ConfigurationManager.AppSettings["mongoPath"];
        private string backupPath = ConfigurationManager.AppSettings["backupPath"];
        #endregion
        #region ctor
        private Manager() 
        {
            var connectionString = ConfigurationManager.AppSettings["mongoConnection"];
            mongoClient = new MongoClient(connectionString);
//            mongoServer = mongoClient.GetServer();
//            mongoDatabase = mongoServer.GetDatabase("Loggers");

            mongoClient = new MongoClient(connectionString);
            mongoDatabase = mongoClient.GetDatabase("Loggers");
        }

        private static readonly Lazy<Manager> lazy = new Lazy<Manager>(() => new Manager());

        public static Manager Instance
        {
            get { return lazy.Value; }
        }
        #endregion
        #region public methods

        internal async Task<bool> InsertSample(Sample sample)
        {
            try
            {
                var collection = mongoDatabase.GetCollection<Sample>(typeof (Sample).Name);
                var builder = Builders<Sample>.Filter;
                var filter = builder.Eq("SerialNumber", sample.SerialNumber) &
                             builder.Eq("SensorTypeID", sample.SensorTypeID) &
                             builder.Eq("CustomerID", sample.CustomerID) & 
                             builder.Eq("SampleTime", sample.SampleTime);

                var result = await collection.Find(filter).ToListAsync();
                if (!result.Any())
                    await collection.InsertOneAsync(sample);
                Common.LastSample.AddOrUpdate(sample.CustomerID, sample.SampleTime, (key, oldValue) => sample.SampleTime);
                return true;
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("[Mongo API] Fail in Insert", this, ex);
            }
            return false;
        }

        internal async Task<bool> InsertBatch(List<Sample> samples)
        {
            try
            {
                Log.Instance.Write("[Mongo API] Start save batch of {0} samples", this, Log4Tech.Log.LogSeverity.DEBUG,
                    samples.Count);
                var collection = mongoDatabase.GetCollection<Sample>(typeof (Sample).Name);

                //var wasInserted = true;
                var lastOrDefault = samples.LastOrDefault();
                var firstSample = samples.FirstOrDefault();
                if (lastOrDefault != null && firstSample!=null)
                {
                    var customerId = lastOrDefault.CustomerID;
                    var sampleTime = lastOrDefault.SampleTime;

                     var builder = Builders<Sample>.Filter;
                    var filter = builder.Eq("SerialNumber", firstSample.SerialNumber) &
                                 builder.Eq("SensorTypeID", firstSample.SensorTypeID) &
                                 builder.Eq("CustomerID", firstSample.CustomerID) &
                                 builder.Gte("SampleTime", firstSample.SampleTime) &
                                 builder.Lte("SampleTime", lastOrDefault.SampleTime);

                    var result  = await collection.DeleteManyAsync(filter);

                    if(result.IsAcknowledged)
                        await collection.InsertManyAsync(samples, new InsertManyOptions() {IsOrdered = true});

                    //insert all samples in parallel
//                    samples.ForEach(
//                        async sample =>
//                        {
//                             builder = Builders<Sample>.Filter;
//                             filter = builder.Eq("SerialNumber", sample.SerialNumber) &
//                                         builder.Eq("SensorTypeID", sample.SensorTypeID) &
//                                         builder.Eq("CustomerID", sample.CustomerID) &
//                                         builder.Eq("SampleTime", sample.SampleTime);
//
//                            //var result = await collection.Find(filter).ToListAsync();
//                            var result = await collection.Find(filter).ToCursorAsync();
//                            await result.MoveNextAsync();
//                            if (!result.Current.Any())
//                                await collection. InsertOneAsync(sample);
//                        });

                    Log.Instance.Write("[Mongo API] End save batch of {0} samples", this,
                        Log.LogSeverity.DEBUG, samples.Count);

                    Common.LastSample.AddOrUpdate(customerId, sampleTime, (key, oldValue) => sampleTime);
                    return true;
                }
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("[Mongo API] Fail in InsertBatch", this, ex);
            }
            return false;
        }

        internal async Task<long> RowsCount(int customerId)
        {
            try
            {
                var collection = mongoDatabase.GetCollection<Sample>(typeof (Sample).Name);
                var aggregate = collection.Aggregate()
                    .Match(new BsonDocument {{"CustomerID", customerId}})
                    .Group(new BsonDocument {{"_id", 0}, {"count",new BsonDocument("$sum", 1)}});
                var results = await aggregate.ToListAsync();
                var x = results.FirstOrDefault();
                var count = 0;
                if (x == null) return count;
                foreach (var elem in x.Elements.Where(elem => elem.Name == "count"))
                {
                    count = elem.Value.AsInt32;
                }
                return count;
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("[Mongo API] Fail in RowsCount", this, ex);
            }
            return 0;
//            { $match: {"CustomerID":19}},
//            { $group: { _id: null, count: { $sum: 1 } }}

//            var query = new MongoQuery<Sample>(mongoDatabase);
//            query.AddParameter("CustomerID", customerId, MongoQuery<Sample>.QueryCond.EQ);
//            var count = query.Execute().Count();
//            return count;
        }

        internal async Task<DateTime> CustomerLastSample(int customerId)
        {
            int lastTime;
            if (Common.LastSample.TryGetValue(customerId, out lastTime))
                return new DateTime(1970, 1, 1, 0, 0, 0).AddSeconds(lastTime);
            try
            {
                var collection = mongoDatabase.GetCollection<Sample>(typeof (Sample).Name);

                var builder = Builders<Sample>.Filter;
                var filter = builder.Eq("CustomerID", customerId);
                var results =
                    await collection.Find(filter).SortByDescending(x => x.SampleTime).Limit(1).FirstOrDefaultAsync();

                if (results != null)
                {
                    Common.LastSample.TryAdd(customerId, results.SampleTime);
                    var dt = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
                    dt = dt.AddSeconds(results.SampleTime);
                    return dt;
                }

            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("[Mongo API] Fail in CustomerLastSample", this, ex);
            }
            return DateTime.MinValue;

//            var query = new MongoQuery<Sample>(mongoDatabase);
//            query.AddParameter("CustomerID", customerId, MongoQuery<Sample>.QueryCond.EQ);
//            return getMaxSampleInUTC(customerId, query);
        }

        internal async Task<double> LastSampleValue(int serialNumber, int sensorTypeID)
        {

            var collection = mongoDatabase.GetCollection<Sample>(typeof(Sample).Name);
            var builder = Builders<Sample>.Filter;
            var filter = builder.Eq("SerialNumber", serialNumber) & builder.Eq("SensorTypeID", sensorTypeID);
            var result = await collection.Find(filter).Limit(1).ToListAsync();
            if (result.Any())
            {
                var firstOrDefault = result.FirstOrDefault();
                if (firstOrDefault != null) return firstOrDefault.Value;
            }

            return 0;

//            var query = new MongoQuery<Sample>(mongoDatabase);
//            query.AddParameter("SerialNumber", serialNumber, MongoQuery<Sample>.QueryCond.EQ);
//            query.AddParameter("SensorTypeID", sensorTypeID, MongoQuery<Sample>.QueryCond.EQ);
//
//            var result = query.Execute().SetSortOrder(SortBy.Descending("SampleTime")).SetLimit(1);
//
//            if (result.Any())
//                return result.ToList()[0].Value;
//
//            return 0;
        }

        internal async Task<List<dynamic>> Find(int serialNumber, int sensorTypeID)
        {
            try
            {

                var dt = DateTime.Now;
                var todayStartTime =
                    Convert.ToInt32(
                        new DateTime(dt.Year, dt.Month, dt.Day, 0, 0, 0).Subtract(new DateTime(1970, 1, 1, 0, 0, 0))
                            .TotalSeconds);
                var todayNow = Convert.ToInt32(dt.Subtract(new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds);

                var collection = mongoDatabase.GetCollection<Sample>(typeof (Sample).Name);
                var builder = Builders<Sample>.Filter;
                var filter = builder.Eq("SerialNumber", serialNumber) & 
                    builder.Eq("SensorTypeID", sensorTypeID)& 
                    builder.Gte("SampleTime", todayStartTime) & 
                    builder.Lte("SampleTime", todayNow) &
                    builder.Eq("IsDummy",0);
                var result =
                    await collection.Find(filter).SortByDescending(time => time.SampleTime).Limit(150).ToListAsync();
                var x = result.OrderBy(a => a.SampleTime).ToList();
                return x.ToList<dynamic>();

//                var query = new MongoQuery<Sample>(mongoDatabase);
//                query.AddParameter("SerialNumber", serialNumber, MongoQuery<Sample>.QueryCond.EQ);
//                query.AddParameter("SensorTypeID", sensorTypeID, MongoQuery<Sample>.QueryCond.EQ);
//                query.AddParameter("SampleTime", todayStartTime, MongoQuery<Sample>.QueryCond.GTE);
//                query.AddParameter("SampleTime", todayNow, MongoQuery<Sample>.QueryCond.LTE);
//
//                var result = query.Execute().SetSortOrder(SortBy.Descending("SampleTime")).SetLimit(150);
//                var x = result.OrderBy(a => a.SampleTime).ToList();
//                return x.ToList<dynamic>();
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("[Mongo API] Fail in Find", this, ex);
            }
            return null;
        }

        internal async Task<List<dynamic>> Find(int serialNumber, int sensorTypeID, bool lastDownloadData = true,
            short alarmStatus = 0, bool last24Hours = true)
        {
            try
            {
                var collection = mongoDatabase.GetCollection<Sample>(typeof (Sample).Name);
                var builder = Builders<Sample>.Filter;

                var sampleTimeDownload = 0;
                var sample24Start = 0;
                var sample24End = 0;

                var filter = builder.Eq("SerialNumber", serialNumber) & builder.Eq("SensorTypeID", sensorTypeID) &
                    builder.Eq("IsDummy", 0);



                //checks if need only the last download data
                if (lastDownloadData)
                {
                    //get the last download start and end time
                    var lastDownloadParams = DataManager.Instance.GetLastSensorDownload(serialNumber, sensorTypeID);
                    //checks if found any
                    if (lastDownloadParams != null
                        && ((IDictionary<string, Object>) lastDownloadParams).ContainsKey("FirstDownloadTime")
                        && ((IDictionary<string, Object>) lastDownloadParams).ContainsKey("LastDownloadTime"))
                    {
                        //add to the condition
                        //builder.And(builder.Gte("SampleTime", Convert.ToInt32(lastDownloadParams.FirstDownloadTime)));
                        sampleTimeDownload = Convert.ToInt32(lastDownloadParams.FirstDownloadTime);
                    }
                    
                }
                
                if (alarmStatus > 0)
                {
                    builder.And(builder.Eq("AlarmStatus", alarmStatus));
                }

                if (last24Hours)
                {
                    var dt = DateTime.Now;
                    var last24StartTime =
                        Convert.ToInt32((dt.AddHours(-24).Subtract(new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds));
                    var todayNow = Convert.ToInt32(dt.Subtract(new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds);

                    sample24Start = last24StartTime;
                    sample24End = todayNow;
                }

                if (lastDownloadData)
                    filter = builder.Eq("SerialNumber", serialNumber) & builder.Eq("SensorTypeID", sensorTypeID) &
                             builder.Eq("IsDummy", 0) & builder.Eq("SampleTime", sampleTimeDownload);
                
                if(last24Hours)
                    filter = builder.Eq("SerialNumber", serialNumber) & builder.Eq("SensorTypeID", sensorTypeID) &
                            builder.Eq("IsDummy", 0) & builder.Gte("SampleTime", sample24Start) &
                            builder.Lte("SampleTime", sample24End);

                //var result = query.Execute().SetSortOrder(SortBy.Descending("SampleTime")).SetLimit(150);
                var result =
                    await collection.Find(filter).SortByDescending(time => time.SampleTime).Limit(150).ToListAsync();
                var x = result.OrderBy(a => a.SampleTime).ToList();
                return x.ToList<dynamic>();
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("[Mongo API] Fail in Find", this, ex);
            }
            return null;
        }

        internal async Task<List<Sample>> FindPart(int serialNumber, int sensorTypeID, int startTime, int endTime, int offset=0, int count=-1)
        {
            try
            {

                var collection = mongoDatabase.GetCollection<Sample>(typeof(Sample).Name);
                var builder = Builders<Sample>.Filter;
                var filter = builder.Eq("SerialNumber", serialNumber) & builder.Eq("SensorTypeID", sensorTypeID)
                    & builder.Gte("SampleTime", startTime) & builder.Lte("SampleTime", endTime)
                    & builder.Eq("IsDummy", 0);


//                var query = new MongoQuery<Sample>(mongoDatabase);
//                query.AddParameter("SerialNumber", serialNumber, MongoQuery<Sample>.QueryCond.EQ);
//                query.AddParameter("SensorTypeID", sensorTypeID, MongoQuery<Sample>.QueryCond.EQ);
//                query.AddParameter("SampleTime", startTime, MongoQuery<Sample>.QueryCond.GTE);
//                query.AddParameter("SampleTime", endTime, MongoQuery<Sample>.QueryCond.LTE);
//                query.AddParameter("IsDummy", 0, MongoQuery<Sample>.QueryCond.EQ);

               var cursor = await collection.Find(filter).SortBy(time => time.SampleTime).ToCursorAsync();

                //                    cursorList.ForEach((c)=>
                //                    {
                //                        c.MoveNextAsync();
                //                        foreach (var sample in c.Current)
                //                        {
                //                            Console.WriteLine(sample.Value);
                //                        }
                //                    });

                //var cursor = query.Execute().SetSortOrder(SortBy.Ascending("SampleTime"));
                //var enumerator = cursor.GetEnumerator();

                //var result = new List<Sample>();
                var result = new ConcurrentDictionary<int,Sample>();
                var bContinue = true;
                while (await cursor.MoveNextAsync())
                {
                    foreach (var sample in cursor.Current)
                    {
                        if (offset > 0)
                            offset--;
                        else if (result.Count < count || count < 0)
                        {
                            Sample tempSample;
                            if(!result.TryGetValue(sample.SampleTime, out tempSample))
                                result.TryAdd(sample.SampleTime, sample);
                        }
                        else
                        {
                            bContinue = false;
                            break;
                        }
                    }
                    if(!bContinue)
                        break;
                }
                return result.Values.ToList();
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("[Mongo API] Fail in FindPart", this, ex);
            }
            return null;
        }

        internal async Task<bool> BuildSampleAggregator(int serialNumber, int sensorTypeID, int startTime,
            int endTime, ISampleAggregator aggregator)
        {
            try
            {

                var collection = mongoDatabase.GetCollection<Sample>(typeof (Sample).Name);
                var builder = Builders<Sample>.Filter;
                var filter = builder.Eq("SerialNumber", serialNumber) & builder.Eq("SensorTypeID", sensorTypeID)
                             & builder.Gte("SampleTime", startTime) & builder.Lte("SampleTime", endTime)
                             & builder.Eq("IsDummy", 0);

                var aggregate = collection.Aggregate()
                    .Match(new BsonDocument
                    {
                        {"SerialNumber", serialNumber},
                        {"SensorTypeID", sensorTypeID},
                        {"IsDummy", 0},
                        {
                            "SampleTime", new BsonDocument
                            {
                                {
                                    "$gte", startTime
                                },
                                {
                                    "$lte", endTime
                                }
                            }
                        }
                    })
                    .Project(new BsonDocument
                    {
                        {"sampleTime", "$SampleTime"},
                        {"sampleValue","$Value"}
                    });
                var results = await aggregate.ToListAsync();

                results.ForEach(x =>
                {
                    aggregator.Add((uint)x["sampleTime"].AsInt32, x["sampleValue"].AsDouble);  
                });

//                MongoQuery<Sample> query = new MongoQuery<Sample>(mongoDatabase);
//                query.AddParameter("SerialNumber", serialNumber, MongoQuery<Sample>.QueryCond.EQ);
//                query.AddParameter("SensorTypeID", sensorTypeID, MongoQuery<Sample>.QueryCond.EQ);
//                query.AddParameter("SampleTime", startTime, MongoQuery<Sample>.QueryCond.GTE);
//                query.AddParameter("SampleTime", endTime, MongoQuery<Sample>.QueryCond.LTE);
//                query.AddParameter("IsDummy", 0, MongoQuery<Sample>.QueryCond.EQ);

//                MongoCursor<Sample> cursor = query.Execute().SetSortOrder(SortBy.Ascending("SampleTime"));

//                var samples = collection.Find(filter);
                //var cursor = await collection.Find(filter).SortBy(time => time.SampleTime).ToCursorAsync();

                //samples.As<ISampleAggregator>()

//                await samples.ForEachAsync(x =>
//                {
//                    aggregator.Add((uint)x.SampleTime, x.Value);  
                    
//                });
//                var enumerator = cursor.GetEnumerator();
//                if (cursor.Any())
//                {
//                    while (enumerator.MoveNext())
//                    while(await cursor.MoveNextAsync())
//                    {
//                        foreach (var sample in sampleList)
//                        {
//                            aggregator.Add((uint)sample.SampleTime, sample.Value);  
//                        }
//                        var sample = enumerator.Current;
//                        if (sample != null)
//                            aggregator.Add((uint) sample.SampleTime, sample.Value);
//                    }
                    return true;
//                }
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("[Mongo API] Fail in BuildSampleAggregator", this, ex);
            }
            return false;
        }

        internal async Task<List<Tuple<int,double>>> FindReduced(int serialNumber, int sensorTypeID, int startTime, int endTime, int maxCount)
        {
            var result = new List<Tuple<int, double>>();
            const int tolerance = 25;
            var threshold = maxCount/tolerance;
            try
            {

                var collection = mongoDatabase.GetCollection<Sample>(typeof(Sample).Name);
                var builder = Builders<Sample>.Filter;
                var filter = builder.Eq("SerialNumber", serialNumber) & builder.Eq("SensorTypeID", sensorTypeID)
                             & builder.Gte("SampleTime", startTime) & builder.Lte("SampleTime", endTime)
                             & builder.Eq("IsDummy", 0);
                
                
//                var query = new MongoQuery<Sample>(mongoDatabase);
//
//                query.AddParameter("SerialNumber", serialNumber, MongoQuery<Sample>.QueryCond.EQ);
//                query.AddParameter("SensorTypeID", sensorTypeID, MongoQuery<Sample>.QueryCond.EQ);
//                query.AddParameter("SampleTime", startTime, MongoQuery<Sample>.QueryCond.GTE);
//                query.AddParameter("SampleTime", endTime, MongoQuery<Sample>.QueryCond.LTE);
//                query.AddParameter("IsDummy", 0, MongoQuery<Sample>.QueryCond.EQ);

//                var cursor = query.Execute().SetSortOrder(SortBy.Ascending("SampleTime"));
                var cursor = await collection.Find(filter).SortBy(time => time.SampleTime).FirstOrDefaultAsync();//  .ToCursorAsync();// .ToListAsync();
                //await cursor.MoveNextAsync();
                //if (cursor.Any())
                if (cursor!=null)
                {
                    var firstSample = cursor;//cursor.Current.First();
                    //get the first sample time
                    var firstSampleTime = firstSample.SampleTime;
                    var deltaTime = endTime - firstSampleTime;
                    //calculate the amount of seconds to split for parallel tasks
                    var deltaToAdd = Convert.ToInt32(Math.Floor((double)deltaTime / tolerance));
                    var totalSamples = new List<List<Tuple<int, double>>>();
                    var currentTime = firstSampleTime;
                    //var cursorList = new List<MongoCursor<Sample>>();
                    var cursorList = new List<IFindFluent<Sample,Sample>>();

                    while (currentTime <= endTime)
                    {
                        var destTime = currentTime + deltaToAdd;

//                        query = new MongoQuery<Sample>(mongoDatabase);
//                        query.AddParameter("SerialNumber", serialNumber, MongoQuery<Sample>.QueryCond.EQ);
//                        query.AddParameter("SensorTypeID", sensorTypeID, MongoQuery<Sample>.QueryCond.EQ);
//                        query.AddParameter("SampleTime", currentTime, MongoQuery<Sample>.QueryCond.GTE);
//                        query.AddParameter("SampleTime", destTime, MongoQuery<Sample>.QueryCond.LTE);
//                        query.AddParameter("IsDummy", 0, MongoQuery<Sample>.QueryCond.EQ);

                        filter = builder.Eq("SerialNumber", serialNumber) & builder.Eq("SensorTypeID", sensorTypeID)
                            & builder.Gte("SampleTime", currentTime) & builder.Lte("SampleTime", destTime)
                            & builder.Eq("IsDummy", 0);

//                        var cursorLocal = query.Execute().SetSortOrder(SortBy.Ascending("SampleTime"));
                       // var cursorLocal = await collection.Find(filter).SortBy(time => time.SampleTime).ToCursorAsync();
                        var list = collection.Find(filter);
                        
//                        if (cursorLocal.FirstOrDefault() != null)
                        cursorList.Add(list);

                        currentTime = destTime + 1;
                    }

                    var algorithm = new RdpAlgorithm();
//                    cursorList.ForEach(c=>
//                    {
//                        totalSamples.Add(algorithm.GetDownSamples(c, threshold).ToList());
//                    });

                    Parallel.ForEach(cursorList,
                        cursorItem =>
                        {
                            //var algorithm = new RdpAlgorithm();
                            totalSamples.Add(algorithm.GetDownSamples(cursorItem, threshold).ToList());
                        });

                    result = totalSamples.SelectMany(l => l).Distinct().OrderBy(o => o.Item1).ToList();
                }
                return result;
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("[Mongo API] Fail in FindReduced", this, ex);
            }
            return null;
        }

        internal async Task<dynamic> GetSensorStatistics(int serialNumber, int sensorTypeID, int startTime, int endTime)
        {
            try
            {
                var collection = mongoDatabase.GetCollection<Sample>(typeof (Sample).Name);
//                var builder = Builders<Sample>.Filter;
//                var filter = builder.Eq("SerialNumber", serialNumber) & builder.Eq("SensorTypeID", sensorTypeID)
//                             & builder.Gte("SampleTime", startTime) & builder.Lte("SampleTime", endTime)
//                             & builder.Eq("IsDummy", 0);

                

                var minTime = 0;
                var maxTime = 0;
                double minValue = 0;
                double maxValue = 0;
                double sum = 0;
                var count = 0;

                var aggregate = collection.Aggregate()
                    .Match(new BsonDocument
                    {
                        {"SerialNumber", serialNumber},
                        {"SensorTypeID", sensorTypeID},
                        {"IsDummy", 0},
                        {
                            "SampleTime", new BsonDocument
                            {
                                {
                                    "$gte", startTime
                                },
                                {
                                    "$lte", endTime
                                }
                            }
                        }
                    })
                    .Group(new BsonDocument
                    {
                        {"_id", 0},
                        {"count", new BsonDocument("$sum", 1)},
                        {"minTime", new BsonDocument("$min", "$SampleTime")},
                        {"maxTime", new BsonDocument("$max", "$SampleTime")},
                        {"minValue", new BsonDocument("$min", "$Value")},
                        {"maxValue", new BsonDocument("$max", "$Value")},
                        {"sumValue", new BsonDocument("$sum", "$Value")}
                    });
                  
		
                var results = await aggregate.ToListAsync();
                var x = results.FirstOrDefault();
                if (x != null)
                {
                    foreach (var elem in x.Elements)
                    {
                        if (elem.Name == "count")
                            count = elem.Value.AsInt32;
                        if (elem.Name == "minTime")
                            minTime = elem.Value.AsInt32;
                        if (elem.Name == "maxTime")
                            maxTime = elem.Value.AsInt32;
                        if (elem.Name == "minValue")
                            minValue = elem.Value.AsDouble;
                        if (elem.Name == "maxValue")
                            maxValue = elem.Value.AsDouble;
                        if (elem.Name == "sumValue")
                            sum = elem.Value.AsDouble;
                    }
                }


//                var list = await collection.Find(filter).SortBy(time => time.SampleTime).ToListAsync();

//                 count = list.Count();
//                 minTime = list.Min(x => x.SampleTime);
//                 maxTime = list.Max(x => x.SampleTime);
//                 minValue = list.Min(x => x.Value);
//                 maxValue = list.Max(x => x.Value);
//                 sum = list.Sum(x => x.Value);


//                var query = new MongoQuery<Sample>(mongoDatabase);
//                query.AddParameter("SerialNumber", serialNumber, MongoQuery<Sample>.QueryCond.EQ);
//                query.AddParameter("SensorTypeID", sensorTypeID, MongoQuery<Sample>.QueryCond.EQ);
//                query.AddParameter("SampleTime", startTime, MongoQuery<Sample>.QueryCond.GTE);
//                query.AddParameter("SampleTime", endTime, MongoQuery<Sample>.QueryCond.LTE);
//                query.AddParameter("IsDummy", 0, MongoQuery<Sample>.QueryCond.EQ);
//
//                var cursor = query.Execute();
//                var enumerator = cursor.GetEnumerator();
//
//                var count = cursor.Count();
//                var min_time = cursor.Min(x=>x.SampleTime);
//                var maxTime = cursor.Max(x => x.SampleTime);
//                var min_value = cursor.Min(x => x.Value);
//                var maxValue = cursor.Max(x => x.Value);
//                var sum = cursor.Sum(x => x.Value);
//
//                while (enumerator.MoveNext())
//                {
//                    var sample = enumerator.Current;
//                    min_time = Math.Min(min_time, sample.SampleTime);
//                    maxTime = Math.Max(maxTime, sample.SampleTime);
//                    min_value = Math.Min(min_value, sample.Value);
//                    maxValue = Math.Max(maxValue, sample.Value);
//                    sum += sample.Value;
//                    count++;
//                }

                dynamic data = new ExpandoObject();
                if (count == 0)
                {
                    data.start = "";
                    data.end = "";
                    data.avg = "";
                    data.min = "";
                    data.max = "";
                }
                else
                {
                    data.start = (ulong) minTime*1000;
                    data.end = (ulong) maxTime*1000;
                    data.avg = sum/count;
                    data.min = minValue;
                    data.max = maxValue;
                }
                data.numOfSamples = count;
                //serial number, sensor name, device name
                data.serialNumber = serialNumber;
                var sensorData = DataManager.Instance.GetDeviceSensorsID(serialNumber);
                foreach (
                    var sensor in
                        sensorData.Where(sensor => Convert.ToInt32(sensor.SensorTypeID.ToString()) == sensorTypeID))
                {
                    data.sensorName = sensor.SensorName;
                    data.deviceName = sensor.DeviceName;
                    break;
                }
                return data;
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("[Mongo API] Fail in GetSensorStatistics", this, ex);
            }
            return null;
        }

        internal byte[] Backup(int customerId, bool wipeData = false, int startTime = 0, int endTime = 0)
        {
            Log.Instance.Write("[Mongo API] Start backup of MongoDB to {0}", this, Log.LogSeverity.DEBUG, backupPath);
            try
            {
                //check the mongo path
                if (mongoPath.Length > 0)
                {
                    //--c -> collection
                    //--d -> database
                    //--o -> output
                    //--q -> query
                    var timeStampBackup =
                        Convert.ToInt32(DateTime.Now.Subtract(new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds);
                    var backupForCustomer = string.Format("{0}\\{1}\\{2}", backupPath, customerId, timeStampBackup);
                    if (!Directory.Exists(backupPath + @"\" + customerId))
                        Directory.CreateDirectory(backupPath + @"\" + customerId);
                    if (!Directory.Exists(backupForCustomer))
                        Directory.CreateDirectory(backupForCustomer);
//                    using (var file = File.Create(backupForCustomer))
//                    {
//
//                    }
                    //build the args to send to the mongodump process
                    var args = string.Format(" --collection Sample --db Loggers --out {0}", backupForCustomer);
                    //checks if need to filter by sample time
                    var currentUnixTime =
                        Convert.ToInt32(DateTime.Now.Subtract(new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds);
                    startTime = startTime == 0 ? 1 : startTime;
                    endTime = endTime == 0 ? currentUnixTime : endTime;
                    args += " --query \"{ CustomerID:{$eq:" + customerId + "},  SampleTime: { $gte: " + startTime +
                            ", $lte: " + endTime + " } }\"";

                    Log.Instance.Write("[Mongo API] mongodump args - {0}", this, Log.LogSeverity.DEBUG, args);
                    //run mongodump
                    var proc = new Process
                    {
                        StartInfo = new ProcessStartInfo
                        {
                            FileName = mongoPath + "\\mongodump.exe",
                            Arguments = args,
                            UseShellExecute = true,
                            CreateNoWindow = true,
                            WorkingDirectory = mongoPath,
                            ErrorDialog=false,
                            WindowStyle = ProcessWindowStyle.Hidden,
                            //RedirectStandardOutput = true
                        }
                    };
                    proc.Start();
                    //proc.BeginOutputReadLine();
                    // To avoid deadlocks, always read the output stream first and then wait.
                    //File.WriteAllText(backupForCustomer, proc.StandardOutput.ReadToEnd());
                    proc.WaitForExit();
                    proc.Close();

                    Log.Instance.Write("[Mongo API] mongodump is done OK", this);

                    //upload the backup file to S3 so it can be downloaded to the client by using a link
                    return normalBackup(customerId, timeStampBackup);

                    //zip the folder of backup and return his link from S3
                    //return zipBackup(customerId, backupForCustomer);

                    //checks if needs to remove all data
//                        if (wipeData)
//                        {
//                            Log.Instance.Write("[Mongo API] Gonna remove all data from mongo", this);
//                            //run a command to wipe out all data
//                            var collection = mongoDatabase.GetCollection<Sample>(typeof(Sample).Name);
//                            collection.RemoveAll();
//                        }
//                        Log.Instance.Write("[Mongo API] return success of backup", this);
//                        return true;
                }
            }
            catch(Exception ex)
            {
                Log.Instance.WriteError("[Mongo API] Fail in Backup", this, ex);
            }
            return null;
        }

        internal bool Backup(List<int> serialNumbers)
        {
            Log.Instance.Write("[Mongo API] Start backup of MongoDB to {0}", this, Log4Tech.Log.LogSeverity.DEBUG, backupPath);
            try
            {
                 //check the mongo path
                if (mongoPath.Length > 0)
                {
                    //build the args to send to the mongodump process
                    string args = string.Format(" --collection Sample --db Loggers --out {0}", backupPath);
                    args += string.Format(" -query { SampleTime: { $in: {0} } }", serialNumbers.ToArray());

                    Log4Tech.Log.Instance.Write("[Mongo API] mongodump args - {0}", this, Log4Tech.Log.LogSeverity.DEBUG, args);
                    //run mongodump
                    var proc = new Process
                    {
                        StartInfo = new ProcessStartInfo
                        {
                            FileName = mongoPath + "\\mongodump.exe",
                            Arguments = args,
                            UseShellExecute = true,
                            CreateNoWindow = true,
                            WorkingDirectory = mongoPath,
                            ErrorDialog = false,
                            WindowStyle = ProcessWindowStyle.Hidden
                        }
                    };
                    proc.Exited += proc_Exited;

                    bool bResult = false;
                    var task = Task.Factory.StartNew(() =>
                    {
                        bResult = proc.Start();
                        proc.WaitForExit();
                    });

                    Task.WaitAll(task);

                    if (proc.ExitCode == 0)
                    {
                        Log4Tech.Log.Instance.Write("[Mongo API] mongodump is done OK", this);
                        return true;
                    }
                }
            }
            catch(Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in Backup", this, ex);
            }
            return false;
        }

        internal bool Restore(int customerId,Stream backupFile)
        {
            try
            {
                //check the mongo path
                if (mongoPath.Length > 0)
                {
//                    var zipFolder = string.Format(@"{0}\zipBackups", backupPath);
//                    var zipBackupsFolder = string.Format(@"{0}\{1}",zipFolder,customerId);
//                    if (!Directory.Exists(zipBackupsFolder))
//                    {
//                        if (!Directory.Exists(zipFolder))
//                            Directory.CreateDirectory(zipFolder);
//                        if (!Directory.Exists(zipBackupsFolder))
//                            Directory.CreateDirectory(zipBackupsFolder);
//                    }
                    
//                    var zipPath = string.Format(@"{0}\{1}.zip",zipBackupsFolder,unixTimeStamp);
//                    using (var fileStream = File.Create(zipPath))
//                    {
//                        zipFile.Seek(0, SeekOrigin.Begin);
//                        zipFile.CopyTo(fileStream);
//                    }
//
//                    ZipFile.ExtractToDirectory(zipPath, zipBackupsFolder);
                    var unixTimeStamp =
                            Convert.ToInt32(DateTime.Now.Subtract(new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds);
                    var restorePath = string.Format(@"{0}\restore\{1}\{2}\restore.bson",backupPath,customerId,unixTimeStamp);
                    if (!Directory.Exists(string.Format(@"{0}\restore", backupPath)))
                        Directory.CreateDirectory(string.Format(@"{0}\restore", backupPath));
                    if (!Directory.Exists(string.Format(@"{0}\restore\{1}", backupPath, customerId)))
                        Directory.CreateDirectory(string.Format(@"{0}\restore\{1}", backupPath, customerId));
                    if (!Directory.Exists(string.Format(@"{0}\restore\{1}\{2}", backupPath, customerId, unixTimeStamp)))
                        Directory.CreateDirectory(string.Format(@"{0}\restore\{1}\{2}", backupPath, customerId, unixTimeStamp));
                    using (var fileStream = File.Create(restorePath))
                    {
                        backupFile.Seek(0, SeekOrigin.Begin);
                        backupFile.CopyTo(fileStream);
                    }
                   
                    var connection = ConfigurationManager.AppSettings["mongoConnection"];
                    var mongoConnectionHost = connection.Substring(10, connection.LastIndexOf('/') - 1);

                    var args =
                        string.Format(
                            "--host {1} --collection Sample --db Loggers --maintainInsertionOrder --keepIndexVersion {0}",
                            restorePath, mongoConnectionHost);
                    Log.Instance.Write("[Mongo API] mongorestore args -> {0}", this, Log.LogSeverity.DEBUG, args);
                    //run mongo restore
                    var proc = new Process
                    {
                        StartInfo = new ProcessStartInfo
                        {
                            FileName = mongoPath + "\\mongorestore.exe",
                            Arguments = args,
                            UseShellExecute = true,
                            CreateNoWindow = true,
                            WorkingDirectory = mongoPath,
                            ErrorDialog = false,
                            WindowStyle = ProcessWindowStyle.Hidden
                        }
                    };
                    proc.Start();

                    proc.WaitForExit();
                    proc.Close();


                    Log.Instance.Write("[Mongo API] mongo restore is done OK", this);
                    return true;

                }
            }
            catch(Exception ex)
            {
                Log.Instance.WriteError("[MongoDB] Fail in Restore", this, ex);
            }
            return false;
        }

        internal bool WipeData(List<int> serialNumbers)
        {
//            Log4Tech.Log.Instance.Write("[Mongo API] Wipe data from mongo", this);
//            try
//            {
//                //run a command to wipe out all data
//                var collection = mongoDatabase.GetCollection<Sample>(typeof(Sample).Name);
//                
//                collection.Remove(Query.In("SerialNumber",serialNumbers.ToBsonDocument().AsBsonArray), RemoveFlags.None);
//
//                return true;
//            }
//            catch(Exception ex)
//            {
//                Log4Tech.Log.Instance.WriteError("[Mongo API] Fail in WipeData", this, ex);
//            }
//            return false;
            return false;
        }
        #endregion
        #region private methods

        private byte[] normalBackup(int customerId, int unixTimeStamp)
        {
            try
            {
                var backupFile = string.Format(@"{0}\{1}\{2}\Loggers\Sample.bson", backupPath, customerId,
                    unixTimeStamp);

                using (var fs = File.Open(backupFile, FileMode.Open))
                {
                    using (var ms = new MemoryStream())
                    {
                        fs.CopyTo(ms);
                        return ms.ToArray();
                    }
                }
            }
            catch
            {
                // ignored
            }
            return null;
        }

        private string zipBackup(int customerId, string backupForCustomer, int unixTimeStamp)
        {
            var keyPath = string.Format("{0}/{1}/backup-{2}", customerId,
                       DateTime.Now.ToString("yyyy-MM-dd"), unixTimeStamp);
            var destinationZipFile = string.Format(@"{0}\{1}\backup-{2}.zip", backupPath, customerId,
                       unixTimeStamp);
            try
            {
                ZipFile.CreateFromDirectory(backupForCustomer, destinationZipFile);
                var backupFile = string.Format(@"{0}", destinationZipFile);
                using (var ms = File.Open(backupFile, FileMode.Open))
                {
                    //take the file dumped and upload to S3
                    var linkToDownload = S3Manager.Instance.UploadDataBackup(keyPath, ms);
                    //send back the link so the end-user can download the file
                    //allow up to 7 days to download this file
                    return linkToDownload;
                }
            }
            catch
            {
                // ignored
            }
            return string.Empty;
        }

//        private DateTime getMaxSampleInUTC(int customerId, MongoQuery<Sample> query)
//        {
//            try
//            {
//                var result = query.Execute().SetSortOrder(SortBy.Descending("SampleTime")).SetLimit(1);
//                //var timestamp = result.Last().SampleTime;
//                var timestamp = 0;
//                foreach (var sample in result)
//                {
//                    timestamp = sample.SampleTime;
//                }
//
//                Common.LastSample.TryAdd(customerId, timestamp);
//
//                var dt = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
//                dt = dt.AddSeconds(timestamp);
//                return dt;
//            }
//            catch
//            {
//                // ignored
//            }
//            return DateTime.MinValue;
//        }

//        private string toJSON(MongoCursor<Sample> cursor)
//        {
//            //return cursor.ToJson(typeof(SensorSample), jsonWriterSettings);
//            return JsonConvert.SerializeObject(cursor);
//        }

        void proc_Exited(object sender, EventArgs e)
        {
            Log4Tech.Log.Instance.Write("[Mongo API] Mongo process has exited.", this);
        }
        #endregion
    }
}
