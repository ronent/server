﻿using Infrastructure;
using Infrastructure.Interfaces;
using Server.Infrastructure;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MongoStorage
{
    public class MongoAPI : ISample
    {

        private readonly Manager _manager = Manager.Instance;

        #region ISample Members

        public List<Tuple<int, double>> FindReduced(int serialNumber, int sensorTypeID, int startTime, int endTime, int maxCount)
        {
            Log4Tech.Log.Instance.Write("[Mongo API] FindReduced - #{0}, {1}, {2}, {3}, {4}", this, Log4Tech.Log.LogSeverity.INFO, serialNumber, sensorTypeID, startTime, endTime, maxCount);
            return _manager.FindReduced(serialNumber, sensorTypeID, startTime, endTime, maxCount).Result;
        }

        public List<Sample> FindPart(int serialNumber, int sensorTypeID, int startTime, int endTime, int offset = 0, int count = -1)
        {
            Log4Tech.Log.Instance.Write("[Mongo API] FindPart - #{0}, {1}, {2}, {3}, {4}, {5}", this, Log4Tech.Log.LogSeverity.INFO, serialNumber, sensorTypeID, startTime, endTime, offset, count);
            return _manager.FindPart(serialNumber, sensorTypeID, startTime, endTime, offset, count).Result;
        }
        
        /// <summary>
        /// find the logs of sensor
        /// </summary>
        /// <param name="deviceSensorID"></param>
        /// <param name="lastDownloadData">filter by last downloaded data</param>
        /// <param name="alarmStatus">if not normalized, then filter by alarm status </param>
        /// <returns>In json format</returns>
        public List<dynamic> Find(int serialNumber, int sensorTypeID, bool lastDownloadData = true, Int16 alarmStatus = 0)
        {
            Log4Tech.Log.Instance.Write("[Mongo API] Find - #{0}, {1}, {2}, {3}", this, Log4Tech.Log.LogSeverity.INFO, serialNumber,sensorTypeID, lastDownloadData, alarmStatus);
            return _manager.Find(serialNumber, sensorTypeID, lastDownloadData, alarmStatus).Result;
        }

        /// <summary>
        /// find the logs of sensor
        /// </summary>
        /// <param name="deviceSensorID"></param>
        /// <returns>In json format</returns>
        public List<dynamic> Find(int serialNumber, int sensorTypeID)
        {
            Log4Tech.Log.Instance.Write("[Mongo API] Find - #{0}, {1}",this,  Log4Tech.Log.LogSeverity.INFO, serialNumber, sensorTypeID);
            return _manager.Find(serialNumber, sensorTypeID).Result;
        }

        public bool BuildSampleAggregator(int serialNumber, int sensorTypeID, int startTime, int endTime, ISampleAggregator aggregator)
        {
            Log4Tech.Log.Instance.Write("[Mongo API] BuildSampleAggregator - #{0}, {1}", this, Log4Tech.Log.LogSeverity.INFO, serialNumber, sensorTypeID);
            return _manager.BuildSampleAggregator(serialNumber, sensorTypeID, startTime, endTime, aggregator).Result;
        }

        public dynamic GetSensorStatistics(int serialNumber, int sensorTypeID, int startTime, int endTime)
        {
            Log4Tech.Log.Instance.Write("[Mongo API] GetSensorStatistics - #{0}, {1}", this, Log4Tech.Log.LogSeverity.INFO, serialNumber, sensorTypeID);
            return _manager.GetSensorStatistics(serialNumber, sensorTypeID, startTime, endTime).Result;
        }

        /// <summary>
        /// returns the amount of rows 
        /// </summary>
        /// <param name="deviceSensorID"></param>
        /// <returns></returns>
        public long RowsCount(int customerId)
        {
            //Log4Tech.Log.Instance.Write("[Mongo API] Rows Count - #{0}, {1}", this, Log4Tech.Log.LogSeverity.INFO, serialNumber, sensorTypeID);
            return _manager.RowsCount(customerId).Result;
        }

        public DateTime CustomerLastSample(int customerId)
        {
            return _manager.CustomerLastSample(customerId).Result;
        }

//        public DateTime LastSampleUTC(int customerId)
//        {
//            return _manager.LastSampleUTC(customerId);
//        }

        public double LastSampleValue(int serialNumber, int sensorTypeID)
        {
            return _manager.LastSampleValue(serialNumber, sensorTypeID).Result;
        }

        /// <summary>
        /// Insert new entity to the document in the collection (eg. insert new row to a table)
        /// </summary>
        /// <param name="value">value of the sensor log</param>
        /// <param name="sampleTime">the timestamp of the sensor log</param>
        /// <param name="isTimeStamp">indicate if the value is timestamp</param>
        /// <param name="timeStampComment">if exists for this timestamp then provide it</param>
        /// <param name="alarmStatus">status of the alarm on the current sensor log value</param>
        /// <returns>on success - true</returns>
        public bool InsertSample(Sample sample)
        {
            Log4Tech.Log.Instance.Write("[Mongo API] Insert Sample - #{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}", this, Log4Tech.Log.LogSeverity.INFO, 
                sample.SerialNumber , sample.SensorTypeID, sample.Value, sample.SampleTime, sample.IsTimeStamp, sample.TimeStampComment, sample.AlarmStatus, sample.IsDummy);
            return _manager.InsertSample(sample).Result;
        }
        /// <summary>
        /// <param name="deviceSensorID"></param>
        /// <param name="value">value of the sensor log</param>
        /// <param name="sampleTime">the timestamp of the sensor log</param>
        /// <param name="isTimeStamp">indicate if the value is timestamp</param>
        /// <param name="timeStampComment">if exists for this timestamp then provide it</param>
        /// <param name="alarmStatus">status of the alarm on the current sensor log value</param>
        /// </summary>
        /// <param name="samples"></param>
        /// <returns></returns>
        public bool InsertBatch(List<Sample> samples)
        {
            Log4Tech.Log.Instance.Write("[Mongo API] Insert Batch #{0} samples", this, Log4Tech.Log.LogSeverity.INFO, samples.Count);
            return _manager.InsertBatch(samples).Result;
        }

        /// <summary>
        /// Backups all data in between the dates provided, if none is provided then backup the entire data
        /// </summary>
        /// <param name="wipeData">true for delete the data permently</param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public byte[] Backup(int customerId, bool wipeData = false, int startTime = 0, int endTime = 0)
        {
            return _manager.Backup(customerId, wipeData, startTime, endTime);
        }

        public bool Backup(List<int> serialNumbers)
        {
            return _manager.Backup(serialNumbers);
        }

        /// <summary>
        /// Restore the backup of mongo database to the current running mongo database
        /// </summary>
        /// <returns>on success return true</returns>
        public bool Restore(int customerId, Stream zipFile)
        {
            return _manager.Restore(customerId,zipFile);
        }

        public bool WipeData(List<int> serialNumbers)
        {
            return _manager.WipeData(serialNumbers);
        }
        #endregion
    }
}
