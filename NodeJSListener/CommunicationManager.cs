﻿using Infrastructure;
using Infrastructure.Interfaces;
using Log4Tech;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace NodeJSListener
{
   /* public class CommunicationManager : IListener
    {
        #region fields
       
        protected ConcurrentDictionary<string, SocketManager> clientScokets;
        private bool keepRunning = false;
        private Int16 port;
        #endregion
        #region ctor
        public CommunicationManager()
        {
            keepRunning = true;
            port = 4000;
            clientScokets = new ConcurrentDictionary<string, SocketManager>();
        }
        #endregion
        #region private
        private void removeSocket(string id)
        {
            Log4Tech.Log.Instance.Write("Socket is closed: {0}", this, Log4Tech.Log.LogSeverity.DEBUG,  id);

            SocketManager removedSocket;
            if (clientScokets.TryRemove(id, out removedSocket))
                removedSocket.Dispose();
        }

        #endregion

        #region IListener Members
        public void Start()
        {
            try
            {
                IPAddress localIP = IPAddress.Any;
                TcpListener listener = new TcpListener(localIP, port);

                listener.Server.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, true);
                listener.Start();

                Task.Factory.StartNew(async () =>
                {
                    while (keepRunning)
                    {
                        try
                        {
                            var tcpClient = await listener.AcceptTcpClientAsync();
                            setSocketProperties(tcpClient);
                            var manager = new SocketManager(tcpClient.GetStream(), this);
                            manager.waitForData();
                        }
                        catch (Exception ex)
                        {
                            Log.Instance.WriteError("Fail in StartListen", this, ex);
                        }
                    }
                }, TaskCreationOptions.LongRunning);
                
            }
            catch(Exception ex) 
            {
                Log.Instance.WriteError("Fail in Start",this, ex);
            }
        }

        private void setSocketProperties(TcpClient client)
        {
            client.NoDelay = true;
            client.Client.NoDelay = true;
            client.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, true);
        }

        public void Stop()
        {
            throw new NotImplementedException();
        }
        #endregion
     
    }*/
}
