﻿using Infrastructure;
using Log4Tech;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace NodeJSListener
{
   /* public class SocketManager
    {
        #region fields
        
        protected CommunicationManager parent;
        private NetworkStream NodeJs;
        private readonly int MESSAGE_MAX_SIZE = 16384;
        #endregion
        #region ctor
        public SocketManager(NetworkStream nodejs, CommunicationManager communicationManager)
        {
            this.NodeJs = nodejs;
            this.parent = communicationManager;
            initialize();
        }
        #endregion
        #region public
        public void waitForData()
        {
            var resp = new byte[MESSAGE_MAX_SIZE];

            Task.Factory.StartNew(async () =>
            {
                try
                {
                    //loop till can't read from the socket
                    //this will holds the connectivity to the client
                    while (NodeJs.CanRead)
                    {
                        var bytescount = await NodeJs.ReadAsync(resp, 0, MESSAGE_MAX_SIZE);
                        if (bytescount <= 0 || !NodeJs.CanWrite) continue;
                       
                        var data = Encoding.ASCII.GetString(resp, 0, bytescount);
                        Log.Instance.Write("[NodeJS] Got from NodeJS -> {0}", this, Log.LogSeverity.DEBUG, data);
                        var response = APIManager.CommandMain.getInstance().execute(data);
                        //DumpResponse(response);
                        var arr = Encoding.ASCII.GetBytes(response);
                        //Log4Tech.Log.Instance.Write("[NodeJS] Send to NodeJS -> {0}", this, Log.LogSeverity.DEBUG, response);
                        NodeJs.WriteAsync(arr, 0, arr.Length).Wait();
                    }
                }
                catch (Exception ex)
                {
                    if (ex.Message != "Unable to read data from the transport connection: A connection attempt failed because the connected party did not properly respond after a period of time, or established connection failed because connected host has failed to respond."
                        && !ex.Message.Contains("Cannot access a disposed object."))

                        Log.Instance.WriteError("Fail in WaitForData", this, ex);
                }
            }, TaskCreationOptions.LongRunning);
        }

//        static void DumpResponse(string data)
//        {
//            var path = "c:\\dev\\dump\\" + DateTime.Now.ToString("HH-mm-ss") + ".json";
//            System.IO.File.WriteAllText(path, data);
//        }

        public void Dispose()
        {
            
        }
        #endregion
        #region private
        private void initialize()
        {
           
        }
        #endregion

      

    }*/
}
