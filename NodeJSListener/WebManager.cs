﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Infrastructure;
using Log4Tech;
using Newtonsoft.Json;
using Server.Infrastructure;

namespace NodeJSListener
{
    internal class WebManager
    {
        protected WebServer _parent;
        private HttpListenerRequest _request;
        private HttpListenerResponse _response;
        

        public WebManager(HttpListenerRequest request, HttpListenerResponse response, WebServer webServer)
        {
            _parent = webServer;
            _request = request;
            _response = response;
        }

        public byte[] HandleData()
        {
            //checks if the requset has body
            //get the request data
            try
            {
                var data = new StreamReader(_request.InputStream,
                    _request.ContentEncoding).ReadToEnd();
                if (_request.ContentType != null )
                {
                    var contentType = _request.ContentType;
                    if (_request.ContentType.IndexOf(';') > 0)
                        contentType = _request.ContentType.Substring(0, _request.ContentType.IndexOf(';'));

                    //assuming that message always comes in JSON format: application/json
                    switch (contentType)
                    {
                        case "application/json":
                            //checks if the json request is backup database

                            //call to api manager to handle the request
                            return APIManager.CommandMain.GetInstance().Execute(data);
                        case "image/png":
                        case "image/gif":
                        case "image/jpeg":
                            return uploadImage(data);
                        case "application/octet-stream":
                            return restoreDatabase(data);
                        case "text/csv":
                            return getAnalyticsCSV();
                        case "application/pdf":
                            return getAnalyticsPdf();

                    }
                    //checks if the content type is supported 
                    if (_request.ContentType != null && (_request.ContentType == "application/vnd.ms-excel" ||
                                                         _request.ContentType.IndexOf("excel", StringComparison.Ordinal) >
                                                         0 ||
                                                         _request.ContentType.IndexOf("csv", StringComparison.Ordinal) >
                                                         0) && data.Length>0)
                        return uploadCSV(data);
                }
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("[Web Server] Fail in HandleData", this, ex);
            }
            return null;
        }

        private byte[] getAnalyticsPdf()
        {
            var user = _request.QueryString.GetValues("userid");
            var userId = 0;
            var sourceData = string.Empty;
            if (user != null)
                userId = Convert.ToInt32(user[0]);
            var source = _request.QueryString.GetValues("source");
            if (source != null)
                sourceData = source[0];
            var path = string.Format(@"{0}\pdf-ready\{1}\{2}", ConfigurationManager.AppSettings["backupPath"], userId,
                sourceData);
            if (!Directory.Exists(path)) return null;

            var fullPath = string.Format(@"{0}\data.pdf", path);
            return File.ReadAllBytes(fullPath);
        }

        private byte[] getAnalyticsCSV()
        {
            var user = _request.QueryString.GetValues("userid");
            var userId = 0;
            var sourceData = string.Empty;
            if (user != null)
                userId = Convert.ToInt32(user[0]);
            var source = _request.QueryString.GetValues("source");
            if (source != null)
                sourceData = source[0];
            var path = string.Format(@"{0}\csv-ready\{1}\{2}", ConfigurationManager.AppSettings["backupPath"], userId,
                sourceData);
            if (!Directory.Exists(path)) return null;

            var fullPath = string.Format(@"{0}\data.csv", path);
            return File.ReadAllBytes(fullPath);
        }

        private byte[] uploadCSV(string data)
        {
            dynamic response = new ExpandoObject();
            response.Result = "Error";
            response.RequestID = "";
            try
            {
                var requestId = _request.QueryString.GetValues("requestid");
                var userId = Convert.ToInt32(_request.QueryString.Get("userid"));
                
                //checks if got 2 parameters
                if (requestId != null && userId > 0)
                {
                    response.RequestID = requestId;
                    int customerId;
                    //if has userid then get the customer id
                    Common.User2Customer.TryGetValue(userId,out customerId);

                    var arrayData = _request.ContentEncoding.GetBytes(data);
                    using (var ms = new MemoryStream(arrayData))
                    {
                        //load into stream reader
                        var reader = new StreamReader(ms);
                        var builder = new StringBuilder();
                        builder.AppendFormat(
                            "INSERT INTO devices(SerialNumber,customerid,SiteID,StatusID) values");
                        //flag to know if found any serial number to insert
                        var bIsSerialNumberFound = false;
                        while (!reader.EndOfStream)
                        {
                            //read line
                            var line = reader.ReadLine();
                            if (line != null)
                            {
                                //get the first column which is the serial number
                                var values = line.Split(',');
                                //checks if the first valus is a number
                                if (values.Length > 0)
                                {
                                    int serialNumber;
                                    if (int.TryParse(values[0], out serialNumber))
                                    {
                                        if (bIsSerialNumberFound)
                                            builder.AppendFormat(",");
                                        else
                                            bIsSerialNumberFound = true;    
                                        
                                        //add to list of insert batch the serial number
                                        builder.AppendFormat("({0},{1},1,2)", serialNumber, customerId);
                                    }
                                }
                            }
                        }
                        if (bIsSerialNumberFound)
                        {
                            DataManager.Instance.InsertDevicesBatch(builder.ToString());
                        }
                    }
                    response.Result = "OK";
                }
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("[Web Server] Fail in uploadCSV", this, ex);
            }
           
            return Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(response));
        }

        private byte[] restoreDatabase(string data)
        {
            dynamic response = new ExpandoObject();
            response.Result = "Error";
            var request = _request.QueryString.GetValues("requestid");
            var requestId = "";
            if (request != null)
                requestId = request[0];
           
            var array = _request.ContentEncoding.GetBytes(data);

            var userId = Convert.ToInt32(_request.QueryString.Get("userid"));
            int customerId;
            Common.User2Customer.TryGetValue(userId, out customerId);
            // using (var fileStream = new FileStream(filePath, FileMode.CreateNew, FileAccess.ReadWrite))
            using (var m = new MemoryStream(array))
            {
                //m.CopyTo(fileStream);
                //backupData = S3Manager.Instance.UploadReportTemplatesHeader(key, m,
                //    _request.ContentType, _request.ContentLength64);
                if (SampleManager.Instance.Restore(customerId, m))
                    response.Result = "OK";
            }

            response.RequestID = requestId;
            

            return Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(response));
        }

        private byte[] uploadImage(string data)
        {
            var request = _request.QueryString.GetValues("requestid");
            var requestId = "";
            if (request != null)
                requestId = request[0];
            //upload to the S3 the file
            var key =
                Convert.ToInt32(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds);

            //var filePath = string.Format("C:\\temp\\{0}.{1}", key, _request.ContentType.Split('/')[1]);

            var aray = _request.ContentEncoding.GetBytes(data);

            dynamic imageData;

            // using (var fileStream = new FileStream(filePath, FileMode.CreateNew, FileAccess.ReadWrite))
            using (var m = new MemoryStream(aray))
            {
                //m.CopyTo(fileStream);
                imageData = S3Manager.Instance.UploadReportTemplatesHeader(key, m,
                    _request.ContentType, _request.ContentLength64);
            }

            //get the image url and send it back to the client
            dynamic response = new ExpandoObject();
            response.Result = "OK";
            response.RequestID = requestId;
            response.Data = new ExpandoObject();
            response.Data.imageSrc = imageData.imageUrl;
            response.Data.imageID = key;
            response.Data.imageType = _request.ContentType.Split('/')[1];

            return Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(response));
        }
    }
}
