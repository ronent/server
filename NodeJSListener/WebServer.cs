﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Infrastructure;
using Infrastructure.Interfaces;
using Log4Tech;
using Newtonsoft.Json;
using Server.Infrastructure;

namespace NodeJSListener
{
   public class WebServer : IListener
    {
        private readonly HttpListener _listener = new HttpListener();

        public WebServer()
        {
            if (!HttpListener.IsSupported)
                throw new NotSupportedException(
                    "Needs Windows XP SP2, Server 2003 or later.");
            var listenPort = Convert.ToInt32( ConfigurationManager.AppSettings["WebServerPort"]);
            _listener.Prefixes.Add(string.Format("http://*:{0}/", listenPort));
            Log.Instance.Write("Webserver listening on port {0}...", this, Log.LogSeverity.DEBUG, listenPort);
            _listener.Start();
        }

        public void Start()
        {
            ThreadPool.QueueUserWorkItem((o) =>
            {
                try
                {
                    while (_listener.IsListening)
                    {
                        ThreadPool.QueueUserWorkItem((c) =>
                        {
                            var ctx = c as HttpListenerContext;
                            try
                            {
                                if (ctx != null)
                                {
                                    var request = ctx.Request;
                                    if (request.ContentType == "application/bak")
                                    {
                                        var fs = backupDatabase(request);
                                        ctx.Response.ContentLength64 = fs.Length;
                                        ctx.Response.StatusCode = 200;
                                        ctx.Response.ContentType = "application/bson";
                                        ctx.Response.AddHeader("content-length", fs.Length.ToString());
                                        ctx.Response.OutputStream.Write(fs, 0, fs.Length);
                                    }
                                    else
                                    {
                                        var webManager = new WebManager(request, ctx.Response, this);
                                        var response = webManager.HandleData();
                                        ctx.Response.StatusCode = 500;
                                        if (response == null)
                                        {
                                            dynamic responseData = new ExpandoObject();
                                            responseData.Result = "Error";
                                            responseData.RequestID = "";
                                            response = Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(responseData));
                                        }
                                        //var buf = Encoding.UTF8.GetBytes(response);
                                        ctx.Response.ContentLength64 = response.Length;
                                        ctx.Response.StatusCode = 200;
                                        ctx.Response.ContentType = getContentType(ctx.Request);
                                        ctx.Response.AddHeader("content-length", response.Length.ToString());
                                        ctx.Response.OutputStream.Write(response, 0, response.Length);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Log.Instance.WriteError("Fail in WebServer listener", this, ex);
                            }
                            finally
                            {
                                // always close the stream
                                if (ctx != null && ctx.Response.OutputStream.CanRead) 
                                    ctx.Response.OutputStream.Close();
                            }
                        }, _listener.GetContext());
                    }
                }
                catch
                {
                    // ignored
                } // suppress any exceptions
            });
        }

        public void Stop()
        {
            throw new NotImplementedException();
        }

        private byte[] backupDatabase(HttpListenerRequest request)
        {
            var userId = request.QueryString.Get("userid");
            int customerId;
            if (Common.User2Customer.TryGetValue(Convert.ToInt32(userId), out customerId))
            {
                return SampleManager.Instance.Backup(customerId);
            }
            return null;
        }

        private string getContentType(HttpListenerRequest request)
        {
            try
            {
                //return the content type according to the action
                var action = request.QueryString.Get("Action");
                if (action != null)
                {
                    //checks which action was sent 
                    switch (action)
                    {
                        case "certificate":
                            return "application/pdf";

                    }
                }
            }
            catch (Exception ex)
            {
                // ignored
            }
            return "application/json";
        }
    }
}
