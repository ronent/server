﻿using Infrastructure;
using Infrastructure.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLStorage
{
    public class DBGet : DBBase
    {
        public DBGet(string connString, string provider)
        {
            Init(connString, provider);
        }


        #region methods
        internal List<SensorSample> GetDownloadData(int deviceSensorID, int startTime, int endTime, int alarmStatus)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            List<SensorSample> response = new List<SensorSample>();
            try
            {
                parameters.Add(CreateParameter("@DeviceSensorID", deviceSensorID, DbType.Int32));
                parameters.Add(CreateParameter("@StartTime", startTime, DbType.Int32));
                parameters.Add(CreateParameter("@EndTime", endTime, DbType.Int32));
                parameters.Add(CreateParameter("@AlarmStatus", alarmStatus, DbType.Int32));
                execute(parameters, response);
            }
            catch (Exception ex)
            {
                Logger4Net.Error("Fail in GetDownloadData", ex);
            }
            return response;
        }

        internal List<SensorSample> GetDownloadData(int deviceSensorID, bool lastDownloadData, int alarmStatus)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            List<SensorSample> response = new List<SensorSample>();
            try
            {
                parameters.Add(CreateParameter("@DeviceSensorID", deviceSensorID, DbType.Int32));
                parameters.Add(CreateParameter("@IsLastDownload", lastDownloadData, DbType.Int16));
                parameters.Add(CreateParameter("@AlarmStatus", alarmStatus, DbType.Int32));
                execute(parameters, response);
            }
            catch (Exception ex)
            {
                Logger4Net.Error("Fail in GetDownloadData", ex);
            }
            return response;
        }

        internal List<SensorSample> GetDownloadData(int deviceSensorID)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            List<SensorSample> response = new List<SensorSample>();
            try
            {
                parameters.Add(CreateParameter("@DeviceSensorID", deviceSensorID, DbType.Int32));
                execute(parameters, response);
            }
            catch (Exception ex)
            {
                Logger4Net.Error("Fail in GetDownloadData", ex);
            }
            return response;
        }

        internal DataTable GetSensorLogsData(int startTime, int endTime)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            DataTable response = new DataTable();
            try
            {
                parameters.Add(CreateParameter("@StartTime", startTime, DbType.Int32));
                parameters.Add(CreateParameter("@EndTime", endTime, DbType.Int32));
                return Execute(SPNames.GetSensorLogsData, parameters);
            }
            catch(Exception ex)
            {
                Logger4Net.Error("Fail in GetSensorLogsData", ex);
            }
            return response;
        }
        #endregion

        #region private methods

        private void execute(List<DbParameter> parameters, List<SensorSample> response)
        {
            DataTable dt = Execute(SPNames.GetDownloadData, parameters);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    SensorSample Sample = new SensorSample();

                    Sample.TimeStampComment = row["TimeStampComment"].ToString();
                    Sample.SampleTime = Convert.ToInt32(row["SampleTime"]);
                    Sample.IsTimeStamp = Convert.ToBoolean(row["IsTimeStamp"]);
                    Sample.Value = Convert.ToDouble(row["Value"]);
                    Sample.AlarmStatus = Convert.ToInt16(row["AlarmStatus"]);
                    Sample.IsDummy = Convert.ToInt16(row["isDummy"]);

                    response.Add(Sample);
                }
            }
        }
        #endregion

    }
}
