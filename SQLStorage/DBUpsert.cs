﻿using Infrastructure;
using Infrastructure.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLStorage
{
    public class DBUpsert : DBBase
    {
        public DBUpsert(string connString, string provider)
        {
            Init(connString, provider);
        }

        internal bool InsertBulkSensorLogsData(DataTable dt)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(_connString))
                {
                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(con.ConnectionString, SqlBulkCopyOptions.TableLock))
                    {
                        bulkCopy.DestinationTableName = "SensorLogs";
                        bulkCopy.BatchSize = dt.Rows.Count;
                        con.Open();
                        bulkCopy.WriteToServer(dt);
                    }
                }
                return true;
            }
            catch(Exception ex)
            {
                Logger4Net.Error("Fail in InsertBulkSensorLogsData", ex);
            }
            return false;
        }

        internal bool AddNewSample(int deviceSensorID, double value, int sampleTime, bool isTimeStamp, string comment, Int16 alarmStatus, int isDummy)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            try
            {
                parameters.Add(CreateParameter("@DeviceSensorID", deviceSensorID, DbType.Int32));
                parameters.Add(CreateParameter("@Value", isDummy == 1 ? 0 : value, DbType.Double));
                parameters.Add(CreateParameter("@SampleTime", sampleTime, DbType.Int32));
                parameters.Add(CreateParameter("@IsTimeStamp", isTimeStamp, DbType.Int32));
                parameters.Add(CreateParameter("@Comment", isTimeStamp ? comment : "", DbType.String));
                parameters.Add(CreateParameter("@AlarmStatus", alarmStatus, DbType.Int16));
                parameters.Add(CreateParameter("@IsDummy", isDummy, DbType.Int16));

                DataTable dt = Execute(SPNames.AddNewSample, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    if (dt.Rows[0][0] != null && dt.Rows[0][0].ToString().Length > 0)
                    {
                        if (dt.Rows[0][0].ToString() == "1")
                        {
                            return true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger4Net.Error("Fail in AddNewSample", ex);
            }
            return false;
        }

        internal bool WipeSensorLogsData()
        {
            try
            {
                DataTable dt = Execute(SPNames.WipeSensorLogsData);
                if (dt != null && dt.Rows.Count > 0 && Convert.ToInt32(dt.Rows[0][0]) > 0)
                {
                    return true;
                }
            }
            catch(Exception ex)
            {
                Logger4Net.Error("Fail in WipeSensorLogsData", ex);
            }
            return false;
        }
    }
}
