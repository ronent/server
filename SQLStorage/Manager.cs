﻿using Infrastructure;
using Infrastructure.DAL;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLStorage
{
    public class Manager
    {
        #region Fields
        private DBUpsert dbUpsert;
        private DBGet dbGet;
        private string backupPath = ConfigurationManager.AppSettings["backupPath"];
        #endregion

        #region ctor
        private Manager()
        {
            dbUpsert = new DBUpsert(ConfigurationManager.AppSettings["SQLConnection"], DBProviders.MSSQL);
            dbGet = new DBGet(ConfigurationManager.AppSettings["SQLConnection"], DBProviders.MSSQL);
        }

        private static readonly Lazy<Manager> lazy = new Lazy<Manager>(() => new Manager());

        public static Manager Instance
        {
            get { return lazy.Value; }
        }
        #endregion

        #region methods
        internal bool Insert(int deviceSensorID, double value, int sampleTime, bool isTimeStamp, string timeStampComment, short alarmStatus, int isDummy)
        {
            return dbUpsert.AddNewSample(deviceSensorID, value, sampleTime, isTimeStamp, timeStampComment, alarmStatus, isDummy);
        }
        
        internal string Find(int deviceSensorID)
        {
            return JsonConvert.SerializeObject(dbGet.GetDownloadData(deviceSensorID));
        }

        internal string Find(int deviceSensorID, bool lastDownloadData, short alarmStatus)
        {
            return JsonConvert.SerializeObject(dbGet.GetDownloadData(deviceSensorID, lastDownloadData, alarmStatus));
        }

        internal string Find(int deviceSensorID, int startTime,int endTime, short alarmStatus)
        {
            return JsonConvert.SerializeObject(dbGet.GetDownloadData(deviceSensorID, startTime,endTime, alarmStatus));
        }

        internal bool Backup(bool wipeData = false, int startTime = 0, int endTime = 0)
        {
            Logger4Net.Debug("Start backup table of SensorLogs");
            try
            {
                Logger4Net.Debug("Get the data from the table of SensorLogs");
                //first get the table data 
                DataTable table = dbGet.GetSensorLogsData(startTime, endTime);
                Logger4Net.Debug(string.Format("SensorLogs rows {0}", table.Rows.Count));
                
                // create a list of strings to hold the file rows
                var lines = new List<string>(); 
                // if there are headers add them to the file first
               // if (isFirstRowHeader)
                //{
                string[] colnames = table.Columns.Cast<DataColumn>().Select(column => column.ColumnName).ToArray();
                var header = string.Join(",", colnames);
                lines.Add(header);
               // }

                // Place commas between the elements of each row
                var valueLines = table.AsEnumerable().Cast<DataRow>().Select(row => string.Join(",", row.ItemArray.Select(o => o.ToString()).ToArray()));

                // Stuff the rows into a string joined by new line characters
                var allLines = string.Join(Environment.NewLine, valueLines.ToArray<string>());
                lines.Add(allLines);

                Logger4Net.Debug("Gonna write the rows to csv format");
                // put that file to bed
                File.WriteAllLines(string.Format("{0}\\SensorLogs.csv",backupPath), lines.ToArray());
                //if the flag of delete is up
                if (wipeData)
                {
                    Logger4Net.Debug("Wipe data of sensor logs begun");
                    //wipe all data from the table
                    if(dbUpsert.WipeSensorLogsData())
                    {
                        Logger4Net.Info("All data of SensorLogs was wipe out");
                    }
                }
                Logger4Net.Debug("Finished backup table of SensorLogs");
                return true;
            }
            catch (Exception ex)
            {
                Logger4Net.Error("Fail in Backup", ex);
            }
            return false;
        }

        internal bool Restore()
        {
            try
            {
                StreamReader sr = new StreamReader(string.Format("{0}\\SensorLogs.csv", backupPath));
                string line = sr.ReadLine();
                string[] value = line.Split(',');
                DataTable dt = new DataTable();
                DataRow row;
                foreach (string dc in value)
                {
                    dt.Columns.Add(new DataColumn(dc));
                }

                while (!sr.EndOfStream)
                {
                    value = sr.ReadLine().Split(',');
                    if (value.Length == dt.Columns.Count)
                    {
                        row = dt.NewRow();
                        row.ItemArray = value;
                        dt.Rows.Add(row);
                    }
                }
                return dbUpsert.InsertBulkSensorLogsData(dt);
            }
            catch (Exception ex)
            {
                Logger4Net.Error("Fail in Restore", ex);
            }
            return false;
        }
        #endregion
    }
}
