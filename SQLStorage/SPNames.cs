﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLStorage
{
    public class SPNames
    {
        public const string GetDownloadData = "spGetDownloadData";
        public const string GetSensorLogsData = "spGetSensorLogsData";
        public const string AddNewSample = "spAddNewSample";
        public const string WipeSensorLogsData = "spWipeSensorLogsData";
    }
}
