﻿using Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLStorage
{
    class SQLAPI : ISample
    {
        #region ISample Members

        public string Find(int deviceSensorID, int startTime, int endTime, short alarmStatus = 0)
        {
            return Manager.Instance.Find(deviceSensorID, startTime, endTime, alarmStatus);
        }

        public string Find(int deviceSensorID, bool lastDownloadData = true, short alarmStatus = 0)
        {
            return Manager.Instance.Find(deviceSensorID, lastDownloadData, alarmStatus);
        }

        public string Find(int deviceSensorID)
        {
            return Manager.Instance.Find(deviceSensorID);
        }

        public bool InsertSample(int deviceSensorID, double value, int sampleTime, bool isTimeStamp, string timeStampComment, short alarmStatus, int isDummy)
        {
            return Manager.Instance.Insert(deviceSensorID, value, sampleTime, isTimeStamp, timeStampComment, alarmStatus, isDummy);
        }

        public bool Backup(bool wipeData = false, int startTime = 0, int endTime = 0)
        {
            return Manager.Instance.Backup(wipeData, startTime, endTime);
        }

        public bool Restore()
        {
            return Manager.Instance.Restore();
        }
        #endregion

    }
}
