﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLStorage
{
    public class SensorSample
    {
        public Int32 DeviceSensorID { get; set; }
        public double Value { get; set; }
        public int SampleTime { get; set; }
        public bool IsTimeStamp { get; set; }
        public string TimeStampComment { get; set; }
        public Int16 AlarmStatus { get; set; }
        public Int16 IsDummy { get; set; }
    }
}
