﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ZeroMQ;
using ZeroMQ.Devices;


namespace ZMQServer
{
    class Listener //: ZMQBase
    {

        ZMQPublisher publisher;

        public Listener()
        {
            publisher = new ZMQPublisher();
            Logger4Net.Debug("After finish zmq ctor call");
            
        }


        public void Start()
        {
            using (var context = ZmqContext.Create())
            {

                using (var queue = new ZeroMQ.Devices.QueueDevice(context, "tcp://*:" + Convert.ToInt32( ConfigurationManager.AppSettings["listenPort"]), "inproc://workers", DeviceMode.Blocking))
                {
                    queue.Initialize();
                    var workerThreads = new Thread[5];
                    for (int threadId = 0; threadId < workerThreads.Length; threadId++)
                    {
                        workerThreads[threadId] = new Thread(WorkerRoutine);
                        workerThreads[threadId].Start(context);
                    }
                    queue.Start();
                }

            }
        }


        private void WorkerRoutine(object context)
        {
            ZmqSocket receiver = ((ZmqContext)context).CreateSocket(SocketType.REP);
            receiver.Connect("inproc://workers");

            while (true)
            {
                string message = receiver.Receive(Encoding.ASCII);

                Logger4Net.Debug(string.Format("Got from client: {0}", message));

                string response = string.Empty;

                if (message != null && message.Length > 0)
                {
                    string action = message.Split(',')[0];
                    string endpoint = message.Split(',')[1];
                    switch (action)
                    {
                        case "Identify":
                            publisher.Publish(string.Format("{0},Download",endpoint));
                            response = "ACK";
                            break;
                        case "DownloadData":
                            response = "PDF Link is ready!";
                            break;
                    }
                }

                receiver.Send(response, Encoding.ASCII);
                Logger4Net.Debug(string.Format("Sent back to client: {0}",response));
            }
        }
        

     
       
    }
}
