﻿using Infrastructure;
using Server.Infrastructure.SMS;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Server.Infrastructure;

namespace Server
{
    partial class MainService : ServiceBase
    {
        public MainService()
        {
            InitializeComponent();
        }
        #region Service Events
        protected override void OnStart(string[] args)
        {
            setLogParams();
            //start the listeners
            ListenerManager.Instance.Start();
            SMSManager.Instance.Start();
            DeviceManager.Instance.Start();
            ReportAutomation.Start();

            Log4Tech.Log.Instance.Write("Server is up", this, Log4Tech.Log.LogSeverity.INFO);
        }

        protected override void OnStop()
        {
            Log4Tech.Log.Instance.Write("Server is stopped!", this, Log4Tech.Log.LogSeverity.INFO);
        }
        #endregion
        #region private methods
        private bool stopSimioService()
        {
            try
            {
                ServiceController scSimio = new ServiceController("Simio");
                if (scSimio.CanStop && scSimio.Status == ServiceControllerStatus.Running)
                {
                    scSimio.Stop();
                    while (scSimio.Status != ServiceControllerStatus.Stopped)
                    {
                        Thread.Sleep(1000);
                        scSimio.Refresh();
                    }
                    return true;
                }
                if (scSimio.Status == ServiceControllerStatus.Stopped)
                    return true;
            }
            catch(Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in stopSimioService, Service 'Simio' is not exists/failed", this, ex);
            }
            return false;
        }

        private bool startSimioService()
        {
            try
            {
                ServiceController scSimio = new ServiceController("simio");
                if (scSimio.Status == ServiceControllerStatus.Stopped)
                {
                    scSimio.Start();
                    while (scSimio.Status != ServiceControllerStatus.Running)
                    {
                        Thread.Sleep(1000);
                        scSimio.Refresh();
                    }
                    return true;
                }
                else if (scSimio.Status == ServiceControllerStatus.Running)
                {
                    if (stopSimioService())
                    {
                        scSimio.Start();
                        while (scSimio.Status != ServiceControllerStatus.Running)
                        {
                            Thread.Sleep(1000);
                            scSimio.Refresh();
                        }
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in startSimioService, Service 'Simio' is not exists/failed", this, ex);
            }
            return false;
        }

        private void setLogParams()
        {
            var bCloud = true;
            var bConsole = false;
            var bFile = true;
            var LogFolder = "Logs";
            //checks if config has parameter then take it, otherwise set default value
            if (System.Configuration.ConfigurationManager.AppSettings["IsWrite2Cloud"] != null && System.Configuration.ConfigurationManager.AppSettings["IsWrite2Cloud"].Length > 0)
            {
                bool.TryParse(System.Configuration.ConfigurationManager.AppSettings["IsWrite2Cloud"], out bCloud);
            }
            if (System.Configuration.ConfigurationManager.AppSettings["IsWrite2Console"] != null && System.Configuration.ConfigurationManager.AppSettings["IsWrite2Console"].Length > 0)
            {
                bool.TryParse(System.Configuration.ConfigurationManager.AppSettings["IsWrite2Console"], out bConsole);
            }
            if (System.Configuration.ConfigurationManager.AppSettings["IsWrite2File"] != null && System.Configuration.ConfigurationManager.AppSettings["IsWrite2File"].Length > 0)
            {
                bool.TryParse(System.Configuration.ConfigurationManager.AppSettings["IsWrite2File"], out bFile);
            }
            if (System.Configuration.ConfigurationManager.AppSettings["LogFolder"] != null && System.Configuration.ConfigurationManager.AppSettings["LogFolder"].Length > 0)
            {
                LogFolder = System.Configuration.ConfigurationManager.AppSettings["LogFolder"];
            }

            Log4Tech.Log.Instance.IsWrite2Cloud = bCloud;
            Log4Tech.Log.Instance.IsWrite2Console = bConsole;
            Log4Tech.Log.Instance.IsWrite2File = bFile;
            Log4Tech.Log.Instance.LogFolderPath = LogFolder;
        }
        #endregion
    }
}
