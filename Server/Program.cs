﻿using Infrastructure;
using Server.Infrastructure.SMS;
using System;
using System.IO;
using System.ServiceModel;
using System.ServiceProcess;
using Server.Infrastructure;

namespace Server
{
    class Program
    {
        static int Main(string[] args)
        {
            //init the logger
            Log4Tech.Log.Instance.IsWrite2Cloud = false;
            Log4Tech.Log.Instance.IsWrite2Console = true;
            Log4Tech.Log.Instance.IsWrite2File = true;
            Log4Tech.Log.Instance.TakeNetworkTime = false;

            Log4Tech.Log.Instance.Write("Start Kick Off...","Main");

            //checks if the license is valid
            var lic = new LicenseAPI.Manager();
            var executablePath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            var licensePath = string.Format("{0}\\license.lic", executablePath);
            Log4Tech.Log.Instance.Write("Try to locate the license at {0} and check if license is valid", "Main", Log4Tech.Log.LogSeverity.DEBUG, executablePath);
            if (!File.Exists(licensePath) || lic.IsLicenseExpired(licensePath))
            {
                Log4Tech.Log.Instance.Write("License is not valid!", "Main");
                return 0;
            }

            //TODO:create folders if not already exists from app.config


            if (args != null && args.Length > 0 && args[0] == "debugOnly")
            {
                //start the listeners
                ListenerManager.Instance.Start();
                SMSManager.Instance.Start();
                DeviceManager.Instance.Start();
                ReportAutomation.Start();
//                var host = new ServiceHost(typeof (WcfDuplexCommunication.Server));
//                
//                host.Open();
//
//                Console.WriteLine("state:{0}",  host.State);
                Console.Read();
            }
            
            else
            {
                if (!Environment.UserInteractive)
                    // running as service
                    using (var service = new MainService())
                        ServiceBase.Run(service);
            }
            return 1;
        }
    }
}
