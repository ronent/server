﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZeroMQ;

namespace ZMQServer
{
    class ZMQPublisher
    {

        public void Publish(string msg)
        {
            using (ZmqContext context = ZmqContext.Create())
            {
                using (ZmqSocket publisher = context.CreateSocket(SocketType.PUB))
                {
                    try
                    {
                        publisher.Bind("tcp://*:" + ConfigurationManager.AppSettings["publisherPort"]);
                        publisher.Send(msg, Encoding.ASCII);
                    }
                    catch (ZmqException zx)
                    {
                        Logger4Net.Error("Fail in Publish (ZMQ)", zx);
                    }
                    catch (System.Exception ex)
                    {
                        Logger4Net.Error("Fail in Publish", ex);
                    }
                }
            }
        }

    }
}
