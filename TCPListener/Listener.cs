﻿using Infrastructure;
using Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace TCPListener
{
    public class Listener //: IListener
    {

        //#region IListenerBase

        //public void Stop()
        //{
        //    throw new NotImplementedException();
        //}

        //public async void Start()
        //{
        //    IPAddress localIP = IPAddress.Any;
        //    TcpListener listener = new TcpListener(localIP, Convert.ToInt16(ConfigurationManager.AppSettings["LanRcvListenPort"]));
            
        //    // new for yuval !
        //    listener.Server.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, true);
            
        //    listener.Start();
        //   // Logger4Net.Debug("Server is running and listen on port " + this.listeningPort);

        //    while (true)
        //    {
        //        try
        //        {

        //            var tcpClient = await listener.AcceptTcpClientAsync();

        //            tcpClient.NoDelay = true;
        //            tcpClient.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, true);
        //            if (!isInternalIP(tcpClient.Client.RemoteEndPoint))
        //            {
        //                HandleConnectionAsync(tcpClient);
        //            }
        //        }
        //        catch (Exception exp)
        //        {
        //            Log4Tech.Log.Instance.WriteError("Fail in StartListen",this, exp);
        //        }
        //    }
        //}
        //#endregion

        //private void HandleConnectionAsync(TcpClient tcpClient)
        //{
        //    Log4Tech.Log.Instance.Write("Got connection request from {0}",this, Log4Tech.Log.LogSeverity.DEBUG, ((System.Net.IPEndPoint)(tcpClient.Client.RemoteEndPoint)).Address);
        //    WaitForData(tcpClient);
        //}

        //private bool isInternalIP(EndPoint remoteEndPoint)
        //{
        //    NetworkInterface[] adapters = NetworkInterface.GetAllNetworkInterfaces();
        //    foreach (NetworkInterface adapter in adapters)
        //    {
        //        IPInterfaceProperties adapterProperties = adapter.GetIPProperties();
        //        IPAddressCollection dnsServers = adapterProperties.DnsAddresses;
        //        if (dnsServers.Count > 0)
        //        {
        //            foreach (IPAddress dns in dnsServers)
        //            {
        //                if (((System.Net.IPEndPoint)(remoteEndPoint)).Address.ToString() == (dns.ToString()))
        //                {
        //                    return true;
        //                }
        //            }
        //        }
        //    }

        //    return false;
        //}

        //private async void WaitForData(TcpClient tcpClient)
        //{
        //    byte[] resp = new byte[2048];
        //    NetworkStream networkStream = tcpClient.GetStream();
        //    try
        //    {
        //        int bytescount = 0;
        //        //loop till can't read from the socket
        //        //this will holds the connectivity to the client
        //        while (networkStream.CanRead)
        //        {
        //            bytescount = await networkStream.ReadAsync(resp, 0, 2048);
        //            if (bytescount > 0 && networkStream.CanWrite)
        //            {
        //                string hex = BitConverter.ToString(resp, 0, bytescount);

        //                Log4Tech.Log.Instance.Write("Got from client : {0}",this, Log4Tech.Log.LogSeverity.DEBUG, hex);

        //                tcpClient.SendBufferSize = bytescount;

        //                //if (resp[7] == 0x01)
        //                //{
        //                //    //parse the username and password for DEBUG only
        //                //    /*******************************************************/
        //                //    //data of username is on byte 9 till 0x0
                            
        //                //    var subset = new byte[20];
        //                //    for (int i = 8; i < 28; i++)
        //                //    {
        //                //        if (resp[i]==0)
        //                //            break;
                                
        //                //        Buffer.BlockCopy(resp,i,subset,i-8,1);
        //                //    }
        //                //    var username = ASCIIEncoding.ASCII.GetString(subset).Replace("\0", "").Trim();
        //                //    Logger4Net.Debug(string.Format("username after parse: {0} {1}", username,username.Length));
                            
        //                //    //data of password is on byte 28 till 0x0
        //                //    var subset2 = new byte[20];
        //                //    for (int i = 28; i < 48; i++)
        //                //    {
        //                //        if (resp[i] == 0)
        //                //            break;

        //                //        Buffer.BlockCopy(resp, i, subset2, i - 28, 1);
        //                //    }
        //                //    var pwd = ASCIIEncoding.ASCII.GetString(subset2, 0, 20).Replace("\0", "").Trim();
        //                //    Logger4Net.Debug(string.Format("pwd after parse: {0} {1}", pwd,pwd.Length));
        //                //    //checking against user: Admin and pwd: Admin
        //                //    if (username.Equals("Admin") && pwd.Equals("admin"))
        //                //    {
        //                //        Logger4Net.Debug(string.Format("Pass the validation, Write to client : {0}", BitConverter.ToString(resp, 0, bytescount)));
        //                //        //if true return echo
        //                //        await networkStream.WriteAsync(resp, 0, bytescount);
        //                //    }
        //                //    else
        //                //    {
        //                //        Logger4Net.Debug(string.Format("The validation of login failed, disconnect the tcp connection", BitConverter.ToString(resp, 0, bytescount)));
        //                //        //else disconnect the connection
        //                //        tcpClient.GetStream().Close();
        //                //        tcpClient.Close();
        //                //    }
        //                //}
        //                //else
        //                //{
        //                    //write echo back
        //                     await networkStream.WriteAsync(resp, 0, bytescount);
        //                     Log4Tech.Log.Instance.Write("Write to client : {0}",this,   Log4Tech.Log.LogSeverity.DEBUG, BitConverter.ToString(resp, 0, bytescount));
        //                //}
        //                /*******************************************************/
        //            }
        //            else
        //            {
        //                break;
        //            }
        //        }
        //        //checks if client is still connected...
        //        if (tcpClient.Client != null && tcpClient.Client.Poll(0, SelectMode.SelectRead))
        //        {
        //            byte[] checkConn = new byte[1];
        //            if (tcpClient.Client!=null && tcpClient.Client.Receive(checkConn, SocketFlags.Peek) == 0)
        //            {
        //                Log4Tech.Log.Instance.Write("{0} has been disconnected!",this, Log4Tech.Log.LogSeverity.DEBUG, ((System.Net.IPEndPoint)(tcpClient.Client.RemoteEndPoint)).Address);
        //                tcpClient.GetStream().Close();
        //                tcpClient.Close();
        //            }
        //        }
        //    }
        //    catch (SocketException sx)
        //    {
        //        Log4Tech.Log.Instance.WriteError("Fail in WaitForData(socket disposed)",this, sx);
        //    }
        //    catch (ObjectDisposedException ox)
        //    {
        //        Log4Tech.Log.Instance.WriteError("Fail in WaitForData(object disposed)",this, ox);
        //    }
        //    catch (System.IO.IOException io)
        //    {
        //        Log4Tech.Log.Instance.WriteError("Fail in WaitForData(IO issue)",this, io);
        //    }
        //    catch (Exception ex)
        //    {
        //        Log4Tech.Log.Instance.WriteError("Fail in WaitForData",this, ex);
        //    }
        //}

       
       
    }
}
