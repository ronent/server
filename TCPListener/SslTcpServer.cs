﻿using Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace TCPListener
{
    public class SslTcpServer : IListener
    {
        X509Certificate serverCertificate = null;
        // The certificate parameter specifies the name of the file  
        // containing the machine certificate. 
        public void RunServer(string certificate)
        {
            serverCertificate = X509Certificate.CreateFromCertFile(certificate);
            // Create a TCP/IP (IPv4) socket and listen for incoming connections.
            TcpListener listener = new TcpListener(IPAddress.Any, Convert.ToInt16(ConfigurationManager.AppSettings["LanRcvListenPort"]));
            listener.Start();
            while (true)
            {
                Log4Tech.Log.Instance.Write("Waiting for a client to connect...",this);
                // Application blocks while waiting for an incoming connection. 
                // Type CNTL-C to terminate the server.
                TcpClient client = listener.AcceptTcpClient();
                Log4Tech.Log.Instance.Write("client accepted, gonna process client...", this);
                ProcessClient(client);
            }
        }
        
        async void ProcessClient(TcpClient client)
        {
            // A client has connected. Create the  
            // SslStream using the client's network stream.
            SslStream sslStream = new SslStream(
                client.GetStream(), false);
            // Authenticate the server but don't require the client to authenticate. 
            try
            {

                sslStream.AuthenticateAsServer(serverCertificate,
                    false, SslProtocols.Tls, true);
                // Display the properties and settings for the authenticated stream.
                DisplaySecurityLevel(sslStream);
                DisplaySecurityServices(sslStream);
                DisplayCertificateInformation(sslStream);
                DisplayStreamProperties(sslStream);

                // Set timeouts for the read and write to 5 seconds.
                sslStream.ReadTimeout = 5000;
                sslStream.WriteTimeout = 5000;
                int bytescount = 0;
                byte[] buffer = new byte[2048];
                while (sslStream.CanRead)
                {
                    // Read a message from the client.   
                    Log4Tech.Log.Instance.Write("Reading message from client...", this);
                    //string messageData = ReadMessage(sslStream);

                    bytescount = await sslStream.ReadAsync(buffer, 0, buffer.Length);

                    if (bytescount > 0 && sslStream.CanWrite)
                    {
                        string messageData = BitConverter.ToString(buffer, 0, bytescount);
                        Log4Tech.Log.Instance.Write("Received: {0}", this, Log4Tech.Log.LogSeverity.DEBUG, messageData);

                        // Write a message to the client. 
                        Log4Tech.Log.Instance.Write("Sending echo message.", this);
                        await sslStream.WriteAsync(buffer,0,bytescount);
                    }
                }
            }
            catch (AuthenticationException e)
            {
                Log4Tech.Log.Instance.WriteError("Exception: ",this, e);
               /* if (e.InnerException != null)
                {
                    Log4Tech.Log.Instance.Write("Inner exception: {0}",this, Log4Tech.Log.LogSeverity.DEBUG, e.InnerException.Message);
                }*/
                Log4Tech.Log.Instance.Write("Authentication failed - closing the connection.",this);
                sslStream.Close();
                client.Close();
                return;
            }
                catch(Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Exception: ", this, ex);
            }
            finally
            {
                // The client stream will be closed with the sslStream 
                // because we specified this behavior when creating 
                // the sslStream.
                sslStream.Close();
                client.Close();
            }
        }
        
        string ReadMessage(SslStream sslStream)
        {
            // Read the  message sent by the client. 
            // The client signals the end of the message using the 
            // "<EOF>" marker.
            byte[] buffer = new byte[2048];
            StringBuilder messageData = new StringBuilder();
            int bytes = -1;
            //do
           // {l
                // Read the client's test message.
                bytes = sslStream.Read(buffer, 0, buffer.Length);

                messageData.Append(System.Text.Encoding.ASCII.GetString(buffer, 0, bytes));

                // Use Decoder class to convert from bytes to UTF8 
                // in case a character spans two buffers.
                /*Decoder decoder = Encoding.UTF8.GetDecoder();
                char[] chars = new char[decoder.GetCharCount(buffer, 0, bytes)];
                decoder.GetChars(buffer, 0, bytes, chars, 0);
                messageData.Append(chars);
                // Check for EOF or an empty message. 
                if (messageData.ToString().IndexOf("<EOF>") != -1)
                {
                    break;
                }*/
           // } while (bytes != 0);

            return messageData.ToString();
        }
        
        void DisplaySecurityLevel(SslStream stream)
        {
            Log4Tech.Log.Instance.Write("Cipher: {0} strength {1}",this, Log4Tech.Log.LogSeverity.DEBUG, stream.CipherAlgorithm, stream.CipherStrength);
            Log4Tech.Log.Instance.Write("Hash: {0} strength {1}",this, Log4Tech.Log.LogSeverity.DEBUG, stream.HashAlgorithm, stream.HashStrength);
            Log4Tech.Log.Instance.Write("Key exchange: {0} strength {1}",this, Log4Tech.Log.LogSeverity.DEBUG, stream.KeyExchangeAlgorithm, stream.KeyExchangeStrength);
            Log4Tech.Log.Instance.Write("Protocol: {0}",this, Log4Tech.Log.LogSeverity.DEBUG, stream.SslProtocol);
        }
   
        void DisplaySecurityServices(SslStream stream)
        {
            Log4Tech.Log.Instance.Write("Is authenticated: {0} as server? {1}", this, Log4Tech.Log.LogSeverity.DEBUG, stream.IsAuthenticated, stream.IsServer);
            Log4Tech.Log.Instance.Write("IsSigned: {0}",this, Log4Tech.Log.LogSeverity.DEBUG, stream.IsSigned);
            Log4Tech.Log.Instance.Write("Is Encrypted: {0}",this, Log4Tech.Log.LogSeverity.DEBUG,  stream.IsEncrypted);
        }
       
        void DisplayStreamProperties(SslStream stream)
        {
            Log4Tech.Log.Instance.Write("Can read: {0}, write {1}",this, Log4Tech.Log.LogSeverity.DEBUG, stream.CanRead, stream.CanWrite);
            Log4Tech.Log.Instance.Write("Can timeout: {0}",this, Log4Tech.Log.LogSeverity.DEBUG, stream.CanTimeout);
        }
       
        void DisplayCertificateInformation(SslStream stream)
        {
            Log4Tech.Log.Instance.Write("Certificate revocation list checked: {0}",this, Log4Tech.Log.LogSeverity.DEBUG, stream.CheckCertRevocationStatus);

            X509Certificate localCertificate = stream.LocalCertificate;
            if (stream.LocalCertificate != null)
            {
                Log4Tech.Log.Instance.Write("Local cert was issued to {0} and is valid from {1} until {2}.",this, Log4Tech.Log.LogSeverity.DEBUG,
                    localCertificate.Subject,
                    localCertificate.GetEffectiveDateString(),
                    localCertificate.GetExpirationDateString());
            }
            else
            {
                Console.WriteLine("Local certificate is null.");
            }
            // Display the properties of the client's certificate.
            X509Certificate remoteCertificate = stream.RemoteCertificate;
            if (stream.RemoteCertificate != null)
            {
                Log4Tech.Log.Instance.Write("Remote cert was issued to {0} and is valid from {1} until {2}.",this, Log4Tech.Log.LogSeverity.DEBUG,
                    remoteCertificate.Subject,
                    remoteCertificate.GetEffectiveDateString(),
                    remoteCertificate.GetExpirationDateString());
            }
            else
            {
                Log4Tech.Log.Instance.Write("Remote certificate is null.",this);
            }
        }
        
        #region IListener Members
        public void Start()
        {
            RunServer(ConfigurationManager.AppSettings["TCPSSLCertificate"]); 
            
        }

        public void Stop()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
