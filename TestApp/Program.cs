﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InfluxDbStorage;
using Server.Infrastructure;

namespace TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var influx = new InfluxDbAPI();
            //Console.WriteLine(influx.Find(913742,1)); 
            var samples = new List<SensorSample>();
            var count = 100000;
            var batchCount = 0;
            for (var i = 0; i < count; i++)
            {
                var rand = new Random(i);
                var temp = rand.Next(0,10) + rand.NextDouble();
                var dt = Convert.ToInt32(DateTime.UtcNow.AddDays(-7).Subtract(new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds);
                samples.Add(new SensorSample
                {
                    SampleTime = dt+i,
                    Value = temp,
                    SerialNumber = 912000,
                    TimeStampComment = string.Empty,
                    SensorTypeID = 1,
                    AlarmStatus = 0,
                    IsDummy = 0,
                    IsTimeStamp = false,
                    SensorName = "InternalNTC"
                });
//                batchCount++;
//                if (batchCount == 50)
//                {
//                    influx.InsertBatch(samples);
//                    batchCount = 0;
//                }
            }
            var stopWatch =new Stopwatch();
            stopWatch.Start();
            Console.WriteLine("Before insert batch");
            influx.InsertBatch(samples);
            stopWatch.Stop();
            Console.WriteLine("After insert batch took {0} ms.",stopWatch.ElapsedMilliseconds);

//            var dt2 = Convert.ToInt32(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds);
//            var rand2 = new Random();
//            var temp2 = 23.0 + rand2.NextDouble();
//            var sample = new SensorSample
//            {
//                SampleTime = dt2 ,
//                Value = temp2,
//                SerialNumber = 913742,
//                TimeStampComment = string.Empty,
//                SensorTypeID = 1,
//                AlarmStatus = 0,
//                IsDummy = 0,
//                IsTimeStamp = false,
//                SensorName = "InternalNTC"
//            };
//            var result = influx.InsertSample(sample);
            Console.Read();
        }
    }
}
