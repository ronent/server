﻿using Server.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Twilio;

namespace TwilioSMS
{
    public class SMSAPI : ISMS
    {
        #region Members
        private TwilioRestClient client;
        private long twilioNumber;
        #endregion
        #region Constructor
        public SMSAPI()
        {
            client = new TwilioRestClient("ACf69837309260fc95689085caaff72a8d", "6defa123e2bb8fc5bd13391fb761f3f4");
            twilioNumber = 14796680063;
        }

        #endregion
        #region ISMS Members
        public void SendSMS(int countryCode, long mobileNumber, string msgBody, int retryInterval)
        {
            var msg = client.SendSmsMessage(string.Concat("+", twilioNumber), string.Concat("+", countryCode, mobileNumber), msgBody);
            if (msg.RestException != null)
            {
                if (retryInterval>0)
                {
                    Log4Tech.Log.Instance.Write("Retry sending SMS ({0}), retry to send SMS to {1}", this, Log4Tech.Log.LogSeverity.WARNING, msg.RestException.Message, msg.To);
                    SendSMS(countryCode, mobileNumber, msgBody, --retryInterval);
                }
                else
                {
                    Log4Tech.Log.Instance.Write("Fail in sending SMS to {0} - {1}", this, Log4Tech.Log.LogSeverity.WARNING, msg.To, msg.RestException.Message);
                }
            }
            else
            {
                Log4Tech.Log.Instance.Write("[{0}] SMS sent on {1} from {2} to {5} (cost - {3}) , status - {4}", this, Log4Tech.Log.LogSeverity.DEBUG, msg.DateSent, msg.From, msg.Price, msg.Status, msg.To);
            }
        }

        public void ReceiveSMS(string fromNumber, string response)
        {
            throw new NotImplementedException();
        }

        #endregion
        
        
    }
}
