﻿using Fleck;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAgentCommunication.Communication
{
    public class SocketManager : SocketCommunication.Communication.SocketManager
    {
        public SocketManager(IWebSocketConnection socketConnection, CommunicationManager communicationManager)
            :base(socketConnection, communicationManager)
        {

        }
    }
}
