﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocketProtocol.Messages.Arguments
{
    // TODO
    public class DeviceDetailsArgs
    {
        public const string command = "OnConnection";
        public string Command { get { return command; } }

        public string DevicePath { get; set; }
        public string SerialNumber { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
    }
}
