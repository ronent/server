﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Infrastructure;

namespace WebServerListener
{
    public class RequestHandler
    {
        private static ManualResetEvent mre = new ManualResetEvent(false);
        private Stream _stream;

        public byte[] Execute(string action, dynamic body)
        {
            //checks which action to execute
            switch (action)
            {
                case "certificate":
                    //call to wait for the response 
                    try
                    {
                        var device = DeviceManager.Instance.GetDevice(body);
                        ReportsManager.Instance.OnFinishReport += Instance_OnFinishReport;
                        ReportsManager.Instance.GenerateCertificate(device);
                        mre.WaitOne();
                        return getAllBytes();
                    }
                    catch
                    {
                        // ignored
                    }
                    break;
            }
            //send back to the client
            return null;
        }

        private void Instance_OnFinishReport(System.IO.Stream obj)
        {
            _stream = obj;
            mre.Set();
        }

        private byte[] getAllBytes()
        {
            var buffer = new byte[16*1024];
            using (var ms = new MemoryStream())
            {
                int read;
                while ((read = _stream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

    }
}
