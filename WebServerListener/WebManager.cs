﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Log4Tech;
//using Newtonsoft.Json;

namespace WebServerListener
{
    class WebManager
    {
        protected WebServer _parent;
        private HttpListenerRequest _request;
        private HttpListenerResponse _response;
        private RequestHandler _requsetHandler;

        public WebManager(HttpListenerRequest request,HttpListenerResponse response,  WebServer webServer)
        {
            _parent = webServer;
            _request = request;
            _response = response;
            _requsetHandler=new RequestHandler();
        }

        public byte[] HandleData()
        {
            //checks if the requset has body
            if (_request.HasEntityBody)
            {
                //get the request data
                var data = new StreamReader(_request.InputStream,
                    _request.ContentEncoding).ReadToEnd();
               // data = JsonConvert.SerializeObject(data); 
                var action = _request.QueryString.Get("Action");
                Log.Instance.Write("[Web Server] Got from UI Action {0} with data -> {1}", this, Log.LogSeverity.DEBUG,action, data);
                //call to api manager to handle the request
                return _requsetHandler.Execute(action,data);
            }
            return null;
        }

        
    }
}
