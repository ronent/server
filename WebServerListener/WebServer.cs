﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Infrastructure.Interfaces;
using Log4Tech;

namespace WebServerListener
{
    public class WebServer : IListener
    {
        private readonly HttpListener _listener = new HttpListener();

        public WebServer()
        {
            if (!HttpListener.IsSupported)
                throw new NotSupportedException(
                    "Needs Windows XP SP2, Server 2003 or later.");
            var listenPort = 6001;
            _listener.Prefixes.Add(string.Format("http://*:{0}/", listenPort));
            Log.Instance.Write("Webserver listening on port {0}...", this, Log.LogSeverity.DEBUG, listenPort);
            _listener.Start();
        }

        public void Start()
        {
             ThreadPool.QueueUserWorkItem((o) =>
            {
                try
                {
                    while (_listener.IsListening)
                    {
                        ThreadPool.QueueUserWorkItem((c) =>
                        {
                            var ctx = c as HttpListenerContext;
                            try
                            {
                                var rstr = new WebManager(ctx.Request,ctx.Response, this);
                                var response = rstr.HandleData();
                                ctx.Response.StatusCode = 500;
                                if (response != null)
                                {
                                    //var buf = Encoding.UTF8.GetBytes(response);
                                    ctx.Response.ContentLength64 = response.Length;
                                    ctx.Response.StatusCode = 200;
                                    ctx.Response.ContentType = getContentType(ctx.Request);
                                    ctx.Response.AddHeader("content-length", response.Length.ToString());
                                    ctx.Response.OutputStream.Write(response, 0, response.Length);
                                }
                            }
                            catch(Exception ex)
                            {
                                Log.Instance.WriteError("Fail in WebServer listener", this, ex);
                            } 
                            finally
                            {
                                // always close the stream
                                ctx.Response.OutputStream.Close();
                            }
                        }, _listener.GetContext());
                    }
                }
                catch
                {
                    // ignored
                } // suppress any exceptions
            });
        }

        public void Stop()
        {
            _listener.Stop();
            _listener.Close();
        }

        private string getContentType(HttpListenerRequest request)
        {
            try
            {
                //return the content type according to the action
                var action = request.QueryString.Get("Action");
                if (action != null)
                { 
                    //checks which action was sent 
                    switch (action)
                    {
                        case "certificate":
                            return "application/pdf";

                    }
                }
            }
            catch (Exception ex)
            {

            }
            return "application/json";
        }
       
    }
}
